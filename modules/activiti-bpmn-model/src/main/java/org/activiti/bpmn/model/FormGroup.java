/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.activiti.bpmn.model;

public class FormGroup extends BaseElement {

	protected String name;
	protected String expression;
	protected String beforeGroupScript;
	protected String afterGroupScript;
	
	public FormGroup(){
		
	}
	
	public FormGroup(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public FormGroup clone() {
		FormGroup clone = new FormGroup();
		clone.setValues(this);
		return clone;
	}

	public String getBeforeGroupScript() {
		return beforeGroupScript;
	}

	public void setBeforeGroupScript(String beforeGroupScript) {
		this.beforeGroupScript = beforeGroupScript;
	}

	public String getAfterGroupScript() {
		return afterGroupScript;
	}

	public void setAfterGroupScript(String afterGroupScript) {
		this.afterGroupScript = afterGroupScript;
	}

	public void setValues(FormGroup otherProperty) {
		super.setValues(otherProperty);
		setName(otherProperty.getName());
		setExpression(otherProperty.getExpression());
		setBeforeGroupScript(otherProperty.getBeforeGroupScript());
		setAfterGroupScript(otherProperty.getAfterGroupScript());
	}
	
	public String toString(){
		return "FormGroup(" + name + ",\"" + expression + "\",\"" + beforeGroupScript + "\",\"" + afterGroupScript + "\"" + ")";
	}
}
