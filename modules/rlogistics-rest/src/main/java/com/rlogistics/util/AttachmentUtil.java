package com.rlogistics.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.RLogisticsAttachment;
import com.rlogistics.packaging.PackagingMaterialInventoryUtil;
import com.rlogistics.rest.AttachmentUploadResult;
import com.rlogistics.master.identity.DoaForm;
import com.rlogistics.master.identity.MasterdataService;

public class AttachmentUtil {

	private static Logger log = LoggerFactory.getLogger(AttachmentUtil.class);
	
	public static AttachmentUploadResult upload(MultipartFile file, String key1, String key2, String key3) {
		AttachmentUploadResult result = new AttachmentUploadResult();
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			RLogisticsAttachment attachment = masterdataService.createRLogisticsAttachmentQuery().key1(key1).key2(key2).key3(key3).singleResult();
			if(attachment==null) {
				RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();
				
				newAttachment.setName(file.getOriginalFilename());
				newAttachment.setKey1(key1);
				
				if(key2 != null && !key2.equals("")){
					newAttachment.setKey2(key2);
				}
				if(key3 != null && !key3.equals("")){
					newAttachment.setKey3(key3);
				}
				
				newAttachment.setContentType(file.getContentType());
				
				/* Copy uploaded bytes into a byte[]*/
				ByteArrayOutputStream bytesOS = new ByteArrayOutputStream();
				InputStream bytesIS = file.getInputStream();
				byte[] buffer = new byte[1024];
				int numRead = 0;
				while((numRead = bytesIS.read(buffer)) != -1){
					bytesOS.write(buffer,0,numRead);
				}
				bytesIS.close();
				bytesOS.flush();
				log.debug("Size of upload:" + bytesOS.toByteArray().length);
				
				CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
				commandExecutor.execute(new Command<Boolean>() {
					@Override
					public Boolean execute(CommandContext commandContext) {
						newAttachment.setContent(bytesOS.toByteArray());					
						masterdataService.saveRLogisticsAttachment(newAttachment);
						return true;
					}
				});
				
				result.setSuccess(true);
				
				if(key1.equals("DOA")) {
					DoaForm doaForm = masterdataService.newDoaForm();
					doaForm.setAttachmentId(newAttachment.getId());
					doaForm.setCategoryId(key2);
					doaForm.setBrandId(key3);
					
					masterdataService.saveDoaForm(doaForm);
				}	
			} else {
				masterdataService.deleteRLogisticsAttachment(attachment.getId());
				
				RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();
				
				newAttachment.setName(file.getOriginalFilename());
				newAttachment.setKey1(key1);
				
				if(key2 != null && !key2.equals("")){
					newAttachment.setKey2(key2);
				}
				if(key3 != null && !key3.equals("")){
					newAttachment.setKey3(key3);
				}
				
				newAttachment.setContentType(file.getContentType());
				
				/* Copy uploaded bytes into a byte[]*/
				ByteArrayOutputStream bytesOS = new ByteArrayOutputStream();
				InputStream bytesIS = file.getInputStream();
				byte[] buffer = new byte[1024];
				int numRead = 0;
				while((numRead = bytesIS.read(buffer)) != -1){
					bytesOS.write(buffer,0,numRead);
				}
				bytesIS.close();
				bytesOS.flush();
				log.debug("Size of upload:" + bytesOS.toByteArray().length);
				
				CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
				commandExecutor.execute(new Command<Boolean>() {
					@Override
					public Boolean execute(CommandContext commandContext) {
						newAttachment.setContent(bytesOS.toByteArray());					
						masterdataService.saveRLogisticsAttachment(newAttachment);
						return true;
					}
				});
				
				result.setSuccess(true);
				if(key1.equals("DOA")) {
					DoaForm doaForm = masterdataService.createDoaFormQuery().categoryId(key2).brandId(key3).singleResult();
					doaForm.setAttachmentId(newAttachment.getId());
					
					masterdataService.saveDoaForm(doaForm);
				}
			}
					
		} catch(Exception ex){
			log.error("Exception during upload", ex);
			result.getMessages().add(ex.getMessage());
			return result;
		}
		
		return result;
	}
}
