package com.rlogistics.util;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.VariableInstance;

public class MapFieldBasedVariableScope extends VariableScopeBase {
	
	private Map<String,VariableInstance> vis = new HashMap<>();
	
	protected Map<String,VariableInstance> getVis() {
		return vis;
	}

	protected void setVis(Map<String,VariableInstance> vis) {
		this.vis = vis;
	}
}
