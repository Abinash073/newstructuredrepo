package com.rlogistics.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.ByteArrayEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstance;

public abstract class VariableScopeBase implements VariableScope {
		@Override
	public Map<String, Object> getVariables() {
		Map<String,Object> vs = new HashMap<String, Object>();
		for(String k:getVis().keySet()){
			vs.put(k, getVis().get(k).getValue());
		}
		return vs;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances() {
		return getVis();
	}

	@Override
	public Map<String, Object> getVariables(Collection<String> variableNames) {
		Map<String,Object> vs = new HashMap<String, Object>();
		for(String k:variableNames){
			vs.put(k, getVis().get(k).getValue());
		}
		return vs;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances(Collection<String> variableNames) {
		Map<String,VariableInstance> vs = new HashMap<String, VariableInstance>();
		for(String k:variableNames){
			vs.put(k, getVis().get(k));
		}
		return vs;
	}

	@Override
	public Map<String, Object> getVariables(Collection<String> variableNames, boolean fetchAllVariables) {
		return getVariables(variableNames);
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances(Collection<String> variableNames,boolean fetchAllVariables) {
		return getVariableInstances(variableNames);
	}

	@Override
	public Map<String, Object> getVariablesLocal() {
		return getVariables();
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal() {
		return getVariableInstances();
	}

	@Override
	public Map<String, Object> getVariablesLocal(Collection<String> variableNames) {
		return getVariables(variableNames);
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal(Collection<String> variableNames) {
		return getVariableInstances(variableNames);
	}

	@Override
	public Map<String, Object> getVariablesLocal(Collection<String> variableNames, boolean fetchAllVariables) {
		return getVariables(variableNames);
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal(Collection<String> variableNames,boolean fetchAllVariables) {
		return getVariableInstances(variableNames);
	}

	@Override
	public Object getVariable(String variableName) {
		VariableInstance vi = getVis().get(variableName);
		return vi != null ? vi.getValue() : null;
	}

	@Override
	public VariableInstance getVariableInstance(String variableName) {
		return getVis().get(variableName);
	}

	@Override
	public Object getVariable(String variableName, boolean fetchAllVariables) {
		return getVariable(variableName);
	}

	@Override
	public VariableInstance getVariableInstance(String variableName, boolean fetchAllVariables) {
		return getVariableInstance(variableName);
	}

	@Override
	public Object getVariableLocal(String variableName) {
		return getVariable(variableName);
	}

	@Override
	public VariableInstance getVariableInstanceLocal(String variableName) {
		return getVariableInstance(variableName);
	}

	@Override
	public Object getVariableLocal(String variableName, boolean fetchAllVariables) {
		return getVariable(variableName);
	}

	@Override
	public VariableInstance getVariableInstanceLocal(String variableName, boolean fetchAllVariables) {
		return getVariableInstance(variableName);
	}

	@Override
	public <T> T getVariable(String variableName, Class<T> variableClass) {
		return variableClass.cast(getVariable(variableName));
	}

	@Override
	public <T> T getVariableLocal(String variableName, Class<T> variableClass) {
		return variableClass.cast(getVariableInstance(variableName));
	}

	@Override
	public Set<String> getVariableNames() {
		return getVis().keySet();
	}

	@Override
	public Set<String> getVariableNamesLocal() {
		return getVariableNames();
	}

	@Override
	public void setVariable(String variableName, Object value) {
		Map<String,VariableInstance> vis = getVis();
		vis.put(variableName,new PreProcessInstanceVariableInstance(variableName, value));
		setVis(vis);
	}

	@Override
	public void setVariable(String variableName, Object value, boolean fetchAllVariables) {
		Map<String,VariableInstance> vis = getVis();
		vis.put(variableName,new PreProcessInstanceVariableInstance(variableName, value));
		setVis(vis);
	}

	@Override
	public Object setVariableLocal(String variableName, Object value) {
		setVariable(variableName,value);
		return getVariable(variableName);
	}

	@Override
	public Object setVariableLocal(String variableName, Object value, boolean fetchAllVariables) {
		return setVariableLocal(variableName, value);
	}

	@Override
	public void setVariables(Map<String, ? extends Object> variables) {
		for(String k:variables.keySet()){
			setVariable(k, variables.get(k));
		}
	}

	@Override
	public void setVariablesLocal(Map<String, ? extends Object> variables) {
		setVariables(variables);
	}
	
	@Override
	public boolean hasVariables() {
		return !getVis().isEmpty();
	}

	@Override
	public boolean hasVariablesLocal() {
		return hasVariables();
	}

	@Override
	public boolean hasVariable(String variableName) {
		return getVis().containsKey(variableName);
	}

	@Override
	public boolean hasVariableLocal(String variableName) {
		return hasVariable(variableName);
	}

	@Override
	public void createVariableLocal(String variableName, Object value) {
		setVariable(variableName, value);
	}

	@Override
	public void removeVariable(String variableName) {
		Map<String,VariableInstance> vis = getVis();
		vis.remove(variableName);
		setVis(vis);
	}

	@Override
	public void removeVariableLocal(String variableName) {
		removeVariable(variableName);
	}

	@Override
	public void removeVariables(Collection<String> variableNames) {
		Map<String,VariableInstance> vis = getVis();
		for(String k:variableNames){
			vis.remove(k);
		}
		setVis(vis);
	}

	@Override
	public void removeVariablesLocal(Collection<String> variableNames) {
		removeVariables(variableNames);
	}

	@Override
	public void removeVariables() {
		Map<String,VariableInstance> vis = getVis();
		vis.clear();
		setVis(vis);
	}

	@Override
	public void removeVariablesLocal() {
		removeVariables();
	}
	
	public Bindings getBindingsAdapter(){
		return new Bindings(){

			@Override
			public int size() {
				return getVis().size();
			}

			@Override
			public boolean isEmpty() {
				return getVis().isEmpty();
			}

			@Override
			public boolean containsValue(Object value) {
				return getVis().containsValue(value);
			}

			@Override
			public void clear() {
				removeVariables();
			}

			@Override
			public Set<String> keySet() {
				return getVis().keySet();
			}

			@Override
			public Collection<Object> values() {
				return getVariables().values();
			}

			@Override
			public Set<java.util.Map.Entry<String, Object>> entrySet() {
				return getVariables().entrySet();
			}

			@Override
			public Object put(String name, Object value) {
				Object oldValue = getVariable(name);
				setVariable(name, value);
				return oldValue;
			}

			@Override
			public void putAll(Map<? extends String, ? extends Object> toMerge) {
				for(String k:toMerge.keySet()){
					setVariable(k,toMerge.get(k));
				}
			}

			@Override
			public boolean containsKey(Object key) {
				if(key.equals("execution")){
					return true;
				} else if(beans.containsKey(key)){
					return true;
				}
				return getVis().containsKey(key);
			}

			@Override
			public Object get(Object key) {
				if(key.equals("execution")){
					return VariableScopeBase.this;
				} else if(beans.containsKey(key)){
					return beans.get(key);
				}
				VariableInstance vi = getVis().get(key);
				return vi != null ? vi.getValue() : null;
			}

			@Override
			public Object remove(Object key) {
				Map<String,VariableInstance> vis = getVis();
				Object retval = getVis().remove(key);
				setVis(vis);
				return retval;
			}
			
			class Beans{
				public boolean containsKey(Object key) {
					return ((ProcessEngineConfigurationImpl)ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration()).getBeans().containsKey(key);
				}

				public Object get(Object key) {
					return ((ProcessEngineConfigurationImpl)ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration()).getBeans().get(key);
				}
			}
			
			private Beans beans = new Beans();
		};
	}
	
	protected abstract Map<String,VariableInstance> getVis();

	protected abstract void setVis(Map<String,VariableInstance> vis);

	public class PreProcessInstanceVariableInstance implements VariableInstance{

		String name;
		Object value;
		private String typeName;
		
		public PreProcessInstanceVariableInstance(String name,Object value){
			this.name = name;
			this.value = value;
		}
		
		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getTextValue() {
			return value != null ? value.toString() : null;
		}

		@Override
		public void setTextValue(String textValue) {
			value = textValue;
		}

		@Override
		public String getTextValue2() {
			throw new RuntimeException("Unimplemented method getTextValue2");
		}

		@Override
		public void setTextValue2(String textValue2) {
			throw new RuntimeException("Unimplemented method setTextValue2");			
		}

		@Override
		public Long getLongValue() {
			return value instanceof Long ? ((Long)value) : null;
		}

		@Override
		public void setLongValue(Long longValue) {
			value = longValue;
		}

		@Override
		public Double getDoubleValue() {
			return value instanceof Double ? ((Double)value) : null;
		}

		@Override
		public void setDoubleValue(Double doubleValue) {
			value = doubleValue;
		}

		@Override
		public byte[] getBytes() {
			throw new RuntimeException("Unimplemented method getBytes");			
		}

		@Override
		public void setBytes(byte[] bytes) {
			throw new RuntimeException("Unimplemented method setBytes");			
		}

		@Override
		public String getByteArrayValueId() {
			throw new RuntimeException("Unimplemented method getByteArrayValueId");			
		}

		@Override
		public ByteArrayEntity getByteArrayValue() {
			throw new RuntimeException("Unimplemented method getByteArrayValue");			
		}

		@Override
		public void setByteArrayValue(byte[] bytes) {
			throw new RuntimeException("Unimplemented method setByteArrayValue");			
		}

		@Override
		public Object getCachedValue() {
			return value;
		}

		@Override
		public void setCachedValue(Object cachedValue) {
			this.value = cachedValue;
		}

		@Override
		public String getId() {
			return null;
		}

		@Override
		public void setId(String id) {
		}

		@Override
		public Object getPersistentState() {
			return value;
		}

		@Override
		public void setRevision(int revision) {
			
		}

		@Override
		public int getRevision() {
			return 0;
		}

		@Override
		public int getRevisionNext() {
			return 0;
		}

		@Override
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String getLocalizedName() {
			return name;
		}

		@Override
		public void setLocalizedName(String name) {
			this.name = name;
		}

		@Override
		public String getLocalizedDescription() {
			return "";
		}

		@Override
		public void setLocalizedDescription(String description) {
		}

		@Override
		public void setProcessInstanceId(String processInstanceId) {
		}

		@Override
		public void setExecutionId(String executionId) {
		}

		@Override
		public Object getValue() {
			return value;
		}

		@Override
		public void setValue(Object value) {
			this.value = value;
		}

		@Override
		public String getTypeName() {
			return typeName;
		}

		@Override
		public void setTypeName(String typeName) {
			this.typeName = typeName;
		}

		@Override
		public String getProcessInstanceId() {
			return null;
		}

		@Override
		public String getTaskId() {
			return null;
		}

		@Override
		public void setTaskId(String taskId) {
		}

		@Override
		public String getExecutionId() {
			return null;
		}
		
	}
}
