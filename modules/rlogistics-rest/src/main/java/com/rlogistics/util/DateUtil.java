package com.rlogistics.util;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.Logger;

public class DateUtil {

	public static final String MMDDYYYYFormat = "MM/dd/yyyy";
	public static final String DDMMYYYYFormat = "dd/MM/yyyy";
	public static final String YYYYMMDDFormat = "yyyy/MM/dd";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd";

	public static final String DATE_SEPARATOR = "/";
	public static String curDate;
	
	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	// Number of Milliseconds in a day is 1000 * 60 * 60 * 24 = 86400000
	public static final long MILLSECONDS_IN_A_DAY = 86400000;
	private static final char digits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	private static Calendar staticCal = null;

	private static Logger log = Logger.getLogger(DateUtil.class.getName());

	/**
	 * Returns true if date1 is after date2. Ignores time component.
	 *
	 * @return boolean
	 * @param date1
	 *            java.util.Date
	 * @param date2
	 *            java.util.Date
	 */
	public static boolean isAfterYYYYMMDD(Date date1, Date date2) {

		return (date1.getTime() > date2.getTime());

	}
	
	/**Return Current Date and Time in yyyy/MM/dd HH:mm:ss Format as a String.
	 * 
	 */
	public static String currentDataTime(){
		Date date = new Date();
        System.out.println(sdf.format(date));
        String dateTime = (sdf.format(date)).toString();
        return dateTime;
	}
	
	/**
	 * Take 13 digit TimeStamp format and change that one into Date in this format(Thu Jan 04 04:53:27 UTC 2018).
	 * return String
	 * */
	public static Date timestampToDate(String timestamp){
		String s = timestamp;
//		BigInteger b = new BigInteger(s);
//      Long l1 = b.longValue();
		Long l = Long.parseLong(s);
        Date d = new Date(l);
//        String strDate = d.toString();
        return d;
	}
	

	/**
	 * Returns true if the two dates are equal. Ignores time component.
	 * 
	 * @return boolean
	 * @param date1
	 *            java.util.Date
	 * @param date2
	 *            java.util.Date
	 */
	public static boolean isEqualYYYYMMDD(Date date1, Date date2) {

		return (date1.getTime() == date2.getTime());

	}

	/**
	 * Returns no of Months differnce between startDate and and endDate.
	 * 
	 * @return int
	 * @param _startData
	 *            java.util.Date
	 * @param _endDate
	 *            java.util.Date
	 */
	public static int getMonthDiff(Date _startDate, Date _endDate) {

		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(_startDate);

		Calendar calendar2 = (Calendar) calendar1.clone();
		calendar2.setTime(_endDate);
		int months = ((calendar2.get(Calendar.YEAR) - calendar1.get(Calendar.YEAR)) * 12);
		return Math.abs(months + calendar2.get(Calendar.MONTH) - calendar1.get(Calendar.MONTH));
	}

	/**
	 * Returns no of Days differnce between startDate and and endDate.
	 *
	 * @return int
	 * @param _startData
	 *            java.util.Date
	 * @param _endDate
	 *            java.util.Date
	 */
	public static int getDayDiff(Date _startDate, Date _endDate) {

		long difference = Math.abs(_endDate.getTime() - _startDate.getTime());
		return (int) (difference / MILLSECONDS_IN_A_DAY);
	}

	/**
	 * Returns Current Date in mm/dd/yyyy format
	 * 
	 * @return String
	 * @param None
	 */

	public static String getCurrentDate() {

		Calendar calendar = Calendar.getInstance();
		// Date now = new Date();
		StringBuffer dateSB = new StringBuffer(10);
		// calendar.setTime(now);

		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DATE);

		dateSB.append(digits[month / 10]);
		dateSB.append(digits[month % 10]);

		dateSB.append("/");

		dateSB.append(digits[day / 10]);
		dateSB.append(digits[day % 10]);

		dateSB.append("/");
		dateSB.append("2");
		dateSB.append(digits[(year % 1000) / 100]);
		dateSB.append(digits[(year % 100) / 10]);
		dateSB.append(digits[year % 10]);
		curDate = dateSB.toString();
		if (curDate == null || curDate.length() <= 0) {
			curDate = dateSB.toString();
		}
		return dateSB.toString();

	}

	public static Date getCurrentDateAsDate() {

		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DATE);
		calendar.set(year, month, day);
		return calendar.getTime();
	}

	/**
	 * Returns Current Date in the requested format format
	 * 
	 * @return String
	 * @param String
	 *            dateFormat
	 */

	public static String getCurrentDate(String dateFormat) {

		if (dateFormat.equalsIgnoreCase(MMDDYYYYFormat))
			return getCurrentDate();

		Calendar calendar = Calendar.getInstance();
		Date now = new Date();
		StringBuffer dateSB = new StringBuffer(10);
		calendar.setTime(now);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DATE);

		if (dateFormat.equalsIgnoreCase(DDMMYYYYFormat)) {

			dateSB.append(digits[day / 10]);
			dateSB.append(digits[day % 10]);
			dateSB.append("/");
			dateSB.append(digits[month / 10]);
			dateSB.append(digits[month % 10]);
			dateSB.append("/");
			dateSB.append("2");
			dateSB.append(digits[(year % 1000) / 100]);
			dateSB.append(digits[(year % 100) / 10]);
			dateSB.append(digits[year % 10]);
			return dateSB.toString();
		} else if (dateFormat.equalsIgnoreCase(YYYYMMDDFormat)) {

			dateSB.append("2");
			dateSB.append(digits[(year % 1000) / 100]);
			dateSB.append(digits[(year % 100) / 10]);
			dateSB.append(digits[year % 10]);
			dateSB.append("/");
			dateSB.append(digits[month / 10]);
			dateSB.append(digits[month % 10]);
			dateSB.append("/");
			dateSB.append(digits[day / 10]);
			dateSB.append(digits[day % 10]);
			return dateSB.toString();
		} else {
			Date currDate = calendar.getTime();
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			return formatter.format(currDate);
		}
	}

	/**
	 * Returns Date after adding noOfYears from givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfYear
	 *            int
	 */
	public static Date addYear(Date _date, int _noOfYears) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.YEAR, _noOfYears);
		return calendar.getTime();
	}

	/**
	 * Returns Date after subtracting noOfYears from givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfYear
	 *            int
	 */
	public static Date subtractYear(Date _date, int _noOfYears) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.YEAR, -_noOfYears);
		return calendar.getTime();
	}

	/**
	 * Returns Date after adding noOfDays to givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfDays
	 *            int
	 */
	public static Date addDay(Date _date, int _noOfDays) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.DATE, _noOfDays);
		return calendar.getTime();
	}

	/**
	 * Returns Date after subtracting noOfDays from givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfDays
	 *            int
	 */
	public static Date subtractDay(Date _date, int _noOfDays) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.DATE, -_noOfDays);
		return calendar.getTime();
	}

	/**
	 * Returns Date after adding noOfMonths to givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfDays
	 *            int
	 */
	public static Date addMonth(Date _date, int _noOfMonths) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.MONTH, _noOfMonths);
		return calendar.getTime();
	}

	/**
	 * Returns Date after incrementing month by 1 *
	 * 
	 * @return addedDt_String
	 * @param dt_String
	 * @param _noOfMonths
	 *            int
	 */
	public static String addMonth(String _date) throws ParseException {
		SimpleDateFormat dtformat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar dtCalendar = dtformat.getCalendar();
		dtCalendar.setTime(dtformat.parse(_date));
		dtCalendar.add(Calendar.MONTH, 1);
		return dtformat.format(dtCalendar.getTime());

	}

	/**
	 * Returns Date after subtracting noOfMonths from givenDate
	 * 
	 * @return java.util.Date
	 * @param _date
	 *            java.util.Date
	 * @param _noOfDays
	 *            int
	 */
	public static Date subtractMonth(Date _date, int _noOfMonths) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);
		calendar.add(Calendar.MONTH, -_noOfMonths);
		return calendar.getTime();
	}

	/**
	 * Returns true if year contains 366 days.
	 * 
	 * @return boolean
	 * @param year
	 *            int
	 */
	public static boolean isLeapYear(int year) {
		if ((year % 400) == 0)
			return true;

		if ((year % 4) == 0) {
			if ((year % 100) == 0)
				return false;
			else
				return true;
		} else
			return false;

	}

	/**
	 * Returns a string that is date formatted according to the specified format
	 * 
	 * @param String(month)
	 * @param String(day)
	 * @param String(year)
	 * @param String(custom
	 *            format ex:"yyyy/dd/mm")
	 * @return String
	 */

	public static String formatDate(String month, String day, String year, String dateFormat) throws ParseException {

		SimpleDateFormat sdfOutput = new SimpleDateFormat(dateFormat);
		return sdfOutput.format(getDate(month, day, year));
	}

	/**
	 * Returns a string that is date formatted ( uses the default date format
	 * "mm/dd/yyyy"
	 * 
	 * @param String(month)
	 * @param String(day)
	 * @param String(year)
	 * @return String
	 */

	public static String formatDate(String month, String day, String year) throws ParseException {

		return formatDate(month, day, year, MMDDYYYYFormat);
		
	}

	/**
	 * Returns a Date object given the string representation
	 * 
	 * @param String(date
	 *            String)
	 * @param String(date
	 *            format)
	 * @return Date ex: ("mm/dd/yyyy")
	 */

	public static Date toDate(String dateStr, String dateFormat) throws ParseException {
		// date format is chosen by the user - ex: "mm/dd/yyyy"

		if (dateFormat.equalsIgnoreCase(MMDDYYYYFormat))
			return toDate(dateStr);

		else if (dateFormat.equalsIgnoreCase(DDMMYYYYFormat)) {

			int year = parseYear(dateStr, dateFormat);
			int month = parseMonth(dateStr, dateFormat);
			int day = parseDay(dateStr, dateFormat);

			/*
			 * Calendar calendar = Calendar.getInstance(); calendar.set(year, month - 1,
			 * day); return calendar.getTime();
			 */
			if (staticCal == null)
				makeStaticCalendar();

			synchronized (staticCal) {
				staticCal.clear();

				staticCal.set(year, month - 1, day);

				return staticCal.getTime();
			}

		} else {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			return sdf.parse(dateStr);
		}
	}

	/**
	 * Returns a Date object given the string representation (used the default date
	 * format "mm/dd/yyyy")
	 * 
	 * @param String(date
	 *            String)
	 * @return Date ex: ("MM/dd/yyyy")
	 */

	public static Date toDate(String dateStr) throws ParseException {

		int year = parseYear(dateStr);
		int month = parseMonth(dateStr);
		int day = parseDay(dateStr);

		// Calendar calendar =
		// Calendar.getInstance();

		if (staticCal == null)
			makeStaticCalendar();

		// calendar.set(year, month - 1, day);

		synchronized (staticCal) {
			staticCal.clear();

			staticCal.set(year, month - 1, day);

			// return calendar.getTime();
			return staticCal.getTime();
		}
	}

	/**
	 * Returns a static calendar object
	 * 
	 * @retun void
	 */
	private synchronized static void makeStaticCalendar() {
		// //System.out.println("makeStaticCalendar is being called..");
		if (staticCal == null) {
			// //System.out.println("creating the calendar object.");
			staticCal = Calendar.getInstance();
		}

	}

	/**
	 * Returns an int for the month given the string representation (used the
	 * default date format "mm/dd/yyyy")
	 * 
	 * @param String(date
	 *            String)
	 * @return Date ex: ("MM/dd/yyyy")
	 */

	private static int parseMonth(String dateStr) throws NumberFormatException {
		int month = 0;

		if (dateStr.length() == 10 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
			// We have "MM/dd/yyyy"
			month = Integer.parseInt(dateStr.substring(0, 2));
		} else if (dateStr.length() == 9 && dateStr.indexOf(DATE_SEPARATOR) == 1) {
			// We have "M/dd/yyyy"
			month = Integer.parseInt(dateStr.substring(0, 1));
		} else if (dateStr.length() == 9 && dateStr.lastIndexOf(DATE_SEPARATOR) == 4) {
			// We have "MM/d/yyyy"
			month = Integer.parseInt(dateStr.substring(0, 2));
		} else if (dateStr.length() == 8 && dateStr.lastIndexOf(DATE_SEPARATOR) == 3) {
			// We have "M/d/yyyy"
			month = Integer.parseInt(dateStr.substring(0, 1));
		} else if (dateStr.length() == 10 && dateStr.indexOf("-") == 4) {
			// We have "yyyy-mm-dd"
			month = Integer.parseInt(dateStr.substring(5, 7));
		}

		if (month <= 0)
			throw new NumberFormatException();

		return month;
	}

	/**
	 * Returns an int for the Month given the string representation
	 * 
	 * @param String(date
	 *            String)
	 * 
	 * @return int
	 */

	private static int parseMonth(String dateStr, String dateFormat) throws NumberFormatException {

		int month = 0;

		if (dateFormat.equalsIgnoreCase(MMDDYYYYFormat))
			return parseMonth(dateStr);

		else if (dateFormat.equalsIgnoreCase(DDMMYYYYFormat)) {
			if (dateStr.length() == 10 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
				// We have "dd/MM/yyyy"
				month = Integer.parseInt(dateStr.substring(3, 5));
			} else if (dateStr.length() == 9 && dateStr.indexOf(DATE_SEPARATOR) == 1) {
				// We have "d/MM/yyyy"
				month = Integer.parseInt(dateStr.substring(2, 4));
			} else if (dateStr.length() == 9 && dateStr.lastIndexOf(DATE_SEPARATOR) == 4) {
				// We have "dd/M/yyyy"
				month = Integer.parseInt(dateStr.substring(3, 4));
			} else if (dateStr.length() == 8 && dateStr.lastIndexOf(DATE_SEPARATOR) == 3) {
				// We have "d/M/yyyy"
				month = Integer.parseInt(dateStr.substring(2, 3));
			}
		}
		if (month <= 0)
			throw new NumberFormatException();

		return month;
	}

	/**
	 * Returns an int for the Day given the string representation (used the default
	 * date format "mm/dd/yyyy")
	 * 
	 * @param String(String
	 *            date)
	 * @return int Day
	 */

	private static int parseDay(String dateStr) throws NumberFormatException {

		int day = 0;

		if (dateStr.length() == 10 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
			// We have "MM/dd/yyyy"
			day = Integer.parseInt(dateStr.substring(3, 5));
		} else if (dateStr.length() == 9 && dateStr.indexOf(DATE_SEPARATOR) == 1) {
			// We have "M/dd/yyyy"
			day = Integer.parseInt(dateStr.substring(2, 4));
		} else if (dateStr.length() == 9 && dateStr.lastIndexOf(DATE_SEPARATOR) == 4) {
			// We have "MM/d/yyyy"
			day = Integer.parseInt(dateStr.substring(3, 4));
		} else if (dateStr.length() == 8 && dateStr.lastIndexOf(DATE_SEPARATOR) == 3) {
			// We have "M/d/yyyy"
			day = Integer.parseInt(dateStr.substring(2, 3));
		} else if (dateStr.length() == 10 && dateStr.indexOf("-") == 4) {
			// We have "yyyy/mm/dd"
			day = Integer.parseInt(dateStr.substring(8, 10));
		}

		if (day <= 0)
			throw new NumberFormatException();

		return day;
	}

	/**
	 * Returns an int for the Day given the string representation
	 * 
	 * @param String(date
	 *            String)
	 * 
	 * @return Date ex: ("MM/dd/yyyy")
	 */

	private static int parseDay(String dateStr, String dateFormat) throws NumberFormatException {

		int day = 0;

		if (dateFormat.equalsIgnoreCase(MMDDYYYYFormat))
			return parseDay(dateStr);

		else if (dateFormat.equalsIgnoreCase(DDMMYYYYFormat)) {

			if (dateStr.length() == 10 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
				// We have "dd/MM/yyyy"
				day = Integer.parseInt(dateStr.substring(0, 2));
			} else if (dateStr.length() == 9 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
				// We have "dd/M/yyyy"
				day = Integer.parseInt(dateStr.substring(0, 2));
			} else if (dateStr.length() == 9 && dateStr.lastIndexOf(DATE_SEPARATOR) == 4) {
				// We have "d/MM/yyyy"
				day = Integer.parseInt(dateStr.substring(0, 1));
			} else if (dateStr.length() == 8 && dateStr.lastIndexOf(DATE_SEPARATOR) == 3) {
				// We have "d/M/yyyy"
				day = Integer.parseInt(dateStr.substring(0, 1));
			}
		}

		if (day <= 0)
			throw new NumberFormatException();

		return day;
	}

	/**
	 * Returns an int for the Year given the string representation (used the default
	 * date format "mm/dd/yyyy")
	 * 
	 * @param String(date
	 *            String)
	 * @return Date ex: ("MM/dd/yyyy")
	 */

	private static int parseYear(String dateStr) throws NumberFormatException {
		int year = 0;

		if (dateStr.length() == 10 && dateStr.indexOf(DATE_SEPARATOR) == 2) {
			// We have "MM/dd/yyyy"
			year = Integer.parseInt(dateStr.substring(6));
		} else if (dateStr.length() == 9 && dateStr.indexOf(DATE_SEPARATOR) == 1) {
			// We have "M/dd/yyyy"
			year = Integer.parseInt(dateStr.substring(5));
		} else if (dateStr.length() == 9 && dateStr.lastIndexOf(DATE_SEPARATOR) == 4) {
			// We have "MM/d/yyyy"
			year = Integer.parseInt(dateStr.substring(5));
		} else if (dateStr.length() == 8 && dateStr.lastIndexOf(DATE_SEPARATOR) == 3) {
			// We have "M/d/yyyy"
			year = Integer.parseInt(dateStr.substring(4));
		} else if (dateStr.length() == 10 && dateStr.indexOf("-") == 4) {
			// We have "yyyy/mm/dd"
			year = Integer.parseInt(dateStr.substring(0, 3));
		}

		if (year <= 0)
			throw new NumberFormatException();

		return year;
	}

	/**
	 * Returns an int for the Year given the string representation
	 * 
	 * @param String(date
	 *            String)
	 * 
	 * @return int
	 */

	private static int parseYear(String dateStr, String dateFormat) throws NumberFormatException {

		int year = 0;

		if (dateFormat.equalsIgnoreCase(MMDDYYYYFormat))
			return parseYear(dateStr);

		else if (dateFormat.equalsIgnoreCase(DDMMYYYYFormat))
			return parseYear(dateStr); // it is the same as MMDDYY format

		if (year <= 0)
			throw new NumberFormatException();

		return year;
	}

	/**
	 * Returns the day of the date String given the date string and the format of
	 * the same
	 * 
	 * @param String(date
	 *            String - "mm/dd/yyyy")
	 * @return int
	 */

	public static int getDate(String dateStr) throws ParseException {

		return parseDay(dateStr);
	}

	/**
	 * Returns the month of the date String given the date string and the format of
	 * the same
	 * 
	 * @param String(date
	 *            String - "mm/dd/yyyy")
	 * @return int
	 */

	public static int getMonth(String dateStr) throws ParseException {
		return parseMonth(dateStr);
	}

	/**
	 * Returns the year of the date String given the date string and the format of
	 * the same
	 * 
	 * @param String(date
	 *            String - "mm/dd/yyyy")
	 * @return int
	 */

	public static int getYear(String dateStr) throws ParseException {

		return parseYear(dateStr, MMDDYYYYFormat);
	}

	/**
	 * Validates short date (i.e. "MM/dd/yyyy") by parsing <i>dateValue</i> with
	 * <i>delimiter</i>, and validating each component of date (i.e. month, day,
	 * year). <br>
	 * If date string represents valid date then "true" is returned <br>
	 * <b>NOTE:</b> A valid month is from 1 to 12, a valid day is from 1 to <# of
	 * days in month, a valid year is 1 or greater, and 4 digits long.
	 * 
	 * @return boolean
	 * @param dateValue
	 *            java.lang.String
	 * @param delimiter
	 *            java.lang.String
	 */
	public static boolean isValidDate(String dateValue, String delimiter) {
		int[] DAYS_IN_MONTH = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (dateValue == null || delimiter == null)
			return false;

		int startIndex = 0, endIndex = dateValue.indexOf(delimiter), month = -1, day = -1, year = -1;

		if (endIndex <= startIndex)
			return false;
		else {
			try {
				// parse month
				month = Integer.parseInt(dateValue.substring(startIndex, endIndex));

				// parse day
				startIndex = endIndex + 1;
				endIndex = dateValue.indexOf(delimiter, startIndex);

				if (endIndex <= startIndex)
					return false;

				day = Integer.parseInt(dateValue.substring(startIndex, endIndex));

				// parse year
				startIndex = endIndex + 1;
				String yearValue = dateValue.substring(startIndex);
				if (yearValue.length() != 4)
					return false;
				year = trimStringToInt(yearValue, -1);
				// Check if the date starts with '0'
				if (year < 1000)
					return false;
			} catch (Throwable t) {
				return false;
			}

			// date has been parsed, validate parsed values
			if (month >= 1 && month <= 12 && day >= 1
					&& (day <= DAYS_IN_MONTH[(month - 1)] || (isLeapYear(year) && day <= 29)) && year >= 1) {

				if (month == 2 && isLeapYear(year)) {
					day--;
					if (day > DAYS_IN_MONTH[1])
						return false;
				}
				return true;
			} else
				return false;
		}
	}

	/**
	 * This returns the years difference between startDate and endDate. This method
	 * assumes that dates are in "mm/dd/yyyy" format and valid.
	 * 
	 * @param startDate
	 *            date string in "mm/dd/yyyy" format.
	 * @param endDate
	 *            date string in "mm/dd/yyyy" format.
	 * @return no of years difference.
	 */
	public static int getYearsDifference(String startDate, String endDate) throws ParseException {
		String date1 = startDate;
		String date2 = endDate;
		int year1 = getYear(date1);
		int year2 = getYear(date2);
		int yearDiff = year2 - year1;

		// !Krishna Tummala:If startDate is after endDate swap dates.
		if (yearDiff < 0) {
			yearDiff = Math.abs(yearDiff);
			date1 = endDate;
			date2 = startDate;
		}

		int month1 = getMonth(date1);
		int month2 = getMonth(date2);

		if (month2 > month1) {
			return yearDiff;
		}

		if (month2 < month1) {
			return --yearDiff;
		}

		int day1 = getDate(date1);
		int day2 = getDate(date2);
		if (day2 >= day1) {
			return yearDiff;
		}

		// If FEBRUARY then go for other checks else return false.
		if (month1 != 2) {
			return --yearDiff;
		}

		if (day2 == 28) {
			return yearDiff;
		}

		return --yearDiff;
	}

	/**
	 * This rolls the given date by given no of years. This method assumes that
	 * dates are in "mm/dd/yyyy" format and valid.
	 * 
	 * @param date
	 *            date string in "mm/dd/yyyy" format.
	 * @param noOfYears
	 *            date to be rolled by.
	 * @return new date after rolling.
	 */
	public static String rollYearsBy(String date, int noOfYears) throws ParseException {
		int year = getYear(date);
		int month = getMonth(date);
		int day = getDate(date);
		year += noOfYears;
		if ((month == 2) && (day == 29) && !DateUtil.isLeapYear(year)) {
			day = 28;
		}
		return month + DATE_SEPARATOR + day + DATE_SEPARATOR + year;
	}

	/**
	 * Returns the date of the date String given the date string and the format of
	 * the same
	 * 
	 * @param String(date
	 *            String - "mm/dd/yyyy")
	 * @return Calendar
	 */
	public static Calendar getCalendar(String dateStr) throws ParseException {

		int year = parseYear(dateStr);
		int month = parseMonth(dateStr);
		int day = parseDay(dateStr);

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, day);
		return calendar;

	}

	/**
	 * Returns the date of the date String given the date string and the format of
	 * the same
	 * 
	 * @param String(date
	 *            String - "mm/dd/yyyy")
	 * @return Date
	 */
	public static Date getDate(String month, String day, String year) throws ParseException {

		if (month == null || month.length() <= 0)
			throw new ParseException("Invalid value for Month", 1);

		if (day == null || day.length() <= 0)
			throw new ParseException("Invalid value for Day", 1);

		if (year == null || year.length() <= 0)
			throw new ParseException("Invalid value for Year", 1);

		StringBuffer dateStr = new StringBuffer();
		dateStr.append(month);
		dateStr.append("/");
		dateStr.append(day);
		dateStr.append("/");
		dateStr.append(year);

		return toDate(dateStr.toString());

	}

	/**
	 * Returns a string that is date formatted
	 * 
	 * @param Date
	 * @return String
	 */
	public static String formatDate(Date date) throws ParseException {

		SimpleDateFormat sdf_MMDDYYYY = new SimpleDateFormat(MMDDYYYYFormat);
		return sdf_MMDDYYYY.format(date);
	}

	/**
	 * Returns a Timestamp object given the string representation (used the default
	 * date format "yyyy-mm-dd-hh.mm.ss.ffffff")
	 * 
	 * @param String
	 *            (timeStmp String)
	 * @return Timestamp
	 */

	public static Timestamp toTimestamp(String timeStmp) {

		if (timeStmp == null || timeStmp.length() < 26) {
			return null;
		}
		int index = timeStmp.lastIndexOf("-");
		String tmpStmpDt = timeStmp.substring(0, index);
		String timeStmpTime = timeStmp.substring(index + 1, timeStmp.length());
		StringTokenizer token = new StringTokenizer(timeStmpTime, ".");
		StringBuffer buffer = new StringBuffer();
		buffer.append(tmpStmpDt).append(" ");
		int i = 0;
		while (token.hasMoreTokens()) {
			i++;
			if (i < 3) {
				buffer.append(token.nextToken()).append(":");
			} else if (i == 3) {
				buffer.append(token.nextToken()).append(".");
			} else {
				buffer.append(token.nextToken()).append("000");
			}
		}
		return Timestamp.valueOf(buffer.toString());
	}

	public static int noOfWorkingDays(Calendar fromDate, Calendar toDate) {

		int workingDays = 0;
		while (fromDate.before(toDate)) {
			int day = fromDate.get(Calendar.DAY_OF_WEEK);
			if ((day != Calendar.SATURDAY) && (day != Calendar.SUNDAY))
				workingDays++;
			fromDate.add(Calendar.DATE, 1);
		}
		return workingDays;

	}

	public static boolean isWeekend() {

		String currentDate = DateUtil.getCurrentDate();
		int currentDD = Integer.parseInt(currentDate.substring(3, 5));
		int currentMM = Integer.parseInt(currentDate.substring(0, 2));
		int currentYYYY = Integer.parseInt(currentDate.substring(6));

		Calendar currentDateCal = Calendar.getInstance();
		currentDateCal.set(currentYYYY, currentMM - 1, currentDD);

		int day = currentDateCal.get(Calendar.DAY_OF_WEEK);
		if ((day == Calendar.SATURDAY) || (day == Calendar.SUNDAY))
			return true;

		return false;

	}

	public static String getCurrentDateAsMMDDYYYY() {
		String date = null;
		try {
			java.util.Date date1 = new java.util.Date();
			String DATE_FORMAT = "MM/dd/yyyy";
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			date = sdf.format(date1);

		} catch (Exception e) {
		}
		return date;
	}

	public static String getCurrentDateAsYYYYMMDD() {
		String date = null;
		try {
			java.util.Date date1 = new java.util.Date();
			String DATE_FORMAT = "yyyy-MM-dd";
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			date = sdf.format(date1);

		} catch (Exception e) {
		}
		return date;
	}

	/**
	 * @param date
	 * @return
	 */
	public static boolean isStringValidDateOrNot(String date) {
		try {
			DateFormat format = new SimpleDateFormat("mm/dd/yyyy");

			format.setLenient(false); // this is important!
			format.parse(date);
		} catch (ParseException e) {
			return false;
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int dateToInt(String date1) throws ParseException {
		int idate = 0;
		if (date1 != null) {
			SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
			java.util.Date originalDate = DateUtil.toDate(date1, "MM/dd/yyyy");
			String dat3 = sd.format(originalDate);
			idate = Integer.parseInt(dat3);
		}
		return idate;

	}

	public static String ltrim(String source) {
		return source.replaceAll("^\\s+", "");
	}

	/**
	 * Remove trailing whitespace
	 */
	public static String rtrim(String source) {
		return source.replaceAll("\\s+$", "");
	}

	/**
	 * Remove leading or trailing whilespaces
	 * 
	 * @param source
	 * @return
	 */
	public static String removeLeadingOrTrailingWhitespace(String source) {
		if (source == null)
			return null;
		String ltrim = ltrim(source);
		return rtrim(ltrim);
	}

	public static boolean isAfterOrEqualYYYYMMDD(Date date1, Date date2) {
		return date1.getTime() >= date2.getTime();
	}

	/**
	 * Converts the passed date to String in yyyy-MM-dd format
	 * 
	 * @param aDate
	 * @return
	 */
	public static Date convStringToDBDate(String Date) {
		Date retDate = null;
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DB_DATE_FORMAT);
		try {
			retDate = sdf.parse(Date);
		} catch (java.lang.Exception e) {
			return null;
		}
		return retDate;
	}

	/**
	 * Removes leading '0' from String value and returns parsed int. If the string
	 * is not parseable, the specified default value is returned.
	 *
	 * @return int
	 * @param value
	 *            java.lang.String
	 * @param defaultValue
	 *            int
	 */
	public static int trimStringToInt(String value, int defaultValue) {
		int returnValue = defaultValue;
		int numTrim = 0, len = 0;

		try {
			value = value.trim();
			len = value.length();

			for (int i = 0; i < len; i++) {
				if (value.charAt(i) == '0') {
					numTrim++;
				} else
					break;
			}

			if (len == numTrim) {
				// value String is all zeroes
				returnValue = 0;
			} else {
				returnValue = Integer.parseInt(value.substring(numTrim));
			}
		} catch (Exception e) {
			returnValue = defaultValue;
		}

		return returnValue;
	}
	
	//Demo Purpose
	public static void main(String[] args) {
		Date date = getCurrentDateAsDate();
		System.out.println("date is : "+timestampToDate("1515494900345"));
//		System.out.println("Compare result : "+isAfterYYYYMMDD(date, date));
		
	}

}
