package com.rlogistics.util;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.VariableInstance;

import com.rlogistics.http.RLogisticsHttpSessionHolder;

public class PreProcessInstanceVariableScope extends VariableScopeBase {
	private static final String PPI_VARIABLE_INSTANCES = "xxPreProcessInstanceVariableInstances";

	protected Map<String,VariableInstance> getVis() {
		@SuppressWarnings("unchecked")
		Map<String,VariableInstance> vis = (Map<String, VariableInstance>) RLogisticsHttpSessionHolder.getHttpSession().getAttribute(PPI_VARIABLE_INSTANCES);
		
		if(vis == null){
			vis = new HashMap<String, VariableInstance>();
		}
		
		return vis;
	}

	protected void setVis(Map<String,VariableInstance> vis) {
		RLogisticsHttpSessionHolder.getHttpSession().setAttribute(PPI_VARIABLE_INSTANCES,vis);
	}
}
