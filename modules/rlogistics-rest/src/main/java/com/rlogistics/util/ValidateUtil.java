package com.rlogistics.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
//import java.util.logging.Logger;

import org.activiti.engine.ProcessEngines;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.MasterdataService;

import org.apache.log4j.Logger;

public class ValidateUtil {
	private static Logger log = Logger.getLogger(ValidateUtil.class.getName());

	public static boolean isStringYESorNO(String str) {

		boolean retVal = false;
		if ((str == null) || (str.trim().length() == 0))
			return retVal;
		if ((str.equalsIgnoreCase("Y")) || (str.equalsIgnoreCase("N")))
			retVal = true;
		return retVal;
	}

	public static boolean isAddressStringValid(String addrStr) {

		String addressStr = addrStr.toLowerCase();
		char charCheck;
		boolean res = false;

		final char[] validChars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
				'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '#', '&',
				'(', ')', ':', ';', ',', '.', '/', '-', ' ', '"', '\'' };

		for (int i = 0; i < addressStr.length(); i++) {
			charCheck = addressStr.charAt(i);
			res = false;
			for (int j = 0; j < validChars.length; j++) {
				if (charCheck == validChars[j]) {
					res = true;
				}
			}
			if (res == false) {
				return false;
			}
		}
		return true;
	}

	public static boolean isPhoneNumberValid(String phoneNum) {

		if ((phoneNum == null) || (phoneNum.trim().length() != 10))
			return false;
		try {
			Long.parseLong(phoneNum.trim());
		} catch (NumberFormatException nf) {
			return false;
		}
		return true;
	}

	public static boolean isZipCodeLengthValid(String zipCd) {

		if ((zipCd != null) && ((zipCd.length() == 5) || (zipCd.length() == 9)))
			return true;
		else
			return false;
	}

	public static boolean isFieldChanged(String activeValue, String existingValue) {
		if (activeValue == null) {
			return false;
		}

		if (activeValue.equals(existingValue)) {
			return false;
		}
		return true;
	}

	/**
	 * checks to see if date1 is greater than date2
	 */
	public static boolean isDate1GreaterThanDate2(String aDate1, String aDate2) {

		Date date1 = null;
		Date date2 = null;
		try {
			date1 = DateUtil.toDate(aDate1);
			date2 = DateUtil.toDate(aDate2);
		} catch (Exception pe) {
			return false;
		}
		if (DateUtil.isAfterYYYYMMDD(date1, date2)) {
			return true;
		}
		return false;
	}

	// checks to see if date1 is less than current date

	public static boolean isDate1LsCurrDate(String aDate1) {

		Date date1 = null;
		Calendar currentDate = Calendar.getInstance();
		Date currentDt = currentDate.getTime();
		try {
			date1 = DateUtil.toDate(aDate1);
		} catch (Exception pe) {
			return false;
		}
		if (!DateUtil.isAfterYYYYMMDD(date1, currentDt)) {
			return true;
		}
		return false;
	}

	

	
	/**
	 * Converts the passed date to String in MM/dd/yyyy format
	 */
	public static String convDateToString(Date aDate) {
		String retDate = null;
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM/dd/yyyy");

		try {

			retDate = sdf.format(aDate);

		} catch (java.lang.Exception e) {
			return null;
		}
		return retDate;
	}

	/**
	 * checks to see if date1 is equal to date2
	 */
	public static boolean isDate1EqualToDate2(String aDate1, String aDate2) {

		Date date1 = null;
		Date date2 = null;
		try {
			date1 = DateUtil.toDate(aDate1);
			date2 = DateUtil.toDate(aDate2);
		} catch (Exception pe) {
			return false;
		}
		if (DateUtil.isEqualYYYYMMDD(date1, date2)) {
			return true;
		}
		return false;
	}

	

	/**
	 * checks to see if date1 is less than date2
	 */
	public static boolean isDate1LessThanDate2(String aDate1, String aDate2) {

		Date date1 = null;
		Date date2 = null;
		try {
			date1 = DateUtil.toDate(aDate1);
			date2 = DateUtil.toDate(aDate2);
		} catch (Exception pe) {
			return false;
		}
		if (DateUtil.isAfterYYYYMMDD(date2, date1)) {
			return true;
		}
		return false;
	}

	
	/**
	 * checks to see if the eMail enterd is valid. 
	 * 
	 *
	 * @param Strign
	 *            emailId
	 * @return boolean true - if a valid value is enterd false - for invalid value
	 *         or empty or null string
	 */
	public static boolean isValidEmail(String anEmailId) {

		boolean VALID = false;
		if (!isFieldEmpty(anEmailId)) {
			for (int i = 0; i < anEmailId.length(); i++) {
				if (anEmailId.charAt(i) == '@') {
					VALID = true;
				}
			}

			
		}
		return VALID;
	}

	public static boolean isFieldEmpty(String field) {
		if ((field == null) || (field.trim().length() == 0) || (field.equals("")) || field.equals("null")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks to see if the passed date is Empty. Expects the passed date in
	 * mm/dd/yyyy format.
	 * 
	 * @param String
	 *            date
	 * @return boolean
	 */
	public static boolean isDateFieldEmpty(String aDate) {

		if (!isFieldEmpty(aDate)) {
			StringTokenizer st = new StringTokenizer(aDate, "/");
			StringBuffer result = new StringBuffer();
			while (st.hasMoreTokens()) {
				result.append(st.nextToken());
			}
			String ss = result.toString().trim();
			if (isFieldEmpty(ss)) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public static int convertDateToInt(String _date) {
		String mm = "00";
		String dd = "00";
		String yy = "0000";
		int index = 0;
		int prevIndex = 0;
		if (_date == null)
			return 0;
		String DELIMTER = "/";
		prevIndex = _date.indexOf(DELIMTER);
		mm = _date.substring(0, prevIndex);
		index = _date.indexOf(DELIMTER, prevIndex + 1);
		dd = _date.substring(prevIndex + 1, index);
		yy = _date.substring(index + 1);
		index = (((Integer.parseInt(yy) * 100) + Integer.parseInt(mm)) * 100) + Integer.parseInt(dd);
		return index;

	}

	/**
	 * This returns the years difference between startDate and endDate. This method
	 * assumes that dates are in "mm/dd/yyyy" format and valid.
	 *
	 * Note: Renovation year field has only year, so we pass it as a string in
	 * mm/dd/yyyy format with month and day defaulted as 01.
	 *
	 * @param renovYr
	 *            date string in "mm/dd/yyyy" format.
	 * @param currDate
	 *            date string in "mm/dd/yyyy" format.
	 * @return no of years difference.
	 */
	public static int getYearDiff(String renovYr, String currDate) throws ParseException {
		String date1 = DateUtil.formatDate("01", "01", renovYr);
		String date2 = currDate;

		int year1 = DateUtil.getYear(date1);
		int year2 = DateUtil.getYear(date2);
		int yearDiff = year2 - year1;

		if (yearDiff < 0) {
			yearDiff = Math.abs(yearDiff);
		}

		return yearDiff;

	}

	/**
	 * @param field
	 * @return
	 */
	public static boolean isNumericValue(String field) {
		boolean isNumeric = false;
		if (field != null && !field.trim().equals("") && field.trim().length() > 0) {
			try {
				Long n = new Long(field);
				isNumeric = true;
			} catch (NumberFormatException nfe) {
				isNumeric = false;
			}
		}
		return isNumeric;
	}

	/**
	 * @param amt
	 * @return
	 */
	public static String format(String amt) {
		Double amtValue = new Double(amt);
		NumberFormat nFormat = NumberFormat.getCurrencyInstance(Locale.US);
		return nFormat.format(amtValue.doubleValue());
	}

		
	// Check date diff. for Application..
	public static boolean checkDateDifferenceForAppln(String applnEffDt, int diff) {
		Date effectiveDate = null;
		Date currentDate = null;
		boolean chk = true;
		try {
			effectiveDate = DateUtil.toDate(applnEffDt, DateUtil.MMDDYYYYFormat);
			currentDate = DateUtil.getCurrentDateAsDate();

		} catch (Exception e) {

			chk = false;
		}
		int dayDiff = DateUtil.getDayDiff(currentDate, effectiveDate);
		if (dayDiff > diff) {
			chk = false;
		}
		return chk;

	}

	
	// Method to check if the date is a back date or future date
	public static boolean chkFutureDated(String applnEffDt) {
		boolean chkFutureDate = false;
		Date effectiveDate = null;
		Date currentDate = null;
		try {
			effectiveDate = DateUtil.toDate(applnEffDt, DateUtil.MMDDYYYYFormat);
			currentDate = DateUtil.getCurrentDateAsDate();
		} catch (Exception e) {
			chkFutureDate = false;
		}
		if (effectiveDate.after(currentDate)) {
			chkFutureDate = true;
		} else {
			chkFutureDate = false;
		}
		return chkFutureDate;
	}

	/* Upload - Dec7th - Converting mm-dd-yyyy to mm/dd/yyyyy */
	public static String formatStringDate(String impldDate) {
		String retImplDate = null;
		try {
			String day = impldDate.substring(impldDate.lastIndexOf("-") + 1, impldDate.length());
			String month = impldDate.substring(impldDate.indexOf("-") + 1, impldDate.lastIndexOf("-"));
			String year = impldDate.substring(0, impldDate.indexOf("-"));
			retImplDate = month + "/" + day + "/" + year;
		} catch (Exception e) {
			retImplDate = "";
		}
		return retImplDate;
	}

	public static int getYearDiffWithoutAbs(String modYr, String currDate) throws ParseException {
		String date1 = DateUtil.formatDate("01", "01", modYr);
		String date2 = currDate;

		int year1 = DateUtil.getYear(date1);
		int year2 = DateUtil.getYear(date2);
		int yearDiff = year2 - year1;

		return yearDiff;

	}

	/**
	 * Method validates whether field is not empty
	 * 
	 * @param field
	 * @return boolean
	 */
	public static boolean isFieldNotEmpty(String field) {
		if ((field == null) || (field.trim().length() == 0) || (field.equals("")) || field.equals("null")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Method validates whether object is not empty
	 * 
	 * @param newObj
	 * @return boolean
	 */
	public static boolean isObjectNotEmpty(Object newObj) {
		if (newObj == null) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isObjectEmpty(Object newObj) {
		if (newObj == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks for numeric validity.
	 * 
	 * @return boolean
	 * @param field
	 */
	public static boolean isBigNumeric(String field) {
		if (field == null)
			return true;

		// Check for numeric field
		try {
			Double n = new Double(field);
			return true;
		} catch (Exception nfe) {
			return false;
		}
	}

	/**
	 * checks for numeric validity.
	 * 
	 * @return boolean
	 * @param field
	 */
	public static boolean isNumeric(String field) {
		if (field == null)
			return true;

		// Check for numeric field
		try {
			Long n = new Long(field);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public static boolean isObjectArrayEmpty(Object[] array) {
		if (array == null || array.length == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static String convertToMixedCase(String stringtoConvert) {
		String finalNameString = "";
		if (stringtoConvert != null) {
			StringTokenizer tok1 = new StringTokenizer(stringtoConvert);
			while (tok1.hasMoreTokens()) {
				String theToken = tok1.nextToken();
				String token = theToken.substring(0, 1);
				String token2 = theToken.substring(1);
				
				finalNameString += token.toUpperCase() + token2.toLowerCase() + " ";
			}

			
		} else {
			finalNameString = stringtoConvert;
		}
		return finalNameString;
	}

	
	/**
	 * 
	 * This method returns true if the array is not null and length is greater than
	 * zero.
	 * 
	 * @param array
	 * @return boolean
	 */
	public static boolean isObjectArrayNotEmpty(Object[] array) {
		if (array != null && array.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method returns true if the input list is not null and list is not empty.
	 * 
	 * @param list
	 * @return boolean.
	 */
	public static boolean isObjectListNotEmpty(List list) {
		return (list != null && !list.isEmpty());
	}
	
	/**
	 * This method change the status of barcode from available to used.
	 * 
	 * @param list
	 */
	public static void changeBarcodeStatus(ArrayList<String> barCodeList2) {
		PackagingMaterialInventory pmi = null;
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			for (int i = 0; i < barCodeList2.size(); i++) {
				pmi = masterdataService.createPackagingMaterialInventoryQuery().barcode(barCodeList2.get(i))
						.singleResult();
				if (pmi != null){
					pmi.setStatus(1);
					masterdataService.savePackagingMaterialInventory(pmi);}
			}
			
		} catch (Exception e) {
			log.debug("error while updating Packing Inventory" + e);
		}

	}
}
