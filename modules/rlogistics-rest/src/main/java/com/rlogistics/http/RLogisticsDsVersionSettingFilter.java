package com.rlogistics.http;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.activiti.engine.ProcessEngines;

public class RLogisticsDsVersionSettingFilter implements Filter {
	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		ProcessEngines.getDefaultProcessEngine().getMetadataService().loadCurrentDsVersions();
		
		chain.doFilter(request, response);
	}

	public void destroy() {
	}
}
