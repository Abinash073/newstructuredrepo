package com.rlogistics.http;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RLogisticsSessionIdentifyingFilter implements Filter {
	private static Logger log = LoggerFactory.getLogger(RLogisticsSessionIdentifyingFilter.class);
	
	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		try {
			RLogisticsHttpSessionHolder.setHttpSession(((HttpServletRequest)request).getSession());

			chain.doFilter(request, response);
		} catch(Exception ex){
			log.error("RLogisticsSessionIdentifyingFilter . Exception while handling URL:" + ((HttpServletRequest)request).getRequestURL(),ex);
		}
	}

	public void destroy() {
	}
}
