package com.rlogistics.ampm;

public class PickupRequestCancellationSpecification extends AMPMRequest {

	private String pickupRequestNumber;
	private String reason;
	private String type;
	public String getPickupRequestNumber() {
		return pickupRequestNumber;
	}
	public void setPickupRequestNumber(String pickupRequestNumber) {
		this.pickupRequestNumber = pickupRequestNumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
