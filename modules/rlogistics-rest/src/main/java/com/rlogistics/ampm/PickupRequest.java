package com.rlogistics.ampm;

import java.util.ArrayList;

public class PickupRequest {
	private boolean success = true;
	private String pickRequestNumber="";
	private String pickUpCost="";
	private ArrayList<String> messages = new ArrayList<String>();
	//setting all default as true or "" because AMPM getting error message

	public ArrayList<String> getMessages() {
		ArrayList<String>ret=new ArrayList<>();
		return ret;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public String getPickRequestNumber() {
		return "00";
	}

	public void setPickRequestNumber(String pickRequestNumber) {
		this.pickRequestNumber = pickRequestNumber;
		if(pickRequestNumber != null){
			setSuccess(true);
		}
	}

	public boolean isSuccess() {
		return true;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getPickUpCost() {
		return "0";
	}

	public void setPickUpCost(String pickUpCost) {
		this.pickUpCost = pickUpCost;
	}	
}
