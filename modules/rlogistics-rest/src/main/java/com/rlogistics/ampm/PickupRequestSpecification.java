package com.rlogistics.ampm;

public class PickupRequestSpecification extends AMPMRequest {
	
	public static class EndPointDetails{
		private String contactName;
		private String compName;
		private String address;
		private String city;
		private String pin;
		private String phone;
		private String mob;
		private String dateTime;
		private String ticketNo;
		private String rlProcessId;
		
		
		public String getRlProcessId() {
			return rlProcessId;
		}
		public void setRlProcessId(String rlProcessId) {
			this.rlProcessId = rlProcessId;
		}
		public String getTicketNo() {
			return ticketNo;
		}
		public void setTicketNo(String ticketNo) {
			this.ticketNo = ticketNo;
		}
		public String getDateTime() {
			return dateTime;
		}
		public void setDateTime(String dateTime) {
			this.dateTime = dateTime;
		}
		public String getCompName() {
			return compName;
		}
		public void setCompName(String compName) {
			this.compName = compName;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getContactName() {
			return contactName;
		}
		public void setContactName(String contactName) {
			this.contactName = contactName;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getPin() {
			return pin;
		}
		public void setPin(String pin) {
			this.pin = pin;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getMob() {
			return mob;
		}
		public void setMob(String mob) {
			this.mob = mob;
		}
	}
	
	private EndPointDetails pickupDetails;
	private EndPointDetails deliveryDetails;
	
	private String pickupType;
	private String prodType;
	
	private int pcs;
	private float actWt;
	private float volWt;
	public String getHubloc() {
		return hubloc;
	}
	public void setHubloc(String hubloc) {
		this.hubloc = hubloc;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public int getEdd() {
		return edd;
	}
	public void setEdd(int edd) {
		this.edd = edd;
	}
	public String getProdFlag() {
		return prodFlag;
	}
	public void setProdFlag(String prodFlag) {
		this.prodFlag = prodFlag;
	}
	public String getCategBulk() {
		return categBulk;
	}
	public void setCategBulk(String categBulk) {
		this.categBulk = categBulk;
	}
	public String getConsolidationFlag() {
		return consolidationFlag;
	}
	public void setConsolidationFlag(String consolidationFlag) {
		this.consolidationFlag = consolidationFlag;
	}
	public String getCashHandling() {
		return cashHandling;
	}
	public void setCashHandling(String cashHandling) {
		this.cashHandling = cashHandling;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	private String retailerCode;
	private String hubloc;
	private String retailerId;
	private String productCategory;
	private int edd;
	private String prodFlag;
	private String categBulk;
	private String consolidationFlag;
	private String cashHandling;
	private String payType;
	private float amount;
	
	public String getRetailerCode() {
		return retailerCode;
	}
	public void setRetailerCode(String retailerCode) {
		this.retailerCode = retailerCode;
	}
	public String getPickupType() {
		return pickupType;
	}
	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public float getActWt() {
		return actWt;
	}
	public void setActWt(float actWt) {
		this.actWt = actWt;
	}
	public float getVolWt() {
		return volWt;
	}
	public void setVolWt(float volWt) {
		this.volWt = volWt;
	}
	public EndPointDetails getPickupDetails() {
		return pickupDetails;
	}
	public void setPickupDetails(EndPointDetails pickupDetails) {
		this.pickupDetails = pickupDetails;
	}
	public EndPointDetails getDeliveryDetails() {
		return deliveryDetails;
	}
	public void setDeliveryDetails(EndPointDetails deliveryDetails) {
		this.deliveryDetails = deliveryDetails;
	}
}
