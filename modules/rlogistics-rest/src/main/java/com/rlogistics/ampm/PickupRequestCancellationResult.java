package com.rlogistics.ampm;

import java.util.ArrayList;

public class PickupRequestCancellationResult {
	
	private boolean success = true;
	private ArrayList<String> messages = new ArrayList<String>();

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
