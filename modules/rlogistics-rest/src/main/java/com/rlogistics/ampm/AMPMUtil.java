package com.rlogistics.ampm;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.ampm.PickupRequestSpecification.EndPointDetails;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.RestResult;
import com.rlogistics.rest.TrackingResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AMPMUtil {
	public AMPMUtil() {
	}

	private static Logger log = LoggerFactory.getLogger(AMPMUtil.class);

	public static PickupRequest generatePickupRequest(PickupRequestSpecification pickupRequestSpecification) {
		PickupRequest pickupRequest = new PickupRequest();
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody requestBody = getBody(soapRequest);

			String serverURI = "http://tempuri.org/";

			SOAPElement requestElement = requestBody.addChildElement("GetUserDetails", "", serverURI);

			if (pickupRequestSpecification.getUserid() != null) {
				addChildElement(requestElement, "Userid", pickupRequestSpecification.getUserid());
			} else {
				addChildElement(requestElement, "Userid", "");
			}
			if (pickupRequestSpecification.getPassword() != null) {
				addChildElement(requestElement, "Password",
						pickupRequestSpecification.getPassword().replaceAll("&amp;", "&"));
			} else {
				addChildElement(requestElement, "Password", "");
			}
			if (pickupRequestSpecification.getPickupType() != null) {
				addChildElement(requestElement, "PickupType", pickupRequestSpecification.getPickupType());
			} else {
				addChildElement(requestElement, "PickupType", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getContactName() != null) {
				addChildElement(requestElement, "PkContactName",
						pickupRequestSpecification.getPickupDetails().getContactName());
			} else {
				addChildElement(requestElement, "PkContactName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCompName() != null) {
				addChildElement(requestElement, "PKCompName",
						pickupRequestSpecification.getPickupDetails().getCompName());
			} else {
				addChildElement(requestElement, "PKCompName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getAddress() != null) {
				addChildElement(requestElement, "PKAddres", pickupRequestSpecification.getPickupDetails().getAddress());
			} else {
				addChildElement(requestElement, "PKAddres", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCity() != null) {
				addChildElement(requestElement, "PKCity", pickupRequestSpecification.getPickupDetails().getCity());
			} else {
				addChildElement(requestElement, "PKCity", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPin() != null) {
				addChildElement(requestElement, "PKPin", pickupRequestSpecification.getPickupDetails().getPin());
			} else {
				addChildElement(requestElement, "PKPin", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPhone() != null) {
				addChildElement(requestElement, "PKPhone", pickupRequestSpecification.getPickupDetails().getPhone());
			} else {
				addChildElement(requestElement, "PKPhone", "1");
			}
			if (pickupRequestSpecification.getPickupDetails().getMob() != null) {
				addChildElement(requestElement, "PKMob", pickupRequestSpecification.getPickupDetails().getMob());
			} else {
				addChildElement(requestElement, "PKMob", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getContactName() != null) {
				addChildElement(requestElement, "DLContactName",
						pickupRequestSpecification.getDeliveryDetails().getContactName());
			} else {
				addChildElement(requestElement, "DLContactName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCompName() != null) {
				addChildElement(requestElement, "DLCompName",
						pickupRequestSpecification.getDeliveryDetails().getCompName());
			} else {
				addChildElement(requestElement, "DLCompName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getAddress() != null) {
				addChildElement(requestElement, "DLAddres",
						pickupRequestSpecification.getDeliveryDetails().getAddress());
			} else {
				addChildElement(requestElement, "DLAddres", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCity() != null) {
				addChildElement(requestElement, "DLCity", pickupRequestSpecification.getDeliveryDetails().getCity());
			} else {
				addChildElement(requestElement, "DLCity", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPin() != null) {
				addChildElement(requestElement, "DLPin", pickupRequestSpecification.getDeliveryDetails().getPin());
			} else {
				addChildElement(requestElement, "DLPin", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPhone() != null) {
				addChildElement(requestElement, "DLPhone", pickupRequestSpecification.getDeliveryDetails().getPhone());
			} else {
				addChildElement(requestElement, "DLPhone", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getMob() != null) {
				addChildElement(requestElement, "DLMob", pickupRequestSpecification.getDeliveryDetails().getMob());
			} else {
				addChildElement(requestElement, "DLMob", "1");
			}
			if (pickupRequestSpecification.getProdType() != null) {
				addChildElement(requestElement, "ProdType", pickupRequestSpecification.getProdType());
			} else {
				addChildElement(requestElement, "ProdType", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "PickupCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "PickupCustCode", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "RLCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "RLCustCode", "");
			}
			addChildElement(requestElement, "Pcs", pickupRequestSpecification.getPcs());
			if (pickupRequestSpecification.getActWt() != 0.0F) {
				addChildElement(requestElement, "ActWt", pickupRequestSpecification.getActWt());
			} else {
				addChildElement(requestElement, "ActWt", 1.0F);
			}
			if (pickupRequestSpecification.getVolWt() != 0.0F) {
				addChildElement(requestElement, "VolWt", pickupRequestSpecification.getVolWt());
			} else {
				addChildElement(requestElement, "VolWt", 1.0F);
			}

			if (pickupRequestSpecification.getPickupDetails().getDateTime() != null) {
				addChildElement(requestElement, "PkpDateTime",
						pickupRequestSpecification.getPickupDetails().getDateTime());
			} else {
				addChildElement(requestElement, "PkpDateTime", "");
			}

			if (pickupRequestSpecification.getDeliveryDetails().getDateTime() != null) {
				addChildElement(requestElement, "DelDateTime",
						pickupRequestSpecification.getDeliveryDetails().getDateTime());
			} else {
				addChildElement(requestElement, "DelDateTime", "");
			}

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/GetUserDetails");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			String url = "http://fl.bizlog.in/webservice/WebService.asmx?op=GetUserDetails";
			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
			log.debug("Server response:" + toString(soapResponse));

			SOAPBody responseBody = getBody(soapResponse);

			Node statusElement = responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild()
					.getFirstChild();

			if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
				log.debug("PickupRequest number : _--> : " + statusElement.getNextSibling().getTextContent());
				pickupRequest.setPickRequestNumber(statusElement.getNextSibling().getTextContent());
				pickupRequest.setPickUpCost(statusElement.getNextSibling().getNextSibling().getTextContent());
			} else {
				pickupRequest.setSuccess(false);
				pickupRequest.getMessages().add(statusElement.getTextContent());
			}
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);

			PickupRequest pickupRequestError = new PickupRequest();
			pickupRequestError.getMessages()
					.add("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
			return pickupRequestError;
		}

		return pickupRequest;
	}

	public static PickupRequest generatePickupRequestNew(PickupRequestSpecification pickupRequestSpecification) {
		PickupRequest pickupRequest = new PickupRequest();
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody requestBody = getBody(soapRequest);

			String serverURI = "http://tempuri.org/";

			SOAPElement requestElement = requestBody.addChildElement("GetUserDetails", "", serverURI);

			if (pickupRequestSpecification.getUserid() != null) {
				addChildElement(requestElement, "Userid", pickupRequestSpecification.getUserid());
			} else {
				addChildElement(requestElement, "Userid", "");
			}
			if (pickupRequestSpecification.getPassword() != null) {
				addChildElement(requestElement, "Password",
						pickupRequestSpecification.getPassword().replaceAll("&amp;", "&"));
			} else {
				addChildElement(requestElement, "Password", "");
			}
			if (pickupRequestSpecification.getPickupType() != null) {
				addChildElement(requestElement, "PickupType", pickupRequestSpecification.getPickupType());
			} else {
				addChildElement(requestElement, "PickupType", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getContactName() != null) {
				addChildElement(requestElement, "PkContactName",
						pickupRequestSpecification.getPickupDetails().getContactName());
			} else {
				addChildElement(requestElement, "PkContactName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCompName() != null) {
				addChildElement(requestElement, "PKCompName",
						pickupRequestSpecification.getPickupDetails().getCompName());
			} else {
				addChildElement(requestElement, "PKCompName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getAddress() != null) {
				addChildElement(requestElement, "PKAddres", pickupRequestSpecification.getPickupDetails().getAddress());
			} else {
				addChildElement(requestElement, "PKAddres", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCity() != null) {
				addChildElement(requestElement, "PKCity", pickupRequestSpecification.getPickupDetails().getCity());
			} else {
				addChildElement(requestElement, "PKCity", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPin() != null) {
				addChildElement(requestElement, "PKPin", pickupRequestSpecification.getPickupDetails().getPin());
			} else {
				addChildElement(requestElement, "PKPin", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPhone() != null) {
				addChildElement(requestElement, "PKPhone", pickupRequestSpecification.getPickupDetails().getPhone());
			} else {
				addChildElement(requestElement, "PKPhone", "1");
			}
			if (pickupRequestSpecification.getPickupDetails().getMob() != null) {
				addChildElement(requestElement, "PKMob", pickupRequestSpecification.getPickupDetails().getMob());
			} else {
				addChildElement(requestElement, "PKMob", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getContactName() != null) {
				addChildElement(requestElement, "DLContactName",
						pickupRequestSpecification.getDeliveryDetails().getContactName());
			} else {
				addChildElement(requestElement, "DLContactName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCompName() != null) {
				addChildElement(requestElement, "DLCompName",
						pickupRequestSpecification.getDeliveryDetails().getCompName());
			} else {
				addChildElement(requestElement, "DLCompName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getAddress() != null) {
				addChildElement(requestElement, "DLAddres",
						pickupRequestSpecification.getDeliveryDetails().getAddress());
			} else {
				addChildElement(requestElement, "DLAddres", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCity() != null) {
				addChildElement(requestElement, "DLCity", pickupRequestSpecification.getDeliveryDetails().getCity());
			} else {
				addChildElement(requestElement, "DLCity", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPin() != null) {
				addChildElement(requestElement, "DLPin", pickupRequestSpecification.getDeliveryDetails().getPin());
			} else {
				addChildElement(requestElement, "DLPin", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPhone() != null) {
				addChildElement(requestElement, "DLPhone", pickupRequestSpecification.getDeliveryDetails().getPhone());
			} else {
				addChildElement(requestElement, "DLPhone", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getMob() != null) {
				addChildElement(requestElement, "DLMob", pickupRequestSpecification.getDeliveryDetails().getMob());
			} else {
				addChildElement(requestElement, "DLMob", "1");
			}
			if (pickupRequestSpecification.getProdType() != null) {
				addChildElement(requestElement, "ProdType", pickupRequestSpecification.getProdType());
			} else {
				addChildElement(requestElement, "ProdType", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "PickupCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "PickupCustCode", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "RLCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "RLCustCode", "");
			}
			addChildElement(requestElement, "Pcs", pickupRequestSpecification.getPcs());
			if (pickupRequestSpecification.getActWt() != 0.0F) {
				addChildElement(requestElement, "ActWt", pickupRequestSpecification.getActWt());
			} else {
				addChildElement(requestElement, "ActWt", 1.0F);
			}
			if (pickupRequestSpecification.getVolWt() != 0.0F) {
				addChildElement(requestElement, "VolWt", pickupRequestSpecification.getVolWt());
			} else {
				addChildElement(requestElement, "VolWt", 1.0F);
			}

			if (pickupRequestSpecification.getPickupDetails().getDateTime() != null) {
				addChildElement(requestElement, "PkpDateTime",
						pickupRequestSpecification.getPickupDetails().getDateTime());
			} else {
				addChildElement(requestElement, "PkpDateTime", "");
			}

			if (pickupRequestSpecification.getDeliveryDetails().getDateTime() != null) {
				addChildElement(requestElement, "DelDateTime",
						pickupRequestSpecification.getDeliveryDetails().getDateTime());
			} else {
				addChildElement(requestElement, "DelDateTime", "");
			}

			addChildElement(requestElement, "Hubloc", pickupRequestSpecification.getHubloc());
			addChildElement(requestElement, "RetailerID", pickupRequestSpecification.getRetailerId());
			addChildElement(requestElement, "Prodcategory", pickupRequestSpecification.getProductCategory());
			addChildElement(requestElement, "EDD", pickupRequestSpecification.getEdd());
			addChildElement(requestElement, "ProdFlag", pickupRequestSpecification.getProdFlag());
			addChildElement(requestElement, "CategBulk", pickupRequestSpecification.getCategBulk());
			addChildElement(requestElement, "ConsolidationFlag", pickupRequestSpecification.getConsolidationFlag());
			addChildElement(requestElement, "CashHandling", pickupRequestSpecification.getCashHandling());
			addChildElement(requestElement, "PayType", pickupRequestSpecification.getPayType());
			addChildElement(requestElement, "Amount", pickupRequestSpecification.getAmount());

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/GetPickupReg_v2");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			String url = "http://bizlog.co.in/BizServices/WebService.asmx?op=GetPickupReg_v2";
			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
			log.debug("Server response:" + toString(soapResponse));

			SOAPBody responseBody = getBody(soapResponse);

			Node statusElement = responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild()
					.getFirstChild();

			if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
				log.debug("PickupRequest number : _--> : " + statusElement.getNextSibling().getTextContent());
				pickupRequest.setPickRequestNumber(statusElement.getNextSibling().getTextContent());
				pickupRequest.setPickUpCost(statusElement.getNextSibling().getNextSibling().getTextContent());
			} else {
				pickupRequest.setSuccess(false);
				pickupRequest.getMessages().add(statusElement.getTextContent());
			}
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);

			PickupRequest pickupRequestError = new PickupRequest();
			pickupRequestError.getMessages()
					.add("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
			return pickupRequestError;
		}

		return pickupRequest;
	}

	public static SOAPBody getBody(SOAPMessage soapMessage) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		SOAPBody soapBody = soapEnvelope.getBody();
		return soapBody;
	}

	public static PickupRequestCancellationResult cancelPickupRequest(
			PickupRequestCancellationSpecification pickupRequestCancellationSpecification) {
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody soapBody = getBody(soapRequest);

			String serverURI = "http://tempuri.org/";

			SOAPElement requestElement = soapBody.addChildElement("GetCancelRequestDetails", "", serverURI);

			addChildElement(requestElement, "Userid", pickupRequestCancellationSpecification.getUserid());
			addChildElement(requestElement, "Password", pickupRequestCancellationSpecification.getPassword());
			addChildElement(requestElement, "ReffNo", pickupRequestCancellationSpecification.getPickupRequestNumber());
			addChildElement(requestElement, "CancelReason", pickupRequestCancellationSpecification.getReason());
			addChildElement(requestElement, "CancelType", pickupRequestCancellationSpecification.getType());

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/GetCancelRequestDetails");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			String url = "http://bizlog.co.in/bizservices/WebService.asmx?op=GetCancelRequestDetails";
			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);

			log.debug("Server response:" + toString(soapResponse));
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of cancelPickupRequest:" + th.getMessage(), th);
			PickupRequestCancellationResult pickupRequestCancellation = new PickupRequestCancellationResult();
			pickupRequestCancellation.getMessages()
					.add("Caught exception while processing SOAP parts of cancelPickupRequest:" + th.getMessage());
			return pickupRequestCancellation;
		}

		return new PickupRequestCancellationResult();
	}

	private static String toString(SOAPMessage message) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		message.writeTo(out);
		return new String(out.toByteArray());
	}

	private static void addChildElement(SOAPElement parentElement, String elementName, int i) throws SOAPException {
		addChildElement(parentElement, elementName, new Integer(i).toString());
	}

	private static void addChildElement(SOAPElement parentElement, String elementName, float f) throws SOAPException {
		addChildElement(parentElement, elementName, new Float(f).toString());
	}

	private static void addChildElement(SOAPElement parentElement, String elementName, String elementText)
			throws SOAPException {
		SOAPElement childElement = parentElement.addChildElement(elementName);

		childElement.addTextNode(elementText);
	}

	public static RestResult trackProductStatus(String conNo) {
		String responseString = null;
		List<TrackingResponse> trackingResponseList = new ArrayList();
		RestResult restResult = new RestResult();

		try {
			CloseableHttpClient client = HttpClients.createDefault();

			HttpGet httpget = new HttpGet("http://bizlog.co.in/biztrackapi/default.aspx?cnno=" + conNo);

			httpget.addHeader("Content-Type", "text/xml");

			CloseableHttpResponse response = client.execute(httpget);

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				responseString = EntityUtils.toString(entity);
			}

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(responseString.getBytes()));

			Element docEle = doc.getDocumentElement();

			NodeList nl = docEle.getElementsByTagName("Table");

			if ((nl != null) && (nl.getLength() > 0)) {
				for (int i = 0; i < nl.getLength(); i++) {

					Element el = (Element) nl.item(i);

					TrackingResponse e = getTrackingResponse(el);

					trackingResponseList.add(e);
				}
			}
			restResult.setResponse(trackingResponseList);
		} catch (Throwable th) {
			restResult.setMessage(th.getMessage());

			return restResult;
		}

		return restResult;
	}

	private static TrackingResponse getTrackingResponse(Element empEl) {
		TrackingResponse trackingResponse = new TrackingResponse();
		trackingResponse.setLocation(getTextValue(empEl, "location"));
		trackingResponse.setType(getTextValue(empEl, "Typee"));
		if (getTextValue(empEl, "Reffno") != null) {
			trackingResponse.setRefNo(getTextValue(empEl, "Reffno"));
		}
		trackingResponse.setConNo(getTextValue(empEl, "Cnno"));
		trackingResponse.setConNoDate(getTextValue(empEl, "CnnoDate"));
		trackingResponse.setConNoTime(getTextValue(empEl, "CnnoTime"));
		if (getTextValue(empEl, "MfNo") != null) {
			trackingResponse.setMfNo(getTextValue(empEl, "MfNo"));
		}
		if (getTextValue(empEl, "CityName") != null) {
			trackingResponse.setCityName(getTextValue(empEl, "CityName"));
		}
		trackingResponse.setStatus(getTextValue(empEl, "Status"));
		if (getTextValue(empEl, "RelationShip") != null) {
			trackingResponse.setRelationShip(getTextValue(empEl, "RelationShip"));
		}
		if (getTextValue(empEl, "Remarks") != null) {
			trackingResponse.setRemarks(getTextValue(empEl, "Remarks"));
		}
		return trackingResponse;
	}

	private static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if ((nl != null) && (nl.getLength() > 0)) {
			Element el = (Element) nl.item(0);
			Node node = el.getFirstChild();
			if (node != null) {
				textVal = node.getNodeValue();
			}
		}

		return textVal;
	}

	/**
	 * Calling from Activiti for delivery status update
	 * 
	 * @param ticketNo
	 * @param ifDelivered
	 * @param ifNotDelivered
	 * @return Delived =
	 */

	public static RestResult updateDeliveryStatus(String ticketNo, String ifDelivered, String ifNotDelivered) {
		RestResult restResult = new RestResult();
		String cnno = ticketNo;
		String delstatus;
		delstatus = ifDelivered != null ? ifDelivered : ifNotDelivered;
		log.debug("Delivery Status: " + delstatus);
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody requestBody = getBody(soapRequest);
			log.debug("inside soapreq: " + ticketNo);

			String serverURI = "http://tempuri.org/";
			log.debug("After org");

			SOAPElement requestElement = requestBody.addChildElement("Insert", "", serverURI);
			log.debug("After reqBody");

			if ((ticketNo != null) || (!ticketNo.isEmpty())) {
				addChildElement(requestElement, "cnno", cnno);
			}

			if ((delstatus != null) || (!delstatus.isEmpty())) {
				addChildElement(requestElement, "delstatus", delstatus);
			} else {
				addChildElement(requestElement, "delstatus", null);
			}

			log.debug("After Adding Data");

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/Insert");
			log.debug("After Header");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			String url = "http://fl.bizlog.in/DeliveryStatus/DeliveryStatus.asmx?op=Insert";
			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
			log.debug("Server response:" + toString(soapResponse));

			SOAPBody responseBody = getBody(soapResponse);

			Node statusElement = responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild()
					.getFirstChild();

			if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
				log.debug("API STatus: " + statusElement);
				restResult.setMessage("Successfully status is updated ");
				restResult.setResponse("Success");
				restResult.setSuccess(true);
			} else {
				restResult.setMessage("API Calling Failed");
				restResult.setResponse("FAILED" + statusElement);
				restResult.setSuccess(true);
			}
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);
			restResult.setMessage("Error while Calling API :");
			restResult.setResponse(
					"Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
			restResult.setSuccess(false);
			return restResult;
		}

		return restResult;
	}

	public static void sendTicketDetails(String processId, String code) {

	}

	/**
	 * This method is for to send ticket details to AMPM for LSP for Advance
	 * Exchange flow only
	 * 
	 * @param pickupRequestSpecification
	 * @return
	 */
	public static PickupRequest generatePickupRequestAdvanceExchange(
			PickupRequestSpecification pickupRequestSpecification) {
		saveToConsoleLogsTable("AMPM", "generatePickupRequestAdvanceExchange");
		PickupRequest pickupRequest = new PickupRequest();
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody requestBody = getBody(soapRequest);

			String serverURI = "http://tempuri.org/";

			SOAPElement requestElement = requestBody.addChildElement("Add", "", serverURI);

			if (pickupRequestSpecification.getUserid() != null) {
				addChildElement(requestElement, "Userid", pickupRequestSpecification.getUserid());
			} else {
				addChildElement(requestElement, "Userid", "");
			}
			if (pickupRequestSpecification.getPassword() != null) {
				addChildElement(requestElement, "Password",
						pickupRequestSpecification.getPassword().replaceAll("&amp;", "&"));
			} else {
				addChildElement(requestElement, "Password", "");
			}
			if (pickupRequestSpecification.getPickupType() != null) {
				addChildElement(requestElement, "PickupType", pickupRequestSpecification.getPickupType());
			} else {
				addChildElement(requestElement, "PickupType", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getContactName() != null) {
				addChildElement(requestElement, "PkContactName",
						pickupRequestSpecification.getPickupDetails().getContactName());
			} else {
				addChildElement(requestElement, "PkContactName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCompName() != null) {
				addChildElement(requestElement, "PKCompName",
						pickupRequestSpecification.getPickupDetails().getCompName());
			} else {
				addChildElement(requestElement, "PKCompName", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getAddress() != null) {
				addChildElement(requestElement, "PKAddres", pickupRequestSpecification.getPickupDetails().getAddress());
			} else {
				addChildElement(requestElement, "PKAddres", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getCity() != null) {
				addChildElement(requestElement, "PKCity", pickupRequestSpecification.getPickupDetails().getCity());
			} else {
				addChildElement(requestElement, "PKCity", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPin() != null) {
				addChildElement(requestElement, "PKPin", pickupRequestSpecification.getPickupDetails().getPin());
			} else {
				addChildElement(requestElement, "PKPin", "");
			}
			if (pickupRequestSpecification.getPickupDetails().getPhone() != null) {
				addChildElement(requestElement, "PKPhone", pickupRequestSpecification.getPickupDetails().getPhone());
			} else {
				addChildElement(requestElement, "PKPhone", "1");
			}
			if (pickupRequestSpecification.getPickupDetails().getMob() != null) {
				addChildElement(requestElement, "PKMob", pickupRequestSpecification.getPickupDetails().getMob());
			} else {
				addChildElement(requestElement, "PKMob", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getContactName() != null) {
				addChildElement(requestElement, "DLContactName",
						pickupRequestSpecification.getDeliveryDetails().getContactName());
			} else {
				addChildElement(requestElement, "DLContactName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCompName() != null) {
				addChildElement(requestElement, "DLCompName",
						pickupRequestSpecification.getDeliveryDetails().getCompName());
			} else {
				addChildElement(requestElement, "DLCompName", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getAddress() != null) {
				addChildElement(requestElement, "DLAddres",
						pickupRequestSpecification.getDeliveryDetails().getAddress());
			} else {
				addChildElement(requestElement, "DLAddres", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getCity() != null) {
				addChildElement(requestElement, "DLCity", pickupRequestSpecification.getDeliveryDetails().getCity());
			} else {
				addChildElement(requestElement, "DLCity", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPin() != null) {
				addChildElement(requestElement, "DLPin", pickupRequestSpecification.getDeliveryDetails().getPin());
			} else {
				addChildElement(requestElement, "DLPin", "");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getPhone() != null) {
				addChildElement(requestElement, "DLPhone", pickupRequestSpecification.getDeliveryDetails().getPhone());
			} else {
				addChildElement(requestElement, "DLPhone", "1");
			}
			if (pickupRequestSpecification.getDeliveryDetails().getMob() != null) {
				addChildElement(requestElement, "DLMob", pickupRequestSpecification.getDeliveryDetails().getMob());
			} else {
				addChildElement(requestElement, "DLMob", "1");
			}
			if (pickupRequestSpecification.getProdType() != null) {
				addChildElement(requestElement, "ProdType", pickupRequestSpecification.getProdType());
			} else {
				addChildElement(requestElement, "ProdType", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "PickupCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "PickupCustCode", "");
			}
			if (pickupRequestSpecification.getRetailerCode() != null) {
				addChildElement(requestElement, "RLCustCode", pickupRequestSpecification.getRetailerCode());
			} else {
				addChildElement(requestElement, "RLCustCode", "");
			}
			addChildElement(requestElement, "Pcs", pickupRequestSpecification.getPcs());
			if (pickupRequestSpecification.getActWt() != 0.0F) {
				addChildElement(requestElement, "ActWt", pickupRequestSpecification.getActWt());
			} else {
				addChildElement(requestElement, "ActWt", 1.0F);
			}
			if (pickupRequestSpecification.getVolWt() != 0.0F) {
				addChildElement(requestElement, "VolWt", pickupRequestSpecification.getVolWt());
			} else {
				addChildElement(requestElement, "VolWt", 1.0F);
			}

			if (pickupRequestSpecification.getPickupDetails().getDateTime() != null) {
				addChildElement(requestElement, "PkpDateTime",
						pickupRequestSpecification.getPickupDetails().getDateTime());
			} else {
				addChildElement(requestElement, "PkpDateTime", "");
			}

			if (pickupRequestSpecification.getDeliveryDetails().getDateTime() != null) {
				addChildElement(requestElement, "DelDateTime",
						pickupRequestSpecification.getDeliveryDetails().getDateTime());
			} else {
				addChildElement(requestElement, "DelDateTime", "2017-07-12 09:57:00");
			}
			/**
			 * Added by prem to send ticket number, PkpRlProcessId to AMPM to
			 * pick up
			 */
			try {
				if (pickupRequestSpecification.getPickupDetails().getTicketNo() != null) {
					addChildElement(requestElement, "PkpTicketNo",
							pickupRequestSpecification.getPickupDetails().getTicketNo());
				} else {
					addChildElement(requestElement, "PkpTicketNo", "");
				}
				if (pickupRequestSpecification.getPickupDetails().getRlProcessId() != null) {
					addChildElement(requestElement, "PkpRlProcessId",
							pickupRequestSpecification.getPickupDetails().getRlProcessId());
				} else {
					addChildElement(requestElement, "PkpRlProcessId", "");
				}
			} catch (Exception e) {

			}

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/Add");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));
			saveToConsoleLogsTable("AMPM", "Request :" + pickupRequestSpecification.getDeliveryDetails().getTicketNo()
					+ ":" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// String url =
			// "http://fl.bizlog.in/webservice/WebService.asmx?op=GetUserDetails";
			String url = "http://fl.bizlog.in/Pickup-req/Pickup-Req.asmx?op=Add";
			// String url =
			// "https://demo.bizlog.in/Pickup-req/Pickup-Req.asmx?op=Add";

			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
			log.debug("Server response:-------------");
			log.debug("Server response:" + toString(soapResponse));
			saveToConsoleLogsTable("AMPM", "Response :" + pickupRequestSpecification.getDeliveryDetails().getTicketNo()
					+ ":" + toString(soapResponse));

			SOAPBody responseBody = getBody(soapResponse);

			/**
			 * 
			 */
			// Node statusElement =
			// responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild();

			Node statusElement = responseBody;

			/**
			 * this is commented because we are not doing anything on response
			 */
			/*
			 * if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
			 * log.debug("PickupRequest number : _--> : " +
			 * statusElement.getNextSibling().getTextContent());
			 * pickupRequest.setPickRequestNumber(statusElement.getNextSibling()
			 * .getTextContent());
			 * pickupRequest.setPickUpCost(statusElement.getNextSibling().
			 * getNextSibling().getTextContent()); } else {
			 * pickupRequest.setSuccess(false);
			 * pickupRequest.getMessages().add(statusElement.getTextContent());
			 * }
			 */
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);

			PickupRequest pickupRequestError = new PickupRequest();
			pickupRequestError.getMessages()
					.add("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
			return pickupRequestError;
		}

		return pickupRequest;
	}

	/*
	 * For RTO
	 */
	public static PickupRequest generateRTORequestAdvanceExchange(
			PickupRequestSpecification pickupRequestSpecification) {
		saveToConsoleLogsTable("AMPM", "generateRTORequestAdvanceExchange");

		PickupRequest pickupRequest = new PickupRequest();
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapRequest = messageFactory.createMessage();
			SOAPBody requestBody = getBody(soapRequest);

			String serverURI = "http://tempuri.org/";

			SOAPElement requestElement = requestBody.addChildElement("AddRTO", "", serverURI);
			/*
			 * if (pickupRequestSpecification.getUserid() != null) {
			 * addChildElement(requestElement, "Userid",
			 * pickupRequestSpecification.getUserid()); } else {
			 * addChildElement(requestElement, "Userid", ""); } if
			 * (pickupRequestSpecification.getPassword() != null) {
			 * addChildElement(requestElement, "Password",
			 * pickupRequestSpecification.getPassword().replaceAll("&amp;",
			 * "&")); } else { addChildElement(requestElement, "Password", "");
			 * } if (pickupRequestSpecification.getPickupType() != null) {
			 * addChildElement(requestElement, "PickupType",
			 * pickupRequestSpecification.getPickupType()); } else {
			 * addChildElement(requestElement, "PickupType", ""); } if
			 * (pickupRequestSpecification.getPickupDetails().getContactName()
			 * != null) { addChildElement(requestElement, "PkContactName",
			 * pickupRequestSpecification.getPickupDetails().getContactName());
			 * } else { addChildElement(requestElement, "PkContactName", ""); }
			 * if (pickupRequestSpecification.getPickupDetails().getCompName()
			 * != null) { addChildElement(requestElement, "PKCompName",
			 * pickupRequestSpecification.getPickupDetails().getCompName()); }
			 * else { addChildElement(requestElement, "PKCompName", ""); } if
			 * (pickupRequestSpecification.getPickupDetails().getAddress() !=
			 * null) { addChildElement(requestElement, "PKAddres",
			 * pickupRequestSpecification.getPickupDetails().getAddress()); }
			 * else { addChildElement(requestElement, "PKAddres", ""); } if
			 * (pickupRequestSpecification.getPickupDetails().getCity() != null)
			 * { addChildElement(requestElement, "PKCity",
			 * pickupRequestSpecification.getPickupDetails().getCity()); } else
			 * { addChildElement(requestElement, "PKCity", ""); } if
			 * (pickupRequestSpecification.getPickupDetails().getPin() != null)
			 * { addChildElement(requestElement, "PKPin",
			 * pickupRequestSpecification.getPickupDetails().getPin()); } else {
			 * addChildElement(requestElement, "PKPin", ""); } if
			 * (pickupRequestSpecification.getPickupDetails().getPhone() !=
			 * null) { addChildElement(requestElement, "PKPhone",
			 * pickupRequestSpecification.getPickupDetails().getPhone()); } else
			 * { addChildElement(requestElement, "PKPhone", "1"); } if
			 * (pickupRequestSpecification.getPickupDetails().getMob() != null)
			 * { addChildElement(requestElement, "PKMob",
			 * pickupRequestSpecification.getPickupDetails().getMob()); } else {
			 * addChildElement(requestElement, "PKMob", "1"); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getContactName()
			 * != null) { addChildElement(requestElement, "DLContactName",
			 * pickupRequestSpecification.getDeliveryDetails().getContactName())
			 * ; } else { addChildElement(requestElement, "DLContactName", "");
			 * } if
			 * (pickupRequestSpecification.getDeliveryDetails().getCompName() !=
			 * null) { addChildElement(requestElement, "DLCompName",
			 * pickupRequestSpecification.getDeliveryDetails().getCompName()); }
			 * else { addChildElement(requestElement, "DLCompName", ""); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getAddress() !=
			 * null) { addChildElement(requestElement, "DLAddres",
			 * pickupRequestSpecification.getDeliveryDetails().getAddress()); }
			 * else { addChildElement(requestElement, "DLAddres", ""); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getCity() !=
			 * null) { addChildElement(requestElement, "DLCity",
			 * pickupRequestSpecification.getDeliveryDetails().getCity()); }
			 * else { addChildElement(requestElement, "DLCity", ""); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getPin() !=
			 * null) { addChildElement(requestElement, "DLPin",
			 * pickupRequestSpecification.getDeliveryDetails().getPin()); } else
			 * { addChildElement(requestElement, "DLPin", ""); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getPhone() !=
			 * null) { addChildElement(requestElement, "DLPhone",
			 * pickupRequestSpecification.getDeliveryDetails().getPhone()); }
			 * else { addChildElement(requestElement, "DLPhone", "1"); } if
			 * (pickupRequestSpecification.getDeliveryDetails().getMob() !=
			 * null) { addChildElement(requestElement, "DLMob",
			 * pickupRequestSpecification.getDeliveryDetails().getMob()); } else
			 * { addChildElement(requestElement, "DLMob", "1"); } if
			 * (pickupRequestSpecification.getProdType() != null) {
			 * addChildElement(requestElement, "ProdType",
			 * pickupRequestSpecification.getProdType()); } else {
			 * addChildElement(requestElement, "ProdType", ""); } if
			 * (pickupRequestSpecification.getRetailerCode() != null) {
			 * addChildElement(requestElement, "PickupCustCode",
			 * pickupRequestSpecification.getRetailerCode()); } else {
			 * addChildElement(requestElement, "PickupCustCode", ""); } if
			 * (pickupRequestSpecification.getRetailerCode() != null) {
			 * addChildElement(requestElement, "RLCustCode",
			 * pickupRequestSpecification.getRetailerCode()); } else {
			 * addChildElement(requestElement, "RLCustCode", ""); }
			 * addChildElement(requestElement, "Pcs",
			 * pickupRequestSpecification.getPcs()); if
			 * (pickupRequestSpecification.getActWt() != 0.0F) {
			 * addChildElement(requestElement, "ActWt",
			 * pickupRequestSpecification.getActWt()); } else {
			 * addChildElement(requestElement, "ActWt", 1.0F); } if
			 * (pickupRequestSpecification.getVolWt() != 0.0F) {
			 * addChildElement(requestElement, "VolWt",
			 * pickupRequestSpecification.getVolWt()); } else {
			 * addChildElement(requestElement, "VolWt", 1.0F); }
			 * 
			 * if (pickupRequestSpecification.getPickupDetails().getDateTime()
			 * != null) { addChildElement(requestElement, "PkpDateTime",
			 * pickupRequestSpecification.getPickupDetails().getDateTime()); }
			 * else { addChildElement(requestElement, "PkpDateTime", ""); }
			 * 
			 * if (pickupRequestSpecification.getDeliveryDetails().getDateTime()
			 * != null) { addChildElement(requestElement, "DelDateTime",
			 * pickupRequestSpecification.getDeliveryDetails().getDateTime()); }
			 * else { addChildElement(requestElement, "DelDateTime",
			 * "2017-07-12 09:57:00"); }
			 */
			/**
			 * Added by prem to for RTO sending only ticket number and status is
			 * hardcode RTO
			 */
			try {
				if (pickupRequestSpecification.getPickupDetails().getTicketNo() != null) {
					addChildElement(requestElement, "Cnno",
							pickupRequestSpecification.getPickupDetails().getTicketNo());
				} else {
					addChildElement(requestElement, "Cnno", "");
				}
				if (pickupRequestSpecification.getPickupDetails().getRlProcessId() != null) {
					addChildElement(requestElement, "status",
							pickupRequestSpecification.getPickupDetails().getRlProcessId());
				} else {
					addChildElement(requestElement, "status", "RTO");
				}
			} catch (Exception e) {

			}

			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/AddRTO");

			soapRequest.saveChanges();

			log.debug("Server request:" + toString(soapRequest));

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			String url = "http://fl.bizlog.in/MarkRto/MarkRto.asmx?op=AddRTO";

			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
			log.debug("Server response:-------------");
			log.debug("Server response:" + toString(soapResponse));

			SOAPBody responseBody = getBody(soapResponse);

			/**
			 * 
			 */
			// Node statusElement =
			// responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild();

			Node statusElement = responseBody;

			/**
			 * this is commented because we are not doing anything on response
			 */
			/*
			 * if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
			 * log.debug("PickupRequest number : _--> : " +
			 * statusElement.getNextSibling().getTextContent());
			 * pickupRequest.setPickRequestNumber(statusElement.getNextSibling()
			 * .getTextContent());
			 * pickupRequest.setPickUpCost(statusElement.getNextSibling().
			 * getNextSibling().getTextContent()); } else {
			 * pickupRequest.setSuccess(false);
			 * pickupRequest.getMessages().add(statusElement.getTextContent());
			 * }
			 */
		} catch (Throwable th) {
			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);

			PickupRequest pickupRequestError = new PickupRequest();
			pickupRequestError.getMessages()
					.add("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
			return pickupRequestError;
		}

		return pickupRequest;
	}

	/**
	 * this is for Pick and drop one way
	 * 
	 * @param rlProcessId
	 */
	public static void commanLSPDrop(String rlProcessId) {
		saveToConsoleLogsTable("AMPM", "commanLSPDrop method call");

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();

		String ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));

		PickupRequestSpecification pickupRequestSpecification = new PickupRequestSpecification();

		pickupRequestSpecification.setPickupType("D");
		EndPointDetails endPointDetails = new EndPointDetails();
		endPointDetails
				.setContactName(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationContact")));
		endPointDetails.setCompName("BizLog");
		endPointDetails.setAddress(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationAddress")));
		endPointDetails.setCity(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationCity")));
		endPointDetails.setPin(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationPincode")));
		endPointDetails.setPhone(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationPhone")));
		endPointDetails.setMob(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationAlternatePhone")));
		endPointDetails.setDateTime(getCurrentTime());
		endPointDetails.setTicketNo(String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo")));
		endPointDetails.setRlProcessId(String.valueOf(runtimeService.getVariable(rlProcessId, "rlProcessId")));

		pickupRequestSpecification.setPickupDetails(endPointDetails);

		EndPointDetails deliveryDetails = new EndPointDetails();
		deliveryDetails
				.setContactName(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationContact")));
		deliveryDetails.setCompName("BizLog");
		deliveryDetails.setAddress(String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocation")) + ","
				+ String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocAddress1") + ","
						+ String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocAddress2"))));
		deliveryDetails.setCity(String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocCity")));
		deliveryDetails.setPin(String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocPincode")));
		deliveryDetails.setPhone(String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocContactNo")));
		deliveryDetails.setMob(String.valueOf(runtimeService.getVariable(rlProcessId, "dropLocAlternateNo")));
		deliveryDetails.setDateTime(getCurrentTime());
		deliveryDetails.setTicketNo(String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo")));
		deliveryDetails.setRlProcessId(String.valueOf(runtimeService.getVariable(rlProcessId, "rlProcessId")));

		pickupRequestSpecification.setDeliveryDetails(deliveryDetails);
		pickupRequestSpecification
				.setRetailerCode(String.valueOf(runtimeService.getVariable(rlProcessId, "rlRetailerCode")));
		pickupRequestSpecification
				.setProdType(String.valueOf(runtimeService.getVariable(rlProcessId, "productCategory")));
		pickupRequestSpecification.setPcs(1);
		pickupRequestSpecification
				.setActWt(Float.valueOf(String.valueOf((runtimeService.getVariable(rlProcessId, "rlActWeight")))));
		pickupRequestSpecification
				.setVolWt(Float.valueOf(String.valueOf((runtimeService.getVariable(rlProcessId, "rlVolWeight")))));

		generatePickupRequestAdvanceExchange(pickupRequestSpecification);

	}

	public static void pnd2WPickUpLSP(String rlProcessId) {
		saveToConsoleLogsTable("AMPM", "pnd2WPickUpLSP method call");
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();

		String ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));

		PickupRequestSpecification pickupRequestSpecification = new PickupRequestSpecification();

		pickupRequestSpecification.setPickupType("P");
		EndPointDetails endPointDetails = new EndPointDetails();
		endPointDetails
				.setContactName(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationContact")));
		endPointDetails.setCompName("BizLog");
		endPointDetails.setAddress(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationAddress")));
		endPointDetails.setCity(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationCity")));
		endPointDetails.setPin(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationPincode")));
		endPointDetails.setPhone(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationPhone")));
		endPointDetails.setMob(String.valueOf(runtimeService.getVariable(rlProcessId, "rlHomeLocationAlternatePhone")));
		endPointDetails.setDateTime(getCurrentTime());
		endPointDetails.setTicketNo(String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo")));
		endPointDetails.setRlProcessId(String.valueOf(runtimeService.getVariable(rlProcessId, "rlProcessId")));
		pickupRequestSpecification.setPickupDetails(endPointDetails);

		EndPointDetails deliveryDetails = new EndPointDetails();
		if (String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterName")).equals(null)
				|| String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterName")).equals("null")) {
			deliveryDetails.setContactName("Bizlog Name");
		} else {
			deliveryDetails
					.setContactName(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterName")));
		}
		deliveryDetails.setCompName("BizLog");
		deliveryDetails.setAddress(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterAddress"))
				+ String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterArea")));
		deliveryDetails.setCity(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterCity")));
		deliveryDetails.setPin(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterPincode")));
		deliveryDetails.setPhone(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterPhone")));
		deliveryDetails
				.setMob(String.valueOf(runtimeService.getVariable(rlProcessId, "rlServiceCenterAlternatePhone")));
		deliveryDetails.setDateTime(getCurrentTime());

		deliveryDetails.setTicketNo(String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo")));
		deliveryDetails.setRlProcessId(String.valueOf(runtimeService.getVariable(rlProcessId, "rlProcessId")));

		pickupRequestSpecification.setDeliveryDetails(deliveryDetails);
		pickupRequestSpecification
				.setRetailerCode(String.valueOf(runtimeService.getVariable(rlProcessId, "rlRetailerCode")));
		pickupRequestSpecification
				.setProdType(String.valueOf(runtimeService.getVariable(rlProcessId, "productCategory")));
		pickupRequestSpecification.setPcs(1);
		pickupRequestSpecification
				.setActWt(Float.valueOf(String.valueOf((runtimeService.getVariable(rlProcessId, "rlActWeight")))));
		pickupRequestSpecification
				.setVolWt(Float.valueOf(String.valueOf((runtimeService.getVariable(rlProcessId, "rlVolWeight")))));

		generatePickupRequestAdvanceExchange(pickupRequestSpecification);

	}

	private static String getCurrentTime() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return dtf.format(now) + "";
	}

	public static void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}

}
