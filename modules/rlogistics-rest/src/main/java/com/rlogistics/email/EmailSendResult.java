package com.rlogistics.email;

import java.util.ArrayList;
import java.util.List;

public class EmailSendResult {

	private List<String> messages = new ArrayList<>();
	private EmailSendCommand command;
	private String expansion;

	public EmailSendResult() {
		this.setCommand(command);
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public EmailSendCommand getCommand() {
		return command;
	}

	public void setCommand(EmailSendCommand command) {
		this.command = command;
	}

	public String getExpansion() {
		return expansion;
	}

	public void setExpansion(String expansion) {
		this.expansion = expansion;
	}

}
