package com.rlogistics.email;

import java.util.ArrayList;
import java.util.HashMap;

public class EmailSendCommand {
	private String retailerId;
	private String smsEmailTriggerCode;
	private HashMap<String,String> parameters = new HashMap<String,String>();
	private String subject;
	private String recipient;
	private String serviceId;
	private ArrayList<String> ccRecipients;
	
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getSmsEmailTriggerCode() {
		return smsEmailTriggerCode;
	}
	public void setSmsEmailTriggerCode(String smsEmailTriggerCode) {
		this.smsEmailTriggerCode = smsEmailTriggerCode;
	}
	public HashMap<String,String> getParameters() {
		return parameters;
	}
	public void setParameters(HashMap<String,String> parameters) {
		this.parameters = parameters;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public ArrayList<String> getCcRecipients() {
		return ccRecipients;
	}
	public void setCcRecipients(ArrayList<String> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}
}
