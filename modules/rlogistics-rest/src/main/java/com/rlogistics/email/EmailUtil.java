package com.rlogistics.email;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.scripting.ScriptingEngines;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;

import org.activiti.engine.task.Task;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.LocationResult;
import com.rlogistics.master.identity.RetailerEmailConfiguration;
import com.rlogistics.master.identity.RetailerEmailConfiguration.RetailerEmailConfigurationQuery;
import com.rlogistics.master.identity.RetailerEmailTemplate;
import com.rlogistics.master.identity.RetailerEmailTemplate.RetailerEmailTemplateQuery;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate.RetailerSmsEmailTemplateQuery;
import com.rlogistics.master.identity.RetailerSmsEmailTriggerConfig;
import com.rlogistics.master.identity.SmsEmailLog;
import com.rlogistics.rest.FreeMarkerTemplateLoader;
import com.rlogistics.rest.ProcessResponseWithVariables;
import com.rlogistics.master.identity.ImeiInventory;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RLogisticsAttachment;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.Retailer.RetailerQuery;
import com.rlogistics.util.MapFieldBasedVariableScope;

import freemarker.template.Configuration;
import freemarker.template.Version;

public class EmailUtil {
	private static Logger log = LoggerFactory.getLogger(EmailUtil.class);
	private static String APP_LINK = "http://rl.bizlog.in/#/login"; // prod
	// private static String APP_LINK = "http://54.202.95.252/#/login"; //Demo

	public static String getRetailerName(String retailerId) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
		String retailerName = retailer.getName();
		return retailerName;

	}

	public static EmailSendResult sendEmail(EmailSendCommand emailSendCommand, String processId) {
		ByteArrayOutputStream debugOutputStream = new ByteArrayOutputStream();
		EmailSendResult emailSendResult = new EmailSendResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		StringWriter output = new StringWriter();
		int status = 0;
		String errorMessage = null;
		String useremail = null;
		String customeremail = "";
		try {
			emailSendResult.setCommand(emailSendCommand);

			// To be discussed, hence commented
			RetailerEmailConfigurationQuery configurationQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService().createRetailerEmailConfigurationQuery();

			RetailerEmailConfiguration configuration = configurationQuery.retailerId(emailSendCommand.getRetailerId())
					.singleResult();

			/*
			 * if(configuration == null){ emailSendResult.getMessages().add(
			 * "Email configuration missing for retailer. Please configure and then try."
			 * ); return emailSendResult; }
			 */

			if (emailSendCommand.getServiceId() != null) {
				RetailerSmsEmailTriggerConfig retailerSmsEmailTriggerConfig = masterdataService
						.createRetailerSmsEmailTriggerConfigQuery().retailerId(emailSendCommand.getRetailerId())
						.smsEmailTriggerCode(emailSendCommand.getSmsEmailTriggerCode())
						.serviceId(emailSendCommand.getServiceId()).singleResult();
				if (retailerSmsEmailTriggerConfig == null) {
					emailSendResult.getMessages().add("Retailer has not opted for receiving emails.");
					return emailSendResult;
				} else {
					if (retailerSmsEmailTriggerConfig.getIsEmailRequired() != 1) {
						emailSendResult.getMessages().add("Retailer has not opted for receiving emails.");
						return emailSendResult;
					}
				}
			}

			RetailerSmsEmailTemplateQuery templateQuery = masterdataService.createRetailerSmsEmailTemplateQuery();

			RetailerQuery retailerQuery = masterdataService.createRetailerQuery();
			Retailer retailer = retailerQuery.id(emailSendCommand.getRetailerId()).singleResult();

			RetailerSmsEmailTemplate template = templateQuery
					.smsEmailTriggerCode(emailSendCommand.getSmsEmailTriggerCode())
					.retailerId(emailSendCommand.getRetailerId()).singleResult();

			if (template == null) {
				emailSendResult.getMessages().add(
						"Email template for specified retailer with specified name is missing. Please configure and then try.");
				return emailSendResult;
			}

			Map<String, Object> root = new HashMap<>();

			Map<String, Object> variables = null;
			// ProcessResponseWithVariables processResponseWithVariables =
			// findByProcessId(processId);
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			variables = runtimeService.getVariables(processId);

			customeremail = String.valueOf(variables.get("emailId"));
			log.debug(customeremail);
			if (customeremail != "" && customeremail != null && customeremail != "null") {
				/*
				 * if(processResponseWithVariables != null) { variables =
				 * processResponseWithVariables.getVariables(); }
				 */

				// Execute javascript
				if (template.getParams() != null && !template.getParams().equals("")) {
					String params = template.getParams();
					String paramsArray[] = null;
					String script = "retval = new java.util.HashMap();";
					if (!params.contains(",")) {
						paramsArray = new String[1];
						paramsArray[0] = params;
					} else {
						paramsArray = params.split(",");
					}
					for (int i = 0; i < paramsArray.length; i++) {
						if (variables.containsKey(paramsArray[i])) {
							script += "retval.put(" + "\"" + paramsArray[i] + "\"" + "," + "\""
									+ String.valueOf(variables.get(paramsArray[i])) + "\"" + ");";
						} else {
							script += "retval.put(" + "\"" + paramsArray[i] + "\"" + "," + "\"" + "\"" + ");";
						}
						/*
						 * if(variables != null) {
						 * emailSendCommand.getParameters().put(paramsArray[i],
						 * String.valueOf(variables.get(paramsArray[i]))); }
						 */
					}
					String pincode = String.valueOf(variables.get("pincode"));
					List<LocationResult> locationResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
							.getContactDetails(pincode);

					if (locationResults.size() > 0) {
						LocationResult locationResult = locationResults.get(0);
						if (locationResult != null) {
							String number = locationResult.getPhoneNumber();
							useremail = locationResult.getEmail();
							log.debug("Location wise Phone and Email Number" + number + useremail);

							script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + number + "\"" + ");";
						} else {
							script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + "\"" + ");";
							useremail = "service@bizlog.in";
							log.debug("Inside Defult usermail ELSE");
						}
					} else {
						script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + "\"" + ");";
					}
					script += "retval;";

					final String executableScript = script;
					CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
							.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
					commandExecutor.execute(new Command<Boolean>() {
						@Override
						public Boolean execute(CommandContext commandContext) {
							ScriptingEngines scriptingEngines = ((ProcessEngineConfigurationImpl) (ProcessEngines
									.getDefaultProcessEngine().getProcessEngineConfiguration())).getScriptingEngines();
							MapFieldBasedVariableScope scope = new MapFieldBasedVariableScope();
							@SuppressWarnings("unchecked")
							Map<String, Object> root2 = (Map<String, Object>) scriptingEngines
									.evaluate(executableScript, "javascript", scope.getBindingsAdapter());
							if (root2 != null) {
								root.putAll(root2);
							}

							return true;
						}
					});

				}

				/*
				 * if(emailSendCommand.getParameters() != null){
				 * root.putAll(emailSendCommand.getParameters()); }
				 */
				if (template.getSubject() != null) {
					emailSendCommand.setSubject(template.getSubject());
				} else {
					emailSendCommand.setSubject("Info");
				}
				if (variables != null) {
					emailSendCommand.setRecipient(String.valueOf(variables.get("emailId")));
					log.debug("email ID" + String.valueOf(variables.get("emailId")));
				}
				String alternateEmailId = null;
				if (variables.get("alternateEmail") != null && !variables.get("alternateEmail").equals("")) {
					// emailSendCommand.setRecipient(String.valueOf(variables.get("alternateEmail")));
					// alternateEmailId =
					// String.valueOf(variables.get("alternateEmail"));
					log.debug("alternate email id" + String.valueOf(variables.get("alternateEmail")));
				}

				Configuration fmConfig = new Configuration(new Version(2, 3, 25));
				fmConfig.setTemplateLoader(new FreeMarkerTemplateLoader(template));

				Map<String, Object> sessionProperties = new HashMap<String, Object>();
				// if (false/*configuration != null*/) {
				/**
				 * Not in requirement hence commented
				 * 
				 * sessionProperties.put("mail.smtp.host",
				 * configuration.getHost());
				 * sessionProperties.put("mail.smtp.user",
				 * configuration.getUsername());
				 * sessionProperties.put("mail.smtp.password",
				 * configuration.getPassword());
				 * sessionProperties.put("mail.smtp.port",
				 * configuration.getPort()); if (configuration.getSocket() !=
				 * null) { if (configuration.getSocket().equals("TTLS")) {
				 * sessionProperties.put("mail.smtp.starttls.enable", "true"); }
				 * else { sessionProperties.put("mail.smtp.ssl.enable", "true");
				 * } } else { sessionProperties.put("mail.smtp.starttls.enable",
				 * "false"); sessionProperties.put("mail.smtp.ssl.enable",
				 * "false"); } }
				 */

				if (useremail != null) {
					sessionProperties.put("mail.smtp.host", "smtp.bizlog.in");
					sessionProperties.put("mail.smtp.user", useremail.toString());
					log.debug(useremail);
					sessionProperties.put("mail.smtp.password", "Welcome123$");
					sessionProperties.put("mail.smtp.port", "587");
					// sessionProperties.put("mail.smtp.ssl.enable","false");
					sessionProperties.put("mail.smtp.starttls.enable", "true");
					log.debug("Email Property Set For Location Wse Details");
				} else {
					sessionProperties.put("mail.smtp.host", "smtp.bizlog.in");
					sessionProperties.put("mail.smtp.user", "service@bizlog.in");
					sessionProperties.put("mail.smtp.password", "Welcome123$");
					sessionProperties.put("mail.smtp.port", "587");
					sessionProperties.put("mail.smtp.starttls.enable", "true");
					log.debug("Inside Defult else");
				}
				// sessionProperties.put("mail.smtp.starttls.enable", "true");

				sessionProperties.put("mail.smtp.auth", "true");
				sessionProperties.put("mail.smtp.ssl.trust", "smtp.bizlog.in");

				Session session = Session.getInstance(makeProperties(sessionProperties),
						new javax.mail.Authenticator() {
							@Override
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(sessionProperties.get("mail.smtp.user").toString(),
										sessionProperties.get("mail.smtp.password").toString());
								// String
								// uname=sessionProperties.containsKey(""));
								// log.debug(uname);
								// return new
								// PasswordAuthentication(uname,"Welcome123$");
							}
						});

				fmConfig.getTemplate(emailSendCommand.getSmsEmailTriggerCode()).process(root, output);
				output.flush();

				log.debug("Sending to:" + emailSendCommand.getRecipient());
				log.debug("Subject:" + emailSendCommand.getSubject());
				// log.debug("EMail Body:" + output.toString());

				emailSendResult.getMessages().add("Expansion:" + output.toString());

				/* Form a multipart alternative */
				MimeBodyPart htmlBodyPart = new MimeBodyPart();
				htmlBodyPart.setContent(output.toString(), "text/html; charset=utf-8");
				MimeBodyPart textBodyPart = new MimeBodyPart();/*
																 * For now use
																 * the same
																 * content for
																 * both text and
																 * html
																 */
				textBodyPart.setText(output.toString(), "utf-8");

				MimeMultipart alternativeMultipart = new MimeMultipart("alternative");
				alternativeMultipart.addBodyPart(textBodyPart);
				alternativeMultipart.addBodyPart(htmlBodyPart);

				/* Form a multipart mixed */
				MimeMultipart mixedMultipart = new MimeMultipart("mixed");

				MimeBodyPart alternativeChild = new MimeBodyPart();
				mixedMultipart.addBodyPart(alternativeChild);
				alternativeChild.setContent(alternativeMultipart);

				MimeMessage message = new MimeMessage(session);

				message.setContent(mixedMultipart);
				log.debug("recipant" + emailSendCommand.getRecipient());
				message.setRecipients(Message.RecipientType.TO, emailSendCommand.getRecipient());
				// message.setRecipients(Message.RecipientType.BCC,
				// retailer.getEmail());
				if (alternateEmailId != null) {
					message.setRecipients(Message.RecipientType.BCC, alternateEmailId);
				}
				if (emailSendCommand.getCcRecipients() != null) {
					message.setRecipients(Message.RecipientType.CC,
							String.join(",", emailSendCommand.getCcRecipients()));
				}
				message.setSubject(emailSendCommand.getSubject());
				message.setFrom(new InternetAddress(useremail.toString()));

				session.setDebug(true);
				session.setDebugOut(new PrintStream(debugOutputStream));

				Transport.send(message);
				status = 1;
				/**
				 * prem code sending message to medwell
				 */
				/*try {
					if (variables.get("rlTicketNo").toString().split("-")[0].equals("MDL")) {
						sendMailToMedwell(processId, variables, message);
					} else {
						Transport.send(message);
						status = 1;
					}
				} catch (Exception x) {
					x.printStackTrace();
				}*/
				/**
				 * prem code sending message to medwell
				 */

				debugOutputStream.flush();
				emailSendResult.getMessages().add("Debug:" + new String(debugOutputStream.toByteArray()));

			} else {
				status = 0;
				log.debug("Email Not Present");
				errorMessage = "Email is not present inside ticket";
			}
		} catch (Exception ex) {
			log.error("Exception during sendEmail", ex);
			emailSendResult.getMessages().add(ex.getMessage());
			errorMessage = ex.getMessage();
			String subject = "Sending Email Failed" + ": " + emailSendCommand.getSmsEmailTriggerCode();
			String content = "following message is failed to delivered: \n" + output.toString()
					+ "\n Due to following Reason:\n" + errorMessage;

			try {
				sendMail("service@bizlog.in", subject, content);
				debugOutputStream.flush();
			} catch (IOException | MessagingException e) {
			}
			emailSendResult.getMessages().add("Debug:" + new String(debugOutputStream.toByteArray()));

		}

		SmsEmailLog smsEmailLog = masterdataService.newSmsEmailLog();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
				+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
		smsEmailLog.setId(id);
		smsEmailLog.setProcessId(processId);
		smsEmailLog.setRecipient(emailSendCommand.getRecipient());
		smsEmailLog.setMessage(output.toString());
		smsEmailLog.setApiMessage("Message successfully sent");
		smsEmailLog.setType(1);
		if (status == 1) {
			smsEmailLog.setStatus("success");
		} else {
			smsEmailLog.setStatus("failed");
			smsEmailLog.setApiMessage(errorMessage);
		}
		masterdataService.saveSmsEmailLog(smsEmailLog);

		return emailSendResult;
	}

	private static void sendMailToMedwell(String processId, Map<String, Object> variables2, MimeMessage message2)
			throws IOException {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Retailer retailer = masterdataService.createRetailerQuery().id(variables2.get("rlRetailerId").toString())
				.singleResult();

		try {
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			Map<String, Object> variables = runtimeService.getVariables(processId);
			sendMail(retailer.getEmail().toLowerCase(),
					variables2.get("rlTicketNo") + ": Customer not responding after 3 time call...", "mail content");
		} catch (Exception e) {
			log.debug("Not bale to send mail to Medwell" + e.getMessage());
		}
	}

	protected static Map<String, Object> parseMap2(String fieldValuesJson) {
		Map<String, Object> retval = new HashMap<String, Object>();

		try {
			HashMap raw = new ObjectMapper().readValue(URLDecoder.decode(fieldValuesJson), HashMap.class);
			for (Object k : raw.keySet()) {
				retval.put(k.toString(), raw.get(k));
			}
		} catch (Exception ex) {
			throw new RuntimeException("Error parsing JSON", ex);
		}

		return retval;
	}

	protected static Properties makeProperties(Map<String, Object> map) {
		Properties retval = new Properties();

		for (String p : map.keySet()) {
			retval.put(p, map.get(p));
		}

		return retval;
	}

	public static boolean sendMail(String to, String subject, String content) throws IOException, MessagingException {
		Properties mailprop = getMailProperties();

		Runnable r = new Runnable() {

			@Override
			public void run() {
				try {
					Session session = Session.getInstance(mailprop, new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication("service@bizlog.in", "Welcome123$");
						}
					});
					try {
						MimeMessage message = new MimeMessage(session);
						message.setFrom(new InternetAddress(mailprop.getProperty("mail.from")));
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
						message.setSubject(subject);
						message.setText(content, "UTF-8", "html");
						Transport.send(message);
					} catch (MessagingException mex) {
						throw mex;
					}
				} catch (Exception e) {
					log.error("Exception sending mail :" + e.getMessage());
				}
			}
		};
		new Thread(r).start();

		return true;
	}

	public static boolean sendAttachmentMail(String processId, String variableName)
			throws IOException, MessagingException {
		Properties mailprop = getMailProperties();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processId)
				.includeProcessVariables().singleResult();
		int index = 0;
		byte[] content = null;

		Map<String, Object> variableValues = null;

		if (processInstance != null) {
			variableValues = processInstance.getProcessVariables();
			log.debug("Variable" + variableValues);
		} else {
			/* Look for a historical instance */
			HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getHistoryService();

			HistoricProcessInstance hiProcessInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(processId).includeProcessVariables().singleResult();

			if (hiProcessInstance != null) {
				variableValues = hiProcessInstance.getProcessVariables();
			}
		}

		/*
		 * String variableName = ""; for(String key : variableValues.keySet()) {
		 * if(key.indexOf("CustomerSignature") != -1) { log.debug(
		 * "Customer Signature present"); variableName = key; log.debug(
		 * "Variable Name : " + variableName); break; } }
		 */

		if (!variableName.equals("")) {
			Object value = variableValues.get(variableName);
			log.debug("Value" + value);
			String textValue = null;

			if (value instanceof ArrayList) {
				ArrayList<String> values = (ArrayList<String>) value;

				if (index < values.size()) {
					if (index < 0) {
						index = values.size() + index;
					}
					log.debug("Inside Array List");
					textValue = values.get(index);
				}
			} else if (value instanceof String) {
				textValue = (String) value;
			}
			log.debug("textValue" + " " + textValue + textValue.toString());
			content = DatatypeConverter.parseBase64Binary(textValue.replaceAll(" ", "+"));
			log.debug("content" + content + " " + content.toString());
		}

		String subject = "Customer Signature";
		String to = null;
		to = String.valueOf(variableValues.get("emailId"));

		try {
			Session session = Session.getInstance(mailprop, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("service@bizlog.in", "Welcome123$");
				}
			});
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(mailprop.getProperty("mail.from")));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				/*
				 * message.addRecipient(Message.RecipientType.CC, new
				 * InternetAddress("archana.sastry@sakhatech.com"));
				 * message.addRecipient(Message.RecipientType.CC, new
				 * InternetAddress("milap.nagar@sakhatech.com"));
				 */
				message.setSubject(subject);

				Multipart multipart = new MimeMultipart();

				// html part
				MimeBodyPart htmlPart = new MimeBodyPart();
				htmlPart.setContent(
						"Dear Customer, Please ensure that all the contents sent for pick up are seen by you physically and the attached signature is yours. If not kindly report back to us immediately. Thanking you.",
						"text/html");
				htmlPart.setDisposition(MimeBodyPart.INLINE);
				multipart.addBodyPart(htmlPart);

				// attachment part
				MimeBodyPart attach = new MimeBodyPart();
				ByteArrayDataSource ds = new ByteArrayDataSource(content, "image/png");
				log.debug("DataSource reached : " + ds);
				attach.setDataHandler(new DataHandler(ds));
				attach.setFileName("CustomerSignature.png");
				multipart.addBodyPart(attach);

				message.setContent(multipart);
				Transport.send(message);
				log.debug("Successfully sent - ");
			} catch (MessagingException mex) {
				throw mex;
			}
		} catch (Exception e) {
			log.error("Exception sending mail e :" + e.getMessage());
		}

		return true;
	}

	public static boolean sendMultipleAttachmentMail(String processId, String variableName)
			throws IOException, MessagingException {

		Properties mailprop = getMailProperties();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
				.getProcessEngineConfiguration())).getCommandExecutor();
		// ProcessInstance processInstance =
		// runtimeService.createProcessInstanceQuery().processInstanceId(processId).includeProcessVariables().singleResult();
		int index = 0;

		List<RLogisticsAttachment> attachments = new ArrayList<>();
		log.debug("processId : " + processId);
		log.debug("Variables : " + variableName);
		Map<String, Object> variables = runtimeService.getVariables(processId);
		System.out.println("After getting variables");

		if (variables != null) {
			attachments = masterdataService.createRLogisticsAttachmentQuery().key1("process").key2(processId)
					.key3(variableName).list();
		}
		System.out.println("After querying attachment table");
		log.debug("natore OF COMPLAINT : " + String.valueOf(variables.get("natureOfComplaint")));
		String subject = "Buyback Device Evaluation";
		if (String.valueOf(variables.get("natureOfComplaint")).equalsIgnoreCase("Buy Back")) {
			subject = "Buyback Device Evaluation";
		} else if (String.valueOf(variables.get("natureOfComplaint")).equalsIgnoreCase("RepairStandBy")) {
			subject = "Courtesy HandSet Damage Cost";
		} else {
			subject = "E-Waste Product Evaluation";
		}

		String to = null;
		log.debug("Recepient mail " + to);
		if (runtimeService.hasVariable(processId, "buyerEmailId")) {
			to = String.valueOf(runtimeService.getVariable(processId, "buyerEmailId"));
		} else {
			log.debug("Inside if condition");
			to = "vinay.murali@sakhatech.com";
		}
		log.debug("Recepient mail " + to);

		try {
			Session session = Session.getInstance(mailprop, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					log.debug("inside getPasswordAuthentication()");
					return new PasswordAuthentication("service@bizlog.in", "Welcome123$");
				}
			});
			log.debug("session : " + session);
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(mailprop.getProperty("mail.from")));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
				/*
				 * message.addRecipient(Message.RecipientType.CC, new
				 * InternetAddress("jp@sakhatech.com"));
				 * message.addRecipient(Message.RecipientType.CC, new
				 * InternetAddress("vinay.murali@sakhatech.com"));
				 */
				message.setSubject(subject);

				Multipart multipart = new MimeMultipart();

				// html part
				MimeBodyPart htmlPart = new MimeBodyPart();
				StringBuffer bodyBuffer = new StringBuffer("Hi,");
				bodyBuffer.append("<br/>");
				bodyBuffer.append("<br/>");
				if (String.valueOf(variables.get("natureOfComplaint")).equalsIgnoreCase("Buy Back")) {
					bodyBuffer.append(
							"Please login to view the device photos and evaluation results of the Field Engineer. Once you login, go to Ticket Status and provide the ticket number and search for the ticket and click on view");
				} else if (String.valueOf(variables.get("natureOfComplaint")).equalsIgnoreCase("RepairStandBy")) {
					bodyBuffer.append(
							"Please login to provide the Courtesy Handset damage cost. Once you login, go to Ticket Status and provide the ticket number and search for the ticket and click on view and provide the Damage Cost");
				} else {
					bodyBuffer.append(
							"Please login to review the product details. Once you login, go to Ticket Status and provide the ticket number and search for the ticket and click on view");
				}
				bodyBuffer.append("<br/>");
				bodyBuffer.append("<br/>");
				bodyBuffer.append(APP_LINK);
				bodyBuffer.append("<br/>");
				bodyBuffer.append("ConsumerName - " + String.valueOf(variables.get("consumerName")));
				bodyBuffer.append("<br/>");
				bodyBuffer.append("TicketNo - " + String.valueOf(variables.get("rlTicketNo")));
				bodyBuffer.append("<br/>");
				if (String.valueOf(variables.get("natureOfComplaint")).equalsIgnoreCase("RepairStandBy")) {
					bodyBuffer.append("Link: http://rl.bizlog.in/#/login");
				}
				bodyBuffer.append("<br/>");
				bodyBuffer.append("<br/>");
				bodyBuffer.append("Regards");

				// String bodyMessage = "Dear Customer, Please find the
				// attachments and verify the ticket details." + " " +
				// "ConsumerName - " +
				// String.valueOf(variables.get("consumerName")) + ", " +
				// "TicketNo - " + String.valueOf(variables.get("rlTicketNo")) +
				// ", " + "Offered Cost - " +
				// String.valueOf(variables.get("rlValueOffered")) + ". " +
				// "Click on the link to login - " + APP_LINK;
				htmlPart.setContent(bodyBuffer.toString(), "text/html");
				htmlPart.setDisposition(MimeBodyPart.INLINE);
				multipart.addBodyPart(htmlPart);

				System.out.println("Before iterating and attaching files");

				// attachment part

				for (RLogisticsAttachment attachment : attachments) {
					System.out.println("iteration - " + attachment.getKey4());
					log.debug("iteration - " + attachment.getKey4());
					MimeBodyPart attach = new MimeBodyPart();
					System.out.println("Mime object " + attach);
					System.out.println("Content-Tye " + attachment.getContentType());
					log.debug("Mime object " + attach);
					log.debug("Content-Tye " + attachment.getContentType());
					commandExecutor.execute(new Command<Boolean>() {
						@Override
						public Boolean execute(CommandContext commandContext) {
							byte[] content = attachment.getContent();
							System.out.println("Content : " + content);
							log.debug("Content : " + content);
							ByteArrayDataSource ds = new ByteArrayDataSource(content, attachment.getContentType());
							System.out.println("Datasource " + ds);
							System.out.println("Before setting handler");
							try {
								attach.setDataHandler(new DataHandler(ds));
							} catch (Exception m) {

							}

							return true;
						}
					});
					attach.setFileName(attachment.getName());
					System.out.println("before setting attach to multipart");
					log.debug("before setting attach to multipart");
					multipart.addBodyPart(attach);
				}

				// Attach Evaluation File
				/*
				 * try { RLogisticsAttachment attachment =
				 * masterdataService.createRLogisticsAttachmentQuery().key1(
				 * "evaluationFile").key2(processId).singleResult();
				 * if(attachment != null) { MimeBodyPart attach = new
				 * MimeBodyPart(); commandExecutor.execute(new
				 * Command<Boolean>() {
				 *
				 * @Override public Boolean execute(CommandContext
				 * commandContext) { byte[] content = attachment.getContent();
				 * ByteArrayDataSource ds = new ByteArrayDataSource(content,
				 * attachment.getContentType());
				 *
				 * try { attach.setDataHandler(new DataHandler(ds)); } catch
				 * (Exception m) {
				 *
				 * }
				 *
				 * return true; } }); attach.setFileName(attachment.getName());
				 * System.out.println("before setting attach to multipart");
				 * multipart.addBodyPart(attach); } } catch(Exception a) {
				 *
				 * }
				 */

				System.out.println("before setting multipart to message");
				log.debug("before setting multipart to message");
				message.setContent(multipart);
				System.out.println("Before sending mail");
				log.debug("Before sending mail");
				Transport.send(message);
				System.out.println("Successfully sent - ");
				log.debug("Successfully sent - ");
			} catch (MessagingException mex) {
				throw mex;
			}
		} catch (Exception e) {
			log.error("Exception sending mail e :" + e.getMessage());
		}

		return true;
	}

	protected static Properties getMailProperties() {
		Properties retval = new Properties();

		retval.setProperty("mail.smtp.auth", "true");
		retval.setProperty("mail.smtp.starttls.enable", "true");
		retval.setProperty("mail.smtp.host", "smtp.bizlog.in");
		retval.setProperty("mail.smtp.port", "587");
		retval.setProperty("mail.from", "service@bizlog.in");
		retval.setProperty("mail.smtp.ssl.trust", "smtp.bizlog.in");
		return retval;
	}

	public static ProcessResponseWithVariables findByProcessId(String processId) {

		try {
			Task task = null;
			ProcessResponseWithVariables processResultWithVariables = null;
			ProcessInstance processInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();

			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();

			if (processInstance != null) {
				List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getTaskService().createTaskQuery().processInstanceId(processId).includeProcessVariables()
						.orderByTaskCreateTime().desc().list();

				if (!tasks.isEmpty()) {
					task = tasks.get(0);
					processResultWithVariables = new ProcessResponseWithVariables(processInstance, task,
							runtimeService.getVariables(processId)

					);
				} else {

					ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getManagementService();

					List<Job> jobs = managementService.createJobQuery()
							.processInstanceId(processInstance.getProcessInstanceId()).list();

					String jobId = null;
					/* TODO Handle if more than one job */
					if (!jobs.isEmpty()) {
						jobId = jobs.get(0).getId();
					}

					processResultWithVariables = new ProcessResponseWithVariables(processInstance, jobId,
							runtimeService.getVariables(processId)

					);
				}
			} else {

				HistoricProcessInstance historicProcessInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery()
								.processInstanceId(processId).includeProcessVariables().singleResult();

				if (historicProcessInstance != null) {
					processResultWithVariables = new ProcessResponseWithVariables(historicProcessInstance,

							historicProcessInstance.getProcessVariables()

					);

					processResultWithVariables.setCompleted(true);
				} else {
					processResultWithVariables = new ProcessResponseWithVariables(

							runtimeService.getVariables(processId)

					);

					log.error("Process Id " + processId + " is neither present not past. It is missing.");
				}
			}

			return processResultWithVariables;
		} catch (Exception ex) {
			log.error("Exception during listInvolvedTasks", ex);
			return null;
		}
	}

	public static String getModelNameFromIMEINo(String valueOf) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		ImeiInventory imeiinventory = masterdataService.createImeiInventoryQuery().imeiNo(valueOf).singleResult();
		String model = imeiinventory.getModel();
		return model;
	}
}
