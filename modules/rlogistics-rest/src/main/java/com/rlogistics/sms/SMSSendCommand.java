package com.rlogistics.sms;

import java.util.HashMap;

public class SMSSendCommand {
	private String recipient;
	private String testMode = "true";
	private String retailerId;
	private String serviceId;
	private String smsEmailTriggerCode;
	private HashMap<String,String> parameters = new HashMap<String,String>();

	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getTestMode() {
		return testMode;
	}
	public void setTestMode(String testMode) {
		this.testMode = testMode;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getSmsEmailTriggerCode() {
		return smsEmailTriggerCode;
	}
	public void setSmsEmailTriggerCode(String smsEmailTriggerCode) {
		this.smsEmailTriggerCode = smsEmailTriggerCode;
	}
	public HashMap<String, String> getParameters() {
		return parameters;
	}
	public void setParameters(HashMap<String, String> parameters) {
		this.parameters = parameters;
	}
}
