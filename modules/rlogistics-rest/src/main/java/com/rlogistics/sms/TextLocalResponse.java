package com.rlogistics.sms;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TextLocalResponse {
	private String status;
	private ArrayList<TextLocalResponseMessage> errors;
	private ArrayList<TextLocalResponseMessage> warnings;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<TextLocalResponseMessage> getErrors() {
		return errors;
	}

	public void setErrors(ArrayList<TextLocalResponseMessage> errors) {
		this.errors = errors;
	}

	public ArrayList<TextLocalResponseMessage> getWarnings() {
		return warnings;
	}

	public void setWarnings(ArrayList<TextLocalResponseMessage> warnings) {
		this.warnings = warnings;
	}
}
