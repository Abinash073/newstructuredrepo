package com.rlogistics.sms;

import java.util.ArrayList;
import java.util.List;

public class SMSSendResult {

	private List<String> messages = new ArrayList<>();
	private SMSSendCommand command;
	private String rawResponse;
	private TextLocalResponse textLocalResponse;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TextLocalResponse getTextLocalResponse() {
		return textLocalResponse;
	}

	public void setTextLocalResponse(TextLocalResponse textLocalResponse) {
		this.textLocalResponse = textLocalResponse;
	}


	public SMSSendResult() {
		this.setCommand(command);
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public SMSSendCommand getCommand() {
		return command;
	}

	public void setCommand(SMSSendCommand command) {
		this.command = command;
	}

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}


}
