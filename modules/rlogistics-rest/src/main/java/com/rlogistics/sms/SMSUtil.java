package com.rlogistics.sms;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.scripting.ScriptingEngines;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.amazonaws.endpointdiscovery.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.LocationResult;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RetailerSmsConfiguration;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;
import com.rlogistics.master.identity.RetailerSmsEmailTriggerConfig;
import com.rlogistics.master.identity.SmsEmailLog;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate.RetailerSmsEmailTemplateQuery;
import com.rlogistics.rest.FreeMarkerTemplateLoader;
import com.rlogistics.rest.FreeMarkerTemplateLoaderSms;
import com.rlogistics.rest.ProcessResponseWithVariables;
import com.rlogistics.util.MapFieldBasedVariableScope;
import com.rlogistics.email.EmailUtil;
import freemarker.template.Configuration;
import freemarker.template.Version;

public class SMSUtil {

	private static Logger log = LoggerFactory.getLogger(SMSUtil.class);
	private static String TEXT_LOCAL_USERNAME = "techinfo@bizlog.in";
	private static String TEXT_LOCAL_HASH_KEY = "c0fc91e6ff4974aabe0d49173bbb34fcfe103a148a185590fb2488b4a7a21b58";

	public static SMSSendResult sendSMS(SMSSendCommand smsSendCommand, String processId) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        log.debug("Command" + smsSendCommand.getParameters().toString() + " " + smsSendCommand.getRetailerId() + smsSendCommand.getSmsEmailTriggerCode());
        int status = 0;
        StringWriter output = new StringWriter();

        SMSSendResult result = new SMSSendResult();
        String smsTemplate = "";

        TextLocalResponse textLocalResponse = null;
        String rawResponse = null;

        result.setCommand(smsSendCommand);

        CloseableHttpClient client = HttpClients.createDefault();

        HttpPost post = new HttpPost("http://api.textlocal.in/send/?");

        /*
         * RetailerSmsConfiguration retailerSmsConfiguration =
         * masterdataService.createRetailerSmsConfigurationQuery().retailerId(
         * smsSendCommand.getRetailerId()).singleResult();
         * if(retailerSmsConfiguration != null) { result.getMessages().add(
         * "Sms configuration missing foTr retailer. Please configure and then try."
         * ); return result; }
         */

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("test", "false"));//smsSendCommand.getTestMode()));
        nvps.add(new BasicNameValuePair("username", TEXT_LOCAL_USERNAME));
        nvps.add(new BasicNameValuePair("hash", TEXT_LOCAL_HASH_KEY));
        nvps.add(new BasicNameValuePair("sender", "BZINFO"));
        // nvps.add(new
        // BasicNameValuePair("message",smsSendCommand.getMessage()));

        if (smsSendCommand.getRetailerId() != null) {

            if (smsSendCommand.getServiceId() != null) {
                RetailerSmsEmailTriggerConfig retailerSmsEmailTriggerConfig = masterdataService
                        .createRetailerSmsEmailTriggerConfigQuery().retailerId(smsSendCommand.getRetailerId())
                        .smsEmailTriggerCode(smsSendCommand.getSmsEmailTriggerCode())
                        .serviceId(smsSendCommand.getServiceId()).singleResult();
                if (retailerSmsEmailTriggerConfig == null) {
                    result.getMessages().add("Retailer has not opted for receiving sms.");
                    return result;
                } else {
                    if (retailerSmsEmailTriggerConfig.getIsSmsRequired() != 1) {
                        result.getMessages().add("Retailer has not opted for receiving sms.");
                        return result;
                    }
                }
            }

            RetailerSmsEmailTemplateQuery templateQuery = masterdataService.createRetailerSmsEmailTemplateQuery();

            RetailerSmsEmailTemplate template = templateQuery
                    .smsEmailTriggerCode(smsSendCommand.getSmsEmailTriggerCode())
                    .retailerId(smsSendCommand.getRetailerId()).singleResult();

            if (template == null) {
                result.getMessages().add(
                        "Sms template for specified RETAILER with specified name is missing. Please configure and then try.");
                return result;
            }

            Map<String, Object> root = new HashMap<>();

            Map<String, Object> variables = null;
            ProcessResponseWithVariables processResponseWithVariables = findByProcessId(processId);
            if (processResponseWithVariables != null) {
                variables = processResponseWithVariables.getVariables();
            }

            // Execute javascrip+t
            if (template.getParams() != null && !template.getParams().equals("")) {
                String params = template.getParams();
                String paramsArray[] = null;
                String script = "retval = new java.util.HashMap();";
                if (!params.contains(",")) {
                    paramsArray = new String[1];
                    paramsArray[0] = params;
                } else {
                    paramsArray = params.split(",");
                }

                smsTemplate = template.getSmsTemplate();
                smsTemplate = smsTemplate.replace("\\{", "\b");
                smsTemplate = smsTemplate.replace("\\}", "\b");
                for (int i = 0; i < paramsArray.length; i++) {
                    if (variables != null) {

                        if (paramsArray[i].equalsIgnoreCase("retailerId")) {
                            log.debug("Inside retailerId from SMSMS : : : : :" + paramsArray[i]);
                            log.debug("Retailer aginst whom sms send" + String.valueOf(variables.get(paramsArray[i])));

                            String retailerName = EmailUtil.getRetailerName(String.valueOf(variables.get("rlRetailerId")));
                            if ((retailerName.equalsIgnoreCase("One Assist Consumer solutions Private Limited")) ||
                                    (retailerName.equalsIgnoreCase("OneAssist"))) {
                                log.debug("retailerName : " + retailerName);
                                script = script + "retval.put(\"retailerId\",\"" + retailerName + "\");";
                            } else if ((retailerName.equalsIgnoreCase("Demo Retailer"))) {
                                log.debug("retailerName"+ retailerName);
                                script = script + "retval.put(\"retailerId\",\"" + retailerName + "\");";
                            } else {
                                log.debug("retailerName : " + retailerName);
                                script = script + "retval.put(\"retailerId\",\"BizLog\");";
                            }
                        } else if (paramsArray[i].equalsIgnoreCase("IMEINoOfStandByDevice")) {
                            log.debug("Inside SMSSM IMEINoOfStandByDevice : : : : :" + paramsArray[i]);
                            log.debug("IMEI no For SMS:::::" + String.valueOf(variables.get(paramsArray[i])));

                            String model = EmailUtil.getModelNameFromIMEINo(String.valueOf(variables.get(paramsArray[i])));
                            if (variables.containsKey(paramsArray[i])) {
                                script = script + "retval.put(\"" + paramsArray[i] + "\",\"" + model + "\");";

                            } else {

                                script = script + "retval.put(\"" + paramsArray[i] + "\",\"NA\");";

                            }


                        } else if ((paramsArray[i].equalsIgnoreCase("rlAppointmentDateForFV")) ||
                                (paramsArray[i].equalsIgnoreCase("rlAppointmentDateForDelvry"))) {
                            try {
                                log.debug("var name :::::" + paramsArray[i]);
                                log.debug("var String Value :::::" + String.valueOf(variables.get(paramsArray[i])));
                                log.debug("var  Value :::::" + variables.get(paramsArray[i]));
                                long l = Long.parseLong(String.valueOf(variables.get(paramsArray[i])));
                                Timestamp stamp = new Timestamp(l);
                                Date date1 = new Date(stamp.getTime());
                                log.debug("date1 : " + date1);
                                String date2 = date1.toString();
                                log.debug("date2 : " + date2);
                                String date = date2.substring(0, 19);
                                log.debug("String Date :" + date);
                                if (variables.containsKey(paramsArray[i])) {
                                    script = script + "retval.put(\"" + paramsArray[i] + "\",\"" + date + "\");";
                                } else {
                                    script = script + "retval.put(\"" + paramsArray[i] + "\",\"\");";
                                }
                            } catch (Exception e) {
                                log.debug("Some error in conversion date :" + e);
                            }
                        } else if (variables.containsKey(paramsArray[i])) {
                            script = script + "retval.put(\"" + paramsArray[i] + "\",\"" + String.valueOf(variables.get(paramsArray[i])) + "\");";

                        } else {

                            script = script + "retval.put(\"" + paramsArray[i] + "\",\"\");";
                        }
                    }
                }
                String pincode = String.valueOf(variables.get("pincode"));
                List<LocationResult> locationResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .getContactDetails(pincode);
                if (locationResults.size() > 0) {
                    LocationResult locationResult = locationResults.get(0);
                    if (locationResult != null) {
                        String number = locationResult.getPhoneNumber();
                        log.debug(number);
                        script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + number + "\""
                                + ");";
                    } else {
                        script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + "8722899736" + "\"" + ");";
                    }
                } else {
                    script += "retval.put(" + "\"" + "phoneNumber" + "\"" + "," + "\"" + "8722899736" + "\"" + ");";
                }
                script += "retval;";

                final String executableScript = script;
                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        ScriptingEngines scriptingEngines = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getScriptingEngines();
                        MapFieldBasedVariableScope scope = new MapFieldBasedVariableScope();
                        @SuppressWarnings("unchecked")
                        Map<String, Object> root2 = (Map<String, Object>) scriptingEngines.evaluate(executableScript, "javascript", scope.getBindingsAdapter());
                        if (root2 != null) {
                            root.putAll(root2);
                        }

                        return true;
                    }
                });
            }

            if (smsSendCommand.getParameters() != null) {
                root.putAll(smsSendCommand.getParameters());
            }
            if (variables != null) {
                smsSendCommand.setRecipient(String.valueOf(variables.get("telephoneNumber")));
            }

            Configuration fmConfig = new Configuration(new Version(2, 3, 25));
            fmConfig.setTemplateLoader(new FreeMarkerTemplateLoaderSms(template));

            try {
                fmConfig.getTemplate(smsSendCommand.getSmsEmailTriggerCode()).process(root, output);
                output.flush();
            } catch (Exception ep) {

            }
            log.debug(output.toString());

            nvps.add(new BasicNameValuePair("numbers", smsSendCommand.getRecipient()));

            nvps.add(new BasicNameValuePair("message", output.toString()));
            result.setMessage(output.toString());
        } else {
            byte[] bytes = new byte[1024];

            nvps.add(new BasicNameValuePair("message", "Hello"));
        }

        try {
            post.setEntity(new UrlEncodedFormEntity(nvps));
        } catch (Exception ex) {
            log.error("Failed to convert message into a form entity:" + ex.getMessage(), ex);
            result.getMessages().add("Failed to convert message into a form entity:" + ex.getMessage());
        }

        CloseableHttpResponse response = null;
        try {
            log.debug("Request to SMS API:" + EntityUtils.toString(post.getEntity()));
            response = client.execute(post);
        } catch (Exception ex) {
            log.error("Failed to send message:" + ex.getMessage(), ex);
            result.getMessages().add("Failed to send message:" + ex.getMessage());
        }

        try {
            rawResponse = EntityUtils.toString(response.getEntity());

            log.debug("Response from SMS API:" + rawResponse);

            result.setRawResponse(rawResponse);

            ObjectMapper mapper = new ObjectMapper();
            textLocalResponse = mapper.readValue(URLDecoder.decode(rawResponse), TextLocalResponse.class);

            result.setTextLocalResponse(textLocalResponse);

            if (textLocalResponse.getStatus().equalsIgnoreCase("failure")) {
                if (textLocalResponse.getWarnings() != null) {
                    for (TextLocalResponseMessage message : textLocalResponse.getWarnings()) {
                        log.debug("Warnings" + message.getMessage());
                        result.getMessages().add(message.getMessage());
                    }
                } else if (textLocalResponse.getErrors() != null) {
                    for (TextLocalResponseMessage message : textLocalResponse.getErrors()) {
                        log.debug("Errors" + message.getMessage());
                        result.getMessages().add(message.getMessage());
                    }
                }
            } else {
                status = 1;
            }

        } catch (Exception ex) {
            log.error("Failed to read response:" + ex.getMessage(), ex);
            result.getMessages().add("Failed to read response:" + ex.getMessage());
        } finally {
            try {
                response.close();
            } catch (IOException e) {
            }
        }
        SmsEmailLog smsEmailLog = masterdataService.newSmsEmailLog();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
                + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
        smsEmailLog.setId(id);
        smsEmailLog.setProcessId(processId);
        smsEmailLog.setRecipient(smsSendCommand.getRecipient());
        smsEmailLog.setMessage(output.toString());
        smsEmailLog.setType(2);
        if (status == 1) {
            smsEmailLog.setStatus("success");
        } else {
            smsEmailLog.setStatus("failed");
        }
        smsEmailLog.setApiMessage(rawResponse);
        masterdataService.saveSmsEmailLog(smsEmailLog);

        return result;
    }

	/*
	 * find by process id
	 * 
	 * 
	 */
	private static ProcessResponseWithVariables findByProcessId(String processId) {

		try {
			Task task = null;
			ProcessResponseWithVariables processResultWithVariables = null;
			ProcessInstance processInstance = ProcessEngines.getDefaultProcessEngine().getRuntimeService()
					.createProcessInstanceQuery().processInstanceId(processId).singleResult();

			RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();

			if (processInstance != null) {
				List<Task> tasks = ProcessEngines.getDefaultProcessEngine().getTaskService().createTaskQuery()
						.processInstanceId(processId).includeProcessVariables().orderByTaskCreateTime().desc().list();

				if (!tasks.isEmpty()) {
					task = tasks.get(0);
					processResultWithVariables = new ProcessResponseWithVariables(processInstance, task,
							runtimeService.getVariables(processId)

					);
				} else {

					ManagementService managementService = ProcessEngines.getDefaultProcessEngine()
							.getManagementService();

					List<Job> jobs = managementService.createJobQuery()
							.processInstanceId(processInstance.getProcessInstanceId()).list();

					String jobId = null;
					if (!jobs.isEmpty()) {
						jobId = jobs.get(0).getId();
					}

					processResultWithVariables = new ProcessResponseWithVariables(processInstance, jobId,
							runtimeService.getVariables(processId)

					);
				}
			} else {

				HistoricProcessInstance historicProcessInstance = ProcessEngines.getDefaultProcessEngine()
						.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(processId)
						.includeProcessVariables().singleResult();

				if (historicProcessInstance != null) {
					processResultWithVariables = new ProcessResponseWithVariables(historicProcessInstance,

							historicProcessInstance.getProcessVariables()

					);

					processResultWithVariables.setCompleted(true);
				} else {
					processResultWithVariables = new ProcessResponseWithVariables(

							runtimeService.getVariables(processId)

					);

					log.error("Process Id " + processId + " is neither present not past. It is missing.");
				}
			}

			return processResultWithVariables;
		} catch (Exception ex) {
			log.error("Exception during listInvolvedTasks", ex);
			return null;
		}
	}

	public static boolean sendSMSSingle(String message, String number) {

		CloseableHttpClient client = HttpClients.createDefault();

		HttpPost post = new HttpPost("http://api.textlocal.in/send/?");

		/*
		 * RetailerSmsConfiguration retailerSmsConfiguration =
		 * masterdataService.createRetailerSmsConfigurationQuery().retailerId(
		 * smsSendCommand.getRetailerId()).singleResult();
		 * if(retailerSmsConfiguration != null) { result.getMessages().add(
		 * "Sms configuration missing for retailer. Please configure and then try."
		 * ); return result; }
		 */

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("test", "false"));
		nvps.add(new BasicNameValuePair("username", TEXT_LOCAL_USERNAME));
		nvps.add(new BasicNameValuePair("hash", TEXT_LOCAL_HASH_KEY));
		nvps.add(new BasicNameValuePair("sender", "BZINFO"));
		nvps.add(new BasicNameValuePair("numbers", number));
		nvps.add(new BasicNameValuePair("message", message));

		try {
			post.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (Exception ex) {
			log.error("Failed to convert message into a form entity:" + ex.getMessage(), ex);

		}

		CloseableHttpResponse response = null;
		try {
			log.debug("Request to SMS API:" + EntityUtils.toString(post.getEntity()));
			response = client.execute(post);
			log.debug("Response from SMS API " + EntityUtils.toString(response.getEntity()));
		} catch (Exception ex) {
			log.error("Failed to send message:" + ex.getMessage(), ex);

		}
		return true;
	}

	// This URL is Only for Checking Purpose To Send SMS
	// @RequestMapping(value = "/tasks/sendsms", produces = "application/json")
	// public boolean getSMS(HttpServletRequest httpRequest, HttpServletResponse
	// httpResponse,
	// @RequestParam("message") String message, @RequestParam("number") String
	// number) {
	// k
	// String number1 = number;
	// String msg = message;
	// System.out.println(sendSMSSingle(msg, number1));
	//// CloseableHttpClient client = HttpClients.createDefault();
	////
	//// HttpPost post = new HttpPost("http://api.textlocal.in/send/?");
	////
	//// /*RetailerSmsConfiguration retailerSmsConfiguration =
	// masterdataService.createRetailerSmsConfigurationQuery().retailerId(smsSendCommand.getRetailerId()).singleResult();
	//// if(retailerSmsConfiguration != null) {
	//// result.getMessages().add("Sms configuration missing for retailer.
	// Please configure and then try.");
	//// return result;
	//// }*/
	////
	//// List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	//// nvps.add(new BasicNameValuePair("test","false"));
	//// nvps.add(new BasicNameValuePair("username","Srini@bizlog.in"));
	//// nvps.add(new
	// BasicNameValuePair("hash","8500d2ba603f193b48a5ec98d6a592fa59581cfd"));
	//// nvps.add(new BasicNameValuePair("sender","BZINFO"));
	//// nvps.add(new BasicNameValuePair("numbers", number));
	//// nvps.add(new BasicNameValuePair("message",message));
	////
	//// try {
	//// post.setEntity(new UrlEncodedFormEntity(nvps));
	//// } catch (Exception ex) {
	//// log.error("Failed to convert message into a form entity:" +
	// ex.getMessage(),ex);
	////
	//// }
	////
	//// CloseableHttpResponse response = null;
	//// try {
	//// log.debug("Request to SMS API:" +
	// EntityUtils.toString(post.getEntity()));
	//// response = client.execute(post);
	//// log.debug("Response from SMS API " +
	// EntityUtils.toString(response.getEntity()));
	//// } catch (Exception ex) {
	//// log.error("Failed to send message:" + ex.getMessage(),ex);
	////
	//// }
	// return true;
	// }
}
