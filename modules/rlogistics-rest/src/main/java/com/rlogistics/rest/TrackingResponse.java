package com.rlogistics.rest;

public class TrackingResponse {
	private String location;
	private String type;
	private String refNo;
	private String conNo;
	private String conNoDate;
	private String conNoTime;
	private String mfNo;
	private String cityName;
	private String status;
	private String relationShip;
	private String remarks;
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getConNo() {
		return conNo;
	}
	public void setConNo(String conNo) {
		this.conNo = conNo;
	}
	public String getConNoDate() {
		return conNoDate;
	}
	public void setConNoDate(String conNoDate) {
		this.conNoDate = conNoDate;
	}
	public String getConNoTime() {
		return conNoTime;
	}
	public void setConNoTime(String conNoTime) {
		this.conNoTime = conNoTime;
	}
	public String getMfNo() {
		return mfNo;
	}
	public void setMfNo(String mfNo) {
		this.mfNo = mfNo;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
