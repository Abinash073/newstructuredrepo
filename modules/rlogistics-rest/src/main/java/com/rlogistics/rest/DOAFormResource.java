package com.rlogistics.rest;

import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.util.AttachmentUtil;

@RestController
public class DOAFormResource extends RLogisticsResource {

	private static Logger log = LoggerFactory.getLogger(RLogisticsAttachmentResource.class);

	@RequestMapping(value = "/doa-form/upload", produces = "application/json")
	public AttachmentUploadResult upload(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "categoryId") String categoryId, @RequestParam(value = "brandId") String brandId) {
		AttachmentUploadResult result = new AttachmentUploadResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			result = AttachmentUtil.upload(file, "DOA", categoryId, brandId);
		} catch (Exception ex) {
			log.error("Exception during upload", ex);
			result.getMessages().add(ex.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/auth-letter/upload", produces = "application/json")
	public AttachmentUploadResult uploadAuthLetter(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "processId") String processId) {
		AttachmentUploadResult result = new AttachmentUploadResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			String fileName = file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf('.'));
			fileName = fileName.replaceAll(" ", "");
			result = AttachmentUtil.upload(file, "AuthLetter", processId, fileName);
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			runtimeService.setVariable(processId, "rlAuthorizationLetterFileName", fileName);
		} catch (Exception ex) {
			log.error("Exception during upload", ex);
			result.getMessages().add(ex.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/purchase-invoice/upload", produces = "application/json")
	public AttachmentUploadResult uploadPurchaseInvoice(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "processId") String processId) {
		AttachmentUploadResult result = new AttachmentUploadResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			String fileName = file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf('.'));
			fileName = fileName.replaceAll(" ", "");
			result = AttachmentUtil.upload(file, "PurchaseInvoice", processId, fileName);
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			runtimeService.setVariable(processId, "rlPurchaseInvoiceFileName", fileName);
		} catch (Exception ex) {
			log.error("Exception during upload", ex);
			result.getMessages().add(ex.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/get/evaluation-result", produces = "application/json")
	public RestResult getEvaluationResult(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
//		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
//			return null;
//		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String URL=getValidataBaseUrl();
			String url = URL+"/biblog-validata/resutlts/all?processId="+processId;
//			String url = "http://www.validata.in/admin/user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {
				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				System.out.println("REsponse from server : "+resp);

				ArrayList<Map<String, Object>> custResponse = (ArrayList<Map<String, Object>>) resp
						.get("customer_response");
				System.out.println("custresp" + custResponse);
				for (Map<String, Object> custObj : custResponse) {
					String answers = String.valueOf(custObj.get("answers"));
					if (answers != null) {
						if (String.valueOf(custObj.get("question_type")).equalsIgnoreCase("Options")) {
							ArrayList<String> modifiedAnswers = new ArrayList<String>();
							String[] answerStrArr = answers.split(",");
							String selectedAnswer = String.valueOf(custObj.get("selected_answer"));
							if (selectedAnswer.equalsIgnoreCase("A")) {
								selectedAnswer = answerStrArr[0];
							} else if (selectedAnswer.equalsIgnoreCase("B")) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("C") && answerStrArr.length == 2) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("C")) {
								selectedAnswer = answerStrArr[2];
							} else if (selectedAnswer.equalsIgnoreCase("D") && answerStrArr.length == 2) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("D")) {
								selectedAnswer = answerStrArr[3];
							}
							selectedAnswer = selectedAnswer.trim();
							if (selectedAnswer.indexOf("=>") != -1) {
								int index = selectedAnswer.indexOf("=>");
								selectedAnswer = selectedAnswer.substring(index + 2).trim();
							}
							if (selectedAnswer.indexOf("=") != -1) {
								int index = selectedAnswer.indexOf("=");
								selectedAnswer = selectedAnswer.substring(index + 1).trim();
							}

							for (int a = 0; a < answerStrArr.length; a++) {
								String singleAnswer = answerStrArr[a];
								singleAnswer = singleAnswer.replaceAll(" ", "");
								modifiedAnswers.add(singleAnswer);
							}
							custObj.put("answers", modifiedAnswers);
							custObj.put("selected_answer", selectedAnswer);
						}

					}
				}

				ArrayList<Map<String, Object>> egnrResponse = (ArrayList<Map<String, Object>>) resp
						.get("engineer_response");
				for (Map<String, Object> egnrObj : egnrResponse) {
					String answers = String.valueOf(egnrObj.get("answers"));
					if (answers != null) {
						if (String.valueOf(egnrObj.get("question_type")).equalsIgnoreCase("Options")) {
							ArrayList<String> modifiedAnswers = new ArrayList<String>();
							String[] answerStrArr = answers.split(",");
							String selectedAnswer = String.valueOf(egnrObj.get("selected_answer"));
							if (selectedAnswer.equalsIgnoreCase("A")) {
								selectedAnswer = answerStrArr[0];
							} else if (selectedAnswer.equalsIgnoreCase("B")) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("C") && answerStrArr.length == 2) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("C")) {
								selectedAnswer = answerStrArr[2];
							} else if (selectedAnswer.equalsIgnoreCase("D") && answerStrArr.length == 2) {
								selectedAnswer = answerStrArr[1];
							} else if (selectedAnswer.equalsIgnoreCase("D")) {
								selectedAnswer = answerStrArr[3];
							}
							selectedAnswer = selectedAnswer.trim();
							if (selectedAnswer.indexOf("=>") != -1) {
								int index = selectedAnswer.indexOf("=>");
								selectedAnswer = selectedAnswer.substring(index + 2).trim();
							}
							if (selectedAnswer.indexOf("=") != -1) {
								int index = selectedAnswer.indexOf("=");
								selectedAnswer = selectedAnswer.substring(index + 1).trim();
							}

							for (int a = 0; a < answerStrArr.length; a++) {
								String singleAnswer = answerStrArr[a];
								singleAnswer = singleAnswer.replaceAll(" ", "");
								modifiedAnswers.add(singleAnswer);
							}
							egnrObj.put("answers", modifiedAnswers);
							egnrObj.put("selected_answer", selectedAnswer);
						}
					}
				}
				
				// ArrayList<Map<String,String>> finalResponse = new
				// ArrayList<Map<String,String>>();
				// for(int i=0; i<custResponse.size();i++) {
				// Map<String,String> finalObj = new HashMap<String, String>();
				// Map<String,Object> custSingleObj = custResponse.get(i);
				// finalObj.put("question",
				// String.valueOf(custSingleObj.get("question")));
				// finalObj.put("cust_selected_answer",
				// String.valueOf(custSingleObj.get("selected_answer")));
				// if(egnrResponse.size()>0) {
				// Map<String, Object> engSingleObj = egnrResponse.get(i);
				// finalObj.put("eng_selected_answer",
				// String.valueOf(engSingleObj.get("selected_answer")));
				// } else {
				// finalObj.put("eng_selected_answer", "");
				// }
				//
				// finalResponse.add(finalObj);
				// }
				Map<String, Object> finalResponse = new HashMap<String, Object>();
				finalResponse.put("cust_response", custResponse);
				finalResponse.put("engg_response", egnrResponse);
				/*
				 * if(finalResponse.size()==0) { for(int i=0;
				 * i<egnrResponse.size();i++) { Map<String,String> finalObj =
				 * new HashMap<String, String>(); Map<String,Object>
				 * engSingleObj = egnrResponse.get(i); finalObj.put("question",
				 * String.valueOf(engSingleObj.get("question")));
				 * finalObj.put("eng_selected_answer",
				 * String.valueOf(engSingleObj.get("selected_answer")));
				 * if(custResponse.size()>0) { Map<String, Object> custSingleObj
				 * = custResponse.get(i); finalObj.put("cust_selected_answer",
				 * String.valueOf(custSingleObj.get("selected_answer"))); } else
				 * { finalObj.put("cust_selected_answer", ""); }
				 * 
				 * finalResponse.add(finalObj); } }
				 */

				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	private String getValidataBaseUrl() {
//		http://54.69.180.84:5000/biblog-validata/resutlts/all?processId=27125017
		final String BIZLOG_PRODUCTION_IP = "172.31.34.213";
		final String BIZLOG_TEST_IP = "172.31.13.246";
		final String BIZLOG_DEV_IP = "172.31.10.210";
		String rtnValue = "";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			String ip=IP.getHostAddress().toString();
			if (ip.equals(BIZLOG_PRODUCTION_IP)) {
				rtnValue = "http://rl.bizlog.in:5000";
			} else if(ip.equals(BIZLOG_TEST_IP)){
				rtnValue = "http://test.bizlog.in:5000";
			}else{
				rtnValue = "http://dev.bizlog.in:5000";
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}
	
	//Getting Technical Evtaluation Result . Using Different Third Party API
	@RequestMapping(value = "/get/tech-evaluation-result", produces = "application/json")
	public RestResult getTechEvaluationResult(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/test_user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {

				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				ArrayList<Map<String, Object>> techResponse = new ArrayList<Map<String, Object>>();
				if(resp.containsKey("automatic_response")) {
					techResponse = (ArrayList<Map<String, Object>>) resp
							.get("automatic_response");
				}
				
				/*System.out.println("techRes: "+techResponse);
				Map<String ,Object> techMapRes = new HashMap<String , Object>();
				
				for (Map<String, Object> map : techResponse)
				     for (Entry<String, Object> mapEntry : map.entrySet())
				        {
				        String key = mapEntry.getKey();
				        Object value = mapEntry.getValue();
				        System.out.println("key : "+key);
				        System.out.println("value : "+value.toString());
				        techMapRes.put(key, value.toString());
				        }
				
//				Map<String, Object> finalResponse = new HashMap<String, Object>();
//				finalResponse.put("automatic_response", techMapRes);
*/
				ArrayList<Map<String, Object>> finalResponse = new ArrayList<Map<String, Object>>();
				Map<String, Object> singleObj = techResponse.get(0);
				for (String key : singleObj.keySet()) {
					Map<String, Object> finalObj = new HashMap<String, Object>();
					finalObj.put("question", key);
					finalObj.put("answer", singleObj.get(key));
					
					finalResponse.add(finalObj);
				}
				
				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Tech Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	
	/**
	 * CALLING THIRD PARTY API FOR 
	 * GETTING PHYSICAL EVALUATION RESULT
	 * AT SERVICE CENTER
	 * */
	@RequestMapping(value = "/get/phy-evaluation-result/service-center", produces = "application/json")
	public RestResult getPhysicalEvalAtServiceCenter(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/test_user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {

				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				ArrayList<Map<String, Object>> techResponse = new ArrayList<Map<String, Object>>();
				if(resp.containsKey("automatic_response")) {
					techResponse = (ArrayList<Map<String, Object>>) resp
							.get("automatic_response");
				}
				ArrayList<Map<String, Object>> finalResponse = new ArrayList<Map<String, Object>>();
				Map<String, Object> singleObj = techResponse.get(0);
				for (String key : singleObj.keySet()) {
					Map<String, Object> finalObj = new HashMap<String, Object>();
					finalObj.put("question", key);
					finalObj.put("answer", singleObj.get(key));
					
					finalResponse.add(finalObj);
				}
				
				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Tech Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	
	/**
	 * CALLING THIRD PARTY API TO 
	 * GET TECHNICAL EVALUATION RESULT 
	 * AT SERVICE CENTER.
	 * */
	
	@RequestMapping(value = "/get/tech-evaluation-result/service-center", produces = "application/json")
	public RestResult getTechEvaluationAtServiceCenter(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/test_user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {

				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				ArrayList<Map<String, Object>> techResponse = new ArrayList<Map<String, Object>>();
				if(resp.containsKey("automatic_response")) {
					techResponse = (ArrayList<Map<String, Object>>) resp
							.get("automatic_response");
				}
				ArrayList<Map<String, Object>> finalResponse = new ArrayList<Map<String, Object>>();
				Map<String, Object> singleObj = techResponse.get(0);
				for (String key : singleObj.keySet()) {
					Map<String, Object> finalObj = new HashMap<String, Object>();
					finalObj.put("question", key);
					finalObj.put("answer", singleObj.get(key));
					
					finalResponse.add(finalObj);
				}
				
				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Tech Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	
	/**
	 * CALLING THIRD PARTY API
	 * FOR GETTING PHYSICAL EVALUATION
	 * RESULT FOR STAND-BY DEVICE
	 * */
	@RequestMapping(value = "/get/phy-evaluation-result/standby-device", produces = "application/json")
	public RestResult getPhysicalEvaluationForStandby(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/test_user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {

				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				ArrayList<Map<String, Object>> techResponse = new ArrayList<Map<String, Object>>();
				if(resp.containsKey("automatic_response")) {
					techResponse = (ArrayList<Map<String, Object>>) resp
							.get("automatic_response");
				}
				ArrayList<Map<String, Object>> finalResponse = new ArrayList<Map<String, Object>>();
				Map<String, Object> singleObj = techResponse.get(0);
				for (String key : singleObj.keySet()) {
					Map<String, Object> finalObj = new HashMap<String, Object>();
					finalObj.put("question", key);
					finalObj.put("answer", singleObj.get(key));
					
					finalResponse.add(finalObj);
				}
				
				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Tech Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	
	/**
	 * CALLING THIRD PARTY API FOR 
	 * GETTING THE TECHNICAL EVALUATION
	 * RESULT FOR STAND-BY DEVICE
	 * */
	
	@RequestMapping(value = "/get/tech-evaluation-result/standby-device", produces = "application/json")
	public RestResult getTechEvaluationForStandby(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(value = "category") String processId) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			// DefaultHttpClient httpclient = new DefaultHttpClient();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/test_user_answers_to_questions?ticket_id=" + processId;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("response from server " + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {

				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
				ArrayList<Map<String, Object>> techResponse = new ArrayList<Map<String, Object>>();
				if(resp.containsKey("automatic_response")) {
					techResponse = (ArrayList<Map<String, Object>>) resp
							.get("automatic_response");
				}
				ArrayList<Map<String, Object>> finalResponse = new ArrayList<Map<String, Object>>();
				Map<String, Object> singleObj = techResponse.get(0);
				for (String key : singleObj.keySet()) {
					Map<String, Object> finalObj = new HashMap<String, Object>();
					finalObj.put("question", key);
					finalObj.put("answer", singleObj.get(key));
					
					finalResponse.add(finalObj);
				}
				
				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Tech Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during READING DATA", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
	//
	
	@RequestMapping(value = "/get/evaluation-history", produces = "application/json")
	public RestResult getEvaluationHistory(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/evaluation_history?ticket_id=4";
			// String url =
			// "http://arcane-gorge-35657.herokuapp.com/admin/evaluation_history?ticket_id=5";
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);

			ArrayList<Map<String, Object>> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse),
					ArrayList.class);
			ArrayList<Map<String, String>> finalResponse = new ArrayList<Map<String, String>>();
			for (Map<String, Object> singleObj : resp) {
				Map<String, String> finalObj = new HashMap<String, String>();
				finalObj.put("evalId", String.valueOf(singleObj.get("id")));
				finalObj.put("ticketId", String.valueOf(singleObj.get("ticket_id")));
				finalObj.put("evalDate", String.valueOf(singleObj.get("created_at")));

				finalResponse.add(finalObj);
			}

			/*
			 * Map<String,String> firstObj = new HashMap<String,String>();
			 * firstObj.put("evalId", "100"); firstObj.put("ticketId",
			 * "DMR-FDG-113-HONIA"); firstObj.put("evalDate", "01/07/2017");
			 * 
			 * Map<String,String> secondObj = new HashMap<String,String>();
			 * secondObj.put("evalId", "110"); secondObj.put("ticketId",
			 * "DMR-FDG-113-A0QWE"); secondObj.put("evalDate", "08/07/2017");
			 * 
			 * Map<String,String> thirdObj = new HashMap<String,String>();
			 * thirdObj.put("evalId", "115"); thirdObj.put("ticketId",
			 * "DMR-FDG-113-BHTYU"); thirdObj.put("evalDate", "13/07/2017");
			 * 
			 * finalResponse.add(firstObj); finalResponse.add(secondObj);
			 * finalResponse.add(thirdObj);
			 */

			result.setResponse(finalResponse);

		} catch (Exception ex) {
		log.error("Exception during upload", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/compare/evaluation-results", produces = "application/json")
	public RestResult compareEvaluationResults(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("eval1") String eval1, @RequestParam("eval2") String eval2) {
		RestResult result = new RestResult();
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {

			CloseableHttpClient httpclient = HttpClients.createDefault();

			// Add HTTP headers
			String url = "http://www.validata.in/admin/compare_answers?id=" + eval1 + "," + eval2;
			HttpGet httpget = new HttpGet(url);

			httpget.addHeader("Content-Type", "application/json");

			CloseableHttpResponse response = httpclient.execute(httpget);
			String rawResponse = EntityUtils.toString(response.getEntity());
			log.debug("Response from server :" + rawResponse);

			// HttpEntity entity = response.getEntity();

			// Assign the response to a VO object
			if (response.getStatusLine().getStatusCode() == 200) {
				// JSONArray resArr = new JSONArray(rawResponse);
				Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);

				ArrayList<Map<String, Object>> e1Response = (ArrayList<Map<String, Object>>) resp.get(eval1);
				for (Map<String, Object> e1Obj : e1Response) {
					String answers = String.valueOf(e1Obj.get("answers"));
					if (answers != null) {
						ArrayList<String> modifiedAnswers = new ArrayList<String>();
						String[] answerStrArr = answers.split(",");
						String selectedAnswer = String.valueOf(e1Obj.get("selected_answer"));
						if (selectedAnswer.equalsIgnoreCase("A")) {
							selectedAnswer = answerStrArr[0];
						} else if (selectedAnswer.equalsIgnoreCase("B")) {
							selectedAnswer = answerStrArr[1];
						} else if (selectedAnswer.equalsIgnoreCase("C")) {
							selectedAnswer = answerStrArr[2];
						} else if (selectedAnswer.equalsIgnoreCase("D") && answerStrArr.length == 2) {
							selectedAnswer = answerStrArr[1];
						} else if (selectedAnswer.equalsIgnoreCase("D")) {
							selectedAnswer = answerStrArr[3];
						}
						selectedAnswer = selectedAnswer.trim();
						if (selectedAnswer.indexOf("=>") != -1) {
							int index = selectedAnswer.indexOf("=>");
							selectedAnswer = selectedAnswer.substring(index + 2).trim();
						}

						for (int a = 0; a < answerStrArr.length; a++) {
							String singleAnswer = answerStrArr[a];
							singleAnswer = singleAnswer.replaceAll(" ", "");
							modifiedAnswers.add(singleAnswer);
						}
						e1Obj.put("answers", modifiedAnswers);
						e1Obj.put("selected_answer", selectedAnswer);
					}
				}

				ArrayList<Map<String, Object>> e2Response = (ArrayList<Map<String, Object>>) resp.get(eval2);
				for (Map<String, Object> e2Obj : e2Response) {
					String answers = String.valueOf(e2Obj.get("answers"));
					if (answers != null) {
						ArrayList<String> modifiedAnswers = new ArrayList<String>();
						String[] answerStrArr = answers.split(",");
						String selectedAnswer = String.valueOf(e2Obj.get("selected_answer"));
						if (selectedAnswer.equalsIgnoreCase("A")) {
							selectedAnswer = answerStrArr[0];
						} else if (selectedAnswer.equalsIgnoreCase("B")) {
							selectedAnswer = answerStrArr[1];
						} else if (selectedAnswer.equalsIgnoreCase("C")) {
							selectedAnswer = answerStrArr[2];
						} else if (selectedAnswer.equalsIgnoreCase("D") && answerStrArr.length == 2) {
							selectedAnswer = answerStrArr[1];
						} else if (selectedAnswer.equalsIgnoreCase("D")) {
							selectedAnswer = answerStrArr[3];
						}
						selectedAnswer = selectedAnswer.trim();
						if (selectedAnswer.indexOf("=>") != -1) {
							int index = selectedAnswer.indexOf("=>");
							selectedAnswer = selectedAnswer.substring(index + 2).trim();
						}

						for (int a = 0; a < answerStrArr.length; a++) {
							String singleAnswer = answerStrArr[a];
							singleAnswer = singleAnswer.replaceAll(" ", "");
							modifiedAnswers.add(singleAnswer);
						}
						e2Obj.put("answers", modifiedAnswers);
						e2Obj.put("selected_answer", selectedAnswer);
					}
				}

				ArrayList<Map<String, String>> finalResponse = new ArrayList<Map<String, String>>();
				for (int i = 0; i < e1Response.size(); i++) {
					Map<String, String> finalObj = new HashMap<String, String>();
					Map<String, Object> e1SingleObj = e1Response.get(i);
					finalObj.put("question", String.valueOf(e1SingleObj.get("question")));
					finalObj.put("e1_selected_answer", String.valueOf(e1SingleObj.get("selected_answer")));
					if (e2Response.size() > 0) {
						Map<String, Object> e2SingleObj = e2Response.get(i);
						finalObj.put("e2_selected_answer", String.valueOf(e2SingleObj.get("selected_answer")));
					} else {
						finalObj.put("e2_selected_answer", "");
					}

					finalResponse.add(finalObj);
				}

				result.setResponse(finalResponse);
				result.setSuccess(true);
			} else {
				result.setMessage("Evaluation is not available");
				result.setSuccess(false);
			}

			response.close();

		} catch (Exception ex) {
			log.error("Exception during upload", ex);
			result.setMessage("Evaluation is not available");
			return result;
		}

		return result;
	}
}
