package com.rlogistics.rest;

import lombok.Data;

@Data
public class ImageList {
    private String url = "https://ibb.co/gDXMaq";
    private String type;
}
