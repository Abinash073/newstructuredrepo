package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.RetailerDoorStepResult;
import com.rlogistics.customqueries.RetailerServicesCustomQueriesMapper;
import com.rlogistics.customqueries.RetailerServicesCustomQuery1Result;

@RestController
public class RetailerServicesCustomQueriesRestResource {
	
	@RequestMapping(value = "/retailerservices/customqueries/servicequery/{retailer}", method=RequestMethod.POST,produces = "application/json")
	List<RetailerServicesCustomQuery1Result> query1(@PathVariable("retailer") String retailer){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerServicesCustomQueriesMapper mapper = session.getMapper(RetailerServicesCustomQueriesMapper.class);
			
			return mapper.findServices(retailer);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/services/byretailer/{retailer}", method=RequestMethod.POST,produces = "application/json")
	List<RetailerServicesCustomQuery1Result> query2(@PathVariable("retailer") String retailer){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerServicesCustomQueriesMapper mapper = session.getMapper(RetailerServicesCustomQueriesMapper.class);
			
			return mapper.findServicesByRetailer(retailer);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/services/forRetailer/{retailer}", method=RequestMethod.POST,produces = "application/json")
	List<RetailerServicesCustomQuery1Result> query3(@PathVariable("retailer") String retailer){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerServicesCustomQueriesMapper mapper = session.getMapper(RetailerServicesCustomQueriesMapper.class);
			
			return mapper.findServicesForRetailer(retailer);
		} finally {
			session.close();
		}
	}

	@RequestMapping(value = {"/list/door-step-services/{retailerId}","/list/door-step-services/{retailerId}/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getDoorStepServices(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		String retailerId = pathVariables.get("retailerId");
		int firstResult = 0;
		int maxResults = 25;
		
		try {
			RetailerServicesCustomQueriesMapper mapper = session.getMapper(RetailerServicesCustomQueriesMapper.class);
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			List<RetailerDoorStepResult> retailerDoorStepResults = mapper.getDoorStepServices(retailerId, firstResult, maxResults);
			paginationResponse.setData(retailerDoorStepResults);
			
			ResultCount resultCount = mapper.getDoorStepServicesCount(retailerId);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
