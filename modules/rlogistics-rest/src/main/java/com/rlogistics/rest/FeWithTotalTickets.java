package com.rlogistics.rest;

import java.util.List;

public class FeWithTotalTickets {
    private String feName;
    private String number;
    private String email;
    private int totalTicket;
    private int pendingTicket;
    private int totalTicketOfCurrentDay;


    public int getTotalTicket() {
        return totalTicket;
    }

    public void setTotalTicket(int totalTicket) {
        this.totalTicket = totalTicket;
    }

    public String getFeName() {
        return feName;
    }

    public void setFeName(String feName) {
        this.feName = feName;
    }

    public int getPendingTicket() {
        return pendingTicket;
    }

    public void setPendingTicket(int pendingTicket) {
        this.pendingTicket = pendingTicket;
    }

    public int getTotalTicketOfCurrentDay() {
        return totalTicketOfCurrentDay;
    }

    public void setTotalTicketOfCurrentDay(int totalTicketOfCurrentDay) {
        this.totalTicketOfCurrentDay = totalTicketOfCurrentDay;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
