package com.rlogistics.rest;

public class MetadataStats {
	private int numProcesses;
	public int getNumProcesses() {
		return numProcesses;
	}
	public void setNumProcesses(int numProcesses) {
		this.numProcesses = numProcesses;
	}
	public int getNumUsers() {
		return numUsers;
	}
	public void setNumUsers(int numUsers) {
		this.numUsers = numUsers;
	}
	public int getNumLocations() {
		return numLocations;
	}
	public void setNumLocations(int numLocations) {
		this.numLocations = numLocations;
	}
	public int getNumRoles() {
		return numRoles;
	}
	public void setNumRoles(int numRoles) {
		this.numRoles = numRoles;
	}
	public int getNumLocationRoles() {
		return numLocationRoles;
	}
	public void setNumLocationRoles(int numLocationRoles) {
		this.numLocationRoles = numLocationRoles;
	}
	private int numUsers;
	private int numLocations;
	private int numRoles;
	private int numLocationRoles;
}
