package com.rlogistics.rest;

import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.impl.variable.VariableType;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.ampm.AMPMUtil;
import com.rlogistics.ampm.PickupRequest;
import com.rlogistics.ampm.PickupRequestCancellationResult;
import com.rlogistics.ampm.PickupRequestCancellationSpecification;
import com.rlogistics.ampm.PickupRequestSpecification;
import com.rlogistics.customqueries.ProcessResult;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.Retailer.RetailerQuery;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.MisBulkTransfer;
import com.rlogistics.master.identity.RLogisticsAttachment;

@RestController
public class AMPMResource extends RLogisticsResource {
	private static Logger log = LoggerFactory.getLogger(AMPMResource.class);

	@RequestMapping(value = "/complete/bulk-pickup-from-seller", produces = "application/json")
	public RestResult completeBulkPickupFromSeller(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("processIds") String processIds, @RequestParam("variables") String variablesJSON) {

		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}
		RestResult restResult = new RestResult();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			Map<String, Object> variables = parseVariableMap(variablesJSON);
			ArrayList<String> processIdList = new ObjectMapper().readValue(URLDecoder.decode(processIds),
					ArrayList.class);
			for (String processId : processIdList) {
				runtimeService.setVariable(processId, "interimComments", variables.get("interimComments"));
				runtimeService.setVariable(processId, "sellerSignature", variables.get("sellerSignature"));
				runtimeService.setVariable(processId, "rlSellerProductPicked", "true");
				runtimeService.setVariable(processId, "rlSalesInvoicePicked", "true");

				try {
					Map<String, String> pathVariables = new HashMap<String, String>();
					pathVariables.put("arg0", processId);
					RLogisticsUrlHandlerResource rlLogisticsUrlHandlerResource = new RLogisticsUrlHandlerResource();
					rlLogisticsUrlHandlerResource.executeUrlHandler(httpRequest, httpResponse, "advance_process",
							pathVariables);
				} catch (Exception ue) {
					log.error("Exception during calling URL handler method :- " + ue.getMessage());
					throw ue;
				}
			}
			restResult.setResponse(true);
			restResult.setMessage("Bulk Pickup from Seller completed");
		} catch (Exception ex) {
			log.error("Exception during completion of Bulk pickup", ex);
			restResult.setResponse(false);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	@RequestMapping(value = "/start/bulk-pickup-from-seller", produces = "application/json")
	public RestResult startBulkPickupFromSeller(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("processIds") String processIds) {

		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}
		RestResult restResult = new RestResult();

		try {

			ArrayList<String> processIdList = new ObjectMapper().readValue(URLDecoder.decode(processIds),
					ArrayList.class);
			for (String processId : processIdList) {

				Map<String, String> pathVariables = new HashMap<String, String>();
				pathVariables.put("arg0", processId);
				RLogisticsUrlHandlerResource rlLogisticsUrlHandlerResource = new RLogisticsUrlHandlerResource();
				rlLogisticsUrlHandlerResource.executeUrlHandler(httpRequest, httpResponse, "advance_process",
						pathVariables);

			}
			restResult.setResponse(true);
			restResult.setMessage("Bulk Pickup from Seller completed");
		} catch (Exception ex) {
			log.error("Exception during completion of Bulk pickup", ex);
			restResult.setResponse(false);
			httpResponse.setStatus(500);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	@RequestMapping(value = "/complete/bulk-drop", produces = "application/json")
	public RestResult completeBulkDrop(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("bulkDropIds") String bulkDropIds) {
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}
		RestResult restResult = new RestResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		try {

			ArrayList<String> dropIdList = new ObjectMapper().readValue(URLDecoder.decode(bulkDropIds),
					ArrayList.class);
			for (String dropId : dropIdList) {
				MisBulkTransfer misBulkTransfer = masterdataService.createMisBulkTransferQuery().id(dropId)
						.singleResult();
				if (misBulkTransfer != null) {
					misBulkTransfer.setStatus("delivered");
					restResult.setMessage("Bulk Drop completed");

					try {
						masterdataService.saveMisBulkTransfer(misBulkTransfer);
					} catch (Exception s) {

					}
				} else {
					restResult.setMessage("Bulk Drop Completed");
				}
			}
			restResult.setResponse(true);
			restResult.setSuccess(true);
			// restResult.setMessage("Bulk Drop completed");

		} catch (Exception ex) {
			log.error("Exception during completion of Bulk pickup", ex);
			restResult.setResponse(false);
			httpResponse.setStatus(500);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	@RequestMapping(value = "/picked/bulk-drop", produces = "application/json")
	public RestResult pickedBulkDrop(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("bulkDropIds") String bulkDropIds) {
		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}
		RestResult restResult = new RestResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		try {

			ArrayList<String> dropIdList = new ObjectMapper().readValue(URLDecoder.decode(bulkDropIds),
					ArrayList.class);
			for (String dropId : dropIdList) {
				MisBulkTransfer misBulkTransfer = masterdataService.createMisBulkTransferQuery().id(dropId)
						.singleResult();
				if (misBulkTransfer != null) {
					misBulkTransfer.setStatus("Picked");
					try {
						masterdataService.saveMisBulkTransfer(misBulkTransfer);
					} catch (Exception s) {

					}
				}
			}
			restResult.setResponse(true);
			restResult.setSuccess(true);
			restResult.setMessage("Bulk Picked");

		} catch (Exception ex) {
			log.error("Exception during picking of Bulk pickup", ex);
			restResult.setResponse(false);
			httpResponse.setStatus(500);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	private static Map<String, Object> parseVariableMap(String fieldValuesJson) {
		Map<String, Object> retval = new HashMap<String, Object>();

		try {
			HashMap raw = new ObjectMapper().readValue(URLDecoder.decode(fieldValuesJson), HashMap.class);
			for (Object k : raw.keySet()) {
				retval.put(k.toString(), raw.get(k));
			}
		} catch (Exception ex) {
			throw new RuntimeException("Error parsing JSON", ex);
		}

		return retval;
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/ampm/generate-pickup-request", produces = "application/json")
	public PickupRequest generatePickupRequest(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("requestSpec") String requestSpecJson) {

		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			PickupRequestSpecification pickupRequestSpec = new ObjectMapper()
					.readValue(URLDecoder.decode(requestSpecJson), PickupRequestSpecification.class);
			return AMPMUtil.generatePickupRequest(pickupRequestSpec);
		} catch (Exception ex) {
			log.error("Exception during generatePickupRequest", ex);
			PickupRequest pickupRequest = new PickupRequest();
			pickupRequest.getMessages().add(ex.getMessage());
			return pickupRequest;
		}
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/ampm/cancel-pickup-request", produces = "application/json")
	public PickupRequestCancellationResult cancelPickupRequest(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam("cancellationSpec") String cancellationSpecJson) {

		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			PickupRequestCancellationSpecification pickupRequestSpec = new ObjectMapper()
					.readValue(URLDecoder.decode(cancellationSpecJson), PickupRequestCancellationSpecification.class);
			return AMPMUtil.cancelPickupRequest(pickupRequestSpec);
		} catch (Exception ex) {
			log.error("Exception during cancelPickupRequest", ex);
			PickupRequestCancellationResult pickupRequest = new PickupRequestCancellationResult();
			pickupRequest.getMessages().add(ex.getMessage());
			return pickupRequest;
		}
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/ampm/update/status", produces = "application/json")
	public RestResult updateAmpmStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("query") String queryJSON) {

		RestResult restResult = new RestResult();
		String header = httpRequest.getHeader("Authorization");

		Map<String, String> queryObj = new HashMap<String, String>();

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> rlAMPMDetailsMap = null;
		try {
			if (!authorizeAPIKey(decodeParameterWithBase64(header))) {
				throw new Exception("Invalid token");
			}

			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			String referenceNumber = queryObj.get("referenceNumber");
			String status = queryObj.get("status");

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcess("rlAMPMDetailsMap");
			if (processResults.size() > 0) {
				for (int i = 0; i < processResults.size(); i++) {
					String processId = processResults.get(i).getProcessId();
					Map<String, Object> variables = runtimeService.getVariables(processId);
					if (variables != null) {
						rlAMPMDetailsMap = (Map<String, Object>) variables.get("rlAMPMDetailsMap");
						if (rlAMPMDetailsMap.containsKey(referenceNumber)) {
							Map<String, String> ampmObj = (Map<String, String>) rlAMPMDetailsMap.get(referenceNumber);
							ampmObj.put("status", status);
							rlAMPMDetailsMap.put(referenceNumber, ampmObj);

							runtimeService.setVariable(processId, "rlAMPMDetailsMap", rlAMPMDetailsMap);

							break;
						}
					}
				}
			}

			restResult.setResponse(rlAMPMDetailsMap);
			restResult.setMessage("");
			return restResult;
		} catch (Exception ex) {
			log.error("Exception during AMPM status update", ex);
			restResult.setResponse(false);
			restResult.setMessage(ex.getMessage());
			return restResult;
		}
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/ampm/update/docket-number", produces = "application/json")
	public RestResult updateAmpmDocketNumber(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("query") String queryJSON) {

		RestResult restResult = new RestResult();
		String header = httpRequest.getHeader("Authorization");

		Map<String, String> queryObj = new HashMap<String, String>();

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> rlAMPMDetailsMap = null;
		try {
			if (!authorizeAPIKey(decodeParameterWithBase64(header))) {
				throw new Exception("Invalid token");
			}

			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			String referenceNumber = queryObj.get("referenceNumber");
			String docketNumber = queryObj.get("docketNumber");

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcess("rlAMPMDetailsMap");
			if (processResults.size() > 0) {
				for (int i = 0; i < processResults.size(); i++) {
					String processId = processResults.get(i).getProcessId();
					Map<String, Object> variables = runtimeService.getVariables(processId);
					if (variables != null) {
						rlAMPMDetailsMap = (Map<String, Object>) variables.get("rlAMPMDetailsMap");
						if (rlAMPMDetailsMap.containsKey(referenceNumber)) {
							Map<String, String> ampmObj = (Map<String, String>) rlAMPMDetailsMap.get(referenceNumber);
							ampmObj.put("docketNo", docketNumber);
							rlAMPMDetailsMap.put(referenceNumber, ampmObj);

							runtimeService.setVariable(processId, "rlAMPMDetailsMap", rlAMPMDetailsMap);

							break;
						}
					}
				}
			}

			restResult.setResponse(rlAMPMDetailsMap);
			restResult.setMessage("");
			return restResult;
		} catch (Exception ex) {
			log.error("Exception during AMPM docketNo update", ex);
			restResult.setResponse(false);
			restResult.setMessage(ex.getMessage());
			return restResult;
		}
	}

	public static String decodeParameterWithBase64(String param) {
		String res1 = null;
		try {
			res1 = URLDecoder.decode(param, "UTF-8");
			byte[] plainCredsBytes = res1.getBytes();
			byte[] base = Base64.decodeBase64(plainCredsBytes);
			return new String(base);
		} catch (Exception e) {
		}
		return "";
	}

	public static boolean authorizeAPIKey(String key) {

		if ("biz123log987678@t0ken2835".equals(key)) {
			return true;
		}

		return false;
	}

	public static ProcessResponseWithVariables findByProcessId(String processId) {
		log.debug("Process Id in findBy........" + processId);
		try {
			Task task = null;
			ProcessResponseWithVariables processResultWithVariables = null;
			ProcessInstance processInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();
			log.debug("processInstance from findByProcessId : " + processInstance);
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();

			if (processInstance != null) {
				List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getTaskService().createTaskQuery().processInstanceId(processId).includeProcessVariables()
						.orderByTaskCreateTime().desc().list();

				if (!tasks.isEmpty()) {
					task = tasks.get(0);
					log.debug("task : " + task);
					processResultWithVariables = new ProcessResponseWithVariables(processInstance, task,
							runtimeService.getVariables(processId)

					);
					Task firstTask = tasks.get(tasks.size() - 1);
					Date createdDate = firstTask.getCreateTime();
					processResultWithVariables.setCreatedDate(createdDate);
					Date modifiedDate = task.getCreateTime();
					processResultWithVariables.setModifiedDate(modifiedDate);
				} else {

					ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getManagementService();

					List<Job> jobs = managementService.createJobQuery()
							.processInstanceId(processInstance.getProcessInstanceId()).list();

					String jobId = null;
					/* TODO Handle if more than one job */
					if (!jobs.isEmpty()) {
						jobId = jobs.get(0).getId();
					}

					processResultWithVariables = new ProcessResponseWithVariables(processInstance, jobId,
							runtimeService.getVariables(processId)

					);
				}
			} else {
				log.debug("inside else");
				HistoricProcessInstance historicProcessInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery()
								.processInstanceId(processId).includeProcessVariables().singleResult();
				log.debug("historicProcessInstance : " + historicProcessInstance);
				if (historicProcessInstance != null) {
					try {
						processResultWithVariables = new ProcessResponseWithVariables(historicProcessInstance,

								historicProcessInstance.getProcessVariables()

						);
						Date createdDate = historicProcessInstance.getStartTime();
						Date modifiedDate = historicProcessInstance.getEndTime();
						processResultWithVariables.setCreatedDate(createdDate);
						processResultWithVariables.setModifiedDate(modifiedDate);
					} catch (Exception e) {
						log.debug("Error in Adding TIME.." + e);
					}

					log.debug("processResultWithVariables value : " + processResultWithVariables);
					processResultWithVariables.setCompleted(true);
				} else {
					processResultWithVariables = new ProcessResponseWithVariables(

							runtimeService.getVariables(processId)

					);

					log.error("Process Id " + processId + " is neither present not past. It is missing.");
				}
			}

			return processResultWithVariables;
		} catch (Exception ex) {
			log.error("Exception during listInvolvedTasks", ex);
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/ampm/track-product/{conNo}", produces = "application/json")
	public RestResult trackProductStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@PathVariable("conNo") String conNo) {

		if (!beforeMethodInvocation(httpRequest, httpResponse)) {
			return null;
		}

		try {
			return AMPMUtil.trackProductStatus(conNo);
		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			RestResult restResult = new RestResult();
			restResult.setMessage(ex.getMessage());
			return restResult;
		}
	}

	@RequestMapping(value = "/ampm/reverse/product-tracking/{ticketNo}", produces = "application/json")
	public RestResult reverseProductTracking(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@PathVariable("ticketNo") String ticketNo) {

		RestResult restResult = new RestResult();
		String header = httpRequest.getHeader("Authorization");

		try {
			if (!authorizeAPIKey(decodeParameterWithBase64(header))) {
				throw new Exception("Invalid token");
			}
			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", ticketNo);
			String processId = null;
			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();

				ProcessResponseWithVariables processResponseWithVariables = findByProcessId(processId);
				ReverseTrackingResponse reverseTrackingResponse = new ReverseTrackingResponse();
				if (processResponseWithVariables != null) {
					reverseTrackingResponse.setTicketNo(ticketNo);
					reverseTrackingResponse.setConsumerName(
							String.valueOf(processResponseWithVariables.getVariables().get("consumerName")));
					reverseTrackingResponse.setCreatedDate(processResponseWithVariables.getCreatedDate());
					reverseTrackingResponse.setModifiedDate(processResponseWithVariables.getModifiedDate());
					if (processResponseWithVariables.getVariables().containsKey("rlProcessStatus")) {
						reverseTrackingResponse.setStatus(
								String.valueOf(processResponseWithVariables.getVariables().get("rlProcessStatus")));
					}

					restResult.setResponse(reverseTrackingResponse);
				}
			}

		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setMessage(ex.getMessage());

		}
		return restResult;
	}

	@RequestMapping(value = "/get/ticket-status/{ticketNo}", produces = "application/json")
	public RestExternalResult getTicketStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@PathVariable("ticketNo") String ticketNo,
			@RequestParam(value = "retailerId", required = true) String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();

		try {
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					throw new Exception("Invalid login");
				}
				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

				/*
				 * Timestamp currentDate = new Timestamp(c.getTime().getTime());
				 * if (currentDate.after(retailer.getTokenExpiryDate())) {
				 * restResult.setSuccess(false); throw new Exception(
				 * "Authentication expired. Please login again"); }
				 */
			}

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", ticketNo);
			String processId = null;
			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();

				ProcessResponseWithVariables processResponseWithVariables = findByProcessId(processId);
				TicketStatusPojo ticketStatusPojo = new TicketStatusPojo();
				if (processResponseWithVariables != null) {
					ticketStatusPojo.setTicketNo(ticketNo);
					ticketStatusPojo.setConsumerName(
							String.valueOf(processResponseWithVariables.getVariables().get("consumerName")));
					ticketStatusPojo.setCreatedDate(processResponseWithVariables.getCreatedDate());
					ticketStatusPojo.setModifiedDate(processResponseWithVariables.getModifiedDate());
					String ticketStatusCode = String
							.valueOf(processResponseWithVariables.getVariables().get("rlTicketStatusCode"));
					String ticketStatusMessage = String
							.valueOf(processResponseWithVariables.getVariables().get("rlProcessStatus"));
					String[] statusArray = new String[2];
					if (getTicketStatusFromTicketStatusExternal(processResponseWithVariables)) {
						String ststus = processResponseWithVariables.getVariables().get("runningTicketStatusExternal")
								.toString();
						statusArray = getTicketStatusFromTicketStatusExternalArray(ststus);
					} else {
						statusArray = getTicketStatusFromStatusCode(ticketStatusCode, ticketStatusMessage);
					}
					String finalStatusCode = statusArray[0];
					String finalStatusMessage = statusArray[1];
					ticketStatusPojo.setTicketStatusCode(finalStatusCode);
					ticketStatusPojo.setStatus(finalStatusMessage);

					/**
					 * commented because we are sending our custom ticket status
					 * messages according to group
					 */
					// if
					// (processResponseWithVariables.getVariables().containsKey("rlProcessStatus"))
					// {
					// reverseTrackingResponse.setStatus(
					// String.valueOf(processResponseWithVariables.getVariables().get("rlProcessStatus")));
					// }

					restResult.setResponse(ticketStatusPojo);
					restResult.setMessage("Status for "+ticketNo+" is: "+finalStatusMessage);
				} else {
					restResult.setSuccess(false);
					restResult.setMessage("Requested ticket not present");
				}
			} else {
				// No ticket is found in ACT_RU_VARIABLE table
				/**
				 * so searching in HI table
				 */

				TicketStatusPojo ticketStatusPojo = new TicketStatusPojo();

				List<String> list = Arrays.asList("rlTicketNo", "rlTicketStatus", "rlProcessStatus",
						"ticketCreationDate");
				processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
						.getHIProcessFromValue("rlTicketNo", ticketNo);
				if (processResults.size() > 0) {
					int isExist = 0;
					ProcessResponseWithVariables processResponseWithVariables = null;
					for (ProcessResult processResult : processResults) {
						processId = processResult.getProcessId();
						log.debug("processId : " + processId);
						processResponseWithVariables = findByProcessId(processId);
						log.debug("processResponseWithVariables : " + processResponseWithVariables);
						String retailerName = String
								.valueOf(processResponseWithVariables.getVariables().get("retailer"));
						if (retailerName != null && retailerName.equalsIgnoreCase(retailer.getName())) {
							isExist = 1;
							break;
						}
					}

					TicketDetailResponse ticketDetailResponse = new TicketDetailResponse();
					Map<String, Object> map = null;
					if (processResponseWithVariables != null && isExist == 1) {
						try {
							// ADDING MORE FIELDS TO THE RESPONSE
							map = new HashMap<String, Object>();
							for (int i = 0; i < list.size(); i++) {
								if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
									map.put(list.get(i), String
											.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
								}
							}

							log.debug("map value :::::::::" + map);

							// ticketDetailResponse.setVariables2(map);
							// ADDING FINISHED HERE
							try {

								log.debug("String.valueOf(processResponseWithVariables.getCreatedDate()) ...."
										+ String.valueOf(processResponseWithVariables.getCreatedDate()));
								log.debug("(processResponseWithVariables.getCreatedDate())  .. "
										+ (processResponseWithVariables.getCreatedDate()));
								ticketDetailResponse
										.setCreatedDate(String.valueOf(processResponseWithVariables.getCreatedDate()));
								ticketDetailResponse.setModifiedDate(
										String.valueOf(processResponseWithVariables.getModifiedDate()));
							} catch (Exception e) {
								log.debug("Error in SETTING TIME " + e);
							}
						} catch (Exception e) {
							log.debug("Exception in converting timestamp to Date :" + e);
						}
						ticketStatusPojo.setTicketNo(map.get("rlTicketNo").toString());
						// reverseTrackingResponse.setStatus((map.get("rlProcessStatus").toString()));
						String ticketStatusExternal = "TICKET_CLOSED";
						try {
							String status = String.valueOf(
									processResponseWithVariables.getVariables().get("runningTicketStatusExternal"));
							if (status.equals("null")) {
								ticketStatusExternal = "TICKET_CLOSED";
							} else {
								ticketStatusExternal = "TICKET_CLOSED_" + status;
							}
						} catch (Exception e) {
							ticketStatusExternal = "TICKET_CLOSED";
						}
						ticketStatusPojo.setStatus(ticketStatusExternal);
						ticketStatusPojo.setTicketStatusCode("BZITTKSTS-9999");
						restResult.setMessage("Ticket got closed on: "
								+ String.valueOf(processResponseWithVariables.getModifiedDate()));

						restResult.setResponse(ticketStatusPojo);
						restResult.setSuccess(true);

					}
				} else {
					restResult.setMessage("Ticket is not Present in the system");
					restResult.setSuccess(false);
				}

			}

			/*
			 * // generate new apiToken c.add(Calendar.HOUR, 1); if
			 * (retailer.getApiUsername() != null) { String rawToken =
			 * retailer.getApiUsername() + "___" +
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); } else { String rawToken =
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); }
			 */
			try {
				// masterdataService.saveRetailer(retailer);
				restResult.setApiToken(apiToken);
			} catch (Exception e) {

			}

		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		saveToConsoleLogsTable(retailerId, "Ticket status :" + ticketNo +" "+ restResult.getMessage());
		return restResult;
	}

	public String[] getTicketStatusFromTicketStatusExternalArray(String status) {
		String[] returnStringArray = new String[2];
		String ticketStatusMessage = "";
		String ticketStatusCode2 = "";

		switch (status) {
		case "TICKET_IS_CREATED":
			ticketStatusCode2 = "BZITTKSTS-1001";
			ticketStatusMessage = "TICKET_IS_CREATED";
			break;
		case "APPOINTMENT_HAS_FIXED":
			ticketStatusCode2 = "BZITTKSTS-1002";
			ticketStatusMessage = "APPOINTMENT_HAS_FIXED";
			break;
		case "TICKET_ASSIGNED_TO_FE_FOR_PICKUP":
			ticketStatusCode2 = "BZITTKSTS-1003";
			ticketStatusMessage = "TICKET_ASSIGNED_TO_FE_FOR_PICKUP";
			break;
		case "PRODUCT_PICKED_FROM_CUSTOMER":
			ticketStatusCode2 = "BZITTKSTS-1004";
			ticketStatusMessage = "PRODUCT_PICKED_FROM_CUSTOMER";
			break;
		case "CUSTOMER_REJECTED":
			ticketStatusCode2 = "BZITTKSTS-1008";
			ticketStatusMessage = "CUSTOMER_REJECTED";
			break;
		case "CUSTOMER_NEEDS_RESCHEDULING":
			ticketStatusCode2 = "BZITTKSTS-1009";
			ticketStatusMessage = "CUSTOMER_NEEDS_RESCHEDULING";
			break;

		case "PRODUCT_RETURN_TO_RETAILER_INITIATE":// buyback
			ticketStatusCode2 = "BZITTKSTS-1010";
			ticketStatusMessage = "PRODUCT_RETURN_TO_RETAILER_INITIATE";
			break;
		case "PRODUCT_RETURNED_TO_RETAILER":// buyback
			ticketStatusCode2 = "BZITTKSTS-1011";
			ticketStatusMessage = "PRODUCT_RETURNED_TO_RETAILER";
			break;
		case "CUSTOMER_REJECTED_TO_COORDINATOR":// buyback
			ticketStatusCode2 = "BZITTKSTS-1012";
			ticketStatusMessage = "CUSTOMER_REJECTED_TO_COORDINATOR";
			break;
		case "CUSTOMER_NOT_AGREE_WITH_THE_REQUOTED_PRICE":// buyback
			ticketStatusCode2 = "BZITTKSTS-1013";
			ticketStatusMessage = "CUSTOMER_NOT_AGREE_WITH_THE_REQUOTED_PRICE";
			break;
		case "PRODUCT_REACHED_TO_HUB":// oneway
			ticketStatusCode2 = "BZITTKSTS-1005";
			ticketStatusMessage = "PRODUCT_REACHED_TO_HUB";
			break;
		case "TICKET_ASSIGNED_FOR_DROP":// oneway
			ticketStatusCode2 = "BZITTKSTS-1006";
			ticketStatusMessage = "TICKET_ASSIGNED_FOR_DROP";
			break;
		case "PRODUCT_DROPPED":// oneway
			ticketStatusCode2 = "BZITTKSTS-1007";
			ticketStatusMessage = "PRODUCT_DROPPED";
			break;
		case "CUSTOMER_NEED_RESCHEDULE_WHILE_DROP":// oneway
			ticketStatusCode2 = "BZITTKSTS-1014";
			ticketStatusMessage = "CUSTOMER_NEED_RESCHEDULE_WHILE_DROP";
			break;
		case "CUSTOMER_REJECT_WHILE_DROP":// oneway
			ticketStatusCode2 = "BZITTKSTS-1015";
			ticketStatusMessage = "CUSTOMER_REJECT_WHILE_DROP";
			break;
		case "CUSTOMER_ASKING_FOR_REQUOTE":// buyback
			ticketStatusCode2 = "BZITTKSTS-1016";
			ticketStatusMessage = "CUSTOMER_ASKING_FOR_REQUOTE";
			break;

		default:
			ticketStatusMessage = status;

		}
		returnStringArray[0] = ticketStatusCode2;
		returnStringArray[1] = ticketStatusMessage;
		return returnStringArray;
	}

	private boolean getTicketStatusFromTicketStatusExternal(ProcessResponseWithVariables processResponseWithVariables) {
		boolean rtnValue = false;
		try {
			String status = String
					.valueOf(processResponseWithVariables.getVariables().get("runningTicketStatusExternal"));
			if (status.equals("null")) {
				rtnValue = false;
			} else {
				rtnValue = true;
			}
		} catch (Exception e) {
			rtnValue = false;

		}
		return rtnValue;
	}

	/**
	 * prem code 21-07-2018 ticket status group wise
	 * 
	 * @param ticketStatusCode
	 * @param ticketStatusMessage2
	 * @return
	 */
	private String[] getTicketStatusFromStatusCode(String ticketStatusCode, String ticketStatusMessage2) {

		String[] returnStringArray = new String[2];
		String ticketStatusMessage = "";
		String ticketStatusCode2 = "";

		switch (ticketStatusCode) {
		case "BZITTKSTS-0011":
		case "BZITTKSTS-0103":
		case "BZITTKSTS-0116":// BuyBack
			ticketStatusCode2 = "BZITTKSTS-1001";
			ticketStatusMessage = "TICKET_IS_CREATED";
			break;
		case "BZITTKSTS-0102":
		case "BZITTKSTS-0032":
		case "BZITTKSTS-0008":
		case "BZITTKSTS-0155":
		case "BZITTKSTS-0082":
			ticketStatusCode2 = "BZITTKSTS-1002";
			ticketStatusMessage = "APPOINTMENT_HAS_FIXED";
			break;
		case "BZITTKSTS-0080":
		case "BZITTKSTS-0277":
		case "BZITTKSTS-0221":// BuyBack
			ticketStatusCode2 = "BZITTKSTS-1003";
			ticketStatusMessage = "TICKET_ASSIGNED_TO_FE_FOR_PICKUP";
			break;
		case "BZITTKSTS-0271":
		case "BZITTKSTS-0237":// BuyBack
			ticketStatusCode2 = "BZITTKSTS-1004";
			ticketStatusMessage = "PRODUCT_PICKED_FROM_CUSTOMER";
			break;
		case "BZITTKSTS-0091":
		case "BZITTKSTS-0127":
			ticketStatusCode2 = "BZITTKSTS-1005";
			ticketStatusMessage = "PRODUCT_REACHED_TO_HUB";
			break;
		case "BZITTKSTS-0109":
		case "BZITTKSTS-0110":
			ticketStatusCode2 = "BZITTKSTS-1006";
			ticketStatusMessage = "TICKET_ASSIGNED_FOR_DROP";
			break;
		case "BZITTKSTS-0256":
		case "BZITTKSTS-0211":
		case "BZITTKSTS-0128":

			ticketStatusCode2 = "BZITTKSTS-1007";
			ticketStatusMessage = "PRODUCT_DROPPED";
			break;
		case "BZITTKSTS-0141":

			ticketStatusCode2 = "BZITTKSTS-1008";
			ticketStatusMessage = "CUSTOMER_REJECTED";
			break;
		case "BZITTKSTS-0134":
		case "BZITTKSTS-0135":
			ticketStatusCode2 = "BZITTKSTS-1009";
			ticketStatusMessage = "CUSTOMER_NEEDS_RESCHEDULING";
			break;

		case "BZITTKSTS-0236":// buyback
		case "BZITTKSTS-0281":// buyback
			ticketStatusCode2 = "BZITTKSTS-1010";
			ticketStatusMessage = "PRODUCT_RETURN_TO_RETAILER_INITIATE";
			break;
		case "BZITTKSTS-0374":// buyback
		case "BZITTKSTS-0118":
			ticketStatusCode2 = "BZITTKSTS-1011";
			ticketStatusMessage = "PRODUCT_RETURNED_TO_RETAILER";
			break;
		default:
			ticketStatusCode2 = ticketStatusCode;
			ticketStatusMessage = ticketStatusMessage2;
			break;

		}
		returnStringArray[0] = ticketStatusCode2;
		returnStringArray[1] = ticketStatusMessage;
		return returnStringArray;

	}

	@RequestMapping(value = "/get/ticket-details", produces = "application/json")
	public RestExternalResult getTicketDetailsByRefNo(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("referenceNo") String referenceNo,
			@RequestParam(value = "retailerCode", required = true) String retailerCode,
			@RequestParam(value = "apiToken", required = true) String apiToken) {

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();

		try {
			if (retailerCode != null) {
				retailer = masterdataService.createRetailerQuery().code(retailerCode).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					restResult.setApiToken(apiToken);
					throw new Exception("Invalid Retailer");
				}

				if (retailer.getApiToken() == null) {
					restResult.setSuccess(false);
					throw new Exception("Authentication not valid. Please login to generate apiToken");
				}

				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

				/**
				 * commentes because concurrent using APi for all reatilers
				 */
				/*
				 * Timestamp currentDate = new Timestamp(c.getTime().getTime());
				 * if (currentDate.after(retailer.getTokenExpiryDate())) {
				 * restResult.setSuccess(false); throw new Exception(
				 * "Authentication expired. Please login again"); }
				 */
			}
			List<String> list = Arrays.asList("rlTicketNo", "rlTicketStatus", "rlProcessStatus", "rlCallTranscript",
					"assignee", "rlAppointmentDate", "dropLocCity", "dropLocAddress1", "dropLocAddress1",
					"consumerName", "emailId", "addressLine1", "addressLine2", "landmark", "city", "pincode",
					"telephoneNumber", "alternateTelephoneNumber", "consumerComplaintNumber", "dateOfComplaint",
					"natureOfComplaint", "problemDescription", "rlRetailerCode", "rlAppointmentDateForFV", "product",
					"productCategory", "brand", "retailer", "rlRetailerCode", "dateOfPurchase", "latitude", "longitude",
					"isBlockToFe", "ticketCreationDate", "rlAMPMDetailsMap", "identificationNo", "isDeclaired",
					"amount", "name", "status", "email", "maxValueToBeOffered", "txnId", "payePhone", "payeEmail",
					"dttimeStampInMs", "sellerContactNumber", "customerContactNumber", "custAltPhoneNo",
					"buyerContactNo", "bulkMode", "retailerPhoneNo", "rlValueOffered", "isCostUpdated",
					"physicalEvaluation", "TechEvalRequired", "_PhotographBefore", "_PhotoBefore", "_PhotographAfter",
					"_Signature", "_ServiceCenterSignature", "_CustomerSignature", "rlAuthorizationLetterFileName",
					"rlPurchaseInvoiceFileName", "model", "rlReturnLocationName", "rlReturnLocationAddress",
					"rlReturnLocationPincode", "rlPickupLocationName", "rlPickupLocationAddress",
					"rlPickupLocationPincode", "productName2", "brand2", "model2", "wgf_FP_PhotographBefore",
					"wgf_FP_PhotographAfter", "wgf_FP_CustomerSignature");
			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", referenceNo);
			String processId = null;
			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();

				ProcessResponseWithVariables processResponseWithVariables = findByProcessId(processId);
				TicketDetailResponse reverseTrackingResponse = new TicketDetailResponse();
				if (processResponseWithVariables != null
						&& String.valueOf(processResponseWithVariables.getVariables().get("retailer"))
								.equalsIgnoreCase(retailer.getName())) {
					try {
						reverseTrackingResponse
								.setCreatedDate((String.valueOf(processResponseWithVariables.getCreatedDate())));
						reverseTrackingResponse
								.setModifiedDate((String.valueOf(processResponseWithVariables.getModifiedDate())));

						// ADDING MORE FIELDS TO THE RESPONSE
						Map<String, String> map = new HashMap<String, String>();
						for (int i = 0; i < list.size(); i++) {
							if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
								map.put(list.get(i),
										String.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
							}
						}
						log.debug("map value :::::::::" + map);
						reverseTrackingResponse.setVariables2(map);
						// ADDING FINISHED HERE

					} catch (Exception e) {
						log.debug("Exception in cnverting Timestamp to Date :" + e);
					}

					restResult.setResponse(reverseTrackingResponse);
				} else {
					processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
							.getProcessFromValue("consumerComplaintNumber", referenceNo);
					if (processResults.size() > 0) {
						int isExist = 0;
						for (ProcessResult processResult : processResults) {
							processId = processResult.getProcessId();
							processResponseWithVariables = findByProcessId(processId);
							String retailerName = String
									.valueOf(processResponseWithVariables.getVariables().get("retailer"));
							if (retailerName != null && retailerName.equalsIgnoreCase(retailer.getName())) {
								isExist = 1;
								break;
							}
						}

						reverseTrackingResponse = new TicketDetailResponse();
						if (processResponseWithVariables != null && isExist == 1) {
							try {
								// ADDING MORE FIELDS TO THE RESPONSE
								Map<String, String> map = new HashMap<String, String>();
								for (int i = 0; i < list.size(); i++) {
									if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
										map.put(list.get(i), String
												.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
									}
								}
								log.debug("map value :::::::::" + map);
								reverseTrackingResponse.setVariables2(map);
								// ADDING FINISHED HERE

								try {
									log.debug("return from method : " + com.rlogistics.util.DateUtil.timestampToDate(
											String.valueOf(processResponseWithVariables.getCreatedDate().getTime())));
								} catch (Exception e) {
									log.debug(" from Date :" + e);
								}
								reverseTrackingResponse.setCreatedDate(
										(String.valueOf(processResponseWithVariables.getCreatedDate())));
								reverseTrackingResponse.setModifiedDate(
										(String.valueOf(processResponseWithVariables.getModifiedDate())));
							} catch (Exception e) {
								log.debug("Exception in converting timestamp to Date :" + e);
							}

							restResult.setResponse(reverseTrackingResponse);
						}
					}
				}
			} else {
				processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
						.getProcessFromValue("consumerComplaintNumber", referenceNo);
				if (processResults.size() > 0) {
					int isExist = 0;
					ProcessResponseWithVariables processResponseWithVariables = null;
					for (ProcessResult processResult : processResults) {
						processId = processResult.getProcessId();
						processResponseWithVariables = findByProcessId(processId);
						String retailerName = String
								.valueOf(processResponseWithVariables.getVariables().get("retailer"));
						if (retailerName != null && retailerName.equalsIgnoreCase(retailer.getName())) {
							isExist = 1;
							break;
						}
					}

					TicketDetailResponse reverseTrackingResponse = new TicketDetailResponse();
					if (processResponseWithVariables != null && isExist == 1) {
						try {
							// ADDING MORE FIELDS TO THE RESPONSE
							Map<String, String> map = new HashMap<String, String>();
							for (int i = 0; i < list.size(); i++) {
								if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
									map.put(list.get(i), String
											.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
								}
							}
							log.debug("map value :::::::::" + map);
							reverseTrackingResponse.setVariables2(map);
							// ADDING FINISHED HERE
							reverseTrackingResponse
									.setCreatedDate((String.valueOf((processResponseWithVariables.getCreatedDate()))));
							reverseTrackingResponse.setModifiedDate(
									(String.valueOf((processResponseWithVariables.getModifiedDate()))));
						} catch (Exception e) {
							log.debug("Exception in converting timestamp to Date :" + e);
						}

						restResult.setResponse(reverseTrackingResponse);
					}
				}
			}
			// Retrieving the processId from Completed Task.

			processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getHIProcessFromValue("rlTicketNo", referenceNo);
			if (processResults.size() > 0) {
				int isExist = 0;
				ProcessResponseWithVariables processResponseWithVariables = null;
				for (ProcessResult processResult : processResults) {
					processId = processResult.getProcessId();
					log.debug("processId : " + processId);
					processResponseWithVariables = findByProcessId(processId);
					log.debug("processResponseWithVariables : " + processResponseWithVariables);
					String retailerName = String.valueOf(processResponseWithVariables.getVariables().get("retailer"));
					if (retailerName != null && retailerName.equalsIgnoreCase(retailer.getName())) {
						isExist = 1;
						break;
					}
				}

				TicketDetailResponse reverseTrackingResponse = new TicketDetailResponse();
				if (processResponseWithVariables != null && isExist == 1) {
					try {
						// ADDING MORE FIELDS TO THE RESPONSE
						Map<String, String> map = new HashMap<String, String>();
						for (int i = 0; i < list.size(); i++) {
							if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
								map.put(list.get(i),
										String.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
							}
						}
						Map<String, String> map1 = new HashMap<String, String>();
						List<RLogisticsAttachment> attachList = masterdataService.createRLogisticsAttachmentQuery()
								.key2(processId).list();
						for (int i = 0; i < attachList.size(); i++) {
							// String preUrl =
							// "http://54.202.95.252/server/attachment/access/process/";
							String preUrl = "http://rl.bizlog.in/server/attachment/access/process/";
							String url = preUrl + attachList.get(i).getKey2() + "/" + attachList.get(i).getKey3() + "/"
									+ attachList.get(i).getKey4();
							String i1 = String.valueOf(i);
							map1.put(i1, url);
						}
						log.debug("map value :::::::::" + map);
						reverseTrackingResponse.setVariables2(map);
						reverseTrackingResponse.setImages(map1);
						// ADDING FINISHED HERE
						try {
							// SimpleDateFormat parserSDF = new
							// SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz
							// yyyy");
							// log.debug("After parseSDF");
							// Date date =
							// parserSDF.parse(processResponseWithVariables.getCreatedDate().toString());
							// log.debug("After Date Variable");
							// String str6 = parserSDF.format(date);
							// log.debug("String in E, E, MMM dd yyyy HH:mm:ss
							// format is: " + str6);
							log.debug("String.valueOf(processResponseWithVariables.getCreatedDate()) ...."
									+ String.valueOf(processResponseWithVariables.getCreatedDate()));
							log.debug("(processResponseWithVariables.getCreatedDate())  .. "
									+ (processResponseWithVariables.getCreatedDate()));
							reverseTrackingResponse
									.setCreatedDate(String.valueOf(processResponseWithVariables.getCreatedDate()));
							reverseTrackingResponse
									.setModifiedDate(String.valueOf(processResponseWithVariables.getModifiedDate()));
						} catch (Exception e) {
							log.debug("Error in SETTING TIME " + e);
						}
					} catch (Exception e) {
						log.debug("Exception in converting timestamp to Date :" + e);
					}

					restResult.setResponse(reverseTrackingResponse);
				}
			}

			processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getHIProcessFromValue("consumerComplaintNumber", referenceNo);
			if (processResults.size() > 0) {
				int isExist = 0;
				ProcessResponseWithVariables processResponseWithVariables = null;
				for (ProcessResult processResult : processResults) {
					processId = processResult.getProcessId();
					processResponseWithVariables = findByProcessId(processId);
					log.debug("processResponseWithVariables : " + processResponseWithVariables);
					String retailerName = String.valueOf(processResponseWithVariables.getVariables().get("retailer"));
					if (retailerName != null && retailerName.equalsIgnoreCase(retailer.getName())) {
						isExist = 1;
						break;
					}
				}

				TicketDetailResponse reverseTrackingResponse = new TicketDetailResponse();
				if (processResponseWithVariables != null && isExist == 1) {
					try {
						// ADDING MORE FIELDS TO THE RESPONSE
						Map<String, String> map = new HashMap<String, String>();
						for (int i = 0; i < list.size(); i++) {
							if (processResponseWithVariables.getVariables().containsKey(list.get(i))) {
								map.put(list.get(i),
										String.valueOf(processResponseWithVariables.getVariables().get(list.get(i))));
							}
						}
						log.debug("map value :::::::::" + map);
						reverseTrackingResponse.setVariables2(map);
						// ADDING FINISHED HERE
						try {
							reverseTrackingResponse
									.setCreatedDate(String.valueOf(processResponseWithVariables.getCreatedDate()));
							reverseTrackingResponse
									.setModifiedDate(String.valueOf(processResponseWithVariables.getModifiedDate()));
						} catch (Exception e) {
							log.debug("Error in SETTING DATE AND TIME...." + e);
						}
					} catch (Exception e) {
						log.debug("Exception in converting timestamp to Date :" + e);
					}

					restResult.setResponse(reverseTrackingResponse);
				}
			}
			// Retrivieng processID from Completed Task END HERE.
			if (processResults.size() == 0) {
				restResult.setMessage("Requested ticket not available");
				restResult.setSuccess(false);
			}

			/*
			 * // generate new apiToken c.add(Calendar.DATE, 1); if
			 * (retailer.getApiUsername() != null) { String rawToken =
			 * retailer.getApiUsername() + "___" +
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); } else { String rawToken =
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); } try {
			 * masterdataService.saveRetailer(retailer);
			 * restResult.setApiToken(apiToken); } catch (Exception e) {
			 * 
			 * }
			 */

			restResult.setApiToken(apiToken);
		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		return restResult;
	}

	@RequestMapping(value = "/upload/delivery-proof", produces = "application/json")
	public RestResult uploadDeliveryProof(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("ticketNo") String consumerComplaintNumber, @RequestParam("processId") String processId,
			@RequestParam("variableName") String variableName) {

		// if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		RestResult restResult = new RestResult();

		try {
			restResult = TaskUtil.uploadProcessDetails(processId, consumerComplaintNumber, variableName);
		} catch (Exception ex) {
			log.error("Exception during completion of Bulk pickup", ex);
			restResult.setResponse(false);
			httpResponse.setStatus(500);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	@RequestMapping(value = "/dummy/update/ampm-variable", produces = "application/json")
	public RestResult updateDummyAMPMVariable(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("processId") String processId) {

		// if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		RestResult restResult = new RestResult();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			Map<String, Object> ampmIntermediateMap = new java.util.HashMap<String, Object>();

			Map<String, String> ampmMap = new java.util.HashMap<String, String>();
			String dummyRefNo = "BLR12300";
			ampmMap.put("variable", dummyRefNo); // Variable
			ampmMap.put("name", "Delivery to the drop location"); // Task
			ampmMap.put("docketNo", ""); // Docket No
			ampmMap.put("status", ""); // Status
			ampmMap.put("requestDate", ""); // Request Date
			ampmMap.put("cost", "0.00"); // Cost
			ampmIntermediateMap.put(dummyRefNo, ampmMap);

			runtimeService.setVariable(processId, "rlAMPMDetailsMap", ampmIntermediateMap);
		} catch (Exception ex) {
			log.error("Exception during completion of Bulk pickup", ex);
			restResult.setResponse(false);
			httpResponse.setStatus(500);
			restResult.setMessage(ex.getMessage());
		}
		return restResult;
	}

	@RequestMapping(value = "/update/ticket-status", produces = "application/json")
	public RestExternalResult updateTicketStatusFromRetailer(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam("referenceNo") String referenceNo,
			@RequestParam(value = "retailerCode", required = true) String retailerCode,
			@RequestParam(value = "apiToken", required = true) String apiToken) {

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			if (retailerCode != null) {
				retailer = masterdataService.createRetailerQuery().code(retailerCode).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					restResult.setApiToken(apiToken);
					throw new Exception("Invalid Retailer");
				}

				if (retailer.getApiToken() == null) {
					restResult.setSuccess(false);
					throw new Exception("Authentication not valid. Please login to generate apiToken");
				}

				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

				Timestamp currentDate = new Timestamp(c.getTime().getTime());
				if (currentDate.after(retailer.getTokenExpiryDate())) {
					restResult.setSuccess(false);
					throw new Exception("Authentication expired. Please login again");
				}
			}

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", referenceNo);
			String processId = null;
			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();

				Map<String, Object> processResponseWithVariables = runtimeService.getVariables(processId);
				if (processResponseWithVariables != null && String.valueOf(processResponseWithVariables.get("retailer"))
						.equalsIgnoreCase(retailer.getName())) {
					String statusVariable = String.valueOf(processResponseWithVariables.get("rlProcessStatus"));
					if (statusVariable != null) {
						statusVariable = "Delivery Confirmed";
						runtimeService.setVariable(processId, "rlProcessStatus", statusVariable);

						restResult.setResponse(true);
						restResult.setSuccess(true);
						restResult.setMessage("Status successfully updated");
					}
				}
			}

			if (processResults.size() == 0) {
				restResult.setMessage("Requested ticket not available");
				restResult.setSuccess(false);
			}

			// generate new apiToken
			c.add(Calendar.DATE, 1);
			if (retailer.getApiUsername() != null) {
				String rawToken = retailer.getApiUsername() + "___"
						+ RandomStringUtils.randomAlphanumeric(8).toLowerCase()
						+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
				apiToken = AuthUtil.cryptWithMD5(rawToken);
				retailer.setApiToken(apiToken);
				retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
			} else {
				String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
						+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
				apiToken = AuthUtil.cryptWithMD5(rawToken);
				retailer.setApiToken(apiToken);
				retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
			}
			try {
				masterdataService.saveRetailer(retailer);
				restResult.setApiToken(apiToken);
			} catch (Exception e) {

			}

		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		return restResult;
	}

	/**
	 * prem code This method is for update ticket status from AMPM application
	 * 
	 * @param httpRequest
	 * @param httpResponse
	 * @param queryJSON
	 * @return
	 */
	@RequestMapping(value = "/ampm/update/ticket-status", produces = "application/json")
	public RestResult updateTicketStatusFromAMPM(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("ticketNo") String ticketNo, @RequestParam("rlProcessId") String rlProcessId,
			@RequestParam("status") String status, @RequestParam("comment") String comment,
			@RequestParam(required = false, value = "customerCode") String customerCode) {
		log.debug("Request form AMPM  call logs------------------------------------------ticketNo " + ticketNo
				+ ", status:" + status + ", comment:" + comment);

		RestResult restResult = new RestResult();

		callToSavexApiForSendingStatus(ticketNo, rlProcessId, status, comment, restResult);

		return restResult;
	}

	private static void callToSavexApiForSendingStatus(String ticketNo_, String rlProcessId, String statusString,
			String comments, RestResult result) {

		try {

			String status = statusString;
			String comment = "";
			String _URL = "http://49.248.166.228/API/Bizlog/Track_Order.ashx";

			/*
			 * switch (statusString) { case "DROP_ASSIGNED": comment =
			 * "Asset is out for delivery."; status = "Out for Delivery";
			 * 
			 * break; case "CUSTOMER_REJECTION": comment =
			 * "Customer is rejected."; status = "Not Delivered"; break; case
			 * "DELIVERED": comment = "Product delivered."; status =
			 * "Delivered"; break; case "RESCHEDULE": comment = "Rescheduled";
			 * status = "In-Transit"; break; default: log.debug(
			 * "Savex Server call logs------------------------------------------ "
			 * + rlProcessId + " " + statusString); break;
			 * 
			 * }
			 */
			/*
			 * RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
			 * (ProcessEngines.getDefaultProcessEngine())) .getRuntimeService();
			 * Map<String, Object> variables =
			 * runtimeService.getVariables(rlProcessId); String ticketNo =
			 * String.valueOf(variables.get("rlTicketNo"));
			 */

			String final_url = _URL;

			String sbiStringTicketNo = ticketNo_.substring(0, 3);
			/**
			 * Call Api when the ticket is from Savex Retailer with Code : MDL
			 */
			if (sbiStringTicketNo.equals("SVX")) {
				log.debug("Savex Server call logs------------------------------------------ " + rlProcessId + " "
						+ statusString);

				JSONObject requestJson = new JSONObject();
				requestJson.put("Ticket_No", ticketNo_);
				requestJson.put("Status", status);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(final_url);
				httppost.addHeader("Content-Type", "application/json");

				log.debug("Savax status from server :".toUpperCase() + requestJson);

				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);

				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug("Savex status from server :".toUpperCase() + rawResponse);
				saveToConsoleLogsTable("Savex API CALL", "REQ:"+requestJson+" RES:"+rawResponse);
				result.setSuccess(true);
			} else {
				// Ticket is not for Savex Retailer
			}

		} catch (Exception e) {
			log.debug("NOT ABLE TO CALL MEDWELL API " + e);
			result.setSuccess(false);
		}

	}

	/*
	 * @RequestMapping(value = "/testing/LSP", produces = "application/json")
	 * public RestResult forTestingOnly(HttpServletRequest httpRequest,
	 * HttpServletResponse httpResponse,
	 * 
	 * @RequestParam("processId") String processIds) {
	 * 
	 * RestResult restResult=new RestResult();
	 * AMPMUtil.commanLSPDrop(processIds);
	 * 
	 * return restResult; }
	 */
	public static void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}
}
