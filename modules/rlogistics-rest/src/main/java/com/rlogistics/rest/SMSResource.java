package com.rlogistics.rest;

import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.sms.SMSSendCommand;
import com.rlogistics.sms.SMSSendResult;
import com.rlogistics.sms.SMSUtil;

@RestController
public class SMSResource extends RLogisticsResource {
	private static Logger log = LoggerFactory.getLogger(SMSResource.class);
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/sms/send",produces = "application/json")
	public SMSSendResult sendSMS(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam("command") String commandJson, @RequestParam("processId") String processId) {

		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		SMSSendCommand smsSendCommand = null;
		try {
			smsSendCommand = new ObjectMapper().readValue(URLDecoder.decode(commandJson),SMSSendCommand.class);
		} catch (Exception ex) {
			log.error("Exception during sendSMS", ex);
			SMSSendResult SMSSendResult = new SMSSendResult();
			SMSSendResult.getMessages().add(ex.getMessage());
			return SMSSendResult;
		}
		
		return SMSUtil.sendSMS(smsSendCommand,processId);
	}
}
