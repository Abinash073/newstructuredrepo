package com.rlogistics.rest;

import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.ServiceProviderLocationServiceCustomQueriesMapper;
import com.rlogistics.customqueries.ServiceProviderLocationServiceCustomQueryBrandResult;
import com.rlogistics.customqueries.ServiceProviderLocationServiceCustomQueryCategoryResult;
import com.rlogistics.customqueries.ServiceProviderLocationServiceCustomQuerySubCategoryResult;
import com.rlogistics.customqueries.ServiceProviderLocationServiceDetailsResult;

@RestController
public class ServiceProviderLocationServiceQueriesRestResource {

	@RequestMapping(value = "/services/filter-by-category/{serviceProviderLocationId}", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationServiceCustomQueryCategoryResult> query1(@PathVariable("serviceProviderLocationId") String serviceProviderLocationId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ServiceProviderLocationServiceCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationServiceCustomQueriesMapper.class);
			
			return mapper.findServicesByCategory(serviceProviderLocationId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/services/filter-by-sub-category/{serviceProviderLocationId}/{categoryId}", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationServiceCustomQuerySubCategoryResult> query2(@PathVariable("serviceProviderLocationId") String serviceProviderLocationId, @PathVariable("categoryId") String categoryId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ServiceProviderLocationServiceCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationServiceCustomQueriesMapper.class);
			
			return mapper.findServicesBySubCategory(serviceProviderLocationId,categoryId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/services/filter-by-brand/{serviceProviderLocationId}", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationServiceCustomQueryBrandResult> query3(@PathVariable("serviceProviderLocationId") String serviceProviderLocationId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ServiceProviderLocationServiceCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationServiceCustomQueriesMapper.class);
			
			return mapper.findServicesByBrand(serviceProviderLocationId);
		} finally {
			session.close();
		}
	}
}
