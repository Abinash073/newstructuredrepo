package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.FAQCustomQueriesMapper;
import com.rlogistics.customqueries.FAQCustomResult;
import com.rlogistics.customqueries.ResultCount;

@RestController
public class FAQCustomQueriesRestResource {

	@RequestMapping(value = {"faq/listing","faq/listing/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse findFAQs(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			FAQCustomQueriesMapper mapper = session.getMapper(FAQCustomQueriesMapper.class);
			List<FAQCustomResult> faqCustomResults = mapper.findFAQs(firstResult,maxResults);
			ResultCount resultCount = mapper.findFAQsCount();
			
			paginationResponse.setData(faqCustomResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
