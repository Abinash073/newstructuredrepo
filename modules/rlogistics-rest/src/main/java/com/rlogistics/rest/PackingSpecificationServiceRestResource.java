package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.PackingSpecificationResult;
import com.rlogistics.customqueries.PackingSpecificationServiceMapper;
import com.rlogistics.customqueries.ResultCount;

@RestController
public class PackingSpecificationServiceRestResource {
		
		@RequestMapping(value = {"/get/packaging-specification/lists","/get/packaging-specification/lists/{firstResult}/{maxResult}"}, method=RequestMethod.POST,produces = "application/json")
		PaginationResponse getPackagingSpecifications(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable Map<String,String> pathVariables){
			
			ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
			
			SqlSession session = pec.getSqlSessionFactory().openSession();
			PaginationResponse paginationResponse = new PaginationResponse();
			
			try {
				int firstResult = 0;
				int maxResults = 10;
				
				PackingSpecificationServiceMapper mapper = session.getMapper(PackingSpecificationServiceMapper.class);
				if(pathVariables.containsKey("firstResult")){
					 firstResult = Integer.parseInt(pathVariables.get("firstResult")); 
					 maxResults = Integer.parseInt(pathVariables.get("maxResult"));
					
				}
				System.out.println("First-Limit"+firstResult+"max Result"+maxResults);
				List<PackingSpecificationResult> packingSpecsRes= mapper.getPackingSpecificationServiceDetails(firstResult,maxResults);
				ResultCount resultCount = mapper.getPackingSpecificationCount();
				paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
				paginationResponse.setData(packingSpecsRes);
	           
				return paginationResponse;
			} finally {
				session.close();
			}
		}

}
