package com.rlogistics.rest;

//import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.ContentType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RLogisticsAttachment;
import com.rlogistics.master.identity.RLogisticsAttachment.RLogisticsAttachmentQuery;
import com.rlogistics.master.identity.TicketImages;

@RestController
public class RLogisticsAttachmentResource extends RLogisticsResource {
    private static Logger log = LoggerFactory.getLogger(RLogisticsAttachmentResource.class);

    @RequestMapping(value = "/attachment/upload", produces = "application/json")
    public AttachmentUploadResult upload(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                         @RequestParam(required = true, value = "file") MultipartFile file,
                                         @RequestParam(value = "key1") String key1,
                                         @RequestParam(value = "key2", defaultValue = "", required = false) String key2,
                                         @RequestParam(value = "key3", defaultValue = "", required = false) String key3,
                                         @RequestParam(value = "key4", defaultValue = "", required = false) String key4,
                                         @RequestParam(value = "longitude", defaultValue = "", required = false) String longitude,
                                         @RequestParam(value = "latitude", defaultValue = "", required = false) String latitude,
                                         @RequestParam(value = "location", defaultValue = "", required = false) String location) {
        AttachmentUploadResult result = new AttachmentUploadResult();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        // if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
        /**
         * this is the request is coming form mobile end:
         * params.put("key1","process");
         * params.put("key2",String.valueOf(processId)); params.put("key3",
         * photoPropertyId);
         * params.put("key4",String.valueOf(System.currentTimeMillis()).
         * substring(0, 10));
         */
        try {
            if (file.getBytes().length > 700) {
				imageSaveToS3(file, key2, key3, key4, longitude, latitude, location);
			}
            result.setSuccess(true);
        } catch (Exception e) {
            log.debug("IMAGE NOT STORE IN S3 OR DATABASE");
        }

        try {
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();

            RLogisticsAttachment attachment = masterdataService.newRLogisticsAttachment();

            attachment.setName(file.getOriginalFilename());
            attachment.setKey1(key1);

            if (key2 != null && !key2.equals("")) {
                attachment.setKey2(key2);
            }
            if (key3 != null && !key3.equals("")) {
                attachment.setKey3(key3);
            }
            if (key4 != null && !key4.equals("")) {
                attachment.setKey4(key4);
                long number = Long.parseLong(key4);
                long time = number * 1000;
                Timestamp t = new Timestamp(time);
                log.debug("TimeStamp : " + t);
                try {
                    runtimeService.setVariable(key2, "rlClosingDate", t);
                } catch (Exception e) {
                    log.debug("Not A Valid ProcessId So not able to assign the rlClosingDate" + e);
                }
            }
            if (longitude != null && !longitude.equals("")) {
                attachment.setLongitude(longitude);
            }
            if (latitude != null && !latitude.equals("")) {
                attachment.setLatitude(latitude);
            }
            if (location != null && !location.equals("")) {
                attachment.setLocation(location);
            }

            attachment.setContentType(file.getContentType());

            /* Copy uploaded bytes into a byte[] */
            ByteArrayOutputStream bytesOS = new ByteArrayOutputStream();
            InputStream bytesIS = file.getInputStream();
            byte[] buffer = new byte[1024];
            int numRead = 0;
            while ((numRead = bytesIS.read(buffer)) != -1) {
                bytesOS.write(buffer, 0, numRead);
            }
            bytesIS.close();
            bytesOS.flush();
            log.debug("Size of upload:" + bytesOS.toByteArray().length);

            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                    .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
            commandExecutor.execute(new Command<Boolean>() {
                @Override
                public Boolean execute(CommandContext commandContext) {
                    attachment.setContent(bytesOS.toByteArray());
                    masterdataService.saveRLogisticsAttachment(attachment);
                    return true;
                }
            });

            // link to ACTIVITY
            System.out.println("Start linking");
            ArrayList<String> photoVariableList = new ArrayList<String>();
            if (key1.equals("process")) {
                Map<String, Object> variables = runtimeService.getVariables(key2);
                if (variables != null) {
                    Object photoVariable = variables.get(key3);
                    if (photoVariable != null) {
                        if (photoVariable instanceof ArrayList) {
                            photoVariableList = (ArrayList<String>) photoVariable;
                            photoVariableList.add(key4);
                            runtimeService.setVariable(key2, key3, photoVariableList);
                        } else {
                            photoVariableList.add(key4);
                            runtimeService.setVariable(key2, key3, photoVariableList);
                        }
                    } else {
                        photoVariableList.add(key4);
                        runtimeService.setVariable(key2, key3, photoVariableList);
                    }
                }
            }

            result.setSuccess(true);
        } catch (Exception ex) {
            log.error("Exception during upload", ex);
            result.getMessages().add(ex.getMessage());
            /**
             * setting responce as true because after completing the task it
             * will remove form RU table in the database but image is not
             * uploading now image is storing in S3 so let it be sent success
             * responce so the ticket will get completed form mobile end
             */
            result.setSuccess(true);
            return result;
        }

        return result;
    }

    	@RequestMapping(value = { "/attachment/{command}/{key1}", "/attachment/{command}/{key1}/{key2}",
			"/attachment/{command}/{key1}/{key2}/{key3}",
			"/attachment/{command}/{key1}/{key2}/{key3}/{key4}" }, method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@PathVariable("command") String command, @PathVariable Map<String, String> pathVariables) {
		// if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}

		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
			return commandExecutor.execute(new Command<ResponseEntity<byte[]>>() {
				@Override
				public ResponseEntity<byte[]> execute(CommandContext commandContext) {

					byte[] content = null;
					String contentType = null;
					String contentName = null;

					// if(pathVariables.get("key1").equals("process")){
					// String processInstanceId = pathVariables.get("key2");
					// String variable = pathVariables.get("key3");
					// int index = pathVariables.containsKey("key4") ?
					// Integer.parseInt(pathVariables.get("key4")) : 0;
					//
					// RuntimeService runtimeService =
					// ((RLogisticsProcessEngineImpl)
					// (ProcessEngines.getDefaultProcessEngine()))
					// .getRuntimeService();
					//
					// ProcessInstance processInstance =
					// runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).includeProcessVariables().singleResult();
					//
					// Map<String,Object> variableValues = null;
					//
					// if(processInstance != null){
					// variableValues = processInstance.getProcessVariables();
					// } else {
					// /* Look for a historical instance */
					// HistoryService historyService =
					// ((RLogisticsProcessEngineImpl)
					// (ProcessEngines.getDefaultProcessEngine()))
					// .getHistoryService();
					//
					// HistoricProcessInstance hiProcessInstance =
					// historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).includeProcessVariables().singleResult();
					//
					// if(hiProcessInstance != null){
					// variableValues = hiProcessInstance.getProcessVariables();
					// }
					// }
					//
					// if(variableValues.containsKey(variable)){
					// Object value = variableValues.get(variable);
					// String textValue = null;
					//
					// if(value instanceof ArrayList){
					// log.debug("Instance of arrayList");
					// ArrayList<String> values = (ArrayList<String>) value;
					//
					// if(index < values.size()){
					// if(index < 0){
					// index = values.size() + index;
					// }
					//
					// textValue = values.get(index);
					// }
					// } else if(value instanceof String){
					// log.debug("Instance of String");
					// textValue = (String) value;
					// }
					//
					// content =
					// DatatypeConverter.parseBase64Binary(textValue.replaceAll("
					// ", "+"));
					// contentType = "image/png";
					// contentName = pathVariables.get("key3");
					// }
					// } else {
					RLogisticsAttachmentQuery query = masterdataService.createRLogisticsAttachmentQuery();

					query.key1(pathVariables.get("key1"));

					if (pathVariables.containsKey("key2")) {
						query.key2(pathVariables.get("key2"));
					}

					if (pathVariables.containsKey("key3")) {
						query.key3(pathVariables.get("key3"));
					}
					if (pathVariables.containsKey("key4")) {
						query.key4(pathVariables.get("key4"));
					}

					RLogisticsAttachment attachment = query.singleResult();

					if (attachment != null) {
						content = attachment.getContent();
						contentType = attachment.getContentType();
						contentName = attachment.getName();
					}
					// }

					if (content != null) {
						HttpHeaders headers = new HttpHeaders();
						MimeType mimeType = MimeTypeUtils.parseMimeType(contentType);
						headers.setContentType(
								new MediaType(mimeType.getType(), mimeType.getSubtype(), mimeType.getParameters()));

						if (command.equals("download")) {
							headers.set("Content-Disposition", "attachment; filename=" + contentName);
						}

						return new ResponseEntity<>(content, headers, HttpStatus.OK);
					} else {
						httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
						return null;
					}
				}
			});

		} catch (Exception ex) {
			log.error("Exception during upload", ex);
			httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}
    @Autowired
    MasterdataService masterdataService;


    /**
     * Author  : Adarsh / 27 oct 2018
     * @param httpRequest
     * @param httpResponse
     * @param processId
     * @param pathVariables key1: process | key2:processId |key3:Attachment Name|key4:Attachment identifier
     * @return
     */
    @RequestMapping(value ="get/photos", method = RequestMethod.GET)
    public List<TicketImages> downloadImage(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                            @RequestParam("processId") String processId, @PathVariable Map<String, String> pathVariables) throws IOException {
        //Session Authentication
//        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
//            httpResponse.setStatus(400);
//            return null;
//        }

        //Fetch Data according to processId
        List<TicketImages> ticketImages = masterdataService.createTicketImagesQuery().procInstId(processId).list();
        return  ticketImages;


        //TODO - will send a string of link <TO decide wht to send in list of link>

    }

    @RequestMapping(value = {"/attachment/browse", "/attachment/browse/{key1}",
            "/attachment/browse/{key1}/{key2}"}, method = RequestMethod.GET)
    public RLogisticsAttachmentBrowseResult browse(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                   @PathVariable Map<String, String> pathVariables) {
        Set<String> leafEntries = new TreeSet<>();
        Set<String> nonLeafEntries = new TreeSet<>();

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();

        RLogisticsAttachmentQuery query = masterdataService.createRLogisticsAttachmentQuery();

        String lookingFor = "key1";

        if (pathVariables.containsKey("key1")) {
            query.key1(pathVariables.get("key1"));
            lookingFor = "key2";
        }

        if (pathVariables.containsKey("key2")) {
            query.key2(pathVariables.get("key2"));
            lookingFor = "key3";
        }

        List<RLogisticsAttachment> matches = query.list();

        for (RLogisticsAttachment match : matches) {
            if (lookingFor.equals("key1") && (match.getKey1() != null)) {
                if (match.getKey2() == null) {
                    leafEntries.add(match.getKey1());
                } else {
                    nonLeafEntries.add(match.getKey1());
                }
            } else if (lookingFor.equals("key2") && (match.getKey2() != null)) {
                if (match.getKey3() == null) {
                    leafEntries.add(match.getKey2());
                } else {
                    nonLeafEntries.add(match.getKey2());
                }
            } else if (lookingFor.equals("key3") && (match.getKey3() != null)) {
                leafEntries.add(match.getKey3());
            }
        }

        return new RLogisticsAttachmentBrowseResult(leafEntries, nonLeafEntries);
    }

    @RequestMapping(value = "/attachment/upload1", produces = "application/json")
    public RestExternalResult upload1(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                      @RequestParam(required = true, value = "file") MultipartFile file,
                                      @RequestParam(value = "processId", defaultValue = "", required = true) String processId) {
        RestExternalResult result = new RestExternalResult();

        // result = imageSaveToS3(file, processId);

        return result;
    }

    public RestExternalResult imageSaveToS3(MultipartFile file, String processId, String photoProperty,
                                            String timeStamp, String longitude, String latitude, String location) {
        String AccessKey = "AKIAJDLZ4JN2IUXXVRZQ";
        String SecretKey = "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";
        String bucketName = "revlog";
        String ticketNo = "NOTICKET";
        String bucketURL = "https://s3-us-west-2.amazonaws.com/revlog/";

        String finalFileName = "";
        RestExternalResult result = new RestExternalResult();
        try {

            ticketNo = getTicketNoByProcessId(processId);
            InetAddress IP = InetAddress.getLocalHost();
            File convFile = new File(file.getOriginalFilename());
            file.transferTo(convFile);

            // log.debug("Size of upload:" + bytesOS.toByteArray().length);
            // log.debug("File Name:" +file.getOriginalFilename());
            // log.debug("Content Type:" + file.getContentType());
            log.debug("RLogisticsAttachmentResource");

            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd");
            String strDate = formatter.format(date);
            finalFileName = IP.getHostAddress() + "/" + strDate + "/" + ticketNo + "/" + photoProperty + "/"
                    + randomAlphaNumeric(5) + file.getOriginalFilename();

            BasicAWSCredentials awsCreds = new BasicAWSCredentials(AccessKey, SecretKey);
            AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
            try {
                File inFile = convFile;
                FileInputStream fis = null;
                fis = new FileInputStream(inFile);

                s3client.putObject(new PutObjectRequest(bucketName, finalFileName, fis, new ObjectMetadata()));

                log.debug("Error Message:    " + "DONE");

                /**
                 * PutObjectRequest(String,String,String) we have send image
                 * directly with stream
                 */
            } catch (AmazonServiceException ase) {
                log.debug("Caught an AmazonServiceException, which " + "means your request made it "
                        + "to Amazon S3, but was rejected with an error response" + " for some reason.");
                log.debug("Error Message:    " + ase.getMessage());
                log.debug("HTTP Status Code: " + ase.getStatusCode());
                log.debug("AWS Error Code:   " + ase.getErrorCode());
                log.debug("Error Type:       " + ase.getErrorType());
                log.debug("Request ID:       " + ase.getRequestId());
            } catch (AmazonClientException ace) {
                log.debug("Caught an AmazonClientException, which " + "means the client encountered "
                        + "an internal error while trying to " + "communicate with S3, "
                        + "such as not being able to access the network.");
                log.debug("Error Message: " + ace.getMessage());
            }
            /**
             * Storing link to database
             */
            addInTicketImagesTable(bucketURL + finalFileName, ticketNo, processId, photoProperty, timeStamp, latitude,
                    longitude, location);
            callToMedwellAPIForSendingImages(processId, photoProperty, bucketURL + finalFileName);
            result.setMessage(bucketURL + finalFileName);
            result.setSuccess(true);
        } catch (Exception ex) {
            log.error("Exception during upload", ex);
            result.setResponse(ex);
            return result;
        }
        return result;

    }

    private void addInTicketImagesTable(String imageUrl, String ticketNo, String processId, String photoProperty,
                                        String timeStamp, String latitude, String longitude, String location) {
        try {
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();

            TicketImages ticketImages = masterdataService.newTicketImages();
            UUID id = UUID.randomUUID();
            ticketImages.setId(String.valueOf(id));
            ticketImages.setImageType(photoProperty);
            ticketImages.setImageUrl(imageUrl);
            ticketImages.setProcInstId(processId);
            ticketImages.setTicketNo(ticketNo);
            ticketImages.setCreateOn(timeStamp);
            ticketImages.setLat(latitude);
            ticketImages.setLongitude(longitude);
            ticketImages.setLocation(location);
			ticketImages.setCreateTimestamp(getCurrentTimeStamp());

            masterdataService.saveTicketImages(ticketImages);
        } catch (Exception e) {
            log.debug("IMAGE NOT SAVE TO DATABASE");
        }

    }

    private String getCurrentTimeStamp() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return timestamp + "";
	}

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    private String getTicketNoByProcessId(String processId) {
        String ticketNo = "";
        try {

            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
        } catch (Exception e) {
            /**
             * If ticket number is not found in the ACT_RU_ table it will add
             * random ticket number
             */
            ticketNo = randomAlphaNumeric(5);
        }
        return ticketNo;
    }

    private void callToMedwellAPIForSendingImages(String processId, String image_type, String image_url) {

        try {

            log.debug("Medwell Server call logs------------------------------------------ " + image_url + " "
                    + processId + " " + image_type);
            String status = "";
            String comment = "";
            //  "https://qa1-api.nightingales.in/v2/wms/shipments/";
            String _URL = "https://api.nightingales.in/v2/wms/shipments/";//prod url

            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            Map<String, Object> variables = runtimeService.getVariables(processId);
            String ticketNo = String.valueOf(variables.get("rlTicketNo"));
            String final_url = _URL + ticketNo + "/documents";

            String sbiStringTicketNo = ticketNo.substring(0, 3);
            /**
             * Call Api when the ticket is from MEDWELL Retailer with Code : MDL
             */
            if (sbiStringTicketNo.equals("MDL")) {

                JSONObject requestJson = new JSONObject();
                requestJson.put("image_type", image_type);
                requestJson.put("image_url", image_url);

                // API call
                CloseableHttpClient httpclient = HttpClients.createDefault();
                HttpPut httppost = new HttpPut(final_url);
                httppost.addHeader("Content-Type", "application/json");
                //change URL in RetailerApiCall also
				//httppost.addHeader("NHHS-Client-Id", "test");
                // httppost.addHeader("NHHS-Client-Secret", "test");
				httppost.addHeader("nhhs-client-id", "6EF55494FD9EFCBFEB96C2462EFDD");//medwell prod
				httppost.addHeader("nhhs-client-secret", "bash ");//medwell prod																			// prod

                log.debug("MEDWELL status from image server :".toUpperCase() + requestJson);
				saveToConsoleLogsTable("Medwell-" + ticketNo,  ":" + "Request: "+final_url+" " + requestJson);

                // Set HTTP entity
                httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
                // Get Response
                CloseableHttpResponse httpResponse = httpclient.execute(httppost);

                String rawResponse = EntityUtils.toString(httpResponse.getEntity());
                log.debug("Medwell status from FOR IMAGE server :".toUpperCase() + rawResponse);
            saveToConsoleLogsTable("Medwell-" + ticketNo,  ":" + "Response: "+" " + rawResponse);} else {
                // Ticket is not for Medwell Retailer
            }

        } catch (Exception e) {
            log.debug("NOT ABLE TO CALL MEDWELL IMAGE API " + e);

        }

    }
	public static void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}
}
