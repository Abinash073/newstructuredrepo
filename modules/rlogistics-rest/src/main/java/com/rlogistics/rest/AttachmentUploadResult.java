package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.List;

public class AttachmentUploadResult {
	private boolean success = false;
	private List<String> messages = new ArrayList<>();
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
}
