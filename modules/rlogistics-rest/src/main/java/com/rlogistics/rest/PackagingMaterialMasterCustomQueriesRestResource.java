package com.rlogistics.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.BarcodeGenerationResult;
import com.rlogistics.customqueries.InventorySearchResponse;
import com.rlogistics.customqueries.PackagingInventoryObject;
import com.rlogistics.customqueries.PackagingInventoryResult;
import com.rlogistics.customqueries.PackagingInventoryResultCount;
import com.rlogistics.customqueries.PackagingMaterialMasterCustomQueriesMapper;
import com.rlogistics.customqueries.PackagingMaterialResult;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.master.identity.ImeiInventory;
//import com.rlogistics.packaging.MasterdataService;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class PackagingMaterialMasterCustomQueriesRestResource {
	private static Logger log = LoggerFactory.getLogger(PackagingMaterialMasterCustomQueriesRestResource.class);

	@RequestMapping(value = {"/get/packaging-material/list","/get/packaging-material/list/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse findPackagingMaterials(@RequestParam("query") String queryJSON, @PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		String locationId = "";
		String packagingTypeCode = "";
		String packagingType = "";
		String firstResult = "0";
		String maxResults = "25";
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			Map<String, String> queryObj = new HashMap<String,String>();
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PackagingMaterialMasterCustomQueriesMapper mapper = session.getMapper(PackagingMaterialMasterCustomQueriesMapper.class);
			
			if(queryObj.containsKey("locationId")) {
				locationId = queryObj.get("locationId");
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingTypeCode = queryObj.get("packagingTypeCode");
			}
			if(queryObj.containsKey("packagingType")) {
				packagingType = String.valueOf(queryObj.get("packagingType"));
			}
			if(pathVariables.containsKey("firstResult")) {
				firstResult = pathVariables.get("firstResult");
				maxResults = pathVariables.get("maxResults");
			}
			List<PackagingMaterialResult> packagingMaterials = mapper.findPackagingMaterials(locationId, packagingTypeCode, packagingType, firstResult, maxResults);
			ResultCount resultCount = mapper.findPackagingMaterialsCount(locationId, packagingTypeCode, packagingType);
			
			paginationResponse.setData(packagingMaterials);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/get/packaging-inventory/details", method=RequestMethod.POST,produces = "application/json")
	List<PackagingInventoryResult> getPackagingInventory(){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			
			PackagingMaterialMasterCustomQueriesMapper mapper = session.getMapper(PackagingMaterialMasterCustomQueriesMapper.class);
			
			return mapper.getPackagingInventory();
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "get/packaging-inventory/{barcode}", method=RequestMethod.POST,produces = "application/json")
	PackagingInventoryObject getPackagingInventoryByBarcode(@PathVariable("barcode") String barcode){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			
			PackagingMaterialMasterCustomQueriesMapper mapper = session.getMapper(PackagingMaterialMasterCustomQueriesMapper.class);
			
			return mapper.getPackagingInventoryByBarcode(barcode);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/get/packaging-inventory/details/{pageNo}/{maxResults}", method=RequestMethod.POST,produces = "application/json")
	InventorySearchResponse getPackagingInventoryBySearch(@RequestParam("query") String queryJSON, @PathVariable(value ="pageNo") int pageNo, @PathVariable(value ="maxResults") String maxResults){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		String barcode = "";
		String packagingTypeCode = "";
		String locationId = "";
		String packagingType = "";
		String status = "";
		String ticketNumber = "";
		String imeiNo = "";
		
		
		int offset = (pageNo - 1) * Integer.valueOf(maxResults);
		String firstResult = String.valueOf(offset);
		
		try {
			Map<String, String> queryObj = new HashMap<String,String>();
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PackagingMaterialMasterCustomQueriesMapper mapper = session.getMapper(PackagingMaterialMasterCustomQueriesMapper.class);
			
			if(queryObj.containsKey("barcode")) {
				barcode = queryObj.get("barcode");
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingTypeCode = queryObj.get("packagingTypeCode");
			}
			if(queryObj.containsKey("locationId")) {
				locationId = queryObj.get("locationId");
			}
			if(queryObj.containsKey("packagingType")) {
				packagingType = String.valueOf(queryObj.get("packagingType"));
			}
			if(queryObj.containsKey("status")) {
				status = String.valueOf(queryObj.get("status"));
			}
			if(queryObj.containsKey("ticketNumber")) {
				ticketNumber = String.valueOf(queryObj.get("ticketNumber"));
			}
			if(queryObj.containsKey("imeiNo")) {
				imeiNo = String.valueOf(queryObj.get("imeiNo"));
			}
			
			List<PackagingInventoryResult> packagingInventoryResult = mapper.getPackagingInventorySearch(barcode, packagingTypeCode, locationId, packagingType, status,ticketNumber,imeiNo, firstResult, maxResults);
			
			PackagingInventoryResultCount packagingInventoryResultCount = mapper.getPackagingInventorySearchCount(barcode, packagingTypeCode, locationId, packagingType, status,ticketNumber,imeiNo);
			int totalRecords = Integer.valueOf(packagingInventoryResultCount.getTotalRecords());
			
			InventorySearchResponse inventorySearchResponse = new InventorySearchResponse();
			inventorySearchResponse.setData(packagingInventoryResult);
			inventorySearchResponse.setTotalRecords(totalRecords);
			
			return inventorySearchResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/barcode-generation/list","/barcode-generation/list/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getGeneratedBarcodes(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			PackagingMaterialMasterCustomQueriesMapper mapper = session.getMapper(PackagingMaterialMasterCustomQueriesMapper.class);
			List<BarcodeGenerationResult> barcodeGenerationResults = mapper.getGeneratedBarcodes(firstResult,maxResults);
			paginationResponse.setData(barcodeGenerationResults);
			
			ResultCount resultCount = mapper.getGeneratedBarcodesCount();
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	/**
	 * UPDATING THE IMEI STATUS
	 * */
	@RequestMapping(value = "/update/imei-inventory", method=RequestMethod.POST,produces = "application/json")
	RestResult updateIMEIStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,@RequestParam("query") String queryJSON){
		
		RestResult restResponse = new RestResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		Map<String, String> queryObj = new HashMap<String, String>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//UPDATING IMEI INVENTORY TABLE
		try {
			String imeiNo = "";
			int status;
			imeiNo = queryObj.get("imeiNo");
			 status = Integer.valueOf(queryObj.get("status"));
			ImeiInventory imeiInventory = masterdataService.createImeiInventoryQuery()
					.imeiNo(imeiNo)
					.singleResult();
			if (imeiInventory != null) {
				imeiInventory.setStatus(status);
				try {
					masterdataService.saveImeiInventory(imeiInventory);
				} catch (Exception e) {
					log.debug("Error in updating Imei Inventory" + e);
					System.out.println("Error in updating Imei Inventory" + e);
				}
				restResponse.setSuccess(true);
				restResponse.setMessage("Status updated successfully");
			} else {
				log.debug("IMEI Is not Present");
				restResponse.setSuccess(true);
				restResponse.setMessage("IMEI Is not Present");
			}
		} catch (Exception e) {
			log.debug("Error in calling Imei Inventory" + e);
			System.out.println("Error in calling Imei Inventory" + e);
		}	
		return restResponse;
		
	}
	
}
