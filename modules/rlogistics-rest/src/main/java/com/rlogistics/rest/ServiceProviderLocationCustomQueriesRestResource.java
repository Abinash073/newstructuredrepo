package com.rlogistics.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.RetailerServiceProvidersResult;
import com.rlogistics.customqueries.ServiceProviderDetailsResult;
import com.rlogistics.customqueries.ServiceProviderLocationCustomQueriesMapper;
import com.rlogistics.customqueries.ServiceProviderLocationCustomQuery1Result;
import com.rlogistics.customqueries.ServiceProviderLocationRetailerResult;
import com.rlogistics.customqueries.ServiceProviderLocationServiceDetailsResult;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class ServiceProviderLocationCustomQueriesRestResource {

	@RequestMapping(value = "/serviceproviderlocation/customqueries/get/servicelocations-retailer", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationRetailerResult> findServiceLocationsByRetailer(@RequestParam("query") String queryJSON){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		SqlSession session = pec.getSqlSessionFactory().openSession();
		String retailerId = "";
		String categoryId = "";
		String brandId = "";
		String subCategoryId = "";
		String cityId = "";
		String isAuthorizedServiceProvider = "";
		String pincode = "";
		
		try {
			Map<String, String> queryObj = new HashMap<String,String>();
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ServiceProviderLocationCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationCustomQueriesMapper.class);
			
			retailerId = queryObj.get("retailerId");
			if(queryObj.containsKey("categoryId")) {
				categoryId = queryObj.get("categoryId");
			}
			if(queryObj.containsKey("brandId")) {
				brandId = queryObj.get("brandId");
			}
			if(queryObj.containsKey("subCategoryId")) {
				subCategoryId = queryObj.get("subCategoryId");
			}
			if(queryObj.containsKey("cityId")) {
				cityId = queryObj.get("cityId");
				if(cityId.equalsIgnoreCase("10351")) {
					cityId = "5061";
				}
			}
			if(queryObj.containsKey("isAuthorizedServiceProvider")) {
				isAuthorizedServiceProvider = queryObj.get("isAuthorizedServiceProvider");
			}
			if(queryObj.containsKey("pincode")) {
				pincode = queryObj.get("pincode");
			}
			
			return mapper.findServiceLocationsByRetailer(retailerId,categoryId,brandId,cityId,subCategoryId,isAuthorizedServiceProvider,pincode);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/serviceproviderlocation/customqueries/get/servicelocations-bizlog", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationCustomQuery1Result> findServiceLocationsByBizlog(@RequestParam("query") String queryJSON){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		String isBizlogAuthorized = "";
		String categoryId = "";
		String brandId = "";
		String subCategoryId = "";
		String cityId = "";
		String isAuthorizedServiceProvider = "";
		
		try {
			Map<String, String> queryObj = new HashMap<String,String>();
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ServiceProviderLocationCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationCustomQueriesMapper.class);
			if(queryObj.containsKey("isBizlogAuthorized")) {
				isBizlogAuthorized = queryObj.get("isBizlogAuthorized");
			}
			if(queryObj.containsKey("categoryId")) {
				categoryId = queryObj.get("categoryId");
			}
			if(queryObj.containsKey("brandId")) {
				brandId = queryObj.get("brandId");
			}
			if(queryObj.containsKey("subCategoryId")) {
				subCategoryId = queryObj.get("subCategoryId");
			}
			if(queryObj.containsKey("cityId")) {
				cityId = queryObj.get("cityId");
			}
			if(queryObj.containsKey("isAuthorizedServiceProvider")) {
				isAuthorizedServiceProvider = queryObj.get("isAuthorizedServiceProvider");
			}
			
			return mapper.findServiceLocationsByBizlog(isBizlogAuthorized,categoryId,brandId,subCategoryId,cityId,isAuthorizedServiceProvider);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/servicelocations/by-service-provider/{serviceProviderId}", method=RequestMethod.POST,produces = "application/json")
	List<ServiceProviderLocationServiceDetailsResult> findServiceLocationsByProvider(@PathVariable("serviceProviderId") String serviceProviderId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ServiceProviderLocationCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationCustomQueriesMapper.class);
			
			return mapper.findServiceLocationDetails(serviceProviderId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/service/providers/list","/service/providers/list/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse findServiceProviders(@RequestParam("query") String queryJSON, @PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		String serviceProviderId = "";
		
		Map<String, String> queryObj = new HashMap<String,String>();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		String firstResult = "0";
		String maxResults = "25";
		try {
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = pathVariables.get("firstResult");
				maxResults = pathVariables.get("maxResults");
			}
			
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			if(queryObj.size()>0) {
				serviceProviderId = queryObj.get("serviceProviderId");
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			ServiceProviderLocationCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationCustomQueriesMapper.class);
			List<ServiceProviderDetailsResult> serviceProviderDetailsResults = mapper.findServiceProviderDetails(serviceProviderId,firstResult,maxResults);
			ResultCount resultCount = mapper.findServiceProviderDetailsCount(serviceProviderId);
			
			paginationResponse.setData(serviceProviderDetailsResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/retailer/service-providers/{retailerId}/{firstResult}/{maxResults}", method=RequestMethod.POST,produces = "application/json")
	PaginationResponse findRetailerServiceProviders(@PathVariable("retailerId") String retailerId,@PathVariable("firstResult") int firstResult,@PathVariable("maxResults") int maxResults){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			ServiceProviderLocationCustomQueriesMapper mapper = session.getMapper(ServiceProviderLocationCustomQueriesMapper.class);
			List<RetailerServiceProvidersResult> retailerServiceProvidersResults = mapper.findRetailerServiceProviders(retailerId, firstResult, maxResults);
			ResultCount resultCount = mapper.findRetailerServiceProvidersCount(retailerId);
			
			paginationResponse.setData(retailerServiceProvidersResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
