package com.rlogistics.rest;

import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.RetailerSmsEmailTriggerConfigCustomQueriesMapper;
import com.rlogistics.customqueries.SmsEmailConfigResult;

@RestController
public class RetailerSmsEmailTriggerConfigCustomQueriesRestResource {

	@RequestMapping(value = "get/retailer/sms-email-configuration", method=RequestMethod.POST,produces = "application/json")
	List<SmsEmailConfigResult> getConfiguration(){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerSmsEmailTriggerConfigCustomQueriesMapper mapper = session.getMapper(RetailerSmsEmailTriggerConfigCustomQueriesMapper.class);
			
			return mapper.getConfiguration();
		} finally {
			session.close();
		}
	}
}
