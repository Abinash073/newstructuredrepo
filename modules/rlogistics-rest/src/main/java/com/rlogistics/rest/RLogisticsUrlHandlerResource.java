package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.scripting.ScriptingEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RLogisticsUrlHandler;
import com.rlogistics.master.identity.RLogisticsUrlHandler.RLogisticsUrlHandlerQuery;
import com.rlogistics.util.MapFieldBasedVariableScope;
import com.rlogistics.util.VariableScopeBase;

@RestController
public class RLogisticsUrlHandlerResource extends RLogisticsResource{

	private static Logger log = LoggerFactory.getLogger(RLogisticsUrlHandlerResource.class);

	@RequestMapping(value = {"/urlhandler/{name}","/urlhandler/{name}/{arg0}","/urlhandler/{name}/{arg0}/{arg1}","/urlhandler/{name}/{arg0}/{arg1}/{arg2}","/urlhandler/{name}/{arg0}/{arg1}/{arg2}/{arg3}","/urlhandler/{name}/{arg0}/{arg1}/{arg2}/{arg3}/{arg4}","/urlhandler/{name}/{arg0}/{arg1}/{arg2}/{arg3}/{arg4}","/urlhandler/{name}/{arg0}/{arg1}/{arg2}/{arg3}/{arg4}/{arg5}"},produces = "application/json")
	public Object executeUrlHandler(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("name") String name,@PathVariable Map<String,String> pathVariables) {

//		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();

		RLogisticsUrlHandlerQuery query = masterdataService.createRLogisticsUrlHandlerQuery();

		for(String k:pathVariables.keySet()){
			log.debug("XXXXXXXXXXXXXXXXXXX PV " + k +":" + pathVariables.get(k));
		}
		
		for(String k:httpRequest.getParameterMap().keySet()){
			log.debug("XXXXXXXXXXXXXXXXXXX PP " + k +":" + httpRequest.getParameterMap().get(k)[0]);
		}
		
		query.name(name);
		
		RLogisticsUrlHandler urlhandler = query.singleResult();
		
		Object retval = null;
		
		if(urlhandler != null){
			CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
			retval = commandExecutor.execute(new Command<Object>() {
				@Override
				public Object execute(CommandContext commandContext) {		
					if((urlhandler.getHandler() != null) && !(urlhandler.getHandler().equals(""))) {			
						ScriptingEngines scriptingEngines = org.activiti.engine.impl.context.Context.getProcessEngineConfiguration().getScriptingEngines();
						
						try{
							VariableScope defaultScope = new MapFieldBasedVariableScope();
							
							defaultScope.setVariable("pathVariables", pathVariables);

							if(urlhandler.getContextDefiner() != null){								
								@SuppressWarnings("unchecked")
								VariableScope scope = (VariableScope) scriptingEngines.evaluate(urlhandler.getContextDefiner(), "javascript",((VariableScopeBase) defaultScope).getBindingsAdapter());
								
								if(scope != null){
									defaultScope = scope;
								}
							}

							defaultScope.setVariable("pathVariables", pathVariables);
							defaultScope.setVariable("postParameters", makeMapOfList(httpRequest.getParameterMap()));
							
							try {
								if(defaultScope instanceof VariableScopeBase){
									return scriptingEngines.evaluate(urlhandler.getHandler(), "javascript",((VariableScopeBase)defaultScope).getBindingsAdapter());
								} else {
									return scriptingEngines.evaluate(urlhandler.getHandler(), "javascript",defaultScope,true);
								}
							} finally{
								defaultScope.removeVariable("pathVariables");
								defaultScope.removeVariable("postParameters");
							}
						}catch(Throwable th){
							log.debug("Supressing scripting layer exception:" + th.getMessage(), th);
							return null;
						}
					} else {
						return new Boolean(true);
					}
				}

				private Object makeMapOfList(Map<String, String[]> mapOfArray) {
					Map<String,List<String>> mapOfList = new HashMap<>();
					
					for(String k:mapOfArray.keySet()){
						String[] array = mapOfArray.get(k);
						List<String> list = new ArrayList<>();
						
						for(String v:array){
							list.add(v);
						}
						
						mapOfList.put(k, list);
					}
					
					return mapOfList;
				}
			});
		}
		
		return retval;
	}
}
