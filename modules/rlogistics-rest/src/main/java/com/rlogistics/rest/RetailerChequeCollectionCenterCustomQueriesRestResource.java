package com.rlogistics.rest;

import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.ChequeCollectionCenterCitiesResult;
import com.rlogistics.customqueries.ChequeCollectionResult;
import com.rlogistics.customqueries.RetailerChequeCollectionCenterCustomQueriesMapper;

@RestController
public class RetailerChequeCollectionCenterCustomQueriesRestResource {
	
	@RequestMapping(value = "/retailer-cheque-collection-centers-cities/customqueries/{retailerId}", method=RequestMethod.POST,produces = "application/json")
	List<ChequeCollectionCenterCitiesResult> getCityList(@PathVariable("retailerId") String retailerId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerChequeCollectionCenterCustomQueriesMapper mapper = session.getMapper(RetailerChequeCollectionCenterCustomQueriesMapper.class);
			
			return mapper.findCities(retailerId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/collectioncenters/customqueries/{retailerId}/{cityId}", method=RequestMethod.POST,produces = "application/json")
	List<ChequeCollectionResult> getRetailerCollectionCenters(@PathVariable("retailerId") String retailerId, @PathVariable("cityId") String cityId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerChequeCollectionCenterCustomQueriesMapper mapper = session.getMapper(RetailerChequeCollectionCenterCustomQueriesMapper.class);
			
			return mapper.findRetailerCollectionCenters(retailerId,cityId);
		} finally {
			session.close();
		}
	}

}
