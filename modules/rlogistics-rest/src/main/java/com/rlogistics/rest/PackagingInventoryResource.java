package com.rlogistics.rest;

import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.packaging.PackagingMaterialInventoryUtil;

@RestController
public class PackagingInventoryResource extends RLogisticsResource{

	private static Logger log = LoggerFactory.getLogger(PackagingInventoryResource.class);
	
	@RequestMapping(value = "/packaging-inventory/status/update",produces = "application/json")
	public RestResult sendEmail(@RequestParam("packagingMaterialInventory") String queryJSON, HttpServletRequest httpRequest ,HttpServletResponse httpResponse) throws Exception{

		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		Map<String, String> queryObj = null;
		String barcode = null;
		int status = -1;
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			barcode = queryObj.get("barcode");
			status = Integer.valueOf(String.valueOf(queryObj.get("status")));
			
		} catch (Exception ex) {
			log.error("Exception during sendEmail", ex);
			RestResult restResponse = new RestResult();
			restResponse.setMessage(ex.getMessage());
			return restResponse;
		}
		
		return PackagingMaterialInventoryUtil.updateStatus(barcode,status);
	}
}
