package com.rlogistics.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.activiti.metadata.impl.MetadataStreamProcessor;

@RestController
public class MetadataUploadResource {

    @Autowired
    protected MetadataService metadataService;

    @Autowired
    protected IdentityService identityService;

    @Autowired
    protected RepositoryService repositoryService;

    @RequestMapping(value = "/metadata/upload", method = RequestMethod.POST, produces = "application/json")
    public MetadataUploadResult uploadMetadata(@RequestParam(required = true, value = "file") MultipartFile file, @RequestParam(value = "id") String id, @RequestParam(value = "password") String password) {
        MetadataUploadResult result = new MetadataUploadResult();
        //Hardcoding this
        Map<String, String> user = new HashMap<>();
        user.put("admin@bizlog.in", "Bizlog@123$");
        Map<String, String> entered = new HashMap<>();

        entered.put(id, password);
        if (!user.equals(entered)) {
            result.setSuccess(false);
            result.setMessage("Invalid Credentials");
            return result;
        }


        try {
            JarInputStream jarIS = new JarInputStream(file.getInputStream());

            JarEntry entry = null;
            Set<String> datasets = new HashSet<String>();
            while ((entry = jarIS.getNextJarEntry()) != null) {

                DatasetEntry datasetEntry = new DatasetEntry(entry.getName());

                if (!datasets.contains(datasetEntry.getDataset())) {
                    /* If the dataset does not exist create one*/
                    ProcessDataset dataset = metadataService.createProcessDatasetQuery().name(datasetEntry.getDataset()).singleResult();

                    if (dataset == null) {
                        dataset = metadataService.newProcessDataset();
                        dataset.setName(datasetEntry.getDataset());
                        metadataService.saveProcessDataset(dataset);
                    }

                    /* If the dataset has a previous revision update it. if not create a new revision*/
                    ProcessDatasetRevision datasetRevision = metadataService.createProcessDatasetRevisionQuery().dataset(datasetEntry.getDataset()).singleResult();
                    if (datasetRevision == null) {
                        datasetRevision = metadataService.newProcessDatasetRevision(datasetEntry.getDataset(), datasetEntry.getDsVersion());
                    } else {
                        datasetRevision.setDsVersion(datasetEntry.getDsVersion());
                    }

                    metadataService.saveProcessDatasetRevision(datasetRevision);
                    datasets.add(datasetEntry.getDataset());
                }

                if (DatasetEntry.CSV.equals(datasetEntry.getType())) {
                    MetadataStreamProcessor.processUpload(datasetEntry.getDataset(), datasetEntry.getDsVersion(), jarIS);
                } else if (datasetEntry.getType().endsWith(DatasetEntry.XML)) {
                    DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().name(datasetEntry.getName());
                    deploymentBuilder.addInputStream(datasetEntry.getFileName(), jarIS);
                    Deployment deployment = deploymentBuilder.deploy();

                    ProcessDeployment pDeployment = metadataService.newProcessDeployment(datasetEntry.getDataset(), datasetEntry.getDsVersion());
                    pDeployment.setDeploymentId(deployment.getId());
                    pDeployment.setName(datasetEntry.getName());
                    metadataService.saveProcessDeployment(pDeployment);
                    result.setMessage("Successfully Uploaded");
                    result.setSuccess(true);
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException("Error reading fdupload jar file", ex);
        }
        return result;
    }

    private class DatasetEntry {

        public DatasetEntry(String entryName) {
            Matcher matcher = Pattern.compile("^(.+)-([^/]+)/(([^.]+)(?:\\.[^.]+)?\\.([^.]+))$").matcher(entryName);

            if (matcher.matches()) {
                dataset = matcher.group(1);
                dsVersion = matcher.group(2);
                type = matcher.group(5).equals(CSV) ? CSV : (matcher.group(5).equals(XML) ? XML : null);
                name = matcher.group(4);
                fileName = matcher.group(3);
            }
        }

        public static final String CSV = "csv";
        public static final String XML = "xml";

        public String getDataset() {
            return dataset;
        }

        public String getDsVersion() {
            return dsVersion;
        }

        public String getType() {
            return type;
        }

        public String getName() {
            return name;
        }

        public String getFileName() {
            return fileName;
        }

        public String toString() {
            return dataset + ":" + dsVersion + ":" + type + ":" + name + ":" + fileName;
        }

        private String dataset;
        private String dsVersion;
        private String type;
        private String name;
        private String fileName;
    }
}
