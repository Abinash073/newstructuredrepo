package com.rlogistics.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.FeatureCustomResult;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.RoleAccessCustomQueriesMapper;
import com.rlogistics.customqueries.RoleAccessResult;
import com.rlogistics.master.identity.ProcessUser;

import com.rlogistics.master.identity.MasterdataService;
import org.slf4j.Logger;

@RestController
public class RoleAccessCustomQueriesRestResource {

	private static Logger log = LoggerFactory.getLogger(RoleAccessCustomQueriesRestResource.class);

	@RequestMapping(value = "user/list/role-based-access/{roleCode}", method = RequestMethod.POST, produces = "application/json")
	List<RoleAccessResult> getRoleAccessList(@PathVariable("roleCode") String roleCode,
			@RequestParam(required = false, value = "query") String queryJSON) {

		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine()
				.getProcessEngineConfiguration();

		SqlSession session = pec.getSqlSessionFactory().openSession();

		// Newly Created
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Map<String, ArrayList<String>> queryObj = null;
		ArrayList<String> orderBy = new ArrayList<String>();
		String roleId = "";
		String retailerId = "";
		try {
			if (queryJSON != null) {
				try {
					queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
					orderBy = queryObj.get("orderBy");
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				ProcessUser processUser = masterdataService.createProcessUserQuery().email(roleCode).singleResult();
				roleId = processUser.getRoleCode();
				retailerId = processUser.getRetailerId();
			} catch (Exception e) {
				log.debug("Error in Getting ProcessUser Details :" + e);
			}

			RoleAccessCustomQueriesMapper mapper = session.getMapper(RoleAccessCustomQueriesMapper.class);

			if (!roleId.equalsIgnoreCase("retailer")) {
				return mapper.getRoleAccess(roleId.toLowerCase(), orderBy);
			} else {
				return mapper.getRoleAccessRetailer(roleId.toLowerCase(), retailerId.toLowerCase(), orderBy);
			}
		} finally {
			session.close();
		}
	}

	// Newly Created API For SUPERADMIN Only

	@RequestMapping(value = "super/list/role-based-access/{roleCode}", method = RequestMethod.POST, produces = "application/json")
	List<RoleAccessResult> getSuperRoleAccessList(@PathVariable("roleCode") String roleCode,
			@RequestParam(required = false, value = "query") String queryJSON) {

		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine()
				.getProcessEngineConfiguration();

		SqlSession session = pec.getSqlSessionFactory().openSession();

		// Newly Created
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Map<String, ArrayList<String>> queryObj = null;
		ArrayList<String> orderBy = new ArrayList<String>();
		try {
			if (queryJSON != null) {
				try {
					queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
					orderBy = queryObj.get("orderBy");
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			RoleAccessCustomQueriesMapper mapper = session.getMapper(RoleAccessCustomQueriesMapper.class);

			return mapper.getRoleAccess(roleCode.toLowerCase(), orderBy);

		} finally {
			session.close();
		}
	}

	@RequestMapping(value = "user/delete/role-based-access/{roleCode}", method = RequestMethod.POST, produces = "application/json")
	boolean deleteRoleAccess(@PathVariable("roleCode") String roleCode) {

		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine()
				.getProcessEngineConfiguration();

		SqlSession session = pec.getSqlSessionFactory().openSession();

		try {
			RoleAccessCustomQueriesMapper mapper = session.getMapper(RoleAccessCustomQueriesMapper.class);

			return mapper.deleteRoleAccess(roleCode.toLowerCase());
		} finally {
			session.close();
		}
	}

	@RequestMapping(value = "feature/listing/{firstResult}/{maxResults}", method = RequestMethod.POST, produces = "application/json")
	PaginationResponse getFeatureList(@PathVariable("firstResult") int firstResult,
			@PathVariable("maxResults") int maxResults) {

		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine()
				.getProcessEngineConfiguration();

		SqlSession session = pec.getSqlSessionFactory().openSession();

		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			RoleAccessCustomQueriesMapper mapper = session.getMapper(RoleAccessCustomQueriesMapper.class);
			List<FeatureCustomResult> featureCustomResults = mapper.getFeatureListing(firstResult, maxResults);
			ResultCount resultCount = mapper.getFeatureListingCount();

			paginationResponse.setData(featureCustomResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));

			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
