package com.rlogistics.rest;

import java.util.HashMap;
import java.util.Map;

public class TicketDetailResponse {
	private Map<String, String> variables2 = new HashMap<>();
	private Map<String, String> images = new HashMap<>();
		public Map<String, String> getImages() {
		return images;
	}
	public void setImages(Map<String, String> map1) {
		this.images = map1;
	}
		private String createdDate;
		private String modifiedDate;
		
		public Map<String, String> getVariables2() {
			return variables2;
		}
		public void setVariables2(Map<String, String> map) {
			this.variables2 = map;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}
		public String getModifiedDate() {
			return modifiedDate;
		}
		public void setModifiedDate(String modifiedDate) {
			this.modifiedDate = modifiedDate;
		}
		
	}