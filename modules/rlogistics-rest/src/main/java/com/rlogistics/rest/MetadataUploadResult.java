package com.rlogistics.rest;

import java.util.Map;

public class MetadataUploadResult {
	private Map<String,MetadataStats> statsByDataset;
	private boolean success = false;
	private String message;

	public Map<String,MetadataStats> getStatsByDataset() {
		return statsByDataset;
	}

	public void setStatsByDataset(Map<String,MetadataStats> statsByDataset) {
		this.statsByDataset = statsByDataset;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
