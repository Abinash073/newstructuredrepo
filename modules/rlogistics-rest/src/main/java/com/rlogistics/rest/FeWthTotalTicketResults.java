package com.rlogistics.rest;

import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class FeWthTotalTicketResults {
    private List<FeWithTotalTickets> data = new ArrayList<>();
    private int totalRecords;
    private String response;

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    private boolean success = false;
    private String message;


    public void setData(FeWithTotalTickets feWithTotalTickets) {
        this.data.add(feWithTotalTickets);
    }

    public List<FeWithTotalTickets> getData() {
        return data;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
