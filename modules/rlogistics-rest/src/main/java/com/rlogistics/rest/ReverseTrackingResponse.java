package com.rlogistics.rest;

import java.util.Date;

public class ReverseTrackingResponse {

	private String ticketNo;
	private String consumerName;
	private String consumerComplaintNumber;
	private Date createdDate;
	private Date modifiedDate;
	private String status;
	
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String pincode;
	private String telephoneNo;
	private String Brand;
	private String ProductCategory;
	private String ProductName;
	private String IdentificationNo;
	private String PhysicalEvaluation;
	private String TechnicalEvaluation;
	private String ticketStatusCode;

//	private String createDate;
//	private String modifyDate;
	
public String getTicketStatusCode() {
		return ticketStatusCode;
	}
	public void setTicketStatusCode(String ticketStatusCode) {
		this.ticketStatusCode = ticketStatusCode;
	}
	//	public String getCreateDate() {
//		return createDate;
//	}
//	public void setCreateDate(String createDate) {
//		this.createDate = createDate;
//	}
//	public String getModifyDate() {
//		return modifyDate;
//	}
//	public void setModifyDate(String modifyDate) {
//		this.modifyDate = modifyDate;
//	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getTelephoneNo() {
		return telephoneNo;
	}
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public String getProductCategory() {
		return ProductCategory;
	}
	public void setProductCategory(String productCategory) {
		ProductCategory = productCategory;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getIdentificationNo() {
		return IdentificationNo;
	}
	public void setIdentificationNo(String identificationNo) {
		IdentificationNo = identificationNo;
	}
	public String getPhysicalEvaluation() {
		return PhysicalEvaluation;
	}
	public void setPhysicalEvaluation(String physicalEvaluation) {
		PhysicalEvaluation = physicalEvaluation;
	}
	public String getTechnicalEvaluation() {
		return TechnicalEvaluation;
	}
	public void setTechnicalEvaluation(String technicalEvaluation) {
		TechnicalEvaluation = technicalEvaluation;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getConsumerComplaintNumber() {
		return consumerComplaintNumber;
	}
	public void setConsumerComplaintNumber(String consumerComplaintNumber) {
		this.consumerComplaintNumber = consumerComplaintNumber;
	}
	public String getConsumerName() {
		return consumerName;
	}
	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
