package com.rlogistics.rest;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import com.rlogistics.master.identity.RetailerEmailTemplate;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;

import freemarker.cache.TemplateLoader;

public class FreeMarkerTemplateLoader implements TemplateLoader {
	private RetailerSmsEmailTemplate template;

	public FreeMarkerTemplateLoader(RetailerSmsEmailTemplate template2){
		this.template = template2;
	}
	
	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
		/* Nothing to do */
	}

	@Override
	public Object findTemplateSource(String name) throws IOException {
		return template;
	}

	@Override
	public long getLastModified(Object templateSource) {
		/* May be add modifiedOn when its merged */
		return 0;
	}

	@Override
	public Reader getReader(final Object templateSource, final String encoding) throws IOException {
		Reader reader = null;
		if(templateSource != null){
			reader = new StringReader(((RetailerSmsEmailTemplate)templateSource).getEmailTemplate());
		}
		return reader;
	}
}
