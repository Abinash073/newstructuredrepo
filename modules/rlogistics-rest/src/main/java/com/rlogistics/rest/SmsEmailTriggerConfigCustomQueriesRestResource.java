package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.SmsEmailConfigResult2;
import com.rlogistics.customqueries.SmsEmailConfigResultByRetailer;
import com.rlogistics.customqueries.SmsEmailTemplateResult;
import com.rlogistics.customqueries.SmsEmailTriggerConfigCustomQueriesMapper;

@RestController
public class SmsEmailTriggerConfigCustomQueriesRestResource {

	@RequestMapping(value = "get/sms-email-configuration", method=RequestMethod.POST,produces = "application/json")
	List<SmsEmailConfigResult2> getConfiguration(){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			SmsEmailTriggerConfigCustomQueriesMapper mapper = session.getMapper(SmsEmailTriggerConfigCustomQueriesMapper.class);
			
			return mapper.getConfiguration();
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "get/sms-email-configuration/by-retailer/{retailerId}", method=RequestMethod.POST,produces = "application/json")
	List<SmsEmailConfigResultByRetailer> getConfigurationByRetailer(@PathVariable(value = "retailerId") String retailerId) {
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			SmsEmailTriggerConfigCustomQueriesMapper mapper = session.getMapper(SmsEmailTriggerConfigCustomQueriesMapper.class);
			
			return mapper.getConfigurationByRetailer(retailerId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"list/sms-email-templates","list/sms-email-templates/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse listSmsEmailTemplates(@PathVariable Map<String,String> pathVariables) {
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			SmsEmailTriggerConfigCustomQueriesMapper mapper = session.getMapper(SmsEmailTriggerConfigCustomQueriesMapper.class);
			List<SmsEmailTemplateResult> smsEmailTemplateResults = mapper.listSmsEmailTemplates(firstResult, maxResults);
			ResultCount resultCount = mapper.listSmsEmailTemplatesCount();
			
			paginationResponse.setData(smsEmailTemplateResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
