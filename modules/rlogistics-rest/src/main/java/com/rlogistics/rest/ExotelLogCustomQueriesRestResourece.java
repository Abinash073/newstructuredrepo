package com.rlogistics.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.customqueries.ExotelLogCustomQueriesMasterMapper;
import com.rlogistics.customqueries.ExotelLogResult;
import com.rlogistics.customqueries.ResultCount;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Exotel Reporting
 * Author:ADARSH
 */
@Slf4j
@RestController
public class ExotelLogCustomQueriesRestResourece {
    /**
     * @param date
     * @return Converted Date :yyyy-MM-dd
     * @throws ParseException
     */
    public static String dateConverter(String date) throws ParseException {
        SimpleDateFormat sourceDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date intermeditaeDate = sourceDate.parse(date);
        SimpleDateFormat destDate = new SimpleDateFormat("yyyy-MM-dd");
        return destDate.format(intermeditaeDate);

    }


    @RequestMapping(value = {"get/exotel/details/{firstpage}/{maxpage}", "get/exotel/details/list"}, method = RequestMethod.POST, produces = "application/json")
    PaginationResponse findExotelLog(HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest, @RequestParam("query") String queryjson, @RequestParam(value = "firstResult", required = false, defaultValue = "") String firstResult,
                                     @RequestParam(value = "maxResults", required = false, defaultValue = "") String maxResults,
                                     @PathVariable Map<String, String> pathVariables) throws Exception {

        PaginationResponse paginationResponse = new PaginationResponse();
        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
        SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();


        String processId = "";
        String consumerNumber = "";
        String assignee = "";
        String exotelNumber = "";
        String date = "";
        String ticketNumber = "";
        String city = "";
        String callStatus = "";
        String firstDate = "";
        String lastDate = "";
        try {
            log.debug(queryjson);

            Map<Object, Object> queryObject = new ObjectMapper().readValue(URLDecoder.decode(queryjson), Map.class);
            log.debug(queryObject + "aa");
            ExotelLogCustomQueriesMasterMapper mapper = sqlSession.getMapper(ExotelLogCustomQueriesMasterMapper.class);
            if (queryObject.containsKey("processId")) {
                processId = queryObject.get("processId").toString();
            }
            if (queryObject.containsKey("consumerNumber")) {
                consumerNumber = queryObject.get("consumerNumber").toString();
            }
            if (queryObject.containsKey("assignee")) {
                assignee = queryObject.get("assignee").toString();
            }
            if (queryObject.containsKey("date")) {
                Map<Object, Object> sdate = new HashMap<>();
                sdate.putAll((Map<?, ?>) queryObject.get("date"));
                firstDate = dateConverter(sdate.get("startDate").toString());
                lastDate = dateConverter(sdate.get("endDate").toString());
            }
            if (queryObject.containsKey("ticketNumber")) {
                ticketNumber = queryObject.get("ticketNumber").toString();
            }
            if (queryObject.containsKey("city")) {
                city = queryObject.get("city").toString();
            }
            if (queryObject.containsKey("callStatus")) {
                callStatus = queryObject.get("callStatus").toString();
            }
            if (queryObject.containsKey("firstDate")) {
                firstDate = queryObject.get("firstDate").toString();
            }
            if (queryObject.containsKey("lastDate")) {
                lastDate = queryObject.get("lastDate").toString();
            }
            List<ExotelLogResult> exotelLogResults = mapper.getExotelLog(processId, consumerNumber, assignee, exotelNumber, date, ticketNumber, city, callStatus, firstResult, maxResults, firstDate, lastDate);
            ResultCount resultCount = mapper.getExotelLogCount(processId,consumerNumber, assignee, exotelNumber, date, ticketNumber, city, callStatus, firstDate, lastDate);
            //TODO Pagination and count Logic
            paginationResponse.setData(exotelLogResults);
            paginationResponse.setTotalRecords(Integer.parseInt(resultCount.getTotalRecords()));
            sqlSession.close();
            return paginationResponse;

        } catch (Exception e) {
            //log.debug("ERROR In EXOTEL log report" + e);
            httpServletResponse.sendError(500, "Some Error occurred");
            System.out.print(e);
            sqlSession.close();
            throw new Exception("Some Error Occurred");
        }


    }
}
