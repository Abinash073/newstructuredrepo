package com.rlogistics.rest;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailUtil;

public class TaskUtil {

	private static Logger log = LoggerFactory.getLogger(TaskUtil.class);
	
	public static RestResult uploadProcessDetails(String processId, String ticketNo, String variableName) {
		RestResult result = new RestResult();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		byte[] content = null;
		String contentType = null;
		String contentName = null;
		int index = 0;
		try {
			ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processId).includeProcessVariables().singleResult();

			Map<String,Object> variableValues = null;
			
			if(processInstance != null){
				variableValues = processInstance.getProcessVariables();
			} else {
				/* Look for a historical instance */
				HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getHistoryService();
				
				HistoricProcessInstance hiProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processId).includeProcessVariables().singleResult();
				
				if(hiProcessInstance != null){
					variableValues = hiProcessInstance.getProcessVariables();
				}
			}
			
			if(variableValues.containsKey(variableName)){
				Object value = variableValues.get(variableName);
				String textValue = null;
				
				if(value instanceof ArrayList){
					ArrayList<String> values = (ArrayList<String>) value;
					
					if(index < values.size()){
						if(index < 0){
							index = values.size() + index;
						}
						
						textValue = values.get(index);
					}
				} else if(value instanceof String){
					textValue = (String) value;
				}
				
				content = DatatypeConverter.parseBase64Binary(textValue.replaceAll(" ", "+"));
				String imageContent = Base64.encodeBase64String(content);
				JSONObject updateObj = new JSONObject();
				updateObj.put("Image", imageContent);
				updateObj.put("Remarks", "Delivered");
				
				JSONObject requestObj = new JSONObject();
				requestObj.put("ExternalReferenceID", ticketNo);
				requestObj.put("ThirdpartyID", "2SXIP3");
				requestObj.put("UpdateObject", updateObj);
				
				
				//Call the Retailer API
				CloseableHttpClient httpclient = HttpClients.createDefault();
				
				HttpPost httppost = new HttpPost("https://api.servify.in/public/live/api/ServiceRequest/UpdateLogsiticsThirdPartyData");
				httppost.addHeader("Authorization","11BSE2N1EN2AF4jEI14E3E6A5TQOP6");
				httppost.addHeader("Content-Type","application/json");
				
				// Set request type, length, and body
				/*InputStreamEntity reqEntity = new InputStreamEntity(
						new ByteArrayInputStream(requestObj.toString().getBytes()),
						requestObj.toString().length());
				reqEntity.setContentType("application/json");*/
				
				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestObj.toString(), "UTF8"));
				
				// Get Response
				CloseableHttpResponse response = httpclient.execute(httppost);
				
				String rawResponse = EntityUtils.toString(response.getEntity());
				log.debug("Response from server :" + rawResponse);
				
				//send email
				try {
					String recepient = "shreya@bizlog.in";
					String subject = "Customer Signature";
	//				EmailUtil.sendAttachmentMail(recepient, subject, content);
				} catch (Exception me) {
					
				}
				
				result.setResponse(rawResponse);
				result.setMessage("Details successfully uploaded with signature");
			}
			
		} catch(Exception ex){
			log.error("Exception during upload", ex);
			result.setMessage(ex.getMessage());
			return result;
		}
		
		return result;
	}

}
