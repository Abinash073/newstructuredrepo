package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.DOAResult;
import com.rlogistics.customqueries.DoaFormCustomQueriesMapper;
import com.rlogistics.customqueries.ResultCount;

@RestController
public class DoaFormCustomQueriesRestResource {

	@RequestMapping(value = {"/list/doa-forms","/list/doa-forms/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse listDoaForms(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			DoaFormCustomQueriesMapper mapper = session.getMapper(DoaFormCustomQueriesMapper.class);
			List<DOAResult> doaResults = mapper.listDoaForms(firstResult,maxResults);
			paginationResponse.setData(doaResults);
			
			ResultCount resultCount = mapper.listDoaFormsCount();
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
