package com.rlogistics.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.customqueries.CostingActivityMappingResult;
import com.rlogistics.customqueries.CostingServiceCustomQueriesMapper;
import com.rlogistics.customqueries.CostingServiceResult;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.RetailerCostingActivityMappingResult;

@RestController
public class RetailerCostingCustomQueriesResource {

	@RequestMapping(value = "/get/costing-services/{retailerId}", method=RequestMethod.POST,produces = "application/json")
	List<CostingServiceResult> getCostingServices(@PathVariable("retailerId") String retailerId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			CostingServiceCustomQueriesMapper mapper = session.getMapper(CostingServiceCustomQueriesMapper.class);
			
			return mapper.getCostingServices(retailerId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/get/costing-activity/mapping","/get/costing-activity/mapping/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getCostingActivityMapping(@PathVariable Map<String,String> pathVariables,@RequestParam("query") String queryJSON){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		Map<String, String> queryObj = new HashMap<String,String>();
		String categoryId = "";
		String serviceId = "";
		String activityId = "";
		try {
			String firstResult = "0";
			String maxResults = "25";
			
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
				if(queryObj.size()>0) {
					if(queryObj.containsKey("categoryId")) {
						categoryId = queryObj.get("categoryId");
					}
					if(queryObj.containsKey("serviceId")) {
						serviceId = queryObj.get("serviceId");
					}
					if(queryObj.containsKey("activityId")) {
						activityId = queryObj.get("activityId");
					}
				}
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = pathVariables.get("firstResult");
				maxResults = pathVariables.get("maxResults");
			}
			
			CostingServiceCustomQueriesMapper mapper = session.getMapper(CostingServiceCustomQueriesMapper.class);
			List<CostingActivityMappingResult> costingActivityMappingResults = mapper.getCostingActivityMapping(categoryId,serviceId,activityId,firstResult,maxResults);
			paginationResponse.setData(costingActivityMappingResults);
			
			ResultCount resultCount = mapper.getCostingActivityMappingCount(categoryId,serviceId,activityId);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/get/retailer/costing-activity/mapping/{retailerId}","/get/retailer/costing-activity/mapping/{retailerId}/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getRetailerCostingActivityMapping(@PathVariable Map<String,String> pathVariables,@RequestParam("query") String queryJSON){
		
		String retailerId = pathVariables.get("retailerId");
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		Map<String, String> queryObj = new HashMap<String,String>();
		String categoryId = "";
		String serviceId = "";
		String activityId = "";
		try {
			String firstResult = "0";
			String maxResults = "25";
			
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
				if(queryObj.size()>0) {
					if(queryObj.containsKey("categoryId")) {
						categoryId = queryObj.get("categoryId");
					}
					if(queryObj.containsKey("serviceId")) {
						serviceId = queryObj.get("serviceId");
					}
					if(queryObj.containsKey("activityId")) {
						activityId = queryObj.get("activityId");
					}
				}
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = pathVariables.get("firstResult");
				maxResults = pathVariables.get("maxResults");
			}
			
			CostingServiceCustomQueriesMapper mapper = session.getMapper(CostingServiceCustomQueriesMapper.class);
			List<RetailerCostingActivityMappingResult> retailerCostingActivityMappingResults = mapper.getRetailerCostingActivityMapping(retailerId,categoryId,serviceId,activityId,firstResult,maxResults);
			paginationResponse.setData(retailerCostingActivityMappingResults);
			ResultCount resultCount = mapper.getRetailerCostingActivityMappingCount(retailerId,categoryId,serviceId,activityId);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
