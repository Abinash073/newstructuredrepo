package com.rlogistics.rest;

import java.util.Date;

import org.activiti.engine.task.Task;

public class TaskResponse {

	private String processId; 
	private String taskId;
	private String assignee;
	private String name;
	private String category;
	private Date createTime;
	private String description;
	private int priority;

	public TaskResponse(Task task){
		setProcessId(task.getProcessInstanceId());
		setTaskId(task.getId());
		setAssignee(task.getAssignee());
		setName(task.getName());
		setCategory(task.getCategory());
		setCreateTime(task.getCreateTime());
		setDescription(task.getDescription());
		setPriority(task.getPriority());
	}
	
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
