package com.rlogistics.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

public class ProcessResponseWithVariables {
	
	private ProcessInstanceResult process;
	private Map<String, Object> variables = new HashMap<>();
	private TaskResponse task;
	private String jobId;
	private boolean completed = false;
	private Date createdDate;
	private Date modifiedDate;

	public ProcessResponseWithVariables(ProcessInstance processInstance,Task task, Map<String,Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		setVariables(variables);
		this.setTask(new TaskResponse(task));
	}

	public ProcessResponseWithVariables(ProcessInstance processInstance, String jobId, Map<String, Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		this.setJobId(jobId);
		setVariables(variables);
	}
	
	public ProcessResponseWithVariables(HistoricProcessInstance processInstance, Map<String,Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		setVariables(variables);
	}
	
	public ProcessResponseWithVariables(Map<String,Object> variables) {
		
		setVariables(variables);
	}
   
	public ProcessResponseWithVariables(Date createdDate) {
		super();
		this.createdDate = createdDate;
	}

	public TaskResponse getTask() {
		return task;
	}

	public void setTask(TaskResponse task) {
		this.task = task;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		
		this.variables.clear();
		
		for(String variable:variables.keySet()){
			Object value = variables.get(variable);
			
			if(value instanceof Date){
				value = ((Date)value).getTime();
			}
			
			this.variables.put(variable, value);
		}
	}

	public ProcessInstanceResult getProcess() {
		return process;
	}

	public void setProcess(ProcessInstanceResult processInstanceResult) {
		this.process = processInstanceResult;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public boolean isCompleted(){
		return completed;
	}
	
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
