package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.RetailerCategoriesCustomQueriesMapper;
import com.rlogistics.customqueries.RetailerCategoriesCustomQuery1Result;

@RestController
public class RetailerCategoriesCustomQueriesRestResource {
	
	@RequestMapping(value = "/retailercategories/customqueries/categoryquery/{retailer}", method=RequestMethod.POST,produces = "application/json")
	List<RetailerCategoriesCustomQuery1Result> query1(@PathVariable("retailer") String retailer){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerCategoriesCustomQueriesMapper mapper = session.getMapper(RetailerCategoriesCustomQueriesMapper.class);
			
			return mapper.findCategories(retailer);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/productcategories/byretailer/{retailer}", method=RequestMethod.POST,produces = "application/json")
	List<RetailerCategoriesCustomQuery1Result> query2(@PathVariable("retailer") String retailer){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			RetailerCategoriesCustomQueriesMapper mapper = session.getMapper(RetailerCategoriesCustomQueriesMapper.class);
			
			return mapper.findProductCategories(retailer);
		} finally {
			session.close();
		}
	}

}
