package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.List;

public class RestExternalResult {

	Object response;
	private List<String> ticketNo = new ArrayList<>();
	public List<String> getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(List<String> ticketNo) {
		this.ticketNo = ticketNo;
	}
	String message;
	boolean success=true;
	String apiToken;
	
	public String getApiToken() {
		return apiToken;
	}
	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public String setMessage(String message) {
		this.message = message;
		return message;
	}
}
