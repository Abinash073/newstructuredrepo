package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.customqueries.LspDetailCustomQueriesMapper;
import com.rlogistics.customqueries.LspDetailResult;
import com.rlogistics.customqueries.ResultCount;

@RestController
public class LspDetailServiceRestResource {
	
	@RequestMapping(value = {"/get/lsp-detail/lists","/get/lsp-detail/lists/{firstResult}/{maxResult}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getLspDetails(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			int firstResult = 0;
			int maxResults = 10;
			
			LspDetailCustomQueriesMapper mapper = session.getMapper(LspDetailCustomQueriesMapper.class);
			if(pathVariables.containsKey("firstResult")){
				 firstResult = Integer.parseInt(pathVariables.get("firstResult")); 
				 maxResults = Integer.parseInt(pathVariables.get("maxResult"));
				
			}
			List<LspDetailResult> lspDetailsRes= mapper.getLspDetailServiceDetails(firstResult,maxResults);
			ResultCount resultCount = mapper.getLspCount();
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			paginationResponse.setData(lspDetailsRes);
           
			return paginationResponse;
			
			
		} finally {
			session.close();
		}
	}

}
