package com.rlogistics.rest;

import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.email.EmailSendCommand;
import com.rlogistics.email.EmailSendResult;
import com.rlogistics.email.EmailUtil;
import com.rlogistics.http.RLogisticsResource;

@RestController
public class EmailResource extends RLogisticsResource{
	private static Logger log = LoggerFactory.getLogger(EmailResource.class);
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/email/send",produces = "application/json")
	public EmailSendResult sendEmail(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam("command") String commandJson, @RequestParam("processId") String processId) {

		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		EmailSendCommand emailSendCommand = null;
		try {
			emailSendCommand = new ObjectMapper().readValue(URLDecoder.decode(commandJson),EmailSendCommand.class);
		} catch (Exception ex) {
			log.error("Exception during sendEmail", ex);
			EmailSendResult emailSendResult = new EmailSendResult();
			emailSendResult.getMessages().add(ex.getMessage());
			return emailSendResult;
		}
		
		return EmailUtil.sendEmail(emailSendCommand,processId);
	}
	
	@RequestMapping(value = "/email/send/attachment",produces = "application/json")
	public boolean sendAttachmentEmail(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam("processId") String processId, @RequestParam("variableName") String variableName) {

//		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		boolean res = false;
		try {
			res = EmailUtil.sendMultipleAttachmentMail(processId, variableName);
		} catch (Exception ex) {
			log.error("Exception during sendEmail", ex);
			EmailSendResult emailSendResult = new EmailSendResult();
			emailSendResult.getMessages().add(ex.getMessage());
			return false;
		}
		
		return res;
	}
}
