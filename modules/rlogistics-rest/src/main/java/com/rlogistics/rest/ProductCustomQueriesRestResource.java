package com.rlogistics.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.customqueries.CategoryResult;
import com.rlogistics.customqueries.LocalPackingResult;
import com.rlogistics.customqueries.OutstationPackingResult;
import com.rlogistics.customqueries.ProductCategoryResult;
import com.rlogistics.customqueries.ProductCustomQueriesMapper;
import com.rlogistics.customqueries.ProductCustomQuery1Result;
import com.rlogistics.customqueries.ProductCustomQuery2Result;
import com.rlogistics.customqueries.ProductIntraCityResult;
import com.rlogistics.customqueries.ProductListingResult;
import com.rlogistics.customqueries.ProductPackingResult;
import com.rlogistics.customqueries.ResultCount;

@RestController
public class ProductCustomQueriesRestResource {

	@RequestMapping(value = "/product/customqueries/query1/{id}", method=RequestMethod.POST,produces = "application/json")
	ProductCustomQuery1Result query1(@PathVariable("id") String id){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			
			return mapper.findProduct(id);
		} finally {
			session.close();
		}
	}

	@RequestMapping(value = "/product/customqueries/query2/{id}", method=RequestMethod.POST,produces = "application/json")
	ProductCustomQuery1Result query2(@PathVariable("id") String id){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			
			return mapper.findProduct2(id);
		} finally {
			session.close();
		}
	}

	@RequestMapping(value = "/product/customqueries/query3", method=RequestMethod.POST,produces = "application/json")
	List<ProductCustomQuery1Result> query3(){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			
			return mapper.findProduct3();
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/product/customqueries/bycategorybrand/{categoryId}/{brandId}/{retailerId}", method=RequestMethod.POST,produces = "application/json")
	List<ProductCustomQuery1Result> productCategory(@PathVariable("categoryId") String categoryId, @PathVariable("brandId") String brandId, @PathVariable("retailerId") String retailerId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			
			return mapper.findProductsByCategoryBrand(categoryId,brandId,retailerId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = "/product/customqueries/retailer-adv-replacement/{retailerId}", method=RequestMethod.POST,produces = "application/json")
	List<ProductCustomQuery2Result> retailerAdvanceReplacementProducts(@PathVariable("retailerId") String retailerId){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		try {
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			
			return mapper.findRetailerAdvanceReplacementProducts(retailerId);
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/product/list/{retailerId}","/product/list/{retailerId}/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse productListing(@PathVariable Map<String,String> pathVariables, @RequestParam("query") String queryJSON){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		String name = "";
		String code = "";
		String categoryId = "";
		String subCategoryId = "";
		String brandId = "";
		String packagingTypeId = "";
		try {
			String firstResult = "0";
			String maxResults = "25";
			String retailerId = pathVariables.get("retailerId"); 
			
			Map<String, String> queryObj = new HashMap<String,String>();
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(queryObj.containsKey("name")) {
				name = queryObj.get("name").toLowerCase();
			}
			if(queryObj.containsKey("code")) {
				code = queryObj.get("code").toLowerCase();
			}
			if(queryObj.containsKey("categoryId")) {
				categoryId = queryObj.get("categoryId");
			}
			if(queryObj.containsKey("subCategoryId")) {
				subCategoryId = queryObj.get("subCategoryId");
			}
			if(queryObj.containsKey("brandId")) {
				brandId = queryObj.get("brandId");
			}
			if(queryObj.containsKey("packagingTypeId")) {
				packagingTypeId = queryObj.get("packagingTypeId");
			}
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = pathVariables.get("firstResult");
				maxResults = pathVariables.get("maxResults");
			}
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<ProductListingResult> productListingResults = mapper.findProductList(retailerId,name,code,categoryId,subCategoryId,brandId,packagingTypeId,firstResult,maxResults);
			ResultCount resultCount = mapper.findProductListCount(retailerId,name,code,categoryId,subCategoryId,brandId,packagingTypeId);
			
			paginationResponse.setData(productListingResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/product/sub-category/list","/product/sub-category/list/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getSubCategories(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<CategoryResult> categoryResults = mapper.getSubCategories(firstResult,maxResults);
			ResultCount resultCount = mapper.getSubCategoriesCount();
			
			paginationResponse.setData(categoryResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/product/category/list","/product/category/list/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getCategories(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<ProductCategoryResult> productCategoryResults = mapper.getCategories(firstResult,maxResults);
			ResultCount resultCount = mapper.getCategoriesCount();
			
			paginationResponse.setData(productCategoryResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/product/intra-city/{retailerId}","/product/intra-city/{retailerId}/{firstResult}/{maxResults}"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getIntraCityProducts(@PathVariable Map<String,String> pathVariables){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			int firstResult = 0;
			int maxResults = 25;
			
			String retailerId = pathVariables.get("retailerId");
			
			if(pathVariables.containsKey("firstResult")) {
				firstResult = Integer.valueOf(pathVariables.get("firstResult"));
				maxResults = Integer.valueOf(pathVariables.get("maxResults"));
			}
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<ProductIntraCityResult> productIntraCityResults = mapper.getIntraCityProducts(retailerId, firstResult, maxResults);
			ResultCount resultCount = mapper.getIntraCityProductsCount(retailerId);
			
			paginationResponse.setData(productIntraCityResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/localpacking/list"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getLocalPackings(@RequestParam("firstResult") int firstResult, @RequestParam("maxResults") int maxResults){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<LocalPackingResult> localPackingResults = mapper.getLocalPackings(firstResult, maxResults);
			ResultCount resultCount = mapper.getLocalPackingsCount();
			
			paginationResponse.setData(localPackingResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/outstationpacking/list"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getOutstationPackings(@RequestParam("firstResult") int firstResult, @RequestParam("maxResults") int maxResults){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<OutstationPackingResult> outstationPackingResults = mapper.getOutstationPackings(firstResult, maxResults);
			ResultCount resultCount = mapper.getOutstationPackingsCount();
			
			paginationResponse.setData(outstationPackingResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
	
	@RequestMapping(value = {"/productpacking/list"}, method=RequestMethod.POST,produces = "application/json")
	PaginationResponse getProductPackings(@RequestParam("query") String queryJson,@RequestParam("firstResult") int firstResult, @RequestParam("maxResults") int maxResults){
		
		ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
		
		SqlSession session = pec.getSqlSessionFactory().openSession();
		
		PaginationResponse paginationResponse = new PaginationResponse();
		
		try {
			Map<String, String> queryObj = null;
			try {
				queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson),Map.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ProductCustomQueriesMapper mapper = session.getMapper(ProductCustomQueriesMapper.class);
			List<ProductPackingResult> productPackingResults = mapper.getProductPackings(queryObj.get("retailerId"), firstResult, maxResults);
			ResultCount resultCount = mapper.getProductPackingsCount(queryObj.get("retailerId"));
			
			paginationResponse.setData(productPackingResults);
			paginationResponse.setTotalRecords(Integer.valueOf(resultCount.getTotalRecords()));
			
			return paginationResponse;
		} finally {
			session.close();
		}
	}
}
