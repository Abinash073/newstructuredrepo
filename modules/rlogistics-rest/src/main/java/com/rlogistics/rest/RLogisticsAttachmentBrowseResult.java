package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RLogisticsAttachmentBrowseResult {

	private List<String> leafEntries = new ArrayList<>();
	private List<String> nonLeafEntries = new ArrayList<>();
	public RLogisticsAttachmentBrowseResult(Set<String> leafEntries, Set<String> nonLeafEntries) {
		this.leafEntries.addAll(leafEntries);
		this.nonLeafEntries.addAll(nonLeafEntries);
	}
	
	public List<String> getLeafEntries() {
		return leafEntries;
	}
	public void setLeafEntries(List<String> leafEntries) {
		this.leafEntries = leafEntries;
	}
	public List<String> getNonLeafEntries() {
		return nonLeafEntries;
	}
	public void setNonLeafEntries(List<String> nonLeafEntries) {
		this.nonLeafEntries = nonLeafEntries;
	}
}
