package com.rlogistics.scripting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogUtil {
	private static Logger log = LoggerFactory.getLogger(LogUtil.class);
	
	public static void debug(String message){
		log.debug(message);
	}

	public static void debug(String message,Throwable th){
		log.debug(message,th);
	}

	public static void error(String message){
		log.debug(message);
	}

	public static void error(String message,Throwable th){
		log.debug(message,th);
	}
}
