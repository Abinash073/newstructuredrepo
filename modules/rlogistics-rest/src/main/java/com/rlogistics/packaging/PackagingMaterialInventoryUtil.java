package com.rlogistics.packaging;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.ImeiInventory;
import com.rlogistics.master.identity.MasterdataService;

import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingMaterialMaster;
import com.rlogistics.rest.RestResult;

public class PackagingMaterialInventoryUtil {

	private static Logger log = LoggerFactory.getLogger(PackagingMaterialInventoryUtil.class);
	
	public static RestResult updateStatus(String barcode, int status) throws Exception{
		RestResult restResponse = new RestResult();
		Map<String, String> result = new HashMap<String, String>();
		boolean isStatusSame = true;
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		try {
			PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcode(barcode).singleResult();
			int previousStatus = packagingMaterialInventory.getStatus();
			int currentStatus = status;
			
			if(previousStatus!=currentStatus) {
				isStatusSame = false;
			}
			if(!isStatusSame && currentStatus==0) {
				packagingMaterialInventory.setUserId(null);
				packagingMaterialInventory.setTicketNumber(null);
			}
			packagingMaterialInventory.setStatus(currentStatus);
			
			masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
			
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(packagingMaterialInventory.getLocationId()).packagingTypeCode(packagingMaterialInventory.getPackagingTypeCode()).singleResult();
			if(packagingMaterialMaster == null) {
				packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
				packagingMaterialMaster.setLocationId(packagingMaterialInventory.getLocationId());
				packagingMaterialMaster.setPackagingTypeCode(packagingMaterialInventory.getPackagingTypeCode());
				if(currentStatus != 2 && currentStatus != 4) {
					packagingMaterialMaster.setAvailableQuantity(1);
				} else {
					packagingMaterialMaster.setAvailableQuantity(0);
				}
			} else {
				if(!isStatusSame) {
					if((currentStatus != 2 && currentStatus != 4) && (previousStatus == 2 || previousStatus == 4)) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						}
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
					} else if ((currentStatus == 4 || currentStatus == 2) && (previousStatus != 2 && previousStatus != 4)){
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}	
			}
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Status updated successfully");
		} catch (Exception e) {
			log.error("Error while updating status " + e.getMessage());
			throw e;
		}
		return restResponse;
	}
	
	public static RestResult updateInventory(String barcode, int status, String ticketNumber, String userId) throws Exception{
		RestResult restResponse = new RestResult();
		Map<String, String> result = new HashMap<String, String>();
		boolean isStatusSame = true;
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		try {
			PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcode(barcode).singleResult();
			int previousStatus = packagingMaterialInventory.getStatus();
			int currentStatus = status;
			
			if(previousStatus!=currentStatus) {
				isStatusSame = false;
			}
			packagingMaterialInventory.setStatus(currentStatus);
			packagingMaterialInventory.setTicketNumber(ticketNumber);
			packagingMaterialInventory.setUserId(userId);
			
			masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
			
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(packagingMaterialInventory.getLocationId()).packagingTypeCode(packagingMaterialInventory.getPackagingTypeCode()).singleResult();
			if(packagingMaterialMaster == null) {
				packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
				packagingMaterialMaster.setLocationId(packagingMaterialInventory.getLocationId());
				packagingMaterialMaster.setPackagingTypeCode(packagingMaterialInventory.getPackagingTypeCode());
				if(currentStatus != 2 && currentStatus != 4) {
					packagingMaterialMaster.setAvailableQuantity(1);
				} else {
					packagingMaterialMaster.setAvailableQuantity(0);
				}
			} else {
				if(!isStatusSame) {
					if((currentStatus != 2 && currentStatus != 4) && (previousStatus == 2 || previousStatus == 4)) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						}
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
					} else if ((currentStatus == 4 || currentStatus == 2) && (previousStatus != 2 && previousStatus != 4)){
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}	
			}
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Status updated successfully");
		} catch (Exception e) {
			log.error("Error while updating status " + e.getMessage());
			throw e;
		}
		return restResponse;
	}
	
	/**
	 * This Is the Same API as Above but this is also included Updating IMEI No.
	 * */
	public static RestResult updateInventoryImei(String barcode, int status, String ticketNumber, String userId,String imeiNo) throws Exception{
		RestResult restResponse = new RestResult();
		Map<String, String> result = new HashMap<String, String>();
		boolean isStatusSame = true;
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		try {
			PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcode(barcode).singleResult();
			int previousStatus = packagingMaterialInventory.getStatus();
			int currentStatus = status;
			
			if(previousStatus!=currentStatus) {
				isStatusSame = false;
			}
			packagingMaterialInventory.setStatus(currentStatus);
			packagingMaterialInventory.setTicketNumber(ticketNumber);
			packagingMaterialInventory.setUserId(userId);
			packagingMaterialInventory.setImeiNo(imeiNo);
			
			masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
			
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(packagingMaterialInventory.getLocationId()).packagingTypeCode(packagingMaterialInventory.getPackagingTypeCode()).singleResult();
			if(packagingMaterialMaster == null) {
				packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
				packagingMaterialMaster.setLocationId(packagingMaterialInventory.getLocationId());
				packagingMaterialMaster.setPackagingTypeCode(packagingMaterialInventory.getPackagingTypeCode());
				if(currentStatus != 2 && currentStatus != 4) {
					packagingMaterialMaster.setAvailableQuantity(1);
				} else {
					packagingMaterialMaster.setAvailableQuantity(0);
				}
			} else {
				if(!isStatusSame) {
					if((currentStatus != 2 && currentStatus != 4) && (previousStatus == 2 || previousStatus == 4)) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						}
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
					} else if ((currentStatus == 4 || currentStatus == 2) && (previousStatus != 2 && previousStatus != 4)){
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}	
			}
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Status updated successfully");
		} catch (Exception e) {
			log.error("Error while updating status " + e.getMessage());
			throw e;
		}
		return restResponse;
	}
	
	/**
	 * This Is For Updating IMEINO Status In ImeiInventory Table.
	 * */
	public static RestResult updateImeiNo(String imeiNo,String imeiStatus,String barcode,String barStatus) throws Exception{
		RestResult restResponse = new RestResult();
		boolean isStatusSame = true;
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		
		//UPDATING IMEI INVENTORY TABLE
		try {
			int status1 = Integer.valueOf(imeiStatus);
			ImeiInventory imeiInventory = masterdataService.createImeiInventoryQuery()
					.imeiNo(imeiNo)
					.singleResult();
			if (imeiInventory != null) {
				imeiInventory.setStatus(status1);
				try {
					masterdataService.saveImeiInventory(imeiInventory);
				} catch (Exception e) {
					log.debug("Error in updating Imei Inventory" + e);
					System.out.println("Error in updating Imei Inventory" + e);
				}
				restResponse.setSuccess(true);
				restResponse.setMessage("Status updated successfully");
			} else {
				log.debug("IMEI Is not Present");
				restResponse.setSuccess(true);
				restResponse.setMessage("IMEI Is not Present");
			}
		} catch (Exception e) {
			log.debug("Error in calling Imei Inventory" + e);
			System.out.println("Error in calling Imei Inventory" + e);
		}	
		
		//UPDATING PACKAGINGMATERIALINVENTORY FOR BARCODE
		try {
			PackagingMaterialInventory packagingMaterialInventory = masterdataService1.createPackagingMaterialInventoryQuery()
					.barcode(barcode)
					.singleResult();
			if (packagingMaterialInventory != null) {
				packagingMaterialInventory.setStatus(Integer.parseInt(barStatus));
				try {
					masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
				} catch (Exception e) {
					log.debug("Error in updating PackagingMaterialInventory" + e);
					System.out.println("Error in updating PackagingMaterialInventory" + e);
				}
			} else {
				log.debug("BARCODE Is not Present to issued");
			}
		} catch (Exception e) {
			log.debug("Error in calling PackagingMaterialInventory" + e);
			System.out.println("Error in calling PackagingMaterialInventory" + e);
		}
		return restResponse;
	}
}
