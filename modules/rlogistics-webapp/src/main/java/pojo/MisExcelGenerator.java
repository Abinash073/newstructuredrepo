package pojo;

import java.io.ByteArrayOutputStream;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.rlogistics.customqueries.TicketMisReportPojo;

public class MisExcelGenerator {

	public static ByteArrayInputStream generateMisExcel(List<TicketMisReportPojo> variables) throws IOException {

		Workbook workbook = new XSSFWorkbook();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		CreationHelper createHelper = workbook.getCreationHelper();

		Sheet sheet = workbook.createSheet("MIS_REPORT");

		Font headerFont = workbook.createFont();
//		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.BLUE.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Row for Header
		Row headerRow = sheet.createRow(0);

		ArrayList<String> headers = getHeaders();
		// Header
		for (int col = 0; col < headers.size(); col++) {
			Cell cell = headerRow.createCell(col);
			cell.setCellValue(headers.get(col));
			cell.setCellStyle(headerCellStyle);
		}
		// CellStyle for Age
		CellStyle ageCellStyle = workbook.createCellStyle();
		ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
		int rowIdx = 1;

		for (int i = 0; i < variables.size(); i++) {
			Row row = sheet.createRow(rowIdx++);
			row.createCell(0).setCellValue(variables.get(i).getCreateDate());
			row.createCell(1).setCellValue(variables.get(i).getCreateTime());
			row.createCell(2).setCellValue(variables.get(i).getNatureOfComplaint());
			row.createCell(3).setCellValue(variables.get(i).getRetailer());
			row.createCell(4).setCellValue(variables.get(i).getRlTicketNo());
			row.createCell(5).setCellValue(variables.get(i).getConsumerName());
			row.createCell(6).setCellValue(variables.get(i).getConsumerComplaintNumber());
			row.createCell(7).setCellValue(variables.get(i).getAddressLine1());
			row.createCell(8).setCellValue(variables.get(i).getAddressLine2());
			row.createCell(9).setCellValue(variables.get(i).getCity());
			row.createCell(10).setCellValue(variables.get(i).getPincode());
			row.createCell(11).setCellValue(variables.get(i).getTelephoneNumber());
			row.createCell(12).setCellValue(variables.get(i).getEmailId());
			row.createCell(13).setCellValue(variables.get(i).getOrderNumber());
			row.createCell(14).setCellValue(variables.get(i).getBrand());
			row.createCell(15).setCellValue(variables.get(i).getProductCategory());
			row.createCell(16).setCellValue(variables.get(i).getProductName());
			row.createCell(17).setCellValue(variables.get(i).getModel());
			row.createCell(18).setCellValue(variables.get(i).getIdentificationNo());
			row.createCell(19).setCellValue(variables.get(i).getDropLocContactPerson());
			row.createCell(20).setCellValue(variables.get(i).getDropLocContactNo());
			row.createCell(21).setCellValue(variables.get(i).getDropLocation());
			row.createCell(22).setCellValue(variables.get(i).getRlLocationName());
			row.createCell(23).setCellValue(variables.get(i).getDropCity());
			row.createCell(24).setCellValue(variables.get(i).getPickupDate());
			row.createCell(25).setCellValue(variables.get(i).getPickupTime());
			row.createCell(26).setCellValue(variables.get(i).getDropDate());
			row.createCell(27).setCellValue(variables.get(i).getDropTime());
			row.createCell(28).setCellValue(variables.get(i).getCurrentStatus());

//			row.createCell(29).setCellValue(variables.get(i).getFranchiseFeName());
//			row.createCell(30).setCellValue(variables.get(i).getAgencyName());

//			row.createCell(29).setCellValue(variables.get(i).getFeTypeForPickup());
//			row.createCell(30).setCellValue(variables.get(i).getFeNameForPickup());
//			row.createCell(31).setCellValue(variables.get(i).getAgencyNameForPickup());
//
//			row.createCell(32).setCellValue(variables.get(i).getFeTypeForDrop());
//			row.createCell(33).setCellValue(variables.get(i).getFeNameForDrop());
//			row.createCell(34).setCellValue(variables.get(i).getAgencyNameForDrop());
		
		

		}
		// Cell ageCell = row.createCell(3);
		// ageCell.setCellValue("28");
		// ageCell.setCellStyle(ageCellStyle);

		workbook.write(out);
		return new ByteArrayInputStream(out.toByteArray());
	}

	private static ArrayList<String> getHeaders() {
		ArrayList<String> arrayList = new ArrayList<>();
		arrayList.add("createDate");
		arrayList.add("createTime");
		arrayList.add("natureOfComplaint");
		arrayList.add("retailer");
		arrayList.add("rlTicketNo");
		arrayList.add("consumerName");
		arrayList.add("consumerComplaintNumber");
		arrayList.add("addressLine1");
		arrayList.add("addressLine2");
		arrayList.add("city");
		arrayList.add("pincode");
		arrayList.add("telephoneNumber");
		arrayList.add("emailId");
		arrayList.add("orderNumber");
		arrayList.add("brand");
		arrayList.add("productCategory");
		arrayList.add("productName");
		arrayList.add("model");
		arrayList.add("identificationNo");
		arrayList.add("dropLocContactPerson");
		arrayList.add("dropLocContactNo");
		arrayList.add("dropLocation");
		arrayList.add("rlLocationName");
		arrayList.add("dropCity");
		arrayList.add("pickupDate");
		arrayList.add("pickupTime");
		arrayList.add("dropDate");
		arrayList.add("dropTime");	
//		arrayList.add("dateOfComplaint");
		arrayList.add("currentStatus");

//		arrayList.add("franchiseFeName");
//		arrayList.add("agencyName");

//		arrayList.add("feTypeForPickup");
//		arrayList.add("feNameForPickup");
//		arrayList.add("agencyNameForPickup");
//
//		arrayList.add("feTypeForDrop");
//		arrayList.add("feNameForDrop");
//		arrayList.add("agencyNameForDrop");
		
	

		return arrayList;
	}

	public static ByteArrayInputStream generateMenifistExcelDownload(List<TicketMisReportPojo> variables)throws IOException {

		Workbook workbook = new XSSFWorkbook();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		CreationHelper createHelper = workbook.getCreationHelper();

		Sheet sheet = workbook.createSheet("MENIFIST");

		Font headerFont = workbook.createFont();
//		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.BLUE.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Row for Header
		Row headerRow = sheet.createRow(0);

		ArrayList<String> headers = getHeadersForMenifistDownload();
		// Header
		for (int col = 0; col < headers.size(); col++) {
			Cell cell = headerRow.createCell(col);
			cell.setCellValue(headers.get(col));
			cell.setCellStyle(headerCellStyle);
		}
		// CellStyle for Age
		CellStyle ageCellStyle = workbook.createCellStyle();
		ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
		int rowIdx = 1;

		for (int i = 0; i < variables.size(); i++) {
			Row row = sheet.createRow(rowIdx++);
			row.createCell(0).setCellValue(variables.get(i).getCreateDate());
//			row.createCell(1).setCellValue(variables.get(i).getCreateTime());
//			row.createCell(2).setCellValue(variables.get(i).getNatureOfComplaint());
			row.createCell(1).setCellValue(variables.get(i).getRetailer());
			row.createCell(2).setCellValue(variables.get(i).getRlTicketNo());
			row.createCell(3).setCellValue(variables.get(i).getConsumerName());
//			row.createCell(6).setCellValue(variables.get(i).getConsumerComplaintNumber());
			row.createCell(4).setCellValue(variables.get(i).getAddressLine1());
			row.createCell(5).setCellValue(variables.get(i).getAddressLine2());
			row.createCell(6).setCellValue(variables.get(i).getCity());
			row.createCell(7).setCellValue(variables.get(i).getPincode());
//			row.createCell(11).setCellValue(variables.get(i).getTelephoneNumber());
//			row.createCell(12).setCellValue(variables.get(i).getEmailId());
//			row.createCell(13).setCellValue(variables.get(i).getOrderNumber());
//			row.createCell(14).setCellValue(variables.get(i).getBrand());
//			row.createCell(15).setCellValue(variables.get(i).getProductCategory());
//			row.createCell(16).setCellValue(variables.get(i).getProductName());
//			row.createCell(17).setCellValue(variables.get(i).getModel());
			row.createCell(8).setCellValue(variables.get(i).getIdentificationNo());
			row.createCell(9).setCellValue(variables.get(i).getDropLocContactPerson());
			row.createCell(10).setCellValue(variables.get(i).getDropLocContactNo());
			row.createCell(11).setCellValue(variables.get(i).getDropLocation());
			row.createCell(12).setCellValue(variables.get(i).getRlLocationName());
			row.createCell(13).setCellValue(variables.get(i).getDropCity());
//			row.createCell(24).setCellValue(variables.get(i).getPickupDate());
//			row.createCell(25).setCellValue(variables.get(i).getPickupTime());
//			row.createCell(26).setCellValue(variables.get(i).getDropDate());
//			row.createCell(27).setCellValue(variables.get(i).getDropTime());
//			row.createCell(28).setCellValue(variables.get(i).getCurrentStatus());
			row.createCell(14).setCellValue(variables.get(i).getDocketNo());
			row.createCell(15).setCellValue(variables.get(i).getRemark());
			row.createCell(16).setCellValue(variables.get(i).getVehicleNo());
			row.createCell(17).setCellValue(variables.get(i).getWeight());


//			row.createCell(18).setCellValue(variables.get(i).getFranchiseFeName());
//			row.createCell(19).setCellValue(variables.get(i).getAgencyName());

//			row.createCell(18).setCellValue(variables.get(i).getFeTypeForPickup());
//			row.createCell(19).setCellValue(variables.get(i).getFeNameForPickup());
//			row.createCell(20).setCellValue(variables.get(i).getAgencyNameForPickup());
//
//			row.createCell(21).setCellValue(variables.get(i).getFeTypeForDrop());
//			row.createCell(22).setCellValue(variables.get(i).getFeNameForDrop());
//			row.createCell(23).setCellValue(variables.get(i).getAgencyNameForDrop());

		}
		// Cell ageCell = row.createCell(3);
		// ageCell.setCellValue("28");
		// ageCell.setCellStyle(ageCellStyle);

		workbook.write(out);
		return new ByteArrayInputStream(out.toByteArray());
	}
	private static ArrayList<String> getHeadersForMenifistDownload() {
		ArrayList<String> arrayList = new ArrayList<>();
		arrayList.add("createDate");
//		arrayList.add("createTime");
//		arrayList.add("natureOfComplaint");
		arrayList.add("retailer");
		arrayList.add("rlTicketNo");
		arrayList.add("consumerName");
//		arrayList.add("consumerComplaintNumber");
		arrayList.add("addressLine1");
		arrayList.add("addressLine2");
		arrayList.add("city");
		arrayList.add("pincode");
//		arrayList.add("telephoneNumber");
//		arrayList.add("emailId");
//		arrayList.add("orderNumber");
//		arrayList.add("brand");
//		arrayList.add("productCategory");
//		arrayList.add("productName");
//		arrayList.add("model");
		arrayList.add("identificationNo");
		arrayList.add("dropLocContactPerson");
		arrayList.add("dropLocContactNo");
		arrayList.add("dropLocation");
		arrayList.add("rlLocationName");
		arrayList.add("dropCity");
//		arrayList.add("pickupDate");
//		arrayList.add("pickupTime");
//		arrayList.add("dropDate");
//		arrayList.add("dropTime");	
//		arrayList.add("dateOfComplaint");
//		arrayList.add("currentStatus");
		arrayList.add("docketNo");
		arrayList.add("remark");
		arrayList.add("vehicle");
		arrayList.add("weight");

//		arrayList.add("franchiseFeName");
//		arrayList.add("agencyName");

//		arrayList.add("feTypeForPickup");
//		arrayList.add("feNameForPickup");
//		arrayList.add("agencyNameForPickup");
//		arrayList.add("feTypeForDrop");
//		arrayList.add("feNameForDrop");
//		arrayList.add("agencyNameForDrop");

		return arrayList;
	}


}
