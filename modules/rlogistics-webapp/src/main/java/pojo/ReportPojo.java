package pojo;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.ImeiInventory;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.TicketMisReport;
import com.rlogistics.rest.PaymentAuth;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Payment;
import com.rlogistics.master.identity.PaymentDetails;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.rest.RetailerApiCalls;
import com.rlogistics.master.identity.impl.cmd.CreateTicketMisReportQueryCmd;
import com.rlogistics.master.identity.impl.cmd.SaveTicketMisReportCmd;

public class ReportPojo {
    public String consumerName = "";
    public String consumerComplaintNumber = "";
    public String addressLine1 = "";
    public String addressLine2 = "";
    public String city = "";
    public String pincode = "";
    public String telephoneNumber = "";
    public String emailId = "";
    public String orderNumber = "";
    public String dateOfComplaint = "";
    public String natureOfComplaint = "";
    public String isUnderWarranty = "";
    public String retailer = "";
    public String brand = "";
    public String productCategory = "";
    public String productName = "";
    public String model = "";
    public String identificationNo = "";
    public String dropLocation = "";// rlReturnLocationAddress
    public String dropLocContactPerson = "";
    public String dropLocContactNo = "";
    public String currentStatus = "";
    public String processId = "";
    public String rlTicketNo = "";
    public String rlLocationName = "";
    public String createDate = "";
    public String pickupDate = "";
    public String dropDate = "";
    public String createTime = "";
    public String pickupTime = "";
    public String dropTime = "";
    public String timeStamp = "";
    public String dropCity = "";
    public String closedReason = "";
    public String closedDate = "";
    public String closedTime = "";

    public String feTypeForPickup = "";
    public String agencyNameForPickup = "";
    public String feNameForPickup = "";


    public String feTypeForDrop = "";
    public String agencyNameForDrop = "";
    public String feNameForDrop = "";


    Map<String, Object> variables = new HashMap<String, Object>();

    private static Logger log = LoggerFactory.getLogger(ReportPojo.class);

    public ReportPojo(String processId) {
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        this.variables = runtimeService.getVariables(processId);
    }

    public String getConsumerName() {
        if (variables.containsKey("consumerName"))
            return variables.get("consumerName").toString();
        else
            return "";
    }

    public String getConsumerComplaintNumber() {
        if (variables.containsKey("consumerComplaintNumber"))
            return variables.get("consumerComplaintNumber").toString();
        else
            return "";
    }

    public String getAddressLine1() {
        if (variables.containsKey("addressLine1"))
            return variables.get("addressLine1").toString();
        else
            return "";
    }

    public String getAddressLine2() {
        if (variables.containsKey("addressLine2"))
            return variables.get("addressLine2").toString();
        else
            return "";
    }

    public String getCity() {
        if (variables.containsKey("city"))
            return variables.get("city").toString();
        else
            return "";
    }

    public String getPincode() {
        if (variables.containsKey("pincode"))
            return variables.get("pincode").toString();
        else
            return "";
    }

    public String getTelephoneNumber() {
        if (variables.containsKey("telephoneNumber"))
            return variables.get("telephoneNumber").toString();
        else
            return "";
    }

    public String getEmailId() {
        if (variables.containsKey("emailId"))
            return variables.get("emailId").toString();
        else
            return "";
    }

    public String getOrderNumber() {
        if (variables.containsKey("orderNumber"))
            return variables.get("orderNumber").toString();
        else
            return "";
    }

    public String getDateOfComplaint() {
        return getCurrectTimeStamp();
    }

    public String getNatureOfComplaint() {
        if (variables.containsKey("natureOfComplaint"))
            return variables.get("natureOfComplaint").toString();
        else
            return "";
    }

    public String getIsUnderWarranty() {
        if (variables.containsKey("isUnderWarranty"))
            return variables.get("isUnderWarranty").toString();
        else
            return "";
    }

    public String getRetailer() {
        if (variables.containsKey("retailer"))
            return variables.get("retailer").toString();
        else
            return "";
    }

    public String getBrand() {
        return variables.get("brand").toString();
    }

    public String getProductCategory() {
        if (variables.containsKey("productCategory"))
            return variables.get("productCategory").toString();
        else
            return "";
    }

    public String getProductName() {
        if (variables.containsKey("productName"))
            return variables.get("productName").toString();
        else
            return "";
    }

    public String getModel() {
        if (variables.containsKey("model"))
            return variables.get("model").toString();
        else
            return "";
    }

    public String getIdentificationNo() {
        if (variables.containsKey("identificationNo"))
            return variables.get("identificationNo").toString();
        else
            return "";
    }

    public String getDropLocation() {
        String dropLocation1 = "";
        try {
            dropLocation1 = variables.get("dropLocation").toString() + "," + variables.get("dropLocAddress1").toString()
                    + "," + variables.get("dropLocAddress2").toString() + "," + variables.get("dropLocCity").toString()
                    + "," + variables.get("dropLocState").toString() + "," + variables.get("dropLocPincode").toString();
        } catch (Exception e) {
            if (variables.containsKey("rlReturnLocationAddress"))
                dropLocation1 = variables.get("rlReturnLocationAddress").toString();

        }
        return dropLocation1;
    }

    public String getDropCity() {
        String dropCity = "";
        try {
            String dropPincode = variables.get("dropLocPincode").toString();
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getMasterdataService();
            LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery().pincode(dropPincode)
                    .singleResult();
            ProcessLocation processLocation = masterdataService.createProcessLocationQuery()
                    .id(locationPincodes.getLocationId()).singleResult();
            dropCity = processLocation.getCity().toString();
        } catch (Exception e) {
            if (variables.containsKey("rlReturnLocationCity"))
                dropCity = variables.get("rlReturnLocationCity").toString();

        }
        return dropCity;
    }

    public String getDropLocContactPerson() {
        if (variables.containsKey("dropLocContactPerson"))
            return variables.get("dropLocContactPerson").toString();
        else
            return "";
    }

    public String getDropLocContactNo() {
        if (variables.containsKey("dropLocContactNo"))
            return variables.get("dropLocContactNo").toString();
        else
            return "";
    }

    public String getProcessId() {
        if (variables.containsKey("rlProcessId"))
            return variables.get("rlProcessId").toString();
        else
            return "";
    }

    public String getCurrentStatus() {
        String status = "TICKET_CREATED";
        if (currentStatus.equals("")) {
            if (variables.containsKey("runningTicketStatusExternal")) {
                status = variables.get("runningTicketStatusExternal").toString();
            }
        } else {
            status = currentStatus;
        }
        return status;
    }

    public String getRlTicketNo() {
        if (variables.containsKey("rlTicketNo"))
            return variables.get("rlTicketNo").toString();
        else
            return "";

    }

    public String getRlLocationName() {
        if (variables.containsKey("rlLocationName"))
            return variables.get("rlLocationName").toString();
        else
            return "";
    }

    public String getCreateDate() {
        return getCurrentDate();
    }

    public String getCreateTime() {
        return getCurrentTime();
    }

    public String getPickupDate() {
        return "";
    }

    public String getDropDate() {
        return "";
    }

    public String getPickupTime() {
        return "";
    }

    public String getDropTime() {
        return "";
    }


    public String getClosedReason() {
        return "";
    }

    public String getClosedDate() {
        return "";
    }

    public String getClosedTime() {
        return "";
    }

    public String getTimeStamp() {
        return getCurrectTimeStamp();
    }

//    public String getAgencyName() {
//        if (variables.containsKey("agencyName"))
//            return String.valueOf(variables.get("agencyName"));
//        else
//            return "";
//    }

    public String getFeTypeForPickup() {
        if (variables.containsKey("feTypeForPickup"))
            return String.valueOf(variables.get("feTypeForPickup"));
        else
            return "";
    }


    public String getAgencyNameForPickup() {
        if (variables.containsKey("agencyNameForPickup"))
            return String.valueOf(variables.get("agencyNameForPickup"));
        else
            return "";
    }


    public void setFeTypeForDrop(String feTypeForDrop) {
        this.feTypeForDrop = feTypeForDrop;
    }

    public String getFeTypeForDrop() {
        if (variables.containsKey("feTypeForDrop"))
            return String.valueOf(variables.get("feTypeForDrop"));
        else
            return "";
    }

    public String getAgencyNameForDrop() {
        if (variables.containsKey("agencyNameForDrop"))
            return String.valueOf(variables.get("agencyNameForDrop"));
        else
            return "";
    }

    public void setAgencyNameForDrop(String agencyNameForDrop) {
        this.agencyNameForDrop = agencyNameForDrop;
    }

    public String getFeNameForPickup() {
        if (variables.containsKey("feNameForPickup"))
            return String.valueOf(variables.get("feNameForPickup"));
        else
            return "";
    }

    public void setFeNameForPickup(String feNameForPickup) {
        this.feNameForPickup = feNameForPickup;
    }

    public String getFeNameForDrop() {
        if (variables.containsKey("feNameForDrop"))
            return String.valueOf(variables.get("feNameForDrop"));
        else
            return "";
    }

    public void setFeNameForDrop(String feNameForDrop) {
        this.feNameForDrop = feNameForDrop;
    }


    @SuppressWarnings("unchecked")
    public void sentToReportServer(Object obj) throws Exception {
        // String HOST = getReportIP();
        // String PATH = "/rlogistics-report/report/set/details";
        //
        // ObjectMapper oMapper = new ObjectMapper();
        //
        // Map<String, Object> variables = oMapper.convertValue(obj, Map.class);
        //
        // URIBuilder builder = new URIBuilder();
        // builder.setScheme("http").setHost(HOST).setPath(PATH).setParameter("variables",
        // new JSONObject(variables).toString());
        // URI uri = builder.build();
        //
        // CloseableHttpClient httpclient = HttpClients.createDefault();
        // HttpPost httppost = new HttpPost(uri);
        // httppost.addHeader("Content-Type", "application/json");
        // CloseableHttpResponse httpResponse = httpclient.execute(httppost);
        // String rawResponse = EntityUtils.toString(httpResponse.getEntity());
        // log.debug("RES:" + httppost.toString() + " RESPONSE:" + rawResponse);

        try {
            saveToMisReportTable(obj);
        } catch (Exception e) {

        }

    }

    public static void updateReport(JSONObject obj) throws Exception {
        log.debug("Update Method Called====================================================");
        // boolean rtn = false;
        // String HOST = getReportIP();
        // String PATH = "/rlogistics-report/report/update/details";
        // URIBuilder builder = new URIBuilder();
        // builder.setScheme("http").setHost(HOST).setPath(PATH).setParameter("variables",
        // obj.toString());
        // URI uri = builder.build();
        //
        // CloseableHttpClient httpclient = HttpClients.createDefault();
        // HttpPost httppost = new HttpPost(uri);
        // httppost.addHeader("Content-Type", "application/json");
        // CloseableHttpResponse httpResponse = httpclient.execute(httppost);
        // String rawResponse = EntityUtils.toString(httpResponse.getEntity());
        // log.debug("RES:" + httppost.toString() + " RESPONSE:" + rawResponse);
        updateTicketMisReportTable(obj);

    }

    private static void updateTicketMisReportTable(JSONObject obj) throws Exception {
        ObjectMapper oMapper = new ObjectMapper();

        log.debug("==============================================================================");
        Map<String, String> variables = oMapper.readValue(obj.toString(), Map.class);

        CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
                .getProcessEngineConfiguration())).getCommandExecutor();
        TicketMisReport.TicketMisReportQuery ticketMisReportQuery = commandExecutor
                .execute(new CreateTicketMisReportQueryCmd());
        TicketMisReport ticketMisReport = ticketMisReportQuery.processId(variables.get("processId").toString())
                .singleResult();
        if (variables.containsKey("pickupDate"))
            ticketMisReport.setPickupDate(variables.get("pickupDate").toString());
        if (variables.containsKey("pickupTime"))
            ticketMisReport.setPickupTime(variables.get("pickupTime").toString());
        if (variables.containsKey("rlTicketNo"))
            ticketMisReport.setRlTicketNo(variables.get("rlTicketNo").toString());
        if (variables.containsKey("currentStatus"))
            ticketMisReport.setCurrentStatus(variables.get("currentStatus").toString());
        if (variables.containsKey("closedDate"))
            ticketMisReport.setClosedDate(variables.get("closedDate").toString());
        if (variables.containsKey("closedTime"))
            ticketMisReport.setClosedTime(variables.get("closedTime").toString());
        if (variables.containsKey("closedReason"))
            ticketMisReport.setClosedReason(variables.get("closedReason").toString());
        if (variables.containsKey("dropDate"))
            ticketMisReport.setDropDate(variables.get("dropDate").toString());
        if (variables.containsKey("dropTime"))
            ticketMisReport.setDropTime(variables.get("dropTime").toString());

        // Abinash Update Franchise Data

        if (variables.containsKey("feTypeForPickup"))
            ticketMisReport.setFeTypeForPickup(String.valueOf(variables.get("feType")));
        if (variables.containsKey("feNameForPickup"))
            ticketMisReport.setFeNameForPickup(String.valueOf(variables.get("feNameForPickup")));
        if (variables.containsKey("agencyNameForPickup"))
            ticketMisReport.setAgencyNameForPickup(String.valueOf(variables.get("agencyNameForPickup")));

        if (variables.containsKey("feTypeForDrop"))
            ticketMisReport.setFeTypeForDrop(String.valueOf(variables.get("feTypeForDrop")));
        if (variables.containsKey("feNameForDrop"))
            ticketMisReport.setFeNameForDrop(String.valueOf(variables.get("feNameForDrop")));
        if (variables.containsKey("agencyNameForDrop"))
            ticketMisReport.setAgencyNameForDrop(String.valueOf(variables.get("agencyNameForDrop")));

//		if (variables.containsKey("agencyName")) {
//			ticketMisReport.setAgencyName(String.valueOf(variables.get("agencyName")));
//			log.debug("Agency Name ===================== " + String.valueOf(variables.get("agencyName")));
//			log.debug(String.valueOf(ticketMisReport.toString()));
//		}
//		if (variables.containsKey("franchiseFe"))
//			ticketMisReport.setFranchiseFe(String.valueOf(variables.get("franchiseFe")));

        commandExecutor.execute(new SaveTicketMisReportCmd(ticketMisReport));

    }

    private String getReportIP() throws UnknownHostException {
        String rtnString = "";
        InetAddress IP = InetAddress.getLocalHost();
        if (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
            rtnString = "52.33.12.1:8080";
        }
        // else
        // rtnString = "52.33.12.1:8080";
        return rtnString;
    }

    public static String getCurrentDate() {
        SimpleDateFormat sd = new SimpleDateFormat("MM/dd/YYYY");
        Date date = new Date();
        sd.setTimeZone(TimeZone.getTimeZone("IST"));
        return sd.format(date) + "";
    }

    public static String getCurrentTime() {
        SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss");// 24 hours
        Date date = new Date();
        sd.setTimeZone(TimeZone.getTimeZone("IST"));
        return sd.format(date) + "";
    }

    public static String getCurrectTimeStamp() {
        SimpleDateFormat sd = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        sd.setTimeZone(TimeZone.getTimeZone("IST"));
        return sd.format(date) + "";
    }

    public static JSONObject pickupdone(Map<String, Object> variables) throws Exception {
        JSONObject map = new JSONObject();
        map.put("currentStatus", "PICK_UP_DONE");
        map.put("processId", variables.get("rlProcessId").toString());
        map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        map.put("pickupDate", getCurrentDate());
        map.put("pickupTime", getCurrentTime());
        return map;

    }

    public static JSONObject dropDone(Map<String, Object> variables) throws Exception {
        JSONObject map = new JSONObject();
        map.put("currentStatus", "DROP_DONE");
        map.put("processId", variables.get("rlProcessId").toString());
        map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        map.put("dropDate", getCurrentDate());
        map.put("dropTime", getCurrentTime());
        return map;

    }

    public static JSONObject cancelTicket(String processId) throws Exception {
        JSONObject map = new JSONObject();
        map.put("currentStatus", "CANCEL_BY_RETAILER_BY_API");
        map.put("processId", processId);
        // map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        return map;

    }

    public static JSONObject closeInInitialCall(String processId) throws Exception {
        JSONObject map = new JSONObject();
        map.put("currentStatus", "CANCELLED_INITIAL_CALL");
        map.put("processId", processId);
        // map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        return map;

    }

    public static JSONObject currentStatus(Map<String, Object> variables, String statusString) throws Exception {
        JSONObject map = new JSONObject();
        map.put("currentStatus", statusString);
        map.put("processId", variables.get("rlProcessId").toString());
        map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        return map;

    }

    public static JSONObject ticketClosed(Map<String, Object> variables, String closedReason) throws Exception {
        JSONObject map = new JSONObject();
        map.put("processId", variables.get("rlProcessId").toString());
        map.put("rlTicketNo", variables.get("rlTicketNo").toString());
        map.put("closedDate", getCurrentDate());
        map.put("closedTime", getCurrentTime());
        map.put("closedReason", closedReason);
        return map;

    }

    private void saveToMisReportTable(Object obj) {
        ObjectMapper oMapper = new ObjectMapper();
        ReportPojo pojo = oMapper.convertValue(obj, ReportPojo.class);
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        TicketMisReport ticketMisReport = masterdataService.newTicketMisReport();
        ticketMisReport.setAddressLine1(pojo.getAddressLine1());
        ticketMisReport.setAddressLine2(pojo.getAddressLine2());
        ticketMisReport.setBrand(pojo.getBrand());
        ticketMisReport.setCity(pojo.getCity());
        ticketMisReport.setConsumerComplaintNumber(pojo.getConsumerComplaintNumber());
        ticketMisReport.setConsumerName(pojo.getConsumerName());
        ticketMisReport.setCreateDate(pojo.getCreateDate());
        ticketMisReport.setCreateTime(pojo.getCreateTime());
        ticketMisReport.setCurrentStatus(pojo.getCurrentStatus());
        ticketMisReport.setDateOfComplaint(pojo.getDateOfComplaint());
        ticketMisReport.setDropCity(pojo.getDropCity());
        ticketMisReport.setDropDate(pojo.getDropDate());
        ticketMisReport.setDropTime(pojo.getDropTime());
        ticketMisReport.setDropLocation(pojo.getDropLocation());
        ticketMisReport.setDropLocContactNo(pojo.getDropLocContactNo());
        ticketMisReport.setDropLocContactPerson(pojo.getDropLocContactPerson());
        ticketMisReport.setEmailId(pojo.getEmailId());
        ticketMisReport.setIdentificationNo(pojo.getIdentificationNo());
        ticketMisReport.setIsUnderWarranty(pojo.getIsUnderWarranty());
        ticketMisReport.setModel(pojo.getModel());
        ticketMisReport.setNatureOfComplaint(pojo.getNatureOfComplaint());
        ticketMisReport.setOrderNumber(pojo.getOrderNumber());
        ticketMisReport.setPickupDate(pojo.getPickupDate());
        ticketMisReport.setPickupTime(pojo.getPickupTime());
        ticketMisReport.setPincode(pojo.getPincode());
        ticketMisReport.setProcessId(pojo.getProcessId());
        ticketMisReport.setProductCategory(pojo.getProductCategory());
        ticketMisReport.setProductName(pojo.getProductName());
        ticketMisReport.setRetailer(pojo.getRetailer());
        ticketMisReport.setRlLocationName(pojo.getRlLocationName());
        ticketMisReport.setRlTicketNo(pojo.getRlTicketNo());
        ticketMisReport.setTelephoneNumber(pojo.getTelephoneNumber());
        ticketMisReport.setTimeStamp(pojo.getTimeStamp());
        ticketMisReport.setClosedDate(pojo.getClosedDate());
        ticketMisReport.setClosedTime(pojo.getClosedTime());
        ticketMisReport.setClosedReason(pojo.getClosedReason());


        ticketMisReport.setFeTypeForPickup(pojo.getFeTypeForPickup());
        ticketMisReport.setFeNameForPickup(pojo.getFeNameForPickup());
        ticketMisReport.setAgencyNameForPickup(pojo.getAgencyNameForPickup());
        ticketMisReport.setFeTypeForDrop(pojo.getFeTypeForDrop());
        ticketMisReport.setFeNameForDrop(pojo.getFeNameForDrop());
        ticketMisReport.setAgencyNameForDrop(pojo.getAgencyNameForDrop());

//		ticketMisReport.setAgencyName(pojo.getAgencyName());
//		ticketMisReport.setFranchiseFe(pojo.getFranchiseFe());
        log.debug("=============================Inside Ticket MIS Report========================");


        masterdataService.saveTicketMisReport(ticketMisReport);
    }

}
