package pojo;

public class PhotoBeforePaking {
	private String imageType;
	private String imageUrl;
	private String imageCreation;

	public String getImageType() {
		return imageType;
	}

	

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageCreation() {
		return imageCreation;
	}

	public void setImageCreation(String imageCreation) {
		this.imageCreation = imageCreation;
	}
}
