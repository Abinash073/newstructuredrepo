package org.activiti.explorer.conf;

//import com.rlogistics.excel.Reader.ExcelReader;
//import com.rlogistics.excel.Reader.ExcelReaderImpl;

import com.rlogistics.excel.Reader.ExcelReader;
import com.rlogistics.excel.Reader.ExcelReaderImpl;
import com.rlogistics.rest.services.excelWriter.MisReportUtilImpl;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtilImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ExcelUtilConfig {


    @Qualifier(value = "reportExcel")
    @Bean
    public ReportExcelUtil getExcelUtil() {
        return new ReportExcelUtilImpl();
    }


    @Qualifier(value = "misExcel")
    @Bean
    public ReportExcelUtil getMisUtil() {
        return new MisReportUtilImpl();
    }


    @Bean("excelReader")
    @Scope(value = "prototype")
    public ExcelReader getExcelReader() {
        return new ExcelReaderImpl();
    }

}
