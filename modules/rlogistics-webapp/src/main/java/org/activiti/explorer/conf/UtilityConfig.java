package org.activiti.explorer.conf;

import com.rlogistics.rest.bulkApiForClient.config.WebConfigureForBulk;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;

@Configuration
@PropertySources({
        @PropertySource(value = "classpath:key.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:variables.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:external.properties", ignoreResourceNotFound = true)
})
@ComponentScan(
        basePackageClasses = {com.rlogistics.rest.services.ticketCreationService.BulkTicketCreationService.class,
                 WebConfigureForBulk.class
        },
        basePackages = {"com.rlogistics.rest.Component", "com.rlogistics.rest.constants", "org.activiti.explorer.conf", "com.rlogistics.rest.bulkApiForClient", "com.rlogistics.rest.bulkApiForClient.config"})
public class UtilityConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    @Bean
    public ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean
    public Validator localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

}

