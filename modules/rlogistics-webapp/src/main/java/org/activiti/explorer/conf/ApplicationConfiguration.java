
package org.activiti.explorer.conf;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import org.activiti.engine.ProcessEngines;
import org.springframework.context.annotation.*;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySources({
        @PropertySource(value = "classpath:db.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:engine.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:ui.properties", ignoreResourceNotFound = true)
})
@ComponentScan(basePackages = {"org.activiti.explorer.conf"})
@ImportResource({"classpath:activiti-login-context.xml", "classpath:activiti-custom-context.xml", "classpath:activiti-ui-context.xml"})
public class ApplicationConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * @return masterdataservice bean
     */
    @Bean("masterdataService")
    public MasterdataService masterdataService() {
        return ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
    }
}
