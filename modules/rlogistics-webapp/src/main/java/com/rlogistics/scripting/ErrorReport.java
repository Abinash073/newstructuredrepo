package com.rlogistics.scripting;

import java.util.ArrayList;
import java.util.List;

public class ErrorReport {
	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String toString(){
		return String.join(",", messages);
	}
	private List<String> messages = new ArrayList<>();
	
}
