package com.rlogistics.ui.login;

import com.rlogistics.master.identity.ProcessUser;

public class ProcessUserUIUserAdapter implements UIUser {

	protected ProcessUser processUser;
	
	public ProcessUserUIUserAdapter(ProcessUser processUser) {
		this.processUser = processUser;
	}

	@Override
	public String getEmail() {
		return processUser.getEmail();
	}

	@Override
	public String getName() {
		return processUser.getFirstName() + " " + processUser.getLastName();
	}

	@Override
	public String getId() {
		return processUser.getId();
	}
	
	public ProcessUser getProcessUser(){
		return processUser;
	}

	@Override
	public boolean isAdmin() {
		if (processUser.getRoleCode() != null) {
			if(processUser.getRoleCode().equalsIgnoreCase("admin")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public String getRoleCode() {
		// TODO Auto-generated method stub
		return processUser.getRoleCode();
	}

	@Override
	public String getLocationId() {
		// TODO Auto-generated method stub
		return processUser.getLocationId();
	}

	@Override
	public String getRetailerId() {
		// TODO Auto-generated method stub
		return processUser.getRetailerId();
	}
	@Override
	public String getNumber(){
		return processUser.getPhone();
	}
}
