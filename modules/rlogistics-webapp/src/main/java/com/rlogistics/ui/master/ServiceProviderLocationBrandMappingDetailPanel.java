/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ServiceProviderLocationBrandMapping;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ServiceProviderLocationBrandMappingDetailPanel extends DetailPanel{

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected ServiceProviderLocationBrandMappingPage categoryPage;
	protected ServiceProviderLocationBrandMapping category;

	protected boolean editingDetails;
	protected HorizontalLayout categoryDetailsLayout;
	protected TextField serviceProviderLocationField;
	protected TextField brandField;
	protected TextField authorizationTypeField;
	protected TextField subCategoryField;

	public ServiceProviderLocationBrandMappingDetailPanel(ServiceProviderLocationBrandMappingPage categoryPage, String categoryId) {
		this.categoryPage = categoryPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.category = masterdataService.createServiceProviderLocationBrandMappingQuery().id(categoryId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationDetails();

		initActions();
	}

	protected void initActions() {
		categoryPage.initCreateLocationButton();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded categoryImage = new Embedded(null, Images.USER_50);
		layout.addComponent(categoryImage);

		Label categoryName = new Label(category.getServiceProviderLocationId() + " " + category.getBrandId());
		categoryName.setSizeUndefined();
		categoryName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(categoryName);
		layout.setComponentAlignment(categoryName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(categoryName, 1.0f);
	}

	protected void initLocationDetails() {
		Label categoryDetailsHeader = new Label(i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_HEADER_DETAILS));
		categoryDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		categoryDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(categoryDetailsHeader);

		// Details: picture and basic info
		categoryDetailsLayout = new HorizontalLayout();
		categoryDetailsLayout.setSpacing(true);
		categoryDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(categoryDetailsLayout);

		populateLocationDetails();
	}

	protected void populateLocationDetails() {
		loadLocationDetails();
		initDetailsActions();
	}

	protected void loadLocationDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		categoryDetailsLayout.addComponent(detailGrid);

		// Details
		addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_SERVICE_PROVIDER_LOCATION), new Label(category.getServiceProviderLocationId())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_SERVICE_PROVIDER_LOCATION),
					new Label(category.getServiceProviderLocationId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_BRAND),
					new Label(category.getBrandId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_AUTHORIZATION_TYPE),
					new Label(category.getAuthorizationType()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_SUB_CATEGORY),
					new Label(category.getSubCategoryId()));
		} else {
			serviceProviderLocationField = new TextField(null, category.getServiceProviderLocationId() != null ? category.getServiceProviderLocationId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_SERVICE_PROVIDER_LOCATION), serviceProviderLocationField);
			serviceProviderLocationField.focus();
			brandField = new TextField(null, category.getBrandId() != null ? category.getBrandId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_BRAND), brandField);
			authorizationTypeField = new TextField(null, category.getAuthorizationType() != null ? category.getAuthorizationType() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_AUTHORIZATION_TYPE), authorizationTypeField);
			subCategoryField = new TextField(null, category.getSubCategoryId() != null ? category.getSubCategoryId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_SUB_CATEGORY), subCategoryField);
		}
	}

	protected void addLocationDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		categoryDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				categoryDetailsLayout.removeAllComponents();
				populateLocationDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalName = category.getServiceProviderLocationId() + category.getBrandId();

				// Change data
				category.setServiceProviderLocationId(serviceProviderLocationField.getValue().toString());
				category.setBrandId(brandField.getValue().toString());
				category.setAuthorizationType(authorizationTypeField.getValue().toString());
				category.setSubCategoryId(subCategoryField.getValue().toString());
				
				masterdataService.saveServiceProviderLocationBrandMapping(category);

				// Refresh detail panel
				editingDetails = false;
				categoryDetailsLayout.removeAllComponents();
				populateLocationDetails();

				// Refresh task list (only if name was changed)
				if (nameChanged(originalName)) {
					categoryPage.notifyLocationChanged(category.getId());
				}
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		if (originalName != null) {
			nameChanged = !originalName.equals(category.getServiceProviderLocationId() + category.getBrandId());
		} else {
			nameChanged = (category.getServiceProviderLocationId() + category.getBrandId()) != null;
		}

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_CONFIRM_DELETE, category.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete category from database
						masterdataService.deleteServiceProviderLocationBrandMapping(masterdataService.createServiceProviderLocationBrandMappingQuery().serviceProviderLocationId(category.getServiceProviderLocationId()).brandId(category.getBrandId()).singleResult().getId());

						// Update ui
						categoryPage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
