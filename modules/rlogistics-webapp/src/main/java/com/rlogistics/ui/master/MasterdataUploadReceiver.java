/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rlogistics.ui.master;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipInputStream;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.explorer.Messages;

import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.rlogistics.ui.RLogisticsNotificationManager;
import com.rlogistics.ui.RLogisticsViewManager;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.Upload.Receiver;

/**
 * @author Joram Barrez
 */
public class MasterdataUploadReceiver implements Receiver, FinishedListener {

	private static final long serialVersionUID = 1L;

	protected transient RepositoryService repositoryService;
	protected RLogisticsI18nManager i18nManager;
	protected RLogisticsNotificationManager notificationManager;
	protected RLogisticsViewManager viewManager;
	
	// Will be assigned during upload
	protected ByteArrayOutputStream outputStream;
	protected String fileName;

	// Will be assigned after deployment
	protected boolean validFile = false;

	public MasterdataUploadReceiver() {
		this.repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.notificationManager = RLogisticsApp.get().getNotificationManager();
		this.viewManager = RLogisticsApp.get().getViewManager();
	}

	public OutputStream receiveUpload(String filename, String mimeType) {
		this.fileName = filename;
		this.outputStream = new ByteArrayOutputStream();
		return outputStream;
	}

	public void uploadFinished(FinishedEvent event) {
		deployUploadedFile();
		if (validFile) {
			notificationManager.showInformationNotification(Messages.MASTERDATA_UPLOAD_FINISHED);
		}
	}

	protected void deployUploadedFile() {
		try {
			try {
				InputStream is = null;
				
				if (fileName.endsWith(".csv") || fileName.endsWith(".txt")) {
					validFile = true;
					is = new ByteArrayInputStream(outputStream.toByteArray());
				} else if (fileName.endsWith(".zip")) {
					validFile = true;
					is = new ZipInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
				} else {
					notificationManager.showErrorNotification(Messages.MASTERDATA_UPLOAD_INVALID_FILE,
							i18nManager.getMessage(Messages.MASTERDATA_UPLOAD_INVALID_FILE_EXPLANATION));
				}

				// If the deployment is valid, run it through the beforeDeploy
				// and actually deploy it in Activiti
				if (validFile) {
					MasterdataStreamProcessor.processUpload(is);
				}
			} catch (ActivitiException e) {
				String errorMsg = e.getMessage().replace(System.getProperty("line.separator"), "<br/>");
				notificationManager.showErrorNotification(Messages.MASTERDATA_UPLOAD_FAILED, errorMsg);
			}
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					notificationManager.showErrorNotification("Server-side error", e.getMessage());
				}
			}
		}
	}
}
