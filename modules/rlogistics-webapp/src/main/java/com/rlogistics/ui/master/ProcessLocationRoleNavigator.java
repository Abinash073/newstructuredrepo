package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProcessLocationRoleNavigator extends ManagementNavigator {

	public static final String LOCATION_ROLE_URI_PART = "processlocationrole";

	public String getTrigger() {
		return LOCATION_ROLE_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String locationRoleId = uriFragment.getUriPart(1);


		if (locationRoleId != null) {
			RLogisticsApp.get().getViewManager().showProcessLocationRolePage(locationRoleId);
		} else {
			RLogisticsApp.get().getViewManager().showProcessLocationRolePage();
		}
	}
}
