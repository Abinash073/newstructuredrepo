package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ServiceProviderLocationNavigator extends ManagementNavigator {

	public static final String SERVICE_PROVIDER_LOCATION_URI_PART = "serviceProviderLocation";

	public String getTrigger() {
		return SERVICE_PROVIDER_LOCATION_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showServiceProviderLocationPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showServiceProviderLocationPage();
		}
	}
}
