/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing masterCategorys.
 * 
 * @author Joram Barrez
 */
public class ServiceProviderLocationBrandMappingPage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String masterCategoryId;
	protected Table masterCategoryTable;
	protected LazyLoadingQuery masterCategoryListQuery;
	protected LazyLoadingContainer masterCategoryListContainer;

	public ServiceProviderLocationBrandMappingPage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(ServiceProviderLocationBrandMappingNavigator.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_URI_PART));
	}

	public ServiceProviderLocationBrandMappingPage(String masterCategoryId) {
		this.masterCategoryId = masterCategoryId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (masterCategoryId == null) {
			selectElement(0);
		} else {
			selectElement(masterCategoryListContainer.getIndexForObjectId(masterCategoryId));
		}

		initCreateLocationButton();
	}

	protected Table createList() {
		masterCategoryTable = new Table();

		masterCategoryListQuery = new ServiceProviderLocationBrandMappingListQuery();
		masterCategoryListContainer = new LazyLoadingContainer(masterCategoryListQuery, 30);
		masterCategoryTable.setContainerDataSource(masterCategoryListContainer);

		// Column headers
		masterCategoryTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		masterCategoryTable.setColumnWidth("icon", 22);
		masterCategoryTable.addContainerProperty("id", String.class, null);
		masterCategoryTable.addContainerProperty("name", String.class, null);
		masterCategoryTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a masterCategory
		masterCategoryTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = masterCategoryTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String masterCategoryId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ServiceProviderLocationBrandMappingDetailPanel(ServiceProviderLocationBrandMappingPage.this, masterCategoryId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(ServiceProviderLocationBrandMappingNavigator.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_URI_PART, masterCategoryId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(ServiceProviderLocationBrandMappingNavigator.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_URI_PART));
				}
			}
		});

		return masterCategoryTable;
	}

	/**
	 * Call when some masterCategory data has been changed
	 */
	public void notifyLocationChanged(String masterCategoryId) {
		// Clear cache
		masterCategoryTable.removeAllItems();
		masterCategoryListContainer.removeAllItems();

		masterCategoryTable.select(masterCategoryListContainer.getIndexForObjectId(masterCategoryId));
	}

	public void initCreateLocationButton() {
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewServiceProviderLocationBrandMappingPopupWindow newServiceProviderLocationBrandMappingPopupWindow = new NewServiceProviderLocationBrandMappingPopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newServiceProviderLocationBrandMappingPopupWindow);
			}
		});
		

	}

}
