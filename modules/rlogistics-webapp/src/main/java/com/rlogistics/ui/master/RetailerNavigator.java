package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class RetailerNavigator extends ManagementNavigator {

	public static final String RETAILER_URI_PART = "retailer";

	public String getTrigger() {
		return RETAILER_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showRetailerPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showRetailerPage();
		}
	}
}
