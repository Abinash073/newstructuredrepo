package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.RetailerFAQ;
import com.rlogistics.master.identity.MasterdataService;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class RetailerFAQListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public RetailerFAQListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createRetailerFAQQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<RetailerFAQ> locations = masterdataService.createRetailerFAQQuery().orderByRetailerId().asc().listPage(start, count);

		List<Item> locationListItems = new ArrayList<Item>();
		for (RetailerFAQ location : locations) {
			locationListItems.add(new RetailerFAQListItem(location));
		}
		return locationListItems;
	}

	public Item loadSingleResult(String id) {
		return new RetailerFAQListItem(masterdataService.createRetailerFAQQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class RetailerFAQListItem extends PropertysetItem implements Comparable<RetailerFAQListItem> {

		private static final long serialVersionUID = 1L;

		public RetailerFAQListItem(RetailerFAQ location) {
			addItemProperty("id", new ObjectProperty<String>(location.getId(), String.class));
			addItemProperty("retailerId", new ObjectProperty<String>(location.getRetailerId(), String.class));
		}

		public int compareTo(RetailerFAQListItem other) {
			// Locations are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("retailer").getValue();
			String otherName = (String) other.getItemProperty("retailer").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
