package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class BrandNavigator extends ManagementNavigator {

	public static final String BRAND_URI_PART = "brand";

	public String getTrigger() {
		return BRAND_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showBrandPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showBrandPage();
		}
	}
}
