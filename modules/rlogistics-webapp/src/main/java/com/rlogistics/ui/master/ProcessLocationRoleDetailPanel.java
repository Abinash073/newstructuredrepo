/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocationRole;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ProcessLocationRoleDetailPanel extends DetailPanel {

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected ProcessLocationRolePage locationRolePage;
	protected ProcessLocationRole locationRole;

	protected boolean editingDetails;
	protected HorizontalLayout locationRoleDetailsLayout;
	protected TextField locationField;
	protected TextField roleField;
	protected TextField usersField;

	public ProcessLocationRoleDetailPanel(ProcessLocationRolePage locationRolePage, String locationRoleId) {
		this.locationRolePage = locationRolePage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.locationRole = masterdataService.createProcessLocationRoleQuery().id(locationRoleId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationRoleDetails();

		initActions();
	}

	protected void initActions() {
		locationRolePage.initCreateLocationRoleButton();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded locationRoleImage = new Embedded(null, Images.USER_50);
		layout.addComponent(locationRoleImage);

		Label locationRoleName = new Label(locationRole.getLocationId() + " " + locationRole.getRoleId());
		locationRoleName.setSizeUndefined();
		locationRoleName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(locationRoleName);
		layout.setComponentAlignment(locationRoleName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(locationRoleName, 1.0f);
	}

	protected void initLocationRoleDetails() {
		Label locationRoleDetailsHeader = new Label(i18nManager.getMessage(Messages.ROLE_HEADER_DETAILS));
		locationRoleDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		locationRoleDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(locationRoleDetailsHeader);

		// Details: picture and basic info
		locationRoleDetailsLayout = new HorizontalLayout();
		locationRoleDetailsLayout.setSpacing(true);
		locationRoleDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(locationRoleDetailsLayout);

		populateLocationRoleDetails();
	}

	protected void populateLocationRoleDetails() {
		loadLocationRoleDetails();
		initDetailsActions();
	}

	protected void loadLocationRoleDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		locationRoleDetailsLayout.addComponent(detailGrid);

		// Details
		addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_ID), new Label(locationRole.getId())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_LOCATION),
					new Label(locationRole.getLocationId()));
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_ROLE),
					new Label(locationRole.getRoleId()));
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_USERS),
					new Label(locationRole.getUsers()));
		} else {
			locationField = new TextField(null, locationRole.getLocationId() != null ? locationRole.getLocationId() : "");
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_LOCATION), locationField);
			roleField = new TextField(null, locationRole.getRoleId() != null ? locationRole.getRoleId() : "");
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_ROLE), roleField);
			usersField = new TextField(null, locationRole.getUsers() != null ? locationRole.getUsers() : "");
			addLocationRoleDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ROLE_USERS), usersField);
			locationField.focus();
		}
	}

	protected void addLocationRoleDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		locationRoleDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.LOCATION_ROLE_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				locationRoleDetailsLayout.removeAllComponents();
				populateLocationRoleDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.LOCATION_ROLE_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalLocation = locationRole.getLocationId();

				// Change data
				locationRole.setLocationId(locationField.getValue().toString());
				locationRole.setRoleId(roleField.getValue().toString());
				locationRole.setUsers(usersField.getValue().toString());
				masterdataService.saveProcessLocationRole(locationRole);

				// Refresh detail panel
				editingDetails = false;
				locationRoleDetailsLayout.removeAllComponents();
				populateLocationRoleDetails();

				// Refresh task list (only if name was changed)
				if (locationChanged(originalLocation)) {
					locationRolePage.notifyLocationRoleChanged(locationRole.getId());
				}
			}
		});
	}

	protected boolean locationChanged(String originalLocation) {
		boolean locationChanged = false;
		if (originalLocation != null) {
			locationChanged = !originalLocation.equals(locationRole.getLocationId());
		} else {
			locationChanged = locationRole.getLocationId() != null;
		}

		return locationChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.LOCATION_ROLE_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.LOCATION_ROLE_CONFIRM_DELETE, locationRole.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete locationRole from database
						masterdataService.deleteProcessLocationRole(masterdataService.createProcessLocationRoleQuery().locationId(locationRole.getLocationId()).roleId(locationRole.getRoleId()).singleResult().getId());

						// Update ui
						locationRolePage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
