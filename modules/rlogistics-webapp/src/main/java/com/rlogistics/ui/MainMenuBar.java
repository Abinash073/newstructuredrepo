/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui;

import java.util.HashMap;
import java.util.Map;

import org.activiti.explorer.Environments;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.profile.ChangePasswordPopupWindow;

import com.rlogistics.ui.login.UIUser;
import com.vaadin.terminal.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 * @author Frederik Heremans
 */
@SuppressWarnings("serial")
public class MainMenuBar extends HorizontalLayout {

  private static final long serialVersionUID = 1L;
  
  protected RLogisticsViewManager viewManager;
  protected RLogisticsI18nManager i18nManager;
  protected Map<String, Button> menuItemButtons;
  protected String currentMainNavigation;
  
  public MainMenuBar() {
    this.viewManager = RLogisticsApp.get().getViewManager();
    this.i18nManager = RLogisticsApp.get().getI18nManager();
    
    menuItemButtons = new HashMap<String, Button>();
    init();
  }
  
  /**
   * Highlights the given main navigation in the menubar.
   */
  public synchronized void setMainNavigation(String navigation) {
    if(currentMainNavigation != null) {
      menuItemButtons.get(currentMainNavigation).removeStyleName(ExplorerLayout.STYLE_ACTIVE);
    }
    currentMainNavigation = navigation;
    
    Button current = menuItemButtons.get(navigation);
    if(current != null) {
      current.addStyleName(ExplorerLayout.STYLE_ACTIVE);
    }
  }
  
  protected void init() {
    setHeight(54, UNITS_PIXELS);
    setWidth(100, UNITS_PERCENTAGE);
    
    setMargin(false, true, false, false);
    
    initTitle();
    initButtons();
    initProfileButton();
  }
 
  protected void initButtons() {
    Button taskButton = addMenuButton(RLogisticsViewManager.MAIN_NAVIGATION_TASK, i18nManager.getMessage(Messages.MAIN_MENU_TASKS), Images.MAIN_MENU_TASKS, false, 80);
    taskButton.addListener(new ShowTasksClickListener());
    menuItemButtons.put(RLogisticsViewManager.MAIN_NAVIGATION_TASK, taskButton);
    
    Button processButton = addMenuButton(RLogisticsViewManager.MAIN_NAVIGATION_PROCESS, i18nManager.getMessage(Messages.MAIN_MENU_PROCESS), Images.MAIN_MENU_PROCESS, false, 80);
    processButton.addListener(new ShowProcessDeploymentsClickListener());
    menuItemButtons.put(RLogisticsViewManager.MAIN_NAVIGATION_PROCESS, processButton);
    
    if(RLogisticsApp.get().adminModeEnabled()){
        Button masterButton = addMenuButton(RLogisticsViewManager.MAIN_NAVIGATION_MASTER, i18nManager.getMessage(Messages.MAIN_MENU_MASTER), Images.MAIN_MENU_MASTER, false, 80);
        masterButton.addListener(new ShowMasterClickListener());
        menuItemButtons.put(RLogisticsViewManager.MAIN_NAVIGATION_MASTER, masterButton);
    }
  }

  protected void initTitle() {
    Label title = new Label();
    title.addStyleName(Reindeer.LABEL_H1);
    
    if (RLogisticsApp.get().getEnvironment().equals(Environments.ALFRESCO)) {
      title.addStyleName(ExplorerLayout.STYLE_WORKFLOW_CONSOLE_LOGO);
    } else {
      title.addStyleName(ExplorerLayout.STYLE_APPLICATION_LOGO);
    }
    
    addComponent(title);
    
    setExpandRatio(title, 1.0f);
  }

  protected Button addMenuButton(String type, String label, Resource icon, boolean active, float width) {
    Button button = new Button(label);
    button.addStyleName(type);
    button.addStyleName(ExplorerLayout.STYLE_MAIN_MENU_BUTTON);
    button.addStyleName(Reindeer.BUTTON_LINK);
    button.setHeight(54, UNITS_PIXELS);
    button.setIcon(icon);
    button.setWidth(width, UNITS_PIXELS);
    
    addComponent(button);
    setComponentAlignment(button, Alignment.TOP_CENTER);
    
    return button;
  }
  
  protected void initProfileButton() {
    final UIUser user = RLogisticsApp.get().getUIUser();

    // User name + link to profile 
    MenuBar profileMenu = new MenuBar();
    profileMenu.addStyleName(ExplorerLayout.STYLE_HEADER_PROFILE_BOX);
    MenuItem rootItem = profileMenu.addItem(user.getName(), null);
    rootItem.setStyleName(ExplorerLayout.STYLE_HEADER_PROFILE_MENU);
    
    if(useProfile()) {
      // Show profile
      rootItem.addItem(i18nManager.getMessage(Messages.PROFILE_SHOW), new Command() {
        public void menuSelected(MenuItem selectedItem) {
          RLogisticsApp.get().getViewManager().showProfilePopup(user.getId());
        }
      });
      
      // Edit profile
      rootItem.addItem(i18nManager.getMessage(Messages.PROFILE_EDIT), new Command() {
        
        public void menuSelected(MenuItem selectedItem) {
          // TODO: Show in edit-mode
          RLogisticsApp.get().getViewManager().showProfilePopup(user.getId());
        }
      });
      
      // Change password
      rootItem.addItem(i18nManager.getMessage(Messages.PASSWORD_CHANGE), new Command() {
        public void menuSelected(MenuItem selectedItem) {
          RLogisticsApp.get().getViewManager().showPopupWindow(new ChangePasswordPopupWindow());
        }
      });
      
      rootItem.addSeparator();
    }
   
    // Logout
    rootItem.addItem(i18nManager.getMessage(Messages.HEADER_LOGOUT), new Command() {
      public void menuSelected(MenuItem selectedItem) {
        RLogisticsApp.get().close();
      }
    });

    addComponent(profileMenu);
    setComponentAlignment(profileMenu, Alignment.TOP_RIGHT);
    setExpandRatio(profileMenu, 1.0f);
  }
  
  protected boolean useProfile() {
    return true;
  }
  
  // Listener classes
  private class ShowTasksClickListener implements ClickListener {
    public void buttonClick(ClickEvent event) {
      RLogisticsApp.get().getViewManager().showInboxPage();
    }
  }  

  private class ShowProcessDeploymentsClickListener implements ClickListener {
	    public void buttonClick(ClickEvent event) {
	    	RLogisticsApp.get().getViewManager().showProcessesPage();
	    }
	  }  

  private class ShowMasterClickListener implements ClickListener {
	    public void buttonClick(ClickEvent event) {
	    	RLogisticsApp.get().getViewManager().showMasterPage();
	    }
	  }  
}
