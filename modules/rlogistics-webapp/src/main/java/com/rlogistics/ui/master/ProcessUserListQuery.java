package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProcessUserListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;
	public ProcessUserListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createProcessUserQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<ProcessUser> users = masterdataService.createProcessUserQuery().orderByUserFirstName().asc().orderByUserLastName()
				.asc().orderByUserId().asc().listPage(start, count);

		List<Item> userListItems = new ArrayList<Item>();
		for (ProcessUser user : users) {
			userListItems.add(new ProcessUserListItem(user));
		}
		return userListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProcessUserListItem(masterdataService.createProcessUserQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProcessUserListItem extends PropertysetItem implements Comparable<ProcessUserListItem> {

		private static final long serialVersionUID = 1L;

		public ProcessUserListItem(ProcessUser user) {
			addItemProperty("id", new ObjectProperty<String>(user.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(
					user.getFirstName() + " " + user.getLastName(), String.class));
		}

		public int compareTo(ProcessUserListItem other) {
			// Users are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
