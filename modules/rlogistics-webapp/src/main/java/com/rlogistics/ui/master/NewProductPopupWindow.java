package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.PopupWindow;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

public class NewProductPopupWindow extends PopupWindow {

	private static final long serialVersionUID = 1L;
	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;
	protected Form form;

	public NewProductPopupWindow() {
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		
		setCaption(i18nManager.getMessage(Messages.PRODUCT_CREATE));
		setModal(true);
		center();
		setResizable(false);
		setWidth(275, UNITS_PIXELS);
		setHeight(300, UNITS_PIXELS);
		addStyleName(Reindeer.WINDOW_LIGHT);

		initEnterKeyListener();
		initForm();
	}

	protected void initEnterKeyListener() {
		addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				handleFormSubmit();
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { new ShortcutAction("enter", ShortcutAction.KeyCode.ENTER, null) };
			}
		});
	}

	protected void initForm() {
		form = new Form();
		form.setValidationVisibleOnCommit(true);
		form.setImmediate(true);
		addComponent(form);

		initInputFields();
		initCreateButton();
	}

	protected void initInputFields() {
		form.addField("name", new TextField(i18nManager.getMessage(Messages.PRODUCT_NAME)));
		form.getField("name").focus();
		form.addField("code", new TextField(i18nManager.getMessage(Messages.PRODUCT_CODE)));
		form.addField("description", new TextField(i18nManager.getMessage(Messages.PRODUCT_DESCRIPTION)));
		form.addField("subCategory", new TextField(i18nManager.getMessage(Messages.PRODUCT_SUB_CATEGORY)));
		form.addField("brand", new TextField(i18nManager.getMessage(Messages.PRODUCT_BRAND)));
		form.addField("model", new TextField(i18nManager.getMessage(Messages.PRODUCT_MODEL)));
		form.addField("packagingType", new TextField(i18nManager.getMessage(Messages.PRODUCT_PACKAGING_TYPE)));
	}

	protected void initCreateButton() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().addComponent(buttonLayout);

		Button createButton = new Button(i18nManager.getMessage(Messages.PRODUCT_CREATE));
		buttonLayout.addComponent(createButton);
		buttonLayout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

		createButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				handleFormSubmit();
			}
		});
	}

	protected void handleFormSubmit() {
		try {
			// create location
			form.commit(); // will throw exception in case validation is false
			Product category = createProduct();

			// close popup and navigate to fresh location
			close();
			RLogisticsApp.get().getViewManager().showProductPage(category.getId());
		} catch (InvalidValueException e) {
			// Do nothing: the Form component will render the errormsgs
			// automatically
			setHeight(340, UNITS_PIXELS);
		}
	}

	protected Product createProduct() {
		Product brand= masterdataService.newProduct();
		
		if (form.getField("name").getValue() != null) {
			brand.setName(form.getField("name").getValue().toString());
		}
		brand.setCode(form.getField("code").getValue().toString());
		brand.setDescription(form.getField("description").getValue().toString());
		brand.setSubCategoryId(form.getField("subCategory").getValue().toString());
		brand.setBrandId(form.getField("brand").getValue().toString());
		brand.setModel(form.getField("model").getValue().toString());
		brand.setPackagingTypeBoxId(form.getField("packagingType").getValue().toString());
		
		masterdataService.saveProduct(brand);
		return brand;
	}
}
