package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProductSubCategoryNavigator extends ManagementNavigator {

	public static final String PRODUCT_SUB_CATEGORY_URI_PART = "productSubCategory";

	public String getTrigger() {
		return PRODUCT_SUB_CATEGORY_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showProductSubCategoryPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showProductSubCategoryPage();
		}
	}
}
