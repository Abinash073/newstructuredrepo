package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.PopupWindow;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocationRole;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

public class NewProcessLocationRolePopupWindow extends PopupWindow {

	private static final long serialVersionUID = 1L;
	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;
	protected Form form;

	public NewProcessLocationRolePopupWindow() {
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		
		setCaption(i18nManager.getMessage(Messages.ROLE_CREATE));
		setModal(true);
		center();
		setResizable(false);
		setWidth(275, UNITS_PIXELS);
		setHeight(300, UNITS_PIXELS);
		addStyleName(Reindeer.WINDOW_LIGHT);

		initEnterKeyListener();
		initForm();
	}

	protected void initEnterKeyListener() {
		addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				handleFormSubmit();
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { new ShortcutAction("enter", ShortcutAction.KeyCode.ENTER, null) };
			}
		});
	}

	protected void initForm() {
		form = new Form();
		form.setValidationVisibleOnCommit(true);
		form.setImmediate(true);
		addComponent(form);

		initInputFields();
		initCreateButton();
	}

	protected void initInputFields() {
		form.addField("location", new TextField(i18nManager.getMessage(Messages.LOCATION_ROLE_LOCATION)));
		form.getField("location").focus();
		form.addField("role", new TextField(i18nManager.getMessage(Messages.LOCATION_ROLE_ROLE)));
		form.addField("users", new TextField(i18nManager.getMessage(Messages.LOCATION_ROLE_USERS)));
	}

	protected void initCreateButton() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().addComponent(buttonLayout);

		Button createButton = new Button(i18nManager.getMessage(Messages.LOCATION_ROLE_CREATE));
		buttonLayout.addComponent(createButton);
		buttonLayout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

		createButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				handleFormSubmit();
			}
		});
	}

	protected void handleFormSubmit() {
		try {
			// create locationRole
			form.commit(); // will throw exception in case validation is false
			ProcessLocationRole locationRole = createLocationRole();

			// close popup and navigate to fresh locationRole
			close();
			RLogisticsApp.get().getViewManager().showProcessLocationRolePage(locationRole.getId());
		} catch (InvalidValueException e) {
			// Do nothing: the Form component will render the errormsgs
			// automatically
			setHeight(340, UNITS_PIXELS);
		}
	}

	protected ProcessLocationRole createLocationRole() {
		ProcessLocationRole locationRole = masterdataService.newProcessLocationRole();
		if (form.getField("location").getValue() != null) {
			locationRole.setLocationId(form.getField("location").getValue().toString());
		}
		if (form.getField("role").getValue() != null) {
			locationRole.setRoleId(form.getField("role").getValue().toString());
		}
		if (form.getField("users").getValue() != null) {
			locationRole.setUsers(form.getField("users").getValue().toString());
		}

		masterdataService.saveProcessLocationRole(locationRole);
		return locationRole;
	}

}
