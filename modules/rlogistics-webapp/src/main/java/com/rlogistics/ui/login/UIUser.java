package com.rlogistics.ui.login;

public interface UIUser {
	String getEmail();

	String getName();

	String getId();
	
	boolean isAdmin();
	
	String getRoleCode();
	
	String getLocationId();
	
	String getRetailerId();

	String getNumber();
}
