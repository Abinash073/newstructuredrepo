package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class PackagingTypeNavigator extends ManagementNavigator {

	public static final String PACKAGING_TYPE_URI_PART = "packagingType";

	public String getTrigger() {
		return PACKAGING_TYPE_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showPackagingTypePage(id);
		} else {
			RLogisticsApp.get().getViewManager().showPackagingTypePage();
		}
	}
}
