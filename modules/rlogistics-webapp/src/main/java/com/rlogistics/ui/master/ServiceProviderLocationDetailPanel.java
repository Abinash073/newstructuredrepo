/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ServiceProviderLocation;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ServiceProviderLocationDetailPanel extends DetailPanel{

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected ServiceProviderLocationPage categoryPage;
	protected ServiceProviderLocation category;

	protected boolean editingDetails;
	protected HorizontalLayout categoryDetailsLayout;
	protected TextField nameField;
	protected TextField codeField;

	private TextField serviceProviderField;

	private TextField address1Field;

	private TextField address2Field;

	private TextField cityField;

	private TextField phoneField;

	private TextField emailField;

	private TextField contactPersonField;

	public ServiceProviderLocationDetailPanel(ServiceProviderLocationPage categoryPage, String categoryId) {
		this.categoryPage = categoryPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.category = masterdataService.createServiceProviderLocationQuery().id(categoryId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationDetails();

		initActions();
	}

	protected void initActions() {
		categoryPage.initCreateLocationMenuItem();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded categoryImage = new Embedded(null, Images.USER_50);
		layout.addComponent(categoryImage);

		/*Label categoryName = new Label(category.getName() + " " + category.getName());
		categoryName.setSizeUndefined();
		categoryName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(categoryName);
		layout.setComponentAlignment(categoryName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(categoryName, 1.0f);*/
	}

	protected void initLocationDetails() {
		Label categoryDetailsHeader = new Label(i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_HEADER_DETAILS));
		categoryDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		categoryDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(categoryDetailsHeader);

		// Details: picture and basic info
		categoryDetailsLayout = new HorizontalLayout();
		categoryDetailsLayout.setSpacing(true);
		categoryDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(categoryDetailsLayout);

		populateServiceProviderLocationDetails();
	}

	protected void populateServiceProviderLocationDetails() {
		loadServiceProviderLocationDetails();
		initDetailsActions();
	}

	protected void loadServiceProviderLocationDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		categoryDetailsLayout.addComponent(detailGrid);

		// Details
//		addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_NAME), new Label(category.getName())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			/*addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_NAME),
					new Label(category.getName()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CODE),
					new Label(category.getCode()));*/
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_SERVICE_PROVIDER),
					new Label(category.getServiceProviderId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_ADDRESS1),
					new Label(category.getAddress1()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_ADDRESS2),
					new Label(category.getAddress2()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CITY),
					new Label(category.getCity()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_PHONE),
					new Label(category.getPhone1()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_EMAIL),
					new Label(category.getEmail()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CONTACT_PERSON),
					new Label(category.getContactPerson()));
		} else {
	//		nameField = new TextField(null, category.getName() != null ? category.getName() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_NAME), nameField);
			nameField.focus();
	//		codeField = new TextField(null, category.getCode() != null ? category.getCode() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CODE), codeField);
			serviceProviderField = new TextField(null, category.getServiceProviderId() != null ? category.getServiceProviderId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_SERVICE_PROVIDER), serviceProviderField);
			address1Field = new TextField(null, category.getAddress1() != null ? category.getAddress1() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_ADDRESS1), address1Field);
			address2Field = new TextField(null, category.getAddress2() != null ? category.getAddress2() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_ADDRESS2),address2Field);
			cityField = new TextField(null, category.getCity() != null ? category.getCity() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CITY), cityField);
			phoneField = new TextField(null, category.getPhone1() != null ? category.getPhone1() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_PHONE), phoneField);
			emailField = new TextField(null, category.getEmail() != null ? category.getEmail() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_EMAIL), emailField);
			contactPersonField = new TextField(null, category.getContactPerson() != null ? category.getContactPerson() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CONTACT_PERSON), contactPersonField);
		}
	}

	protected void addLocationDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		categoryDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				categoryDetailsLayout.removeAllComponents();
				populateServiceProviderLocationDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

			//	String originalName = category.getName();

				// Change data
			//	category.setName(nameField.getValue().toString());
			//	category.setCode(codeField.getValue().toString());
				category.setServiceProviderId(serviceProviderField.getValue().toString());
				category.setAddress1(address1Field.getValue().toString());
				category.setAddress2(address2Field.getValue().toString());
				category.setCity(cityField.getValue().toString());
				category.setPhone1(phoneField.getValue().toString());
				category.setEmail(emailField.getValue().toString());
				category.setContactPerson(contactPersonField.getValue().toString());

				masterdataService.saveServiceProviderLocation(category);

				// Refresh detail panel
				editingDetails = false;
				categoryDetailsLayout.removeAllComponents();
				populateServiceProviderLocationDetails();

				// Refresh task list (only if name was changed)
				/*if (nameChanged(originalName)) {
					categoryPage.notifyLocationChanged(category.getId());
				}*/
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		/*if (originalName != null) {
			nameChanged = !originalName.equals(category.getName());
		} else {
			nameChanged = category.getName() != null;
		}*/

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.SERVICE_PROVIDER_LOCATION_CONFIRM_DELETE, category.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete category from database
						masterdataService.deleteServiceProviderLocation(category.getId());

						// Update ui
						categoryPage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
