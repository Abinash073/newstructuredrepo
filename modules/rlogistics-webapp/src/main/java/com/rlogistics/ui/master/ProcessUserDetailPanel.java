/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ProcessUserDetailPanel extends DetailPanel{

  private static final long serialVersionUID = 1L;
  
  protected transient MasterdataService masterdataService;
  protected RLogisticsI18nManager i18nManager;
  
  protected ProcessUserPage userPage;
  protected ProcessUser user;
  
  protected boolean editingDetails;
  protected HorizontalLayout userDetailsLayout;
  protected TextField firstNameField;
  protected TextField lastNameField;
  protected TextField emailField;
  protected TextField phoneField;
  protected PasswordField passwordField;
  
  public ProcessUserDetailPanel(ProcessUserPage userPage, String userId) {
    this.userPage = userPage;
    this.i18nManager = RLogisticsApp.get().getI18nManager();
    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
    this.user = masterdataService.createProcessUserQuery().id(userId).singleResult();
    
    init();
  }
  
  protected void init() {
    setSizeFull();
    addStyleName(Reindeer.PANEL_LIGHT);
    
    initPageTitle();
    initUserDetails();

    initActions();
  }
  
	protected void initActions() {
		userPage.initCreateUserButton();
	}

  protected void initPageTitle() {
    HorizontalLayout layout = new HorizontalLayout();
    layout.setWidth(100, UNITS_PERCENTAGE);
    layout.setSpacing(true);
    layout.setMargin(false, false, true, false);
    layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
    addDetailComponent(layout);
    
    Embedded userImage = new Embedded(null, Images.USER_50);
    layout.addComponent(userImage);
    
    Label userName = new Label(user.getFirstName() + " " + user.getLastName());
    userName.setSizeUndefined();
    userName.addStyleName(Reindeer.LABEL_H2);
    layout.addComponent(userName);
    layout.setComponentAlignment(userName, Alignment.MIDDLE_LEFT);
    layout.setExpandRatio(userName, 1.0f);
  }
  
  protected void initUserDetails() {
    Label userDetailsHeader = new Label(i18nManager.getMessage(Messages.USER_HEADER_DETAILS));
    userDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
    userDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
    addDetailComponent(userDetailsHeader);
    
    // Details: picture and basic info
    userDetailsLayout = new HorizontalLayout();
    userDetailsLayout.setSpacing(true);
    userDetailsLayout.setMargin(false, false, true, false);
    addDetailComponent(userDetailsLayout);
    
    populateUserDetails();
  }
  
  protected void populateUserDetails() {
    loadUserDetails();
    initDetailsActions();
  }

  protected void loadUserDetails() {
    // Grid of details
    GridLayout detailGrid = new GridLayout();
    detailGrid.setColumns(2);
    detailGrid.setSpacing(true);
    detailGrid.setMargin(true, true, false, true);
    userDetailsLayout.addComponent(detailGrid);
    
    // Details
    addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_ID), new Label(user.getId())); // details are non-editable
    if (!editingDetails) {
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_FIRSTNAME), new Label(user.getFirstName()));
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_LASTNAME), new Label(user.getLastName()));
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_EMAIL), new Label(user.getEmail()));
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_PHONE), new Label(user.getPhone()));
    } else {
      firstNameField = new TextField(null, user.getFirstName() != null ? user.getFirstName() : "");
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_FIRSTNAME), firstNameField);
      firstNameField.focus();
      
      lastNameField = new TextField(null, user.getLastName() != null ? user.getLastName() : "");
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_LASTNAME), lastNameField);
      
      emailField = new TextField(null, user.getEmail() != null ? user.getEmail() : "");
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_EMAIL), emailField);
      
      phoneField = new TextField(null, user.getPhone() != null ? user.getPhone() : "");
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_PHONE), phoneField);

      passwordField = new PasswordField();
      Label cautionLabel = new Label(i18nManager.getMessage(Messages.USER_RESET_PASSWORD));
      cautionLabel.addStyleName(Reindeer.LABEL_SMALL);
      HorizontalLayout passwordLayout = new HorizontalLayout();
      passwordLayout.setSpacing(true);
      passwordLayout.addComponent(passwordField);
      passwordLayout.addComponent(cautionLabel);
      passwordLayout.setComponentAlignment(cautionLabel, Alignment.MIDDLE_LEFT);
      addUserDetail(detailGrid, i18nManager.getMessage(Messages.USER_PASSWORD), passwordLayout);
    }
  }
  
  protected void addUserDetail(GridLayout detailLayout, String detail, Component value) {
    Label label = new Label(detail + ": ");
    label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
    detailLayout.addComponent(label);
    detailLayout.addComponent(value);
  }
  
  protected void initDetailsActions() {
    VerticalLayout actionLayout = new VerticalLayout();
    actionLayout.setSpacing(true);
    actionLayout.setMargin(false, false, false, true);
    userDetailsLayout.addComponent(actionLayout);
    
    if (!editingDetails) {
      initEditButton(actionLayout);
      initDeleteButton(actionLayout);
    } else {
      initSaveButton(actionLayout);
    }
  }

  protected void initEditButton(VerticalLayout actionLayout) {
    Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
    editButton.addStyleName(Reindeer.BUTTON_SMALL);
    actionLayout.addComponent(editButton);
    editButton.addListener(new ClickListener() {
      public void buttonClick(ClickEvent event) {
        editingDetails = true;
        userDetailsLayout.removeAllComponents();
        populateUserDetails(); // the layout will be populated differently since the 'editingDetails' boolean is set
      }
    });
  }
  
  protected void initSaveButton(VerticalLayout actionLayout) {
    Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
    saveButton.addStyleName(Reindeer.BUTTON_SMALL);
    actionLayout.addComponent(saveButton);
    saveButton.addListener(new ClickListener() {
      public void buttonClick(ClickEvent event) {
        
        String originalFirstName = user.getFirstName();
        String originalLastName = user.getLastName();
        
        // Change data
        user.setFirstName(firstNameField.getValue().toString());
        user.setLastName(lastNameField.getValue().toString());
        user.setEmail(emailField.getValue().toString());
        user.setPhone(phoneField.getValue().toString());
        if (passwordField.getValue() != null && !"".equals(passwordField.getValue().toString())) {
          user.setPassword(passwordField.getValue().toString());
        }
        masterdataService.saveProcessUser(user);
        
        // Refresh detail panel
        editingDetails = false;
        userDetailsLayout.removeAllComponents();
        populateUserDetails();
        
       // Refresh task list (only if name was changed)
       if (nameChanged(originalFirstName, originalLastName)) {
         userPage.notifyUserChanged(user.getId());
       }
      }
    });
  }
  
  protected boolean nameChanged(String originalFirstName, String originalLastName) {
    boolean nameChanged = false;
    if (originalFirstName != null) {
      nameChanged = !originalFirstName.equals(user.getFirstName());
    } else {
      nameChanged = user.getFirstName() != null;
    }
    
    if (!nameChanged) {
      if (originalLastName != null) {
        nameChanged = !originalLastName.equals(user.getLastName());
      } else {
        nameChanged = user.getLastName() != null;
      }
    }
    return nameChanged;
  }
  
  protected void initDeleteButton(VerticalLayout actionLayout) {
    Button deleteButton = new Button(i18nManager.getMessage(Messages.USER_DELETE));
    deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
    actionLayout.addComponent(deleteButton);
    deleteButton.addListener(new ClickListener() {
      public void buttonClick(ClickEvent event) {
    	  RLogisticsConfirmationDialogPopupWindow confirmPopup = 
          new RLogisticsConfirmationDialogPopupWindow(i18nManager.getMessage(Messages.USER_CONFIRM_DELETE, user.getId()));
        
        confirmPopup.addListener(new ConfirmationEventListener() {
          protected void rejected(ConfirmationEvent event) {
          }
          protected void confirmed(ConfirmationEvent event) {
            // Delete user from database
            masterdataService.deleteProcessUser(user.getId());

            // Update ui
            userPage.refreshSelectNext();
          }
        });
        
        RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
      }
    });
  }  
}
