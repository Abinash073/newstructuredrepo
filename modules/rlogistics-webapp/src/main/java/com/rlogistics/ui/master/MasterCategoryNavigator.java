package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class MasterCategoryNavigator extends ManagementNavigator {

	public static final String MASTER_CATEGORY_URI_PART = "masterCategory";

	public String getTrigger() {
		return MASTER_CATEGORY_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showMasterCategoryPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showMasterCategoryPage();
		}
	}
}
