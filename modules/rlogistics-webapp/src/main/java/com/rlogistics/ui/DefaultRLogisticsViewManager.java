/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.UploadPopupWindow;
import org.activiti.explorer.ui.management.deployment.rlogistics.MetadataUploadReceiver;
import org.springframework.beans.factory.annotation.Autowired;

import com.rlogistics.ui.master.BrandPage;
import com.rlogistics.ui.master.FAQPage;
import com.rlogistics.ui.master.MasterCategoryPage;
import com.rlogistics.ui.master.MasterMenuBar;
import com.rlogistics.ui.master.MasterdataUploadReceiver;
import com.rlogistics.ui.master.PackagingTypePage;
import com.rlogistics.ui.master.ProcessLocationPage;
import com.rlogistics.ui.master.ProcessLocationRolePage;
import com.rlogistics.ui.master.ProcessUserPage;
import com.rlogistics.ui.master.ProductCategoryPage;
import com.rlogistics.ui.master.ProductPage;
import com.rlogistics.ui.master.ProductSubCategoryPage;
import com.rlogistics.ui.master.RLogisticsUploadPopupWindow;
import com.rlogistics.ui.master.RetailerFAQPage;
import com.rlogistics.ui.master.RetailerPage;
import com.rlogistics.ui.master.ServiceProviderLocationBrandMappingPage;
import com.rlogistics.ui.master.ServiceProviderLocationPage;
import com.rlogistics.ui.master.ServiceProviderPage;
import com.rlogistics.ui.process.ListDeployedProcessesPage;
import com.rlogistics.ui.task.InboxPage;
import com.rlogistics.ui.task.InvolvedPage;
import com.rlogistics.ui.task.QueuedPage;
import com.rlogistics.ui.task.TaskMenuBar;
import com.rlogistics.ui.task.TasksPage;
import com.vaadin.ui.Window;

/**
 * @author Joram Barrez
 */
public class DefaultRLogisticsViewManager implements RLogisticsViewManager, Serializable {

	private static final long serialVersionUID = -1712344958488358861L;

	protected RLogisticsAbstractPage currentPage;

	@Autowired
	protected RLogisticsMainWindow mainWindow;

	protected transient TaskService taskService;
	protected transient HistoryService historyService;
	protected transient IdentityService identityService;

	public DefaultRLogisticsViewManager() {
		this.taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
		this.historyService = ProcessEngines.getDefaultProcessEngine().getHistoryService();
		this.identityService = ProcessEngines.getDefaultProcessEngine().getIdentityService();
	}

	public void showLoginPage() {
		if (!mainWindow.isShowingLoginPage()) {
			mainWindow.showLoginPage();
		}
	}

	public void showDefaultPage() {
		mainWindow.showDefaultContent();
		showInboxPage();
	}

	public void showPopupWindow(Window window) {
		mainWindow.addWindow(window);
	}

	//Process
	
	// Tasks

	/**
	 * Generic method which will figure out to which task page must be jumped,
	 * based on the task data.
	 * 
	 * Note that, if possible, it is always more performant to use the more
	 * specific showXXXPage() methods.
	 */
	public void showTaskPage(String taskId) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		String loggedInUserId = RLogisticsApp.get().getUIUser().getEmail();

		if (loggedInUserId.equals(task.getOwner())) {
			showTasksPage(taskId);
		} else if (loggedInUserId.equals(task.getAssignee())) {
			showInboxPage(taskId);
		} else if (taskService.createTaskQuery().taskInvolvedUser(loggedInUserId).count() == 1) {
			showInvolvedPage(taskId);
		} else {
			// queued
			List<String> groupIds = getGroupIds(loggedInUserId);
			List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(task.getId());
			Iterator<IdentityLink> identityLinkIterator = identityLinks.iterator();

			boolean pageFound = false;
			while (!pageFound && identityLinkIterator.hasNext()) {
				IdentityLink identityLink = identityLinkIterator.next();
				if (identityLink.getGroupId() != null && groupIds.contains(identityLink.getGroupId())) {
					showQueuedPage(identityLink.getGroupId(), task.getId());
					pageFound = true;
				}
			}

			// We've tried hard enough, the user now gets a notification. He
			// deserves it.
			if (!pageFound) {
				showNavigationError(taskId);
			}
		}
	}

	protected List<String> getGroupIds(String userId) {
		List<String> groupIds = new ArrayList<String>();
		List<Group> groups = identityService.createGroupQuery().groupMember(userId).list();
		for (Group group : groups) {
			groupIds.add(group.getId());
		}

		return groupIds;
	}

	protected void showNavigationError(String taskId) {
		RLogisticsApp.get().getNotificationManager().showErrorNotification(Messages.NAVIGATION_ERROR_NOT_INVOLVED_TITLE,
				RLogisticsApp.get().getI18nManager().getMessage(Messages.NAVIGATION_ERROR_NOT_INVOLVED, taskId));
	}

	@Override
	public void showMasterPage() {
		showProcessUserPage();
	}

	@Override
	public void showProcessesPage() {
		switchView(new ListDeployedProcessesPage(), RLogisticsViewManager.MAIN_NAVIGATION_PROCESS, null);
	}

	@Override
	public void showProcessPage(String processName) {
		switchView(new ListDeployedProcessesPage(processName), RLogisticsViewManager.MAIN_NAVIGATION_PROCESS, null);
	}
	public void showTasksPage() {
		switchView(new TasksPage(), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_TASKS);
	}

	public void showTasksPage(String taskId) {
		switchView(new TasksPage(taskId), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_TASKS);
	}

	public void showInboxPage() {
		switchView(new InboxPage(), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_INBOX);
	}

	public void showInboxPage(String taskId) {
		switchView(new InboxPage(taskId), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_INBOX);
	}

	public void showQueuedPage(String groupId) {
		switchView(new QueuedPage(groupId), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_QUEUED);
	}

	public void showQueuedPage(String groupId, String taskId) {
		switchView(new QueuedPage(groupId, taskId), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_QUEUED);
	}

	public void showInvolvedPage() {
		switchView(new InvolvedPage(), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_INVOLVED);
	}

	public void showInvolvedPage(String taskId) {
		switchView(new InvolvedPage(taskId), RLogisticsViewManager.MAIN_NAVIGATION_TASK, TaskMenuBar.ENTRY_INVOLVED);
	}

	// Helper

	protected void switchView(RLogisticsAbstractPage page, String mainMenuActive, String subMenuActive) {
		currentPage = page;
		mainWindow.setMainNavigation(mainMenuActive);
		mainWindow.switchView(page);
	}

	public RLogisticsAbstractPage getCurrentPage() {
		return currentPage;
	}

	public void setMainWindow(RLogisticsMainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}

	@Override
	public void showProfilePopup(String id) {
	}

	@Override
	public RLogisticsMainWindow getMainWindow() {
		return mainWindow;
	}

	@Override
	public void showProcessUserPage() {
		switchView(new ProcessUserPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_USERS);
	}

	@Override
	public void showProcessUserPage(String userId) {
		switchView(new ProcessUserPage(userId), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_USERS);
	}

	@Override
	public void showProcessLocationPage() {
		switchView(new ProcessLocationPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_LOCATIONS);
	}

	@Override
	public void showProcessLocationPage(String locationId) {
		switchView(new ProcessLocationPage(locationId), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_LOCATIONS);
	}

	@Override
	public void showProcessLocationRolePage() {
		switchView(new ProcessLocationRolePage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_LOCATION_ROLES);
	}

	@Override
	public void showProcessLocationRolePage(String locationRoleId) {
		switchView(new ProcessLocationRolePage(locationRoleId), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_LOCATION_ROLES);
	}
	
	public void showUploadMetadataPopup() {
	    MetadataUploadReceiver receiver = new MetadataUploadReceiver();
		RLogisticsI18nManager i18nManager = RLogisticsApp.get().getI18nManager();
	    UploadPopupWindow uploadPopupWindow = new UploadPopupWindow(
	            i18nManager.getMessage(Messages.MASTERDATA_UPLOAD),
	            i18nManager.getMessage(Messages.MASTERDATA_UPLOAD_DESCRIPTION),
	            receiver);
	    
	    // The receiver also acts as a listener for the end of the upload 
	    // so it can switch to the new deployment page
	    uploadPopupWindow.addFinishedListener(receiver);
	    showPopupWindow(uploadPopupWindow);
	}

	@Override
	public void showUploadMasterDataPopup() {
	    MasterdataUploadReceiver receiver = new MasterdataUploadReceiver();
		RLogisticsI18nManager i18nManager = RLogisticsApp.get().getI18nManager();
		RLogisticsUploadPopupWindow uploadPopupWindow = new RLogisticsUploadPopupWindow(
	            i18nManager.getMessage(Messages.MASTERDATA_UPLOAD),
	            i18nManager.getMessage(Messages.MASTERDATA_UPLOAD_DESCRIPTION),
	            receiver);
	    
	    // The receiver also acts as a listener for the end of the upload 
	    // so it can switch to the new deployment page
	    uploadPopupWindow.addFinishedListener(receiver);
	    showPopupWindow(uploadPopupWindow);
	}

	@Override
	public void showMasterCategoryPage() {
		switchView(new MasterCategoryPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_MASTER_CATEGORIES);
	}

	@Override
	public void showMasterCategoryPage(String id) {
		switchView(new MasterCategoryPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_MASTER_CATEGORIES);
	}


	@Override
	public void showProductCategoryPage() {
		switchView(new ProductCategoryPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCT_CATEGORIES);
	}

	@Override
	public void showProductCategoryPage(String id) {
		switchView(new ProductCategoryPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCT_CATEGORIES);
	}

	@Override
	public void showProductSubCategoryPage() {
		switchView(new ProductSubCategoryPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCT_SUB_CATEGORIES);
	}

	@Override
	public void showProductSubCategoryPage(String id) {
		switchView(new ProductSubCategoryPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCT_SUB_CATEGORIES);
	}

	@Override
	public void showBrandPage() {
		switchView(new BrandPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_BRANDS);
	}

	@Override
	public void showBrandPage(String id) {
		switchView(new BrandPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_BRANDS);
	}

	@Override
	public void showPackagingTypePage() {
		switchView(new PackagingTypePage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PACKAGING_TYPES);
	}

	@Override
	public void showPackagingTypePage(String id) {
		switchView(new PackagingTypePage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PACKAGING_TYPES);
	}

	@Override
	public void showServiceProviderPage() {
		switchView(new ServiceProviderPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDERS);
	}

	@Override
	public void showServiceProviderPage(String id) {
		switchView(new ServiceProviderPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDERS);
	}

	@Override
	public void showServiceProviderLocationPage() {
		switchView(new ServiceProviderLocationPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDER_LOCATIONS);
	}

	@Override
	public void showServiceProviderLocationPage(String id) {
		switchView(new ServiceProviderLocationPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDER_LOCATIONS);
	}

	@Override
	public void showServiceProviderLocationBrandMappingPage() {
		switchView(new ServiceProviderLocationBrandMappingPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDER_LOCATION_BRAND_MAPPINGS);
	}

	@Override
	public void showServiceProviderLocationBrandMappingPage(String id) {
		switchView(new ServiceProviderLocationBrandMappingPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_SERVICE_PROVIDER_LOCATION_BRAND_MAPPINGS);
	}

	@Override
	public void showProductPage() {
		switchView(new ProductPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCTS);
	}

	@Override
	public void showProductPage(String id) {
		switchView(new ProductPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_PRODUCTS);
	}

	@Override
	public void showFAQPage() {
		switchView(new FAQPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_FAQS);
	}

	@Override
	public void showFAQPage(String id) {
		switchView(new FAQPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_FAQS);
	}

	@Override
	public void showRetailerPage() {
		switchView(new RetailerPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_RETAILERS);
	}

	@Override
	public void showRetailerPage(String id) {
		switchView(new RetailerPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_RETAILERS);
	}

	@Override
	public void showRetailerFAQPage() {
		switchView(new RetailerFAQPage(), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_RETAILER_FAQS);
	}

	@Override
	public void showRetailerFAQPage(String id) {
		switchView(new RetailerFAQPage(id), RLogisticsViewManager.MAIN_NAVIGATION_MASTER, MasterMenuBar.ENTRY_RETAILER_FAQS);
	}
}
