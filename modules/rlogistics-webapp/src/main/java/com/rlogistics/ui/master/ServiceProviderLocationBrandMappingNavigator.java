package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ServiceProviderLocationBrandMappingNavigator extends ManagementNavigator {

	public static final String SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_URI_PART = "serviceProviderLocationBrandMapping";

	public String getTrigger() {
		return SERVICE_PROVIDER_LOCATION_BRAND_MAPPING_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showServiceProviderLocationBrandMappingPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showServiceProviderLocationBrandMappingPage();
		}
	}
}
