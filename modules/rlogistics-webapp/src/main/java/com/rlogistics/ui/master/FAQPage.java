/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing faqs.
 * 
 * @author Joram Barrez
 */
public class FAQPage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String faqId;
	protected Table faqTable;
	protected LazyLoadingQuery faqListQuery;
	protected LazyLoadingContainer faqListContainer;

	public FAQPage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(FAQNavigator.FAQ_URI_PART));
	}

	public FAQPage(String faqId) {
		this.faqId = faqId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (faqId == null) {
			selectElement(0);
		} else {
			selectElement(faqListContainer.getIndexForObjectId(faqId));
		}

		initCreateLocationMenuItem();
	}

	protected Table createList() {
		faqTable = new Table();

		faqListQuery = new FAQListQuery();
		faqListContainer = new LazyLoadingContainer(faqListQuery, 30);
		faqTable.setContainerDataSource(faqListContainer);

		// Column headers
		faqTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		faqTable.setColumnWidth("icon", 22);
		faqTable.addContainerProperty("id", String.class, null);
		faqTable.addContainerProperty("code", String.class, null);
		faqTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a faq
		faqTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = faqTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String faqId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new FAQDetailPanel(FAQPage.this, faqId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(FAQNavigator.FAQ_URI_PART, faqId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(FAQNavigator.FAQ_URI_PART));
				}
			}
		});

		return faqTable;
	}

	/**
	 * Call when some faq data has been changed
	 */
	public void notifyLocationChanged(String faqId) {
		// Clear cache
		faqTable.removeAllItems();
		faqListContainer.removeAllItems();

		faqTable.select(faqListContainer.getIndexForObjectId(faqId));
	}

	public void initCreateLocationMenuItem() {
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.FAQ_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewFAQPopupWindow newFAQPopupWindow = new NewFAQPopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newFAQPopupWindow);
			}
		});		
	}

}
