package com.rlogistics.ui.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.FormGroup;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.form.FormData;
import org.activiti.engine.form.FormProperty;

import com.rlogistics.form.FormPropertiesEventListener;
import com.rlogistics.form.FormPropertiesForm;
import com.rlogistics.form.FormPropertiesForm.FormPropertiesEvent;
import com.rlogistics.http.RLogisticsHttpSessionHolder;

public class FormPaginatorUtil {
	protected transient FormService formService;
	protected transient RepositoryService repositoryService;
	protected transient RuntimeService runtimeService;
	protected transient FormDisplay display;
	protected transient ScriptExecutor scriptExecutor;
	
	public FormPaginatorUtil(FormDisplay display,ScriptExecutor scriptExecutor){
		this.display = display;
		this.scriptExecutor = scriptExecutor;
		this.formService = ProcessEngines.getDefaultProcessEngine().getFormService();
		this.repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		this.runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
	}

	public void paginate(FormData formData) {
		if (formData != null && formData.getFormProperties() != null && !formData.getFormProperties().isEmpty()) {
			/* Partition the properties into group based properties */
			final Map<String,List<org.activiti.engine.form.FormProperty>> propertiesByGroup = partitionPropertiesIntoGroups(formData.getFormProperties());
			List<FormGroup> formGroups = new ArrayList<FormGroup>(formData.getFormGroups());
			FormGroup defaultGroup = new FormGroup("default");
			formGroups.add(0,defaultGroup );
			
			display.displayForm(createTaskFormForGroup(null,propertiesByGroup, formGroups));
		}
	}

	private FormPropertiesForm createTaskFormForGroup(String group,final Map<String, List<org.activiti.engine.form.FormProperty>> propertiesByGroup,final List<FormGroup> formGroups) {
		final List<String> formGroupNames = listFormGroupNames(formGroups);
		
		if(group == null){
			group = formGroupNames.get(0);
		}
		
		int currentGroupIndex = formGroupNames.indexOf(group);
		final FormGroup formGroup = formGroups.get(currentGroupIndex);
		List<FormProperty> groupProperties = propertiesByGroup.get(group);
		
		if(currentGroupIndex == -1){
			throw new RuntimeException("Unable to find group " + group + " in " + String.join(",", formGroupNames) + "'). Configuration error.");
		}
		
		if(groupProperties == null || groupProperties.isEmpty()){
			/* Skip to next group*/
			return skipToNextGroup(propertiesByGroup, formGroups, formGroupNames, currentGroupIndex);
		}
		
		if(formGroup.getExpression() != null && !formGroup.getExpression().equals("")){
			Object expressionValue = scriptExecutor.execute(formGroup.getExpression());
			if (!(expressionValue instanceof Boolean)) {
				throw new RuntimeException("Expression " + formGroup.getExpression() + " does not evaluate to a boolean. It is " + (expressionValue != null ? expressionValue.getClass() : "NULL"));
			} else {
				boolean enabled = ((Boolean)expressionValue);
				if(!enabled){
					return skipToNextGroup(propertiesByGroup, formGroups, formGroupNames, currentGroupIndex);
				}
			}
		}

		if(formGroup.getBeforeGroupScript() != null && !formGroup.getAfterGroupScript().equals("")){
			scriptExecutor.execute(formGroup.getBeforeGroupScript());
		}
		
		final FormPropertiesForm form = new FormPropertiesForm(scriptExecutor);
		final boolean isLastGroup = (currentGroupIndex == (formGroupNames.size()-1));
		if(isLastGroup){
			form.setSubmitButtonCaption(display.getCommitButtonCaption());
		} else {
			form.setSubmitButtonCaption(display.getNextButtonCaption());
		}
		form.setCancelButtonCaption(display.getCancelButtonCaption());
		form.setFormHelp(display.getHelpMessage());

		form.setFormProperties(groupProperties);

		form.addListener(new FormPropertiesEventListener() {

			private static final long serialVersionUID = -3893467157397686736L;

			@Override
			protected void handleFormSubmit(FormPropertiesEvent event) {
				Map<String, String> properties = event.getFormProperties();
				try {
					String previousGroup =(String) getInProgressValue(MARKER_GROUP_NAME);
					
					int previousGroupIndex = 0;
					
					if(previousGroup != null){
						previousGroupIndex = formGroupNames.indexOf(previousGroup);
						
						for(String property:properties.keySet()){
							setInProgressValue(property, properties.get(property));
						}
	
						display.submitFormProperties(properties);
						
						if(previousGroupIndex == -1){
							throw new RuntimeException("Unable to locate previously committed group '" + previousGroup + "' within groups defined('" + String.join(",", formGroupNames) + "'). Configuration error.");
						}
						
						if(formGroup.getAfterGroupScript() != null && !formGroup.getAfterGroupScript().equals("")){
							scriptExecutor.execute(formGroup.getAfterGroupScript());
						}
					}
					
					if(previousGroupIndex == (formGroupNames.size()-1)){
						/* All groups done */
						completeForm();
					} else {
						/* Show next group */
						FormPropertiesForm nextForm = createTaskFormForGroup(formGroupNames.get(previousGroupIndex+1), propertiesByGroup, formGroups);
						
						if(nextForm == null){
							completeForm();
						} else {
							display.displayForm(nextForm);
						}
					}
				} catch (Exception ex) {
					display.handleException(ex);
				}
			}

			private void completeForm() {
				display.handleFormCompletion(getInProgressValues());
				clearInProgressValues();
			}

			@Override
			protected void handleFormCancel(FormPropertiesEvent event) {
				/* TODO GOTO group 1*/
				form.clear();
				display.displayForm(createTaskFormForGroup(formGroupNames.get(0), propertiesByGroup, formGroups));
			}
		});
		
		// Only if current user is task's assignee
		form.setEnabled(display.isFormEnabled());

		setInProgressValue(MARKER_GROUP_NAME, group);
		
		return form;
	}

	private FormPropertiesForm skipToNextGroup(
			final Map<String, List<org.activiti.engine.form.FormProperty>> propertiesByGroup,
			final List<FormGroup> formGroups, final List<String> formGroupNames, int currentGroupIndex) {
		if(currentGroupIndex >= (formGroupNames.size()-1)){
			/* No more groups -- no form */
			return null;
		}
		
		return createTaskFormForGroup(formGroupNames.get(currentGroupIndex+1), propertiesByGroup, formGroups);
	}

	@SuppressWarnings("unchecked")
	private HashMap<String,String> getInProgressValues(){
		return (HashMap<String, String>) RLogisticsHttpSessionHolder.getHttpSession().getAttribute(FORM_IN_PROGRESS_VALUES);
	}

	private String getInProgressValue(String k){
		HashMap<String,String> inProgressValues = getInProgressValues();
		return inProgressValues == null ? null : inProgressValues.get(k);
	}

	private void setInProgressValue(String k,String v){
		HashMap<String, String> inProgressValues = getInProgressValues();
		
		if(inProgressValues == null){
			inProgressValues = new HashMap<String,String>(); 
		}

		inProgressValues.put(k,v);
		
		RLogisticsHttpSessionHolder.getHttpSession().setAttribute(FORM_IN_PROGRESS_VALUES,inProgressValues);
	}
	
	private void clearInProgressValues(){
		RLogisticsHttpSessionHolder.getHttpSession().removeAttribute(FORM_IN_PROGRESS_VALUES);
	}
	
	private List<String> listFormGroupNames(List<FormGroup> formGroups) {
		List<String> formGroupNames = new ArrayList<String>();
		
		for(FormGroup formGroup:formGroups){
			formGroupNames.add(formGroup.getName());
		}

		return formGroupNames;
	}

	private static final String MARKER_GROUP_NAME = "xxMarkerGroup";
	private static final String FORM_IN_PROGRESS_VALUES = "xxFormInProgressValues";
	
	private Map<String, List<org.activiti.engine.form.FormProperty>> partitionPropertiesIntoGroups(List<org.activiti.engine.form.FormProperty> formProperties) {
		Map<String, List<org.activiti.engine.form.FormProperty>> propertiesByGroup = new HashMap<String, List<org.activiti.engine.form.FormProperty>>();
		
		for(org.activiti.engine.form.FormProperty formProperty:formProperties){
			String group = formProperty.getGroup();
			if(group == null){
				group = "default";
			}
			if(!propertiesByGroup.containsKey(group)){
				propertiesByGroup.put(group,new ArrayList<org.activiti.engine.form.FormProperty>());
			}
			
			propertiesByGroup.get(group).add(formProperty);
		}
		return propertiesByGroup;
	}


	public static interface FormDisplay{
		public void displayForm(FormPropertiesForm form);
		public void submitFormProperties(Map<String, String> properties);
		public boolean isFormEnabled();
		public void handleException(Exception ex);
		public String getHelpMessage();
		public String getCancelButtonCaption();
		public String getCommitButtonCaption();
		public String getNextButtonCaption();
		public void handleFormCompletion(Map<String,String> properties);
		public void handleFormCancellation();
	}
}
