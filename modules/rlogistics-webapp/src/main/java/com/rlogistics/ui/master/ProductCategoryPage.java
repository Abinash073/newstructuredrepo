/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing productCategorys.
 * 
 * @author Joram Barrez
 */
public class ProductCategoryPage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String productCategoryId;
	protected Table productCategoryTable;
	protected LazyLoadingQuery productCategoryListQuery;
	protected LazyLoadingContainer productCategoryListContainer;

	public ProductCategoryPage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(ProductCategoryNavigator.PRODUCT_CATEGORY_URI_PART));
	}

	public ProductCategoryPage(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (productCategoryId == null) {
			selectElement(0);
		} else {
			selectElement(productCategoryListContainer.getIndexForObjectId(productCategoryId));
		}

		initCreateLocationButton();
	}

	protected Table createList() {
		productCategoryTable = new Table();

		productCategoryListQuery = new ProductCategoryListQuery();
		productCategoryListContainer = new LazyLoadingContainer(productCategoryListQuery, 30);
		productCategoryTable.setContainerDataSource(productCategoryListContainer);

		// Column headers
		productCategoryTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		productCategoryTable.setColumnWidth("icon", 22);
		productCategoryTable.addContainerProperty("id", String.class, null);
		productCategoryTable.addContainerProperty("name", String.class, null);
		productCategoryTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a productCategory
		productCategoryTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = productCategoryTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String productCategoryId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ProductCategoryDetailPanel(ProductCategoryPage.this, productCategoryId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(ProductCategoryNavigator.PRODUCT_CATEGORY_URI_PART, productCategoryId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(ProductCategoryNavigator.PRODUCT_CATEGORY_URI_PART));
				}
			}
		});

		return productCategoryTable;
	}

	/**
	 * Call when some productCategory data has been changed
	 */
	public void notifyLocationChanged(String productCategoryId) {
		// Clear cache
		productCategoryTable.removeAllItems();
		productCategoryListContainer.removeAllItems();

		productCategoryTable.select(productCategoryListContainer.getIndexForObjectId(productCategoryId));
	}

	public void initCreateLocationButton() {
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.PRODUCT_CATEGORY_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewProductCategoryPopupWindow newProductCategoryPopupWindow = new NewProductCategoryPopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newProductCategoryPopupWindow);
			}
		});
		
	}

}
