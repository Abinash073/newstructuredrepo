package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.MasterdataService;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProductCategoryListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public ProductCategoryListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createProductCategoryQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<ProductCategory> locations = masterdataService.createProductCategoryQuery().orderByName().asc().listPage(start, count);

		List<Item> locationListItems = new ArrayList<Item>();
		for (ProductCategory location : locations) {
			locationListItems.add(new ProductCategoryListItem(location));
		}
		return locationListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProductCategoryListItem(masterdataService.createProductCategoryQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProductCategoryListItem extends PropertysetItem implements Comparable<ProductCategoryListItem> {

		private static final long serialVersionUID = 1L;

		public ProductCategoryListItem(ProductCategory location) {
			addItemProperty("id", new ObjectProperty<String>(location.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(location.getName(), String.class));
		}

		public int compareTo(ProductCategoryListItem other) {
			// Locations are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
