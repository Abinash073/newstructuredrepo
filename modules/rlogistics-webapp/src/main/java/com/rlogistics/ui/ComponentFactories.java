package com.rlogistics.ui;

import com.rlogistics.ui.master.MasterEntityManagementMenuBarFactory;
import com.rlogistics.ui.master.RLogisticsUploadComponentFactory;

public class ComponentFactories extends org.activiti.explorer.ComponentFactories {

	public ComponentFactories(){
		factories.put(MasterEntityManagementMenuBarFactory.class, new MasterEntityManagementMenuBarFactory());
		factories.put(RLogisticsUploadComponentFactory.class, new RLogisticsUploadComponentFactory());
	}
}
