package com.rlogistics.ui.master;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.MasterCategory;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.ProcessLocationRole;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.ServiceProvider;
import com.rlogistics.master.identity.ServiceProviderLocation;
import com.rlogistics.master.identity.ServiceProviderLocationBrandMapping;
import com.rlogistics.ui.CSVDataProcessor;

public class MasterdataStreamProcessor extends CSVDataProcessor{
	public static void processUpload(InputStream is) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		
		try {
			BufferedReader bis = new BufferedReader(new InputStreamReader(is));
			
			String line = null;
			int lineNo = 0;
			while((line = bis.readLine()) != null){
				lineNo++;
				
				String[] fields = parseLine(line,lineNo);
				
				if(fields == null || fields.length == 0) continue;
				
				String op = fields[1];
				
				System.out.println("PROCESSING:" + lineNo + ":" + line);
				for(String f:fields){
					System.out.println("------------" + f);
				}

				if(fields[0].equals(LOCATION)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String parentLocationId = fields.length >= 5 ? fields[4] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProcessLocation location = masterdataService.createProcessLocationQuery().code(code).singleResult();
						
						if(location == null){
							location = masterdataService.newProcessLocation();
						}
						
						location.setName(name);
						location.setCode(code);
						location.setParentLocationId(parentLocationId);
						
						masterdataService.saveProcessLocation(location);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProcessLocation(name);
					}					
				} else if(fields[0].equals(USER)){
					String firstName = fields.length >= 3 ? fields[2] : null;
					String lastName = fields.length >= 4 ? fields[3] : null;
					String password = fields.length >= 5 ? fields[4] : null;
					String email = fields.length >= 6 ? fields[5] : null;
					String phone = fields.length >= 7 ? fields[6] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProcessUser user = masterdataService.createProcessUserQuery().email(email).singleResult();
						
						if(user == null){
							user = masterdataService.newProcessUser();
						}
						
						user.setFirstName(firstName);
						user.setLastName(lastName);
						user.setEmail(email);
						user.setPassword(password);
						user.setPhone(phone);
						
						masterdataService.saveProcessUser(user);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProcessUser(email);
					}					
				} else if(fields[0].equals(LOCATION_ROLE)){
					String location = fields.length >= 3 ? fields[2] : null;
					String role = fields.length >= 4 ? fields[3] : null;
					String users = fields.length >= 5 ? fields[4] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProcessLocationRole locationRole = masterdataService.createProcessLocationRoleQuery().locationId(location).roleId(role).singleResult();
						
						if(locationRole == null){
							locationRole = masterdataService.newProcessLocationRole();
						}
						
						locationRole.setLocationId(location);
						locationRole.setRoleId(role);
						locationRole.setUsers(users);
						
						masterdataService.saveProcessLocationRole(locationRole);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProcessLocationRole(masterdataService.createProcessLocationRoleQuery().locationId(location).roleId(role).singleResult().getId());
					}
				} else if(fields[0].equals(MASTER_CATEGORY)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String description = fields.length >= 5 ? fields[4] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						MasterCategory masterCategory = masterdataService.createMasterCategoryQuery().code(code).singleResult();
						
						if(masterCategory == null){
							masterCategory = masterdataService.newMasterCategory();
						}
						
						masterCategory.setName(name);
						masterCategory.setCode(code);
						masterCategory.setDescription(description);
						
						masterdataService.saveMasterCategory(masterCategory);
					} else if(op.equals(DELETE)){
						masterdataService.deleteMasterCategory(name);
					}
				} else if(fields[0].equals(PRODUCT_CATEGORY)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String masterCategory = fields.length >= 5 ? fields[4] : null;
					String description = fields.length >= 6 ? fields[5] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProductCategory productCategory = masterdataService.createProductCategoryQuery().code(code).singleResult();
						
						if(productCategory == null){
							productCategory = masterdataService.newProductCategory();
						}
						
						productCategory.setName(name);
						productCategory.setCode(code);
						productCategory.setMasterCategoryId(masterCategory);
						productCategory.setDescription(description);
						
						masterdataService.saveProductCategory(productCategory);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProductCategory(name);
					}
				} else if(fields[0].equals(PRODUCT_SUB_CATEGORY)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String productCategory = fields.length >= 5 ? fields[4] : null;
					String description = fields.length >= 6 ? fields[5] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProductSubCategory productSubCategory = masterdataService.createProductSubCategoryQuery().code(code).singleResult();
						
						if(productSubCategory == null){
							productSubCategory = masterdataService.newProductSubCategory();
						}
						
						productSubCategory.setName(name);
						productSubCategory.setCode(code);
						productSubCategory.setProductCategoryId(productCategory);
						productSubCategory.setDescription(description);
						
						masterdataService.saveProductSubCategory(productSubCategory);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProductSubCategory(name);
					}
				} else if(fields[0].equals(BRAND)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String parentBrand = fields.length >= 5 ? fields[4] : null;
					String description = fields.length >= 6 ? fields[5] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						Brand brand = masterdataService.createBrandQuery().code(code).singleResult();
						
						if(brand == null){
							brand = masterdataService.newBrand();
						}
						
						brand.setName(name);
						brand.setCode(code);
						brand.setParentBrandId(parentBrand);
						brand.setDescription(description);
						
						masterdataService.saveBrand(brand);
					} else if(op.equals(DELETE)){
						masterdataService.deleteBrand(name);
					}
				} else if(fields[0].equals(FAQ)){
					String code = fields.length >= 3 ? fields[2] : null;
					String product = fields.length >= 4 ? fields[3] : null;
					String brand = fields.length >= 5 ? fields[4] : null;
					String category = fields.length >= 6 ? fields[5] : null;
					String subCategory = fields.length >= 7 ? fields[6] : null;
					String faqText = fields.length >= 8 ? fields[7] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						com.rlogistics.master.identity.FAQ faq = masterdataService.createFAQQuery().code(code).singleResult();
						
						if(faq == null){
							faq = masterdataService.newFAQ();
						}
						
						faq.setCode(code);
						faq.setProductId(product);
						faq.setBrandId(brand);
						faq.setCategoryId(category);
						faq.setSubCategoryId(subCategory);
						faq.setFaq(faqText);

						masterdataService.saveFAQ(faq);
					} else if(op.equals(DELETE)){
						masterdataService.deleteFAQ(code);
					}
				} else if(fields[0].equals(RETAILER)){
					String code = fields.length >= 3 ? fields[2] : null;
					String name = fields.length >= 4 ? fields[3] : null;
					String isAdvancePickupAvailable = fields.length >= 5 ? fields[4] : null;
					String email = fields.length >= 6 ? fields[5] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						com.rlogistics.master.identity.Retailer retailer = masterdataService.createRetailerQuery().code(code).singleResult();
						
						if(retailer == null){
							retailer = masterdataService.newRetailer();
						}
						
						retailer.setCode(code);
						retailer.setName(name);
						
						retailer.setEmail(email);

						masterdataService.saveRetailer(retailer);
					} else if(op.equals(DELETE)){
						masterdataService.deleteRetailer(code);
					}
				} else if(fields[0].equals(RETAILER_FAQ)){
					String retailer = fields.length >= 3 ? fields[2] : null;
					String faq = fields.length >= 4 ? fields[3] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						com.rlogistics.master.identity.RetailerFAQ retailerFAQ = masterdataService.createRetailerFAQQuery().retailerId(retailer).singleResult();
						
						if(retailerFAQ == null){
							retailerFAQ = masterdataService.newRetailerFAQ();
						}
						
						retailerFAQ.setRetailerId(retailer);
						retailerFAQ.setFaq(faq);
						
						masterdataService.saveRetailerFAQ(retailerFAQ);
					} else if(op.equals(DELETE)){
						masterdataService.deleteRetailerFAQ(retailer);
					}
				} else if(fields[0].equals(PACKAGING_TYPE)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					float cost = Float.parseFloat(fields.length >= 5 ? fields[4] : "0");
					String description = fields.length >= 6 ? fields[5] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						PackagingType packagingType = masterdataService.createPackagingTypeQuery().code(code).singleResult();
						
						if(packagingType == null){
							packagingType = masterdataService.newPackagingType();
						}
						
						packagingType.setName(name);
						packagingType.setCode(code);
						packagingType.setCost(cost);
						packagingType.setDescription(description);
						
						masterdataService.savePackagingType(packagingType);
					} else if(op.equals(DELETE)){
						masterdataService.deletePackagingType(name);
					}
				} else if(fields[0].equals(SERVICE_PROVIDER)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String description = fields.length >= 5 ? fields[4] : "";

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ServiceProvider serviceProvider = masterdataService.createServiceProviderQuery().code(code).singleResult();
						
						if(serviceProvider == null){
							serviceProvider = masterdataService.newServiceProvider();
						}
						
						serviceProvider.setName(name);
						serviceProvider.setCode(code);
						serviceProvider.setDescription(description);
						
						masterdataService.saveServiceProvider(serviceProvider);
					} else if(op.equals(DELETE)){
						masterdataService.deleteServiceProvider(name);
					}
				} else if(fields[0].equals(SERVICE_PROVIDER_LOCATION)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String serviceProvider = fields.length >= 5 ? fields[4] : null;
					String address1 = fields.length >= 6 ? fields[5] : null;
					String address2 = fields.length >= 7 ? fields[6] : null;
					String city = fields.length >= 8 ? fields[7] : null;
					String phone = fields.length >= 9 ? fields[8] : null;
					String email = fields.length >= 10 ? fields[9] : null;
					String contactPerson = fields.length >= 11 ? fields[10] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ServiceProviderLocation serviceProviderLocation = masterdataService.createServiceProviderLocationQuery().serviceProviderCode(code).singleResult();
						
						if(serviceProviderLocation == null){
							serviceProviderLocation = masterdataService.newServiceProviderLocation();
						}
						
						/*serviceProviderLocation.setName(name);
						serviceProviderLocation.setCode(code);*/
						serviceProviderLocation.setServiceProviderId(serviceProvider);
						serviceProviderLocation.setAddress1(address1);
						serviceProviderLocation.setAddress2(address2);
						serviceProviderLocation.setCity(city);
						serviceProviderLocation.setPhone1(phone);
						serviceProviderLocation.setEmail(email);
						serviceProviderLocation.setContactPerson(contactPerson);
						
						masterdataService.saveServiceProviderLocation(serviceProviderLocation);
					} else if(op.equals(DELETE)){
						masterdataService.deleteServiceProviderLocation(name);
					}
				} else if(fields[0].equals(SERVICE_PROVIDER_LOCATION_BRAND_MAPPING)){
					String serviceProviderLocation = fields.length >= 3 ? fields[2] : null;
					String brand = fields.length >= 4 ? fields[3] : null;
					String authorizationType= fields.length >= 5 ? fields[4] : null;
					String subCategory = fields.length >= 6 ? fields[5] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						ServiceProviderLocationBrandMapping serviceProviderLocationBrandMapping = masterdataService.createServiceProviderLocationBrandMappingQuery().serviceProviderLocationId(serviceProviderLocation).brandId(brand).singleResult();
						
						if(serviceProviderLocationBrandMapping == null){
							serviceProviderLocationBrandMapping = masterdataService.newServiceProviderLocationBrandMapping();
						}
						
						serviceProviderLocationBrandMapping.setServiceProviderLocationId(serviceProviderLocation);
						serviceProviderLocationBrandMapping.setBrandId(brand);
						serviceProviderLocationBrandMapping.setAuthorizationType(authorizationType);
						serviceProviderLocationBrandMapping.setSubCategoryId(subCategory);
						
						masterdataService.saveServiceProviderLocationBrandMapping(serviceProviderLocationBrandMapping);
					} else if(op.equals(DELETE)){
						masterdataService.deleteServiceProviderLocationBrandMapping(masterdataService.createServiceProviderLocationBrandMappingQuery().serviceProviderLocationId(serviceProviderLocation).brandId(brand).singleResult().getId());
					}
				} else if(fields[0].equals(PRODUCT)){
					String name = fields.length >= 3 ? fields[2] : null;
					String code = fields.length >= 4 ? fields[3] : null;
					String subCategory = fields.length >= 5 ? fields[4] : null;
					String brand = fields.length >= 6 ? fields[5] : null;
					String model = fields.length >= 7 ? fields[6] : null;
					String packagingType = fields.length >= 8 ? fields[7] : null;
					String description = fields.length >= 9 ? fields[8] : null;

					if(op.equals(INSERT) || op.equals(UPDATE)){
						Product product = masterdataService.createProductQuery().code(code).singleResult();
						
						if(product == null){
							product = masterdataService.newProduct();
						}
						
						product.setName(name);
						product.setCode(code);
						product.setSubCategoryId(subCategory);
						product.setBrandId(brand);
						product.setModel(model);
						product.setPackagingTypeBoxId(packagingType);
						product.setDescription(description);
						
						masterdataService.saveProduct(product);
					} else if(op.equals(DELETE)){
						masterdataService.deleteProduct(name);
					}
				}
			}
			
		} catch(IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	public static void main(String[] args) {
		String[] fields = parseLine("USER,INSERT,bangalore1,1,welcome,bangalore1@gmail.com,9898989898", 1);
		for(String f:fields){
			System.out.println("------------" + f);
		}
	}
	
	public static final String LOCATION = "LOCATION";
	public static final String LOCATION_ROLE = "LOCATION_ROLE";
	public static final String USER = "USER";
	public static final String MASTER_CATEGORY = "MASTER_CATEGORY";
	public static final String PRODUCT_CATEGORY = "PRODUCT_CATEGORY";
	public static final String PRODUCT_SUB_CATEGORY = "PRODUCT_SUB_CATEGORY";
	public static final String BRAND = "BRAND";
	public static final String PACKAGING_TYPE = "PACKAGING_TYPE";
	public static final String SERVICE_PROVIDER = "SERVICE_PROVIDER";
	public static final String SERVICE_PROVIDER_LOCATION = "SERVICE_PROVIDER_LOCATION";
	public static final String SERVICE_PROVIDER_LOCATION_BRAND_MAPPING = "SERVICE_PROVIDER_LOCATION_BRAND_MAPPING";
	public static final String PRODUCT = "PRODUCT";
	public static final String FAQ = "FAQ";
	public static final String RETAILER = "RETAILER";
	public static final String RETAILER_FAQ = "RETAILER_FAQ";

	public static final String INSERT = "INSERT";
	public static final String UPDATE = "UPDATE";
	public static final String DELETE = "DELETE";
}
