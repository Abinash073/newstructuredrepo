package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.PopupWindow;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

public class NewProcessUserPopupWindow extends PopupWindow {

	private static final long serialVersionUID = 1L;
	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;
	protected Form form;

	public NewProcessUserPopupWindow() {
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		setCaption(i18nManager.getMessage(Messages.USER_CREATE));
		setModal(true);
		center();
		setResizable(false);
		setWidth(275, UNITS_PIXELS);
		setHeight(300, UNITS_PIXELS);
		addStyleName(Reindeer.WINDOW_LIGHT);

		initEnterKeyListener();
		initForm();
	}

	protected void initEnterKeyListener() {
		addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				handleFormSubmit();
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { new ShortcutAction("enter", ShortcutAction.KeyCode.ENTER, null) };
			}
		});
	}

	protected void initForm() {
		form = new Form();
		form.setValidationVisibleOnCommit(true);
		form.setImmediate(true);
		addComponent(form);

		initInputFields();
		initCreateButton();
	}

	protected void initInputFields() {
		form.addField("email", new TextField(i18nManager.getMessage(Messages.USER_EMAIL)));
		form.getField("email").setRequired(true);
		form.getField("email").setRequiredError(i18nManager.getMessage(Messages.USER_EMAIL_REQUIRED));
		// Password is required
		form.addField("password", new PasswordField(i18nManager.getMessage(Messages.USER_PASSWORD)));
		form.getField("password").focus();
		form.getField("password").setRequired(true);
		form.getField("password").setRequiredError(i18nManager.getMessage(Messages.USER_PASSWORD_REQUIRED));

		// Password must be at least 5 characters
		StringLengthValidator passwordLengthValidator = new StringLengthValidator(
				i18nManager.getMessage(Messages.USER_PASSWORD_MIN_LENGTH, 5), 5, -1, false);
		form.getField("password").addValidator(passwordLengthValidator);

		form.addField("firstName", new TextField(i18nManager.getMessage(Messages.USER_FIRSTNAME)));
		form.addField("lastName", new TextField(i18nManager.getMessage(Messages.USER_LASTNAME)));
		form.addField("phone", new TextField(i18nManager.getMessage(Messages.USER_PHONE)));
	}

	protected void initCreateButton() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().addComponent(buttonLayout);

		Button createButton = new Button(i18nManager.getMessage(Messages.USER_CREATE));
		buttonLayout.addComponent(createButton);
		buttonLayout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

		createButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				handleFormSubmit();
			}
		});
	}

	protected void handleFormSubmit() {
		try {
			// create user
			form.commit(); // will throw exception in case validation is false
			ProcessUser user = createUser();

			// close popup and navigate to fresh user
			close();
			RLogisticsApp.get().getViewManager().showProcessUserPage(user.getId());
		} catch (InvalidValueException e) {
			// Do nothing: the Form component will render the errormsgs
			// automatically
			setHeight(340, UNITS_PIXELS);
		}
	}

	protected ProcessUser createUser() {
		ProcessUser user = masterdataService.newProcessUser();
		user.setPassword(form.getField("password").getValue().toString());
		if (form.getField("firstName").getValue() != null) {
			user.setFirstName(form.getField("firstName").getValue().toString());
		}
		if (form.getField("lastName").getValue() != null) {
			user.setLastName(form.getField("lastName").getValue().toString());
		}
		if (form.getField("email").getValue() != null) {
			user.setEmail(form.getField("email").getValue().toString());
		}
		if (form.getField("phone").getValue() != null) {
			user.setPhone(form.getField("phone").getValue().toString());
		}
		masterdataService.saveProcessUser(user);
		return user;
	}

}
