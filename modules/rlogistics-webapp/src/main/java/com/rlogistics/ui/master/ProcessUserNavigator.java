package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProcessUserNavigator extends ManagementNavigator {

	public static final String USER_URI_PART = "processuser";

	public String getTrigger() {
		return USER_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String userId = uriFragment.getUriPart(1);


		if (userId != null) {
			RLogisticsApp.get().getViewManager().showProcessUserPage(userId);
		} else {
			RLogisticsApp.get().getViewManager().showProcessUserPage();
		}
	}
}
