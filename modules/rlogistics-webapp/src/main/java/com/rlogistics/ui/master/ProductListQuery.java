package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.MasterdataService;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProductListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public ProductListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createProductQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<Product> locations = masterdataService.createProductQuery().orderByName().asc().listPage(start, count);

		List<Item> locationListItems = new ArrayList<Item>();
		for (Product location : locations) {
			locationListItems.add(new ProductListItem(location));
		}
		return locationListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProductListItem(masterdataService.createProductQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProductListItem extends PropertysetItem implements Comparable<ProductListItem> {

		private static final long serialVersionUID = 1L;

		public ProductListItem(Product location) {
			addItemProperty("id", new ObjectProperty<String>(location.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(location.getName(), String.class));
		}

		public int compareTo(ProductListItem other) {
			// Locations are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
