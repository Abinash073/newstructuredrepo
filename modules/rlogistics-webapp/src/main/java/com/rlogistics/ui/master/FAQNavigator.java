package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class FAQNavigator extends ManagementNavigator {

	public static final String FAQ_URI_PART = "faq";

	public String getTrigger() {
		return FAQ_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showFAQPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showFAQPage();
		}
	}
}
