package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.FAQ;
import com.rlogistics.master.identity.MasterdataService;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class FAQListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public FAQListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createFAQQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<FAQ> locations = masterdataService.createFAQQuery().orderByCode().asc().listPage(start, count);

		List<Item> locationListItems = new ArrayList<Item>();
		for (FAQ location : locations) {
			locationListItems.add(new FAQListItem(location));
		}
		return locationListItems;
	}

	public Item loadSingleResult(String id) {
		return new FAQListItem(masterdataService.createFAQQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class FAQListItem extends PropertysetItem implements Comparable<FAQListItem> {

		private static final long serialVersionUID = 1L;

		public FAQListItem(FAQ location) {
			addItemProperty("id", new ObjectProperty<String>(location.getId(), String.class));
			addItemProperty("code", new ObjectProperty<String>(location.getCode(), String.class));
		}

		public int compareTo(FAQListItem other) {
			// Locations are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("code").getValue();
			String otherName = (String) other.getItemProperty("code").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
