package com.rlogistics.ui;

public interface MainLayoutFactory {
	public MainLayout createLayout();
}
