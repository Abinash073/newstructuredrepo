package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocationRole;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProcessLocationRoleListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public ProcessLocationRoleListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createProcessLocationRoleQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<ProcessLocationRole> locationLocationRoles = masterdataService.createProcessLocationRoleQuery().orderByLocationId().asc().orderByRoleId().asc().listPage(start, count);

		List<Item> locationLocationRoleListItems = new ArrayList<Item>();
		for (ProcessLocationRole locationLocationRole : locationLocationRoles) {
			locationLocationRoleListItems.add(new ProcessLocationRoleListItem(locationLocationRole));
		}
		return locationLocationRoleListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProcessLocationRoleListItem(masterdataService.createProcessLocationRoleQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProcessLocationRoleListItem extends PropertysetItem implements Comparable<ProcessLocationRoleListItem> {

		private static final long serialVersionUID = 1L;

		public ProcessLocationRoleListItem(ProcessLocationRole locationLocationRole) {
			addItemProperty("id", new ObjectProperty<String>(locationLocationRole.getId(), String.class));
			addItemProperty("location-role", new ObjectProperty<String>(locationLocationRole.getLocationId() + "-" + locationLocationRole.getRoleId(), String.class));
		}

		public int compareTo(ProcessLocationRoleListItem other) {
			// LocationRoles are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("location-role").getValue();
			String otherName = (String) other.getItemProperty("location-role").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
