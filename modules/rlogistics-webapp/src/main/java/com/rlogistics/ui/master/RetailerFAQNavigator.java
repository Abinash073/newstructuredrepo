package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class RetailerFAQNavigator extends ManagementNavigator {

	public static final String RETAILER_FAQ_URI_PART = "retailerfaq";

	public String getTrigger() {
		return RETAILER_FAQ_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showRetailerFAQPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showRetailerFAQPage();
		}
	}
}
