/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.FAQ;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class FAQDetailPanel extends DetailPanel{

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected FAQPage faqPage;
	protected FAQ faq;

	protected boolean editingDetails;
	protected HorizontalLayout faqDetailsLayout;
	protected TextField codeField;
	protected TextField productField;
	protected TextField brandField;
	protected TextField categoryField;
	protected TextField subCategoryField;
	protected TextArea faqField;

	public FAQDetailPanel(FAQPage faqPage, String faqId) {
		this.faqPage = faqPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.faq = masterdataService.createFAQQuery().id(faqId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationDetails();

		initActions();
	}

	protected void initActions() {
		faqPage.initCreateLocationMenuItem();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded faqImage = new Embedded(null, Images.USER_50);
		layout.addComponent(faqImage);
	}

	protected void initLocationDetails() {
		Label faqDetailsHeader = new Label(i18nManager.getMessage(Messages.FAQ_HEADER_DETAILS));
		faqDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		faqDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(faqDetailsHeader);

		// Details: picture and basic info
		faqDetailsLayout = new HorizontalLayout();
		faqDetailsLayout.setSpacing(true);
		faqDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(faqDetailsLayout);

		populateFAQDetails();
	}

	protected void populateFAQDetails() {
		loadFAQDetails();
		initDetailsActions();
	}

	protected void loadFAQDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		faqDetailsLayout.addComponent(detailGrid);

		if (!editingDetails) {
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_CODE),
					new Label(faq.getCode()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_PRODUCT),
					new Label(faq.getProductId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_BRAND),
					new Label(faq.getBrandId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_CATEGORY),
					new Label(faq.getCategoryId()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_SUB_CATEGORY),
					new Label(faq.getSubCategoryId()));
			TextArea ta = null;
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_FAQ),
					ta = new TextArea(faq.getFaq()));
			ta.setReadOnly(true);
		} else {
			codeField = new TextField(null, faq.getCode() != null ? faq.getCode() : "");
			codeField.focus();
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_CODE), codeField);
			productField = new TextField(null, faq.getProductId() != null ? faq.getProductId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_PRODUCT), productField);
			brandField = new TextField(null, faq.getBrandId() != null ? faq.getBrandId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_BRAND), brandField);
			categoryField = new TextField(null, faq.getCategoryId() != null ? faq.getCategoryId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_CATEGORY), categoryField);
			subCategoryField = new TextField(null, faq.getSubCategoryId() != null ? faq.getSubCategoryId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_SUB_CATEGORY), subCategoryField);
			faqField = new TextArea(null, faq.getFaq() != null ? faq.getFaq() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.FAQ_FAQ), faqField);
		}
	}

	protected void addLocationDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		faqDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				faqDetailsLayout.removeAllComponents();
				populateFAQDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalCode = faq.getCode();

				// Change data
				faq.setCode(codeField.getValue().toString());
				faq.setProductId(productField.getValue().toString());
				faq.setBrandId(brandField.getValue().toString());
				faq.setCategoryId(categoryField.getValue().toString());
				faq.setSubCategoryId(subCategoryField.getValue().toString());
				faq.setFaq(faqField.getValue().toString());

				masterdataService.saveFAQ(faq);

				// Refresh detail panel
				editingDetails = false;
				faqDetailsLayout.removeAllComponents();
				populateFAQDetails();

				// Refresh task list (only if name was changed)
				if (codeChanged(originalCode)) {
					faqPage.notifyLocationChanged(faq.getId());
				}
			}
		});
	}

	protected boolean codeChanged(String originalCode) {
		boolean codeChanged = false;
		if (originalCode != null) {
			codeChanged = !originalCode.equals(faq.getCode());
		} else {
			codeChanged = faq.getCode() != null;
		}

		return codeChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.FAQ_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.FAQ_CONFIRM_DELETE, faq.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete faq from database
						masterdataService.deleteFAQ(faq.getId());

						// Update ui
						faqPage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
