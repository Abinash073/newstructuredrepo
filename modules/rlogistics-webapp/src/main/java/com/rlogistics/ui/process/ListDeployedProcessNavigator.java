package com.rlogistics.ui.process;

import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.Navigator;
import com.rlogistics.ui.RLogisticsApp;

public class ListDeployedProcessNavigator implements Navigator {

	public static final String LIST_DEPLOYED_URI_PART = "listdeployed";

	@Override
	public String getTrigger() {
		return LIST_DEPLOYED_URI_PART;
	}

	@Override
	public void handleNavigation(UriFragment uriFragment) {
		String processId = uriFragment.getUriPart(1);

		if (processId != null) {
			RLogisticsApp.get().getViewManager().showProcessPage(processId);
		} else {
			RLogisticsApp.get().getViewManager().showProcessesPage();
		}
	}
}
