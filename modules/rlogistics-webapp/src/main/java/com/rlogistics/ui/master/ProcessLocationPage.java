/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing locations.
 * 
 * @author Joram Barrez
 */
public class ProcessLocationPage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String locationId;
	protected Table locationTable;
	protected LazyLoadingQuery locationListQuery;
	protected LazyLoadingContainer locationListContainer;

	public ProcessLocationPage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(ProcessLocationNavigator.LOCATION_URI_PART));
	}

	public ProcessLocationPage(String locationId) {
		this.locationId = locationId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (locationId == null) {
			selectElement(0);
		} else {
			selectElement(locationListContainer.getIndexForObjectId(locationId));
		}

		initCreateLocationButton();
	}

	protected Table createList() {
		locationTable = new Table();

		locationListQuery = new ProcessLocationListQuery();
		locationListContainer = new LazyLoadingContainer(locationListQuery, 30);
		locationTable.setContainerDataSource(locationListContainer);

		// Column headers
		locationTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		locationTable.setColumnWidth("icon", 22);
		locationTable.addContainerProperty("id", String.class, null);
		locationTable.addContainerProperty("name", String.class, null);
		locationTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a location
		locationTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = locationTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String locationId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ProcessLocationDetailPanel(ProcessLocationPage.this, locationId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(ProcessLocationNavigator.LOCATION_URI_PART, locationId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(ProcessLocationNavigator.LOCATION_URI_PART));
				}
			}
		});

		return locationTable;
	}

	/**
	 * Call when some location data has been changed
	 */
	public void notifyLocationChanged(String locationId) {
		// Clear cache
		locationTable.removeAllItems();
		locationListContainer.removeAllItems();

		locationTable.select(locationListContainer.getIndexForObjectId(locationId));
	}

	public void initCreateLocationButton() {
	
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.LOCATION_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewProcessLocationPopupWindow newProcessLocationPopupWindow = new NewProcessLocationPopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newProcessLocationPopupWindow);
			}
		});
		
	}
}
