package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProcessLocationNavigator extends ManagementNavigator {

	public static final String LOCATION_URI_PART = "processlocation";

	public String getTrigger() {
		return LOCATION_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String locationId = uriFragment.getUriPart(1);

		if (locationId != null) {
			RLogisticsApp.get().getViewManager().showProcessLocationPage(locationId);
		} else {
			RLogisticsApp.get().getViewManager().showProcessLocationPage();
		}
	}
}
