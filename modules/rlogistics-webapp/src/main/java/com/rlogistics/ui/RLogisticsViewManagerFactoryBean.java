/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui;

import java.io.Serializable;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author Joram Barrez
 */
public class RLogisticsViewManagerFactoryBean implements FactoryBean<RLogisticsViewManager>, Serializable {

	protected String environment;
	protected RLogisticsMainWindow mainWindow;

	public RLogisticsViewManager getObject() throws Exception {
		DefaultRLogisticsViewManager viewManagerImpl;
		viewManagerImpl = new DefaultRLogisticsViewManager();
		viewManagerImpl.setMainWindow(mainWindow);
		return viewManagerImpl;
	}

	public Class<?> getObjectType() {
		return RLogisticsViewManager.class;
	}

	public boolean isSingleton() {
		return true; // See https://jira.springsource.org/browse/SPR-5060
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public void setMainWindow(RLogisticsMainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}
}
