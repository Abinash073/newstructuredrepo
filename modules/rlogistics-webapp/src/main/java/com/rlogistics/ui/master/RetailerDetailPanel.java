/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class RetailerDetailPanel extends DetailPanel{

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected RetailerPage retailerPage;
	protected Retailer retailer;

	protected boolean editingDetails;
	protected HorizontalLayout retailerDetailsLayout;
	protected TextField nameField;
	protected TextField codeField;
	protected TextField emailField;

	public RetailerDetailPanel(RetailerPage retailerPage, String retailerId) {
		this.retailerPage = retailerPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationDetails();

		initActions();
	}

	protected void initActions() {
		retailerPage.initCreateLocationMenuItem();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded retailerImage = new Embedded(null, Images.USER_50);
		layout.addComponent(retailerImage);

		Label retailerName = new Label(retailer.getName() + " " + retailer.getName());
		retailerName.setSizeUndefined();
		retailerName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(retailerName);
		layout.setComponentAlignment(retailerName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(retailerName, 1.0f);
	}

	protected void initLocationDetails() {
		Label retailerDetailsHeader = new Label(i18nManager.getMessage(Messages.RETAILER_HEADER_DETAILS));
		retailerDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		retailerDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(retailerDetailsHeader);

		// Details: picture and basic info
		retailerDetailsLayout = new HorizontalLayout();
		retailerDetailsLayout.setSpacing(true);
		retailerDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(retailerDetailsLayout);

		populateRetailerDetails();
	}

	protected void populateRetailerDetails() {
		loadRetailerDetails();
		initDetailsActions();
	}

	protected void loadRetailerDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		retailerDetailsLayout.addComponent(detailGrid);

		// Details
		addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_NAME), new Label(retailer.getName())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_NAME),
					new Label(retailer.getName()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_CODE),
					new Label(retailer.getCode()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_ADVANCE_PICKUP_AVAILABLE),
					new CheckBox());
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_EMAIL),
					new Label(retailer.getEmail()));
		} else {
			nameField = new TextField(null, retailer.getName() != null ? retailer.getName() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_NAME), nameField);
			nameField.focus();
			codeField = new TextField(null, retailer.getCode() != null ? retailer.getCode() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_CODE), codeField);
			
			emailField = new TextField(null, retailer.getEmail() != null ? retailer.getEmail() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.RETAILER_EMAIL), emailField);
		}
	}

	protected void addLocationDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		retailerDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				retailerDetailsLayout.removeAllComponents();
				populateRetailerDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalName = retailer.getName();

				// Change data
				retailer.setName(nameField.getValue().toString());
				retailer.setCode(codeField.getValue().toString());
				
				retailer.setEmail(emailField.getValue().toString());

				masterdataService.saveRetailer(retailer);

				// Refresh detail panel
				editingDetails = false;
				retailerDetailsLayout.removeAllComponents();
				populateRetailerDetails();

				// Refresh task list (only if name was changed)
				if (nameChanged(originalName)) {
					retailerPage.notifyLocationChanged(retailer.getId());
				}
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		if (originalName != null) {
			nameChanged = !originalName.equals(retailer.getName());
		} else {
			nameChanged = retailer.getName() != null;
		}

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.RETAILER_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.RETAILER_CONFIRM_DELETE, retailer.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete retailer from database
						masterdataService.deleteRetailer(retailer.getId());

						// Update ui
						retailerPage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
