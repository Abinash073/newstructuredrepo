/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.process;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.scripting.ScriptingEngines;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.DetailPanel;

import com.rlogistics.form.FormPropertiesForm;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.rlogistics.ui.RLogisticsNotificationManager;
import com.rlogistics.ui.task.FormPaginatorUtil;
import com.rlogistics.ui.task.FormPaginatorUtil.FormDisplay;
import com.rlogistics.util.PreProcessInstanceVariableScope;
import com.rlogistics.ui.task.ScriptExecutor;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class StartProcessInstancePanel extends DetailPanel implements FormDisplay, ScriptExecutor {

	private static final long serialVersionUID = 1L;

	private RepositoryService repositoryService;
	protected transient MetadataService metadataService;
	private FormService formService;
	protected transient ScriptingEngines scriptingEngines;

	protected RLogisticsNotificationManager notificationManager;
	protected RLogisticsI18nManager i18nManager;

	protected ListDeployedProcessesPage listDeployedProcessesPage;
	protected ProcessDeployment processDeployment;
	protected ProcessDefinition processDefinition;
	private StartFormData startFormData;

	protected boolean editingDetails;
	protected HorizontalLayout roleDetailsLayout;

	private VerticalLayout detailPanelLayout;

	private HorizontalLayout detailContainer;

	private PreProcessInstanceVariableScope scope = new PreProcessInstanceVariableScope();
	
	public StartProcessInstancePanel(ListDeployedProcessesPage listDeployedProcessesPage, String deploymentId) {
		this.listDeployedProcessesPage = listDeployedProcessesPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.notificationManager = RLogisticsApp.get().getNotificationManager();
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		this.repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		this.formService = ProcessEngines.getDefaultProcessEngine().getFormService();
		this.processDeployment = metadataService.createProcessDeploymentQuery().id(deploymentId).singleResult();
		this.scriptingEngines = ((ProcessEngineConfigurationImpl)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getScriptingEngines();

		this.processDefinition =  repositoryService.createProcessDefinitionQuery().deploymentId(processDeployment.getDeploymentId()).singleResult();
		this.startFormData = formService.getStartFormData(processDefinition.getId());

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		detailPanelLayout = new VerticalLayout();
		detailPanelLayout.setWidth(100, UNITS_PERCENTAGE);
		detailPanelLayout.setMargin(true);
		setDetailContainer(detailPanelLayout);

		detailContainer = new HorizontalLayout();
		detailContainer.addStyleName(Reindeer.PANEL_LIGHT);
		detailPanelLayout.addComponent(detailContainer);
		detailContainer.setSizeFull();
		
		loadDeploymentDetails();
	}

	protected void loadDeploymentDetails() {
		new FormPaginatorUtil(this,this).paginate(startFormData);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		roleDetailsLayout.addComponent(actionLayout);

		initInstantiateButton(actionLayout);
	}

	protected String getProcessDisplayName(ProcessDefinition processDefinition) {
		if (processDefinition.getName() != null) {
			return processDefinition.getName();
		} else {
			return processDefinition.getKey();
		}
	}

	protected void initInstantiateButton(VerticalLayout actionLayout) {
		Button instantiateButton = new Button(i18nManager.getMessage(Messages.PROCESS_START));
		instantiateButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(instantiateButton);
		instantiateButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

			}
		});
	}

	@Override
	public void displayForm(FormPropertiesForm form) {
		detailContainer.removeAllComponents();
		detailContainer.addComponent(form);
	}

	@Override
	public boolean isFormEnabled() {
		return true;
	}

	@Override
	public void handleException(Exception ex) {
		notificationManager.showErrorNotification(Messages.PROCESS_NOT_STARTED_NOTIFICATION, ex);
	}

	@Override
	public String getHelpMessage() {
		return i18nManager.getMessage(Messages.TASK_FORM_HELP);
	}

	@Override
	public String getCancelButtonCaption() {
		return i18nManager.getMessage(Messages.BUTTON_CANCEL);
	}

	@Override
	public String getCommitButtonCaption() {
		return i18nManager.getMessage(Messages.PROCESS_START);
	}

	@Override
	public String getNextButtonCaption() {
		return i18nManager.getMessage(Messages.TASK_NEXT_GROUP);
	}
	
	@Override
	public void handleFormCompletion(Map<String, String> properties) {
		formService.submitStartFormData(processDefinition.getId(), properties);
		notificationManager.showInformationNotification(Messages.PROCESS_STARTED_NOTIFICATION,getProcessDisplayName(processDefinition));
	}

	@Override
	public void handleFormCancellation() {
	}

	@Override
	public Object execute(String script) {
		return scriptingEngines.evaluate(script, "javascript",scope.getBindingsAdapter());
	}

	@Override
	public void submitFormProperties(Map<String, String> properties) {
		Map<String,FormProperty> propertyByName = new HashMap<String, FormProperty>();
		for(FormProperty property: startFormData.getFormProperties()){
			propertyByName.put(property.getId(),property);
		}
		
		Map<String,Object> variables = new HashMap<String, Object>();
		
		for(String property:properties.keySet()){
			FormProperty formProperty = propertyByName.get(property);
			String variable = formProperty.getVariable(); 
			if(variable== null||variable.equals("")){
				variable = property;
			}

			variables.put(variable,properties.get(property));
		}
		
		scope.setVariables(variables);
	}
}
