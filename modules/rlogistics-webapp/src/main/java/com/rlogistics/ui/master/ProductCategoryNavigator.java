package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProductCategoryNavigator extends ManagementNavigator {

	public static final String PRODUCT_CATEGORY_URI_PART = "productCategory";

	public String getTrigger() {
		return PRODUCT_CATEGORY_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showProductCategoryPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showProductCategoryPage();
		}
	}
}
