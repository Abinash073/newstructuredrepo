package com.rlogistics.ui.task;

public interface ScriptExecutor {
	public Object execute(String script);
}
