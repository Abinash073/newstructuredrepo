/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing locationRoles.
 * 
 * @author Joram Barrez
 */
public class ProcessLocationRolePage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String locationRoleId;
	protected Table locationRoleTable;
	protected LazyLoadingQuery locationRoleListQuery;
	protected LazyLoadingContainer locationRoleListContainer;

	public ProcessLocationRolePage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(ProcessLocationRoleNavigator.LOCATION_ROLE_URI_PART));
	}

	public ProcessLocationRolePage(String locationRoleId) {
		this.locationRoleId = locationRoleId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (locationRoleId == null) {
			selectElement(0);
		} else {
			selectElement(locationRoleListContainer.getIndexForObjectId(locationRoleId));
		}

		initCreateLocationRoleButton();
	}

	protected Table createList() {
		locationRoleTable = new Table();

		locationRoleListQuery = new ProcessLocationRoleListQuery();
		locationRoleListContainer = new LazyLoadingContainer(locationRoleListQuery, 30);
		locationRoleTable.setContainerDataSource(locationRoleListContainer);

		// Column headers
		locationRoleTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		locationRoleTable.setColumnWidth("icon", 22);
		locationRoleTable.addContainerProperty("id", String.class, null);
		locationRoleTable.addContainerProperty("role", String.class, null);
		locationRoleTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a locationRole
		locationRoleTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = locationRoleTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String locationRoleId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ProcessLocationRoleDetailPanel(ProcessLocationRolePage.this, locationRoleId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(ProcessLocationRoleNavigator.LOCATION_ROLE_URI_PART, locationRoleId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(ProcessLocationRoleNavigator.LOCATION_ROLE_URI_PART));
				}
			}
		});

		return locationRoleTable;
	}

	/**
	 * Call when some locationRole data has been changed
	 */
	public void notifyLocationRoleChanged(String locationRoleId) {
		// Clear cache
		locationRoleTable.removeAllItems();
		locationRoleListContainer.removeAllItems();

		locationRoleTable.select(locationRoleListContainer.getIndexForObjectId(locationRoleId));
	}

	public void initCreateLocationRoleButton() {

		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.LOCATION_ROLE_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewProcessLocationRolePopupWindow newProcessLocationRolePopupWindow = new NewProcessLocationRolePopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newProcessLocationRolePopupWindow);
			}
		});
		
	}

}
