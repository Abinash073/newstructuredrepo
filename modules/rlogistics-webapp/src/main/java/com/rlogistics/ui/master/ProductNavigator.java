package com.rlogistics.ui.master;

import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

import com.rlogistics.ui.RLogisticsApp;

public class ProductNavigator extends ManagementNavigator {

	public static final String PRODUCT_URI_PART = "product";

	public String getTrigger() {
		return PRODUCT_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String id = uriFragment.getUriPart(1);

		if (id != null) {
			RLogisticsApp.get().getViewManager().showProductPage(id);
		} else {
			RLogisticsApp.get().getViewManager().showProductPage();
		}
	}
}
