/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui;

import com.vaadin.ui.Window;

/**
 * @author Joram Barrez
 */
public interface RLogisticsViewManager {

	static final String MAIN_NAVIGATION_TASK = "task";
	static final String MAIN_NAVIGATION_PROCESS = "process";
	static final String MAIN_NAVIGATION_MASTER = "master";

	// Generic

	void showLoginPage();

	void showDefaultPage();

	void showPopupWindow(Window window);

	// Tasks

	void showTaskPage(String taskId);

	void showTasksPage();

	void showTasksPage(String taskId);

	void showInboxPage();

	void showInboxPage(String taskId);

	void showQueuedPage(String groupId);

	void showQueuedPage(String groupId, String taskId);

	void showInvolvedPage();

	void showInvolvedPage(String taskId);

	void showProfilePopup(String id);

	void showProcessesPage();

	void showMasterPage();

	void showProcessPage(String processName);
	
	RLogisticsMainWindow getMainWindow();

	void showProcessUserPage();
	void showProcessUserPage(String userId);
	void showProcessLocationPage();
	void showProcessLocationPage(String locationId);
	void showProcessLocationRolePage();
	void showProcessLocationRolePage(String locationRoleId);

	void showUploadMasterDataPopup();

	void showMasterCategoryPage();
	void showMasterCategoryPage(String id);

	void showProductCategoryPage();
	void showProductCategoryPage(String id);

	void showProductSubCategoryPage();
	void showProductSubCategoryPage(String id);

	void showBrandPage();
	void showBrandPage(String id);

	void showPackagingTypePage();
	void showPackagingTypePage(String id);

	void showServiceProviderPage();
	void showServiceProviderPage(String id);

	void showServiceProviderLocationPage();
	void showServiceProviderLocationPage(String id);

	void showServiceProviderLocationBrandMappingPage();
	void showServiceProviderLocationBrandMappingPage(String id);

	void showProductPage();
	void showProductPage(String id);

	void showFAQPage();
	void showFAQPage(String id);

	void showRetailerPage();
	void showRetailerPage(String id);

	void showRetailerFAQPage();
	void showRetailerFAQPage(String id);
}
