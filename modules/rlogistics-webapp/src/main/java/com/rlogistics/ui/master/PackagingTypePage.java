/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing brands.
 * 
 * @author Joram Barrez
 */
public class PackagingTypePage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String brandId;
	protected Table brandTable;
	protected LazyLoadingQuery brandListQuery;
	protected LazyLoadingContainer brandListContainer;

	public PackagingTypePage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(PackagingTypeNavigator.PACKAGING_TYPE_URI_PART));
	}

	public PackagingTypePage(String brandId) {
		this.brandId = brandId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (brandId == null) {
			selectElement(0);
		} else {
			selectElement(brandListContainer.getIndexForObjectId(brandId));
		}

		initCreateLocationButton();
	}

	protected Table createList() {
		brandTable = new Table();

		brandListQuery = new PackagingTypeListQuery();
		brandListContainer = new LazyLoadingContainer(brandListQuery, 30);
		brandTable.setContainerDataSource(brandListContainer);

		// Column headers
		brandTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		brandTable.setColumnWidth("icon", 22);
		brandTable.addContainerProperty("id", String.class, null);
		brandTable.addContainerProperty("name", String.class, null);
		brandTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a brand
		brandTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = brandTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String brandId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new PackagingTypeDetailPanel(PackagingTypePage.this, brandId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(PackagingTypeNavigator.PACKAGING_TYPE_URI_PART, brandId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(PackagingTypeNavigator.PACKAGING_TYPE_URI_PART));
				}
			}
		});

		return brandTable;
	}

	/**
	 * Call when some brand data has been changed
	 */
	public void notifyLocationChanged(String brandId) {
		// Clear cache
		brandTable.removeAllItems();
		brandListContainer.removeAllItems();

		brandTable.select(brandListContainer.getIndexForObjectId(brandId));
	}

	public void initCreateLocationButton() {
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.PACKAGING_TYPE_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewPackagingTypePopupWindow newPackagingTypePopupWindow = new NewPackagingTypePopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newPackagingTypePopupWindow);
			}
		});
	}

}
