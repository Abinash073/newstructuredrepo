/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ProcessLocationDetailPanel extends DetailPanel{

	private static final long serialVersionUID = 1L;

	protected transient MasterdataService masterdataService;
	protected RLogisticsI18nManager i18nManager;

	protected ProcessLocationPage locationPage;
	protected ProcessLocation location;

	protected boolean editingDetails;
	protected HorizontalLayout locationDetailsLayout;
	protected TextField nameField;
	protected TextField codeField;
	protected TextField parentLocationIdField;

	public ProcessLocationDetailPanel(ProcessLocationPage locationPage, String locationId) {
		this.locationPage = locationPage;
		this.i18nManager = RLogisticsApp.get().getI18nManager();
		this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
		this.location = masterdataService.createProcessLocationQuery().id(locationId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initLocationDetails();

		initActions();
	}

	protected void initActions() {
		locationPage.initCreateLocationButton();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded locationImage = new Embedded(null, Images.USER_50);
		layout.addComponent(locationImage);

		Label locationName = new Label(location.getName() + " " + location.getName());
		locationName.setSizeUndefined();
		locationName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(locationName);
		layout.setComponentAlignment(locationName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(locationName, 1.0f);
	}

	protected void initLocationDetails() {
		Label locationDetailsHeader = new Label(i18nManager.getMessage(Messages.LOCATION_HEADER_DETAILS));
		locationDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		locationDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(locationDetailsHeader);

		// Details: picture and basic info
		locationDetailsLayout = new HorizontalLayout();
		locationDetailsLayout.setSpacing(true);
		locationDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(locationDetailsLayout);

		populateLocationDetails();
	}

	protected void populateLocationDetails() {
		loadLocationDetails();
		initDetailsActions();
	}

	protected void loadLocationDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		locationDetailsLayout.addComponent(detailGrid);

		// Details
		addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_ID), new Label(location.getId())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_NAME),
					new Label(location.getName()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_CODE),
					new Label(location.getCode()));
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_PARENT_LOCATION_ID),
					new Label(location.getParentLocationId()));
		} else {
			nameField = new TextField(null, location.getName() != null ? location.getName() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_NAME), nameField);
			nameField.focus();
			codeField = new TextField(null, location.getCode() != null ? location.getCode() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_CODE), codeField);
			parentLocationIdField = new TextField(null, location.getParentLocationId() != null ? location.getParentLocationId() : "");
			addLocationDetail(detailGrid, i18nManager.getMessage(Messages.LOCATION_PARENT_LOCATION_ID), parentLocationIdField);
		}
	}

	protected void addLocationDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		locationDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				locationDetailsLayout.removeAllComponents();
				populateLocationDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalName = location.getName();

				// Change data
				location.setName(nameField.getValue().toString());
				location.setCode(codeField.getValue().toString());
				String parentLocationIdString = parentLocationIdField.getValue().toString();
				location.setParentLocationId(parentLocationIdString.equals("") ? null : parentLocationIdString);
				masterdataService.saveProcessLocation(location);

				// Refresh detail panel
				editingDetails = false;
				locationDetailsLayout.removeAllComponents();
				populateLocationDetails();

				// Refresh task list (only if name was changed)
				if (nameChanged(originalName)) {
					locationPage.notifyLocationChanged(location.getId());
				}
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		if (originalName != null) {
			nameChanged = !originalName.equals(location.getName());
		} else {
			nameChanged = location.getName() != null;
		}

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.USER_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				RLogisticsConfirmationDialogPopupWindow confirmPopup = new RLogisticsConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.USER_CONFIRM_DELETE, location.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete location from database
						masterdataService.deleteProcessLocation(location.getId());

						// Update ui
						locationPage.refreshSelectNext();
					}
				});

				RLogisticsApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}
}
