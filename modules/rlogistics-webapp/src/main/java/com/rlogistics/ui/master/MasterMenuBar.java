/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;

import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.rlogistics.ui.RLogisticsViewManager;
import com.vaadin.ui.MenuBar;

/**
 * The menu bar which is shown when 'Tasks' is selected in the main menu.
 * 
 * @author Joram Barrez
 * @author Frederik Heremans
 */
public class MasterMenuBar extends MenuBar {

	private static final long serialVersionUID = 1L;

	public static final String ENTRY_ALL_MASTERS = "allMasters";
	public static final String ENTRY_USERS = "users";
	public static final String ENTRY_LOCATIONS = "locations";
	public static final String ENTRY_LOCATION_ROLES = "locationRoles";
	public static final String ENTRY_MASTER_CATEGORIES = "masterCategories";
	public static final String ENTRY_PRODUCT_CATEGORIES = "productCategories";
	public static final String ENTRY_PRODUCT_SUB_CATEGORIES = "productSubCategories";
	public static final String ENTRY_BRANDS = "brands";
	public static final String ENTRY_PACKAGING_TYPES = "packagingTypes";
	public static final String ENTRY_UPLOAD_MASTER_DATA = "uploadMasterData";
	public static final String ENTRY_SERVICE_PROVIDERS = "serviceProviders";
	public static final String ENTRY_SERVICE_PROVIDER_LOCATIONS = "serviceProviderLocations";
	public static final String ENTRY_SERVICE_PROVIDER_LOCATION_BRAND_MAPPINGS = "serviceProviderLocationBrandMappings";
	public static final String ENTRY_PRODUCTS = "products";
	public static final String ENTRY_FAQS = "faqs";
	public static final String ENTRY_RETAILERS = "retailers";
	public static final String ENTRY_RETAILER_FAQS = "retailerFAQs";

	protected RLogisticsViewManager viewManager;
	protected RLogisticsI18nManager i18nManager;

	private MenuItem creationMenuItem;

	public MasterMenuBar() {
		this.viewManager = RLogisticsApp.get().getViewManager();
		this.i18nManager = RLogisticsApp.get().getI18nManager();

		initItems();
		initActions();
	}

	@SuppressWarnings("serial")
	protected void initItems() {
		setWidth("100%");

		MenuBar.MenuItem allMastersItem = addItem(i18nManager.getMessage(Messages.MASTER_MENU_ALL_MASTERS), null);

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_USERS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProcessUserPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_LOCATIONS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProcessLocationPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_LOCATION_ROLES), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProcessLocationRolePage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_MASTER_CATEGORIES), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showMasterCategoryPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_PRODUCT_CATEGORIES), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProductCategoryPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_PRODUCT_SUB_CATEGORIES), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProductSubCategoryPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_BRANDS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showBrandPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_PACKAGING_TYPES), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showPackagingTypePage();
			}
		});
		
		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_SERVICE_PROVIDERS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showServiceProviderPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_SERVICE_PROVIDER_LOCATIONS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showServiceProviderLocationPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_SERVICE_PROVIDER_LOCATION_BRAND_MAPPINGS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showServiceProviderLocationBrandMappingPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_PRODUCTS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showProductPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_FAQS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showFAQPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_RETAILERS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showRetailerPage();
			}
		});

		allMastersItem.addItem(i18nManager.getMessage(Messages.MASTER_MENU_RETAILER_FAQS), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showRetailerFAQPage();
			}
		});

		addItem(i18nManager.getMessage(Messages.MASTER_MENU_UPLOAD_MASTER_DATA), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showUploadMasterDataPopup();
			}
		});
	}

	public void setCreateAction(String caption, MenuBar.Command command){
		if(creationMenuItem != null){
			removeItem(creationMenuItem);
			creationMenuItem = null;
		}
		
		creationMenuItem = addItem(caption, command);
		
		requestRepaint();
	}
	
	@SuppressWarnings("serial")
	protected void initActions() {
	}

}
