package com.rlogistics.ui.master;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.MasterdataService;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class RetailerListQuery extends AbstractLazyLoadingQuery {

	protected transient MasterdataService masterdataService;

	public RetailerListQuery() {
	    this.masterdataService = ((RLogisticsProcessEngineImpl)(ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
	  }

	public int size() {
		return (int) masterdataService.createRetailerQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<Retailer> locations = masterdataService.createRetailerQuery().orderByName().asc().listPage(start, count);

		List<Item> locationListItems = new ArrayList<Item>();
		for (Retailer location : locations) {
			locationListItems.add(new RetailerListItem(location));
		}
		return locationListItems;
	}

	public Item loadSingleResult(String id) {
		return new RetailerListItem(masterdataService.createRetailerQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class RetailerListItem extends PropertysetItem implements Comparable<RetailerListItem> {

		private static final long serialVersionUID = 1L;

		public RetailerListItem(Retailer location) {
			addItemProperty("id", new ObjectProperty<String>(location.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(location.getName(), String.class));
		}

		public int compareTo(RetailerListItem other) {
			// Locations are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
