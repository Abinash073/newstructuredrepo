/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.task;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.activiti.explorer.Messages;

import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.RLogisticsI18nManager;
import com.rlogistics.ui.RLogisticsViewManager;
import com.vaadin.ui.MenuBar;

/**
 * The menu bar which is shown when 'Tasks' is selected in the main menu.
 * 
 * @author Joram Barrez
 * @author Frederik Heremans
 */
public class TaskMenuBar extends MenuBar {

	private static final long serialVersionUID = 1L;

	public static final String ENTRY_TASKS = "tasks";
	public static final String ENTRY_INBOX = "inbox";
	public static final String ENTRY_QUEUED = "queued";
	public static final String ENTRY_INVOLVED = "involved";
	public static final String ENTRY_ARCHIVED = "archived";

	protected transient IdentityService identityService;
	protected RLogisticsViewManager viewManager;
	protected RLogisticsI18nManager i18nManager;

	public TaskMenuBar() {
		this.identityService = ProcessEngines.getDefaultProcessEngine().getIdentityService();
		this.viewManager = RLogisticsApp.get().getViewManager();
		this.i18nManager = RLogisticsApp.get().getI18nManager();

		initItems();
		initActions();
	}

	protected void initItems() {
		setWidth("100%");
		// Inbox
		long inboxCount = new InboxListQuery().size();
		addItem(makeItemTextWithCount(i18nManager.getMessage(Messages.TASK_MENU_INBOX), inboxCount), new Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showInboxPage();
			}
		});

		// Tasks
		long tasksCount = new TasksListQuery().size();
		addItem(makeItemTextWithCount(i18nManager.getMessage(Messages.TASK_MENU_TASKS), tasksCount), new Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showTasksPage();
			}
		});

		// Involved
		long involvedCount = new InvolvedListQuery().size();
		addItem(makeItemTextWithCount(i18nManager.getMessage(Messages.TASK_MENU_INVOLVED), involvedCount), new Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				viewManager.showInvolvedPage();
			}
		});
	}

	private String makeItemTextWithCount(String text, long count) {
		return count > 0 ? text + "(" + count + ")" : text;
	}

	protected void initActions() {
	}
}
