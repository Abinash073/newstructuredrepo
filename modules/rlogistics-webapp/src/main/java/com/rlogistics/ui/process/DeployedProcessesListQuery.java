package com.rlogistics.ui.process;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class DeployedProcessesListQuery extends AbstractLazyLoadingQuery {

	protected transient MetadataService metadataService;
	public DeployedProcessesListQuery() {
	    this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
	  }

	public int size() {
		Map<String,String> currentDsVersions = metadataService.getCurrentDsVersions();
		int numProcesses = 0;
		for(String dataset:currentDsVersions.keySet()){
			numProcesses += (int) metadataService.createProcessDeploymentQuery().dataset(dataset).dsVersion(currentDsVersions.get(dataset)).count();
		}
		return numProcesses;
	}

	public List<Item> loadItems(int start, int count) {
		List<Item> allItems = new ArrayList<Item>();
		
		Map<String,String> currentDsVersions = metadataService.getCurrentDsVersions();
		int numProcesses = 0;
		for(String dataset:currentDsVersions.keySet()){
			List<ProcessDeployment> deployments = metadataService.createProcessDeploymentQuery().dataset(dataset).dsVersion(currentDsVersions.get(dataset)).orderByName().asc().list();
			for(ProcessDeployment deployment:deployments){
				allItems.add(new ProcessDeploymentListItem(deployment));
			}
		}

		allItems.sort(new Comparator<Item>() {
			@Override
			public int compare(Item i1, Item i2) {
				ProcessDeploymentListItem pdi1 = (ProcessDeploymentListItem) i1;
				ProcessDeploymentListItem pdi2 = (ProcessDeploymentListItem) i2;
				
				return pdi1.compareTo(pdi2);
			}
		});
		
		if(start > (allItems.size()-1)){
			return null;
		} else {
			return start+count > allItems.size() ? allItems.subList(start,allItems.size()) : allItems.subList(start, start+count);
		}
	}

	public Item loadSingleResult(String id) {
		Map<String,String> currentDsVersions = metadataService.getCurrentDsVersions();
		int numProcesses = 0;
		for(String dataset:currentDsVersions.keySet()){
			List<ProcessDeployment> deployments = metadataService.createProcessDeploymentQuery().dataset(dataset).dsVersion(currentDsVersions.get(dataset)).orderByName().asc().list();
			for(ProcessDeployment deployment:deployments){
				if(deployment.getId().equals(id)){
					return new ProcessDeploymentListItem(deployment);
				}
			}
		}
		
		return null;
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProcessDeploymentListItem extends PropertysetItem implements Comparable<ProcessDeploymentListItem> {

		private static final long serialVersionUID = 1L;

		public ProcessDeploymentListItem(ProcessDeployment deployment) {
			addItemProperty("id", new ObjectProperty<String>(deployment.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(deployment.getName(), String.class));
		}

		public int compareTo(ProcessDeploymentListItem other) {
			// Users are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
