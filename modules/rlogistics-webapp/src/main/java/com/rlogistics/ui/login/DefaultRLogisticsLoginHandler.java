/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.MetaUser;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.explorer.Constants;

import com.rlogistics.activiti.RLogisticsSpringProcessEngineConfiguration;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.ui.RLogisticsApp;

/**
 * Default login handler, using activiti's {@link IdentityService}.
 * 
 * @author Frederik Heremans
 */
public class DefaultRLogisticsLoginHandler implements RLogisticsLoginHandler {

	public UIUser lookup(String userName){
		ProcessUser processUser = ((RLogisticsSpringProcessEngineConfiguration)ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration()).getMasterdataService().createProcessUserQuery().email(userName).singleResult();
		
		if(processUser != null){
			return new ProcessUserUIUserAdapter(processUser);
		} 

		return null;
	}
	
	public UIUser authenticate(String userName, String password, HttpServletResponse response) throws Exception{
		UIUser user = lookup(userName);
		
		if(user instanceof ProcessUserUIUserAdapter){
			if(((ProcessUserUIUserAdapter)user).getProcessUser().getPassword().equals(AuthUtil.cryptWithMD5(password))) {
				if(((ProcessUserUIUserAdapter)user).getProcessUser().getStatus()==1) {
					return user;
				} else {
					response.setStatus(403);
					throw new Exception("User Deactivated");
				}
			} else {
				response.setStatus(401);
				throw new Exception("Invalid credentials");
			}
		} else {
			response.setStatus(401);
			throw new Exception("Invalid credentials");
		}
	}

	public void onRequestStart(HttpServletRequest request, HttpServletResponse response) {
		if (RLogisticsApp.get().getUIUser() != null && request.getSession(false) != null) {

			request.getSession().setAttribute(Constants.AUTHENTICATED_USER_ID,
					RLogisticsApp.get().getUIUser().getEmail());
		}
	}

	public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) {
		// Noting to do here
	}

	public UIUser authenticate(HttpServletRequest request, HttpServletResponse response) {
		// No automatic authentication is used by default, always through
		// credentials.
		return null;
	}

	public void logout(UIUser userToLogout) {
		// Clear activiti authentication context
		Authentication.setAuthenticatedUserId(null);
	}
}
