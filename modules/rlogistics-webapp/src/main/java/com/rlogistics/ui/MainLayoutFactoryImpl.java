package com.rlogistics.ui;


public class MainLayoutFactoryImpl implements MainLayoutFactory{

	@Override
	public MainLayout createLayout() {
		return new MainLayoutImpl();
	}

}
