/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.ui.master;

import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.rlogistics.ui.RLogisticsApp;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;

/**
 * Page for managing serviceProviders.
 * 
 * @author Joram Barrez
 */
public class ServiceProviderPage extends MasterEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String serviceProviderId;
	protected Table serviceProviderTable;
	protected LazyLoadingQuery serviceProviderListQuery;
	protected LazyLoadingContainer serviceProviderListContainer;

	public ServiceProviderPage() {
		RLogisticsApp.get().setCurrentUriFragment(new UriFragment(ServiceProviderNavigator.SERVICE_PROVIDER_URI_PART));
	}

	public ServiceProviderPage(String serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		if (serviceProviderId == null) {
			selectElement(0);
		} else {
			selectElement(serviceProviderListContainer.getIndexForObjectId(serviceProviderId));
		}

		initCreateLocationMenuItem();
	}

	protected Table createList() {
		serviceProviderTable = new Table();

		serviceProviderListQuery = new ServiceProviderListQuery();
		serviceProviderListContainer = new LazyLoadingContainer(serviceProviderListQuery, 30);
		serviceProviderTable.setContainerDataSource(serviceProviderListContainer);

		// Column headers
		serviceProviderTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		serviceProviderTable.setColumnWidth("icon", 22);
		serviceProviderTable.addContainerProperty("id", String.class, null);
		serviceProviderTable.addContainerProperty("name", String.class, null);
		serviceProviderTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a serviceProvider
		serviceProviderTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = serviceProviderTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String serviceProviderId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ServiceProviderDetailPanel(ServiceProviderPage.this, serviceProviderId));

					// Update URL
					RLogisticsApp.get().setCurrentUriFragment(
							new UriFragment(ServiceProviderNavigator.SERVICE_PROVIDER_URI_PART, serviceProviderId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					RLogisticsApp.get()
							.setCurrentUriFragment(new UriFragment(ServiceProviderNavigator.SERVICE_PROVIDER_URI_PART));
				}
			}
		});

		return serviceProviderTable;
	}

	/**
	 * Call when some serviceProvider data has been changed
	 */
	public void notifyLocationChanged(String serviceProviderId) {
		// Clear cache
		serviceProviderTable.removeAllItems();
		serviceProviderListContainer.removeAllItems();

		serviceProviderTable.select(serviceProviderListContainer.getIndexForObjectId(serviceProviderId));
	}

	public void initCreateLocationMenuItem() {
		setCreateAction(RLogisticsApp.get().getI18nManager().getMessage(Messages.SERVICE_PROVIDER_CREATE), new MenuBar.Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				NewServiceProviderPopupWindow newServiceProviderPopupWindow = new NewServiceProviderPopupWindow();
				RLogisticsApp.get().getViewManager().showPopupWindow(newServiceProviderPopupWindow);
			}
		});		
	}

}
