package com.rlogistics.excel.Reader;


import java.util.ArrayList;
import java.util.List;


public class FilterExcelResult {
    private boolean isSuccess = true;
    List<ExcelErrorData> excelErrorData;
    private String data;


    public FilterExcelResult() {
        excelErrorData = new ArrayList<>();
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public List<ExcelErrorData> getExcelErrorData() {
        return excelErrorData;
    }

    public void setExcelErrorData(ExcelErrorData excelErrorData) {
        this.excelErrorData.add(excelErrorData);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
