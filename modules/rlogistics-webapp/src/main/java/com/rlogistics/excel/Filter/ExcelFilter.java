package com.rlogistics.excel.Filter;

import com.rlogistics.excel.Reader.FilterExcelResult;

/**
 * Filter of Excel based on Business Requirement
 * @author Adarsh
 */
public interface ExcelFilter {


    public static enum FilterPolicy {
        BULK_FLOW_FILTER;

        private FilterPolicy() {

        }
    }
    FilterExcelResult doFilter(FilterPolicy policy) throws IllegalAccessException;

}
