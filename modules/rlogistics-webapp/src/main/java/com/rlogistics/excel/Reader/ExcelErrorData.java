package com.rlogistics.excel.Reader;

import lombok.Data;

@Data
public class ExcelErrorData {
    private int rowNum;
    private int colNum;
    private String errorMsg;
}
