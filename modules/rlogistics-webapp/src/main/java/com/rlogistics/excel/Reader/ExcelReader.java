package com.rlogistics.excel.Reader;

import com.rlogistics.excel.Filter.ExcelFilter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Defines steps to Read an excel sheet
 * @author Adarsh
 */
public interface ExcelReader extends ExcelFilter {

    ExcelReader initExcel(InputStream stream, int sheetIndex);

    ExcelReader readHeader() throws IOException, InvalidFormatException;

    ExcelReader readRows();

    void map();

    List<Map<String, String>> getObject();

}
