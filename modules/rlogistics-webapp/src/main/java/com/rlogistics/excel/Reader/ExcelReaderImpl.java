package com.rlogistics.excel.Reader;

import com.rlogistics.excel.Filter.ExcelFilter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Implementation of Excel Reader with some base level logic
 * @author Adarsh
 */
public class  ExcelReaderImpl implements ExcelReader, ExcelFilter {
    int totalRow;
    DataFormatter formatter;
    LinkedList<String> headers;
    LinkedList<Map<String, String>> excelAsObject;
    private InputStream stream;
    private Workbook workbook;
    private Sheet sheet;
    private int sheetNumber;

    /**
     * Initializes the basic objects to start reading the stream
     * @param stream  Input stream of Excel
     * @param sheetNumber Sheet number to be read
     * @return Class object
     */
    @Override
    public ExcelReader initExcel(InputStream stream, int sheetNumber) {
        this.stream = stream;
        this.sheetNumber = sheetNumber;
        this.headers = new LinkedList<>();
        this.excelAsObject = new LinkedList<>();
        formatter = new DataFormatter();
        return this;
    }

    /**
     * Converts the stream to workbookfactory
     * @return Class Object
     * @throws IOException
     * @throws InvalidFormatException
     */
    @Override
    public ExcelReader readHeader() throws IOException, InvalidFormatException {
        this.workbook = WorkbookFactory.create(stream);
        this.addSheetHeadToList(workbook, sheetNumber);
        return this;

    }

    /**
     * It reads rows using rowIterator
     * @return Class Object
     */
    @Override
    public ExcelReader readRows() {
        Iterator<Row> rows = sheet.rowIterator();
        while (rows.hasNext()) {
            Row eachRow = rows.next();
            if (eachRow.getRowNum() == 0) {
                System.out.println("Ignoring");
            } else {
                excelAsObject.add(addShellToMap(eachRow));
            }
        }
        return this;
    }


    @Override
    public void map() {

    }

    /**
     * Gives the object in a Java collection format
     * @return Excel object in List of map of row value pairs
     */
    @Override
    public List<Map<String, String>> getObject() {
        return excelAsObject;
    }


    /** Reads Sheet head
     * @param workbook Workbook
     * @param sheetNumber sheetNumber
     */
    private void addSheetHeadToList(Workbook workbook, int sheetNumber) {
        sheet = workbook.getSheetAt(sheetNumber);
        totalRow = sheet.getLastRowNum();
        Row firstRow = sheet.getRow(0);
        for (int i = firstRow.getFirstCellNum(); i < firstRow.getLastCellNum(); i++) {
            this.headers.add(firstRow.getCell(i).getStringCellValue().trim());
        }
    }


    private LinkedHashMap<String, String> addShellToMap(Row row) {
        LinkedHashMap<String, String> headerValuePairs = new LinkedHashMap<>();
        try {

            for (int i = 0; i < headers.size(); i++) {
                headerValuePairs.put(headers.get(i), formatter.formatCellValue(row.getCell(i)));
            }
            return headerValuePairs;
        } catch (Exception e) {
            System.out.println(e);
        }
        return headerValuePairs;
    }

    /**
     * @param policy : Filter Policy that needs to be added to filter the excel
     * @return Filtered Object
     * @throws IllegalAccessException
     */
    @Override
    public FilterExcelResult doFilter(FilterPolicy policy) throws IllegalAccessException {
        switch (policy) {
            case BULK_FLOW_FILTER:
                return
                        filterExcelObject(initFiltervars());
        }
        //Todo might cause bugs
        return null;
    }

    /**
     * All the variables that needs to be filtered ! Add new filter variables here
     * @return Map of Filter variable pair
     */
    private Map<String, String> initFiltervars() {
        Map<String, String> b = new HashMap<>();
        b.put("consumerName", "");
        b.put("consumerComplaintNumber", "");
        b.put("addressLine1", "");
        b.put("city", "");
        b.put("pincode", "");
        b.put("natureOfComplaint", "");
        b.put("productCategory", "");

        return b;
        //TODO add more according to requirement
    }

    /**
     * Logic to filter object
     * It checks all the rows value with 2nd row value and if it founds any duplicate then add them into result
     * @param filVars : variables that needs to filtered with all the rows
     * @return  result
     * @throws IllegalAccessException
     */
    //TODO  can be injected : for multiple filter logic create a bean
    //prem comment beacuse it was checking category type with 1st category index

//    private FilterExcelResult filterExcelObject(Map<String, String> filVars) throws IllegalAccessException {
//        if (excelAsObject.isEmpty()) throw new IllegalAccessException("Excel Object Is Not Initialized");
//
//        FilterExcelResult filterExcelResult = new FilterExcelResult();
//        /*
//         First we will take values from first row of excel data and put it in filterObject(filterVars)
//         */
//        filVars.keySet().stream().forEach(s -> filVars.put(s, excelAsObject.get(0).get(s).trim())); //By default first index is base for validating with other rows
//        for (int i = 1; i < excelAsObject.size(); i++) { //for each excelObject :index start from 1 because no need to compare with first row
//            int finalI = i;
//
//            String misMastchedVar =
//                    filVars.keySet().stream()//stream of keys of filterObject
//                            .filter(s -> !filVars.get(s).trim() //comparing  value with excelObject value presented in tht location
//                                    .equalsIgnoreCase(
//                                            excelAsObject
//                                                    .get(finalI)
//                                                    .get(s).trim()
//                                    )
//                            )
//                            .collect(Collectors.joining(","));
//
//            if (!misMastchedVar.isEmpty()) {
//                ExcelErrorData data = new ExcelErrorData();
//                data.setRowNum(i + 1);
//                data.setErrorMsg(misMastchedVar + " " + "column is not matching with first row.");
//                filterExcelResult.setExcelErrorData(data);
//                filterExcelResult.setSuccess(false);
//
//            }
//
//        }
//        return filterExcelResult;
//    }
    
    //prem changes not the check category with first index and message will not show
    private FilterExcelResult filterExcelObject(Map<String, String> filVars) throws IllegalAccessException {
        if (excelAsObject.isEmpty()) throw new IllegalAccessException("Excel Object Is Not Initialized");

        FilterExcelResult filterExcelResult = new FilterExcelResult();
        /*
         First we will take values from first row of excel data and put it in filterObject(filterVars)
         */
        filVars.keySet().stream().forEach(s -> filVars.put(s, excelAsObject.get(0).get(s).trim())); //By default first index is base for validating with other rows
        for (int i = 1; i < excelAsObject.size(); i++) { //for each excelObject :index start from 1 because no need to compare with first row
            int finalI = i;

            String misMastchedVar =
                    filVars.keySet().stream()//stream of keys of filterObject
                            .filter(s -> !filVars.get(s).trim() //comparing  value with excelObject value presented in tht location
                                    .equalsIgnoreCase(
                                            excelAsObject
                                                    .get(finalI)
                                                    .get(s).trim()
                                    )
                            )
                            .collect(Collectors.joining(","));

            if (!misMastchedVar.isEmpty()) {
                ExcelErrorData data = new ExcelErrorData();
                //commented because error will not show if category will mismatch also
//                data.setRowNum(i + 1);
//                data.setErrorMsg(misMastchedVar + " " + "column is not matching with first row.");
                filterExcelResult.setExcelErrorData(data);
                filterExcelResult.setSuccess(false);

            }

        }
        filterExcelResult.setExcelErrorData(null);
        filterExcelResult.setSuccess(true);
        return filterExcelResult;
    }


}
