package com.rlogistics.rest.bulkApiForClient.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TicketNumber {
    @Builder.Default
    private String ticketNumber = "N/A";
}
