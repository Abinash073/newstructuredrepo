package com.rlogistics.rest.controllers;

import com.rlogistics.customqueries.EachFeDistanceDetails;
import com.rlogistics.customqueries.EachFeDistanceWithTotalDistanceData;
import com.rlogistics.customqueries.FeDisatnceCustomQueriesMapper;
import com.rlogistics.customqueries.FeDistanceReportData;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.rest.FeTrackingReportHeader;
import com.rlogistics.rest.RestResult;
import com.rlogistics.rest.repository.FetrackingDetailsRepository;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import com.rlogistics.rest.services.excelWriter.SaveExcelInHostUtil;
import com.rlogistics.rest.services.lambda.GroupingFeData;
import com.rlogistics.rest.services.util.DateUtil;
import com.rlogistics.rest.services.util.DistanceConversionAndCalculationUtil;
import com.rlogistics.rest.services.util.FeDistanceCalculatorUtil;
import com.rlogistics.rest.services.util.GroupedFeDataHeader;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@RestController
@val
public class FeDistanceController {

    private ProcessEngineConfigurationImpl configuration;

    private DateUtil util;

    private DistanceConversionAndCalculationUtil distanceConversionAndCalculationUtil;

    private FetrackingDetailsRepository repository;

    private FeDistanceCalculatorUtil distanceCalculatorUtil;

    private ReportExcelUtil excelUtil;

    private SaveExcelInHostUtil saveExcelInHostUtil;

    private MasterdataService masterdataService;

    private GroupingFeData groupingFeData;

    private ReportExcelUtil groupedExcelUtil;

    private ReportExcelUtil allFeGroupedExcelUtil;

    @Autowired
    public FeDistanceController(ProcessEngineConfigurationImpl configuration,
                                DateUtil util,
                                DistanceConversionAndCalculationUtil distanceConversionAndCalculationUtil,
                                FetrackingDetailsRepository repository,
                                FeDistanceCalculatorUtil distanceCalculatorUtil,
                                @Qualifier("feDistanceReport") ReportExcelUtil excelUtil,
                                SaveExcelInHostUtil saveExcelInHostUtil,
                                MasterdataService masterdataService,
                                GroupingFeData groupingFeData,
                                @Qualifier("groupedFeDataReport") ReportExcelUtil groupedExcelUtil,
                                @Qualifier("eachFeGroupedReport") ReportExcelUtil allFeGroupedExcelUtil) {
        this.configuration = configuration;
        this.util = util;
        this.distanceConversionAndCalculationUtil = distanceConversionAndCalculationUtil;
        this.repository = repository;
        this.distanceCalculatorUtil = distanceCalculatorUtil;
        this.excelUtil = excelUtil;
        this.saveExcelInHostUtil = saveExcelInHostUtil;
        this.masterdataService = masterdataService;
        this.groupingFeData = groupingFeData;
        this.groupedExcelUtil = groupedExcelUtil;
        this.allFeGroupedExcelUtil = allFeGroupedExcelUtil;
    }

    @RequestMapping(value = "/get/fe-distance", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult getDistanceDetailsOfFe(@RequestParam(value = "feName") String feName,
                                              @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {
        RestResult restResult = new RestResult();

        try (SqlSession session = configuration.getSqlSessionFactory().openSession()) {
            EachFeDistanceWithTotalDistanceData totalDistanceData = new EachFeDistanceWithTotalDistanceData();
            List<EachFeDistanceDetails> eachFeDistance = session.getMapper(FeDisatnceCustomQueriesMapper.class)
                    .getEachFeDistance(feName, util.getDateByReducingDate(startDate, 0, "dd-MM-yyyy"),
                            util.getDateByAddingDate(endDate, 1, "dd-MM-yyyy"));

            ArrayList<String> distanceData = eachFeDistance.stream()
                    .filter(e -> e.getTotalDistance().contains("km") || e.getTotalDistance().contains("m"))// filter
                    // if

                    // it
                    // has
                    // "KM"
                    // or
                    // "M"
                    // string
                    // is
                    // thr?
                    .collect(ArrayList::new, (arrayList, e) -> arrayList.add(e.getTotalDistance()), ArrayList::addAll); // Adding
            // them
            // in
            // ArrayList

            totalDistanceData.setDetails(eachFeDistance);
            totalDistanceData.setTotalDistance(
                    distanceConversionAndCalculationUtil.getDistance(distanceData).convertMeterToKm());

            restResult.setSuccess(true);
            restResult.setResponse(totalDistanceData);

        } catch (Exception e) {
            log.debug("Error" + e);
            restResult.setSuccess(false);
        }

        return restResult;
    }

    // @RequestMapping(value = "/get/fe-distance/excel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult getExcelForDistanceOfFe(@RequestParam(value = "feName", required = false, defaultValue = "") String feName,
                                               @RequestParam("startDate") String startDate,
                                               @RequestParam("endDate") String endDate,
                                               @RequestParam("location") String location) {
        RestResult restResult = new RestResult();
        try {
            String loc = masterdataService.createProcessLocationQuery().id(location).singleResult().getName();
            List<FeDistanceReportData> data = repository.getFedistanceDetails(startDate, endDate, loc, feName);
            List<FeDistanceReportData> groupedData = distanceCalculatorUtil.getFeWiseCalcualtedDistance(data);
            FeTrackingReportHeader header = new FeTrackingReportHeader();
            byte[] feReportData = excelUtil.initExcel("Fe_Distance_Data")
                    .setStyling()
                    .createHeader(header.getHeader())
                    .insertData(groupedData, "distance")
                    .saveExcel()
                    .getExcel();

            saveExcelInHostUtil.saveExcel(
                    "get",
                    "excel",
                    "fe-distance",
                    "fe_distance.xlsx",
                    feReportData);
            restResult.setSuccess(true);
            restResult.setResponse("server/attachment/download/get/excel/fe-distance");

        } catch (Exception e) {
            log.debug("Error" + e);
            restResult.setSuccess(false);
            restResult.setMessage("Failed!");
        }
        return restResult;
    }

    @RequestMapping(value = "/get/each-fe/distance", method = RequestMethod.POST)
    private RestResult getEachFeDistance(@RequestParam(value = "feName") String feName,
                                         @RequestParam("startDate") String startDate,
                                         @RequestParam("endDate") String endDate,
                                         @RequestParam("location") String location
    ) {
        try (SqlSession session = configuration.getSqlSessionFactory().openSession()) {
            val restResult = new RestResult();

            GroupedFeDataHeader groupedFeDataHeader = new GroupedFeDataHeader();

            List<EachFeDistanceDetails> eachFeDistance = session.getMapper(FeDisatnceCustomQueriesMapper.class)
                    .getEachFeDistance(feName, util.getDateByReducingDate(startDate, 0, "dd-MM-yyyy"),
                            util.getDateByAddingDate(endDate, 1, "dd-MM-yyyy"));

            if (eachFeDistance.size() == 0) {
                restResult.setMessage("No records found");
                restResult.setResponse("404");
                return restResult;
            }

            Map<String, Map<String, BigDecimal>> data = groupingFeData.groupingFeLocationData(eachFeDistance);

            ArrayList<Map<String, BigDecimal>> listData =
                    data
                            .get(feName)
                            .entrySet()
                            .stream()
                            .collect(ArrayList::new, (m, s) -> m.add(Collections.singletonMap(s.getKey(), s.getValue())), ArrayList::addAll);

            byte[] bytes =
                    groupedExcelUtil
                            .initExcel(feName + "-" + startDate + "--" + endDate)
                            .setStyling()
                            .createHeader(groupedFeDataHeader.getHeader())
                            .insertData(listData, "groupedFeData")
                            .saveExcel()
                            .getExcel();

            saveExcelInHostUtil.saveExcel(
                    "get",
                    "excel",
                    "fe-distance-grouped-new",
                    "fe_distance" + "-" + feName + ".xlsx",
                    bytes);
            restResult.setSuccess(true);
            restResult.setResponse("server/attachment/download/get/excel/fe-distance-grouped-new");
            return restResult;
        } catch (Exception e) {
            log.debug("error" + e);
        }
        return null;

    }

    /**
     * Group of Fe Ticket based on all location
     */
    @RequestMapping(value = "/get/fe-distance/excel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult getData(@RequestParam String location,
                               @RequestParam String startDate,
                               @RequestParam String endDate) {
        RestResult restResult = new RestResult();
        try {

            String locationId = masterdataService.createProcessLocationQuery().name(location).singleResult().getId();

            List<ProcessUser> processUser = masterdataService.createProcessUserQuery().locationId(locationId).roleCode("field_engineer").list();

            Map<String, Map<String, BigDecimal>> data = new HashMap<>();

            processUser
                    .stream()
                    //Querying for each fe :  TODO create a dynamic query by appending all fe
                    .map(p -> getDetailsForEachFe(p.getEmail(), startDate, endDate))
                    .filter(a -> !a.isEmpty())
                    .collect(Collectors.toList())
                    .forEach(map ->
                            data.putAll(map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
                    );
            data.forEach(this::addEachfeDataInOneExcel);

            byte[] excel =
                    allFeGroupedExcelUtil
                            .saveExcel()
                            .getExcel();

            saveExcelInHostUtil
                    .saveExcel(
                            "get",
                            "grouped_data",
                            "all_fe",
                            location + "_fe_data.xlsx",
                            excel
                    );
            restResult.setSuccess(true);
            restResult.setResponse("server/attachment/download/get/grouped_data/all_fe");
            return restResult;


        } catch (Exception e) {
            log.debug("Exception while saving data" + e);
            restResult.setSuccess(false);
            restResult.setResponse("server/attachment/download/get/grouped_data/all_fe");
            return restResult;

        }
    }

    private void addEachfeDataInOneExcel(String feName, Map<String, BigDecimal> data) {
        log.debug("<--------------------------------------------------------------->" + feName);
        GroupedFeDataHeader groupedFeDataHeader = new GroupedFeDataHeader();
        List<Map.Entry<String, BigDecimal>> c = new ArrayList<>(data.entrySet());

        try {

            allFeGroupedExcelUtil
                    .initExcel(feName)
                    .setStyling()
                    .createHeader(groupedFeDataHeader.getHeader())
                    .insertData(c, "groupedFeData");


        } catch (Exception e) {
            log.debug("failed for fe" + feName + " " + e);
        }

    }

    private Map<String, Map<String, BigDecimal>> getDetailsForEachFe(String feName, String startDate, String endDate) {
        try (SqlSession session = configuration.getSqlSessionFactory().openSession()) {

            List<EachFeDistanceDetails> eachFeDistance = session.getMapper(FeDisatnceCustomQueriesMapper.class)
                    .getEachFeDistance(feName, util.getDateByReducingDate(startDate, 0, "dd-MM-yyyy"),
                            util.getDateByAddingDate(endDate, 1, "dd-MM-yyyy"));


            return groupingFeData.groupingFeLocationData(eachFeDistance);


        } catch (Exception e) {
            log.debug("Error" + e);
        }


        return new HashMap<>();
    }
}
