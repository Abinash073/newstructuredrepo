package com.rlogistics.rest;

/*
 * /**
 * Created by prem for Image Links response to retailers
 * @author Bizlog
 *
 *
 */
public class ImagesListResponse {
	private String imageType;
	private String imageUrl;
	private String imageCreation;
//	private String longitude;
//	private String city;;
//	private String latitude;

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageCreation() {
		return imageCreation;
	}

	public void setImageCreation(String imageCreation) {
		this.imageCreation = imageCreation;
	}
}
