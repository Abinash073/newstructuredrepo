package com.rlogistics.rest.services.ticketCreationService;

import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.persistence.entity.AdditionalProductDetailsEntity;
import com.rlogistics.rest.FormFillerUtil;
import com.rlogistics.rest.TicketStatusHistoryClass;
import com.rlogistics.rest.services.ticketCreationService.responseEntity.DropOffTicketCreationData;
import lombok.extern.slf4j.Slf4j;
import pojo.ReportPojo;

import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


/**
 * Creation of Drop off ticket while dropping bulk items
 *
 * @author Adarsh
 */
@Service
@Slf4j
public class DropOffTicketCreationService {

    private MasterdataService masterdataService;

    private MetadataService metadataService;

    private RepositoryService repositoryService;

    private FormService formService;

    private RuntimeService runtimeService;

    @Value("${dropoff.variables}")
    private List<String> dropOffVariables;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String currentDate;

    //To get only those variables that is only required for creating drop off ticket

    public DropOffTicketCreationService(MasterdataService masterdataService,
                                        MetadataService metadataService,
                                        RepositoryService repositoryService,
                                        FormService formService,
                                        RuntimeService runtimeService) {
        this.masterdataService = masterdataService;
        this.metadataService = metadataService;
        this.repositoryService = repositoryService;
        this.formService = formService;
        this.runtimeService = runtimeService;

    }

    /**
     * Creating drop off ticket
     *
     * @param processId    = process Id of master ticket
     * @param retailerName =retailer info
     * @param ids          = Set of all items ID
     */
    public DropOffTicketCreationData createDropOffTicketByProcessId(String processId, String retailerName, Set<String> ids) {
        DropOffTicketCreationData creationData = new DropOffTicketCreationData();
        try {
            //OLD DATA
            Map<String, String> fetchedVariables = runtimeService
                    .getVariables(processId, dropOffVariables)
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> String.valueOf(e.getValue()))); //Converting object to String


//            Map<String, String> addData =
//                    ((Map<String, Object>) entity
//                            .getPersistentState())
//                            .entrySet()
//                            .stream()
//                            .collect(Collectors.toMap(Map.Entry::getKey, e -> String.valueOf(e.getValue())));

            //Adding Old Data and New Data

            //creating multi line string for box details
            String allItemsInBoxes = ids.stream().map(this::getImeiFromId).collect(Collectors.joining("\n"));


            /////////////some extra variables adding manually
            fetchedVariables.put("brand", "BulkBrand");
            fetchedVariables.put("productCategory", "Mobile");
            fetchedVariables.put("productName", "BulkProductName");
            fetchedVariables.put("productCode", "BulkProductCode");
            fetchedVariables.put("dateOfPurchase", currentDate);
            fetchedVariables.put("levelOfIrritation", "1");
            fetchedVariables.put("model", "BulkModel");
            fetchedVariables.put("Model", "BulkModel");
            fetchedVariables.put("allItems", allItemsInBoxes);
            fetchedVariables.put("allItemsCounts", String.valueOf(ids.size()));
            fetchedVariables.put("dateOfComplaint", currentDate);
            fetchedVariables.put("natureOfComplaint", "Drop Off");
            fetchedVariables.put("isUnderWarranty", "No");
            fetchedVariables.put("physicalEvaluation", "No");
            fetchedVariables.put("retailer", retailerName);
            //////////////


            fetchedVariables.forEach((key, value) -> log.debug("Value" + "----" + key + "-----" + value));


            String processName = "it_products";
            String dataset = "BizLog";


            ProcessDeployment processDeployment =
                    metadataService
                            .createProcessDeploymentQuery()
                            .dataset(dataset)
                            .dsVersion(metadataService.getCurrentDsVersion(dataset))
                            .name(processName)
                            .singleResult();


            ProcessDefinition processDefinition =
                    repositoryService
                            .createProcessDefinitionQuery()
                            .deploymentId(processDeployment.getDeploymentId())
                            .singleResult();

            FormFillerUtil fillerUtil
                    = new FormFillerUtil(processDeployment.getDeploymentId(), false);


            ProcessInstance processInstance = (ProcessInstance) fillerUtil
                    .submitForm(
                            (FormFillerUtil
                                    .FormSubmissionAction<ProcessInstance>) data ->
                                    formService.
                                            submitStartFormData(
                                                    processDefinition.getId(),
                                                    fetchedVariables)
                    );

            String dropOffProcessId = processInstance.getProcessInstanceId();

            String dropOffTicketNo = String.valueOf(runtimeService.getVariable(dropOffProcessId, "rlTicketNo"));

            log.debug(dropOffProcessId + "-------" + dropOffTicketNo);
            ///rprem code for saving bulk ticket into MIS 
            new Thread(new Runnable() {
    			@Override
    			public void run() {
    				try {
    					ReportPojo pojo = new ReportPojo(dropOffProcessId);
    					pojo.sentToReportServer(pojo);
    					TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(dropOffProcessId),
    							dropOffProcessId, "TICKET_CREATED");
    				} catch (Exception e) {
    					log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

    				}
    			}
    		}).start();
            ////
            
            creationData.setSuccess(true);
            creationData.setTicketNo(dropOffTicketNo);
            //creationData.setImei(String.valueOf(productDetails.getIdentificationNo()));
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.submit(() -> ids.forEach(this::changeStatusForDropOff));

        } catch (Exception e) {

            creationData.setSuccess(false);
            creationData.setErrorMsg(e.getMessage());

        }

        return creationData;


    }

    /**
     * To change the status in additional product details table
     *
     * @param id id of items
     */
    private void changeStatusForDropOff(String id) {
        AdditionalProductDetails productDetails = masterdataService.createAdditionalProductDetailsQuery().id(id).singleResult();
        AdditionalProductDetailsEntity entity = (AdditionalProductDetailsEntity) productDetails;
        entity.setStatus("drop_off_ticket_created");
        masterdataService.saveAdditionalProductDetails(entity);
    }

    /**
     * To get the imei # from id
     *
     * @param id id of item
     * @return Imei of item
     */
    private String getImeiFromId(String id) {
        try {
            return String.valueOf(masterdataService.createAdditionalProductDetailsQuery().id(id).singleResult().getIdentificationNo());
        } catch (Exception e) {
            return "N/A";
        }
    }


}
