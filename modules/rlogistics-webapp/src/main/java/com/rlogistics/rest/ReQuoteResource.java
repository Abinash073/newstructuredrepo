package com.rlogistics.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ExecutionQuery;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.ProcessResult;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class ReQuoteResource {
	private static Logger log = LoggerFactory.getLogger(PaymentResource.class);

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/retailer/requote-value", produces = "application/json")
	public Map counts(@RequestParam(value = "rlProcessId") String processId) {
		Map<String, Object> map = new HashMap<>();
		/**
		 * this is commented because Recycle device npt need this api to call in
		 * mobile end so not changing any this in mobile return false so that
		 * requote will happen
		 */
		try {
			if (RetailerApiCalls.callRecyclerDeviceAPI(processId)) {
				map.put("success", true);

			} else {
				map.put("success", false);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("success", null);

		}
		return map;

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/retailer/buyback-requote-value", produces = "application/json")
	public RestResponse retailerReQuote(@RequestParam(value = "ticketNo", required = true) String ticketNo,
			@RequestParam(value = "retailerId", required = true) String retailerId,
			@RequestParam(value = "reQuotePrice", required = true) String reQuotePrice) {

		RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "Re-Quote Price: " + reQuotePrice);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestResponse restResponse = new RestResponse();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {

			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();

			String processId = "";
			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", ticketNo);
			if (retailer.getCode().equals(ticketNo.split("-")[0])) {
				if (processResults.size() > 0) {
					try {
						processId = processResults.get(0).getProcessId();
						log.debug("Process id:" + processId + " Reqoute price:" + reQuotePrice + " Ticket No:"
								+ ticketNo);
						if (runtimeService.getVariable(processId, "rlPickUpConfirmation").toString().equals("true")) {
							restResponse.setResponse(null);
							restResponse.setMessage(
									"Ticket:" + ticketNo + " is closed form FE end,Price value cannot be update");
							restResponse.setSuccess(false);
						} else {
							TaskResource taskResource = new TaskResource();
							RestResult restResult = taskResource.updateProductCost(processId, reQuotePrice);
							if (restResult.getResponse().toString().equals("success")) {
								restResponse.setResponse(restResult.getResponse());
								restResponse.setMessage("Price updated for Ticket Number :" + ticketNo);
								restResponse.setSuccess(true);
								runtimeService.setVariable(processId, "rlValueOffered", reQuotePrice);
								runtimeService.setVariable(processId, "isCostUpdated", "yes");
							} else {
								restResponse.setResponse(null);
								restResponse.setMessage("Price not updated for Ticket Number :" + ticketNo);
								restResponse.setSuccess(false);
							}

						}

					} catch (Exception exception) {
						restResponse.setResponse(null);
						restResponse.setMessage("Price not updated for Ticket Number :" + ticketNo);
						restResponse.setSuccess(false);
					}

				} else {
					restResponse.setResponse(null);
					restResponse.setMessage("Ticket Number: " + ticketNo + " is not present in System.");
					restResponse.setSuccess(false);
				}
			} else {
				restResponse.setResponse(null);
				restResponse.setMessage("Ticket Number: " + ticketNo + " is not for this Retailer");
				restResponse.setSuccess(false);
			}

		} catch (Exception e) {
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);

		}

		return restResponse;

	}
}
