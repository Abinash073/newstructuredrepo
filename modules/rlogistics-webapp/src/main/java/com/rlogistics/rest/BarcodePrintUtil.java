package com.rlogistics.rest;

import java.io.*;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;

public class BarcodePrintUtil
{
     public static void main (String argv[]) throws Exception
	 {
    	 String toPrint = "123456789";
    	 InputStream is = new ByteArrayInputStream(toPrint.getBytes());
		 FileInputStream textStream;
	//	 textStream = "1234567";
		
		 DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
		 Doc mydoc = new SimpleDoc(is, flavor, null);
		
		 	PrintService[] services = PrintServiceLookup.lookupPrintServices(
		         flavor, null);
		    PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
		
		    
		        if(defaultService == null) {
		              //no printer found
		
		        } 
		      
		
		       //built in UI for printing you may not use this
		       PrintService service = ServiceUI.printDialog(null, 200, 200, services, defaultService, flavor, null);
		
		
		        if (service != null)
		         {
		            DocPrintJob job = service.createPrintJob();
		            job.print(mydoc, null);
		         }
		
		     }
}