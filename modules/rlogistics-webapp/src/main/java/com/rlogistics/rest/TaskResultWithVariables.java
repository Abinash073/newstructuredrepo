package com.rlogistics.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.task.Task;

public class TaskResultWithVariables extends TaskResult {

		private Map<String,Object> variables = new HashMap<>();

		public TaskResultWithVariables(Task task,Map<String,Object> variables) {			
			super(task);
			
			for(String variable:variables.keySet()){
				Object value = variables.get(variable);
				
				if(value instanceof Date){
					value = ((Date)value).getTime();
				}
				
				this.getVariables().put(variable, value);
			}
		}

		public Map<String,Object> getVariables() {
			return variables;
		}

		public void setVariables(Map<String,Object> variables) {
			this.variables = variables;
		}
}
