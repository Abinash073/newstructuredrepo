package com.rlogistics.rest.Component;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.rlogistics.customqueries.LastDistanceDetails;
import com.rlogistics.rest.constants.LocationWiseLatLong;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;


/**
 * Component for Google distance calculation logic
 * Can be changed with other providers
 * TODO - Create a interface so that it can be injected later using different provider or create different profile
 *
 * @author Adarsh
 */
@Slf4j
@Component(value = "googleApiDistanceMatrix")
@DependsOn(value = "locationWiseLatLongDetails")
public class DistanceBetweenTwoLatLong {

    private LocationWiseLatLong locationWiseLatLong;
    @Value(value = "${google.maps.api.key}")
    private String API_KEY;
    private GeoApiContext context;

    @Autowired
    public DistanceBetweenTwoLatLong(LocationWiseLatLong locationWiseLatLong) {
        this.locationWiseLatLong = locationWiseLatLong;
    }

    @PostConstruct
    void init() {
        context = new GeoApiContext.Builder().apiKey(API_KEY).build();
    }

    /**
     * This method will call to google api to fetch the Distance Matrix Result
     *
     * @param departure Departing / Initial Latlong entity
     * @param arrivals  Arriving Latlong Entity
     * @return Distance Matrix
     */
    private synchronized DistanceMatrix estimateRouteTime(LatLng departure, LatLng arrivals) {
        try {
            //log.debug(API_KEY);
            DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(context);

            return req.origins(departure)
                    .destinations(arrivals)
                    .mode(TravelMode.DRIVING)
                    .language("en-US")
                    .await();

        } catch (Exception e) {
            log.debug("Error" + e);
        }
        return null;
    }

    /**
     * Checks if new last Lang long details is there or not .if its not there it adds last lat long details
     * from a specific hub location address and then calculate the distance between last lat long and new lat long
     * and then returns it.
     *
     * @param lastDetails Last Lat long of FE
     * @param newDetails  New Lat Long of Fe
     * @return Distance between Lat long in string
     */
    public String getUpdatedTotalDistance(LastDistanceDetails lastDetails, Map<String, String> newDetails) {
        try {

            LatLng newLatLng = new LatLng(Double.parseDouble(newDetails.get("latitude")), Double.parseDouble(newDetails.get("longitude")));
            DistanceMatrix matrix;
            if (lastDetails == null) {

                matrix = this.estimateRouteTime(locationWiseLatLong.getLatLongDetails(newDetails.get("city")), newLatLng);

            } else {
                matrix = this.estimateRouteTime(new LatLng(
                                Double.parseDouble(lastDetails.getLatitude()),
                                Double.parseDouble(lastDetails.getLongitude()
                                )
                        ), newLatLng
                );


            }
            return matrix != null ? matrix.rows[0].elements[0].distance.humanReadable : "N/A";

        } catch (Exception ex) {
            log.debug("Error" + ex);
            //throw new RuntimeException("SomeErrorOccurred");
            return "N/A";
        }


    }

}
