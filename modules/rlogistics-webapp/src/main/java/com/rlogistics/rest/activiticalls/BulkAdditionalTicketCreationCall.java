package com.rlogistics.rest.activiticalls;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.TicketStatusHistoryClass;
import com.rlogistics.rest.services.TicketCreationService;
import lombok.extern.slf4j.Slf4j;
import pojo.ReportPojo;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides {@code static} utility methods to help bulk flow
 * 
 * @author Adarsh
 */
@Slf4j
@SuppressWarnings("unused")
public class BulkAdditionalTicketCreationCall {

	/**
	 * Creates Sub ticket for bulk fow and assign it to specific field engineer
	 * 
	 * @param feName
	 *            Fe Name whom ticket needs to be assigned
	 * @param processId
	 *            Process Id of main ticket
	 * @param retailerId
	 *            Retailer Id
	 * @return Ticket No of Sub ticket
	 */
	// @RequestMapping(value = "/test/add/subticket", method =
	// RequestMethod.POST
	public static String initBulkSubTicket(@RequestParam(value = "feName") String feName,
			@RequestParam(value = "processId") String processId,
			@RequestParam(value = "retailerId") String retailerId) {
		TicketCreationService ticketCreationService = new TicketCreationService();

		log.debug("Calling sub ticket creation process for bulk");

		RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();

		List<Map<String, String>> list = new ArrayList<>();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processId)
				.includeProcessVariables().singleResult();

		Map<String, Object> var = processInstance.getProcessVariables();
		Map<String, String> convertedVar = var.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, e -> String.valueOf(e.getValue())));
		// Temp. storing local var
		String tempMainTicket = String.valueOf(convertedVar.get("rlTicketNo"));
		// Removing duplicate variables
		convertedVar.remove("rlTicketNo");

		convertedVar.put("rlMainTicketNumber", tempMainTicket);
		convertedVar.put("rlSelectedFieldEngineer", feName);
		list.add(convertedVar);
		ProcessInstance p2 = ticketCreationService.createTicket(retailerId, list, "bulk_sub");
		log.debug(p2.getProcessDefinitionId() + " " + p2.getProcessDefinitionKey() + " " + p2.getId());
		String processIdOfSubTicket = p2.getProcessInstanceId();
		log.debug(processIdOfSubTicket);

		// Setting ticket No
		String subTicketNo = createTicketNo(retailerId, convertedVar.get("productCategory"),
				convertedVar.get("pincode"));
		runtimeService.setVariable(processIdOfSubTicket, "rlTicketNo", subTicketNo);
		runtimeService.setVariable(processIdOfSubTicket, "mainTicketProcessId", processId);

		// Setting Sub-ticket Names and assignee of them
		createSubTicketAndFeString(feName, subTicketNo, processId);

		return String.valueOf(runtimeService.getVariable(processIdOfSubTicket, "rlTicketNo"));

	}

	/**
	 * Creates Ticket No from below parameters
	 * 
	 * @param retailerId
	 *            Retailer Id
	 * @param productCat
	 *            Product Category
	 * @param pincode
	 *            Pincode
	 * @return Ticket No
	 */
	public static String createTicketNo(String retailerId, String productCat, String pincode) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) ProcessEngines.getDefaultProcessEngine())
				.getMasterdataService();
		log.debug(retailerId, productCat, pincode);

		StringBuffer buffer = new StringBuffer();

		String rCode = masterdataService.createRetailerQuery().id(retailerId).singleResult().getCode();
		String pCode = masterdataService.createProductCategoryQuery().name(productCat).singleResult().getCode();

		String lCode = masterdataService.createProcessLocationQuery()
				.id(masterdataService.createLocationPincodesQuery().pincode(pincode).singleResult().getLocationId())
				.singleResult().getCode();

		return buffer.append(rCode).append("-").append(pCode).append("-").append(lCode).append("-")
				.append(RandomStringUtils.random(5, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ")).toString();

	}

	/**
	 * Creates a Activiti variable to store all the subticket name and Fe name
	 * belongs to that user
	 * 
	 * @param feEmail
	 *            Email of Fe
	 * @param ticketNo
	 *            Ticket No
	 * @param processId
	 *            Process Id
	 */
	public static void createSubTicketAndFeString(String feEmail, String ticketNo, String processId) {
		log.debug(feEmail + " " + ticketNo + " " + processId);
		String val = "";
		RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
		String a = String.valueOf(runtimeService.getVariable(processId, "rlSubTicketFeMap"));
		if (a.equals("null") || a.equals("") || a.isEmpty()) {
			val = feEmail + " : " + ticketNo;
		} else {
			val = a + "\n" + feEmail + " : " + ticketNo;
		}
		runtimeService.setVariable(processId, "rlSubTicketFeMap", val);
	}

}
