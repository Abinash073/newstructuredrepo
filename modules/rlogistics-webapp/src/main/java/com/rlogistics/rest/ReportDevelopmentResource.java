package com.rlogistics.rest;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.activiti.RLogisticsSpringProcessEngineConfiguration;
import com.rlogistics.reports.ReportTableCreatingVisitor;
import com.rlogistics.reports.ReportTableSetMetaVisitor;

@RestController
public class ReportDevelopmentResource {
	static Logger log = LoggerFactory.getLogger(ReportDevelopmentResource.class);

	@RequestMapping(value = "/reports/configure-report-tables", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = "application/json")
	public void configureReportTables() {

		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		commandExecutor.execute(new Command<Object>() {
			@Override
			public Object execute(CommandContext commandContext) {
				MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getMetadataService();

				Map<String, String> dsVersionByName = metadataService.getCurrentDsVersions();

				for (String dataset : dsVersionByName.keySet()) {
					List<ProcessDeployment> processDeployments = metadataService.createProcessDeploymentQuery().dataset(dataset)
							.dsVersion(dsVersionByName.get(dataset)).list();

					for (ProcessDeployment processDeployment : processDeployments) {
						new ReportTableSetMetaVisitor(processDeployment,new ReportTableCreatingVisitor(processDeployment)).visitTables();
					}
				}
				return null;
			}
		});

	}
}
