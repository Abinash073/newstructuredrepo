package com.rlogistics.rest.services.helper;

import com.rlogistics.rest.AdditionalProductEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BoxNumberWithCount {
    private List<AdditionalProductEntity> productDetails;
    private String boxNumber;

    int count = 0;
    public BoxNumberWithCount() {
        this.productDetails = new ArrayList<>();
    }

}