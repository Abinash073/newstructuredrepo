package com.rlogistics.rest.bulkApiForClient.customValidators.anotation;

import com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor.BizlogserviceTypeAnotationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BizlogserviceTypeAnotationValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface BizlogServiceType {
    String message() default "Not a valid Bizlog Service Type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
