package com.rlogistics.rest.franchise;

import com.rlogistics.activiti.RLogisticsSpringProcessEngineConfiguration;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.ui.RLogisticsApp;
import com.rlogistics.ui.login.ProcessUserUIUserAdapter;
import com.rlogistics.ui.login.RLogisticsLoginHandler;
import com.rlogistics.ui.login.UIUser;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.explorer.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FranchiseLoginHandler implements RLogisticsLoginHandler {

    public UIUser lookup(String userName){
        ProcessUser processUser = ((RLogisticsSpringProcessEngineConfiguration) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration()).getMasterdataService().createProcessUserQuery().email(userName).singleResult();

        if(processUser != null){
            return new ProcessUserUIUserAdapter(processUser);
        }

        return null;
    }
    @Override
    public UIUser authenticate(String userName, String password, HttpServletResponse response) throws Exception {
        UIUser user = lookup(userName);

        if(user instanceof ProcessUserUIUserAdapter){
            if(((ProcessUserUIUserAdapter)user).getProcessUser().getPassword().equals(AuthUtil.cryptWithMD5(password))) {
                if(((ProcessUserUIUserAdapter)user).getProcessUser().getStatus()==1) {
                    return user;
                } else {
                    response.setStatus(403);
                    throw new Exception("User Deactivated");
                }
            } else {
                response.setStatus(401);
                throw new Exception("Invalid credentials");
            }
        } else {
            response.setStatus(401);
            throw new Exception("Invalid credentials");
        }
    }

    @Override
    public UIUser authenticate(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }

    @Override
    public void logout(UIUser userTologout) {

        Authentication.setAuthenticatedUserId(null);
    }

    @Override
    public void onRequestStart(HttpServletRequest request, HttpServletResponse response) {

        if (RLogisticsApp.get().getUIUser() != null && request.getSession(false) != null) {

            request.getSession().setAttribute(Constants.AUTHENTICATED_USER_ID,
                    RLogisticsApp.get().getUIUser().getEmail());
        }
    }

    @Override
    public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) {

    }
}
