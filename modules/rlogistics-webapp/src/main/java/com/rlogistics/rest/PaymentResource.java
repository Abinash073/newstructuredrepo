package com.rlogistics.rest;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLDecoder;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.batik.dom.util.HashTable;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailSendCommand;
import com.rlogistics.master.identity.CostingActivity;
import com.rlogistics.master.identity.DeviceInfo;
import com.rlogistics.master.identity.DeviceInfo.DeviceInfoQuery;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.Payment;
import com.rlogistics.master.identity.Payment.PaymentQuery;
import com.rlogistics.master.identity.PaymentAcknowledgement;
import com.rlogistics.master.identity.RetailerCostingActivity;
import com.rlogistics.master.identity.RetailerPackingCost;
import com.rlogistics.master.identity.ServiceMaster;
import com.rlogistics.master.identity.TicketCosting;
import com.rlogistics.master.processor.MasterdataServiceUtil;
import com.rlogistics.sms.SMSUtil;
import com.rlogistics.util.AttachmentUtil;

@RestController
public class PaymentResource {
	/*

	private static Logger log = LoggerFactory.getLogger(PaymentResource.class);
	private String merchantKey = "FCyqqZ"; // test credentials
	private String salt = "sfBpGA8E"; // test credentials
	private String paymentUrl = "https://test.payu.in/_payment"; // test
																	// credentials
	public static java.util.Hashtable<String, String> costingDetails = new java.util.Hashtable<String, String>();

	@RequestMapping(value = "/payment/initiate", method = RequestMethod.POST)
	public RestResponse addPaymentInfo(@RequestParam("payment") String queryJSON, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Map<String, String> paymentObj = null;
		String APP_LINK = "http://184.171.164.146/dev/#/make/payment/";
		try {

			paymentObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);

			Payment payment = masterdataService.newPayment();
			UUID id = UUID.randomUUID();
			payment.setId(String.valueOf(id));
			payment.setFirstName(paymentObj.get("firstName"));
			payment.setEmail(paymentObj.get("email"));
			payment.setPhone(paymentObj.get("phone"));
			payment.setAmount(Float.valueOf(String.valueOf(paymentObj.get("amount"))));
			payment.setProductInfo(paymentObj.get("productInfo"));

			// generate txnId
			Random rand = new Random();
			String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
			String txnId = hashCal("SHA-256", randomString).substring(0, 20);

			float amount = Float.valueOf(String.valueOf(paymentObj.get("amount")));

			// generate Hash
			String hash = hashCal("SHA-512",
					merchantKey + "|" + txnId + "|" + amount + "|" + paymentObj.get("productInfo") + "|"
							+ paymentObj.get("firstName") + "|" + paymentObj.get("email") + "|||||||||||" + salt);

			payment.setTxnId(txnId);
			payment.setHash(hash);

			try {
				masterdataService.savePayment(payment);
				String paymentId = payment.getId();
				APP_LINK = APP_LINK + paymentId;
				restResponse.setResponse(APP_LINK);
				restResponse.setMessage("Payment Info saved");
			} catch (Exception i) {

			}

		} catch (Exception e) {

		}

		return restResponse;
	}

	public String hashCal(String type, String str) {
		byte[] hashSequence = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashSequence);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append("0");
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException NSAE) {

		}
		return hexString.toString();
	}

	@RequestMapping(value = "/payment/request", method = RequestMethod.POST)
	public RestResponse requestPayment(@RequestParam("processId") String processId,
			@RequestParam("loggedEmail") String deliveryBoyEmail, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		JSONObject paymentObj = new JSONObject();
		String merchantId = "FPTEST"; // test value
		// String merchantId = "A469";
		String api_key = "08Z1782051U62BY9OUGW4XM67GF2004"; // test value
		// String api_key = "5C546E02213T406H0Y19I5L45W3525A";

		try {
			Map<String, Object> variables = runtimeService.getVariables(processId);
			String email = String.valueOf(variables.get("emailId"));
			String phone = String.valueOf(variables.get("telephoneNumber"));

			// calculate amount
			float amount = calculateAmount(variables);

			// just for testing should be removed in ideal scenario
			if (amount == 0f) {
				amount = 1f;
			}

			System.out.println("Before setting the values");
			paymentObj.put("id", merchantId);
			paymentObj.put("merchant_id", merchantId);
			paymentObj.put("valid_for_mins", 10);
			paymentObj.put("consumer_mobile", phone);
			paymentObj.put("consumer_email", email);
			paymentObj.put("amount", amount);

			Random rand = new Random();
			String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
			String invoice_id = hashCal("SHA-256", randomString).substring(0, 10);

			System.out.println("Before generating hash");
			String hashInput = api_key + "#" + merchantId + "#" + merchantId + "#" + invoice_id + "#";
			String hash = generateHashForMessage(hashInput);

			System.out.println("After computing hash");
			paymentObj.put("invoice_id", invoice_id);
			paymentObj.put("sign", hash);

			log.debug("Invoice Id :- " + invoice_id);
			log.debug("Signature Hash :- " + hash);
			System.out.println("Invoice Id :- " + invoice_id);
			System.out.println("Signature Hash :- " + hash);

			// API call
			CloseableHttpClient httpclient = HttpClients.createDefault();

			String apiUrl = "https://test.fonepaisa.com/portal/payment/request"; // test
			// value
			// String apiUrl =
			// "https://secure.fonepaisa.com/portal/payment/request";
			HttpPost httppost = new HttpPost(apiUrl);
			httppost.addHeader("Content-Type", "application/json");

			System.out.println("\n Payment Request :" + paymentObj);
			// Set HTTP entity
			httppost.setEntity(new StringEntity(paymentObj.toString(), "UTF8"));

			// Get Response
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("Response from server :" + rawResponse);

			Map<String, String> paymentRes = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);

			restResponse.setResponse(paymentRes);

			// send sms
			String message = "Please go to this link and complete your payment " + paymentRes.get("payment_url");
			try {
				SMSUtil.sendSMSSingle(message, phone);
			} catch (Exception s) {

			}

			// insert into payment table
			Payment payment = masterdataService.newPayment();
			payment.setId(invoice_id);
			payment.setTxnId(paymentRes.get("payment_reference"));
			payment.setHash(hash);
			payment.setAmount(amount);
			payment.setProcessId(processId);
			payment.setPaymentRequest(paymentObj.toString());
			payment.setPaymentType("collectFrom");

			// set Delivery Boy Email (Assignee)
			payment.setEmail(deliveryBoyEmail);
			payment.setStatus(paymentRes.get("status"));

			try {
				System.out.println("Before Adding payment info");
				masterdataService.savePayment(payment);
			} catch (Exception p) {

			}

			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				restResponse.setSuccess(true);
			} else {
				restResponse.setSuccess(false);
				response.setStatus(httpResponse.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
			System.out.println("Exception occured " + e.getMessage());
			restResponse.setSuccess(false);
			restResponse.setMessage("Error in processing");
			response.setStatus(500);
		}

		return restResponse;
	}

	private static float calculateAmount(Map<String, Object> variables) {
		float amount = 0f;

		// if (variables.containsKey("rlServiceChargesAssigned")) {
		// String val =
		// String.valueOf(variables.get("rlServiceChargesAssigned"));
		// if (Float.valueOf(val) > 0f) {
		// amount = amount + Float.valueOf(val);
		// }
		// }
		// if (variables.containsKey("rlDataTransferCharges")) {
		// String val = String.valueOf(variables.get("rlDataTransferCharges"));
		// if (Float.valueOf(val) > 0f) {
		// amount = amount + Float.valueOf(val);
		// }
		// }
		// if (variables.containsKey("rlPackagingCharges")) {
		// String val = String.valueOf(variables.get("rlPackagingCharges"));
		// if (Float.valueOf(val) > 0f) {
		// amount = amount + Float.valueOf(val);
		// }
		// }

		if (variables.containsKey("amtPaidToCustomer")) {
			String val = String.valueOf(variables.get("amtPaidToCustomer"));
			if (Float.valueOf(val) > 0f) {
				amount = amount + Float.valueOf(val);
			}
		}

		// More costs will be added and should bee calculated in the same way

		return amount;
	}

	
	@RequestMapping(value = "/pay-to/customer", method = RequestMethod.POST)
	public RestResponse payToCustomer(@RequestParam("processId") String processId, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		JSONObject paymentObj = new JSONObject();
		// String merchantId = "FPTEST"; //test value
		// String merchantId = "A469";
		String merchantId = "FPAP";// new Merchant ID
		String api_key = "08Z1782051U62BY9OUGW4XM67GF2004"; // test value
		// String api_key = "5C546E02213T406H0Y19I5L45W3525A";
		try {
			Map<String, Object> variables = runtimeService.getVariables(processId);
			String acc_HolderName = String.valueOf(variables.get("accountHolderName"));
			String acc_no = String.valueOf(variables.get("accountNo"));
			String ifsc = String.valueOf(variables.get("ifscCode"));

			// calculate amount
			float amount = calculateAmount(variables);

			// just for testing should be removed in ideal scenario
			if (amount == 0f) {
				amount = 1f;
			}

			System.out.println("Before setting the values");
			paymentObj.put("id", merchantId);
			paymentObj.put("merchant_id", merchantId);

			// Random rand = new Random();
			String randomString = processId + (System.currentTimeMillis() / 1000L);
			String request_id = hashCal("SHA-256", randomString).substring(0, 12);

			System.out.println("Before generating hash");
			String hashInput = api_key + "#" + merchantId + "#" + merchantId + "#" + request_id + "#";
			log.debug("hashInput :" + hashInput);
			String hash = generateHashForMessage(hashInput);

			System.out.println("After computing hash");
			paymentObj.put("request_id", request_id);
			paymentObj.put("sign", hash);

			JSONArray requests = new JSONArray();
			JSONObject singleRequestObj = new JSONObject();
			singleRequestObj.put("request_id",
					hashCal("SHA-256", processId + (System.currentTimeMillis() / 1000L)).substring(0, 12));
			// Need to Get The Detail From Process End Instead Of HardCoding.
			singleRequestObj.put("acct_name", acc_HolderName);
			// singleRequestObj.put("acct_number", "123786781234");
			singleRequestObj.put("acct_number", acc_no);
			singleRequestObj.put("ifsc_code", ifsc);
			singleRequestObj.put("transfer_type", "I");
			singleRequestObj.put("amount", amount);
			requests.put(singleRequestObj);

			paymentObj.put("requests", requests);

			log.debug("Request Id :- " + request_id);
			log.debug("Signature Hash :- " + hash);
			System.out.println("Request Id :- " + request_id);
			System.out.println("Signature Hash :- " + hash);

			// API call
			CloseableHttpClient httpclient = HttpClients.createDefault();

			String apiUrl = "https://test.fonepaisa.com/portal/funds/transfer"; // test
			// value

			// String apiUrl =
			// "https://secure.fonepaisa.com/portal/funds/transfer";
			HttpPost httppost = new HttpPost(apiUrl);
			httppost.addHeader("Content-Type", "application/json");

			System.out.println("\n Payment Request :" + paymentObj);
			log.debug("\n Payment Request :" + paymentObj);
			// Set HTTP entity
			httppost.setEntity(new StringEntity(paymentObj.toString(), "UTF8"));

			// Get Response
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			log.debug("Response from server :" + rawResponse);
			log.debug("fonePaisa URL : " + apiUrl);
			System.out.println("Response from server :" + rawResponse);

			Map<String, Object> paymentRes = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);

			restResponse.setResponse(paymentRes);

			

			// insert into payment table
			Payment payment = masterdataService.newPayment();
			payment.setId(request_id);
			payment.setHash(hash);
			payment.setAmount(amount);
			payment.setProcessId(processId);
			payment.setPaymentType("payTo");
			payment.setPaymentRequest(paymentObj.toString());
			payment.setPaymentResponse(paymentRes.toString());
			payment.setRequestId(request_id);
//			payment.setTxnDate();

			ArrayList<Map<String, String>> responseRequests = (ArrayList<Map<String, String>>) paymentRes
					.get("requests");
			Map<String, String> singleResObj = responseRequests.get(0);
			String statusCode = singleResObj.get("status");
			if (statusCode.equalsIgnoreCase("I")) {
				statusCode = "Pending";
			} else {
				statusCode = "Failed";
			}
			payment.setStatus(statusCode);

			try {
				System.out.println("Before Adding payment info");
				log.debug("Before Adding Payment Info");
				masterdataService.savePayment(payment);
				System.out.println("After Adding payment info");
				log.debug("After adding Payment Info");
			} catch (Exception e) {
				System.out.println("Error in Saving Payment Info : " + e);
				log.debug("Error in Saving Payment Info : " + e);
			}

			if (httpResponse.getStatusLine().getStatusCode() == 200) {

				restResponse.setSuccess(true);
				restResponse.setMessage("Your Payment is Initiated.Check status in few minutes.");
			} else {
				restResponse.setSuccess(false);
				response.setStatus(httpResponse.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
			System.out.println("Exception occured " + e.getMessage());
			restResponse.setSuccess(false);
			restResponse.setMessage("Error in processing");
			response.setStatus(500);
		}
		return restResponse;
	}

	@RequestMapping(value = "/insert/costing", produces = "application/json")
	public void insertActivityCost(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("processId") String processId, @RequestParam("activityCode") String activityCode,
			@RequestParam("actor") String actor, @RequestParam("transactionType") String transactionType) {
		AttachmentUploadResult result = new AttachmentUploadResult();
		// if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}

		insertActivityCost(processId, activityCode, actor, transactionType);
	}

	public static void insertActivityCost(String processId, String activityCode, String actor, String transactionType) {
		float amount = 0f;

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		MasterdataService masterdataService2 = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		MasterdataService masterdataService3 = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(processId);
		String natureOfComplaint = String.valueOf(variables.get("natureOfComplaint"));
		String retailerId = String.valueOf(variables.get("rlRetailerId"));
		String categoryId = String.valueOf(variables.get("rlProductCategoryId"));
		String serviceId = String.valueOf(variables.get("rlServiceId"));
		log.debug("Process Variable : " + variables);
		double materialCost = 0;
		double packingCost = 0;
		double hopmCost = 0;

		if (costingDetails.size() == 0) {
			// call create hashtable function
			generateHashTableForCost();
			log.debug("Calling generateHashTableForCost");
		}
		try {
			if (activityCode.equals("HOPM")) {
				List<String> packingList = new ArrayList<String>();
				List<PackagingMaterialInventory> pacList = masterdataService1.createPackagingMaterialInventoryQuery()
						.ticketNumber(String.valueOf(variables.get("rlTicketNo"))).list();
				log.debug("pacList : " + pacList);
				if (!(pacList.isEmpty())) {
					for (PackagingMaterialInventory plist : pacList) {
						String packCode = plist.getPackagingTypeCode();
						PackagingType ptype = masterdataService2.createPackagingTypeQuery().code(packCode)
								.singleResult();
						log.debug("ptype : " + ptype);
						String packId = ptype.getId();
						RetailerPackingCost retailerPackCost = masterdataService3.createRetailerPackingCostQuery()
								.packingMaterialId(packId).retailerId(retailerId).singleResult();
						log.debug("retailerPackCost : " + retailerPackCost);
						materialCost = retailerPackCost.getMaterialCost();
						log.debug("retailerPackCost.getMaterialCost() : " + retailerPackCost.getMaterialCost());
						packingCost = retailerPackCost.getPackingCost();
						log.debug("retailerPackCost.getPackingCost() : " + retailerPackCost.getPackingCost());
						hopmCost = hopmCost + materialCost + packingCost;
						packingList.add(plist.getPackagingTypeCode());
						log.debug("packingList : " + packingList.get(0));
					}
				}
			}
		} catch (Exception e) {
			log.debug("Error in Gettig HOPM Cost : " + e);
		}
		// Creating Map Object
		Map<String, Object> mapCost = new HashMap<String, Object>();
		Map<String, Object> mapCostvariables = null;
		mapCostvariables = (Map<String, Object>) variables.get("rlTicketCostMap");
		Map<String, String> mapTotalCost = new HashMap<String, String>();
		TicketCosting ticketCosting = masterdataService.newTicketCosting();
		UUID id = UUID.randomUUID();

		// create key
		// read cost from hashtable
		if (hopmCost < 1) {
			String key = retailerId + categoryId + serviceId + activityCode;
			log.debug("The key " + key);
			try {
				log.debug("costingDetails.get(key) : " + costingDetails.get(key));
				String value = costingDetails.get(key);
				amount = Float.valueOf(value);
				log.debug("it Workss FINE");
			} catch (Exception e) {
				log.debug("error in getting cost : " + e);
			}

			log.debug("Calculated amount " + amount);
		}
		if (hopmCost > 0) {

			ticketCosting.setId(String.valueOf(id));
			ticketCosting.setProcessId(processId);
			ticketCosting.setRetailerId(retailerId);
			ticketCosting.setServiceId(serviceId);
			ticketCosting.setCategoryId(categoryId);
			ticketCosting.setCostingActivityCode(activityCode);
			ticketCosting.setTransactionType(transactionType);
			ticketCosting.setActor(actor);
			ticketCosting.setCost((float) hopmCost);

			masterdataService.saveTicketCosting(ticketCosting);
			mapCost.put("id", id.toString());
			log.debug("id :" + id);
			mapCost.put("cost", String.valueOf(hopmCost));
			log.debug("amount :" + packingCost);
			mapCost.put("actor", String.valueOf(actor));
			log.debug("actor :" + actor);
			mapCost.put("service", natureOfComplaint);
			mapCost.put("name", "Material & Packaging Cost");
		}

		if (amount > 0f) {

			// insert into table
			// TicketCosting ticketCosting =
			// masterdataService.newTicketCosting();
			// UUID id = UUID.randomUUID();
			ticketCosting.setId(String.valueOf(id));
			ticketCosting.setProcessId(processId);
			ticketCosting.setRetailerId(retailerId);
			ticketCosting.setServiceId(serviceId);
			ticketCosting.setCategoryId(categoryId);
			ticketCosting.setCostingActivityCode(activityCode);
			ticketCosting.setTransactionType(transactionType);
			ticketCosting.setActor(actor);
			ticketCosting.setCost(amount);
			// Written Coz Newly HOPM Creation
			masterdataService.saveTicketCosting(ticketCosting);
			mapCost.put("id", id.toString());
			mapCost.put("cost", String.valueOf(amount));
			mapCost.put("actor", String.valueOf(actor));
		} // this } Newly Added After HOPM Cost

		try {
			// Commented coz upper written
			// masterdataService.saveTicketCosting(ticketCosting);
			// mapCost.put("id", id.toString());
			// mapCost.put("cost", String.valueOf(amount));
			// mapCost.put("actor", String.valueOf(actor));

			// get service name and activitiName based on serviceId and
			// activitiCode it as service
			if (amount > 0f) {
				try {
					ServiceMaster serviceMasterList = masterdataService.createServiceMasterQuery().id(serviceId)
							.singleResult();
					CostingActivity costingActivitiList = masterdataService.createCostingActivityQuery()
							.code(activityCode).singleResult();
					mapCost.put("service", serviceMasterList.getName());
					mapCost.put("name", costingActivitiList.getName());
				} catch (Exception e) {
					log.debug("error while quering Table" + e);
				}
			}
			try {
				if (mapCostvariables == null) {

					mapCostvariables = new HashMap<String, Object>();

				}
				mapCostvariables.put(activityCode, mapCost);

				runtimeService.setVariable(processId, "rlTicketCostMap", mapCostvariables);

				// Call method to calculate total cost by iterating
				// rlTicketCostMap variable and set the totalCost variable
				// in runtimeService
				try {
					getTotalCost(processId);
				}

				catch (Exception e) {
					log.debug("Error while calling getTotalCost() : " + e);
				}

			} catch (Exception e) {
				log.debug("eroor during adding map to runtime service : " + e.getMessage());
			}
			log.debug("Added ticket costing");
		} catch (Exception m) {
			log.debug("Eror in adding ticket costing" + m.getLocalizedMessage());
		}
		// }

		String COLLECTFROMRETAILER = "collectFromRetailer";
		String COLLECTFROMCUSTOMER = "collectFromCustomer";
		String PAYTORETAILER = "payToRetailer";
		String PAYTOCUSTOMER = "payToCustomer";
		String COLLECTFROM = "collectFrom";
		String PAYTO = "payTo";
		String RETAILER = "Retailer";
		String CUSTOMER = "Customer";

		// calculate ticket cost
		try {
			if (transactionType.equalsIgnoreCase(COLLECTFROM) && actor.equalsIgnoreCase(RETAILER)) {
				double cost = 0f;
				if (runtimeService.getVariable(processId, COLLECTFROMRETAILER) != null) {
					log.debug("Collect from Retailer - " + runtimeService.getVariable(processId, COLLECTFROMRETAILER));
					cost = amount + Double
							.valueOf(String.valueOf(runtimeService.getVariable(processId, COLLECTFROMRETAILER)));
					;
				} else {
					log.debug("Amount Value1:", amount);
					cost = amount;
				}
				runtimeService.setVariable(processId, COLLECTFROMRETAILER, cost);
			} else if (transactionType.equalsIgnoreCase(COLLECTFROM) && actor.equalsIgnoreCase(CUSTOMER)) {
				double cost = 0f;
				if (runtimeService.getVariable(processId, COLLECTFROMCUSTOMER) != null) {
					cost = amount + Double
							.valueOf(String.valueOf(runtimeService.getVariable(processId, COLLECTFROMCUSTOMER)));
					log.debug("Amount Value21:", amount);
				} else {
					log.debug("Amount Value22:", amount);
					cost = amount;
				}
				runtimeService.setVariable(processId, COLLECTFROMCUSTOMER, cost);
			} else if (transactionType.equalsIgnoreCase(PAYTO) && actor.equalsIgnoreCase(RETAILER)) {

				double cost = 0f;
				if (runtimeService.getVariable(processId, PAYTORETAILER) != null) {
					cost = amount
							+ Double.valueOf(String.valueOf(runtimeService.getVariable(processId, PAYTORETAILER)));
					log.debug("Amount Value31:", amount);
				} else {
					log.debug("Amount Value32:", amount);
					cost = amount;
				}
				runtimeService.setVariable(processId, PAYTORETAILER, cost);
			} else if (transactionType.equalsIgnoreCase(PAYTO) && actor.equalsIgnoreCase(CUSTOMER)) {

				double cost = 0f;
				if (runtimeService.getVariable(processId, PAYTOCUSTOMER) != null) {
					cost = amount
							+ Double.valueOf(String.valueOf(runtimeService.getVariable(processId, PAYTOCUSTOMER)));
					log.debug("Amount Value41:", amount);
				} else {
					cost = amount;
					log.debug("Amount Value42:", amount);
				}

				runtimeService.setVariable(processId, PAYTOCUSTOMER, cost);
			}
		} catch (Exception e) {
			log.debug(e.getLocalizedMessage());
			System.out.println(e.getMessage());
		}

	}


	public static void getTotalCost(String processId) {
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(processId);
		Map<String, Object> mapTotalCostvariables = null;
		mapTotalCostvariables = (Map<String, Object>) variables.get("rlTicketCostMap");
		try {
			float totalCost = 0.0f;
			for (String key : mapTotalCostvariables.keySet()) {
				log.debug("key :: " + key);
				log.debug("Value :: " + mapTotalCostvariables.get(key));
				Map<String, Object> singleObj = (Map<String, Object>) mapTotalCostvariables.get(key);
				String singleCost = (String) singleObj.get("cost");
				float f = Float.parseFloat(singleCost);
				totalCost = totalCost + f;
			}
			log.debug("totalCost :  " + totalCost);
			runtimeService.setVariable(processId, "rlTicketCost", totalCost);
		} catch (Exception e) {
			log.debug("error while iterating map" + e);
		}

	}

	private static void generateHashTableForCost() {

		// create a new static hash table
		// write a query to get all rows from activity costing table
		// iterate every row
		// create hash key by concatenating (retailer-id,, service-id,
		// category-id, activity_costing-code)
		// craete value = cost
		// insert key, value to hash
		// end

		// Get all Retailer costs and store it in a table
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			List<RetailerCostingActivity> retailerCostingActivityList = masterdataService
					.createRetailerCostingActivityQuery().list();
			for (RetailerCostingActivity retailerCostingActivity : retailerCostingActivityList) {
				String key = retailerCostingActivity.getRetailerId() + retailerCostingActivity.getCategoryId()
						+ retailerCostingActivity.getServiceId() + retailerCostingActivity.getCostingActivityCode();
				float value = retailerCostingActivity.getCost();

				costingDetails.put(key, String.valueOf(value));
				log.debug("key : " + key);
				log.debug("key : " + value);
			}
		} catch (Exception e) {
			log.debug("Error while putting value to costingDetaild()");
		}
	}

	private static String bytesToHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);

		Formatter formatter = new Formatter(sb);
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}
		formatter.close();
		return sb.toString();
	}

	public static String generateHashForMessage(String message) {
		try {

			// String privKey = "";
			// privKey += "-----BEGIN RSA PRIVATE KEY-----\n";
			// privKey +=
			// "MIIEpAIBAAKCAQEAhoNv+JjbuVyhIbCyx5vTEbkMXGjYL3TjVpXluyW6cq+oMqK7\n";
			// privKey +=
			// "NEC01yE2HX/LJDhEeTBBtZebPmC0m3fD8ZbgdRo/EUgZe4Gbkj1Q4KeKMuGhDNfL\n";
			// privKey +=
			// "hAk985AdTLDm+tg7ZtvG4ggxhMjg0okaNx5Ck/qqgBCEO4tQuq98cURpVZ2eud+s\n";
			// privKey +=
			// "e2/uTP9VAHDYYQz+5S1rL3OlV/Pry1hYBH1A5w99v+4SH5j4KkwUIKxZ3Zs9qWHU\n";
			// privKey +=
			// "PlDMypAP0nFQflJz3XVoqWY3LQa99wypOxm3zarryV396LAEyVYjEsvcF8LD2hK5\n";
			// privKey +=
			// "8IkVZUmcggH4J6bsLmdUV8wSsbJbCCWpShRREwIDAQABAoIBAAandeCMX0I6LNZn\n";
			// privKey +=
			// "0VBCixxzVQM1RIyEccKUw7qQUTrfKajnl+8rzEZgJDctXTU7e1erZXTAPTPGwOh3\n";
			// privKey +=
			// "8j9/QTdJxxjpwijz7mg7lEaowxNlSfxCqJStiF0GIgzrK2H5VGbwNFR2J0z0Svrf\n";
			// privKey +=
			// "QWa8nnqHieuv5Xq0l5fEmplX0wYjJQwIPyNk+dtkrKSNdwhJ2lhK26bvJDYIq7Wn\n";
			// privKey +=
			// "8cu7haOCJrRlKne30H37koJkZRpipH40olW5BzJn0EzpgB/JVxMmi7Z4UGtJUCyH\n";
			// privKey +=
			// "6Zlv9gnhhwNTtT27Pusv3FEGfCcTnnuXp9Rn6aCmEvl6pVzlTK6sbWU1iIEGydx0\n";
			// privKey +=
			// "iuW4uVECgYEA7bY1tu01YF+qQX7sIYzUWWRfawkk5sSKz2TMTDF4icgaU+/shzCr\n";
			// privKey +=
			// "KVxKJoCYjRCdsWL5RLXL5NXA68l5W1Qz6s9yi2ILhCCyUqIXIiaW8YntbAHUIGen\n";
			// privKey +=
			// "AWpp2sa/NRamvSOv4YlxUYgeX1YYAJOSv843bcmmbxX80CyDYSK8MB0CgYEAkNy2\n";
			// privKey +=
			// "L2KhvW8bRaFA1peB3+y7XptE3M4XAlEYpQoH7GtAyyvFnsMpK/9vBF6fw2vioYc6\n";
			// privKey +=
			// "Ps3mRkiWtHsqaZz5gyw0ixONUubsEm5g6GokYBkiWBJ/PUCP/VXcDhwiWnG6xyFK\n";
			// privKey +=
			// "umRFPthL9apxj2H2rJ1gEB5rTsHTUhBZh03AHu8CgYEAjUjHFDpYhzdU0ijF4Ar8\n";
			// privKey +=
			// "C0WFEPEWZbJbbnjSlFay9fFg9JKdWpyqnqKjz86O3QkLMOtKjHQhA5moWHwfDRE2\n";
			// privKey +=
			// "6lDyG7Xicl2cn1CHv1yASn9YBeHwWnJA6K5+y/FkpAEaiUA0wTAXkgX9R1y8V1te\n";
			// privKey +=
			// "1XGTsEUyDsRaxsAuNgWXJGECgYBuckjZQfsNAlwpr3NO6X7U6ppE3F8ZdmvmkUwX\n";
			// privKey +=
			// "8NjvsSpafUoT7LpEWGEdVMO8bFIDU6Xd4oBST9/PRSSmF0/Jafe5nbOfZH7S4qMy\n";
			// privKey +=
			// "MFMBjcg4nAm0+reNsFuxylOK37FHR6PBDtgYvTLU4Bnq9wbqvqZSQ88O4gqrQ9nu\n";
			// privKey +=
			// "piH3swKBgQCauCbTzKRCkL86eHMV2c1qXLMkPwE9PLZIv1tQCiP/rgeE88uhng57\n";
			// privKey +=
			// "Y0gWkfd0oKENzd+RNu4z7YJXv7jPyg2Z7Dtpztg/YnA8qYvvBqtJeddtAWvKymnY\n";
			// privKey +=
			// "VR50+w8FC3hOSReZo5ylrkV1GM3jOBnUBL2DYATylDdBHb6KfQoINA==\n";
			// privKey += "-----END RSA PRIVATE KEY-----\n";

			// New Private Key
			String privKey = "";
			privKey += "-----BEGIN RSA PRIVATE KEY-----\n";
			privKey += "MIIEowIBAAKCAQEAzcwX4G00jaets0mJknH/acMq++0AhIxkb8rrx2kUPQLD6mL8\n";
			privKey += "SMLLWq+FhEiyCjfbVL78xOMSRkbsuydLBp3oUaJvd30lrXXJOlfjAgE38VDn60SQ\n";
			privKey += "jajxBHDYtfNPXXM9arJlH2XoBt+KfwEVESd2xwGlb0t2HV/LQMHJxxRl9kC5ff3l\n";
			privKey += "9NqgGVm8aQRaI7AJc6ZBdROjZGiGiIrsprzzdRJkEJnom3klCZceo7lILRdivXku\n";
			privKey += "oW5HtJIQNlKcvtJwOjyxg56VbkJxaVxtFMYqIyzHZYf0b9KAmnXdnsWHRgdgk96v\n";
			privKey += "zRh7Q2Eipy1NcQSJr0vdpzzB1Zju4GrqMSkkHwIDAQABAoIBAQCjNQaCf1i8Nox0\n";
			privKey += "sQ8fSrTqJVODc1ODyuskFWOjQ1w/fl/tFA9LjOBEzQov/I7lt6KDtOs1IXeusDSx\n";
			privKey += "v9mqJ7TEePO5aVBmHhE16dkoD9tTz3v9guS405BAm1XiBlGcpPXCFjRIEENQoBtv\n";
			privKey += "2WXhstBpxo5ykv/bD8tbUdQ5w52RChtowDRnhOh+6LmAoRVcD/OVBDoIh79n5onU\n";
			privKey += "XX5OSqzcOKa1Xpac50zrYSsf2Yvu6vaBpBInvz5hOIXQjtbnF1LTlcVN0DDgS9EF\n";
			privKey += "iIcgZVoU3mOp7e2sl+DJyMHkbLuAkJT9C7K4oZkn7BHIKSEbXhEM4RvU7gtQlR78\n";
			privKey += "yOlx9okBAoGBAOrXldEUgklDvQAeuAPaecIAeVffkpGgRd5Qj35iWzm6ELOSZdFO\n";
			privKey += "Rdz4eyH1tTs2Q7aj04UX8vslevl1rt33Mxtxj9jhQjqz5XrJhlz071gFm36MBJIk\n";
			privKey += "ApHXa0RZsqSOkEdmQSLax14cz30fQa/Ecmwc6y7lL+8oaNARgif2Z2dfAoGBAOBW\n";
			privKey += "nV7FSZycSuuWpCPIoRiabtSoFZqyqOs2H6ZcN23c6BT2y0QJrnNPSfWkHLgEGggR\n";
			privKey += "H4Lh3lTSW9gkwL/dHcexN4Fxwg21uElHMsm20wQRuWVCM+C7b+C5yyE6hSsYk6Yj\n";
			privKey += "nPr4vPlxXzrw7asl3wVVXoCG1fsVImjmD0uFCztBAoGANHtVWdJRg3oF5N74lLPg\n";
			privKey += "fgCJHaAzKyQ8OQCb8MyeQnpYfSj8ZBgv+L/3FJHKnJ715v0Zqia+AG5R2yn3mFdE\n";
			privKey += "Lp/kW72LhX7qi9Q5mNCMJImsRE2aP+aYRGt152J8T9YkXDB34ggugdPCct3nWhZ2\n";
			privKey += "075quKIzYikPs2AWTEP+u9UCgYBF/P+vt2EVyPTetuqSd18668Mz+RR0ZNSqPQJ2\n";
			privKey += "xkJMtiR5ld0oZtTUCKKMThzfk/gDGER6crkIQXCB6EVyFivaRwGIEtN1r4HE6r9/\n";
			privKey += "itgeZuEuJA9HR3LJ62zh+v3cyhgWNvocmklqkOIi41Nil7gSU+XdtzM+2AMaMtwG\n";
			privKey += "tYUhgQKBgByYCXu3+j0+QYHg2Wwu8sNUXC8h+7hcDGk02Qh3J67L89RR21iyODeZ\n";
			privKey += "QndPEQc8soBLSKH0FxrMaROsMMgxpZzxwWV50LFFiJvnmEOG5dDuZgLpz1VPVVpe\n";
			privKey += "j/MpwqAanoW4wMst+G+fVyfdoMXHSu98m1Wx8npqHD1OBY/LKePK\n";
			privKey += "-----END RSA PRIVATE KEY-----\n";
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			System.out.println("Key [" + privKey + "]");
			PEMParser parser = new PEMParser(new StringReader(privKey));
			PEMKeyPair pemPair = (PEMKeyPair) parser.readObject();
			KeyPair keyPair = new JcaPEMKeyConverter().setProvider("BC").getKeyPair(pemPair);
			PrivateKey privateKey = keyPair.getPrivate();
			Signature sign = Signature.getInstance("SHA512withRSA");
			sign.initSign(privateKey);
			System.out.println("TEST3");
			sign.update(message.getBytes());
			byte[] hashBytes = sign.sign();
			return bytesToHexString(hashBytes);
		} catch (Exception e) {
			log.debug("Error in GeneratingHAsh :" + e);
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(value = "/payment/acknowledge", method = RequestMethod.POST)
	public ModelAndView callBackPayment(HttpServletRequest req, HttpServletResponse res, HttpSession session,
			@RequestBody String queryJson) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		ModelAndView model = new ModelAndView("review");
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		JSONObject output = new JSONObject();
		try {
			System.out.println("Query String : " + queryJson);
			log.debug("Query String : " + queryJson);
			Map<String, Object> reqObj = new HashMap<String, Object>();

			try {
				reqObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
			} catch (IOException i) {
				log.debug("Error - " + i.getMessage());
			}

			output.put("invoice", reqObj.get("invoice"));
			System.out.println("Invoice : " + reqObj.get("invoice"));
			log.debug("Invoice : " + reqObj.get("invoice"));
			output.put("status", reqObj.get("status"));
			System.out.println("Status : " + reqObj.get("status"));
			output.put("paymentReference", reqObj.get("payment_reference"));
			log.debug("paymentReference : " + reqObj.get("payment_reference"));

			PaymentQuery paymentQuery = masterdataService.createPaymentQuery();
			Payment payment = paymentQuery.txnId(String.valueOf(reqObj.get("payment_reference"))).singleResult();
			output.put("Amount", payment.getAmount());

			String message = "";
			if (String.valueOf(reqObj.get("status")).equalsIgnoreCase("C")) {
				output.put("status", "Success");

				message += "Payment Successful - Reference No : " + reqObj.get("payment_reference") + " Amount : "
						+ payment.getAmount();
				payment.setStatus("Success");
				payment.setPaymentResponse(queryJson);

				masterdataService.savePayment(payment);
			} else {
				payment.setStatus("Failure");
			}

			// set processVariables
			String processId = payment.getProcessId();
			/*
			 * runtimeService.setVariable(processId,
			 * "amount",payment.getAmount());
			 * runtimeService.setVariable(processId,
			 * "name",payment.getFirstName());
			 * runtimeService.setVariable(processId,
			 * "status",payment.getStatus());
			 * runtimeService.setVariable(processId,
			 * "transactionId",payment.getId());
			 * runtimeService.setVariable(processId,
			 * "payePhone",payment.getPhone());
			 * runtimeService.setVariable(processId,
			 * "payeEmail",payment.getEmail());
			 * runtimeService.setVariable(processId, "PaymentType",
			 * payment.getPaymentType());
			 *

			// Send push notification

			try {
				DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
				List<DeviceInfo> deviceInfoList = deviceInfoQuery.userId(payment.getEmail()).list();

				for (DeviceInfo deviceInfo : deviceInfoList) {
					String deviceId = deviceInfo.getDeviceId();
					AndroidPush.pushFCMNotification(deviceId, processId, "PaymentStatus", message);
				}

			} catch (Exception s) {
				log.debug("Error in sending push : " + s.getMessage());
			}

		} catch (JSONException j) {

		}

		// for(String attribute : reqAttr)
		model.addObject("output", output);
		return model;
	}

	@RequestMapping(value = "/fund/enquiry1", method = RequestMethod.POST)
	public static RestResponse fundEnquiry1(@RequestParam("processId") String processId, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		JSONObject paymentObj = new JSONObject();
		// String merchantId = "FPTEST"; //test value
		String merchantId = "FPAP"; // new merchant Id
		// String merchantId = "A469";
		String api_key = "08Z1782051U62BY9OUGW4XM67GF2004"; // test value
		// String api_key = "5C546E02213T406H0Y19I5L45W3525A";

		try {
			List<Payment> payment = masterdataService.createPaymentQuery().processId(processId).list();
			if (payment.size() > 0) {
				int noOfPay = payment.size() - 1;
				log.debug("PaymentSize() :" + payment.size());
				log.debug("noOfPay : " + noOfPay);
				String requestId = payment.get(noOfPay).getRequestId();
				log.debug("requestId : " + requestId);
				System.out.println("Before setting the values");
				paymentObj.put("id", merchantId);
				paymentObj.put("merchant_id", merchantId);
				String eventId = "CSHOT";
				paymentObj.put("event_id", "CSHOT");
				paymentObj.put("from_date*", "2018-02-20");

				System.out.println("Before generating hash");
				String hashInput = api_key + "#" + merchantId + "#" + merchantId + "#" + eventId + "#";
				String hash = generateHashForMessage(hashInput);

				System.out.println("After computing hash");
				paymentObj.put("request_id", requestId);
				paymentObj.put("sign", hash);

				log.debug("Signature Hash :- " + hash);
				System.out.println("Signature Hash :- " + hash);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();

				String apiUrl = "https://test.fonepaisa.com/portal/funds/enquire"; // test
																					// value
				// String apiUrl =
				// "https://secure.fonepaisa.com/portal/funds/enquire";
				HttpPost httppost = new HttpPost(apiUrl);
				httppost.addHeader("Content-Type", "application/json");

				System.out.println("\n Payment Request :" + paymentObj);
				log.debug("\n Payment Request :" + paymentObj);
				// Set HTTP entity
				httppost.setEntity(new StringEntity(paymentObj.toString(), "UTF8"));

				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);

				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug("Response from server :" + rawResponse);
				System.out.println("Response from server :" + rawResponse);

				Map<String, Object> paymentRes = new ObjectMapper().readValue(URLDecoder.decode(rawResponse),
						Map.class);

				restResponse.setResponse(insertIntoPaymentAcknowledgement(processId, paymentRes));
				log.debug("inside Saving To DB PAYACK :"+restResponse.getResponse());
				log.debug("Before Inserting into PaymentAcknowledgement");
//				insertIntoPaymentAcknowledgement(processId, paymentRes);
				if (httpResponse.getStatusLine().getStatusCode() == 200) {

					restResponse.setSuccess(true);
					
				} else {
					restResponse.setSuccess(false);
					response.setStatus(httpResponse.getStatusLine().getStatusCode());
				}
			} else {
				restResponse.setMessage("No Payment Info is Available For This Ticket");
				restResponse.setSuccess(true);
			}

		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
			System.out.println("Exception occured " + e.getMessage());
			restResponse.setSuccess(false);
			restResponse.setMessage("Error in processing");
			response.setStatus(500);
		}
		return restResponse;
	}



	public static PaymentAcknowledgement insertIntoPaymentAcknowledgement(@RequestParam("processId") String processId,
			Map<String, Object> response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
//		RestResponse restResponse = new RestResponse();
		PaymentAcknowledgement payAck = masterdataService.newPaymentAcknowledgement();
		try {
			log.debug("inside insertIntoPaymentAcknowledgement");
			
			ArrayList<Map<String, Object>> txnResponse = (ArrayList<Map<String, Object>>) response.get("tran_list");
			Map<String, Object> requestObj = txnResponse.get(0);
			String status = String.valueOf(requestObj.get("status"));
			if (status.equalsIgnoreCase("f") || status.equalsIgnoreCase("s")) {
				try {
					log.debug("inside insertIntoPaymentAcknowledgement try block");
					log.debug("requestObj : "+requestObj);
					
					payAck.setAccHolderName(String.valueOf(requestObj.get("acct_name")));
					payAck.setEnquiryResponse(String.valueOf(requestObj));
					payAck.setAccNo(String.valueOf(requestObj.get("acct_number")));
					payAck.setAmount(Float.valueOf(String.valueOf(requestObj.get("total_amount"))));
					payAck.setIfsc(String.valueOf(requestObj.get("ifsc_code")));
					payAck.setMessage(String.valueOf(requestObj.get("resp_msg")));
					payAck.setPaymentRef(String.valueOf(requestObj.get("payment_reference")));
					payAck.setProcessId(processId);
					payAck.setRequestId(String.valueOf(requestObj.get("request_id")));
					payAck.setStatus(String.valueOf(requestObj.get("status")));
					payAck.setTxnDate(String.valueOf(requestObj.get("request_date")));
					try{
						masterdataService.savePaymentAcknowledgement(payAck);
						log.debug("inside Saving To DB PAYACK :"+payAck);
						return payAck;
//						restResponse.setResponse(payAck);
					}catch(Exception e){
						log.debug("Error in Saving Data To Payment Acknowledgement : "+e);
					}
				} catch (Exception e) {
					log.debug("Error in Saving Txn Details In Payment:" + e);
				}
			}
		} catch (Exception e) {
			log.debug("Error in InsertPayAckn method : " + e);
		}
		return payAck;
	}

	@RequestMapping(value = "/fund/enquiry", method = RequestMethod.POST)
	public RestResponse fundEnquiry(@RequestParam("processId") String processId, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		String requestId = "";
		try {
			List<Payment> payment = masterdataService.createPaymentQuery().processId(processId).list();
			if (payment.size() > 0) {
				int noOfPay = payment.size() - 1;
				 requestId = payment.get(noOfPay).getRequestId();
				}
			List<PaymentAcknowledgement> payAckList = masterdataService.createPaymentAcknowledgementQuery()
					.processId(processId).requestId(requestId).orderByTxnDate().desc().list();
			if (payAckList != null && payAckList.size()>0) {
				try {
					PaymentAcknowledgement payAck = payAckList.get(0);
					restResponse.setResponse(payAck);
					restResponse.setMessage("Success");
					restResponse.setSuccess(true);
				} catch (Exception e) {
					log.debug("Error in Calling PackAcknow table : " + e);
				}
				return restResponse;
			} else {
				try {
					return fundEnquiry1(processId, null);
				} catch (Exception e) {
					log.debug("fundEnquiry1 :" + e);
				}
			}

		} catch (Exception e) {
			log.debug("Error in Calling fund enquiry :" + e);
		}
		return restResponse;

	}
	
	
	@RequestMapping(value = "/payment/test1", method = RequestMethod.POST)
	public RestResponse requestPayment11(@RequestParam("processId") String processId,
			@RequestParam("loggedEmail") String deliveryBoyEmail, HttpServletResponse response) {
		RestResponse restResponse = new RestResponse();
		
	

		try {
			JSONObject paymentObj = new JSONObject();

			

			// calculate amount
			

			System.out.println("Before setting the values");
			paymentObj.put("id", "");

		


	

			// API call
			CloseableHttpClient httpclient = HttpClients.createDefault();

			String apiUrl = "http://logistics-integ.api.stage.cashify.in:84/v1/status/callback/csh_bizlog"; // test
			// value
			// String apiUrl =
			// "https://secure.fonepaisa.com/portal/payment/request";
			HttpPost httppost = new HttpPost(apiUrl);
			httppost.addHeader("Content-Type", "application/json");

			System.out.println("\n Payment Request :" + paymentObj);
			// Set HTTP entity
			httppost.setEntity(new StringEntity(paymentObj.toString(), "UTF8"));

			// Get Response
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			log.debug("Response from server :" + rawResponse);
			System.out.println("Response from server :" + rawResponse);

			

			
			


		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
			System.out.println("Exception occured " + e.getMessage());
			restResponse.setSuccess(false);
			restResponse.setMessage("Error in processing");
			response.setStatus(500);
		}

		return restResponse;
	}
	
	*/
	public static void insertActivityCost(String processId, String activityCode, String actor, String transactionType) {
	}

}
