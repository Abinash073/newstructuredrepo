package com.rlogistics.rest.activiticalls;

import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.rest.RetailerApiCalls;

public class ActivitiBuyBack {

	public static void buyBackCustomerAgreeWithValueStatusAfterScript(String processId) {
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(processId);
		RetailerApiCalls.saveToConsoleLogsTable("ACTIVITI CALL FROM JAVA",
				"ActivitiBuyBack.buyBackCustomerAgreeWithValueStatusAfterScript");
		try {

			if (variables.get("rlCustomerStatusDuringFV").equals("available")) {
				if (variables.get("rlBBCustomerAgreement").equals("true")) {
					variables.put("rlProcessStatus", "Buy back product picked up.");
					variables.put("rlTicketStatus", "Field engineer has assessed the value and picked up the product");
					variables.put("rlTicketStatusCode", "BZITTKSTS-0238");
				} else if (variables.get("rlBBCustomerAgreement").equals("false")) {
					variables.put("rlProcessStatus", "Customer is not agree after Evaluation");
					variables.put("rlTicketStatus",
							"Field engineer has assessed the value but customer is not agree after Evaluation");
					variables.put("rlTicketStatusCode", "BZITTKSTS-0380");
				} else if (variables.get("rlBBCustomerAgreement").equals("AskedforRequote")) {
					variables.put("rlProcessStatus",
							"Field Engineer visited and assessed the product. Customer did not agree with the value offered. Awaiting Retailer reply for Re-quote");
					variables.put("rlTicketStatus",
							"Field engineer has assessed the value. Customer did not agree with the value offered. Get the confirmation and/or amount from the Retailer for Re-quote");
					variables.put("rlTicketStatusCode", "BZITTKSTS-0239");
				}
			}

//			else if (variables.get("rlCustomerStatusDuringFV").equals("nopickup")) {
//				variables.put("rlProcessStatus", "Customer refused service");
//				variables.put("rlTicketStatus", "Customer refused service. Close the ticket");
//				variables.put("rlTicketStatusCode", "BZITTKSTS-0240");
//			} else if (variables.get("rlCustomerStatusDuringFV").equals("reschedule")) {
//				variables.put("rlProcessStatus", "Customer requested for rescheduling");
//				variables.put("rlTicketStatus", "Customer needs rescheduling of the product");
//			}
			runtimeService.setVariables(processId, variables);
		} catch (Exception e) {


			RetailerApiCalls.saveToConsoleLogsTable("ERROR IN ACTIVITI CALL FROM JAVA"
					, "ActivitiBuyBack.buyBackCustomerAgreeWithValueStatusAfterScript");
		}

	}

}
