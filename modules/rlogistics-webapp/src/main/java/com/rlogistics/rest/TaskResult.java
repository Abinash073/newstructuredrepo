package com.rlogistics.rest;

import java.util.Date;

import org.activiti.engine.task.Task;

public class TaskResult {

    private String processId;
    private String taskId;
    private String assignee;
    private String name;
    private String category;
    private Date createTime;
    private String description;
    private int priority;
    private String retailer;
    private String rlTicketNo;
    private String natureOfComplaint;
    private String ticketPriority;
    private String address1;
    private String address2;
    private Object appointmentDate;
    private Object pincode;
    private Object orderNumber;



    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public String getNatureOfComplaint() {
        return natureOfComplaint;
    }

    public void setNatureOfComplaint(String natureOfComplaint) {
        this.natureOfComplaint = natureOfComplaint;
    }

    public TaskResult(Task task) {
        setProcessId(task.getProcessInstanceId());
        setTaskId(task.getId());
        setAssignee(task.getAssignee());
        setName(task.getName());
        setCategory(task.getCategory());
        setCreateTime(task.getCreateTime());
        setDescription(task.getDescription());
        setPriority(task.getPriority());
        setPincode(task.getProcessVariables().get("pincode"));
        setOrderNumber(task.getProcessVariables().get("orderNumber"));
//			setTicketPriority(ticketPriority);
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }

    public String getRlTicketNo() {
        return rlTicketNo;
    }

    public void setRlTicketNo(String rlTicketNo) {
        this.rlTicketNo = rlTicketNo;
    }

    public String getTicketPriority() {
        return ticketPriority;
    }

    public void setTicketPriority(String ticketPriority) {
        this.ticketPriority = ticketPriority;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Object getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Object object) {
        this.appointmentDate = object;
    }

	public Object getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Object orderNumber) {
		this.orderNumber = orderNumber;
	}
    


}
