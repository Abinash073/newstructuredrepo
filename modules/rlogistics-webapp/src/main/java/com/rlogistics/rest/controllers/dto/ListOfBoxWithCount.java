package com.rlogistics.rest.controllers.dto;

import com.rlogistics.rest.services.helper.BoxNumberWithCount;
import lombok.Data;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@Data
public class ListOfBoxWithCount {
    private List<BoxNumberWithCount> boxDetails;
    private int count = 0;
    private String date = new LocalDateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Calcutta"))).toString();
    private String message;

    public ListOfBoxWithCount() {
        this.boxDetails = new ArrayList<>();
    }
}

