package com.rlogistics.rest;

import java.util.List;

public class TaskResultWithPagination {

	List<TaskResult> data;
	long totalRecords;
	
	
	public List<TaskResult> getData() {
		return data;
	}
	public void setData(List<TaskResult> data) {
		this.data = data;
	}
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
}
