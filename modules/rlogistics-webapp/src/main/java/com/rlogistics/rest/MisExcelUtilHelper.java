package com.rlogistics.rest;

import java.util.LinkedHashMap;

public class MisExcelUtilHelper {
    private LinkedHashMap<String, String> cvMap = new LinkedHashMap<>();

    public MisExcelUtilHelper() {
        cvMap.put("S.No", String.valueOf(cvMap.size()));
        cvMap.put("Ticket No.", "rlTicketNo");
        cvMap.put("City", "rlReportingCity");
        cvMap.put("ProcessId", "processId");
        cvMap.put("Retailer Name", "retailer");
        cvMap.put("Start Date", "startDate");
        cvMap.put("End Date", "endDate");

    }

    public LinkedHashMap<String, String> getCvMap() {
        return cvMap;
    }

}
