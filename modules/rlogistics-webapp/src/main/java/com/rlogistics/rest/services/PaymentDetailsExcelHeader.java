package com.rlogistics.rest.services;

import java.util.LinkedHashMap;

public class PaymentDetailsExcelHeader {

    private LinkedHashMap<String, String> data = new LinkedHashMap<>();

    public PaymentDetailsExcelHeader() {
        data.put("Serial No", "#");
        data.put("Ticket No", "ticketNo");
        data.put("Location", "location");
        data.put("Customer Name", "customerName");
        data.put("Retailer Name", "retailerName");
        data.put("Amount", "amount");
        data.put("Status", "status");
        data.put("Phone No", "phoneNo");
        data.put("Payment Type", "paymentType");
        data.put("Created On", "createdOn");
        data.put("Updated On", "updatedOn");

    }

    public LinkedHashMap<String, String> getHeader() {
        return data;
    }
}
