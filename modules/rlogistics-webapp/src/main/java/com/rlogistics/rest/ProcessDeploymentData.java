package com.rlogistics.rest;

import org.activiti.engine.identity.rlogistics.ProcessDeployment;

public class ProcessDeploymentData {
	private String dataset;
	private String name;
	private String deploymentId;
	
	public ProcessDeploymentData(ProcessDeployment deployment){
		setDataset(deployment.getDataset());
		setName(deployment.getName());
		setDeploymentId(deployment.getDeploymentId());
	}
	
	public String getDataset() {
		return dataset;
	}
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
}
