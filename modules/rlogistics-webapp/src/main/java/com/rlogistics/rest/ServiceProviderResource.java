package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.CostingActivity;
import com.rlogistics.master.identity.CostingActivity.CostingActivityQuery;
import com.rlogistics.master.identity.CostingActivityMapping;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerCostingActivity;
import com.rlogistics.master.identity.ServiceMaster;
import com.rlogistics.master.identity.ServiceProvider;
import com.rlogistics.rest.ExcelProcessResource.ProcessCommandProcessorWithRenamingExcel;
import com.rlogistics.rest.ProductResource.ProductCommandProcessorExcel;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class ServiceProviderResource extends RLogisticsResource {
	
	private static Logger log = LoggerFactory.getLogger(ServiceProviderResource.class);

	@RequestMapping(value = "/service-provider/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadServiceProviderExcel(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file, @RequestParam(value = "retailerId") String retailerId) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			return new ServiceProviderCommandProcessorExcel().serviceProviderUpload(file.getInputStream(),file);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	@RequestMapping(value = "/costing-mapping/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadCostingMappingExcel(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			return new CostingMappingCommandProcessorExcel().costingMappingUpload(file.getInputStream(),file);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	@RequestMapping(value = "/retailer-costing/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadRetailerCostingExcel(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file,@RequestParam(required = true, value = "retailerId") String retailerId) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			return new RetailerCostingCommandProcessorExcel().retailerCostingUpload(file.getInputStream(),file,retailerId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	protected class ServiceProviderCommandProcessorExcel {
		public UploadProcessCommandsResult serviceProviderUpload(InputStream is, MultipartFile file) {

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			
			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {
				
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));
				
				String fileName = file.getName();
				Workbook workbook = null;
				/*if (fileName.toLowerCase().contains("xlsx")) {
					workbook = new XSSFWorkbook(is);
				} else {
					workbook = new XSSFWorkbook(is);
				}*/
				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {
					
				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();
				record = bis.readLine();
				
				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {
					
					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo=1;
					
					/* Header record*/
					for(int fr=firstRow.getFirstCellNum();fr<firstRow.getLastCellNum();fr++){
						orderedFields.add(firstRow.getCell(fr).getStringCellValue());
					}
					if(log.isDebugEnabled()){
						log.debug("HEADER:" + recordNo + ":" + record);
						for (String f : orderedFields) {
							log.debug("------------" + f);
						}
					}
					
					for (int r = 1; r <= totalRowCount; r++) {
						
						recordNo++;
						
						if (sheet.getRow(r) != null) {
							
							Row row = sheet.getRow(r);
							final Map<String,String> fieldValuesPreFilter = new HashMap<String, String>();
							for(int i = row.getFirstCellNum();i < row.getLastCellNum();i++) {
								if(row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if(!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}
							
							final Map<String,String> fieldValues = filterFieldValues(fieldValuesPreFilter);
							
							ServiceProvider serviceProvider = masterdataService.newServiceProvider();
							UUID id = UUID.randomUUID();
							serviceProvider.setId(String.valueOf(id));
							serviceProvider.setName(fieldValues.get("name"));
							serviceProvider.setCode(fieldValues.get("code"));
							if(fieldValues.get("description") != null) {
								if(!fieldValues.get("description").equals("")) {
									serviceProvider.setDescription(fieldValues.get("description"));
								}
							}
							
							try {
								masterdataService.saveServiceProvider(serviceProvider);
								insertCount ++;
							} catch (Exception e) {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("Duplicate Records");
								retval.getErrorMessages().add(errorObject);
								continue;
							}
							
						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Record");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);
				
			} catch (IOException ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			return retval;	
		}
		
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}
	
	
	protected class CostingMappingCommandProcessorExcel {
		public UploadProcessCommandsResult costingMappingUpload(InputStream is, MultipartFile file) {

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			
			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {
				
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));
				
				String fileName = file.getName();
				Workbook workbook = null;
				/*if (fileName.toLowerCase().contains("xlsx")) {
					workbook = new XSSFWorkbook(is);
				} else {
					workbook = new XSSFWorkbook(is);
				}*/
				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {
					
				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();
				record = bis.readLine();
				
				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {
					
					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo=1;
					
					try {
						/* Header record*/
						for(int fr=firstRow.getFirstCellNum();fr<firstRow.getLastCellNum();fr++){
							orderedFields.add(firstRow.getCell(fr).getStringCellValue());
						}
						if(log.isDebugEnabled()){
							log.debug("HEADER:" + recordNo + ":" + record);
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
					} catch (Exception h) {
						
					}
					
					
					for (int r = 1; r <= totalRowCount; r++) {
						
						recordNo++;
						
						if (sheet.getRow(r) != null) {
							
							Row row = sheet.getRow(r);
							final Map<String,String> fieldValuesPreFilter = new HashMap<String, String>();
							for(int i = row.getFirstCellNum();i < row.getLastCellNum();i++) {
								if(row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if(!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}
							
							final Map<String,String> fieldValues = filterFieldValues(fieldValuesPreFilter);
							
							CostingActivityMapping costingMapping = masterdataService.newCostingActivityMapping();
							UUID id = UUID.randomUUID();
							costingMapping.setId(String.valueOf(id));
							
							String costingActivity = fieldValues.get("costingActivity");
							log.debug("Costing Activity --->" + costingActivity);
							CostingActivityQuery costingActivityQuery = masterdataService.createCostingActivityQuery();
							CostingActivity costingActivityObj = null;
							try {
								costingActivityObj = costingActivityQuery.name(costingActivity).singleResult();
							} catch (Exception ce) {
								break;
							}
							
							if(costingActivityObj != null) {
								String productCategory = fieldValues.get("category");
								ProductCategory productCategoryObj = null;
								try {
									productCategoryObj = masterdataService.createProductCategoryQuery().name(productCategory).singleResult();
								} catch(Exception pe) {
									break;
								}
								
								if(productCategoryObj != null) {
									String service = fieldValues.get("service");
									ServiceMaster serviceObj = null;
									try {
										serviceObj = masterdataService.createServiceMasterQuery().name(service).singleResult();
									} catch (Exception se) {
										break;
									}
									
									if(serviceObj != null) {
										costingMapping.setCostingActivityId(costingActivityObj.getId());
										costingMapping.setCategoryId(productCategoryObj.getId());
										costingMapping.setServiceId(serviceObj.getId());
										
										try {
											masterdataService.saveCostingActivityMapping(costingMapping);
											insertCount++;
										} catch (Exception e) {
											ErrorObject errorObject = new ErrorObject();
											errorObject.setRowNo(recordNo);
											errorObject.setMessage("Ignoring line " + recordNo);
											errorObject.setReason("Duplicate Record");
											retval.getErrorMessages().add(errorObject);
										}
									} else {
										ErrorObject errorObject = new ErrorObject();
										errorObject.setRowNo(recordNo);
										errorObject.setMessage("Ignoring line " + recordNo);
										errorObject.setReason("Service does not exist");
										retval.getErrorMessages().add(errorObject);
									}
								} else {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("Category does not exist");
									retval.getErrorMessages().add(errorObject);
								}
							} else {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("CostingService does not exist");
								retval.getErrorMessages().add(errorObject);
							}
						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Record");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);
				
			} catch (IOException ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			return retval;	
		}
		
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}
	
	
	protected class RetailerCostingCommandProcessorExcel {
		public UploadProcessCommandsResult retailerCostingUpload(InputStream is, MultipartFile file, String retailerId) {

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			
			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {
				
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));
				
				String fileName = file.getName();
				Workbook workbook = null;
				/*if (fileName.toLowerCase().contains("xlsx")) {
					workbook = new XSSFWorkbook(is);
				} else {
					workbook = new XSSFWorkbook(is);
				}*/
				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {
					
				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();
	//			record = bis.readLine();
				
				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {
					
					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo=1;
					
					try {
						/* Header record*/
						for(int fr=firstRow.getFirstCellNum();fr<firstRow.getLastCellNum();fr++){
							orderedFields.add(firstRow.getCell(fr).getStringCellValue());
						}
						if(log.isDebugEnabled()){
							log.debug("HEADER:" + recordNo + ":" + record);
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
					} catch (Exception h) {
						
					}
					
					for (int r = 1; r <= totalRowCount; r++) {
						
						recordNo++;
						
						if (sheet.getRow(r) != null) {
							
							Row row = sheet.getRow(r);
							final Map<String,String> fieldValuesPreFilter = new HashMap<String, String>();
							for(int i = row.getFirstCellNum();i < row.getLastCellNum();i++) {
								if(row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if(!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}
							
							final Map<String,String> fieldValues = filterFieldValues(fieldValuesPreFilter);
							
							RetailerCostingActivity retailerCosting = masterdataService.newRetailerCostingActivity();
							UUID id = UUID.randomUUID();
							retailerCosting.setId(String.valueOf(id));
							
							if(fieldValues.get("category") == null || fieldValues.get("category").equals("")) {
								break;
							}
							
							String costingActivity = fieldValues.get("costingActivity");
							
							CostingActivityQuery costingActivityQuery = masterdataService.createCostingActivityQuery();
							CostingActivity costingActivityObj = null;
							try {
								costingActivityObj = costingActivityQuery.name(costingActivity).singleResult();
							} catch (Exception ce) {
								break;
							}
							
							if(costingActivityObj != null) {
								String productCategory = fieldValues.get("category");
								ProductCategory productCategoryObj = null;
								try {
									productCategoryObj = masterdataService.createProductCategoryQuery().name(productCategory).singleResult();
								} catch (Exception pe) {
									break;
								}
								if(productCategoryObj != null) {
									String service = fieldValues.get("service");
									ServiceMaster serviceObj = null;
									try {
										serviceObj = masterdataService.createServiceMasterQuery().name(service).singleResult();
									} catch (Exception se) {
										break;
									}
									
									if(serviceObj != null) {
										
										retailerCosting.setCostingActivityCode(costingActivityObj.getCode());
										retailerCosting.setCategoryId(productCategoryObj.getId());
										retailerCosting.setServiceId(serviceObj.getId());
										retailerCosting.setRetailerId(retailerId);
										retailerCosting.setCost(Float.valueOf(String.valueOf(fieldValues.get("cost"))));
										retailerCosting.setStatus(1);
											
											try {
												masterdataService.saveRetailerCostingActivity(retailerCosting);
												insertCount++;
											} catch (Exception e) {
												ErrorObject errorObject = new ErrorObject();
												errorObject.setRowNo(recordNo);
												errorObject.setMessage("Ignoring line " + recordNo);
												errorObject.setReason("Duplicate record");
												retval.getErrorMessages().add(errorObject);
											} 
									} else {
										ErrorObject errorObject = new ErrorObject();
										errorObject.setRowNo(recordNo);
										errorObject.setMessage("Ignoring line " + recordNo);
										errorObject.setReason("Service does not exist");
										retval.getErrorMessages().add(errorObject);
									}
								} else {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("Category does not exist");
									retval.getErrorMessages().add(errorObject);
								}
							} else {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("Costing Activity does not exist");
								retval.getErrorMessages().add(errorObject);
							}
						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Record");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);
				
			} catch (Exception ex) {
				ErrorObject errorObject = new ErrorObject();
				errorObject.setMessage("Error");
				errorObject.setReason("Failed to read uploaded content");
				retval.getErrorMessages().add(errorObject);
			}
			return retval;	
		}
		
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}
}
