package com.rlogistics.rest;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.master.identity.*;
import lombok.extern.slf4j.Slf4j;
import pojo.ReportPojo;

import org.activiti.engine.*;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Slf4j

public class TicketCreationImpl {
    public ProcessInstance submitStartForm(FormService formService, ProcessDefinition definition,
                                           FormFillerUtil formFillerUtil) {
        ProcessInstance processInstance = (ProcessInstance) formFillerUtil
                .submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
                    @Override
                    public ProcessInstance submitForm(Map<String, String> fieldValues) {
                        return formService.submitStartFormData(definition.getId(), fieldValues);
                    }
                });

        /*
         * TODO: Leads to bugs -- hence disabling. But will need to be fixed
         * when people start using start forms elaborately CommandExecutor
         * commandExecutor =
         * ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.
         * getDefaultProcessEngine().getProcessEngineConfiguration())).
         * getCommandExecutor(); commandExecutor.execute(new Command<Boolean>()
         * {
         *
         * @Override public Boolean execute(CommandContext commandContext) {
         * execution.setVariables(formFillerUtil.getVariables()); return true; }
         * });
         */
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        ReportingController reportingController = new ReportingController();
        try {

            ProcessLocation pincode = masterdataService.createProcessLocationQuery()
                    .id(masterdataService.createLocationPincodesQuery()
                            .pincode(formFillerUtil.getVariables().get("pincode").toString()).singleResult()
                            .getLocationId())
                    .singleResult();
            log.debug(pincode.getCity());
//            reportingController.addReportDetails(pincode.getCity(), "add",
//                    formFillerUtil.getVariables().get("retailer").toString());
        } catch (Exception e) {
            log.debug("ERROR---------------------" + e);
        }
        return processInstance;
    }

    public UploadProcessCommandsResult buildTicket(Map<String, String> fieldValues, String retailerId, int iteration) {
        log.debug("Inside build Ticket");
        UploadProcessCommandsResult retval = new UploadProcessCommandsResult();

        //Initializing all the objects/Variables required
        MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMetadataService();
        RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getRepositoryService();
        FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getFormService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();


        try {

            String dataset = "BizLog";
            String processName = null;


            List<RetailerProcessField> retailerProcessFields = masterdataService
                    .createRetailerProcessFieldQuery().retailerId(retailerId).list();
            for (RetailerProcessField retailerProcessField : retailerProcessFields) {
                if (!fieldValues.containsKey(retailerProcessField.getCanonicalField())) {
                    fieldValues.put(retailerProcessField.getCanonicalField(), "");
                }
            }
            
            
           

            try {
            	FranchisePincode franchisePincode = masterdataService.createFranchisePincodeQuery().
        				pincode(fieldValues.get("pincode")).singleResult();
            	log.debug("Field Values ==================== " + String.valueOf(fieldValues));
        		log.debug("**************************************"+franchisePincode +  "********Nature Of Complaint : ********" + fieldValues.get("natureOfComplaint") + "**********************************");

        		if((franchisePincode!=null)&& (fieldValues.get("natureOfComplaint").equalsIgnoreCase("Pick And Drop (One Way)")))
        		{
        			processName="fl_Flow";
        			log.debug("************Inside Fl Floow****************");
        		}
        		else {
            	

                log.debug("Nature Of Complaint :- " + fieldValues.get("natureOfComplaint"));
                // String productCategory = "mobile";
                if (fieldValues.containsKey("natureOfComplaint")
                        && fieldValues.get("natureOfComplaint").equalsIgnoreCase("E-Waste")) {
                    log.debug("Inside E-Waste if condition");
                    processName = "e_waste";
                } else if (fieldValues.get("natureOfComplaint").indexOf("Bulk") != -1
                        || fieldValues.get("natureOfComplaint").indexOf("bulk") != -1) {
                    processName = "bulk_txn";
                } else if (fieldValues.containsKey("natureOfComplaint")
                        && fieldValues.get("natureOfComplaint").equalsIgnoreCase("BITS")) {
                    processName = "bits_txn";
                } else if (fieldValues.containsKey("natureOfComplaint")
                        && fieldValues.get("natureOfComplaint").equalsIgnoreCase("RepairStandBy")) {
                    processName = "repair_standby";
                } else if (fieldValues.containsKey("natureOfComplaint")
                        && fieldValues.get("natureOfComplaint").equalsIgnoreCase("PND-BULK")) {
                    processName = "itpnd_bulk";
                }else if(fieldValues.containsKey("natureOfComplaint")&& fieldValues.get("natureOfComplaint").equalsIgnoreCase("BULK-MAIN")){
                    processName = "mass_flow";
                }
                else {
                    log.debug("Inside default else condition");
                    try {
                        String productCategory = fieldValues.get("productCategory").toLowerCase();
                        log.debug(productCategory);
                        MasterCategoryResult masterCategoryResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                                .getMasterCategory(productCategory);
                        processName = masterCategoryResult.getProcessFlow();
                        log.debug(processName);

                    } catch (Exception e) {
                        log.debug("Exception in masterCategoryResult value :" + e);
                    }
                }
        		}

                if (retailerId != null) {
                    Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId)
                            .singleResult();
                    String retailer = retailerObj.getName();
                    fieldValues.put("retailer", retailer);
                }

                // CHECKING FOR TECH EVAL FOR ADV EXCHENAGE
                // AND PICKNDROP(1WAY)
                /*
                 * if (fieldValues.containsKey(
                 * "natureOfComplaint")) { if
                 * (fieldValues.get("natureOfComplaint").
                 * equalsIgnoreCase("Advance Exchange") ||
                 * fieldValues.get("natureOfComplaint")
                 * .equalsIgnoreCase(
                 * "Pick And Drop (One Way)")) { if
                 * (fieldValues.containsKey(
                 * "TechEvalRequired") &&
                 * !(fieldValues.get("TechEvalRequired").
                 * isEmpty())) {
                 *
                 * } else { continue; } } }
                 */

                // UPDATING THE IMEI AND BARCODE INVENTORY
                if (fieldValues.containsKey("IMEINoOfStandByDevice")
                        && !(fieldValues.get("IMEINoOfStandByDevice").isEmpty())
                        && fieldValues.containsKey("barcodeForStandByDevice")
                        && !(fieldValues.get("barcodeForStandByDevice").isEmpty())) {
                    try {
                        // ImeiInventory imeiInventory1 =
                        // masterdataService.newImeiInventory();
                        MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                                .getDefaultProcessEngine())).getMasterdataService();
                        PackagingMaterialInventory packagingMaterialInventory = masterdataService1
                                .createPackagingMaterialInventoryQuery()
                                .barcode(fieldValues.get("barcodeForStandByDevice")).status(0)
                                .singleResult();
                        ImeiInventory imeiInventory = masterdataService.createImeiInventoryQuery()
                                .imeiNo(fieldValues.get("IMEINoOfStandByDevice")).status(0)
                                .singleResult();
                        if (imeiInventory != null && packagingMaterialInventory != null) {
                            packagingMaterialInventory.setStatus(1);
                            packagingMaterialInventory
                                    .setImeiNo(fieldValues.get("IMEINoOfStandByDevice"));
                            imeiInventory.setStatus(1);
                            imeiInventory.setBarcode(fieldValues.get("barcodeForStandByDevice"));
                            try {
//                                masterdataService
//                                        .savePackagingMaterialInventory(packagingMaterialInventory);
//                                masterdataService.saveImeiInventory(imeiInventory);
                            } catch (Exception e) {
                                log.debug("Error in updating Imei or Packaging Material Inventory"
                                        + e);
                                System.out.println(
                                        "Error in updating Imei or Packaging Inventory" + e);
                            }
                        } else {
                            log.debug("IMEI or Packaging Is not Present to issued");
                        }
                    } catch (Exception e) {
                        log.debug("Error in calling Imei or Packaging Inventory" + e);
                        System.out.println("Error in calling Imei or Packaging Inventory" + e);
                    }
                }
                try {
                    log.debug("currentDsVersion(dataset) : "
                            + metadataService.getCurrentDsVersion(dataset));
                    log.debug("dataset(dataset):" + dataset);
                    log.debug("dataset :" + processName);
                    ProcessDeployment deployment = metadataService.createProcessDeploymentQuery()
                            .dataset(dataset)
                            .dsVersion(metadataService.getCurrentDsVersion(dataset))
                            .name(processName).singleResult();
                    log.debug("deployment value :" + deployment);

                    ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
                            .deploymentId(deployment.getDeploymentId()).singleResult();

                    log.debug("definition : " + definition);
                    FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(),
                            false);
                    FormFillerUtil.GroupDescription groupDescription = null;
                    while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Current groupDescription:" + groupDescription);
                        }

                        if (groupDescription.isLastGroup()) {
                            break;
                        }
                    }

                    ProcessInstance processInstance = submitStartForm(formService, definition,
                            formFillerUtil);
                    System.out.println("processInstance.getProcessInstanceId() : "
                            + processInstance.getProcessInstanceId());
                    String processId = processInstance.getProcessInstanceId();
                    String taskId = "";

                    // RETRIVING TASKID FOR ASSIGNMENT

                    List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getTaskService().createTaskQuery()
                            .processInstanceId(processId).orderByTaskCreateTime().desc()
                            .list();
                    if (!tasks.isEmpty()) {
                        Task task = tasks.get(0);
                        taskId = task.getId();

                        log.debug("taskId : " + taskId);
                    }

                    RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getRuntimeService();
                    String ticketNo = String
                            .valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
                    // Setting Pincode while
                    // processing/uploading a ticket

                    if ((fieldValues.containsKey("pincode"))
                            && fieldValues.get("pincode") != null) {

                        try {
                            LocationPincodes locationPincodes = masterdataService
                                    .createLocationPincodesQuery()
                                    .pincode(fieldValues.get("pincode")).singleResult();

                            ProcessLocation processLocation = masterdataService
                                    .createProcessLocationQuery()
                                    .id(locationPincodes.getLocationId()).singleResult();

                            runtimeService.setVariable(processId, "rlReportingCity",
                                    processLocation.getCity());
                            log.debug(runtimeService.getVariable(processId, "rlReportingCity")
                                    .toString());

                        } catch (Exception e) {
                            log.debug("Error in putting value to reporting city" + e);
                        }

                        // TODO Setting a Date

                    }

                    if (fieldValues.containsKey("ticketPriority")
                            && (fieldValues.get("ticketPriority").isEmpty())) {
                        log.debug("inside Priority");
                        // String ticketPriority = String
                        // .valueOf(runtimeService.getVariable(processId,
                        // "ticketPriority"));
                        // if(ticketPriority==null ||
                        // ticketPriority =="")
                        // {
                        runtimeService.setVariable(processId, "ticketPriority", "Medium");
                    }
                    // }
                    // Manually Setting Assignee
                    if (fieldValues.containsKey("pincode")
                            && !(fieldValues.get("pincode").isEmpty()) && ticketNo.contains("-")
                            && !((ticketNo).isEmpty())) {
                        String pincode = fieldValues.get("pincode");
                        log.debug("Ticket No: " + ticketNo);
                        System.out.println("Ticket No: " + ticketNo);
                        MasterdataService masterdataService2 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                                .getDefaultProcessEngine())).getMasterdataService();
                        MasterdataService masterdataService3 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                                .getDefaultProcessEngine())).getMasterdataService();
                        // runtimeService.getVariable(processId,
                        // "taskId");3
                        // CoordinatorServicablePincodesQuery
                        // coordinatorServicablePincodes =
                        // masterdataService2.createCoordinatorServicablePincodesQuery().pincode(pincode);
                        // System.out.println("coordinatorServicablePincodes
                        // COUNT
                        // :"+coordinatorServicablePincodes.count());
                        // System.out.println("coordinatorServicablePincodes:
                        // "+coordinatorServicablePincodes);
                        // ArrayList al = new
                        // ArrayList<>(coordinatorServicablePincodes.list());
                        // System.out.println("ALOIU :
                        // "+al.get(0));
                        log.debug("taskId From GetVariable : "
                                + runtimeService.getVariable(processId, "taskId"));
                        try {
                            if (masterdataService2.createCoordinatorServicablePincodesQuery()
                                    .pincode(pincode) != null) {
                                List<CoordinatorServicablePincodes> list = masterdataService3
                                        .createCoordinatorServicablePincodesQuery().pincode(pincode)
                                        .list();
                                System.out
                                        .println("CoordinatorServicablePincodes : " + list.size());
                                log.debug("CoordinatorServicablePincodes list :" + list);
                                if (list.size() >= 1) {
                                    String coId = "";
                                    for (int i = 0; i < list.size(); i++) {
                                        MasterdataService masterdataService4 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                                                .getDefaultProcessEngine())).getMasterdataService();
                                        ProcessUser userList = masterdataService4
                                                .createProcessUserQuery()
                                                .email(list.get(i).getCoordinatorEmailId())
                                                .status(1).singleResult();
                                        if (userList != null) {
                                            coId = String.valueOf(userList.getEmail());
                                            log.debug("inside Assigmnent assignee is :" + coId);
                                            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                                                    .getDefaultProcessEngine())).getTaskService();

                                            taskService.setAssignee(taskId, coId);
                                            break;
                                        }
                                    }

                                }
                            }
                        } catch (Exception e) {
                            log.debug("multiple record are there fot this Pincode!" + e);
                        }
                    }
                    // Till Here manual assignment
                    // DateFormat dateFormat = new
                    // SimpleDateFormat("dd/MM/yyyy
                    // hh:mm:ss");
                    // Date date = new Date();

                    DateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date = new Date();
                    s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                    String tt = s.format(date);
                    DateTime dateTimeIndia = new DateTime(tt);
                    Date actDate = dateTimeIndia.toDate();
                    runtimeService.setVariable(processId, "ticketCreationDate", actDate);
                    log.debug("TicketCreation DATE" + String
                            .valueOf(runtimeService.getVariable(processId, "ticketCreationDate")));
                    System.out.println("ticketNo : " + ticketNo);
                    /**
        			 * prem created 21-03-2019 calling ReportPojo class to send
        			 * variables to report server with new thread
        			 */
        			new Thread(new Runnable() {
        				@Override
        				public void run() {
        					try {
        						ReportPojo pojo = new ReportPojo(processId);
        						pojo.sentToReportServer(pojo);
        						TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(processId), processId, "TICKET_CREATED");
        					} catch (Exception e) {
        						log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

        					}
        				}
        			}).start();

                    // Below Code For No Of Ticket COmpleted
                    // and
                    // Non Completed On A Date.
                    // try {
                    // insertIntoOpenTickets(ticketNo);
                    // } catch (Exception e) {
                    // System.out.println("Exception in
                    // Calling
                    // insertIntoOpenTickets() :" + e);
                    // }
                    retval.getTicketNo().add(ticketNo);


                } catch (Exception e) {
                    log.debug("Ecxeption in ProcessDeployment" + e);
                }
            } catch (Exception t) {
                log.debug("Ecxeption in Updating All" + t);
                ErrorObject errorObject = new ErrorObject();
                if (t != null && t.getMessage() != null && !t.getMessage().contains("Cell index")) {
                    errorObject.setRowNo(iteration);
                    errorObject.setMessage("Ignoring line " + iteration);
                    errorObject.setReason(t.getMessage());
                    retval.getErrorMessages().add(errorObject);
                }

            }

        } catch (Exception inEx) {
            ErrorObject errorObject = new ErrorObject();
            if (inEx != null && inEx.getMessage() != null
                    && !inEx.getMessage().contains("Cell index")) {
                errorObject.setRowNo(iteration);
                errorObject.setMessage("Ignoring line " + iteration);
                errorObject.setReason(inEx.getMessage());
                retval.getErrorMessages().add(errorObject);
            }
        }
        return retval;
    }

}
