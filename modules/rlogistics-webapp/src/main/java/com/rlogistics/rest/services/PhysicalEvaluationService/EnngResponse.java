package com.rlogistics.rest.services.PhysicalEvaluationService;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
class EnngResponse {
    @JsonProperty("question")
    private String question;

    @JsonProperty("question_type")
    private String question_type;

    @JsonProperty("selected_answer")
    private String selected_answer;

    @JsonProperty("answers")
    private String answers;


}

