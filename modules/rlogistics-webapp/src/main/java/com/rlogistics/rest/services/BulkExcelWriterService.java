package com.rlogistics.rest.services;

import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import com.rlogistics.rest.services.excelWriter.SaveExcelInHostUtil;
import com.rlogistics.rest.services.helper.ResponseForExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service to write physical eval data into excel
 * @author Adarsh
 */
@Service
@Primary
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Slf4j
public class BulkExcelWriterService {
    private SaveExcelInHostUtil saveExcelInHostUtil;

    private ReportExcelUtil excelUtil;

    @Autowired
    public BulkExcelWriterService(SaveExcelInHostUtil saveExcelInHostUtil, @Qualifier("bulkPhysicalEvalData") ReportExcelUtil excelUtil) {
        this.saveExcelInHostUtil = saveExcelInHostUtil;
        this.excelUtil = excelUtil;
    }

    /**
     * It creates Excel sheet from all the data
     * @param headerData values to be added as header in excel
     * @param data values to be added as values in excel
     * @return Url of the Excel
     * @throws IOException
     */
    public String creatExcel(LinkedHashMap<String, String> headerData, List<ResponseForExcel> data) throws IOException {
        headerData.forEach((key, value) -> log.debug(key + "---" + value));
        excelUtil.initExcel("Bulk-data");
        excelUtil.createHeader(headerData).setStyling();
        List<ResponseForExcel> filterImeiWithQandAnsData =
                data.stream()
                        .filter(a -> a.getProcessId().contains("__")) // check if __ is present or not
                        .map(a -> new ResponseForExcel(a.getProcessId().replaceAll("^[^_]*(__)", ""), a.getData())) //if present remove it
                        .collect(Collectors.toList()); //and then collect it to a new list

        filterImeiWithQandAnsData.forEach(
                a ->
                {
                    try {
                        excelUtil.insertData(a.getData(), a.getProcessId());
                    } catch (IOException e) {
                        log.debug("Error inside Excel creation :(" + e);
                    }

                }
        );


        byte[] excel =
                excelUtil
                        .saveExcel()
                        .getExcel();

        saveExcelInHostUtil.saveExcel("get", "bulk", "excel", "bulk-excel.xlsx", excel);
        return "server/attachment/download/get/bulk/excel";

    }
}
