package com.rlogistics.rest;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.logging.Handler;

import javax.servlet.http.HttpServletRequest;

import com.rlogistics.master.identity.TicketMisReport;
import com.rlogistics.master.identity.impl.cmd.CreateTicketMisReportQueryCmd;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.commons.mail.Email;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailUtil;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.sms.SMSSendCommand;

import pojo.ReportPojo;

public class RetailerApiCalls {
	private static Logger log = LoggerFactory.getLogger(RetailerApiCalls.class);
	private static String RECYCLE_DEVICE_REQUOTE = "https://api.recycledevice.com/bizlog/shipment/qc/update";
	private static String AMTRUST_URL = "https://webxpress.azure-api.net/api/pushTracking/PushTracking";
	private static String AMTRUST_HEADER_LSP = "BIZ";
	private static String AMTRUST_HEADER_CUSTOMER_ID = "C00177";
	private static String AMTRUST_HEADER_USER_NAME = "api.fmc@amtrustindia.onlogicloud.in";
	private static String AMTRUST_HEADER_PASSWORD = "Admin123!@#";
	private static String AMTRUST_HEADER_SUBSCRIPTION_KEY = "de39179c98c04d3ebb0ab6e6a6ce5841";
	// private static String
	// RECYCLE_DEVICE_REQUOTE="https://api.recycledevice.com/bizlog/shipment/get/quote-value";//old
	// one
	// test

	public static void assignToCoordinatorAfterHO(String ticketno, String processId, String str) {
		try {
			new java.util.Timer().schedule(new java.util.TimerTask() {
				@Override
				public void run() {
					try {

						String newTicketNo = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
								.getRuntimeService().getVariable(processId, "rlTicketNo").toString();
						if (str.equals("ASSIGN_TO_COORDINATOR_FROM_HO")) {
							org.json.JSONObject jsonObject = new org.json.JSONObject();
							jsonObject.put("processId", processId);
							jsonObject.put("rlTicketNo", newTicketNo);
							ReportPojo.updateReport(jsonObject);
						}
					} catch (Exception e) {
						log.debug("ERROR IN assignToCoordinatorAfterHO" + e);
					}
				}
			}, 10000);
		} catch (Exception e) {
			log.debug("ERROR IN assignToCoordinatorAfterHO" + e);
		}

	}
	// activiti call buy back

	public static void sendStatusToRetailer(String rlProcessId, String number) {
		saveToConsoleLogsTable("BUYBACK STATUS", rlProcessId + " Number : " + number);
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
			TicketStatusHistoryClass.insertIntoHistoryTable(variables, rlProcessId, number);
			// 2 means PICK UP DONE
			if (number.equals("2")) {
				try {
					runtimeService.setVariable(rlProcessId, "rlPickUpConfirmation", "true");

				} catch (Exception e) {

				}
			}

		} catch (Exception e) {
			log.debug(e + "");
		}
		callToCashifyApiForSendingStatus(rlProcessId, number);
		callToRecycleDeviceApiForSendingStatus(rlProcessId, number);
	}

	public static void assigneToOtherLocation(String ticketno, String rlProcessId) {
		saveToConsoleLogsTable("RetailerApiCall.assigneToOtherLocation()", rlProcessId + " Number : ");
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			String location1 = "";
			String location2 = " ";
			Map<String, Object> map = runtimeService.getVariables(rlProcessId);
			if (map.containsKey("pincode")) {
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();
				LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
						.pincode(map.get("pincode").toString()).singleResult();
				location1 = locationPincodes.getLocationId();
				// &&map.get("rlServiceCode").toString().equals("PICKNDROPONEWAY")
				// IF BUYBACK AND ADVANCEEXCHANGE THEN APPLY THIS CONDITION
				if (map.containsKey("dropLocPincode")) {
					LocationPincodes locationPincodes2 = masterdataService.createLocationPincodesQuery()
							.pincode(map.get("dropLocPincode").toString()).singleResult();
					location2 = locationPincodes2.getLocationId();
				} else {
					LocationPincodes locationPincodes2 = masterdataService.createLocationPincodesQuery()
							.pincode(map.get("rlReturnLocationPincode").toString()).singleResult();
					location2 = locationPincodes2.getLocationId();
				}

				if (location1.equals(location2)) {

					// runtimeService.setVariable(rlProcessId,
					// "itp_IC_DropLocWithIn", "true");
					// runtimeService.setVariable(rlProcessId,
					// "rlDropLocWithinCity", "Yes");
					map.put("rlDropLocWithinCity", "true");

				} else {
					// runtimeService.setVariable(rlProcessId,
					// "itp_IC_DropLocWithIn", "false");
					// runtimeService.setVariable(rlProcessId,
					// "rlDropLocWithinCity", "No");
					map.put("rlDropLocWithinCity", "false");

				}
				runtimeService.setVariables(rlProcessId, map);

			}

		} catch (Exception e) {
			log.debug("ERROR IN assigneToOtherLocation(String ticketno,String rlProcessId) " + e);
		}

	}

	// activiti call
	public static void sendStatusToRetailerPND1W(String rlProcessId, String statusString) {
		log.debug("sendStatusToRetailerPND1W");
		saveToConsoleLogsTable("Pick And Drop (One Way)", statusString + " " + rlProcessId);
		Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService().getVariables(rlProcessId);
		if (statusString.equals("PROBLEM_SOLVED_DURING_INITIAL_CALL_TICKET_CLOSED")||statusString.equals("CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL")
				||statusString.equals("PICKUP_REJECTED_TO_FE_BY_CUSTOMER"))
 {
			try {
				org.json.JSONObject jsonObject = new org.json.JSONObject();
				jsonObject.put("processId", rlProcessId);
				jsonObject.put("currentStatus", statusString);
				jsonObject.put("closedDate", ReportPojo.getCurrentDate());
				jsonObject.put("closedTime", ReportPojo.getCurrentTime());
				 ReportPojo.updateReport(jsonObject);
			} catch (Exception e) {
				log.debug("RetailerAPICALL.sendStatusToRetailerPND1W(): " + e);
			}
		}

		callToMedWellApiForSendingStatus(rlProcessId, statusString);

		new Thread(new Runnable() {
			@Override
			public void run() {
				TicketStatusHistoryClass.insertIntoHistoryTable(variables, rlProcessId, statusString);
				callToOneAssistPnd1Way(rlProcessId, statusString);
				amTrustAPICall(variables, rlProcessId, statusString);
			}
		}).start();
		try {
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			Map<String, Object> map = runtimeService.getVariables(rlProcessId);
			switch (statusString) {
			case "PRODUCT_PICKED_UP_FROM_CUSTOMER":
				SMSSendCommand command = new com.rlogistics.sms.SMSSendCommand();
				command.setServiceId(map.get("rlServiceId").toString());
				command.setRetailerId(map.get("rlRetailerId").toString());
				command.setSmsEmailTriggerCode("COMMON_PICKUP_COMPLETED");
				com.rlogistics.sms.SMSUtil.sendSMS(command, rlProcessId);

			case "FE_SCHEDULED_FOR_PICKUP":
				SMSSendCommand command1 = new com.rlogistics.sms.SMSSendCommand();
				command1.setServiceId(map.get("rlServiceId").toString());
				command1.setRetailerId(map.get("rlRetailerId").toString());
				command1.setSmsEmailTriggerCode("COMMON_CONFIRMED_APPNMT");
				com.rlogistics.sms.SMSUtil.sendSMS(command1, rlProcessId);
			}
		} catch (Exception e) {
			log.debug("ERROE" + e);
		}

	}

	// activiti call
	public static void sendStatusToRetailerAdvanceExchange(String ticketNo, String rlProcessId, String statusConstant) {
		callToSavexApiForSendingStatus(ticketNo, rlProcessId, statusConstant);
		saveToConsoleLogsTable("ADVANCE EXCHANGE", statusConstant + "  " + rlProcessId);
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
		new Thread(new Runnable() {
			@Override
			public void run() {
				TicketStatusHistoryClass.insertIntoHistoryTable(variables, rlProcessId, statusConstant);
				//callToSavexApiForSendingBarCode(variables, ticketNo, rlProcessId, statusConstant);
			}

		}).start();
	}

	// activiti call
	public static void closedTicket(String processId, String reason) {
		log.debug("closedTicket()-----" + processId + " " + reason);
		saveToConsoleLogsTable("closedTicket()", processId + " " + reason);

		try {
			// Map<String, Object> variables = ((RLogisticsProcessEngineImpl)
			// (ProcessEngines.getDefaultProcessEngine()))
			// .getRuntimeService().getVariables(processId);

			org.json.JSONObject jsonObject = new org.json.JSONObject();
			jsonObject.put("processId", processId);
			jsonObject.put("currentStatus", reason);
			jsonObject.put("closedReason", reason);
			jsonObject.put("closedDate", ReportPojo.getCurrentDate());
			jsonObject.put("closedTime", ReportPojo.getCurrentTime());
			ReportPojo.updateReport(jsonObject);
		} catch (Exception e) {
			log.debug(e + "closedTicket()");

		}
	}

	// Abinash activiti call to update Franchise Data

	public static void updateAgencyDataForPickup(String processId, String pickupAgencyName, String pickupFeType, String pickupFeName){
		log.debug("=======================Process Id : " + processId + " Pickup Agency Name: "
				+ pickupAgencyName +" ====================" + "Pickup Fe Type " + pickupFeType + " Pickup Fe Name : " + pickupFeName);

		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("processId", processId);
			jsonObject.put("agencyNameForPickup", pickupAgencyName);
			jsonObject.put("feTypeForPickup", pickupFeType);
			jsonObject.put("franchiseFeForPickup", pickupFeName);
			log.debug("========================= "  +jsonObject + "=======================");
			ReportPojo.updateReport(jsonObject);

//			CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
//					.getProcessEngineConfiguration())).getCommandExecutor();
//			TicketMisReport.TicketMisReportQuery ticketMisReportQuery = commandExecutor
//					.execute(new CreateTicketMisReportQueryCmd());
//			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//					.getDefaultProcessEngine())).getMasterdataService();
//			TicketMisReport ticketMisReport = masterdataService.newTicketMisReport();
//			ticketMisReport = masterdataService.createTicketMisReportQuery().processId(processId).singleResult();
//			TicketMisReport ticketMisReport = ticketMisReportQuery.processId(processId).singleResult();
//			log.debug("====================== " + ticketMisReport + " ==============================");
//			ticketMisReport.setFranchiseFe(franchiseFe);
//			log.debug("====================== " + franchiseFe + " ==============================");
//			ticketMisReport.setAgencyName(agencyName);
//			masterdataService.saveTicketMisReport(ticketMisReport);

		} catch (Exception e){
			log.debug("Exception During Franchise Update");
			log.debug(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void updateAgencyDataForDrop(String processId, String dropAgencyName, String dropFeType, String dropFeName){
		log.debug("=======================Process Id : " + processId + " Drop Agency Name: "
				+ dropAgencyName +" ====================" + "Drop Fe Type " + dropFeType + " Drop Fe Name : " + dropFeName);

		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("processId", processId);
			jsonObject.put("agencyNameForDrop", dropAgencyName);
			jsonObject.put("feTypeForDrop", dropFeType);
			jsonObject.put("franchiseFeForDrop", dropFeName);
			log.debug("========================= "  +jsonObject + "=======================");
			ReportPojo.updateReport(jsonObject);

//			CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
//					.getProcessEngineConfiguration())).getCommandExecutor();
//			TicketMisReport.TicketMisReportQuery ticketMisReportQuery = commandExecutor
//					.execute(new CreateTicketMisReportQueryCmd());
//			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//					.getDefaultProcessEngine())).getMasterdataService();
//			TicketMisReport ticketMisReport = masterdataService.newTicketMisReport();
//			ticketMisReport = masterdataService.createTicketMisReportQuery().processId(processId).singleResult();
//			TicketMisReport ticketMisReport = ticketMisReportQuery.processId(processId).singleResult();
//			log.debug("====================== " + ticketMisReport + " ==============================");
//			ticketMisReport.setFranchiseFe(franchiseFe);
//			log.debug("====================== " + franchiseFe + " ==============================");
//			ticketMisReport.setAgencyName(agencyName);
//			masterdataService.saveTicketMisReport(ticketMisReport);

		} catch (Exception e){
			log.debug("Exception During Franchise Update");
			log.debug(e.getMessage());
			e.printStackTrace();
		}
	}


	private static void callToRecycleDeviceApiForSendingStatus(String rlProcessId, String number) {

		String status = "";
		String remark = "";

		String RECYCLE_DEVIEC_STATUS_UPDATE = "https://api.recycledevice.com/bizlog/shipment/qc/update"; // PROD

		switch (number) {
		case "1":
			remark = "PICKUP_INITIATED";
			status = "BZITTKSTS-1001";
			break;
		case "2":
			remark = "PICKUP_COMPLETE";
			status = "BZITTKSTS-1002";
			break;
		case "3":
			remark = "PICKUP_RESCHEDULE";
			status = "BZITTKSTS-1003";
			break;

		case "4":
			remark = "PICKUP_CANCEL";
			status = "BZITTKSTS-1004";
			break;
		}

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();

		try {
			Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
			String ticketNo = String.valueOf(variables.get("rlTicketNo"));

			String sbiStringTicketNo = ticketNo.substring(0, 3);

			if (sbiStringTicketNo.equals("RCB")) {

				JSONObject requestJson = new JSONObject();
				requestJson.put("ticket_number", ticketNo);
				requestJson.put("username", RetailersResource.RECYCLE_DEVICE_USER_NAME);
				requestJson.put("password", RetailersResource.RECYCLE_DEVICE_PASSWORD);
				requestJson.put("ticket_status", remark);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(RECYCLE_DEVIEC_STATUS_UPDATE);
				httppost.addHeader("Content-Type", "application/json");
				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				RetailerApiCalls.saveToConsoleLogsTable(ticketNo,
						"STATUS UPDATE: REQUEST:" + requestJson + "RESPONSE:" + rawResponse);
			} else {
				// Ticket is not for Recycle device Retailer
			}

		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
		}

	}

	private static void callToSavexApiForSendingBarCode(Map<String, Object> variables, String ticketNo,
			String rlProcessId, String statusConstant) {
		String final_url = "http://49.248.166.228/API/Bizlog/Track_Barcode.ashx";

		String logs = "";
		String barCode = "0";// FE_OUT_FOR_PICKUP
		try {
			barCode = variables.get("rlCoverBarcode").toString();
			String sbiStringTicketNo = ticketNo.substring(0, 3);

			if (sbiStringTicketNo.equals("SVX") && statusConstant.equals("FE_OUT_FOR_PICKUP")) {
				JSONObject requestJson = new JSONObject();
				requestJson.put("ticketNo", ticketNo);
				requestJson.put("barCode", barCode);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(final_url);
				httppost.addHeader("Content-Type", "application/json");

				log.debug("Savax status from server :".toUpperCase() + requestJson);

				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);

				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug("Savex status from server :".toUpperCase() + rawResponse);
				logs = "SAVEX API CALL FOR BARCODE REQ: " + requestJson.toString() + " RES: " + rawResponse;
			} else {
				// Ticket is not for Savex Retailer
			}

		} catch (Exception e) {
			logs = "SAVEX API CALL FOR BARCODE ERROR " + e + " LOG: " + log;
		}
		saveToConsoleLogsTable("SAVEX BARCODE API CALL", logs);
	}

	private static void callToSavexApiForSendingStatus(String ticketNo_, String rlProcessId, String statusString) {

		try {

			log.debug("Savex Server call logs------------------------------------------ " + rlProcessId + " "
					+ statusString);
			String status = statusString;
			String comment = "";
			String _URL = "http://49.248.166.228/API/Bizlog/Track_Order.ashx";

			switch (statusString) {
			case "DELIVERED":
				status = "Product Delivered.";

				break;
			case "CUSTOMER_REJECTION":
				status = "Product is Rejected by Customer.";
				break;
			case "FE_REJECTION":
				status = "Rejected by Field Engineer or Delivery Boy.";
				break;
			case "CUSTOMER_RESCHEDULE":
				status = "Reschedule by Customer.";
				break;
			case "PRODUCT_REACHED_TO_HUB":
				status = "Product Reached at Hub.";
				break;
			default:
				log.debug("Savex Server call logs------------------------------------------ " + rlProcessId + " "
						+ statusString);
				break;

			}

			/*
			 * RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
			 * (ProcessEngines.getDefaultProcessEngine())) .getRuntimeService();
			 * Map<String, Object> variables =
			 * runtimeService.getVariables(rlProcessId); String ticketNo =
			 * String.valueOf(variables.get("rlTicketNo"));
			 */

			String final_url = _URL;

			String sbiStringTicketNo = ticketNo_.substring(0, 3);
			/**
			 * Call Api when the ticket is from Savex Retailer with Code : MDL
			 */
			if (sbiStringTicketNo.equals("SVX")) {

				JSONObject requestJson = new JSONObject();
				requestJson.put("Ticket_No", ticketNo_);
				requestJson.put("Status", status);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(final_url);
				httppost.addHeader("Content-Type", "application/json");

				log.debug("Savax status from server :".toUpperCase() + requestJson);

				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);

				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug("Savex status from server :".toUpperCase() + rawResponse);
			} else {
				// Ticket is not for Savex Retailer
			}

		} catch (Exception e) {
			log.debug("NOT ABLE TO CALL MEDWELL API " + e);
		}

	}

	private static void callToMedWellApiForSendingStatus(String rlProcessId, String statusString) {
		String ticketNo = "";
		String status = "";
		String comment = "";
		String reScheduleDateTime = "";
		String finalScheduleTime = "";

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(rlProcessId);

		try {
			ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));
		} catch (Exception e) {
		}
		try {
			comment = String.valueOf(runtimeService.getVariable(rlProcessId, "itp_MD_Comments"));
		} catch (Exception e) {
		}
		try {
			reScheduleDateTime = String.valueOf(runtimeService.getVariable(rlProcessId, "rlAppointmentDateForFV"));
		} catch (Exception e) {
		}

		try {
			// String _URL =
			// "https://qa1-api.nightingales.in/v2/wms/shipments/";
			// String _URL =
			// "https://kube-api.nightingales.in/v2/wms/shipments/";
			String _URL = "https://api.nightingales.in/v2/wms/shipments/";// prod
																			// url
			// change URL in RLAttachments also

			switch (statusString) {
			case "DROP_ASSIGNED":
				comment = "Asset is out for delivery.";
				status = "Out for Delivery";

				break;
			case "FE_BACK_TO_WAREHOUSE_WITH_PRODUCT_CUSTOMER_REJECT_TO_FE_WHILE_DROPPING":
				comment = "Customer is rejected.Back to ware house";
				status = "Not Delivered";

				break;
			case "DELIVERED":
				comment = "Product delivered.";
				status = "Delivered";

				break;
			case "RESCHEDULE":
				comment = "Rescheduled";
				status = "In-Transit";

				break;
			// case "FE_SCHEDULED_FOR_PICKUP":
			// comment = "Rescheduled";
			// status = "Re-scheduled";
			// finalScheduleTime=reScheduleDateTime;
			//
			// break;
			case "COORDINATOR_RE_SCHEDULED_FE_FOR_PICKUP":
				comment = "Rescheduled";
				status = "Re-scheduled";
				finalScheduleTime = reScheduleDateTime;

				break;
			// case "CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL":
			// comment = "Customer Rejected at initial call";
			// status = "Not Delivered";
			// break;

			default:
				comment = "";
				status = "";
				log.debug("Medwell Server call logs------------------------------------------ " + rlProcessId + " "
						+ statusString);
				break;

			}
			String final_url = _URL + ticketNo;
			String sbiStringTicketNo = ticketNo.substring(0, 3);
			if (sbiStringTicketNo.equals("MDL") && !status.equals("")) {

				JSONObject requestJson = getMedwellJson(status, comment, finalScheduleTime);
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPut httppost = new HttpPut(final_url);
				httppost.addHeader("Content-Type", "application/json");
				// Change value at RLAttachment ALso
				httppost.addHeader("nhhs-client-id", "6EF55494FD9EFCBFEB96C2462EFDD");// medwel
																						// //
																						// prod
				httppost.addHeader("nhhs-client-secret", "z3rcXP8YUnzwZyi5awIlTKeOtFSz2ACt");// medwell
				saveToConsoleLogsTable(ticketNo, statusString + ":" + "Request: " + final_url + " " + requestJson);
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				saveToConsoleLogsTable(ticketNo, statusString + ":" + "Response: " + rawResponse);
				sendEmailToMedwell(status, variables);

			} else {
				// Ticket is not for Medwell Retailer
			}

		} catch (Exception e) {
			log.debug("NOT ABLE TO CALL MEDWELL API " + e);

		}

	}

	private static void sendEmailToMedwell(String status, Map<String, Object> variables) {
		String sub = " Appointment for Ticket number : " + variables.get("rlTicketNo").toString();
		String date = " Time not selected";
		try {
			if (status.equals("Out for Delivery")) {
				date = variables.get("rlAppointmentDateForCalc").toString();
				String mailBody = "Hi " + variables.get("consumerName").toString() + ", A ticket "
						+ variables.get("rlTicketNo").toString()
						+ " is Out for Delivery and our executive will visit on : " + date
						+ " for drop, the product with us is " + variables.get("product").toString();
				new Thread(new Runnable() {

					@Override
					public void run() {
						ArrayList<String> emailArray = new ArrayList<>();
						emailArray.add("anmol.meghanadan@nightingales.in");
						emailArray.add("prem.nath@bizlog.in");
						for (int i = 0; i < emailArray.size(); i++) {
							try {
								EmailUtil.sendMail(emailArray.get(i), sub, mailBody);
							} catch (Exception e) {
								log.debug("" + e);
								RetailerApiCalls.saveToConsoleLogsTable("anmol.meghanadan@nightingales.in",
										"ERROR WHILE EMAIL SENT:" + e);
							}
						}
					}
				}).start();
			}
		} catch (Exception e) {
			RetailerApiCalls.saveToConsoleLogsTable("anmol.meghanadan@nightingales.in", "ERROR WHILE EMAIL SENT:" + e);
		}
	}

	private static void callToOneAssistPnd1Way(String rlProcessId, String statusString) {
		String HOST = "";
		String PATNER_CODE = "";
		String BASIC_AUTH = "";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
				HOST = "webservice.oneassist.in";// Prod
				PATNER_CODE = "536";// Prod
				BASIC_AUTH = "dW5pdGUtd3M6c0JHUGVTR1c1UHRiQzVlbg==";// Prod
			} else {
				HOST = "sit4admin.1atesting.in";// test
				PATNER_CODE = "341";// test
				BASIC_AUTH = "dW5pdGUtd3M6dW5pdGUtMTIz";// test
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		// Prod Base Url (HostName): https://webservice.oneassist.in
		// Test Base Url (HostName): https://sit4admin.1atesting.in
		// String _URL =
		// "/serviceplatform/api/shipments/tracking?partnerCode=303";// test

		// String HOST="webservice.oneassist.in";//prod

		String ticketNo = "";
		String orderNumber = "";
		String retailerId = "";
		String statusMessage = "";
		String statusCode = "";

		String localStatus = "";
		String localCode = "";

		switch (statusString) {
		case "DELIVERED":
			localStatus = "PRODUCT_DROPPED";
			localCode = "BZITTKSTS-1007";
			break;
		case "CUSTOMER_REJECTION":
			localStatus = "CUSTOMER_REJECT_WHILE_DROP";
			localCode = "BZITTKSTS-1015";
			break;
		case "RESCHEDULE":
			localStatus = "CUSTOMER_NEED_RESCHEDULE_WHILE_DROP";
			localCode = "BZITTKSTS-1014";
			break;
		case "DROP_ASSIGNED":
			localStatus = "TICKET_ASSIGNED_FOR_DROP";
			localCode = "BZITTKSTS-1006";
			break;
		case "CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL":
			localStatus = "CUSTOMER_REJECTED_TO_COORDINATOR";
			localCode = "BZITTKSTS-1012";
			break;
		case "FE_SCHEDULED_FOR_PICKUP":
			localStatus = "TICKET_ASSIGNED_TO_FE_FOR_PICKUP";
			localCode = "BZITTKSTS-1003";
			break;
		// case "COORDINATOR_RE_SCHEDULED_FE_FOR_PICKUP":
		// localStatus = "";
		// localCode = "";
		// break;
		// case
		// "FE_BACK_TO_WAREHOUSE_WITH_PRODUCT_CUSTOMER_REJECT_TO_FE_WHILE_DROPPING":
		// localStatus = "";
		// localCode = "";
		// break;
		case "PRODUCT_PICKED_UP_FROM_CUSTOMER":
			localStatus = "PRODUCT_PICKED_FROM_CUSTOMER";
			localCode = "BZITTKSTS-1004";
			break;
		case "PICKUP_REJECTED_TO_FE_BY_CUSTOMER":
			localStatus = "CUSTOMER_REJECTED";
			localCode = "BZITTKSTS-1008";
			break;
		case "PICKUP_RESCHEDULED_TO_FE_BY_CUSTOMER":
			localStatus = "CUSTOMER_NEEDS_RESCHEDULING";
			localCode = "BZITTKSTS-1009";
			break;
		default:
			localStatus = statusString;
			localCode = "";
			break;

		}

		statusMessage = localStatus;
		statusCode = localCode;

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();

		try {
			ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));
		} catch (Exception e) {
		}

		try {
			retailerId = String.valueOf(runtimeService.getVariable(rlProcessId, "rlRetailerId"));
		} catch (Exception e) {
		}

		try {

			String PATH = "/serviceplatform/api/shipments/tracking";// test

			String sbiStringTicketNo = ticketNo.split("-")[0];
			if (sbiStringTicketNo.equals("OA")) {
				URIBuilder builder = new URIBuilder();
				builder.setScheme("https").setHost(HOST).setPath(PATH).setParameter("partnerCode", PATNER_CODE);
				URI uri = builder.build();

				orderNumber = String.valueOf(runtimeService.getVariable(rlProcessId, "orderNumber"));

				JSONObject requestJson = getOneAssistJson(ticketNo, statusCode, statusMessage, orderNumber);
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(uri);
				httppost.addHeader("Content-Type", "application/json");
				httppost.addHeader("Authorization", "Basic " + BASIC_AUTH);//
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				saveToConsoleLogsTable(retailerId, " Status sent to OneAssist " + ticketNo + " " + statusString
						+ " Request: " + uri.toString() + " " + requestJson + ":" + "Response: " + rawResponse);

			} else {
				// Ticket is not for Medwell Retailer
			}

		} catch (Exception e) {
			log.debug("NOT ABLE TO CALL ONEASSIST API " + e);
			saveToConsoleLogsTable(retailerId, ticketNo + " NOT ABLE TO CALL ONEASSIST API:" + e);

		}

	}

	private static JSONObject getOneAssistJson(String ticketNo, String statusCode, String statusMessage,
			String orderNumber) throws JSONException {
		JSONObject requestJson = new JSONObject();
		requestJson.put("awb", ticketNo);
		requestJson.put("datetime", getCurrentIstTimestamp());
		requestJson.put("reason_code", statusCode);
		requestJson.put("reason_code_number", statusMessage);
		requestJson.put("order_number", orderNumber);
		return requestJson;

	}

	private static JSONObject getMedwellJson(String status, String comment, String finalreScheduleDateTime)
			throws JSONException {

		JSONObject requestJson = new JSONObject();
		requestJson.put("status", status);
		requestJson.put("comment", comment);
		if (!finalreScheduleDateTime.equals("")) {
			requestJson.put("rescheduled_time", finalreScheduleDateTime);
		}
		return requestJson;
	}

	private static void callToCashifyApiForSendingStatus(String rlProcessId, String number) {

		log.debug("Cashify Server call logs------------------------------------------ " + rlProcessId + " " + number);
		String status = "";
		String remark = "";
		// String CASHIFY_URL =
		// "http://logistics-integ.api.stage.cashify.in:84/v1/status/callback/csh_bizlog";
		// // cashify
		String CASHIFY_URL = "https://lg3p1.api.cashify.in/ns/v1/status/callback/csh_bizlog"; // PROD
																								// cashify

		switch (number) {
		case "1":
			remark = "PICKUP_INITIATED";
			status = "BZITTKSTS-1001";
			break;
		case "2":
			remark = "PICKUP_COMPLETE";
			status = "BZITTKSTS-1002";
			break;
		case "3":
			remark = "PICKUP_RESCHEDULE";
			status = "BZITTKSTS-1003";
			break;

		case "4":
			remark = "PICKUP_CANCEL";
			status = "BZITTKSTS-1004";
			break;
		}

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();

		try {
			Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
			String ticketNo = String.valueOf(variables.get("rlTicketNo"));

			String sbiStringTicketNo = ticketNo.substring(0, 3);
			/**
			 * Call Api when the ticket is from CASHIFY Retailer with Code : CFY
			 */
			if (sbiStringTicketNo.equals("CFY")) {

				JSONObject requestJson = new JSONObject();
				requestJson.put("awb", ticketNo);
				requestJson.put("st", status);
				requestJson.put("rm", remark);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(CASHIFY_URL);
				httppost.addHeader("Content-Type", "application/json");
				System.out.println(" Cashify status Request :" + requestJson);
				log.debug("Cashify status from server :" + requestJson);

				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);

				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug("Cashify status from server :" + rawResponse);
				System.out.println("Response from server :" + rawResponse);
			} else {
				// Ticket is not for Cashify Retailer
			}

		} catch (Exception e) {
			log.debug("Exception occured " + e.getMessage());
		}
	}

	public static void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}

	/**
	 * This is calling from ACTIVITI
	 * 
	 * @param ticketNo
	 * @param processId
	 */
	public static void apiCallForRequote(String ticketNo, String processId) {
		try {

		} catch (Exception e) {
		}

	}

	/**
	 * NOT IN USE
	 * 
	 * @param processId
	 * @return
	 * @throws Exception
	 */
	public static boolean callRecyclerDeviceAPI(String processId) throws Exception {
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		String ticketNo = runtimeService.getVariable(processId, "rlTicketNo").toString();
		boolean value = false;

		ArrayList<Map<String, Object>> egnrResponse = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String url = "http://www.validata.in/admin/user_answers_to_questions?ticket_id=" + processId;
		HttpGet httpget = new HttpGet(url);

		httpget.addHeader("Content-Type", "application/json");

		CloseableHttpResponse response = httpclient.execute(httpget);
		String rawResponse = EntityUtils.toString(response.getEntity());
		log.debug("Response from server :" + rawResponse);

		// if (response.getStatusLine().getStatusCode() == 200) {
		JSONObject rawResponcejsonObject = new JSONObject(rawResponse);
		org.json.JSONArray rawResponceJsonArray = rawResponcejsonObject.getJSONArray("engineer_response");
		// org.json.JSONArray array1 = new org.json.JSONArray();

		// }

		// saveToConsoleLogsTable("VALIDATA API CALL", url + "::" +
		// rawResponse);

		String sbiStringTicketNo = ticketNo.substring(0, 3);

		if (sbiStringTicketNo.equals("RCB")) {
			try {
				JSONObject requestJson = new JSONObject();
				requestJson.put("username", "rdbiz_user");
				requestJson.put("password", "Orange_2829");
				requestJson.put("ticket_number", ticketNo);
				requestJson.put("evaluation_result", rawResponceJsonArray);

				// API call
				CloseableHttpClient httpclient1 = HttpClients.createDefault();

				HttpPost httppost = new HttpPost(RECYCLE_DEVICE_REQUOTE);
				httppost.addHeader("Content-Type", "application/json");
				// Set HTTP entity
				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				// Get Response
				CloseableHttpResponse httpResponse = httpclient1.execute(httppost);

				String rawResponse1 = EntityUtils.toString(httpResponse.getEntity());
				saveToConsoleLogsTable("RCB API CALL FOR GET REQOUTE VALUE--" + ticketNo,
						"REQUEST:" + requestJson + " RESPONSE:" + rawResponse1);

				TaskResource resource = new TaskResource();// for testing

				JSONObject jsonObject = new JSONObject(rawResponse1);
				if (!jsonObject.getBoolean("error")) {
					String reQuotePrice = jsonObject.getString("quote_value");
					// resource.updateProductCost(processId, reQuotePrice);
					value = true;
					resource.updateProductCost(processId, reQuotePrice);
					// try {
					// callRecyclerDeviceAPIForSetValue(requestJson, ticketNo);
					//
					// } catch (Exception e) {
					//
					// }
				}
			} catch (Exception e) {
				// resource.updateProductCost(processId, "9999");
				value = false;
				saveToConsoleLogsTable("RCB API CALL FAILD ", "Exception :" + e);

			}

		} else {
			saveToConsoleLogsTable("RCB API CALL FAIL" + ticketNo, "NOT RCB TICKET");

		}
		return value;
	}

	private static void callRecyclerDeviceAPIForSetValue(JSONObject requestJson, String ticketNo) throws Exception {
		CloseableHttpClient httpclient1 = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://api.recycledevice.com/bizlog/shipment/set/quote-value");
		httppost.addHeader("Content-Type", "application/json");
		// Set HTTP entity
		httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
		// Get Response
		CloseableHttpResponse httpResponse = httpclient1.execute(httppost);
		String rawResponse1 = EntityUtils.toString(httpResponse.getEntity());
		saveToConsoleLogsTable("RCB API CALL SET VALUE--" + ticketNo,
				"REQUEST:" + requestJson.toString() + " RESPONSE:" + rawResponse1);

	}

	public static void buyBackCallForOTPForPickup(String ticketNo, String processId) {
		saveToConsoleLogsTable("OTP", "buyBackCallForOTPForPickup(String ticketNo, String processId)");

		try {

		} catch (Exception e) {
		}

	}

	public static String getCurrentIstTimestamp() {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	public static String getCurrentDateSQLFormatForAmtrust() {
		SimpleDateFormat sd = new SimpleDateFormat("dd MMM yyyy HH:mm");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	public static String getCurrentDateSQLFormat() {
		SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	public static String getCurrentDate() {
		SimpleDateFormat sd = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	public static String getCurrentTime() {
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss");// 24 hours
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	private static void amTrustAPICall(Map<String, Object> variables, String processId, String statusString) {

		log.debug("amTrustAPICall Server call logs------------------------------------------ " + statusString + " "
				+ processId + " " + variables.get("rlServiceCode").toString());
		String status = "";
		int statusCode = 0;
		int deliveryStatus = 0;
		String remark = "";
		String rawResponse = "";

		switch (variables.get("rlServiceCode").toString()) {
		case "PICKNDROPONEWAY":
			switch (statusString) {
			case "DELIVERED":
				remark = "Product Delivered to Drop Location";
				status = "Delivered";
				statusCode = 4;
				deliveryStatus = 2;
				break;
			case "CUSTOMER_REJECTION":
				remark = "Customer Cancel the Service While Drop";
				status = "Cancelled";
				statusCode = 13;
				deliveryStatus = 1;
				break;
			// case "RESCHEDULE":
			// remark = "Rescheduled from Customer While Drop";
			// break;
			case "DROP_ASSIGNED":
				remark = "Field Engineer out for Delivery";
				status = "Out For Delivery";
				statusCode = 3;
				deliveryStatus = 1;
				break;
			case "CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL":
				remark = "Customer Reject at Initail Call";
				status = "Cancelled";
				statusCode = 13;
				deliveryStatus = 1;
				break;
			case "FE_SCHEDULED_FOR_PICKUP":
				remark = "Field Engineer out for Pickup";
				status = "Out For Pickup";
				statusCode = 14;
				break;
			case "COORDINATOR_RE_SCHEDULED_FE_FOR_PICKUP":
				status = "BookingConfirmed";
				statusCode = 23;
				deliveryStatus = 1;
				break;
			// this status will not come in the case of Amtrust
			// case
			// "FE_BACK_TO_WAREHOUSE_WITH_PRODUCT_CUSTOMER_REJECT_TO_FE_WHILE_DROPPING":
			// remark = "Product Back to Ware house after customer rejection";
			// status = "RTO Delivered";
			// statusCode = 19;
			// deliveryStatus = 1;
			// break;
			case "PRODUCT_PICKED_UP_FROM_CUSTOMER":
				remark = "Product Picked up";
				status = "InTransit";
				statusCode = 2;
				deliveryStatus = 1;
				break;
			case "PICKUP_REJECTED_TO_FE_BY_CUSTOMER":
				remark = "Product Cancel by Customer While Pickup";
				status = "Cancelled";
				statusCode = 13;
				deliveryStatus = 1;
				break;
			case "PICKUP_RESCHEDULED_TO_FE_BY_CUSTOMER":
				status = "BookingConfirmed";
				statusCode = 23;
				deliveryStatus = 1;

				break;
			case "TICKET_ASSIGNED_TO_FE_FOR_PICKUP":
				remark = "Appointment Fixed";
				status = "Booked";
				statusCode = 1;
				deliveryStatus = 1;
				break;
			case "LSP_INITIATED_TO_DROP_PICKUP_PRODUCT_AT_HUB":
				remark = "In-transit for pickup location";
				status = "InTransit";
				statusCode = 2;
				deliveryStatus = 1;
				break;
			case "LSP_INITIATED_TO_DROP_OLD_PRODUCT_TO_DROPPED_LOCATION":
				remark = "In-transit for drop location";
				status = "InTransit";
				statusCode = 2;
				deliveryStatus = 1;
				break;
			case "PROBLEM_SLOVE_DURING_INITIAL_CALL_TICKET_CLOSED":
				remark = "Problem Solve During Initial Call.Ticket Closed";
				status = "Cancelled";
				statusCode = 13;
				deliveryStatus = 1;
				break;
			}
			break;
		}
		try {
			String ticketNo = variables.get("rlTicketNo").toString();
			String location = "";
			if (variables.containsKey("rlLocationName"))
				location = variables.get("rlLocationName").toString();
			String customerName = "";
			if (variables.containsKey("consumerName"))
				customerName = variables.get("consumerName").toString();
			String orderNo = "";
			if (variables.containsKey("orderNumber"))
				orderNo = variables.get("orderNumber").toString();
			String city = "";
			if (variables.containsKey("city"))
				city = variables.get("city").toString();

			String sbiStringTicketNo = ticketNo.substring(0, 3);
			if (sbiStringTicketNo.equals("AMS") && !status.equals("")) {
				JSONObject requestObject = getAmtrustJsonRequest(variables, status, statusCode, deliveryStatus);

				// API call
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(AMTRUST_URL);
				httppost.addHeader("Content-Type", "application/json");
				httppost.addHeader("Ocp-Apim-Subscription-Key", AMTRUST_HEADER_SUBSCRIPTION_KEY);
				httppost.setEntity(new StringEntity(requestObject.toString(), "UTF8"));

				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse1 = EntityUtils.toString(httpResponse.getEntity());
				RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "REQ:" + requestObject + " RES:" + rawResponse1);
				log.debug("AMTRUST API CALL: " + ticketNo, "REQ:" + requestObject + " RES:" + rawResponse1);

				// Get Response
				// InetAddress IP = InetAddress.getLocalHost();
				// if
				// (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP))
				// {
				// CloseableHttpResponse httpResponse =
				// httpclient.execute(httppost);
				// String rawResponse1 =
				// EntityUtils.toString(httpResponse.getEntity());
				// RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "REQ:" +
				// requestObject + " RES:" + rawResponse1);
				// log.debug("AMTRUST API CALL: "+ticketNo, "REQ:" +
				// requestObject + " RES:" + rawResponse1);
				// } else {
				// CloseableHttpResponse httpResponse =
				// httpclient.execute(httppost);
				// String rawResponse1 =
				// EntityUtils.toString(httpResponse.getEntity());
				// RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "REQ:" +
				// requestObject + " RES:" + rawResponse1);
				// log.debug("AMTRUST API CALL: "+ticketNo, "REQ:" +
				// requestObject + " RES:" + rawResponse1);
				// }
			} else {
				// Ticket is not for respective retailer
			}

		} catch (Exception e) {
			log.debug("" + e);
			RetailerApiCalls.saveToConsoleLogsTable("AMTRUST API CALL ERROR :", e + "");

		}
	}

	public static JSONObject getAmtrustJsonRequest(Map<String, Object> variables, String status, int statusCode,
			int deliveryStatus) throws Exception {
		String ticketNo = variables.get("rlTicketNo").toString();
		String location = "";
		if (variables.containsKey("rlLocationName"))
			location = variables.get("rlLocationName").toString();
		String customerName = "";
		if (variables.containsKey("consumerName"))
			customerName = variables.get("consumerName").toString();
		String orderNo = "";
		if (variables.containsKey("orderNumber"))
			orderNo = variables.get("orderNumber").toString();
		String city = "";
		if (variables.containsKey("city"))
			city = variables.get("city").toString();
		String dropLocation = "";
		if (variables.containsKey("dropLocCity"))
			city = variables.get("dropLocCity").toString();

		JSONObject returnJson = new JSONObject();

		returnJson.put("UserName", AMTRUST_HEADER_USER_NAME);
		returnJson.put("Password", AMTRUST_HEADER_PASSWORD);
		returnJson.put("CustomerId", AMTRUST_HEADER_CUSTOMER_ID);
		returnJson.put("LSPAlias", AMTRUST_HEADER_LSP);
		returnJson.put("DocketNo", ticketNo);
		returnJson.put("ReferenceNo", "");
		returnJson.put("ConsignorName", "");
		returnJson.put("ConsigneeName", "");
		returnJson.put("CurrentStatus", status);
		returnJson.put("Origin", location);
		returnJson.put("Destination", dropLocation);
		returnJson.put("StatusDate", getCurrentDateSQLFormatForAmtrust());
		returnJson.put("CurrentLocation", "");
		returnJson.put("StatusCode", statusCode);
		returnJson.put("DeliveryStatus", deliveryStatus);

		JSONObject eventMap = new JSONObject();
		eventMap.put("Status", status);
		eventMap.put("Location", location);
		eventMap.put("StatusDate", getCurrentDateSQLFormatForAmtrust());
		eventMap.put("StatusCode", statusCode);
		JSONArray eventList = new JSONArray();
		eventList.put(eventMap);
		returnJson.put("Events", eventList);

		return returnJson;
	}

	// activity call
	public static void sendStatusPickAndDropTwoWay(String rlProcessId, String statusString) {
		log.debug("sendStatusToRetailer Pick And Drop Two Way------------------------");
		saveToConsoleLogsTable("Pick And Drop (One Way)", statusString + " " + rlProcessId);
		Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService().getVariables(rlProcessId);

		callTVSAPIForUpdatingStatus(rlProcessId, statusString);
	}

	// activity call
	public static void sendAppointmentStatus(String rlProcessId, String feName, String date, String reasonForVisit) {
		log.debug("sendStatusToRetailer Pick And Drop Two Way ETA----------------------");
		log.debug("------------------" + " rlProcessId : " + rlProcessId + "******" + " feName : " + feName + "*******"
				+ " date : " + date + " ---------------");
		saveToConsoleLogsTable("Pick And Drop (One Way)", feName + " " + rlProcessId + " " + date);
		Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService().getVariables(rlProcessId);
		callTVSAPIForAppointmentStatus(rlProcessId, feName, date, reasonForVisit);
	}

	private static void callTVSAPIForUpdatingStatus(String rlProcessId, String statusString) {
		log.debug("Calling TVS API-------------------------------------");
		String ticketNo = "";
		String status = "";
		String comment = "";
		status = statusString;

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(rlProcessId);

		try {
			ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));
			log.debug(ticketNo);
		} catch (Exception e) {

		}

		final String retailerAPI = "http://servicetec.democdts.xyz:8082/claimRoutes/getTickeckStatus/";
		final String retailerName = ticketNo.substring(0, 3);

		try {

			if (retailerName.equals("TVS")) {
				log.debug("TVS Ticket Processing...........");
				JSONObject requestJson = getTVSJson(ticketNo, status);
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(retailerAPI);
				httppost.addHeader("Content-Type", "application/json");

				saveToConsoleLogsTable(ticketNo, statusString + ":" + "Request: " + retailerAPI + " " + requestJson);
				log.debug("Ticket Number : " + ticketNo + "Retailer API : " + retailerAPI + " Request Json : "
						+ requestJson);

				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug(rawResponse);
				saveToConsoleLogsTable(ticketNo, statusString + ":" + "Response: " + rawResponse);
			} else {
				// Ticket is not for TVS
			}
		} catch (Exception e) {
			log.debug("Exception Caught...............");
			log.debug(e.getMessage());
		}
	}

	private static void callTVSAPIForAppointmentStatus(String rlProcessId, String feName, String date,
			String reasonForVisit) {
		log.debug("Calling TVS API FOR ETA--------------------------");
		String ticketNo = "";
		String feName1;
		String scheduleDateTime;
		// String reasonForVisit = "";
		if (feName.isEmpty() || feName == null) {
			feName1 = "";
		} else {
			feName1 = feName;
		}

		if (date.isEmpty() || date == null) {
			scheduleDateTime = "";
		} else {
			scheduleDateTime = date;
		}

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
		log.debug("Call Retailer API here");

		try {
			ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));
			log.debug(ticketNo);
		} catch (Exception e) {

		}

		final String retailerAPI = "http://servicetec.democdts.xyz:8082/claimRoutes/getETADetails/";
		final String retailerName = ticketNo.substring(0, 3);

		try {

			if (retailerName.equals("TVS")) {
				log.debug("TVS Ticket Processing...........");
				JSONObject requestJson = getTVSJson(ticketNo, feName1, scheduleDateTime, reasonForVisit);
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httppost = new HttpPost(retailerAPI);
				httppost.addHeader("Content-Type", "application/json");

				saveToConsoleLogsTable(ticketNo, feName1 + ":" + "Request: " + retailerAPI + " " + requestJson);
				log.debug("Ticket Number : " + ticketNo + "Retailer API : " + retailerAPI + " Request Json : "
						+ requestJson);

				httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				String rawResponse = EntityUtils.toString(httpResponse.getEntity());
				log.debug(rawResponse);
				saveToConsoleLogsTable(ticketNo, feName1 + ":" + "Response: " + rawResponse);
			} else {
				// Ticket is not for TVS
			}
		} catch (Exception e) {
			log.debug("Exception Caught...............");
			log.debug(e.getMessage());
		}
	}

	// call retailer APIs
	// private static String callRetailerAPI(String api, JSONObject jsonObject){
	// CloseableHttpClient httpclient = HttpClients.createDefault();
	// HttpPost httppost = new HttpPost(api);
	// httppost.addHeader("Content-Type", "application/json");
	// httppost.setEntity(new StringEntity(jsonObject.toString(), "UTF8"));
	// CloseableHttpResponse httpResponse = httpclient.execute(httppost);
	// String rawResponse = EntityUtils.toString(httpResponse.getEntity());
	// log.debug(rawResponse);
	// }

	private static JSONObject getTVSJson(String ticketNo, String status) throws JSONException {
		JSONObject requestJson = new JSONObject();
		requestJson.put("ticketNo", ticketNo);
		requestJson.put("status", status);
		return requestJson;
	}

	private static JSONObject getTVSJson(String ticketNo, String feName, String appointmentDate, String reasonForVisit)
			throws JSONException {
		JSONObject requestJson = new JSONObject();
		requestJson.put("ticketNo", ticketNo);
		requestJson.put("feName", feName);
		requestJson.put("ETATime", appointmentDate);
		requestJson.put("reasonForVisit", reasonForVisit);
		return requestJson;
	}

	// get ticketNumber from processId
	private static String getTicketNumberFromProcessId(String rlProcessId) {
		String ticketNo = "";
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		Map<String, Object> variables = runtimeService.getVariables(rlProcessId);
		try {
			ticketNo = String.valueOf(runtimeService.getVariable(rlProcessId, "rlTicketNo"));
			log.debug(ticketNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketNo;
	}

}
