package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.BarcodeGeneration;
import com.rlogistics.master.identity.BarcodeOffset;
import com.rlogistics.master.identity.BarcodeOffset.BarcodeOffsetQuery;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingMaterialMaster;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.ui.CSVDataProcessor;

@RestController
public class PackagingMaterialResource extends RLogisticsResource{
	
	private static Logger log = LoggerFactory.getLogger(PackagingMaterialResource.class);

	@RequestMapping(value = "/packaging-material/inventory/add", method = RequestMethod.POST)
	public RestResponse addPackagingInventory(@RequestParam("packagingMaterialInventory") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, String> result = new HashMap<String, String>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			PackagingMaterialInventory packagingMaterialInventory = masterdataService.newPackagingMaterialInventory();
			int status = 0;
			
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			packagingMaterialInventory.setId(id);
			
			if(queryObj.containsKey("locationId")) {
				packagingMaterialInventory.setLocationId(queryObj.get("locationId"));
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingMaterialInventory.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
			}
			if(queryObj.containsKey("barcode")) {
				packagingMaterialInventory.setBarcode(queryObj.get("barcode"));
			}
			if(queryObj.containsKey("status")) {
				status = Integer.valueOf(String.valueOf(queryObj.get("status")));
				packagingMaterialInventory.setStatus(status);
			}
			if(queryObj.containsKey("ticketNumber")) {
				packagingMaterialInventory.setTicketNumber(queryObj.get("ticketNumber"));
			}
			if(queryObj.containsKey("userId")) {
				packagingMaterialInventory.setUserId(queryObj.get("userId"));
			}
			if(queryObj.containsKey("additionalDetails")) {
				packagingMaterialInventory.setAdditionalDetails(queryObj.get("additionalDetails"));
			}
			masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
			
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
			if(packagingMaterialMaster == null) {
				packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
				c.setTime(new Date());
				packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
				packagingMaterialMaster.setLocationId(queryObj.get("locationId"));
				packagingMaterialMaster.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				if(status==0) {
					packagingMaterialMaster.setAvailableQuantity(1);
				} else {
					packagingMaterialMaster.setAvailableQuantity(0);
				}
			} else {
				if(status==0) {
					if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
						if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
							masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
							throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
						}  
					} 
					packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
					
				} else {
					if(packagingMaterialMaster.getAvailableQuantity() != 0) {
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
					}
				}
			}
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Inventory added");
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/packaging-material/inventory/update", method = RequestMethod.POST)
	public RestResponse updatePackagingInventory(@RequestParam("packagingMaterialInventory") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, String> result = new HashMap<String, String>();
		boolean isStatusSame = true;
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().id(queryObj.get("id")).singleResult();
			int previousStatus = packagingMaterialInventory.getStatus();
			int currentStatus = 0;
			
			if(queryObj.containsKey("locationId")) {
				packagingMaterialInventory.setLocationId(queryObj.get("locationId"));
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingMaterialInventory.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
			}
			if(queryObj.containsKey("barcode")) {
				packagingMaterialInventory.setBarcode(queryObj.get("barcode"));
			}
			if(queryObj.containsKey("status")) {
				if(packagingMaterialInventory.getStatus()!=Integer.valueOf(String.valueOf(queryObj.get("status")))) {
					isStatusSame = false;
				}
				currentStatus = Integer.valueOf(String.valueOf(queryObj.get("status")));
				packagingMaterialInventory.setStatus(currentStatus);
			} 
			if(queryObj.containsKey("ticketNumber")) {
				packagingMaterialInventory.setTicketNumber(queryObj.get("ticketNumber"));
			}
			if(queryObj.containsKey("userId")) {
				packagingMaterialInventory.setUserId(queryObj.get("userId"));
			}
			if(queryObj.containsKey("additionalDetails")) {
				packagingMaterialInventory.setAdditionalDetails(queryObj.get("additionalDetails"));
			}
			masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
			
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
			if(packagingMaterialMaster == null) {
				packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
				packagingMaterialMaster.setLocationId(queryObj.get("locationId"));
				packagingMaterialMaster.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				if(currentStatus != 2 && currentStatus != 4) {
					packagingMaterialMaster.setAvailableQuantity(1);
				} else {
					packagingMaterialMaster.setAvailableQuantity(0);
				}
			} else {
				if(!isStatusSame) {
					if((currentStatus != 2 && currentStatus != 4) && (previousStatus == 2 || previousStatus == 4)) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						}
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
					} else if ((currentStatus == 4 || currentStatus == 2) && (previousStatus != 2 && previousStatus != 4)){
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}	
			}
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Inventory updated");
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to update : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/packaging-inventory/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadPackagingInventoryCommands(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file, @RequestParam(value = "locationId") String locationId) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			return new PackagingCommandProcessor().packagingInventoryUpload(file.getInputStream(),locationId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	protected class PackagingCommandProcessor extends CSVDataProcessor {

		public UploadProcessCommandsResult packagingInventoryUpload(InputStream is, String locationId) {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			
			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			
			try {
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String record = null;
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				while ((record = bis.readLine()) != null) {
					recordNo++;

					String[] recordFields = parseLine(record, recordNo);

					if (recordFields == null || recordFields.length == 0)
						continue;

					if(recordNo == 1){
						/* Header record*/
						for(int i = 0; i< recordFields.length;++i){
							orderedFields.add(recordFields[i]);
						}
						if(log.isDebugEnabled()){
							log.debug("HEADER:" + recordNo + ":" + record);
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
						continue;
					}
					
					if(log.isDebugEnabled()){
						log.debug("PROCESSING:" + recordNo + ":" + record);
						for (String f : recordFields) {
							log.debug("------------" + f);
						}
					}

						final Map<String,String> fieldValuesPreFilter = new HashMap<String, String>();
						
						for(int i = 0;i < recordFields.length;++i){
							fieldValuesPreFilter.put(orderedFields.get(i), recordFields[i]);
						}

						final Map<String,String> fieldValues = filterFieldValues(fieldValuesPreFilter);
						String packagingTypeCode = fieldValues.get("packagingTypeCode");
						String barcode = fieldValues.get("barcode");
						
						PackagingMaterialInventory packagingMaterialInventory = masterdataService.newPackagingMaterialInventory();
						int status = 0;
						
						Calendar c = Calendar.getInstance();
						c.setTime(new Date());
						String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
						packagingMaterialInventory.setId(id);
						packagingMaterialInventory.setLocationId(locationId);
						packagingMaterialInventory.setPackagingTypeCode(packagingTypeCode);
						packagingMaterialInventory.setBarcode(barcode);
						packagingMaterialInventory.setStatus(status);
						
						try {
							masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
							
							PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(locationId).packagingTypeCode(packagingTypeCode).singleResult();
							if(packagingMaterialMaster == null) {
								packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
								c.setTime(new Date());
								packagingMaterialMaster.setId(RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
								packagingMaterialMaster.setLocationId(locationId);
								packagingMaterialMaster.setPackagingTypeCode(packagingTypeCode);
								if(status==0) {
									packagingMaterialMaster.setAvailableQuantity(1);
								} else {
									packagingMaterialMaster.setAvailableQuantity(0);
								}
							} else {
								if(status==0) {
									if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
										if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
											masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
											ErrorObject errorObject = new ErrorObject();
											errorObject.setRowNo(recordNo);
											errorObject.setMessage("Ignoring line " + recordNo);
											errorObject.setReason("Available quantity cannot exceed Maximum allowed quantity");
											retval.getErrorMessages().add(errorObject);
											continue;
										}  else {
											packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
										}
									} else {
										packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
									}
									
								} else {
									if(packagingMaterialMaster.getAvailableQuantity() != 0) {
										packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
									}
								}
							}
							masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
							insertCount++;
						} catch (Exception e) {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Duplicate Record");
							retval.getErrorMessages().add(errorObject);
							continue;
						}	 	
				}
				retval.setNoOfRecordsInserted(insertCount);
			} catch (IOException ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			
			return retval;
		}
		
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}
	
	@RequestMapping(value = "/packaging-material/update", method = RequestMethod.POST)
	public RestResponse updatePackagingMaterial(@RequestParam("query") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, String> result = new HashMap<String, String>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().id(queryObj.get("id")).singleResult();
			
			if(queryObj.containsKey("maximumQuantityAllowed")) {
				packagingMaterialMaster.setMaximumQuantityAllowed(Integer.valueOf(String.valueOf(queryObj.get("maximumQuantityAllowed"))));
			}
			if(queryObj.containsKey("reorderLevel")) {
				packagingMaterialMaster.setReorderLevel(Integer.valueOf(String.valueOf(queryObj.get("reorderLevel"))));
			}
			
			masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Record updated");
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
				log.error(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/packaging-material/barcode/generate", method = RequestMethod.POST)
	public RestResponse generateBarcode(@RequestParam("query") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		/*if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}*/
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			
			int itemCount = Integer.valueOf(String.valueOf(queryObj.get("itemCount")));
			BarcodeOffsetQuery barcodeOffsetQuery = masterdataService.createBarcodeOffsetQuery();
			BarcodeOffset barcodeOffset = barcodeOffsetQuery.locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
			if(barcodeOffset == null) {
				barcodeOffset = masterdataService.newBarcodeOffset();
				barcodeOffset.setLocationId(queryObj.get("locationId"));
				barcodeOffset.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				barcodeOffset.setOffset(10000);
				
				masterdataService.saveBarcodeOffset(barcodeOffset);
			}
			int offset = barcodeOffset.getOffset();
			String startingBarcode = String.valueOf(offset);
			String endingBarcode = "";
			String locationCode = "";
			int status = 0;
			String locationId = "";
			String packagingTypeCode = "";
			if(queryObj.containsKey("locationId")) {
				locationId = queryObj.get("locationId");
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(queryObj.get("locationId")).singleResult();
				locationCode = processLocation.getCode();
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingTypeCode = queryObj.get("packagingTypeCode");
			}
			if(queryObj.containsKey("status")) {
				status = Integer.valueOf(String.valueOf(queryObj.get("status")));	
			}
			
			for(int i=0;i<itemCount;i++) {
				PackagingMaterialInventory packagingMaterialInventory = masterdataService.newPackagingMaterialInventory();
				
				UUID id = UUID.randomUUID();
				packagingMaterialInventory.setId(String.valueOf(id));
				
				packagingMaterialInventory.setLocationId(queryObj.get("locationId"));
				packagingMaterialInventory.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				packagingMaterialInventory.setStatus(status);
				
				
				packagingMaterialInventory.setBarcode(locationCode + packagingTypeCode + String.valueOf(offset));
				System.out.print(locationCode + packagingTypeCode + String.valueOf(offset));
				packagingMaterialInventory.setBarcodeNumber(String.valueOf(offset));
				packagingMaterialInventory.setLocationCode(locationCode);
				packagingMaterialInventory.setPrintStatus(0);
				masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
				
				PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
				if(packagingMaterialMaster == null) {
					packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
					
					packagingMaterialMaster.setId(String.valueOf(UUID.randomUUID()));
					packagingMaterialMaster.setLocationId(queryObj.get("locationId"));
					packagingMaterialMaster.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
					if(status==0) {
						packagingMaterialMaster.setAvailableQuantity(1);
					} else {
						packagingMaterialMaster.setAvailableQuantity(0);
					}
				} else {
					if(status==0) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						} 
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
						
					} else {
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}
				masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
				if(i==itemCount-1) {
					endingBarcode = String.valueOf(offset);
				}
				offset++;
			}
			
			//update the barcode offset
			barcodeOffset.setOffset(Integer.valueOf(endingBarcode)+1);
			masterdataService.saveBarcodeOffset(barcodeOffset);
			
			//insert record to barcode generation table
			BarcodeGeneration barcodeGeneration = masterdataService.newBarcodeGeneration();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			barcodeGeneration.setId(String.valueOf(UUID.randomUUID()));
			barcodeGeneration.setItemCount(itemCount);
			barcodeGeneration.setLocationId(queryObj.get("locationId"));
			Timestamp timestamp = new Timestamp(c.getTime().getTime());
			barcodeGeneration.setGeneratedDate(timestamp);
			barcodeGeneration.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
			barcodeGeneration.setStartingBarcode(locationCode + packagingTypeCode + startingBarcode);
			barcodeGeneration.setEndingBarcode(locationCode + packagingTypeCode + endingBarcode);
			barcodeGeneration.setStartingBarcodeRange(startingBarcode);
			barcodeGeneration.setEndingBarcodeRange(endingBarcode);
			barcodeGeneration.setPrintStatus(0);
			
			masterdataService.saveBarcodeGeneration(barcodeGeneration);
			
			result.put("barcodeGeneration", barcodeGeneration);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Barcode generated successfully");
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
		
	public static boolean generateBarcodeMethod(Map<String, String> queryObj) throws Exception {
		/*if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}*/
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		boolean res = false;
		try {
			
			int itemCount = Integer.valueOf(String.valueOf(queryObj.get("itemCount")));
			BarcodeOffsetQuery barcodeOffsetQuery = masterdataService.createBarcodeOffsetQuery();
			BarcodeOffset barcodeOffset = barcodeOffsetQuery.locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
			if(barcodeOffset == null) {
				barcodeOffset = masterdataService.newBarcodeOffset();
				barcodeOffset.setLocationId(queryObj.get("locationId"));
				barcodeOffset.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				barcodeOffset.setOffset(10000);
				
				masterdataService.saveBarcodeOffset(barcodeOffset);
			}
			int offset = barcodeOffset.getOffset();
			String startingBarcode = String.valueOf(offset);
			String endingBarcode = "";
			String locationCode = "";
			int status = 0;
			String locationId = "";
			String packagingTypeCode = "";
			if(queryObj.containsKey("locationId")) {
				locationId = queryObj.get("locationId");
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(queryObj.get("locationId")).singleResult();
				locationCode = processLocation.getCode();
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingTypeCode = queryObj.get("packagingTypeCode");
			}
			if(queryObj.containsKey("status")) {
				status = Integer.valueOf(String.valueOf(queryObj.get("status")));	
			}
			
			for(int i=0;i<itemCount;i++) {
				PackagingMaterialInventory packagingMaterialInventory = masterdataService.newPackagingMaterialInventory();
				
				UUID id = UUID.randomUUID();
				packagingMaterialInventory.setId(String.valueOf(id));
				
				packagingMaterialInventory.setLocationId(queryObj.get("locationId"));
				packagingMaterialInventory.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				packagingMaterialInventory.setStatus(status);
				
				
				packagingMaterialInventory.setBarcode(locationCode + packagingTypeCode + String.valueOf(offset));
				packagingMaterialInventory.setBarcodeNumber(String.valueOf(offset));
				packagingMaterialInventory.setLocationCode(locationCode);
				packagingMaterialInventory.setPrintStatus(0);
				masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
				
				PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
				if(packagingMaterialMaster == null) {
					packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
					
					packagingMaterialMaster.setId(String.valueOf(UUID.randomUUID()));
					packagingMaterialMaster.setLocationId(queryObj.get("locationId"));
					packagingMaterialMaster.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
					if(status==0) {
						packagingMaterialMaster.setAvailableQuantity(1);
					} else {
						packagingMaterialMaster.setAvailableQuantity(0);
					}
				} else {
					if(status==0) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						} 
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
						
					} else {
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}
				masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
				if(i==itemCount-1) {
					endingBarcode = String.valueOf(offset);
				}
				offset++;
			}
			
			//update the barcode offset
			barcodeOffset.setOffset(Integer.valueOf(endingBarcode)+1);
			masterdataService.saveBarcodeOffset(barcodeOffset);
			
			//insert record to barcode generation table
			BarcodeGeneration barcodeGeneration = masterdataService.newBarcodeGeneration();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			barcodeGeneration.setId(String.valueOf(UUID.randomUUID()));
			barcodeGeneration.setItemCount(itemCount);
			barcodeGeneration.setLocationId(queryObj.get("locationId"));
			Timestamp timestamp = new Timestamp(c.getTime().getTime());
			barcodeGeneration.setGeneratedDate(timestamp);
			barcodeGeneration.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
			barcodeGeneration.setStartingBarcode(locationCode + packagingTypeCode + startingBarcode);
			barcodeGeneration.setEndingBarcode(locationCode + packagingTypeCode + endingBarcode);
			barcodeGeneration.setStartingBarcodeRange(startingBarcode);
			barcodeGeneration.setEndingBarcodeRange(endingBarcode);
			barcodeGeneration.setPrintStatus(0);
			
			masterdataService.saveBarcodeGeneration(barcodeGeneration);
			
			res = true;
		} catch (Exception e) {
			
		}
		return res;
	}
	
	
	@RequestMapping(value = "/update/barcode-status", method = RequestMethod.POST)
	public RestResponse updateBarcodePrintStatus(@RequestParam("query") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, String> result = new HashMap<String, String>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			int status = Integer.valueOf(String.valueOf(queryObj.get("status")));
			if(status==1) {
				
				//update the print Status in barcode generation table
				BarcodeGeneration barcodeGeneration = masterdataService.createBarcodeGenerationQuery().id(queryObj.get("barcodeGenerationId")).singleResult();
				barcodeGeneration.setPrintStatus(1);
				masterdataService.saveBarcodeGeneration(barcodeGeneration);
				
				//increment the print count for all barcodes in inventory table
				int startingIndex = Integer.valueOf(barcodeGeneration.getStartingBarcodeRange());
				int endingIndex = Integer.valueOf(barcodeGeneration.getEndingBarcodeRange());
				/*String startingBarcode = barcodeGeneration.getStartingBarcode();
				String endingBarcode = barcodeGeneration.getEndingBarcode();*/
				int itemCount = barcodeGeneration.getItemCount();
				
				for(int i=0;i<itemCount;i++) {
					PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcodeNumber(String.valueOf(startingIndex)).singleResult();
					int printCount = packagingMaterialInventory.getPrintStatus();
					
					packagingMaterialInventory.setPrintStatus(printCount+1);
					masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
					startingIndex++;
				}
				
				result.put("success","true");
				restResponse.setResponse(result);
				restResponse.setMessage("Updated successfully");
			}
			
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
				log.error(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/barcode/listing", method = RequestMethod.POST)
	public RestResponse barcodeBulkPrint(@RequestParam("query") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			
				//update the print Status in barcode generation table
				BarcodeGeneration barcodeGeneration = masterdataService.createBarcodeGenerationQuery().id(queryObj.get("barcodeGenerationId")).singleResult();
				
				int startingIndex = Integer.valueOf(barcodeGeneration.getStartingBarcodeRange());
				int endingIndex = Integer.valueOf(barcodeGeneration.getEndingBarcodeRange());
				int itemCount = barcodeGeneration.getItemCount();
				
				List<String> barcodeList = new ArrayList<String>();
				for(int i=0;i<itemCount;i++) {
					PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcodeNumber(String.valueOf(startingIndex)).singleResult();
					String barcode = packagingMaterialInventory.getBarcode();
					barcodeList.add(barcode);
					
					startingIndex++;
				}
				
				result.put("startingBarcode",barcodeGeneration.getStartingBarcode());
				result.put("endingBarcode",barcodeGeneration.getEndingBarcode());
				result.put("startingBarcodeRange",barcodeGeneration.getStartingBarcodeRange());
				result.put("endingBarcodeRange",barcodeGeneration.getEndingBarcodeRange());
				result.put("itemCount",String.valueOf(barcodeGeneration.getItemCount()));
				result.put("barcodeList", barcodeList);
				
				restResponse.setResponse(result);
				restResponse.setMessage("List of barcodes");
			
			
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
				log.error(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/add/barcode", method = RequestMethod.POST)
	public RestResponse addBarcodes(@RequestParam("query") String queryJSON, HttpServletRequest request ,HttpServletResponse response) throws Exception {
		if(!beforeMethodInvocation(request,response)){
			throw new Exception("rltoken null or invalid");
		}
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> queryObj = new HashMap<String, String>();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			
			int itemCount = Integer.valueOf(String.valueOf(queryObj.get("itemCount")));
			/*BarcodeOffsetQuery barcodeOffsetQuery = masterdataService.createBarcodeOffsetQuery();
			BarcodeOffset barcodeOffset = barcodeOffsetQuery.locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
			if(barcodeOffset == null) {
				barcodeOffset = masterdataService.newBarcodeOffset();
				barcodeOffset.setLocationId(queryObj.get("locationId"));
				barcodeOffset.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				barcodeOffset.setOffset(10000);
				
				masterdataService.saveBarcodeOffset(barcodeOffset);
			}*/
		//	int offset = barcodeOffset.getOffset();
			String barcodesString = queryObj.get("barcodes");
			String [] barcodeStrArr = barcodesString.split("\n");
			ArrayList<String> barcodeList = new ArrayList<String>();
			for(int b=0;b<barcodeStrArr.length;b++) {
				barcodeList.add(barcodeStrArr[b]);
			}
			String startingBarcode = barcodeList.get(0);
			String endingBarcode = "";
			String locationCode = "";
			int status = 0;
			String locationId = "";
			String packagingTypeCode = "";
			if(queryObj.containsKey("locationId")) {
				locationId = queryObj.get("locationId");
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(queryObj.get("locationId")).singleResult();
				locationCode = processLocation.getCode();
			}
			if(queryObj.containsKey("packagingTypeCode")) {
				packagingTypeCode = queryObj.get("packagingTypeCode");
			}
			if(queryObj.containsKey("status")) {
				status = Integer.valueOf(String.valueOf(queryObj.get("status")));	
			}
			
			for(int i=0;i<itemCount;i++) {
				PackagingMaterialInventory packagingMaterialInventory = masterdataService.newPackagingMaterialInventory();
				
				UUID id = UUID.randomUUID();
				packagingMaterialInventory.setId(String.valueOf(id));
				
				packagingMaterialInventory.setLocationId(queryObj.get("locationId"));
				packagingMaterialInventory.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
				packagingMaterialInventory.setStatus(status);
				
				
				packagingMaterialInventory.setBarcode(locationCode + packagingTypeCode + barcodeList.get(0));
				packagingMaterialInventory.setBarcodeNumber(locationCode + packagingTypeCode + barcodeList.get(0));
				packagingMaterialInventory.setLocationCode(locationCode);
				packagingMaterialInventory.setPrintStatus(0);
				masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
				
				PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(queryObj.get("locationId")).packagingTypeCode(queryObj.get("packagingTypeCode")).singleResult();
				if(packagingMaterialMaster == null) {
					packagingMaterialMaster = masterdataService.newPackagingMaterialMaster();
					
					packagingMaterialMaster.setId(String.valueOf(UUID.randomUUID()));
					packagingMaterialMaster.setLocationId(queryObj.get("locationId"));
					packagingMaterialMaster.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
					if(status==0) {
						packagingMaterialMaster.setAvailableQuantity(1);
					} else {
						packagingMaterialMaster.setAvailableQuantity(0);
					}
				} else {
					if(status==0) {
						if(packagingMaterialMaster.getMaximumQuantityAllowed()!=0) {
							if(packagingMaterialMaster.getAvailableQuantity()+1 > packagingMaterialMaster.getMaximumQuantityAllowed()) {
								masterdataService.deletePackagingMaterialInventory(packagingMaterialInventory.getId());
								throw new Exception("Available quantity cannot exceed Maximum allowed quantity");
							}  
						} 
						packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()+1);
						
					} else {
						if(packagingMaterialMaster.getAvailableQuantity() != 0) {
							packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
						}
					}
				}
				masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
				
			}
			
			
			//insert record to barcode generation table
			BarcodeGeneration barcodeGeneration = masterdataService.newBarcodeGeneration();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			barcodeGeneration.setId(String.valueOf(UUID.randomUUID()));
			barcodeGeneration.setItemCount(itemCount);
			barcodeGeneration.setLocationId(queryObj.get("locationId"));
			Timestamp timestamp = new Timestamp(c.getTime().getTime());
			barcodeGeneration.setGeneratedDate(timestamp);
			barcodeGeneration.setPackagingTypeCode(queryObj.get("packagingTypeCode"));
			barcodeGeneration.setStartingBarcode(locationCode + packagingTypeCode + startingBarcode);
			barcodeGeneration.setEndingBarcode(locationCode + packagingTypeCode + endingBarcode);
			barcodeGeneration.setStartingBarcodeRange(startingBarcode);
			barcodeGeneration.setEndingBarcodeRange(endingBarcode);
			barcodeGeneration.setPrintStatus(0);
			
			masterdataService.saveBarcodeGeneration(barcodeGeneration);
			
			result.put("barcodeGeneration", barcodeGeneration);
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Barcode generated successfully");
		} catch (Exception e) {
			if(e.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Failed to add : Duplicate record");
			} else {
				restResponse.setMessage(e.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
}
