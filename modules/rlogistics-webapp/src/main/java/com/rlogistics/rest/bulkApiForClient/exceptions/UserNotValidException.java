package com.rlogistics.rest.bulkApiForClient.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
@SuppressWarnings("unused")
public class UserNotValidException extends RuntimeException {
    private String message;

    public UserNotValidException(String message) {
        super(message);
        fillInStackTrace();
    }
}
