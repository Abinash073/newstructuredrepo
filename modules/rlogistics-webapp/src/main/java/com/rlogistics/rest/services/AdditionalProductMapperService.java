package com.rlogistics.rest.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.AdditionalProductEntity;
import com.rlogistics.rest.services.helper.BoxNumberWithCount;
import com.rlogistics.rest.services.helper.DropLocationWithCountOfProdDTO;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Service to fetch bulk items data based of business logic requirement
 *
 * @author Adarsh
 */
@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@val
@Slf4j
@SuppressWarnings("deprecation")
public class AdditionalProductMapperService {

    private MasterdataService masterdataService;
    private ObjectMapper mapper;

    public AdditionalProductMapperService(MasterdataService masterdataService, ObjectMapper mapper) {
        this.masterdataService = masterdataService;
        this.mapper = mapper;
    }

    /**
     * Configuring object mapper
     */
    @PostConstruct
    void init() {
        mapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
        ;
    }

    /**
     * Get total box number with there count for a bulk processId
     *
     * @param box       Box number
     * @param processId processId
     * @return
     */
    public BoxNumberWithCount getByBox(String box, String processId) {


        List<AdditionalProductDetails> details = masterdataService.createAdditionalProductDetailsQuery()
                .boxNumber(box)
                .processId(processId)
                .list();


        int count = (int) masterdataService.createAdditionalProductDetailsQuery()
                .boxNumber(box)
                .processId(processId)
                .status("in_hub")
                .count();
        BoxNumberWithCount boxNumberWithCount = new BoxNumberWithCount();

        @Deprecated
        List<AdditionalProductEntity> a = mapper.convertValue(details, TypeFactory.defaultInstance().constructType(List.class, AdditionalProductEntity.class));


        boxNumberWithCount.setProductDetails(a);
        boxNumberWithCount.setCount(count);
        boxNumberWithCount.setBoxNumber(box);

        return boxNumberWithCount;


    }


    /**
     * Get bulk items by drop location
     *
     * @param processId process id of bulk ticket
     * @return List of items based on drop location
     */
    public List<DropLocationWithCountOfProdDTO> getByDropLocation(String processId) {
        List<AdditionalProductDetails> additionalProductDetails = masterdataService.createAdditionalProductDetailsQuery().processId(processId).list();

        Map<String, List<AdditionalProductDetails>> a =
                Collections
                        .synchronizedMap(
                                additionalProductDetails.stream().collect(Collectors.groupingBy(o ->
                                        {
                                            val c = o.getDropLocation().trim() + String.valueOf(o.getDropLocPincode());
                                            return c.replaceAll("[^a-zA-z0-9]+", "");
                                        })
                                )
                        );
        ConcurrentHashMap<String, List<AdditionalProductDetails>> syncA = new ConcurrentHashMap<>(a);

        HashMap<String, List<AdditionalProductDetails>> tempsyncA = new HashMap<>();

        syncA.keySet().forEach(b -> {
            val value = syncA.get(b);
            if (!value.isEmpty()) {
                tempsyncA.put(value.get(0).getDropLocation() + "-" + value.get(0).getDropLocPincode(), value);
            }
            syncA.remove(b);
        });

        return tempsyncA.keySet().stream().map(b -> {
            val c = new DropLocationWithCountOfProdDTO();
            c.setDropLocationAddress(b);
            c.setAdditionalProductEntities(tempsyncA.get(b));
            c.setCount(tempsyncA.get(b).size());
            return c;
        }).collect(Collectors.toList());


    }


}
