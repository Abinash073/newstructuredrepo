package com.rlogistics.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.activiti.bpmn.model.FormGroup;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.form.FormData;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.impl.form.BooleanFormType;
import org.activiti.engine.impl.form.DateFormType;
import org.activiti.engine.impl.form.DoubleFormType;
import org.activiti.engine.impl.form.DynamicEnumFormType;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.impl.form.LongFormType;
import org.activiti.engine.impl.form.StringFormType;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.scripting.ScriptingEngines;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.activiti.RLogisticsSpringProcessEngineConfiguration;
import com.rlogistics.http.RLogisticsHttpSessionHolder;
import com.rlogistics.scripting.ErrorReport;
import com.rlogistics.scripting.ExecutionReport;
import com.rlogistics.util.PreProcessInstanceVariableScope;

public class FormFillerUtil {
	private static Logger log = LoggerFactory.getLogger(FormFillerUtil.class);
	
	private String underlyingId;
	private boolean inTaskContext;
	private FormData formData;
	private VariableScope scope;
	private String fillSessionId;
	private int currentGroupIndex;

	private Map<String,List<FormProperty>> propertiesByGroup;
	private List<FormGroup> formGroups;	
	
	public FormFillerUtil(String deploymentId,boolean dummy){
		this(deploymentId,new PreProcessInstanceVariableScope(),false);
	}
	
	public FormFillerUtil(String underlyingId,VariableScope scope,boolean inTaskContext){
		this.inTaskContext = inTaskContext;
		this.underlyingId = underlyingId;
		this.formData = inTaskContext ? getFormDataForTaskId(underlyingId) : getFormDataForDeploymentId(underlyingId);
		this.scope = scope;
		this.currentGroupIndex = -1;
		this.fillSessionId = generateFillSessionId();

		paginate();
		
		saveToSession();
	}

	public FormFillerUtil(String fillSessionId){
		this.fillSessionId = fillSessionId;
		
		loadFromSession();
	}

	private void paginate() {
		formGroups = new ArrayList<FormGroup>();
		FormGroup defaultGroup = new FormGroup("default");
		formGroups.add(0,defaultGroup );
		if (formData != null && formData.getFormProperties() != null && !formData.getFormProperties().isEmpty()) {
			/* Partition the properties into group based properties */
			propertiesByGroup = partitionPropertiesIntoGroups(formData.getFormProperties());
			formGroups.addAll(formData.getFormGroups());
		} else {
			propertiesByGroup = new HashMap<String,List<FormProperty>>();
		}
	}

	private void saveToSession() {
		log.debug("Saving to session key " + fillSessionId);
		FormFillerUtilSessionAspect sessionAspect = new FormFillerUtilSessionAspect(true);
		log.debug("Saving. Scope not null?" + (sessionAspect.getScope() != null) + ". ExecutionId:" + sessionAspect.getExecutionId());
		RLogisticsHttpSessionHolder.getHttpSession().setAttribute(fillSessionId, sessionAspect);
	}

	private void loadFromSession() {
		log.info("Loading from session key " + fillSessionId);
		FormFillerUtilSessionAspect saved = (FormFillerUtilSessionAspect) RLogisticsHttpSessionHolder.getHttpSession().getAttribute(fillSessionId);
		log.debug("Saved session ---->" + saved);
		log.debug("Loading. Scope not null?" + (saved.getScope() != null) + ". ExecutionId:" + saved.getExecutionId());
		
		inTaskContext = saved.isInTaskContext();
		underlyingId = saved.getUnderlyingId();
		formData = inTaskContext ? getFormDataForTaskId(underlyingId) : getFormDataForDeploymentId(underlyingId);
		if(saved.getScope() != null){
			scope = saved.getScope();
		} else if(saved.getExecutionId() != null){
			scope = getExecutionEntityScope(saved.getExecutionId());
		} else {
			throw new RuntimeException("Unexpected state of FormFillerUtilSessionAspect. BOTH scope and executionId are null.");
		}
		fillSessionId = saved.getFillSessionId();
		currentGroupIndex = saved.getCurrentGroupIndex();

		paginate();
	}

	private static String generateFillSessionId() {
		try {
			Random random = new Random(new Date().getTime());
			SecretKeySpec sks = new SecretKeySpec("kfdskfajksdjfsd".getBytes(), "HmacSHA1");
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(sks);
			byte[] data = new byte[16];
			random.nextBytes(data);
			return Base64.getUrlEncoder().encodeToString(mac.doFinal(data));
		} catch (Exception ex) {
			throw new RuntimeException("Exception while trying to generate an id", ex);
		}
	}
	
	public GroupDescription getCurrentGroup(){
		return buildPageDescription(currentGroupIndex);
	}

	public GroupDescription goToNextGroup(Map<String,String> fieldValues){
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		return commandExecutor.execute(new Command<GroupDescription>() {
			@Override
			public GroupDescription execute(CommandContext commandContext) {
				return goToNextGroupImpl(fieldValues);
			}
		});
	}
	public GroupDescription goToUploadNextGroup(Map<String,String> fieldValues){
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		return commandExecutor.execute(new Command<GroupDescription>() {
			@Override
			public GroupDescription execute(CommandContext commandContext) {
				//return goToNextGroupImpl(fieldValues);
				return goToUploadNextGroupImpl(fieldValues);
			}
		});
	}

	private GroupDescription goToNextGroupImpl(Map<String,String> fieldValues){
		int nextGroupIndex = currentGroupIndex + 1;
		FormGroup currentGroup = null;
		FormGroup nextGroup = null;
		
		GroupDescription nextPageDescription = null;
		
		/* Execute "after" script for previous group*/
		if((currentGroupIndex >= 0) && (currentGroupIndex < formGroups.size())){
			currentGroup = formGroups.get(currentGroupIndex);
		}
		
		nextGroup = getGroupByIndex(nextGroupIndex);

		if(log.isDebugEnabled()){
		//	log.debug("currentGroup(" + currentGroupIndex + ")" + currentGroup);
		}
		
		/* Store in the scope */
		processFieldValues(fieldValues);

		/* Execute after script for current group */
		ExecutionReport executionReport = null;
		if(currentGroup != null){
			
			log.debug("Executing currentGroup.getAfterGroupScript():" + currentGroup.getAfterGroupScript());

			Object retval = executeJavaScript(currentGroup.getAfterGroupScript());

			if(retval != null){
				if(retval instanceof ErrorReport){
					throw new ScriptingErrorsException((ErrorReport)retval);
				} else if(retval instanceof ExecutionReport){
					executionReport = (ExecutionReport) retval;
				} else {
					log.info("Ignoring return from afterscript " + currentGroup.getAfterGroupScript() + ":" + retval);
				}
			}
		}
				
		/* Locate next enabled group */
		while(nextGroup != null){
			if(log.isDebugEnabled()){
				log.debug("nextGroup(" + nextGroupIndex + ")" + nextGroup);
			}
			
			Object expressionValue = executeJavaScript(nextGroup.getExpression());
			if ((expressionValue != null) && !(expressionValue instanceof Boolean)) {
				throw new RuntimeException("Expression " + nextGroup.getExpression() + " does not evaluate to a boolean. It is " + expressionValue.getClass());
			} else {
				boolean enabled = expressionValue != null ? ((Boolean)expressionValue) : false;
				if(!enabled){
					/* Look for any mandatory fields in the skipped group 
					 * and set them to default values*/
					processFieldValues(makeDefaultValues(nextGroup));
					nextGroup = getGroupByIndex(++nextGroupIndex);
					
					continue;
				}
			}
			
			/* Execute before script */
			executeJavaScript(nextGroup.getBeforeGroupScript());
			List<FormProperty> groupProperties = propertiesByGroup.get(nextGroup.getName());
			
			if(groupProperties == null){
				nextGroup = getGroupByIndex(++nextGroupIndex);
				
				continue;
			}
			
			nextPageDescription = buildPageDescription(nextGroupIndex);
			
			break;
		}
		
		/* Advance even when we do nothing so that we do nothing even when this method is retried*/ 
		currentGroupIndex = nextGroupIndex;
		saveToSession();
		
		if(nextPageDescription == null){
			nextPageDescription = new GroupDescription("");
			nextPageDescription.setLastGroup(true);
		}
		
		if(executionReport != null){
			nextPageDescription.getNotifications().addAll(executionReport.getMessages());
		}
		
		return nextPageDescription;
	}

	private GroupDescription goToUploadNextGroupImpl(Map<String,String> fieldValues){
		int nextGroupIndex = currentGroupIndex + 1;
		FormGroup currentGroup = null;
		FormGroup nextGroup = null;
		
		GroupDescription nextPageDescription = null;
		
		/* Execute "after" script for previous group*/
		if((currentGroupIndex >= 0) && (currentGroupIndex < formGroups.size())){
			currentGroup = formGroups.get(currentGroupIndex);
		}
		
		nextGroup = getGroupByIndex(nextGroupIndex);

		if(log.isDebugEnabled()){
			log.debug("currentGroup(" + currentGroupIndex + ")" + currentGroup);
		}
		
		/* Store in the scope */
		//processFieldValues(fieldValues);
		processUploadFieldValues(fieldValues);

		/* Execute after script for current group */
		ExecutionReport executionReport = null;
		if(currentGroup != null){
			
			log.debug("Executing currentGroup.getAfterGroupScript():" + currentGroup.getAfterGroupScript());

			Object retval = executeJavaScript(currentGroup.getAfterGroupScript());

			if(retval != null){
				if(retval instanceof ErrorReport){
					throw new ScriptingErrorsException((ErrorReport)retval);
				} else if(retval instanceof ExecutionReport){
					executionReport = (ExecutionReport) retval;
				} else {
					log.info("Ignoring return from afterscript " + currentGroup.getAfterGroupScript() + ":" + retval);
				}
			}
		}
				
		/* Locate next enabled group */
		while(nextGroup != null){
			if(log.isDebugEnabled()){
				log.debug("nextGroup(" + nextGroupIndex + ")" + nextGroup);
			}
			
			Object expressionValue = executeJavaScript(nextGroup.getExpression());
			if ((expressionValue != null) && !(expressionValue instanceof Boolean)) {
				throw new RuntimeException("Expression " + nextGroup.getExpression() + " does not evaluate to a boolean. It is " + expressionValue.getClass());
			} else {
				boolean enabled = expressionValue != null ? ((Boolean)expressionValue) : false;
				if(!enabled){
					/* Look for any mandatory fields in the skipped group 
					 * and set them to default values*/
					processFieldValues(makeDefaultValues(nextGroup));
					nextGroup = getGroupByIndex(++nextGroupIndex);
					
					continue;
				}
			}
			
			/* Execute before script */
			executeJavaScript(nextGroup.getBeforeGroupScript());
			List<FormProperty> groupProperties = propertiesByGroup.get(nextGroup.getName());
			
			if(groupProperties == null){
				nextGroup = getGroupByIndex(++nextGroupIndex);
				
				continue;
			}
			
			nextPageDescription = buildPageDescription(nextGroupIndex);
			
			break;
		}
		
		/* Advance even when we do nothing so that we do nothing even when this method is retried*/ 
		currentGroupIndex = nextGroupIndex;
		saveToSession();
		
		if(nextPageDescription == null){
			nextPageDescription = new GroupDescription("");
			nextPageDescription.setLastGroup(true);
		}
		
		if(executionReport != null){
			nextPageDescription.getNotifications().addAll(executionReport.getMessages());
		}
		
		return nextPageDescription;
	}

	
	public FormGroup getGroupByIndex(int index) {
		FormGroup nextGroup;
		if(index < formGroups.size()){
			nextGroup = formGroups.get(index);
		} else {
			nextGroup = null;
		}
		return nextGroup;
	}

	private Map<String, String> makeDefaultValues(FormGroup formGroup) {
		Map<String,String> fieldValues = new HashMap<>();

		for(FormProperty property: propertiesByGroup.get(formGroup.getName())){
			if(property.isRequired()){
				Object value = makeDefaultValue(property);
				
				fieldValues.put(property.getId(), value.toString());
			}
		}
		
		return fieldValues;
	}

	public Object makeDefaultValue(FormProperty property) {
		Object value = null;
		
		FormType type = property.getType();
		
		if(type instanceof BooleanFormType){
			value = false;
		} else if(type instanceof DateFormType){
			value = ((DateFormType) type).convertModelValueToFormValue(new Date());
		} else if(type instanceof DoubleFormType){
			value = new Double(0.0);
		} else if(type instanceof DynamicEnumFormType || type instanceof EnumFormType){
			Map<String,String> values = listAllowedValues(property.getType());			
			if(values != null && !values.isEmpty()){
				/* Pick the first key */
				for(String ek:values.keySet()){
					value = ek;
					break;
				}
			} else {
				log.error("Form Property " + property.getName() + " is mandatory but values are not defined or is empty.");
				value = "undefined";
			}
		} else if(type instanceof LongFormType){
			value = new Long(0);
		} else if(type instanceof StringFormType){
			value = "";
		} else {
			value = "";
		}
		return value;
	}

	private String makeStringValue(FormProperty property, Object value) {
		FormType type = property.getType();
		
		if((type instanceof DateFormType)&&(value instanceof Date)){
			value = ((DateFormType) type).convertModelValueToFormValue(value);
		}
		
		return value != null ? value.toString() : null;
	}

	private void processFieldValues(Map<String, String> fieldValues) {
		if(fieldValues == null){
			return;
		}
		
		if(log.isDebugEnabled()){
			try {
				log.debug("Processing field values:" + new ObjectMapper().writeValueAsString(fieldValues));
			} catch (JsonProcessingException ex) {
				log.debug("Failed to get a string representation of field values" , ex);
			}
		}

		Map<String,FormProperty> propertyByName = new HashMap<String, FormProperty>();
		for(FormProperty property: formData.getFormProperties()){
			propertyByName.put(property.getId(),property);
		}
		
		Map<String,Object> variables = new HashMap<String, Object>();
		
		for(String property:fieldValues.keySet()){
			FormProperty formProperty = propertyByName.get(property);
			String variable = formProperty != null ? formProperty.getVariable() : null; 
			if(variable== null||variable.equals("")){
				variable = property;
			}

			if(fieldValues.containsKey(property)){
				variables.put(variable,fieldValues.get(property));
				if(!variable.equals(property)){
					variables.put(property,fieldValues.get(property));
				}
			}
		}
		
		if(!variables.isEmpty()){
			setVariables(variables);
		}
	}

	private void processUploadFieldValues(Map<String, String> fieldValues) {
		if(fieldValues == null){
			return;
		}
		
		if(log.isDebugEnabled()){
			try {
				log.debug("Processing field values:" + new ObjectMapper().writeValueAsString(fieldValues));
			} catch (JsonProcessingException ex) {
				log.debug("Failed to get a string representation of field values" , ex);
			}
		}

		Map<String,FormProperty> propertyByName = new HashMap<String, FormProperty>();
		for(FormProperty property: formData.getFormProperties()){
			propertyByName.put(property.getId(),property);
		}
		
		Map<String,Object> variables = new HashMap<String, Object>();
		
		for(String property:fieldValues.keySet()){
			FormProperty formProperty = propertyByName.get(property);
			String variable = formProperty != null ? formProperty.getVariable() : null; 
			if(variable== null||variable.equals("")){
				variable = property;
			}

			if(fieldValues.containsKey(property)){
				variables.put(variable,fieldValues.get(property));
				if(!variable.equals(property)){
					variables.put(property,fieldValues.get(property));
				}
			}
		}
		
		if(!variables.isEmpty()){
			//setVariables(variables,reset);
			setUploadVariables(variables);
		}
	}

	
	private GroupDescription buildPageDescription(int groupIndex) {
		FormGroup group = formGroups.get(groupIndex);
		List<FormProperty> groupProperties = propertiesByGroup.get(group.getName());
		GroupDescription nextPageDescription = new GroupDescription(group.getName());
		nextPageDescription.setLastGroup(groupIndex == (formGroups.size()-1));

		for(FormProperty formProperty:groupProperties){
			PropertyDescription propertyDescription = new PropertyDescription(formProperty.getId(),formProperty.getName(),formProperty.getType());
			
			log.debug("Building property description:" + formProperty.getId() + ":" + formProperty.getName() + ":" + formProperty.getType());
			
			if(formProperty.getShowIfExpression() != null){
				Object expressionValue = executeJavaScript(formProperty.getShowIfExpression());
				if ((expressionValue != null) && !(expressionValue instanceof Boolean)) {
					throw new RuntimeException("Expression " + formProperty.getShowIfExpression() + " does not evaluate to a boolean. It is " + expressionValue.getClass());
				} else {
					boolean enabled = expressionValue != null ? ((Boolean)expressionValue) : false;
					if(!enabled){
						/* If the field is mandatory,  
						 * and set it to default values*/
						if(formProperty.isRequired()){
							Map<String,String> defaultValues = new HashMap<>();
							defaultValues.put(formProperty.getId(), makeDefaultValue(formProperty).toString());
							processFieldValues(defaultValues);
						}
						continue;
					}
				}
			}
			
			propertyDescription.setAllowedValues(listAllowedValues(formProperty.getType()));
			
			if(formProperty.getType() instanceof DateFormType){
				propertyDescription.setFormat((String)((DateFormType)formProperty.getType()).getInformation("datePattern"));
			}
			propertyDescription.setMandatory(formProperty.isRequired());
			propertyDescription.setReadOnly(!formProperty.isWritable());
			
			if(formProperty.getVariable() != null){
				propertyDescription.setValue(executeJavaScript(formProperty.getVariable()));
			}
			if(formProperty.getTab() != null){
				propertyDescription.setTab(formProperty.getTab());
			}
			nextPageDescription.getProperties().add(propertyDescription);
		}
		return nextPageDescription;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String,String> listAllowedValues(FormType formType) {		
		Map<String,String> retval = null;
		
		if(formType instanceof DynamicEnumFormType){
			String controllingPV = ((DynamicEnumFormType)formType).getControllingPV();
	
			/*Object controllingPVValue = variables.get(controllingPV);*/
			Object controllingPVValue = executeJavaScript(controllingPV);
			
			if(controllingPVValue == null){
				log.warn( "controllingPV " + controllingPV + " has a value of null. Ignoring.");
			} else if(!((controllingPVValue instanceof List<?>)||(controllingPVValue instanceof Map<?,?>))){
				log.warn( "controllingPV " + controllingPV + " has a value with an incomaptible type " + controllingPVValue.getClass() + ":" + controllingPVValue + ". Ignoring.");
			} else {
				Map<String,String> values = DynamicEnumFormType.ensureMap(controllingPVValue);
		
				if (values != null) {
					retval = new TreeMap<String,String>(values);
				}
				
			}
			if(retval == null){
				retval = new TreeMap<String,String>();
			}
			
		} else if(formType instanceof EnumFormType){
			Map<String,String> values = (Map<String, String>) ((EnumFormType)formType).getInformation("values");
			retval = values;
		}
		
		return retval;
	}

	public Object executeJavaScript(String script) {
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		return commandExecutor.execute(new Command<Object>() {
			@Override
			public Object execute(CommandContext commandContext) {		
				if((script != null) && !(script.equals(""))) {			
					ScriptingEngines scriptingEngines = org.activiti.engine.impl.context.Context.getProcessEngineConfiguration().getScriptingEngines();
					try{
						if(scope instanceof PreProcessInstanceVariableScope){
							return scriptingEngines.evaluate(script, "javascript",((PreProcessInstanceVariableScope)scope).getBindingsAdapter());
						} else {
							return scriptingEngines.evaluate(script, "javascript",scope,true);
						}
					}catch(Throwable th){
						log.debug("Supressing scripting layer exception:" + th.getMessage(), th);
						return null;
					}
				} else {
					return new Boolean(true);
				}
			}
		});
	}
	
	public static interface FormSubmissionAction<R>{
		public R submitForm(Map<String,String> fieldValues);
	}
	
	public Object submitForm(FormSubmissionAction<?> submissionAction){

		/*
		 * Preservation-Step-1.
		 *  
		 * This map will also serve as a record of state. This will be used at
		 * the end to set variables so that any variable values programmatically
		 * computed in various script blocks such as before and after script
		 * blocks are preserved
		 * 
		 * See Preservation-Step-2 and Preservation-Step-3  below.
		 */
		Map<String,Object> scopeVariables = getVariables();
		
		Map<String,Object> scopeVariablesWithFormExposure = new HashMap<>();

		List<FormProperty> properties = formData.getFormProperties();
		
		Map<String, String> formFieldValues = new HashMap<>();
		log.debug("Inside default form handler");
		for(FormProperty property:properties){
			if(!property.isWritable()){
				continue;
			}
			
			String propertyVariable = property.getVariable() != null ? property.getVariable() : property.getId();
			Object value = scopeVariables.get(propertyVariable);
			
			if(value != null){
				formFieldValues.put(property.getId(), makeStringValue(property,value));
				log.debug(property.getId() + " : " + makeStringValue(property,value));
				/* Preservation-Step-2 */
				scopeVariablesWithFormExposure.put(propertyVariable,scopeVariables.get(propertyVariable));
			}
		}

		 
		try{
			return submissionAction.submitForm(formFieldValues);
		} finally {
			/* Preservation-Step-3 */
			setVariables(scopeVariablesWithFormExposure,true);
		}
	}
	
	public Map<String,Object> getVariables(){
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		return commandExecutor.execute(new Command<Map<String,Object>>() {
			@Override
			public Map<String,Object> execute(CommandContext commandContext) {		
				return scope.getVariables();
			}
		});
	}
	
	private void setVariables(Map<String, Object> variables) {
		setVariables(variables,false);
	}
	
	private void setVariables(Map<String, Object> variables,boolean refreshScope) {
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		commandExecutor.execute(new Command<Boolean>() {
			@Override
			public Boolean execute(CommandContext commandContext) {
				
				VariableScope scopeToUse = scope;
				log.debug("");
				if(refreshScope && (scopeToUse instanceof ExecutionEntity)){
					scopeToUse = getExecutionEntityScope(((ExecutionEntity)scopeToUse).getId());
				}
				
				if(scopeToUse != null){
					scopeToUse.setVariables(variables);
				}
				
				return true;
			}
		});
	}
	
	private void setUploadVariables(Map<String, Object> variables) {
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		commandExecutor.execute(new Command<Boolean>() {
			@Override
			public Boolean execute(CommandContext commandContext) {
				
				VariableScope scopeToUse = scope;
				
				log.debug("Variables before setting scope variables");
				for(String key: variables.keySet()) {
					log.debug(key + " : " + variables.get(key));
				}
				
				/*Map<String, Object> scopeVariables = scopeToUse.getVariables();
				for(String scopeKey : scopeVariables.keySet()) {
					if(!variables.containsKey(scopeKey)) {
						scopeVariables.remove(scopeKey);
					}
				}
				scopeToUse.setVariables(scopeVariables);
				if((scopeToUse instanceof ExecutionEntity)){
					scopeToUse = getExecutionEntityScope(((ExecutionEntity)scopeToUse).getId());
				}*/
				
				if(scopeToUse != null){
					scopeToUse.setVariables(variables);
				}
				
				return true;
			}
		});
	}
	
	private VariableScope getExecutionEntityScope(String executionId){
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		ExecutionEntity execution = (ExecutionEntity) runtimeService.createExecutionQuery().executionId(executionId).singleResult();
		
		return execution;
	}
	
	private Map<String, List<org.activiti.engine.form.FormProperty>> partitionPropertiesIntoGroups(List<org.activiti.engine.form.FormProperty> formProperties) {
		Map<String, List<org.activiti.engine.form.FormProperty>> propertiesByGroup = new HashMap<String, List<org.activiti.engine.form.FormProperty>>();
		
		for(org.activiti.engine.form.FormProperty formProperty:formProperties){
			String group = formProperty.getGroup();
			if(group == null){
				group = "default";
			}
			if(!propertiesByGroup.containsKey(group)){
				propertiesByGroup.put(group,new ArrayList<org.activiti.engine.form.FormProperty>());
			}
			
			propertiesByGroup.get(group).add(formProperty);
		}
		return propertiesByGroup;
	}

	private static FormData getFormDataForTaskId(String taskId) {
		FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getFormService();
		FormData formData = formService.getTaskFormData(taskId);
		return formData;
	}
	
	private static FormData getFormDataForDeploymentId(String deploymentId) {
		FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getFormService();
		RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getRepositoryService();

		ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).singleResult();
		FormData formData = formService.getStartFormData(definition.getId());
		return formData;
	}
	
	public class GroupDescription {
		private String fillSessionId;
		private String name;
		private List<PropertyDescription> properties = new ArrayList<PropertyDescription>();
		private boolean lastGroup;
		private List<String> messages = new ArrayList<>();
		private List<String> notifications = new ArrayList<>();
		
		public GroupDescription(String name) {
			this.name = name;
			setFillSessionId(FormFillerUtil.this.fillSessionId);
		}

		public void setLastGroup(boolean lastGroup) {
			this.lastGroup = lastGroup;
		}
		
		public boolean isLastGroup(){
			return lastGroup;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<PropertyDescription> getProperties() {
			return properties;
		}

		public void setProperties(List<PropertyDescription> properties) {
			this.properties = properties;
		}

		public String getFillSessionId() {
			return fillSessionId;
		}

		public void setFillSessionId(String fillSessionId) {
			this.fillSessionId = fillSessionId;
		}
		
		public String toString(){
			return "GroupDescription(" + name + "," + fillSessionId + "," + lastGroup + "," + (properties != null ? properties.stream().map(Object::toString).collect(Collectors.joining(",")) : "")+ ")";
		}

		public List<String> getMessages() {
			return messages;
		}

		public void setMessages(List<String> messages) {
			this.messages = messages;
		}

		public List<String> getNotifications() {
			return notifications;
		}

		public void setNotifications(List<String> notifications) {
			this.notifications = notifications;
		}
	}

	public class PropertyDescription {
		private String id;
		private String name;
		private String type;
		private String format;
		private String tab;
		private Map<String,String> allowedValues;
		private boolean mandatory = false;
		private Object value;
		private boolean readOnly;
		
		public PropertyDescription(String id,String name, FormType type) {
			this.id = id;
			this.name = name;
			this.type = type.getName();
		}
		public String getId() {
			return id;
		}
		public String getTab() {
			return tab;
		}
		public void setTab(String tab) {
			this.tab = tab;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Map<String,String> getAllowedValues() {
			return allowedValues;
		}
		public void setAllowedValues(Map<String,String> allowedValues) {
			this.allowedValues = allowedValues;
		}		
		public String toString(){
			return "PropertyDescription(" + name+ "," + type + "," + (allowedValues != null ? allowedValues : "") + ")";
		}
		public boolean isMandatory() {
			return mandatory;
		}
		public void setMandatory(boolean mandatory) {
			this.mandatory = mandatory;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public boolean isReadOnly() {
			return readOnly;
		}
		public void setReadOnly(boolean readOnly) {
			this.readOnly = readOnly;
		}
		public String getFormat() {
			return format;
		}
		public void setFormat(String format) {
			this.format = format;
		}
	}
	
	public class FormFillerUtilSessionAspect implements Serializable{

		private static final long serialVersionUID = 1L;
		
		private boolean inTaskContext;
		private String underlyingId;
		private VariableScope scope;
		private String executionId;
		private String fillSessionId;
		private int currentGroupIndex;
		
		public FormFillerUtilSessionAspect(){
		
		}
		
		public FormFillerUtilSessionAspect(boolean ignored){
			setInTaskContext(FormFillerUtil.this.inTaskContext);
			setUnderlyingId(FormFillerUtil.this.underlyingId);
			if(FormFillerUtil.this.scope instanceof PreProcessInstanceVariableScope){
				this.setScope(FormFillerUtil.this.scope);
			} else {
				this.setExecutionId(((ExecutionEntity)FormFillerUtil.this.scope).getId());
			}
			this.setFillSessionId(FormFillerUtil.this.fillSessionId);
			this.setCurrentGroupIndex(FormFillerUtil.this.currentGroupIndex);
		}

		public VariableScope getScope() {
			return scope;
		}

		public void setScope(VariableScope scope) {
			this.scope = scope;
		}

		public String getFillSessionId() {
			return fillSessionId;
		}

		public void setFillSessionId(String fillSessionId) {
			this.fillSessionId = fillSessionId;
		}

		public int getCurrentGroupIndex() {
			return currentGroupIndex;
		}

		public void setCurrentGroupIndex(int currentGroupIndex) {
			this.currentGroupIndex = currentGroupIndex;
		}

		public String getExecutionId() {
			return executionId;
		}

		public void setExecutionId(String executionId) {
			this.executionId = executionId;
		}

		public boolean isInTaskContext() {
			return inTaskContext;
		}

		public void setInTaskContext(boolean inTaskContext) {
			this.inTaskContext = inTaskContext;
		}

		public String getUnderlyingId() {
			return underlyingId;
		}

		public void setUnderlyingId(String underlyingId) {
			this.underlyingId = underlyingId;
		}
	}
}
