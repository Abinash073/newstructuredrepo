package com.rlogistics.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.*;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RLogisticsAttachment;
import com.rlogistics.master.identity.impl.interceptor.Report;
import com.rlogistics.master.identity.impl.interceptor.impl.cmd.CreateReportQueryCmd;
import com.rlogistics.master.identity.impl.interceptor.impl.cmd.SaveReportCmd;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@RestController
@Slf4j
public class ReportingController extends RLogisticsResource {
    /**
     * Author  : Adarsh
     *
     * @Date : 12th-sep-2018
     */

    /**
     * Task: invoking methods to insert data in <table></table> at the time of ticket creation and complitation
     */


    /**
     * Instantiating required classes for properties
     */
    TaskResource taskResource = new TaskResource();
    List<ProcessResultWithVariables> retval = new ArrayList<>();
    ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
    /**
     * Methods to add/insert/update in reporting table
     */

    MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
    CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
    //TODO
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    ProcessEngineConfigurationImpl processEngineConfiguration;

    @Qualifier("reportExcel")
    @Autowired
    ReportExcelUtil util;

    @Qualifier("misExcel")
    @Autowired
    ReportExcelUtil misUtil;

    private static String currentDate() {
        DateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String tt = s.format(date);
        DateTime dateTimeIndia = DateTime.parse(tt);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        return fmt.print(dateTimeIndia);

    }

    /**
     * Can be used to get earlier days
     */
    private static String beforeDate(int days) {
        DateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String tt = s.format(date);
        DateTime dateTimeIndia = DateTime.parse(tt);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        return fmt.print(dateTimeIndia.minusDays(days));
    }

    /**
     * Call it from activiti to add assignee name in JSONarray format
     *
     * @param processId
     * @param assignee
     */
    @RequestMapping(value = "/test/addAssignee", method = RequestMethod.POST)
    public static void addAssignee(@NotNull @RequestParam("a") String processId, @RequestParam("b") String assignee) {
        log.debug(processId + "-------" + assignee);
        JSONArray jsonArray = new JSONArray();
        JSONParser jsonParser = new JSONParser();
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine()
                .getRuntimeService();
        String aa = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
        log.debug(aa);
        String a = String.valueOf(runtimeService.getVariable(processId, "rlInvolvedUser"));
        log.debug("aa");
        log.debug(a);
        try {
            if (a == null || a.equals("null")) {
                jsonArray.add(0, assignee);
            } else {
                jsonArray = (JSONArray) jsonParser.parse(runtimeService.getVariable(processId, "rlInvolvedUser").toString());
                if (!String.valueOf(jsonArray.get(jsonArray.size() - 1)).equals(assignee)) {

                    jsonArray.add(jsonArray.size(), assignee);
                }
            }
            log.debug("ASSIGNEE NAMES" + " -----------> " + jsonArray.toString());
            runtimeService.setVariable(processId, "rlInvolvedUser", jsonArray.toString());
        } catch (Exception e) {

            log.debug("duhhhhhhhh.." + e);
        }
    }

    @RequestMapping(value = "/get/variables")
    public static void addVariables(@NotNull @RequestParam(value = "a") String processId) {
        HistoricVariableInstanceQuery query = ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricVariableInstanceQuery().executionId(processId);
        List<HistoricVariableInstance> instances = query.list();
        HashMap<Object, Object> a = instances
                .stream()
                .collect
                        (HashMap::new,
                                (hashMap, historicVariableInstance) -> hashMap.put(historicVariableInstance.getVariableName()
                                        , historicVariableInstance.getValue())
                                , HashMap::putAll
                        );
        log.debug("Value" + a);

    }


    //Filter the query object based on input from front-end
    public HistoricProcessInstanceQuery setQueryVariable(HistoricProcessInstanceQuery hQuery, ArrayList<TaskQueryParameter> query) {

        for (TaskQueryParameter tqp : query) {
            String variable = tqp.getVariable();
            String value = tqp.getValue();

            boolean isNumber = taskResource.isNumber(value);
            boolean isDate = taskResource.isDate(value);

            if (isNumber || isDate) {
                String op = taskResource.getOp(value);
                Object valueObj = isNumber ? taskResource.getNumber(value) : taskResource.getDate(value);
                if (op == null) {
                    hQuery.variableValueEquals(variable, valueObj);
                } else if (op.equals(">")) {
                    hQuery.variableValueGreaterThan(variable, valueObj);
                } else if (op.equals(">=")) {
                    hQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                } else if (op.equals("<")) {
                    hQuery.variableValueLessThan(variable, valueObj);
                } else if (op.equals("<=")) {
                    hQuery.variableValueLessThanOrEqual(variable, valueObj);
                }
            } else {

                if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                    hQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    hQuery.variableValueEqualsIgnoreCase(variable, value);
                }
            }
        }
        return hQuery;
    }

    /**
     * Report for manager /ALL Details
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param queryJson
     * @param variableOfIntrest
     * @param roleCode
     * @param pathVariables
     * @return
     */
    @RequestMapping(value = "/report/tickets", method = RequestMethod.POST, produces = "application/json")
    public ProcessTaskWithPagination reportingDetails(HttpServletRequest httpServletRequest,
                                                      HttpServletResponse httpServletResponse,
                                                      @RequestParam(value = "Query", required = false) String queryJson,
                                                      @RequestParam(value = "variableOfIntrest", defaultValue = "[]") String variableOfIntrest,
                                                      @RequestParam(value = "roleCode", required = false) String roleCode,
                                                      @PathVariable Map<String, String> pathVariables
    ) {

        try {

            // if (!(beforeMethodInvocation(httpServletRequest, httpServletResponse))) {
            //   return null;
            //}
            ObjectMapper mapper = new ObjectMapper();
            /**
             * Base Query Object
             */
            ArrayList<TaskQueryParameter> query = mapper.readValue(URLDecoder.decode(queryJson),
                    mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            /**
             * All the variables
             */
            Set<String> variablesOfInterest = parseSet(variableOfIntrest);
            /**
             * Can write custome SQL quaries in below lines
             */
            //NativeHistoricProcessInstanceQuery processInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getHistoryService().createNativeHistoricProcessInstanceQuery();
            //List<HistoricProcessInstance> a = processInstance.sql("SQL Queries").list();

            /**
             * We will do it in programmatic way
             */
            //Including All the VariableOfIntrest
            HistoricProcessInstanceQuery historicProcessInstanceQuery = (ProcessEngines.getDefaultProcessEngine()).getHistoryService().createHistoricProcessInstanceQuery().includeProcessVariablesOfInterest(variablesOfInterest);
            historicProcessInstanceQuery = setQueryVariable(historicProcessInstanceQuery, query);

            //Executing the query getting the historicProcessInstance
            List<HistoricProcessInstance> historicProcessInstances;
            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                historicProcessInstances = historicProcessInstanceQuery.listPage(firstResult, maxResults);
            } else {
                historicProcessInstances = historicProcessInstanceQuery.list();
            }

            //Getting The Count

            int count = Math.toIntExact(historicProcessInstanceQuery.count());

            /**
             * Looping in Each process instance to get variableOfIntrest
             */
            for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
                //log.debug("TicketNo :"
                //  + String.valueOf(historicProcessInstance.getProcessVariables().get("rlTicketNo")));
                retval.add(new ProcessResultWithVariables(historicProcessInstance, variablesOfInterest.isEmpty()
                        ? historicProcessInstance.getProcessVariables()
                        : historicProcessInstance.getProcessVariables().entrySet().stream()
                        .filter(entry -> variablesOfInterest.contains(entry.getKey())).collect(HashMap::new,
                                (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll)));
            }


            processTaskWithPagination.setData(retval);
            processTaskWithPagination.setTotalRecords(count);
            return processTaskWithPagination;


        } catch (Exception e) {

            httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            //log.debug("Error in API" + e);
            return null;

        }

    }

    //To be used @ time of ticket creation
    //Syncronizing the Method
    @RequestMapping(value = "/test/report")
    synchronized public void addReportDetails(@RequestParam(value = "loc") String location, @RequestParam(value = "status") String status, @RequestParam(value = "re") String retailer, String... date) {
        log.debug("inside addReportDetails");
        CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();

        //log.debug(date[0]);
        if (status.equals("add")) {
            //Fetching the prvious date open ticket
            ///masterdataService.createReportQuery().location(location).date(beforeDate(1).toString()).retailer(retailer).singleResult();
            //check if data is there for specfic date for specific location & retailer
            int countForSpecificDate = 0;
            try {
                countForSpecificDate = (int) masterdataService.createReportQuery().location(location).date(currentDate()).retailer(retailer).count();
            } catch (Exception e) {
                log.debug("a" + e);
            }

            if (countForSpecificDate == 0) {
                Report report = masterdataService.newReport();
                report.setId(UUID.randomUUID().toString());
                report.setDate(currentDate());
                report.setLocation(location);
                //Report temp = masterdataService.createReportQuery().location(location).date(beforeDate(1)).retailer(retailer).singleResult();
                report.setTicketCreated(1);
                report.setOpenTicket(1);
                report.setRetailer(retailer);
                masterdataService.saveReport(report);
            } else {
                //If record is already present then update
                Report.ReportQuery report = commandExecutor.execute(new CreateReportQueryCmd());
                Report report1 = report.retailer(retailer).date(currentDate()).location(location).singleResult();
                //Record if not found for update
                if (report1 == null) {
                    //Add response
                }
                //Increasing Opened Ticket
                AtomicIntegerImpl atomicInteger = new AtomicIntegerImpl(report1.getOpenTicket());
                atomicInteger.increament();
                report1.setOpenTicket(atomicInteger.getValue());


                //Increasing total ticket
                AtomicIntegerImpl atomicIntegerForTotalTicket = new AtomicIntegerImpl(report1.getTicketCreated());
                atomicIntegerForTotalTicket.increament();
                report1.setTicketCreated(atomicIntegerForTotalTicket.getValue());

                try {
                    commandExecutor.execute(new SaveReportCmd(report1));
                } catch (Exception e) {
                    log.debug("Some Error while updating" + e);
                }


            }
        }

        //If ticket is geting closed
        else if (status.equals("close")) {
            log.debug(date[0]);
            int count = (int) masterdataService.createReportQuery().location(location).date(date[0]).retailer(retailer).count();
            if (count == 0) {
                Report report = masterdataService.newReport();
                report.setId(UUID.randomUUID().toString());
                report.setDate(date[0]);
                report.setLocation(location);
                report.setClosedTicket(1);
                report.setRetailer(retailer);
                masterdataService.saveReport(report);

            } else {
                Report.ReportQuery report = commandExecutor.execute(new CreateReportQueryCmd());
                Report report1 = report.retailer(retailer).date(date[0]).location(location).singleResult();
                if (report1 == null) {

                }

                AtomicIntegerImpl atomicInteger = new AtomicIntegerImpl(report1.getClosedTicket());
                atomicInteger.increament();
                report1.setClosedTicket(atomicInteger.getValue());

                AtomicIntegerImpl atomicInteger1 = new AtomicIntegerImpl(report1.getOpenTicket());
                atomicInteger1.decrement();
                report1.setOpenTicket(atomicInteger1.getValue() < 0 ? 0 : atomicInteger1.getValue());

                try {
                    commandExecutor.execute(new SaveReportCmd(report1));
                } catch (Exception e) {
                    log.debug("Error while update" + e);
                }

            }


        }

    }

    @RequestMapping(value = "/get/report", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private ReportResultWithCount getReportDetails(@RequestParam(value = "query") String query,
                                                   @RequestParam(value = "first", defaultValue = "") String first,
                                                   @RequestParam(value = "last", defaultValue = "") String last) {
        ReportResultWithCount resultWithCount = new ReportResultWithCount();
        String location = "";
        String retailer = "";
        String startDate = "";
        String endDate = "";
        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
				.getDefaultProcessEngine().getProcessEngineConfiguration();
        SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
        try {

            Map queryObject = objectMapper.readValue(query, Map.class);
            if (queryObject.containsKey("location")) {
                location = queryObject.get("location").toString();
            }
            if (queryObject.containsKey("retailer")) {
                retailer = queryObject.get("retailer").toString();
            }
            if (queryObject.containsKey("startDate") && queryObject.containsKey("endDate")) {
                startDate = queryObject.get("startDate").toString();
                endDate = queryObject.get("endDate").toString();
            }
            ReportingMapper reportingMapper = sqlSession.getMapper(ReportingMapper.class);
            List<ReportingResult> report = reportingMapper.getReport(location, retailer, startDate, endDate, first, last);
            int count = reportingMapper.getReportCount(location, retailer, startDate, endDate);
            resultWithCount.setData(report);
            resultWithCount.setSuccess(true);
            resultWithCount.setTotalCount(count);


        } catch (Exception e) {
            log.debug("Error" + e);
        } finally {
            sqlSession.close();
            return resultWithCount;
        }


    }

    private ResponseWithCount getReportData(String queryJson, String dateRange, String variables, Map<String, String> pathVariables, boolean isCountRequired) {
        SqlSession sqlSession = processEngineConfiguration
                .getSqlSessionFactory()
                .openSession();
        ResponseWithCount restResponse = new ResponseWithCount();
        try {

            Map<String, String> filterMap = parseMap(queryJson);
            Map<String, String> dateRangeMap = parseMap(dateRange);
            Set<String> variablesMap = this.parseSet(variables);

            String startTime = "";
            String endTime = "";
            if (dateRangeMap != null && !dateRangeMap.isEmpty()) {
                startTime = String.valueOf(dateRangeMap.get("startDate"));
                endTime = String.valueOf(dateRangeMap.get("endDate"));
            }

            HistoricDataQueryMapper mapper = sqlSession.getMapper(HistoricDataQueryMapper.class);
            List<HistoricQueryData> data = mapper.getHistoricData(startTime, endTime, filterMap, variablesMap, pathVariables);
            int count = 0;
            if (isCountRequired) {
                count = mapper.getHistoricDataCount(startTime, endTime, filterMap, variablesMap, pathVariables);
            } else {
                log.debug("Count not required,Ignoring");
            }
            restResponse.setData(data);
            restResponse.setTotalCount(count);
            restResponse.setSuccess(true);
            restResponse.setMessage("Fetched");
            return restResponse;

        } catch (Exception e) {
            log.debug("Error while Fetching" + e);

            restResponse.setMessage("Failed");
            restResponse.setSuccess(false);
            return restResponse;
        } finally {
            sqlSession.close();
        }

    }

    @SuppressWarnings("deprecation")
    @RequestMapping(value = {"get/completed-tickets/report", "get/completed-tickets/report/{firstResult}/{maxResults}"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseWithCount filterCompletedTickets(HttpServletRequest httpRequest,
                                                    HttpServletResponse httpResponse,
                                                    @RequestParam("query") String queryJson,
                                                    @RequestParam(value = "dateRange") String dateRange,
                                                    @RequestParam(value = "variablesOfIntrest") String variables,
                                                    @PathVariable Map<String, String> pathVariables) {

        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
            return null;
        }

        return this.getReportData(queryJson, dateRange, variables, pathVariables, true);

    }


    @RequestMapping(value = "get/excel/complted-tickets", method = RequestMethod.POST)
    private RestResponse getCompltedTicketsExcel(HttpServletRequest httpRequest,
                                                 HttpServletResponse httpResponse,
                                                 @RequestParam("query") String queryJson,
                                                 @RequestParam(value = "dateRange") String dateRange,
                                                 @RequestParam(value = "variablesOfIntrest") String variables,
                                                 @PathVariable Map<String, String> pathVariables) {
        try {
            log.debug("Inside Excel");
            RestResponse response = new RestResponse();

            //List to be Inserted In excel
            List<HistoricQueryData> excelData = (List<HistoricQueryData>) this.getReportData(queryJson, dateRange, variables, pathVariables, false).getData();
            //Adding Header
            ReportExcelUtilHelper utilHelper = new ReportExcelUtilHelper();

            //Initialize  excel
            byte[] resource =
                    util
                            .initExcel("Bizlog_Value_Chain")
                            .createHeader(utilHelper.getHeader())
                            .setStyling()
                            .insertData(excelData, "completed")
                            .saveExcel()
                            .getExcel();

            RLogisticsAttachment attachment = null;

            if (masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("abc").key3("bca")
                    .singleResult() != null) {
                attachment = masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("abc").key3("bca")
                        .singleResult();
            }
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();


                newAttachment.setKey1("search");
                newAttachment.setKey2("abc");
                newAttachment.setKey3("bca");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Search_Results.xlsx");

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(resource);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("search");
                newAttachment.setKey2("abc");
                newAttachment.setKey3("bca");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                String attachmentName = "Search_Results.xlsx";
                newAttachment.setName(attachmentName);
                log.debug("key value attachment is NOT null here" + newAttachment.getKey3());

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(resource);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }
            response.setMessage("Excel Created");
            return response;


        } catch (Exception e) {
            log.debug("Error" + e);
        }

        return null;


    }

    @RequestMapping(value = "get/mis", method = RequestMethod.POST)
    private boolean getMisReport(@RequestParam(value = "startDate", required = false, defaultValue = "") String startDate,
                                 @RequestParam(value = "endDate", required = false, defaultValue = "") String endDate) {
    	ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
				.getDefaultProcessEngine().getProcessEngineConfiguration();
        SqlSession session = processEngineConfiguration.getSqlSessionFactory().openSession();

        try {
            MisReportMapper mapper = session.getMapper(MisReportMapper.class);
            List<MisResult> results = mapper.getMisReport(startDate, endDate);
            MisExcelUtilHelper helper = new MisExcelUtilHelper();

            byte[] excelData =
                    misUtil.initExcel("MIS-REPORT")
                            .createHeader(helper.getCvMap())
                            .setStyling()
                            .insertData(results, "mis")
                            .saveExcel()
                            .getExcel();

            RLogisticsAttachment attachment = null;

            if (masterdataService.createRLogisticsAttachmentQuery().key1("mis").key2("report").key3("daily")
                    .singleResult() != null) {
                attachment = masterdataService.createRLogisticsAttachmentQuery().key1("mis").key2("report").key3("daily")
                        .singleResult();
            }
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();


                newAttachment.setKey1("mis");
                newAttachment.setKey2("report");
                newAttachment.setKey3("daily");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Mis_Results.xlsx");

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(excelData);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("mis");
                newAttachment.setKey2("report");
                newAttachment.setKey3("daily");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Mis_Results.xlsx");
                log.debug("key value attachment is NOT null here" + newAttachment.getKey3());

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(excelData);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }


        } catch (Exception e) {
            log.debug("Error" + e);
        } finally {
            session.close();
        }

        return true;
    }


}
