package com.rlogistics.rest;



import java.util.concurrent.atomic.AtomicInteger;

public class  AtomicIntegerImpl {
    AtomicInteger atomicInteger;

    AtomicIntegerImpl(int a){
        atomicInteger = new AtomicInteger(a);
    }
    public int getValue(){
        return atomicInteger.get();
    }
    public void increament(){
        while (true){
            int existingValue = getValue();
            int newValue = existingValue+1;
            if(atomicInteger.compareAndSet(existingValue,newValue)){
                return;
            }
        }
    }

    public void decrement(){
        while (true){
            int existingValue = getValue();
            int newValue = existingValue-1;
            if(atomicInteger.compareAndSet(existingValue,newValue)){
                return;
            }
        }
    }


}
