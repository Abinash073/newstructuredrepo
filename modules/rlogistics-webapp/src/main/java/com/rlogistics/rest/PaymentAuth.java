package com.rlogistics.rest;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;

@RestController
public class PaymentAuth {
	public static Logger log = LoggerFactory.getLogger(PaymentAuth.class);
	private String CASHFREE_AUTHORIZE = "/payout/v1/authorize";
	private String CASHFREE_VALIDATE = "/payout/v1/verifyToken";
	private static String CASHFREETOKEN = "cashfreetoken";
	public final static String BIZLOG_PRODUCTION_IP = "172.31.34.213";
	public final static String BIZLOG_TEST_IP = "172.31.32.41";
	public final static String BIZLOG_DEV_IP = "172.31.21.190";
	// public final static String BIZLOG_TEST_IP = "172.31.13.246";
	// public final static String BIZLOG_DEV_IP = "172.31.10.210";

	private static String CASHFREE_PROD_URL = "https://payout-api.cashfree.com";
	private static String CASHFREE_PROD_CLIENT_ID = "CF9809B2U6C13K23ZMMAE";
	private static String CASHFREE_PROD_CLIENT_SECRET = "7ee3c4f28de84048866f17901b55f1562314903b";

	private static String CASHFREE_TEST_URL = "https://payout-gamma.cashfree.com";
	private static String CASHFREE_TEST_CLIENT_ID = "CF3184EOE9527T1G2UYIA";
	public static String CASHFREE_TEST_CLIENT_SECRET = "d2d54a126a5343119ef780f82e0e322a5a5e2cbb";

	public synchronized boolean generateCashFreeToken() {
		Map<String, Object> restResponse = new HashMap<>();
		String cashFreeBaseUrl = getCashFreeBaseUrl();
		String finalCashFreeUrl = cashFreeBaseUrl + CASHFREE_AUTHORIZE;
		try {
			String[] strings = getCashFreeAuth();
			String clientId = strings[0];
			String clientSecret = strings[1];
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(finalCashFreeUrl);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("X-Client-Id", clientId);
			httppost.addHeader("X-Client-Secret", clientSecret);
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			// RetailerApiCalls.saveToConsoleLogsTable("CASH FREE",
			// "CASH FREE:REQUEST: " + httppost.toString() + ":" + "RESPONSE: "
			// + rawResponse);
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			JSONObject dataJson = rawJson.getJSONObject("data");
			// String value2 = System.getProperty("cashfreetoken");
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				restResponse.put("success", true);
				System.setProperty(CASHFREETOKEN, dataJson.getString("token"));
			} else {
				restResponse.put("success", false);
			}

		} catch (Exception e) {
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "TOKENGEN:ERROR: in generateCashFreeToken(): " + e);
			restResponse.put("success", false);
		}

		return (boolean) restResponse.get("success");
	}

	public boolean validateCashFreeToken() {
		String token = System.getProperty(CASHFREETOKEN);
		Map<String, Object> restResponse = new HashMap<>();
		String cashFreeBaseUrl = getCashFreeBaseUrl();
		String finalCashFreeUrl = cashFreeBaseUrl + CASHFREE_VALIDATE;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(finalCashFreeUrl);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			// RetailerApiCalls.saveToConsoleLogsTable("CASH FREE",
			// "CASH FREE:REQUEST: " + httppost.toString() + ":" + "RESPONSE: "
			// + rawResponse);
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			// String value2 = System.getProperty("cashfreetoken");
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				restResponse.put("success", true);
			} else {
				restResponse.put("success", false);
			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "TOKENVER: in generateCashFreeToken(): ");

		} catch (Exception e) {
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "TOKENVER:ERROR: in generateCashFreeToken(): " + e);
			restResponse.put("success", false);
		}

		return (boolean) restResponse.get("success");
	}

	public String getCashFreeToken() {
		String rtnValue = "";
		if (validateCashFreeToken()) {
			rtnValue = System.getProperty(PaymentAuth.CASHFREETOKEN);
		} else {
			generateCashFreeToken();
			rtnValue = System.getProperty(PaymentAuth.CASHFREETOKEN);
		}
		return rtnValue;
	}

	public static String getCashFreeBaseUrl() {
		String rtnValue = "URL";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(BIZLOG_PRODUCTION_IP)) {
				rtnValue = CASHFREE_PROD_URL;
			} else {
				rtnValue = CASHFREE_TEST_URL;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

	private String[] getCashFreeAuth() {
		String[] rtnValue = new String[2];
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(BIZLOG_PRODUCTION_IP)) {
				rtnValue[0] = CASHFREE_PROD_CLIENT_ID;
				rtnValue[1] = CASHFREE_PROD_CLIENT_SECRET;
			} else {
				rtnValue[0] = CASHFREE_TEST_CLIENT_ID;
				rtnValue[1] = CASHFREE_TEST_CLIENT_SECRET;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

}
