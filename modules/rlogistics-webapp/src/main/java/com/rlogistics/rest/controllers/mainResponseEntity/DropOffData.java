package com.rlogistics.rest.controllers.mainResponseEntity;

import com.rlogistics.rest.services.ticketCreationService.responseEntity.DropOffTicketCreationData;

import java.util.ArrayList;
import java.util.List;

public class DropOffData {

    private DropOffTicketCreationData data ;
    private boolean isSuccess = false;
    private String messages;
    private String errormsg;
    private int count = 0;

    public DropOffTicketCreationData getData() {
        return data;
    }

    public void setData(DropOffTicketCreationData data) {
        this.data = data;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSucess(boolean success) {
        isSuccess = success;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
