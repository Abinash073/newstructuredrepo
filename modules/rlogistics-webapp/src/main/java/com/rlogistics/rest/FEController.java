package com.rlogistics.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rlogistics.customqueries.*;
import com.rlogistics.master.identity.*;

import com.rlogistics.master.identity.impl.cmd.CreateFeDistanceQueryCmd;
import com.rlogistics.master.identity.impl.cmd.SaveFeDistanceCmd;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.task.TaskQuery;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.FeOpenTicketStatus;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.TrackFeLocation;

import static java.lang.Math.toIntExact;

@RestController
public class FEController extends RLogisticsResource {

    private static Logger log = LoggerFactory.getLogger(FEController.class);


    //calculate total distance of an FE and Track the path
    // This API is Used By Android to update the current latlng of Fe and Time
    // To Reach their destination


    // TODO parsing and comapring is coming need to b checke Line no 174
    @RequestMapping(value = "/track/fe", produces = "application/json")
    public RestResult updateFeLocation(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                       @RequestParam("query") String queryJson) {

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        Map<String, String> queryObj = new HashMap<String, String>();
        try {
            queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String processId = "";
        String latitude = "";
        String longitude = "";
        String timeToLoc = "";
        String lastAssign = "";
        if (queryObj.containsKey("processId")) {
            if (queryObj.containsKey("processId")) {
                processId = queryObj.get("processId");
                // runtimeService.setVariable(processId, "isDeclaired","no");
            }
            if (queryObj.containsKey("latitude")) {
                latitude = queryObj.get("latitude");
            }
            if (queryObj.containsKey("longitude")) {
                longitude = queryObj.get("longitude");
            }
            if (queryObj.containsKey("timeToLoc")) {
                timeToLoc = queryObj.get("timeToLoc");
            }
            TrackFeLocation trackFe = null;
            if (!(processId.equals("") || processId.equals(null))) {
                try {
                    // lastAssign = latitude + "," +longitude;
                    log.debug("Last Assign Value :" + lastAssign);
                    String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
                    try {
                        trackFe = masterdataService.createTrackFeLocationQuery().ticketNo(ticketNo).singleResult();
                    } catch (Exception e) {
                        log.debug("Ticket No : " + ticketNo);
                        log.debug("No Data Related to This Ticket:" + e);
                        restResult.setMessage("Ticket is not there for This ProcessId " + processId);
                        restResult.setResponse("False");
                        restResult.setSuccess(false);
                        return restResult;
                    }
                    if (!(trackFe.equals("") || trackFe.equals(null))) {
                        trackFe.setCurrentLatitude(latitude);
                        trackFe.setCurrentLongitude(longitude);
                        // trackFe.setTimeToLocation(timeToLoc);
                        // trackFe.setLastAssign(lastAssign);
                        int hours = 0;
                        int minutes = 30;
                        try {
                            String ttloc = timeToLoc;
                            try {
                                if (ttloc.contains("h")) {
                                    String[] hh = ttloc.split("h");
                                    String hour = hh[0];
                                    String totalhours = hour.replaceAll("\\s", "");
                                    hours = Integer.parseInt(totalhours);
                                    ttloc = hh[1];
                                }
                                if (ttloc.contains("mins")) {
                                    // String[] mm1 = ttloc.split(" ");
                                    // String[] mm = mm1[1].split("mins");
                                    String[] mm = ttloc.split("mins");
                                    String min2 = mm[0];
                                    String totalmins = min2.replaceAll("\\s", "");
                                    minutes = minutes + Integer.parseInt(totalmins);
                                }
                            } catch (Exception e) {
                                log.debug("Error in Assiging Hour And Minutes To Variable :" + e);
                            }
                            String appointT = String
                                    .valueOf(runtimeService.getVariable(processId, "rlAppointmentDate"));
                            try {
                                DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                                DateFormat dateFormat1 = new SimpleDateFormat("MM dd yyyy hh:mm:ss");
                                Date date = new Date();
                                Calendar cal1 = Calendar.getInstance();
                                cal1.setTime(date);
                                cal1.add(Calendar.HOUR, hours);
                                cal1.add(Calendar.MINUTE, minutes);
                                String newTime1 = dateFormat1.format(cal1.getTime());
                                log.debug("Appointment date : " + appointT);
                                log.debug("Time To reach :" + ttloc);
                                log.debug("Current Date nd Time : " + date);
                                log.debug("currentTime + TTR+ThresholdTime : " + newTime1);
                                log.debug("Added Time : " + newTime1);
                                try {
                                    Date appointTime;
                                    appointTime = dateFormat.parse(appointT);
                                    System.out.println("Compare Date Result" + appointTime.compareTo(cal1.getTime()));
                                    log.debug("Compare Date Result" + appointTime.compareTo(cal1.getTime()));
                                    if (appointTime.compareTo(cal1.getTime()) < 0) {
                                        trackFe.setDelayStatus("0");
                                        log.debug("inside SetStatus 0");
                                    } else {
                                        trackFe.setDelayStatus("1");
                                        log.debug("inside SetStatus 1");
                                    }
                                } catch (Exception e) {
                                    log.debug("Error in Comparing appoint Time nd Threshold Time :" + e);
                                }
                            } catch (Exception e) {
                                log.debug("ParseExceptiooooooN:" + e);

                            }
                        } catch (Exception e) {
                            log.debug("Error in Comparing TimeTOLOC nd AppointTime : " + e);
                        }
                    } else {
                        restResult.setMessage("No Such Ticket For This ProcessId ");
                    }
                    try {
                        masterdataService.saveTrackFeLocation(trackFe);
                        restResult.setMessage("Successfully Updated");
                        restResult.setResponse("Success");
                    } catch (Exception e) {
                        System.out.println("Error in saving FeDetail " + e);
                        log.debug("Error in saving FeDetail " + e);
                    }
                } catch (Exception e) {
                    System.out.println("Error in updating FE Location Details : " + e);
                    log.debug("Error in updating FE Location Details : " + e);
                }
            }
        } else {
            restResult.setResponse("Process ID Cannot be Empty");
        }

        restResult.setSuccess(true);
        return restResult;
    }


    // API to fetch records from FE_DISTANCE
    // Krishna shastry
    // 23 Aug 2018
//    @RequestMapping(value = "/fe/gettracking-details", produces = "application/json")
//    public RestResponse getFeTrackingDetails(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
//                                             @RequestParam("location") String location, @RequestParam(value = "date") String date) {
//        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
//        RestResponse restResponse = new RestResponse();
//        try {
//            ListFeDistanceResult listFeDistanceResult = new ListFeDistanceResult();
//            List<FeDistance> feDistanceListResult = masterdataService.createFeDistanceQuery().location(location).date(date).list();
//            listFeDistanceResult.setData(feDistanceListResult);
//            listFeDistanceResult.setTotalRecords((int) masterdataService.createFeDistanceQuery().location(location).date(date).count());
//            restResponse.setSuccess(true);
//            restResponse.setResponse(listFeDistanceResult);
//            return restResponse;
//        } catch (Exception e) {
//            log.debug("Exception in fetching fe track details" + e);
//            restResponse.setSuccess(false);
//            restResponse.setResponse("Error");
//            return restResponse;
//        }
//    }

    // API to fetch latitude and longitude of FEg and Calculate distance travelled on the day
    @RequestMapping(value = "/fe/distance", produces = "application/json")
    public RestResult getFeDistance(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                    @RequestParam("feName") String feName, @RequestParam(value = "date", required = false) String date) {
        double totalDistance = 0.0;
        log.debug("fename " + feName + " date:  " + date);
        RestResult restResult = new RestResult();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
        if (feName == null) {
            restResult.setSuccess(false);
            restResult.setMessage("feName not found");
            restResult.setResponse("feName not found");
            return restResult;
        }
        if (date == null || date.isEmpty()) {
            Date date1 = new Date();
            String dateformat = "dd/MM/yyyy";
            DateFormat dateFormat = new SimpleDateFormat(dateformat);
            date = dateFormat.format(date1);
        }
        if (feName != null) {
            int countOfFename = Math.toIntExact(masterdataService.createFeDistanceQuery().feId(feName).status(1).date(date).count());
            log.debug("count" + countOfFename);
            if (countOfFename == 0) {
                restResult.setSuccess(false);
                restResult.setMessage("FE not found ");
                restResult.setResponse("FE not found");
                return restResult;
            } else if (countOfFename == 1) {
                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                FeDistance.FeDistanceQuery feDistanceQuery1 = commandExecutor.execute(new CreateFeDistanceQueryCmd());
                FeDistance FfDistanceInstance = feDistanceQuery1.feId(feName).date(date).singleResult();
                if (FfDistanceInstance == null) {
                    throw new RuntimeException("Could not find ExotelLog with Sid" + feName);
                }

                List<FeLocationTransResult> feList = com.rlogistics.customqueries.impl.CustomQueriesService.FeLocationTransCustomQueries.feLocationTransREsult(date, feName);
                for (int i = 0; i < feList.size() - 1; i++) {
                    log.debug("Latitude1 : " + feList.get(i).getLatitude());
                    log.debug("Latitude2 : " + feList.get(i + 1).getLatitude());

                    //getDistanceBetweenCoordinates(lat1,long1,lat2,long2) method returns distance between coordinates.
                    //calculate the total distance
                    // totalDistance += getDistanceBetweenCoordinates(feList.get(i).getLatitude(), feList.get(i).getLongitude(), feList.get(i + 1).getLatitude(), feList.get(i + 1).getLongitude());
                    /**
                     * changing to haversian formula
                     */
                    totalDistance += totalDistanceByHaversin(Double.parseDouble(feList.get(i).getLatitude()), Double.parseDouble(feList.get(i).getLongitude()), Double.parseDouble(feList.get(i + 1).getLatitude()), Double.parseDouble(feList.get(i + 1).getLongitude()));

                    log.debug("total distance======" + totalDistance);

                }
                FfDistanceInstance.setDistance(totalDistance + "");
                FfDistanceInstance.setDate(date);
                FfDistanceInstance.setStatus(0);
                try {
                    commandExecutor.execute(new SaveFeDistanceCmd(FfDistanceInstance));
                    restResult.setMessage("Sucesfully Updated");
                } catch (Exception e) {
                    log.debug("Failed In Updating Exotel Log" + e);
                    restResult.setMessage("Failed To Update Exotel Log");
                    return restResult;
                }

                return restResult;
            }
        }
        //List<FeLocationTransResult> feList = null;

        //Get the data from the DB and store it in an list

        restResult.setResponse("{TotalDistance:" + totalDistance + "}");
        restResult.setSuccess(true);
        return restResult;
    }

    // Aug 08 2018 Krishna
    /**
     * Modified by Adarsh >needs refactoring
     */
    // API to Send start tracking notification to the Android

    /**
     * @param httpRequest
     * @param httpResponse
     * @param userId
     * @param status
     * @param distance
     * @param latitude
     * @param longitude
     * @return
     */
    @RequestMapping(value = "/fe/start-tracking", produces = "application/json")
    public RestResult sendStartTrackingNotification(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                    @RequestParam(value = "userId", required = false) String userId,
                                                    @RequestParam(value = "status") String status,
            /*@RequestParam(value = "location" , required = false) String location,*/
                                                    @RequestParam(value = "distance") String distance,
                                                    @RequestParam(value = "latitude", defaultValue = "0.0") String latitude,
                                                    @RequestParam(value = "longitude", defaultValue = "0.0") String longitude) {
        log.debug(userId + " " + status + " " + distance + " " + latitude + " " + longitude);
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
        FeDistance feDistance = masterdataService.newFeDistance();
        RestResult restResult = new RestResult();
        /**
         * Setting Date /Need to use JODA time for Time Zone Fix
         */
        Date formattedDate = new Date();
        String strDateFormat = "dd/MM/yyyy";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String date = dateFormat.format(formattedDate);
        try {

            DeviceInfo deviceInfo = null;
            //If Angular sends array of UserIds then create list of DeviceInfo Objects
            //If Angular sends one UserId then create one Device Object
            if (userId != null && !userId.equals("")) {
                feDistance.setFeId(userId);
                ProcessUser processUser = masterdataService.createProcessUserQuery().email(userId).singleResult();
                feDistance.setLocation(processUser.getLocationId());
                feDistance.setLocationName(masterdataService.createProcessLocationQuery().id(processUser.getLocationId()).singleResult().getCity());
                feDistance.setFeName(processUser.getFirstName() != null && processUser.getLastName() != null ? processUser.getFirstName() + " " + processUser.getLastName() : processUser.getFirstName());
            }

            //If there is only onde UserId then send notification to that device
            if (status != null) {
                if (status.equals("start")) {
                    feDistance.setDate(date);
                    feDistance.setStatus(1);
                    feDistance.setStartLatitude(latitude);
                    feDistance.setStartLongitude(longitude);
                    feDistance.setDistance(distance);
                    log.debug("Sending status -------->>>>> " + status);
                    restResult.setMessage("Tracking Started");

                } else if (status.equals("stop")) {
                    feDistance.setDate(date);
                    feDistance.setStatus(0);
                    feDistance.setStopLatitude(latitude);
                    feDistance.setStopLongitude(longitude);
                    feDistance.setDistance(distance);
                    log.debug("Sending Notification status -------->>>>> " + status);
                    restResult.setMessage("Tracking Stopped");
                }
                masterdataService.saveFeDistance(feDistance);
                restResult.setSuccess(true);
            }


        } catch (
                Exception e) {
            restResult.setResponse(e);
            restResult.setMessage("Failed! Try Again");
            log.debug("error in Sending start tracking notification" + e.getMessage());
            restResult.setSuccess(false);
        }
        return restResult;
    }

    /**
     * Author: ADARSh
     * <p>Here we are fetching distance for specific FE from specific date & date range </p>
     *
     * @param feName
     * @param date
     * @param dateRange
     * @return
     */
    //TODO check daterange logic is working or not
    @RequestMapping(value = "get/distance", produces = "application/json", method = RequestMethod.POST)
    public TotalDistance getTotalDistance(@RequestParam(value = "feName", defaultValue = "") String feName, @RequestParam(value = "date", defaultValue = "") String date, @RequestParam(value = "dateRange", defaultValue = "{}", required = false) String dateRange) {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) ProcessEngines.getDefaultProcessEngine()).getMasterdataService();
        TotalDistance totalDistance = new TotalDistance();
        //Opening Sql Session
        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
        SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();

        String startDate = "";
        String endDate = "";


        try {
            FeDisatnceCustomQueriesMapper mapper = sqlSession.getMapper(FeDisatnceCustomQueriesMapper.class);
            Map<String, String> dateMap = new ObjectMapper().readValue(dateRange, Map.class);

            if (dateMap.containsKey("startDate") && dateMap.containsKey("endDate")) {
                startDate = dateMap.get("startDate");
                endDate = dateMap.get("endDate");
            }
            List<com.rlogistics.customqueries.TotalDistance> totalDistances = mapper.getTotalDistance(feName, date, startDate, endDate);
            int size = totalDistances.size();
            Iterator<com.rlogistics.customqueries.TotalDistance> totalDistanceIterator = totalDistances.iterator();
            //TODO Implementation of iterator for List Type
            if (size == 1) {
                totalDistance.setFeId(totalDistances.get(0).getFeId());
                totalDistance.setFeName(totalDistances.get(0).getFeName());
                totalDistance.setDistance(totalDistances.get(0).getDistance());
            }
            if (size > 1) {
                BigDecimal defult = new BigDecimal(0.0);

                //Logic to add all the distance
                while (totalDistanceIterator.hasNext()) {
                    com.rlogistics.customqueries.TotalDistance totalDistance1 = totalDistanceIterator.next();
                    log.debug("test" + totalDistance1);
                    defult = defult.add(totalDistance1.getDistance());
                    log.debug("a" + defult);
                }
                totalDistance.setFeId(totalDistances.get(0).getFeId());
                totalDistance.setFeName(totalDistances.get(0).getFeName());
                totalDistance.setDistance(defult);


            }
            //TODO If not found sending as null now


        } catch (Exception e) {
            log.debug("Error while fetching" + e);
        }
        sqlSession.close();
        return totalDistance;
    }


    /**
     * @param httpRequest
     * @param httpResponse
     * @param locReqJson
     * @return
     * @throws Exception
     */

    // Krishna -July 30 2018
    //This API is used by Android application to update the current latitude and longitude  of FE and and Time of lat-long collection
    //

    /**
     * Modified by Adarsh
     * Needs refactoring of code
     */
    @RequestMapping(value = "/location/fe", produces = "application/json")
    public RestResult utrackFE(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                               @RequestParam("data") String locReqJson) throws Exception {
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();

        //Create masterdataservices object that contains methods save, update createQuesry..

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();

        //Create object of the Entity class

        FeLocationTrans feLocationTrans = masterdataService.newFeLocationTrans();
        RestResult restResult = new RestResult();
        Map<String, String> queryObj = new HashMap<String, String>();

        log.debug(locReqJson);

        // Android sends data in JSON object format
        queryObj = new ObjectMapper().readValue(URLDecoder.decode(locReqJson), Map.class);

        String latitude = "";
        String longitude = "";
        String totalDistance = "";
        String id = "";
        String feName = "";
        String date = "";
        String ticket = "";
        String city = "";

        if (queryObj.containsKey("latitude")) {
            latitude = queryObj.get("latitude");
        }
        if (queryObj.containsKey("longitude")) {
            longitude = queryObj.get("longitude");
        }

        if (queryObj.containsKey("totalDistance")) {
            totalDistance = queryObj.get("totalDistance");
        }
        if (queryObj.containsKey("feName")) {
            feName = queryObj.get("feName");
        }

        if (queryObj.containsKey("ticket")) {
            ticket = queryObj.get("ticket");
        }
        if (queryObj.containsKey("city")) {
            city = queryObj.get("city");
        }

        try {
            Date formattedDate = new Date();
            String strDateFormat = "dd/MM/yyyy HH:mm:ss";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            date = dateFormat.format(formattedDate);

            feLocationTrans.setFeName(feName);
            feLocationTrans.setLatitude(latitude);
            feLocationTrans.setLongitude(longitude);
            feLocationTrans.setDate(date);
            feLocationTrans.setTotalDistance(totalDistance);
            feLocationTrans.setTicket(ticket);
            feLocationTrans.setCity(city);

            log.debug("id===========" + id);
            log.debug("latitude===========" + latitude);
            log.debug("longitude===========" + longitude);
            log.debug("feName========" + feName);
            log.debug("date===========" + date);
            log.debug("city====" + city);

            // Database rule:
            //	Column names and Table name see auto generated {{$EntitiClassName}}.xml file
            //	column with name REV_ and type integer has to created in every table

            //  save{{$EntitiClassName}}() is the method name to save data into database and pass the entiti object to the method.
            //Don't forget to include the auto generated {{$EntitiClassName}}.xml XML file in
            // activiti-custom-context.xml file.

            masterdataService.saveFeLocationTrans(feLocationTrans);
            restResult.setSuccess(true);
            restResult.setMessage("Sucessfully inserted");
        } catch (Exception e) {
            log.debug("Error in Saving FE Loaction Details For Tracking :" + e);
        }


        restResult.setSuccess(true);
        return restResult;
    }


    // This API is used By Process End to Track FE Location After assignment of
    // tickets.
    @RequestMapping(value = "/track/fe/create", produces = "application/json")
    public static RestResult createFeLocation(@RequestParam("processId") String processId,
                                              @RequestParam("feName") String feName, @RequestParam("dbName") String dbName,
                                              @RequestParam("appointmentDate") String appointmentDate, @RequestParam("feCity") String feCity,
                                              @RequestParam("destination") String destination) {

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        TrackFeLocation trackFe = null;
        log.debug("processId :" + processId);
        log.debug("feName :" + feName);
        log.debug("dbName :" + dbName);
        log.debug("appointmentDate :" + appointmentDate);
        log.debug("feCity :" + feCity);
        log.debug("destination :" + destination);
        String assignee = "";
        String ticketNo = "";
        String priority = "";

        if (!(processId.equals("") || feName.equals(""))) {
            try {
                try {
                    trackFe = masterdataService.newTrackFeLocation();
                } catch (Exception e) {
                    log.debug("Error in Creating TrackFELocation Object :" + e);
                }
                try {
                    ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
                    priority = String.valueOf(runtimeService.getVariable(processId, "ticketPriority"));
                } catch (Exception e) {
                    log.debug("Error while getting ticket no from ProcessId :" + e);
                }
                log.debug("ticketNo :" + ticketNo);

                if (!(priority.equals("") || priority == null))
                    trackFe.setPriority(priority);
                if (!(appointmentDate.equals("") && appointmentDate == null))
                    trackFe.setAppointmentDate(appointmentDate);
                if (!(feName.equals("") || feName == null)) {
                    trackFe.setFeName(feName);
                }
                if (!(ticketNo.equals("") || ticketNo == null))
                    trackFe.setTicketNo(ticketNo);
                if (!(feCity.equals("") || feCity == null))
                    trackFe.setFeCity(feCity);
                if (!(dbName.equals("") || dbName == null))
                    trackFe.setDbName(dbName);
                if (!(destination.equals("") || destination == null))
                    trackFe.setDestination(destination);
                try {
                    try {
                        String lastAssignne = setLatLong(destination);
                        trackFe.setLastAssign(lastAssignne);
                    } catch (Exception e) {
                        log.debug("Unable to Convert Address to LatLong : " + e);
                    }
                    masterdataService.saveTrackFeLocation(trackFe);
                    restResult.setMessage("Successfully Created");
                } catch (Exception e) {
                    log.debug("Error in Saving FE Loaction Detain For Tracking :" + e);
                }
            } catch (Exception e) {
                System.out.println("Error in Creating FE Location Details : " + e);
                log.debug("Error in Creating FE Location Details : " + e);
            }
        }
        restResult.setSuccess(true);
        return restResult;
    }

    // Retrieving all the FE Tickets Of Present Day
    @RequestMapping(value = "/list/all/fe", produces = "application/json")
    public static SingleFeTrackResult listAllFe(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                @RequestParam(value = "feCity", required = false) String feCity,
                                                @RequestParam(value = "feName", required = false) String feName,
                                                @RequestParam(value = "appointmentDate", required = false) String appointmentDate,
                                                @RequestParam("firstResult") String firstResult, @RequestParam("maxResults") String maxResults) {

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        // TrackFeLocation trackFe = null;
        String feCity1 = "";
        String feNam1 = "";
        String firstResult1 = "";
        String maxResults1 = "";
        String appointmentDate1 = "";
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        SingleFeTrackResult trackResult = new SingleFeTrackResult();
        if (!(feCity.equals(null) || feCity.equals("")))
            feCity1 = feCity;
        if (!(feName.equals(null) || feName.equals("")))
            feNam1 = feName;
        if (!(firstResult.equals(null) || firstResult.equals("")))
            firstResult1 = firstResult;
        if (!(maxResults.equals(null) || maxResults.equals("")))
            maxResults1 = maxResults;
        if (!(appointmentDate.equals(null) || appointmentDate.equals("")))
            appointmentDate1 = appointmentDate;
        List<TrackFeLocResult> list1 = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                .getTodayFeList(feCity1, appointmentDate1, firstResult1, maxResults1);

        ResultCount resultCount = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                .getCountOfTodayFeList(feCity1, appointmentDate1);
        int totalRecords = Integer.valueOf(resultCount.getTotalRecords());
        try {
            if (list1 != null) {
                Collections.sort(list1, new Comparator<TrackFeLocResult>() {
                    public int compare(TrackFeLocResult o1, TrackFeLocResult o2) {
                        try {
                            Date d1 = dateFormat.parse(o1.getAppointmentDate());
                            Date d2 = dateFormat.parse(o2.getAppointmentDate());
                            // ascending order
                            return d1.compareTo(d2);
                        } catch (Exception e) {
                            System.out.println("parsevError " + e);
                        }
                        return 0;
                    }
                });
            }
            trackResult.setData(list1);
            trackResult.setTotalRecords(totalRecords);
        } catch (Exception e) {
            log.debug("SOme Error In Comparing Appoint Dates::::::: " + e);
        }

        restResult.setSuccess(true);
        restResult.setResponse(list1);
        return trackResult;
    }

    @RequestMapping(value = "/sort/fe/list", produces = "application/json")
    public static SingleFeTrackResult getFESortedList(@RequestParam("feName") String feName,
                                                      @RequestParam("firstResult") String firstResult, @RequestParam("maxResults") String maxResults) {
        // public static void getFESortedList(String feName,String
        // processId,String destination){
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date = new Date();
        String rawResponse = "";
        int firstResult1 = 0;
        int maxResults1 = 0;
        SingleFeTrackResult trackResult = new SingleFeTrackResult();
        try {
            if (!(firstResult.equals(null) || firstResult.equals("")))
                firstResult1 = Integer.valueOf(firstResult);
            if (!(maxResults.equals(null) || maxResults.equals("")))
                maxResults1 = Integer.valueOf(maxResults);
            String queryDate = "";
            String dateString = dateFormat.format(date);
            // Date date = dateFormat.parse(dateString);
            String[] words = dateString.split(" ");
            queryDate = "%" + words[0] + " " + words[1] + " " + words[2] + "%";
            log.debug("Current date : " + queryDate);
            System.out.println("queryDate : " + queryDate);
            System.out.println("feName : " + feName);
            List<TrackFeLocResult> list1 = null;
            ResultCount resultCount = null;
            try {
                list1 = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .likeOperatorForDate(feName, queryDate, firstResult1, maxResults1);
                resultCount = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .likeOperatorTotalCount(feName, queryDate);
                System.out.println("List " + list1);
            } catch (Exception e) {
                System.out.println("Error in LikeOPe : " + e);
            }
            if (list1 != null) {
                Collections.sort(list1, new Comparator<TrackFeLocResult>() {
                    public int compare(TrackFeLocResult o1, TrackFeLocResult o2) {
                        try {
                            Date d1 = dateFormat.parse(o1.getAppointmentDate());
                            Date d2 = dateFormat.parse(o2.getAppointmentDate());
                            // ascending order
                            return d1.compareTo(d2);
                        } catch (Exception e) {
                            System.out.println("parsevError " + e);
                        }
                        return 0;
                    }
                });
                try {

                    list1 = getFeLocation(list1, feName);
                } catch (Exception e) {
                    log.debug("Error in Calling getLocation() :" + e);
                }
                // Logic For 1/2 Hour

                // Call Location API

                // }

                // Logic for 1/2 hour ends here
                // for (int i = 0; i < list1.size(); i++) {
                // String s2 = "";
                // String s4 = "";
                // if (i == 0) {
                // s2 = "12.9288634,77.5647366";
                // s4 = list1.get(i).getLastAssign();
                // } else {
                // s2 = list1.get(i - 1).getLastAssign();
                // s4 = list1.get(i).getLastAssign();
                // }
                //
                // try {
                // if (true) {
                // System.out.println("s2 : " + s2);
                // System.out.println("s4 : " + s4);
                // CloseableHttpClient httpclient = HttpClients.createDefault();
                // String url =
                // "https://maps.googleapis.com/maps/api/distancematrix/json?origins="
                // + s2
                // + "&destinations=" + s4;
                // HttpGet httpget = new HttpGet(url);
                //
                // httpget.addHeader("Content-Type", "application/json");
                //
                // CloseableHttpResponse response = httpclient.execute(httpget);
                // rawResponse = EntityUtils.toString(response.getEntity());
                // log.debug("Response from server :" + rawResponse);
                // System.out.println("response from server " +
                // rawResponse);
                // JSONObject obj = new JSONObject(rawResponse);
                // try {
                // String time =
                // obj.getJSONArray("rows").getJSONObject(0).getJSONArray("elements")
                // .getJSONObject(0).getJSONObject("duration").getString("text");
                // list1.get(i).setTimeToLocation(time);
                // System.out.println("time toloc :" + time);
                // } catch (Exception e) {
                // System.out.println("excep dur sto :" + e);
                // }
                // }
                //
                // } catch (Exception e) {
                // log.debug("Error While Calling Google API :" + e);
                // }
                // }
            }
            trackResult.setData(list1);
            int totalRecords = Integer.valueOf(resultCount.getTotalRecords());
            trackResult.setTotalRecords(totalRecords);
        } catch (Exception e) {
            log.debug("Error while creating Date ObjeCT :" + e);
        }

        return trackResult;
    }

    // @RequestMapping(value = "/fe/loc", produces = "application/json")
    // public RestResult feLoc(HttpServletRequest httpRequest,
    // HttpServletResponse httpResponse,
    // @RequestParam("add") String add, @RequestParam("addres") String addres) {
    //
    // try {
    // RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
    // (ProcessEngines.getDefaultProcessEngine()))
    // .getRuntimeService();
    // MasterdataService masterdataService = ((RLogisticsProcessEngineImpl)
    // (ProcessEngines
    // .getDefaultProcessEngine())).getMasterdataService();
    // DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z
    // yyyy");
    // Date date = new Date();
    // String lat = "";
    // String longi = "";
    // String dateString = dateFormat.format(date);
    // System.out.println("curr Date" + dateString);
    // log.debug("Current date : " + dateString);
    // String dateString1 = "Tue Feb 06%";
    // String feName = "blrfe3@yopmail.com";
    // List<TrackFeLocation> lists =
    // masterdataService.createTrackFeLocationQuery().feName(feName)
    // .appointmentDate(dateString1).list();
    // System.out.println("list : " + lists);
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // String url =
    // "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + add
    // + "&destinations="
    // + addres;
    // HttpGet httpget = new HttpGet(url);
    //
    // httpget.addHeader("Content-Type", "application/json");
    //
    // CloseableHttpResponse response = httpclient.execute(httpget);
    // String rawResponse = EntityUtils.toString(response.getEntity());
    // log.debug("Response from server :" + rawResponse);
    // System.out.println("response from server " + rawResponse);
    // } catch (Exception e) {
    // log.debug("Error While Calling Google API :" + e);
    // System.out.println("error " + e);
    // }
    // return null;
    // }

    public static String setLatLong(String destination) {
        String s1 = destination.replaceAll("#", "");
        String s2 = s1.replaceAll(",", "");
        String s3 = s2.replaceAll(" ", "+");
        log.debug("Converting Address : " + s3);
        System.out.println("Converting Address : " + s3);
        String latLong = "";
        try {
            // api key= AIzaSyCsgJvkbHjRUeiOdW1tE7zRk4X2EJQvQL4;
            CloseableHttpClient httpclient = HttpClients.createDefault();
            String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + s3
                    + "&key=AIzaSyCsgJvkbHjRUeiOdW1tE7zRk4X2EJQvQL4";
            System.out.println("url : " + url);
            HttpGet httpget = new HttpGet(url);
            httpget.addHeader("accept", "application/json");
            CloseableHttpResponse response = httpclient.execute(httpget);
            String rawResponse = EntityUtils.toString(response.getEntity());
            log.debug("Response from server :" + rawResponse);
            System.out.println("response from server " + rawResponse);
            JSONObject obj = new JSONObject(rawResponse);
            String lat = obj.getJSONArray("results").getJSONObject(0).getJSONObject("geometry")
                    .getJSONObject("location").getString("lat");
            String lng = obj.getJSONArray("results").getJSONObject(0).getJSONObject("geometry")
                    .getJSONObject("location").getString("lng");
            log.debug("lat :" + lat);
            System.out.println("Lat " + lat);
            System.out.println("Long  " + lng);
            log.debug("long : " + lng);
            latLong = lat + "," + lng;
        } catch (Exception e) {
            log.debug("Error in getting Latitude and Longitude" + e);
            System.out.println("Error in getting Latitude and Longitude" + e);
        }
        return latLong;

    }

    public double getDistanceBetweenCoordinates(String latitude1, String longitute1, String latitude2, String
            longitute2) {
        double distance = 0.0;
        try {
            // api key= AIzaSyCsgJvkbHjRUeiOdW1tE7zRk4X2EJQvQL4;
            CloseableHttpClient httpclient = HttpClients.createDefault();

            //API URL to get Distance
            // pass lat1,long1,lat2,long2
            String url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins="
                    + latitude1 + "," + longitute1
                    + "&destinations=" + latitude2 + "," + longitute2
                    + "&mode=driving&language=en-EN&sensor=false";
            System.out.println("url : " + url);
            HttpGet httpget = new HttpGet(url);
            httpget.addHeader("accept", "application/json");
            CloseableHttpResponse response = httpclient.execute(httpget);
            String rawResponse = EntityUtils.toString(response.getEntity());
            log.debug("Response from server :" + rawResponse);
            System.out.println("response from server " + rawResponse);
            JSONObject obj = new JSONObject(rawResponse);
            distance = (obj.getJSONArray("rows").getJSONObject(0).getJSONArray("elements")
                    .getJSONObject(0).getJSONObject("distance").getDouble("value")) / 1000;
            log.debug("lat :" + distance);
            System.out.println("Lat " + distance);
        } catch (Exception e) {
            log.debug("Error in getting Latitude and Longitude" + e);
            System.out.println("Error in getting Latitude and Longitude" + e);
        }
        // DIstance between the coordinates
        return distance;
    }

    public static List<TrackFeLocResult> getFeLocation(List<TrackFeLocResult> list1, String feName) {

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        List<FeOpenTicketStatus> openTicList = null;
        String ticketNo = "";
        String rawResponse = "";
        // String currentProcess
        try {
            openTicList = masterdataService.createFeOpenTicketStatusQuery().userName(feName).list();
            if (openTicList != null && openTicList.size() > 0) {
                ticketNo = openTicList.get(0).getTicketNo();
                log.debug("Open TIcket No :" + ticketNo);
            }
        } catch (Exception e) {
            log.debug("error while reading FeOPenTicketStatus :" + e);
        }
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date = new Date();
        String dateString = dateFormat.format(date);

        for (int i = 0; i < list1.size(); i++) {
            if (ticketNo.equals(list1.get(i).getTicketNo())) {
                log.debug("Value Of Iiiiii :" + i);
                try {
                    Date aptDate = dateFormat.parse(list1.get(i).getAppointmentDate());
                    Date sysDate = dateFormat.parse(dateString);
                    log.debug("Current Time :" + sysDate);
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeZone(TimeZone.getDefault());
                    cal.setTime(sysDate);
                    log.debug("Calender Sysytem Date : " + cal.getTime());
                    // cal.add(Calendar.MINUTE, 30);
                    cal.add(Calendar.HOUR_OF_DAY, 6);
                    String newSysDate1 = dateFormat.format(cal.getTime());
                    Date newSysDate = dateFormat.parse(newSysDate1);
                    log.debug("AppointMent DAte :" + aptDate);
                    log.debug("System Date After Adding  :" + newSysDate);
                    // if (newSysDate.compareTo(aptDate) >= 0) {
                    String s2 = "";
                    String s1 = list1.get(i).getCurrentLongitude();
                    String s3 = list1.get(i).getCurrentLatitude();
                    s2 = s3 + "," + s1;
                    String s4 = list1.get(i).getLastAssign();
                    // if (i == 0) {
                    // s2 = "12.9288634,77.5647366";
                    // s4 = list1.get(i).getLastAssign();
                    // } else {
                    // s2 = list1.get(i - 1).getLastAssign();
                    // s4 = list1.get(i).getLastAssign();
                    // }

                    try {
                        if (true) {
                            System.out.println("s2 : " + s2);
                            System.out.println("s4 : " + s4);
                            log.debug("Current Lat,lONG:" + s2);
                            log.debug("Desti Lat : " + s4);
                            CloseableHttpClient httpclient = HttpClients.createDefault();
                            String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + s2
                                    + "&destinations=" + s4;
                            HttpGet httpget = new HttpGet(url);
                            log.debug("Google url  " + url);
                            httpget.addHeader("Content-Type", "application/json");

                            CloseableHttpResponse response = httpclient.execute(httpget);
                            rawResponse = EntityUtils.toString(response.getEntity());
                            log.debug("Response from server :" + rawResponse);
                            System.out.println("response from server " + rawResponse);
                            JSONObject obj = new JSONObject(rawResponse);
                            try {
                                String time = obj.getJSONArray("rows").getJSONObject(0).getJSONArray("elements")
                                        .getJSONObject(0).getJSONObject("duration").getString("text");
                                String distance = obj.getJSONArray("rows").getJSONObject(0).getJSONArray("elements")
                                        .getJSONObject(0).getJSONObject("distance").getString("text");
                                list1.get(i).setTimeToLocation(time);
                                list1.get(i).setDistance(distance);
                                System.out.println("time toloc :" + time);
                            } catch (Exception e) {
                                System.out.println("excep dur sto :" + e);
                            }
                        }

                    } catch (Exception e) {
                        log.debug("Error While Calling Google API :" + e);
                    }
                    // }
                } catch (Exception e) {
                    log.debug("Error in Converting Date :" + e);
                }
                break;
            } else {
                list1.get(i).setCurrentLatitude("");
                list1.get(i).setCurrentLongitude("");
            }
        }
        return list1;
    }

    @RequestMapping(value = "/fe/tickets", produces = "application/json")
    public static FeWthTotalTicketResults feWthTotalTicketResults(HttpServletRequest
                                                                          httpRequest, HttpServletResponse httpResponse,
                                                                  @RequestParam("Location") String location,
                                                                  @RequestParam("firstResult") String firstResult,
                                                                  @RequestParam("maxResults") String maxResults) {
        FeWthTotalTicketResults feWthTotalTicketResults = new FeWthTotalTicketResults();


        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();

        int first = firstResult != null && firstResult != "" ? Integer.valueOf(firstResult) : 0;
        int last = maxResults != null && maxResults != "" ? Integer.valueOf(maxResults) : 0;
        log.debug("first: " + first + " " + "last: " + last);
        List<ProcessUser> processUser;

        //To Get All The FE
        if (last != 0) {
            processUser = masterdataService.createProcessUserQuery().locationId(location).roleCode("field_engineer").listPage(first, last);
            log.debug("Size " + processUser.size());
        } else {
            processUser = masterdataService.createProcessUserQuery().locationId(location).roleCode("field_engineer").list();
        }
        int total = toIntExact(masterdataService.createProcessUserQuery().locationId(location).roleCode("field_engineer").count());
        //Loop for adding response for each FE


        for (ProcessUser user : processUser) {
            FeWithTotalTickets feWithTotalTickets = new FeWithTotalTickets();
            //Adding name
            String lastname = user.getLastName() != null && !user.getLastName().isEmpty() ? user.getLastName() : "";
            feWithTotalTickets.setFeName(user.getFirstName() + " " + lastname);
            feWithTotalTickets.setEmail(user.getEmail());
            feWithTotalTickets.setNumber(user.getPhone());
            //Fetching Total Tickets for specific FE
            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();
            feWithTotalTickets.setTotalTicket(totalTaskCount(user.getEmail()));
            Date formattedDate = new Date();
            String strDateFormat = "dd/MM/yyyy";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            String date = dateFormat.format(formattedDate);
            //FeDistance feDistance = masterdataService.createFeDistanceQuery().feId(user.getEmail()).date(date).singleResult();
            //feWithTotalTickets.setStatus(feDistance == null ? 0 : feDistance.getStatus());

            //TODO Logic to find ticket of specific /current date


            feWthTotalTicketResults.setData(feWithTotalTickets);
        }
        feWthTotalTicketResults.setTotalRecords(total);
        return feWthTotalTicketResults;
    }

    private static int totalTaskCount(String assignee) {
        int totalTask;

        TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getTaskService();

        TaskQuery taskQuery = taskService.createTaskQuery();

        totalTask = toIntExact(taskQuery.taskAssignee(assignee).count());

        return totalTask;
    }

    /**
     * Using Haversin method to calculate distance between two lat long
     * R = Radius of earth
     */
    public static final double R = 6372.8;  //In kilometer

    private static double totalDistanceByHaversin(double lat1, double long1, double lat2, double long2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(long2 - long1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    /**
     * Author : Adarsh
     * To get the FE location{latitude/longitude} to render the UI
     * needs to fetch first 10 lat-long
     * REMOVED Can be used later
     */
//    @RequestMapping(value = "/get/fe/location", produces = "application/json", method = RequestMethod.POST)
//    public RestResponse getFeLocation(@RequestParam(value = "feName",defaultValue = "")String feName, @RequestParam(value = "date")String date, @RequestParam(value ="ticket",defaultValue ="")String ticket){
//        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
//        SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
//        RestResponse restResponse = new RestResponse();
//        try{
//            FeLocationCustomQueryMapper mapper = sqlSession.getMapper(FeLocationCustomQueryMapper.class);
//            List<FeLocationResult> feLocationResults = mapper.getFeLocation(date, feName, ticket);
//            restResponse.setResponse(feLocationResults);
//            restResponse.setSuccess(true);
//            restResponse.setMessage("Fetched");
//        }catch (Exception e){
//            log.debug("Error"+e);
//            restResponse.setMessage("failed");
//            restResponse.setSuccess(false);
//        }
//
//        sqlSession.close();
//        return restResponse;
//    }

    /**
     *
     * @param location
     * @param date  :
     * @return
     */
    @RequestMapping(value = "/get/fe/location", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public RestResponse getFesLocation(@RequestParam(value = "location", defaultValue = "") String location,@RequestParam(value ="date")String date) {
        RestResponse restResponse = new RestResponse();
        try {
            log.debug(date);
            ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
            SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
            FeLocationFetchMapper mapper = sqlSession.getMapper(FeLocationFetchMapper.class);
            List<FeLocationResult> feLocationResults = mapper.fetchFeLocation(location,date);
            restResponse.setResponse(feLocationResults);
            restResponse.setSuccess(true);
            restResponse.setMessage("Fetched");
            return restResponse;
        } catch (Exception e) {
            log.debug("Exception in Fetching location of all the FE " + e);
            restResponse.setResponse("Error");
            restResponse.setMessage("Error Occoured!!");
            restResponse.setSuccess(false);
            return restResponse;
        }
    }

    @RequestMapping(value = "/get/fe/distance/{first}/{max}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public RestResponse getFeDistanceRange(@RequestParam(value = "location") String location, @RequestParam(value = "startDate") String firstDate,@RequestParam(value = "endDate")String lastDate) {
        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();
        SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
        RestResponse restResponse = new RestResponse();
        try {
            FeLocationByLocationCustomQueryMapper mapper = sqlSession.getMapper(FeLocationByLocationCustomQueryMapper.class);
            List<FeNameWithDistance> feDistanceByLocation = mapper.getFeDistanceByLocation(firstDate,lastDate,location);
            restResponse.setResponse(feDistanceByLocation);
            restResponse.setMessage("Fetched");
            restResponse.setSuccess(true);

        } catch (Exception e) {
          restResponse.setMessage("Failed");
          restResponse.setSuccess(false);
        } finally {
            sqlSession.close();
            return restResponse;
        }


    }

}