package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.customqueries.PaymentDetailsData;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Implementation of payment report
 *
 * @author Adarsh
 */
@Service
@Qualifier("paymentReport")
@Scope(value = "prototype")
public class PaymentReportUtilImpl extends ReportExcelUtilImpl {

    @Override
    public ReportExcelUtil insertData(List<?> data, String type) throws IOException {
        try {
            if (type.equals("payment")) {
                ((List<PaymentDetailsData>) data)
                        .forEach(this::insertInCell);
            }

        } catch (Exception e) {
            wb.write(outputStream);
        }
        return this;

    }

    private void insertInCell(PaymentDetailsData data) {
        int colNo = 0;

        //New Row
        Row row = sheet.createRow(rownum++);

        //Create S. No cell
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Ticket No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getTicketNo());

        //Location
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getLocation());

        //Customer Name
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getCustomerName());


        //Retailer Name
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRetailerName());

        //Amount
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getAmount());

        //Status
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getStatus());

        //Phone No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getPhone());

        //Payment Type
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getPaymentType());

        //Created On
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getCreatedOn());

        //Updated On
        cell = row.createCell(colNo);
        cell.setCellValue(data.getUpdatedOn());

    }
}
