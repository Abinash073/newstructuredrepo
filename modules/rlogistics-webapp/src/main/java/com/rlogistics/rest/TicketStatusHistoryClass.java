package com.rlogistics.rest;

import java.net.InetAddress;
import java.util.Map;
import java.util.UUID;

import org.activiti.engine.ProcessEngines;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.TicketStatusHistory;

import pojo.ReportPojo;

public class TicketStatusHistoryClass {
	private static Logger log = LoggerFactory.getLogger(TicketStatusHistoryClass.class);

	public static void insertIntoHistoryTable(Map<String, Object> variables, String rlProcessId, String statusString) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			TicketStatusHistory statusHistory = masterdataService.newTicketStatusHistory();
			UUID id = UUID.randomUUID();
			statusHistory.setId(String.valueOf(id));
			statusHistory.setProcInstId(rlProcessId);
			statusHistory.setTicketNo(variables.get("rlTicketNo").toString());
			String sta = getStatusForWebSite(variables, statusString, rlProcessId);
			statusHistory.setStatus(sta);
			statusHistory.setCreateTime(ReportPojo.getCurrectTimeStamp());
			if (!sta.equals(""))
				masterdataService.saveTicketStatusHistory(statusHistory);

			JSONObject reportStatus = getStatusForReport(variables, rlProcessId, statusString);
			if (!reportStatus.equals("")) {
				ReportPojo.updateReport(reportStatus);
			}

		} catch (Exception e) {
		}
	}

	private static String getStatusForWebSite(Map<String, Object> variables, String statusString, String processId) {

		String rtnString = "";
		switch (variables.get("rlServiceCode").toString()) {
		case "PICKNDROPONEWAY":
			switch (statusString) {
			case "DELIVERED":
				rtnString = "Product Delivered to Drop Location";

				break;
			case "CUSTOMER_REJECTION":
				rtnString = "Customer Cancel the Service While Drop";
				break;
			case "RESCHEDULE":
				rtnString = "Rescheduled from Customer While Drop";
				break;
			// case "DROP_ASSIGNED":
			//
			// break;
			// case "CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL":
			//
			// break;

			// case "FE_SCHEDULED_FOR_PICKUP":
			//
			// break;
			// case "COORDINATOR_RE_SCHEDULED_FE_FOR_PICKUP":
			// break;
			// case
			// "FE_BACK_TO_WAREHOUSE_WITH_PRODUCT_CUSTOMER_REJECT_TO_FE_WHILE_DROPPING":
			// break;
			case "PRODUCT_PICKED_UP_FROM_CUSTOMER":
				rtnString = "Product Picked up";
				break;
			case "PICKUP_REJECTED_TO_FE_BY_CUSTOMER":
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			// case "PICKUP_RESCHEDULED_TO_FE_BY_CUSTOMER":
			//
			// break;
			case "TICKET_ASSIGNED_TO_FE_FOR_PICKUP":
				rtnString = "Appointment Fixed";

				break;
			case "TICKET_CREATED":// it should be in all service for ticket
									// create history
				rtnString = "Ticket Created";
				break;
			}

			break;

		case "BUYBACK":
			switch (statusString) {
			case "1":
				rtnString = "FE Schedule for Pick up";
				break;
			case "2":// pick up done
				rtnString = "Product Picked up";
				break;
			case "3":
				rtnString = "Customer Reschedule at Door Step";
				break;
			case "4":
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			case "TICKET_CREATED":// it should be in all service for ticket
									// create history
				rtnString = "Ticket Created";
				break;
			case "PRODUCT_DROPPED_AT_DROP_LOCATION":
				rtnString = "Product Delivered to Drop Location";
				break;
			case "LSP_INITIATED_TO_DROP_THE_PRODUCT_TO_RETAILER":
				rtnString = "In-transit for drop location";
				break;
			}
			break;
		case "ADVEXCHANGE":
			switch (statusString) {
			case "CUSTOMER_REJECT_TO_COORDINATOR_AT_INITIALCALL":
				rtnString = "Customer Reject at Initail Call";
				break;
			case "CUSTOMER_REJECT_SERVICE_TO_FE_AT_DOORSTEP":// pick up done
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			case "CUSTOMER_RESCHEDULE_TO_FE":
				rtnString = "Customer Reschedule at Door Step";
				break;
			case "CUSTOMER_RESCHEDULE_TO_COORDINATOR":
				rtnString = "Customer Rescheduled to Coordinator";
				break;
			case "PICKUP_PRODUCT_EXCHANGED_SUCCESSFULLY":
				rtnString = "Product Exchanged done";
				break;
			case "FE_OUT_FOR_PICKUP":
				rtnString = "FE Schedule for Pick up";
				break;
			case "TICKET_CREATED":// it should be in all service for ticket
									// create history
				rtnString = "Ticket Created";
				break;
			case "PRODUCT_DROPPED_AT_DROP_LOCATION":
				rtnString = "Product Delivered to Drop Location";
				break;
			case "LSP_INITIATED_TO_DROP_OLD_PRODUCT_TO_DROPPED_LOCATION":
				rtnString = "In-transit for drop location";
				break;
			}

		}
		return rtnString;
	}

	private static JSONObject getStatusForReport(Map<String, Object> variables, String processId, String statusString)
			throws Exception {
		
		org.json.JSONObject reportJsonObject = new org.json.JSONObject();

		String ticketNo = variables.get("rlTicketNo").toString();

		String rtnString = "";

		switch (variables.get("rlServiceCode").toString()) {

		case "PICKNDROPONEWAY":
			switch (statusString) {
			case "DELIVERED":
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "DROP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("dropDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("dropTime", ReportPojo.getCurrentTime());
				rtnString = "Product Delivered to Drop Location";

				break;
			case "CUSTOMER_REJECTION":
				rtnString = "Customer Cancel the Service While Drop";
				break;
			case "RESCHEDULE":
				rtnString = "Rescheduled from Customer While Drop";
				break;
			case "DROP_ASSIGNED":
				rtnString = "Cordinator Assigned to FE for Drop";
				break;
			case "CUSTOMER_REJECT_TO_CORDINATOR_AT_INITAILCALL":
				rtnString = "Customer Reject at Initail Call";
				break;
			case "FE_SCHEDULED_FOR_PICKUP":
				rtnString = "FE Schedule for Pick up";
				break;
			case "COORDINATOR_RE_SCHEDULED_FE_FOR_PICKUP":
				rtnString = "Cordinator Rescheduled for Pick up";
				break;
			case "FE_BACK_TO_WAREHOUSE_WITH_PRODUCT_CUSTOMER_REJECT_TO_FE_WHILE_DROPPING":
				rtnString = "Product Back to Ware house after customer rejection";
				break;
			case "PRODUCT_PICKED_UP_FROM_CUSTOMER":
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "PICK_UP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("pickupDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("pickupTime", ReportPojo.getCurrentTime());
				rtnString = "Product Picked up";
				break;
			case "PICKUP_REJECTED_TO_FE_BY_CUSTOMER":
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			case "PICKUP_RESCHEDULED_TO_FE_BY_CUSTOMER":
				rtnString = "Customer Reschedule at Door Step";
				break;
			case "TICKET_ASSIGNED_TO_FE_FOR_PICKUP":
				rtnString = "Appointment Fixed";
				break;
			case "LSP_INITIATED_TO_DROP_PICKUP_PRODUCT_AT_HUB":
				rtnString = "In-transit for drop location";
				break;
			case "LSP_INITIATED_TO_DROP_OLD_PRODUCT_TO_DROPPED_LOCATION":
				rtnString = "In-transit for drop location";
				break;
			case "PROBLEM_SLOVE_DURING_INITIAL_CALL_TICKET_CLOSED":
				rtnString = "Problem Solve During Initial Call.Ticket Closed";
				break;
			}

			break;

		case "BUYBACK":
			switch (statusString) {
			case "1":
				rtnString = "FE Schedule for Pick up";
				break;
			case "2":// pick up done
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "PICK_UP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("pickupDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("pickupTime", ReportPojo.getCurrentTime());
				rtnString = "Product Picked up";
				break;
			case "3":
				rtnString = "Customer Reschedule at Door Step";
				break;
			case "4":
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			case "PRODUCT_DROPPED_AT_DROP_LOCATION":
				rtnString = "Product Delivered to Drop Location";
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "DROP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("dropDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("dropTime", ReportPojo.getCurrentTime());
				break;
			case "LSP_INITIATED_TO_DROP_THE_PRODUCT_TO_RETAILER":
				rtnString = "In-transit for drop location";
				break;
			case "PROBLEM_SLOVE_DURING_INITIAL_CALL_TICKET_CLOSED":
				rtnString = "Problem Solve During Initial Call.Ticket Closed";
				break;

			}
			break;
		case "ADVEXCHANGE":
			switch (statusString) {
			case "CUSTOMER_REJECT_TO_COORDINATOR_AT_INITIALCALL":
				rtnString = "Customer Reject at Initail Call";
				break;
			case "CUSTOMER_REJECT_SERVICE_TO_FE_AT_DOORSTEP":// pick up done
				rtnString = "Product Cancel by Customer While Pickup";
				break;
			case "CUSTOMER_RESCHEDULE_TO_FE":
				rtnString = "Customer Reschedule at Door Step";
				break;
			case "CUSTOMER_RESCHEDULE_TO_COORDINATOR":
				rtnString = "Customer Rescheduled to Coordinator";
				break;
			case "PICKUP_PRODUCT_EXCHANGED_SUCCESSFULLY":
				rtnString = "Product Exchanged done";
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "PICK_UP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("pickupDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("pickupTime", ReportPojo.getCurrentTime());
				break;
			case "FE_OUT_FOR_PICKUP":
				rtnString = "FE Schedule for Pick up";
				break;
			case "PRODUCT_DROPPED_AT_DROP_LOCATION":
				rtnString = "Product Delivered to Drop Location";
				reportJsonObject.put("processId", processId);
				reportJsonObject.put("currentStatus", "DROP_DONE");
				reportJsonObject.put("rlTicketNo", ticketNo);
				reportJsonObject.put("dropDate", ReportPojo.getCurrentDate());
				reportJsonObject.put("dropTime", ReportPojo.getCurrentTime());
				break;
			case "LSP_INITIATED_TO_DROP_OLD_PRODUCT_TO_DROPPED_LOCATION":
				rtnString = "In-transit for drop location";
				break;
			case "LSP_INITIATED_TO_PICKUP_NEW_EXCHANGE_PRODUCT_FROM_SELLER":
				rtnString = "In-transit for pich up new device";
				break;
			}
			break;

		}
		
		return reportJsonObject;
	}

}
