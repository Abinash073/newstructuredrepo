package com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.BizlogServiceType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BizlogserviceTypeAnotationValidator implements ConstraintValidator<BizlogServiceType, String> {
    private MasterdataService masterdataService;

    @Autowired
    public BizlogserviceTypeAnotationValidator(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    @Override
    public void initialize(BizlogServiceType constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return  isServiceTypeValid(value);
    }
    private synchronized boolean isServiceTypeValid(String v){
      return   masterdataService.createServiceMasterQuery().name(v).count() == 1 ;
    }
}
