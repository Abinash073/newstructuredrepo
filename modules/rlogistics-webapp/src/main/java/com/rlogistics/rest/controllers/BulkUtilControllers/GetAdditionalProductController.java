package com.rlogistics.rest.controllers.BulkUtilControllers;

import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.controllers.dto.ListOfAddWithItemsNCounts;
import com.rlogistics.rest.controllers.dto.ListOfBoxWithCount;
import com.rlogistics.rest.services.AdditionalProductMapperService;
import com.rlogistics.rest.services.helper.BoxNumberWithCount;
import com.rlogistics.rest.services.helper.DropLocationWithCountOfProdDTO;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Controller to get the bulk tickets item grouped by specific grouping logic
 *
 * @author : Adarsh
 */
@Slf4j
@RestController
@NoArgsConstructor
public class GetAdditionalProductController {

    private MasterdataService masterdataService;
    private AdditionalProductMapperService productMapperService;

    @Autowired
    public GetAdditionalProductController(MasterdataService masterdataService, AdditionalProductMapperService productMapperService) {
        this.masterdataService = masterdataService;
        this.productMapperService = productMapperService;
    }

    //Get by Main Ticket Number ref

    /**
     * Grouping of bulk items by box number
     *
     * @param processId :processId Of main ticket
     * @return group by box off all items
     * Not using now
     */
    @RequestMapping(value = "/get/additional-product/by-box-number", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private ListOfBoxWithCount getAddProductDetailsByMainTicketNum(@RequestParam(value = "processId") String processId) {
        ListOfBoxWithCount list = new ListOfBoxWithCount();
        try {

            List<AdditionalProductDetails> additionalProductDetails = masterdataService
                    .createAdditionalProductDetailsQuery()
                    .processId(processId)
                    .list();

            //Will not allow duplicate
            HashSet<String> boxes = additionalProductDetails.stream()
                    .map(AdditionalProductDetails::getBoxNumber)
                    .collect(HashSet::new, HashSet::add, HashSet::addAll);
            int count = boxes.size();


            List<BoxNumberWithCount> boxNumberWithCountList = boxes.stream().map(s -> productMapperService.getByBox(s, processId)).collect(Collectors.toList());

            list.setBoxDetails(boxNumberWithCountList);
            list.setCount(count);
            list.setMessage("SuccessFully Fetched");


        } catch (Exception e) {
            log.debug("Error while fetching data" + e);
            list.setMessage("Error occurred while fetching data");
        }
        return list;
    }

    /**
     * Grouping of items by drop location
     *
     * @param processId : ProcessId Of main ticket
     * @return List of grouped data of additional product by dropLocation
     */
    @RequestMapping(value = "/get/additional-product/by-drop-location", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private ListOfAddWithItemsNCounts getAddProdDetailsByDropLocation(@RequestParam(value = "processId") String processId) {
        try {
            List<DropLocationWithCountOfProdDTO> countOfProdDTOS = productMapperService.getByDropLocation(processId);
            return ListOfAddWithItemsNCounts
                    .builder()
                    .dropLocDetails(countOfProdDTOS)
                    .count(countOfProdDTOS.size())
                    .message("Fetched")
                    .build();
        } catch (Exception e) {
            log.debug("Error" + e);
            return ListOfAddWithItemsNCounts.builder().message("Failed").build();
        }
    }

    /**
     * Brand no count to be displayed in android for FE
     *
     * @param mainTicketNo :  mainTicketNo
     * @return count of Each brand
     * For Android
     */
    @RequestMapping(value = "/get/brand-by-count", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Map<String, Long> countByBrand(@RequestParam(value = "mainTicketNo") String mainTicketNo) {
        List<AdditionalProductDetails> details = masterdataService.createAdditionalProductDetailsQuery().mainTicketRef(mainTicketNo).status("new").list();
        return details.stream().collect(Collectors.groupingBy(a -> a.getBrand().toLowerCase().trim(), Collectors.counting()));
    }
}
