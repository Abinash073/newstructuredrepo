package com.rlogistics.rest;

import com.rlogistics.excel.Reader.FilterExcelResult;
import lombok.Data;
import org.joda.time.LocalDateTime;

import java.util.Date;

@Data
public class TicketCreationResult {
    private FilterExcelResult filterExcelResult;
    private String time = new LocalDateTime().toString();
    private boolean isSucess;
    private String message;
}

