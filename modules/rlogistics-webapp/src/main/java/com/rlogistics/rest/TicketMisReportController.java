package com.rlogistics.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.FeLocationFetchMapper;
import com.rlogistics.customqueries.FeLocationResult;
import com.rlogistics.customqueries.TicketMisCustomQueryMapper;
import com.rlogistics.customqueries.TicketMisReportPojo;
import com.rlogistics.master.identity.Payment;
import com.rlogistics.master.identity.PaymentDetails;
import com.rlogistics.master.identity.TicketMisReport;

import pojo.MisExcelGenerator;

@RestController
@RequestMapping("/ticket-mis-report")
public class TicketMisReportController {
	private static Logger log = LoggerFactory.getLogger(TicketMisReportController.class);
	// http://test.bizlog.in:9080/rlogistics-execution/rlservice/ticket-mis-report/get?from=2019-06-02&till=2019-06-06&location=Bangalore&retailer=Demo
	// Retailer

	@RequestMapping(value = "/get", produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> excelCusatomersReport(
			@RequestParam(required = true, value = "from") String from,
			@RequestParam(required = true, value = "till") String till,
			@RequestParam(required = false, value = "retailer", defaultValue = "") String retailer,
			@RequestParam(required = false, value = "location", defaultValue = "") String location) throws IOException {
		ByteArrayInputStream in = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=" + from + "_" + till + "_" + retailer + "_"
				+ location.replace(",", "_") + ".xlsx");
		till = addOneDay(till);
		SqlSession sqlSession = null;
		try {
			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			String sqlFormatLocatins = "";
			if (!location.trim().equals("")) {
				sqlFormatLocatins = splitLocationsForSQLFormat(location);
			}
			List<TicketMisReportPojo> ticketMisReportList = mapper.getMisDetails(from, till, retailer,
					sqlFormatLocatins);
			in = MisExcelGenerator.generateMisExcel(ticketMisReportList);
		} catch (Exception e) {
			log.debug("MisExcelGenerator.generateMisExcel  " + e);
		} finally {
			sqlSession.close();
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	private String addOneDay(String till) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(till));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.add(Calendar.DAY_OF_MONTH, 1);
		String newDate = sdf.format(c.getTime());
		return newDate;
	}

	@RequestMapping(value = "/manifest/get-data", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public List<TicketMisReportPojo> test(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "query") String query,
			@RequestParam(required = false, value = "retailer", defaultValue = "") String retailer,
			@RequestParam(required = false, value = "location", defaultValue = "") String location) throws IOException {
		ByteArrayInputStream in = null;
		List<TicketMisReportPojo> ticketMisReportList = null;

		Map<String, Object> map = new ObjectMapper().readValue(URLDecoder.decode(query), Map.class);
		String from = map.get("from").toString();
		String till = map.get("till").toString();
		String pickupLoaction = map.get("rlReportingCity").toString();
		String dropLocation = map.get("dropCity").toString();

		till = addOneDay(till);

		try {
			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			ticketMisReportList = mapper.generateManifist(from, till, pickupLoaction, dropLocation);

			log.debug(ticketMisReportList.toString());
			sqlSession.close();

		} catch (Exception e) {

			log.debug("" + e);

		}
		return ticketMisReportList;

	}

	@RequestMapping(value = "/manifest/show", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public List<TicketMisReportPojo> showMenifist(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "docketNo") String docketNo) throws IOException {
		ByteArrayInputStream in = null;
		List<TicketMisReportPojo> ticketMisReportList = null;

		try {
			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			ticketMisReportList = mapper.showManifist(docketNo);
			sqlSession.close();

			log.debug(ticketMisReportList.toString());
		} catch (Exception e) {

			log.debug("" + e);

		}
		return ticketMisReportList;

	}

	@RequestMapping(value = "/manifest/generate", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public Map<String, Object> outgoingMenifist(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "query") String query,
			@RequestParam(required = true, value = "docketNo") String docketNo,
			@RequestParam(required = true, value = "vehicleNo") String vehicleNo,
			@RequestParam(required = true, value = "weight") String weight) throws IOException {
		Map<String, Object> retString = new HashMap<String, Object>();

		try {

			String processIds = query.replace("[", "").replace("]", "");

			List<TicketMisReportPojo> ticketMisReportList = null;

			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			ticketMisReportList = mapper.updateManifist(processIds, docketNo, vehicleNo, weight);

			retString.put("success", true);
			retString.put("msg", "Docket number updated successfully");

			log.debug(ticketMisReportList.toString());
			sqlSession.close();

		} catch (Exception e) {
			retString.put("success", false);
			retString.put("msg", "ERROR: " + e);

			log.debug("" + e);

		}

		return retString;

	}

	@RequestMapping(value = "/manifest/incoming", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public Map<String, Object> incomingMenifist(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "query") String query) throws IOException {
		Map<String, Object> retString = new HashMap<String, Object>();

		try {

			String processIds = query.replace("[", "").replace("]", "");

			List<TicketMisReportPojo> ticketMisReportList = null;

			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			ticketMisReportList = mapper.incomingMenifist(processIds);

			retString.put("success", true);
			retString.put("msg", "Docket number updated successfully");

			log.debug(ticketMisReportList.toString());
			sqlSession.close();

		} catch (Exception e) {
			retString.put("success", false);
			retString.put("msg", "ERROR: " + e);

			log.debug("" + e);

		}

		return retString;

	}

	@RequestMapping(value = "/manifest/download", produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> generateManifist(
			@RequestParam(required = true, value = "docketNo") String docketNo) throws IOException {
		ByteArrayInputStream in = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=" + docketNo + ".xlsx");
		SqlSession sqlSession = null;
		try {
			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			List<TicketMisReportPojo> ticketMisReportList = null;
			ticketMisReportList = mapper.downloadManifist(docketNo);
			in = MisExcelGenerator.generateMenifistExcelDownload(ticketMisReportList);

		} catch (Exception e) {
			log.debug("MisExcelGenerator.generateMisExcel  " + e);
		} finally {
			sqlSession.close();
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/manifest/check", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public Map<String, Object> checkMenifist(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "docketNo") String docketNo) throws IOException {
		List<TicketMisReportPojo> ticketMisReportList = null;
		Map<String, Object> retString = new HashMap<String, Object>();
		try {
			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			ticketMisReportList = mapper.showManifist(docketNo);
			if (ticketMisReportList.size() > 0) {
				retString.put("success", true);
				retString.put("msg", "Docket number is correct");
			} else {
				retString.put("success", false);
				retString.put("msg", "Docket number is incorrect");
			}
			sqlSession.close();
			log.debug(ticketMisReportList.toString());
		} catch (Exception e) {
			log.debug("" + e);
		}
		return retString;

	}

	@RequestMapping(value = "/manifest/update", produces = "application/json", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "status") String status) throws IOException {
		Map<String, Object> retString = new HashMap<String, Object>();
		try {

			ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration();
			SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
			TicketMisCustomQueryMapper mapper = sqlSession.getMapper(TicketMisCustomQueryMapper.class);
			mapper.updateManifistStatus(ticketNo, status);

			retString.put("success", true);
			retString.put("msg", "Docket number updated successfully");

			sqlSession.close();

		} catch (Exception e) {
			retString.put("success", false);
			retString.put("msg", "ERROR: " + e);

			log.debug("" + e);

		}
		return retString;
	}

	private static String splitLocationsForSQLFormat(String str) {
		StringBuffer newLoactionsStr = new StringBuffer("");
		String[] strArray = str.split(",");
		for (int i = 0; i < strArray.length; i++) {
			newLoactionsStr.append("'" + strArray[i].trim() + "',");
		}
		newLoactionsStr.deleteCharAt(newLoactionsStr.length() - 1).toString();
		// System.out.println(newLoactionsStr);
		return newLoactionsStr.toString();
	}

}
