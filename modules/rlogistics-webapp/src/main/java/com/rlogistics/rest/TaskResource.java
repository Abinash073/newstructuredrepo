package com.rlogistics.rest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.ImeiInventoryResult;
import com.rlogistics.customqueries.ImeiInventoryResultResponse;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.*;
import com.rlogistics.master.identity.DeviceInfo.DeviceInfoQuery;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;
import com.rlogistics.rest.services.FeTrackingDataProcessor;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class TaskResource extends RLogisticsResource {
    public static Pattern datePattern = Pattern.compile(
            "((([12][90][0-9][0-9])-([01][0-9])-([0123][0-9]))|(([0123][0-9])-([01][0-9])-([12][90][0-9][0-9])))");
    private static Logger log = LoggerFactory.getLogger(TaskResource.class);
    @Autowired
    ObjectMapper objectMapper;
    /**
     * This Api will completed the Ticket
     *
     * @param httpRequest
     * @param httpResponse
     * @param taskId
     * @param fillSessionId
     * @param fieldValues
     * @return
     */
    @Autowired
    TaskService taskService;
    @Autowired
    RuntimeService runtimeService;
    @Autowired
    MasterdataService masterdataService;

    private FeTrackingDataProcessor feTrackingDataProcessor;

    private static ArrayList<Map<String, String>> getRetailerTicketsForExcel(String retailerId, String startDateString,
                                                                             String endDateString) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        int firstResult = 0;
        int maxResults = 20;

        ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>();

        Retailer retailer = null;
        Calendar c = Calendar.getInstance();
        ArrayList<TaskQueryParameter> query = new ArrayList<>();
        try {
            retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
            if (retailer == null) {
                throw new Exception("Retailer does not exist");
            }
            TaskQueryParameter taskQueryParameter = new TaskQueryParameter();
            taskQueryParameter.setVariable("retailer");
            taskQueryParameter.setValue(retailer.getName());

            query.add(taskQueryParameter);

            // Set<String> variablesOfInterest =
            // parseSet(variablesOfInterestJson);

            Set<String> variablesOfInterest = new HashSet<String>(10);
            variablesOfInterest.add("productName");
            variablesOfInterest.add("telephoneNumber");
            variablesOfInterest.add("retailer");
            variablesOfInterest.add("problemDescription");
            variablesOfInterest.add("consumerName");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("natureOfComplaint");
            variablesOfInterest.add("rlTicketNo");
            variablesOfInterest.add("rlTicketStatus");
            variablesOfInterest.add("consumerComplaintNumber");

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery();

            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();

                if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                    piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    piQuery.variableValueEqualsIgnoreCase(variable, value);
                }
            }
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;

            processInstances = piQuery.list();

            long totalRecords = piQuery.count();

            System.out.println("Processing start date--> " + startDateString);
            String[] dateArr = startDateString.split("-");

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DATE, Integer.valueOf(dateArr[0]));
            cal.set(Calendar.MONTH - 1, Integer.valueOf(dateArr[1]));
            cal.set(Calendar.YEAR, Integer.valueOf(dateArr[2]));
            cal.set(Calendar.HOUR_OF_DAY, 00);
            cal.set(Calendar.MINUTE, 00);
            cal.set(Calendar.SECOND, 00);
            Date startDate = cal.getTime();
            System.out.println("Start Query Date---> " + cal.getTime());

            System.out.println("Processing start date--> " + startDateString);
            dateArr = endDateString.split("-");
            cal.set(Calendar.DATE, Integer.valueOf(dateArr[0]));
            cal.set(Calendar.MONTH - 1, Integer.valueOf(dateArr[1]));
            cal.set(Calendar.YEAR, Integer.valueOf(dateArr[2]));
            cal.set(Calendar.HOUR_OF_DAY, 00);
            cal.set(Calendar.MINUTE, 00);
            cal.set(Calendar.SECOND, 00);
            Date endDate = cal.getTime();
            System.out.println("End Query Date---> " + cal.getTime());

            System.out.println("Query start date in mills : " + startDate.getTime() / 1000);
            System.out.println("Query end date in mills : " + endDate.getTime() / 1000);

            for (ProcessInstance processInstance : processInstances) {

                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);

                    Date d = task.getCreateTime();
                    Calendar taskCal = Calendar.getInstance();
                    taskCal.setTime(d);
                    taskCal.set(Calendar.HOUR_OF_DAY, 00);
                    taskCal.set(Calendar.MINUTE, 00);
                    taskCal.set(Calendar.SECOND, 00);
                    d = taskCal.getTime();

                    System.out.println("Task date in mills : " + d.getTime() / 1000);
                    // if(task.getCreateTime().getDate()==date.getDate() &&
                    // task.getCreateTime().getMonth()==date.getMonth() &&
                    // task.getCreateTime().getYear()==date.getYear()) {
                    if ((d.getTime() / 1000) >= (startDate.getTime() / 1000)
                            && (d.getTime() / 1000) <= (endDate.getTime() / 1000)) {

                        retval.add(new ProcessResultWithVariables(processInstance, task,
                                variablesOfInterest.isEmpty()
                                        ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                        : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                        variablesOfInterest)));
                    }
                }
            }

            // form the final response

            for (ProcessResultWithVariables processResultWithVariables : retval) {
                Map<String, String> result = new HashMap<String, String>();
                result.put("processId", processResultWithVariables.getProcess().getProcessId());
                result.put("productName", String.valueOf(processResultWithVariables.getVariables().get("productName")));
                result.put("telephoneNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("telephoneNumber")));
                result.put("retailer", String.valueOf(processResultWithVariables.getVariables().get("retailer")));
                result.put("problemDescription",
                        String.valueOf(processResultWithVariables.getVariables().get("problemDescription")));
                result.put("consumerName",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerName")));
                result.put("productCategory",
                        String.valueOf(processResultWithVariables.getVariables().get("productCategory")));
                result.put("natureOfComplaint",
                        String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint")));
                result.put("rlTicketNo", String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo")));
                result.put("rlTicketStatus",
                        String.valueOf(processResultWithVariables.getVariables().get("rlTicketStatus")));
                result.put("consumerComplaintNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerComplaintNumber")));
                if (processResultWithVariables.getTask() != null
                        && processResultWithVariables.getTask().getName() != null) {
                    result.put("taskName", processResultWithVariables.getTask().getName());
                } else {
                    result.put("taskName", "");
                }
                result.put("taskCreatedDate", String.valueOf(processResultWithVariables.getTask().getCreateTime()));
                data.add(result);
            }

        } catch (Exception ex) {
            log.error("Exception during retailer tickets list", ex);
        }
        return data;
    }

    public static RestResponse createExcel(@NotNull List<ProcessResultWithVariables> retval) {
        RestResponse restResponse = new RestResponse();
        log.debug("Size of List to be added to Excel" + retval.size());
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        Cell cell;

        // STYLING THE EXCEL HEADER

        // XSSFCellStyle style = workbook.createCellStyle();
        // style.setBorderTop((short) 6); // double lines border
        // style.setBorderBottom((short) 1); // single line border
        // XSSFFont font = workbook.createFont();
        // font.setFontHeightInPoints((short) 15);
        // font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        // style.setFont(font);

        try {
            workbook = new XSSFWorkbook();
            XSSFCellStyle style = workbook.createCellStyle();
            XSSFFont hSSFFont = workbook.createFont();
            hSSFFont.setFontHeightInPoints((short) 13);
            // ((Font) style).setColor(IndexedColors.BLUE.getIndex());
            hSSFFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(hSSFFont);
            sheet = workbook.createSheet("Search_Results");
            Row firstRow = sheet.createRow(0);
            cell = firstRow.createCell(0);
            cell.setCellValue("S.No");
            cell.setCellStyle(style);
            cell = firstRow.createCell(1);
            cell.setCellValue("Retailer Name");
            cell.setCellStyle(style);
            cell = firstRow.createCell(2);
            cell.setCellValue("Service Type");
            cell.setCellStyle(style);
            cell = firstRow.createCell(3);
            cell.setCellValue("Ticket Creation Date");
            cell.setCellStyle(style);
            cell = firstRow.createCell(4);
            cell.setCellValue("Ticket Creation Time");
            cell.setCellStyle(style);
            cell = firstRow.createCell(5);
            cell.setCellValue("Consumer Name");
            cell.setCellStyle(style);
            cell = firstRow.createCell(6);
            cell.setCellValue("Phone No");
            cell.setCellStyle(style);
            cell = firstRow.createCell(7);
            cell.setCellValue("ProcessId");
            cell.setCellStyle(style);
            cell = firstRow.createCell(8);
            cell.setCellValue("Consumer Request No.");
            cell.setCellStyle(style);
            cell = firstRow.createCell(9);
            cell.setCellValue("Ticket No");
            cell.setCellStyle(style);
            cell = firstRow.createCell(10);
            cell.setCellValue("Product");
            cell.setCellStyle(style);
            cell = firstRow.createCell(11);
            cell.setCellValue("Product Category");
            cell.setCellStyle(style);
            cell = firstRow.createCell(12);
            cell.setCellValue("Code");
            cell.setCellStyle(style);
            cell = firstRow.createCell(13);
            cell.setCellValue("Brand");
            cell.setCellStyle(style);
            cell = firstRow.createCell(14);
            cell.setCellValue("Model");
            cell.setCellStyle(style);
            cell = firstRow.createCell(15);
            cell.setCellValue("Pick-Up Location");
            cell.setCellStyle(style);
            cell = firstRow.createCell(16);
            cell.setCellValue("Drop Location");
            cell.setCellStyle(style);
            cell = firstRow.createCell(17);
            cell.setCellValue("Mode Of Payment");
            cell.setCellStyle(style);
            cell = firstRow.createCell(18);
            cell.setCellValue("LSP Tracking");
            cell.setCellStyle(style);
            cell = firstRow.createCell(19);
            cell.setCellValue("Status");
            cell.setCellStyle(style);
            cell = firstRow.createCell(20);
            cell.setCellValue("Status Date");
            cell.setCellStyle(style);
            cell = firstRow.createCell(21);
            cell.setCellValue("Status Time");
            cell.setCellStyle(style);
            //added by prem
            cell = firstRow.createCell(22);
            cell.setCellValue("stickyAssignee");
            cell.setCellStyle(style);


            int rowNum = 1;
            int exe = 0;
            int exe1 = 0;
            int sno = 0;
            // dataArray =
            // getRetailerTicketsForExcel(retailerId,startDate,endDate);
            log.debug("HEADER is DONE HERE)");
            try {
                for (ProcessResultWithVariables processResultWithVariables : retval) {
                    Row row = sheet.createRow(rowNum++);
                    int colNum = 0;

                    // for(int i=0;i<14;i++) {
                    sno++;
                    cell = row.createCell(colNum++);
                    cell.setCellValue(sno);

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("retailer")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("retailer")).isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("retailer")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading retailer NAme : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("natureOfComplaint")) != null
                                && !(String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint"))
                                .isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading natureOfComplaint : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if (processResultWithVariables.getVariables().containsKey("ticketCreationDate")
                                && !(String.valueOf(processResultWithVariables.getVariables().get("ticketCreationDate"))
                                .isEmpty())) {
                            try {
                                System.out.println("Timestamp value " + String
                                        .valueOf(processResultWithVariables.getVariables().get("ticketCreationDate")));

                                // CHANGE THE DATE IN DD/MM/YYYY FORMAT
                                String dat = String
                                        .valueOf(processResultWithVariables.getVariables().get("ticketCreationDate"));
                                Long l = Long.parseLong(dat);
                                Timestamp ts = new Timestamp(l);
                                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");

                                Date date = new Date(ts.getTime());
                                String strDate = String.valueOf(date);
                                System.out.println("strDate : " + strDate);

                                // Spliting the DATE
                                String array4[] = strDate.split(" ");

                                Calendar cal = Calendar.getInstance();
                                cal.setTime(date);
                                String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/"
                                        + cal.get(Calendar.YEAR);
                                // +":" + cal.get(Calendar.HOUR_OF_DAY)

                                // STYLING THE DATE CELL
                                CellStyle cellStyle = workbook.createCellStyle();
                                CreationHelper createHelper = workbook.getCreationHelper();
                                cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
                                System.out.println("formatedDate : " + formatedDate);
                                cell.setCellValue(formatedDate);
                                cell.setCellStyle(cellStyle);
                                cell = row.createCell(colNum++);
                                cell.setCellValue(array4[3]);
                                cell.setCellStyle(cellStyle);
                            } catch (Exception e) {
                                cell.setCellValue("");
                                cell = row.createCell(colNum++);
                                cell.setCellValue("");
                                log.debug("Error in Extracting Date From TimeStamp" + e);
                                System.out.println("Error in Extracting Date From TimeStamp : " + e);
                            }
                        } else {
                            cell.setCellValue("");
                            cell = row.createCell(colNum++);
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        cell.setCellValue("");
                        cell = row.createCell(colNum++);
                        cell.setCellValue("");
                        log.debug("Error While reading ticketCreationDate : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("consumerName")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("consumerName")).isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("consumerName")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading Consumer Name : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("telephoneNumber")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("telephoneNumber")).isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("telephoneNumber")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading Phone No : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getProcess().getProcessId()) != null && !(String
                                .valueOf(processResultWithVariables.getProcess().getProcessId()).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getProcess().getProcessId()));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading ProcessId : " + e);
                    }

                    cell = row.createCell(colNum++);
                    if ((processResultWithVariables.getVariables().get("consumerComplaintNumber")) != null && !(String
                            .valueOf(processResultWithVariables.getVariables().get("consumerComplaintNumber"))
                            .isEmpty())) {
                        cell.setCellValue(String
                                .valueOf(processResultWithVariables.getVariables().get("consumerComplaintNumber")));
                    } else {
                        cell.setCellValue("");
                    }

                    cell = row.createCell(colNum++);
                    if ((processResultWithVariables.getVariables().get("rlTicketNo")) != null && !(String
                            .valueOf(processResultWithVariables.getVariables().get("rlTicketNo")).isEmpty())) {
                        cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo")));
                    } else {
                        cell.setCellValue("");
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("product")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("rlTicketNo")).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("product")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading product : " + e);
                    }

                    cell = row.createCell(colNum++);
                    if ((processResultWithVariables.getVariables().get("productCategory")) != null && !(String
                            .valueOf(processResultWithVariables.getVariables().get("productCategory")).isEmpty())) {
                        cell.setCellValue(
                                String.valueOf(processResultWithVariables.getVariables().get("productCategory")));
                    } else {
                        cell.setCellValue("");
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("productCode")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("productCode")).isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("productCode")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading productCode : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("brand")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("brand")).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("brand")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading BRAND : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("model")) != null
                                && !(String.valueOf(processResultWithVariables.getVariables().get("model")).isEmpty()))
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("model")));
                        else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading model : " + e);
                    }

                    cell = row.createCell(colNum++);
                    String pickLoc = "";
                    try {
                        if ((processResultWithVariables.getVariables().containsKey("addressLine1"))) {
                            String pickLocation1 = String
                                    .valueOf(processResultWithVariables.getVariables().get("addressLine1"));
                            pickLoc = pickLoc + pickLocation1 + ",";
                        }

                        if ((processResultWithVariables.getVariables().containsKey("addressLine2"))) {
                            String pickLocation2 = String
                                    .valueOf(processResultWithVariables.getVariables().get("addressLine2"));
                            pickLoc = pickLoc + pickLocation2 + ",";
                        }
                        if ((processResultWithVariables.getVariables().containsKey("city"))) {
                            String pickLocation3 = String
                                    .valueOf(processResultWithVariables.getVariables().get("city"));
                            pickLoc = pickLoc + pickLocation3;
                        }
                        if ((processResultWithVariables.getVariables().containsKey("pincode"))) {
                            String pickLocation4 = String
                                    .valueOf(processResultWithVariables.getVariables().get("pincode"));
                            pickLoc = pickLoc + pickLocation4;
                        }
                        if (pickLoc != null && !pickLoc.isEmpty()) {
                            cell.setCellValue(pickLoc);
                            // cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("city")));
                        } else {
                            cell.setCellValue("");
                        }
                        cell = row.createCell(colNum++);
                        String dropLoc = "";
                        if ((processResultWithVariables.getVariables().containsKey("dropLocAddress1"))) {
                            String dropLocation1 = String
                                    .valueOf(processResultWithVariables.getVariables().get("dropLocAddress1"));
                            dropLoc = dropLoc + dropLocation1 + ",";
                        }

                        if ((processResultWithVariables.getVariables().containsKey("dropLocAddress2"))) {
                            String dropLocation2 = String
                                    .valueOf(processResultWithVariables.getVariables().get("dropLocAddress2"));
                            dropLoc = dropLoc + dropLocation2 + ",";
                        }
                        if ((processResultWithVariables.getVariables().containsKey("dropLocCity"))) {
                            String dropLocation3 = String
                                    .valueOf(processResultWithVariables.getVariables().get("dropLocCity"));
                            dropLoc = dropLoc + dropLocation3 + ", ";
                        }
                        if ((processResultWithVariables.getVariables().containsKey("dropLocPincode"))) {
                            String dropLocation4 = String
                                    .valueOf(processResultWithVariables.getVariables().get("dropLocPincode"));
                            dropLoc = dropLoc + dropLocation4;
                        }
                        if ((processResultWithVariables.getVariables().containsKey("rlReturnLocationAddress"))) {
                            String dropLocation5 = String
                                    .valueOf(processResultWithVariables.getVariables().get("rlReturnLocationAddress "));
                            dropLoc = dropLoc + dropLocation5 + ", ";
                        }
                        if ((processResultWithVariables.getVariables().containsKey("returnSellerCode"))) {
                            String dropLocation4 = String
                                    .valueOf(processResultWithVariables.getVariables().get("returnSellerCode"));
                            dropLoc = dropLoc + dropLocation4 + ", ";
                        }

                        if (dropLoc != null && !dropLoc.isEmpty()) {
                            cell.setCellValue(dropLoc);
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading dropLocCity : " + e);
                    }

                    cell = row.createCell(colNum++);
                    if ((processResultWithVariables.getVariables().get("modeOfPayment")) != null && !(String
                            .valueOf(processResultWithVariables.getVariables().get("modeOfPayment")).isEmpty()))
                        cell.setCellValue(
                                String.valueOf(processResultWithVariables.getVariables().get("modeOfPayment")));
                    else {
                        cell.setCellValue("");
                    }
                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("rlAMPMDetailsMap")) != null
                                && !(String.valueOf(processResultWithVariables.getVariables().get("rlAMPMDetailsMap"))
                                .isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("rlAMPMDetailsMap")));
                        } else {
                            cell.setCellValue("");
                        }
                        cell = row.createCell(colNum++);
                        if ((processResultWithVariables.getVariables().get("rlTicketStatus")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("rlTicketStatus")).isEmpty())) {
                            cell.setCellValue(
                                    String.valueOf(processResultWithVariables.getVariables().get("rlTicketStatus")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading rlTicketStatus or rlAMPMDetailsMap : " + e);
                    }

                    cell = row.createCell(colNum++);
                    if (processResultWithVariables.getProcessEndTime() != null) {
                        try {

                            // CHANGE THE DATE IN DD/MM/YYYY FORMAT
                            String dat = String.valueOf(processResultWithVariables.getProcessEndTime());

                            // Spliting the DATE
                            String array4[] = dat.split(" ");

                            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                            Date date = (Date) formatter.parse(dat);

                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/"
                                    + cal.get(Calendar.YEAR);

                            // STYLING THE DATE CELL
                            CellStyle cellStyle = workbook.createCellStyle();
                            CreationHelper createHelper = workbook.getCreationHelper();
                            cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
                            cell.setCellValue(formatedDate);
                            cell.setCellStyle(cellStyle);
                            cell = row.createCell(colNum++);
                            cell.setCellValue(array4[3]);
                            cell.setCellStyle(cellStyle);

                        } catch (Exception e) {
                            log.debug("Error in Extracting Date From TimeStamp" + e);
                            System.out.println("Error in Extracting Date From TimeStamp : " + e);
                        }
                    } else {
                        cell.setCellValue("");

                    }
                    cell = firstRow.createCell(23);
                    cell.setCellValue("City");
                    cell.setCellStyle(style);

                    cell = row.createCell(23);
                    try {
                        if ((processResultWithVariables.getVariables().get("rlReportingCity")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("rlReportingCity")).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("rlReportingCity")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading rlReportingCity: " + e);
                    }
                    //Adding Involved assignee
                    cell = firstRow.createCell(24);
                    cell.setCellValue("Involved Users");
                    cell.setCellStyle(style);

                    cell = row.createCell(24);

                    try {
                        if ((processResultWithVariables.getVariables().get("rlInvolvedUser")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("rlInvolvedUser")).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("rlInvolvedUser")).replaceAll("[\\[\\](){}]", ""));

                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error while Reading rlinvolvedUser" + e);
                    }
                    //Adding Drop location details
                    cell = firstRow.createCell(25);
                    cell.setCellValue("Drop Location");
                    cell.setCellStyle(style);


                    cell = row.createCell(25);

                    try {
                        if ((processResultWithVariables.getVariables().get("dropLocCity")) != null && !(String
                                .valueOf(processResultWithVariables.getVariables().get("dropLocCity")).isEmpty())) {
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("dropLocCity")));


                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error while getting DropLocCity");
                    }
                    //Adding Drop location contact person

                    cell = firstRow.createCell(26);
                    cell.setCellValue("Drop Location Contact Person");
                    cell.setCellStyle(style);
                    cell = row.createCell(26);
                    try {

                        if (processResultWithVariables.getVariables().get("dropLocContactPerson") != null
                                && !String.valueOf(processResultWithVariables.getVariables().get("dropLocContactPerson")).isEmpty()) {

                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("dropLocContactPerson")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error while getting Drop contact Person");
                    }
                    //Adding Drop location Contact person's number
                    cell = firstRow.createCell(27);
                    cell.setCellValue("Drop Location Contact Person's Number");
                    cell.setCellStyle(style);

                    cell = row.createCell(27);
                    try {

                        if (processResultWithVariables.getVariables().get("dropLocContactNo") != null
                                && !String.valueOf(processResultWithVariables.getVariables().get("dropLocContactNo")).isEmpty()) {

                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("dropLocContactNo")));
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error while getting Drop contact Person number");
                    }
                    //added by prem for stickyAssignee name in excel
                    cell = row.createCell(colNum++);
                    try {
                        if ((processResultWithVariables.getVariables().get("stickyAssignee")) != null
                                && !(String.valueOf(processResultWithVariables.getVariables().get("stickyAssignee")).isEmpty()))
                            cell.setCellValue(String.valueOf(processResultWithVariables.getVariables().get("stickyAssignee")));
                        else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error While reading stickyAssignee in excel generation : " + e);
                    }

                }
            } catch (Exception e) {
                log.debug("Error while writing Data To Excel : " + e);
            }
            log.debug("All DATA IS inserted In EXCEL");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            byte[] xlsx = baos.toByteArray();
            baos.flush();

            // write the excel file to database
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();
            log.debug("before quering Attachment Table");
            RLogisticsAttachment attachment = null;
            if (masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("abc").key3("bca")
                    .singleResult() != null) {
                attachment = masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("abc").key3("bca")
                        .singleResult();
            }
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();
                System.out.println("inside attachemnt if block");
                log.debug("After quering Attachment Table If Block");

                newAttachment.setKey1("search");
                newAttachment.setKey2("abc");
                newAttachment.setKey3("bca");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Search_Results.xlsx");
                /* Copy uploaded bytes into a byte[] */
                log.debug("key value attachment is null here" + newAttachment.getKey3());

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());
                System.out.println("inside attachment else block");
                log.debug("After quering Attachment Table ELSE Block");

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("search");
                newAttachment.setKey2("abc");
                newAttachment.setKey3("bca");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                String date = com.rlogistics.util.DateUtil.getCurrentDate();
                String attachmentName = date + "-Search_Results.xlsx";
                newAttachment.setName(attachmentName);
                log.debug("key value attachment is NOT null here" + newAttachment.getKey3());
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(commandContext -> {
                    newAttachment.setContent(xlsx);
                    masterdataService.saveRLogisticsAttachment(newAttachment);
                    return true;
                });

            }

            restResponse.setMessage("successfully created Excel");
            restResponse.setSuccess(true);

        } catch (

                Exception e) {
            restResponse.setSuccess(false);
            restResponse.setMessage(e.getMessage());
        }

        return restResponse;
    }

    // Writing The API for Deleting FEOPENTICKETSTATUS From Table
    public static void deleteFeOpenTicket(String processId) {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();

        // Retriving the FE_OPEN_TICKET_STATUS details
        try {
            FeOpenTicketStatus feOpenTicketStatus = masterdataService.createFeOpenTicketStatusQuery()
                    .processId(processId).singleResult();
            String id = feOpenTicketStatus.getId();
            System.out.println("feOpenTicketStatus .getId(); :  " + id);
            if (!(id.equals(null) || id.equals(""))) {
                MasterCategoryResult masterCategoryResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .deleteFeOpenTicket(processId);
            }
        } catch (Exception e) {
            log.debug("error while calling FEopenticstatus : " + e);
            System.out.println("error while calling FEopenticstatus : " + e);
        }

    }

    public static void addTicketCloseReport(String processId) {
        log.debug(processId);
        try {
            ReportingController reportingController = new ReportingController();
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            ProcessLocation pincode = masterdataService.createProcessLocationQuery().id(masterdataService.
                    createLocationPincodesQuery().
                    pincode(runtimeService.getVariable(processId, "pincode").toString())
                    .singleResult().
                            getLocationId()).singleResult();
            log.debug(pincode.getCity());
            DateTimeFormatter formatter = DateTimeFormat.forPattern("E MMM dd HH:mm:ss z yyyy");
            DateTime date = formatter.parseDateTime(runtimeService.getVariable(processId, "ticketCreationDate").toString());
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            log.debug(date.toString());
            //       reportingController.addReportDetails(pincode.getName(), "close", runtimeService.getVariable(processId, "retailer").toString(), dateTimeFormatter.print(date));
        } catch (Exception e) {
            log.debug("Error -------" + e);
        }
    }

    @Autowired
    public void setFeTrackingDataProcessor(FeTrackingDataProcessor feTrackingDataProcessor) {
        this.feTrackingDataProcessor = feTrackingDataProcessor;
    }

    @RequestMapping(value = "/tasks/counts", produces = "application/json")
    public InboxCountsResult counts(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {

        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            InboxCountsResult retval = new InboxCountsResult();

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            retval.setNumInvolved(taskService.createTaskQuery()
                    .taskInvolvedUser(Authentication.getAuthenticatedUserId()).taskUnassigned().count());
            retval.setNumAssigned(
                    taskService.createTaskQuery().taskAssignee(Authentication.getAuthenticatedUserId()).count());

            return retval;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/tasks/involved",
            "/tasks/involved/{firstResult}/{maxResults}"}, produces = "application/json")
    public TaskResultWithPagination listInvolvedTasks(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                      @PathVariable Map<String, String> pathVariables, @RequestParam("query") String query) {

        TaskResultWithPagination taskResultWithPagination = new TaskResultWithPagination();
        ProcessResultWithVariables processResultWithVariables = null;

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();

        try {
            log.debug(query);
            if (query.equals("{}")) {
                query = "[]";
            }
            ArrayList<TaskQueryParameter> queryJson = objectMapper.readValue(URLDecoder.decode(query),
                    objectMapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            log.debug("Decode done");
            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();
            TaskQuery taskQuery = taskService.createTaskQuery();

            taskQuery.taskInvolvedUser(Authentication.getAuthenticatedUserId()).taskUnassigned().orderByTaskCreateTime()
                    .desc();
            /**
             * Adarsh - 22 nov 2018 Adding filter related logic
             */

            for (TaskQueryParameter tqp : queryJson) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();
                log.debug(variable + " " + value);
                // taskQuery.processVariableValueLikeIgnoreCase(variable,
                // value.replaceAll("\\*", "%"));
                boolean isNumber = isNumber(value);
                boolean isDate = isDate(value);
                log.debug("number - date" + isNumber + " " + isDate);

                if (isNumber || isDate) {
                    String op = getOp(value);
                    Object valueObj = isNumber ? getNumber(value) : getDate(value);
                    log.debug("value of search obj" + valueObj);
                    if (op == null) {
                        taskQuery.processVariableValueEquals(variable, valueObj);
                        log.debug("Inside equal");
                    } else if (op.equals(">")) {
                        taskQuery.processVariableValueGreaterThan(variable, valueObj);
                        log.debug("inside >");
                    } else if (op.equals(">=")) {
                        taskQuery.processVariableValueGreaterThanOrEqual(variable, valueObj);
                        log.debug("inside >=");
                    } else if (op.equals("<")) {
                        taskQuery.processVariableValueLessThan(variable, valueObj);
                        log.debug("inside <");
                    } else if (op.equals("<=")) {
                        taskQuery.processVariableValueLessThanOrEqual(variable, valueObj);
                        log.debug("inside <=");
                    }
                } else {

                    if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                        taskQuery.processVariableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                        log.debug("Inside * and %");
                    } else {
                        taskQuery.processVariableValueEqualsIgnoreCase(variable, value);
                        log.debug("inside equal ignore cases");
                    }
                }
            }

            List<Task> involvedTasks = null;

            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                involvedTasks = taskQuery.listPage(firstResult, maxResults);
            } else {
                involvedTasks = taskQuery.list();
            }
            long totalRecords = taskQuery.count();
            List<TaskResult> retval = new ArrayList<TaskResult>();
            for (Task task : involvedTasks) {

                ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getRuntimeService().createProcessInstanceQuery();
                ProcessInstance processInstance = piQuery.processInstanceId(task.getProcessInstanceId()).singleResult();
                processResultWithVariables = new ProcessResultWithVariables(processInstance, task,
                        runtimeService.getVariables(processInstance.getProcessInstanceId()));
                Map<String, Object> variables = processResultWithVariables.getVariables();

                TaskResult itr = new TaskResult(task);
                if (variables != null) {
                    itr.setCategory(String.valueOf(variables.get("productCategory")));
                    itr.setRetailer(String.valueOf(variables.get("retailer")));
                    itr.setRlTicketNo(String.valueOf(variables.get("rlTicketNo")));
                    if (variables.containsKey("ticketPriority"))
                        itr.setTicketPriority(String.valueOf(variables.get("ticketPriority")));
                    itr.setPincode(String.valueOf(variables.get("pincode")));
                }

                retval.add(itr);
            }
            taskResultWithPagination.setData(retval);
            taskResultWithPagination.setTotalRecords(totalRecords);

            return taskResultWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/tasks/assigned",
            "/tasks/assigned/{firstResult}/{maxResults}"}, produces = "application/json")
    public TaskResultWithPagination listAssignedTasks(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                      @PathVariable Map<String, String> pathVariables) {

        TaskResultWithPagination taskResultWithPagination = new TaskResultWithPagination();
        ProcessResultWithVariables processResultWithVariables = null;
        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        log.debug("Before Call");
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }
            log.debug("rlToken Authenticated Success");

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            TaskQuery taskQuery = taskService.createTaskQuery();

            taskQuery.taskAssignee(Authentication.getAuthenticatedUserId()).orderByTaskCreateTime().desc();

            log.debug("Some Authentication is also Success");

            List<Task> assignedTasks = null;

            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                assignedTasks = taskQuery.listPage(firstResult, maxResults);
            } else {
                assignedTasks = taskQuery.list();
            }
            long totalRecords = taskQuery.count();
            List<TaskResult> retval = new ArrayList<TaskResult>();
            for (Task task : assignedTasks) {

                ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getRuntimeService().createProcessInstanceQuery();
                ProcessInstance processInstance = piQuery.processInstanceId(task.getProcessInstanceId()).singleResult();
                processResultWithVariables = new ProcessResultWithVariables(processInstance, task,
                        runtimeService.getVariables(processInstance.getProcessInstanceId()));
                Map<String, Object> variables = processResultWithVariables.getVariables();

                TaskResult itr = new TaskResult(task);
                if (variables != null) {
                    itr.setCategory(String.valueOf(variables.get("productCategory")));
                    itr.setRetailer(String.valueOf(variables.get("retailer")));
                    itr.setRlTicketNo(String.valueOf(variables.get("rlTicketNo")));
                    itr.setNatureOfComplaint(String.valueOf(variables.get("natureOfComplaint")));
                    if (variables.containsKey("ticketPriority"))
                        itr.setTicketPriority(String.valueOf(variables.get("ticketPriority")));
                    log.debug("ticketPriority::::::::::::::::" + variables.get("ticketPriority"));
                    if (variables.containsKey("isBlockToFe"))
                        itr.setTicketPriority(String.valueOf(variables.get("isBlockToFe")));
                    if (variables.containsKey("addressLine1"))
                        itr.setAddress1(String.valueOf(variables.get("addressLine1")));
                    if (variables.containsKey("addressLine2"))
                        itr.setAddress2(String.valueOf(variables.get("addressLine2")));
                    if (variables.containsKey("rlAppointmentDate"))
                        itr.setAppointmentDate(variables.get("rlAppointmentDate"));
                    if (variables.containsKey("pincode"))
                        itr.setPincode(variables.get("pincode"));
                    if (variables.containsKey("orderNumber"))
                        itr.setOrderNumber((variables.get("orderNumber").toString()));
                }

                retval.add(itr);
                // retval.add(processResultWithVariables);
            }
            taskResultWithPagination.setData(retval);
            taskResultWithPagination.setTotalRecords(totalRecords);

            return taskResultWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/tasks/manager/unclaimed",
            "/tasks/manager/unclaimed/{firstResult}/{maxResults}"}, produces = "application/json")
    public TaskResultWithPagination listManagerUnclaimedTasks(HttpServletRequest httpRequest,
                                                              HttpServletResponse httpResponse, @PathVariable Map<String, String> pathVariables) {

        TaskResultWithPagination taskResultWithPagination = new TaskResultWithPagination();

        ProcessResultWithVariables processResultWithVariables = null;

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            TaskQuery taskQuery = taskService.createTaskQuery();

            taskQuery.taskUnassigned().orderByTaskId().orderByTaskName().orderByTaskCreateTime().desc();

            List<Task> unclaimedTasks = null;

            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                unclaimedTasks = taskQuery.listPage(firstResult, maxResults);
            } else {
                unclaimedTasks = taskQuery.list();
            }
            long totalRecords = taskQuery.count();
            List<TaskResult> retval = new ArrayList<TaskResult>();
            for (Task task : unclaimedTasks) {
                ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getRuntimeService().createProcessInstanceQuery();
                ProcessInstance processInstance = piQuery.processInstanceId(task.getProcessInstanceId()).singleResult();
                processResultWithVariables = new ProcessResultWithVariables(processInstance, task,
                        runtimeService.getVariables(processInstance.getProcessInstanceId()));
                Map<String, Object> variables = processResultWithVariables.getVariables();

                TaskResult itr = new TaskResult(task);
                if (variables != null) {
                    itr.setCategory(String.valueOf(variables.get("productCategory")));
                    itr.setRetailer(String.valueOf(variables.get("retailer")));
                    itr.setRlTicketNo(String.valueOf(variables.get("rlTicketNo")));
                    if (variables.containsKey("ticketPriority"))
                        itr.setTicketPriority(String.valueOf(variables.get("ticketPriority")));
                }

                retval.add(itr);
            }
            taskResultWithPagination.setData(retval);
            taskResultWithPagination.setTotalRecords(totalRecords);

            return taskResultWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/tasks/manager/assigned",
            "/tasks/manager/assigned/{firstResult}/{maxResults}"}, produces = "application/json")
    public TaskResultWithPagination listManagerAssignedTasks(HttpServletRequest httpRequest,
                                                             HttpServletResponse httpResponse, @PathVariable Map<String, String> pathVariables) {

        TaskResultWithPagination taskResultWithPagination = new TaskResultWithPagination();

        ProcessResultWithVariables processResultWithVariables = null;

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            TaskQuery taskQuery = taskService.createTaskQuery();

            taskQuery.taskAssigned().orderByTaskCreateTime().desc();

            List<Task> involvedTasks = null;

            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                involvedTasks = taskQuery.listPage(firstResult, maxResults);
                System.out.println("involved  TAsk : " + involvedTasks.get(0));
            } else {
                involvedTasks = taskQuery.list();
            }
            long totalRecords = taskQuery.count();
            List<TaskResult> retval = new ArrayList<TaskResult>();
            for (Task task : involvedTasks) {
                ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getRuntimeService().createProcessInstanceQuery();
                ProcessInstance processInstance = piQuery.processInstanceId(task.getProcessInstanceId()).singleResult();
                processResultWithVariables = new ProcessResultWithVariables(processInstance, task,
                        runtimeService.getVariables(processInstance.getProcessInstanceId()));
                Map<String, Object> variables = processResultWithVariables.getVariables();

                TaskResult itr = new TaskResult(task);
                if (variables != null) {
                    itr.setCategory(String.valueOf(variables.get("productCategory")));
                    itr.setRetailer(String.valueOf(variables.get("retailer")));
                    itr.setRlTicketNo(String.valueOf(variables.get("rlTicketNo")));
                    if (variables.containsKey("ticketPriority"))
                        itr.setTicketPriority(String.valueOf(variables.get("ticketPriority")));
                }

                retval.add(itr);
            }
            taskResultWithPagination.setData(retval);
            taskResultWithPagination.setTotalRecords(totalRecords);

            return taskResultWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listManagerAssignedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/tasks/manager/completed",
            "/tasks/manager/completed/{firstResult}/{maxResults}"}, produces = "application/json")
    public ProcessTaskWithPagination listManagerCompletedInstances(HttpServletRequest httpRequest,
                                                                   HttpServletResponse httpResponse,
                                                                   @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson,
                                                                   @PathVariable Map<String, String> pathVariables) {
        return listCompletedInstances(httpRequest, httpResponse, variablesOfInterestJson, null, pathVariables);
    }

    @RequestMapping(value = {"/tasks/my-completed-instances",
            "/tasks/my-completed-instances/{firstResult}/{maxResults}"}, produces = "application/json")
    public ProcessTaskWithPagination listMyCompletedInstances(HttpServletRequest httpRequest,
                                                              HttpServletResponse httpResponse,
                                                              @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson,
                                                              @PathVariable Map<String, String> pathVariables) {
        return listCompletedInstances(httpRequest, httpResponse, variablesOfInterestJson,
                Authentication.getAuthenticatedUserId(), pathVariables);
    }

    public ProcessTaskWithPagination listCompletedInstances(HttpServletRequest httpRequest,
                                                            HttpServletResponse httpResponse, String variablesOfInterestJson, String involvedUserId,
                                                            Map<String, String> pathVariables) {

        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            Set<String> variablesOfInterest = parseSet(variablesOfInterestJson);

            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();

            HistoricProcessInstanceQuery historicPiQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery();

            log.debug("Before calling variables ***********");
            historicPiQuery.finished().orderByProcessInstanceEndTime().desc()
                    .includeProcessVariablesOfInterest(variablesOfInterest);
            System.out.println("hi_procInst size : " + historicPiQuery.list().size());
            log.debug("hi_procInst size : " + historicPiQuery.list().size());
            log.debug("variableOfInterest : " + variablesOfInterest);
            log.debug("Path Variable : " + pathVariables.entrySet());
            for (Entry<String, String> map : pathVariables.entrySet()) {
                log.debug("Key : " + map.getKey());
                log.debug("Value : " + map.getValue());
            }
            if (involvedUserId != null) {
                historicPiQuery.involvedUser(involvedUserId);
                log.debug("inside involved User :" + involvedUserId);

            }

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            log.debug("About historicPiQuery : " + historicPiQuery);
            List<HistoricProcessInstance> processInstances = null;

            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                log.debug("Before quering table ACT_HI_PROCINST ***********");
                processInstances = historicPiQuery.listPage(firstResult, maxResults);
                log.debug("After quering table ACT_HI_PROCINST ***********");
            } else {
                processInstances = historicPiQuery.list();
            }
            log.debug("Before quering count ***********");
            long totalRecords = historicPiQuery.count();

            // long totalRecords = historicPiQuery.list().size();
            // long totalRecords = Long.valueOf(total);
            log.debug("Looping through the processInstances***********");
            for (HistoricProcessInstance processInstance : processInstances) {
                log.debug("processing process---->" + processInstance.getId());
                ProcessResultWithVariables processResultWithVariables = new ProcessResultWithVariables(processInstance,
                        variablesOfInterest.isEmpty() ? processInstance.getProcessVariables()
                                : processInstance.getProcessVariables().entrySet().stream()
                                .filter(entry -> variablesOfInterest.contains(entry.getKey()))
                                .collect(HashMap::new, (m, v) -> m.put(v.getKey(), v.getValue()),
                                        HashMap::putAll)
                        // .collect(Collectors.toMap(Entry::getKey,Entry::getValue))
                );
                processResultWithVariables.setCompleted(true);

                /*
                 * Calendar cal = Calendar.getInstance();
                 * cal.setTime(endTimeDate); System.out.println("Long value - "
                 * + cal.getTime());
                 */
                processResultWithVariables.setProcessEndTime(processInstance.getEndTime());

                retval.add(processResultWithVariables);
                log.debug("added");
            }

            log.debug("Loop Completed");
            processTaskWithPagination.setData(retval);
            log.debug("Data set to model");
            processTaskWithPagination.setTotalRecords(totalRecords);
            log.debug("totalRecords set to model");

        } catch (Exception ex) {
            log.debug("Exception during listCompletedTickets", ex);
            processTaskWithPagination.setMessage("Error in processing");
            // httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        }
        log.debug("No error before returning");
        log.debug("No of records in this API call---->" + String.valueOf(processTaskWithPagination.getTotalRecords()));
        // List<ProcessResultWithVariables> retval =
        // processTaskWithPagination.getData();
        /*
         * for(ProcessResultWithVariables processResultWithVariables:retval) {
         * Map<String, Object> variables =
         * processResultWithVariables.getVariables(); log.debug(
         * "For processId :" +
         * processResultWithVariables.getProcess().getProcessId());
         * for(Map.Entry<String, Object> entry : variables.entrySet()) {
         *
         * log.debug(entry.getKey() + "--->" + entry.getValue());
         *
         * } }
         *
         * log.debug("the request variables " + variablesOfInterestJson);
         */

        return processTaskWithPagination;
    }

    @RequestMapping(value = "/task/{id}/command/claim", produces = "application/json")
    public boolean claimInvolvedTask(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                     @PathVariable("id") String taskId) {
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return false;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) ProcessEngines.getDefaultProcessEngine())
                    .getTaskService();

            taskService.setAssignee(taskId, Authentication.getAuthenticatedUserId());

            return true;
        } catch (Exception ex) {
            log.error("Exception during claimInvolvedTask", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
    }

    @RequestMapping(value = "/task/{id}/command/assign", produces = "application/json")
    public boolean assignTask(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                              @PathVariable("id") String taskId, @RequestParam("assignee") String assignee) {
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return false;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            taskService.setAssignee(taskId, assignee);

            return true;
        } catch (Exception ex) {
            log.error("Exception during claimInvolvedTask", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
    }

    @RequestMapping(value = "/task/{id}/command/unassign", produces = "application/json")
    public boolean unassignTask(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                @PathVariable("id") String taskId) {
        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return false;
            }

            TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService();

            taskService.unclaim(taskId);

            return true;
        } catch (Exception ex) {
            log.error("Exception during claimInvolvedTask", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
    }

    @RequestMapping(value = "/task/{id}/command/firstgroup", produces = "application/json")
    public GroupDescription getFirstTaskGroup(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                              @PathVariable("id") String taskId) {
        return getTaskGroup(httpRequest, httpResponse, taskId, null, null, true);
    }

    @RequestMapping(value = "/task/{id}/{fillSessionId}/command/nextgroup", produces = "application/json")
    public GroupDescription getNextTaskGroup(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                             @PathVariable("id") String taskId, @PathVariable("fillSessionId") String fillSessionId,
                                             @RequestParam("fieldValues") String fieldValues) {
        return getTaskGroup(httpRequest, httpResponse, taskId, fillSessionId, parseMap(fieldValues), true);
    }

    @RequestMapping(value = "/task/{id}/{fillSessionId}/command/currentgroup", produces = "application/json")
    public GroupDescription getCurrentTaskGroup(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                @PathVariable("id") String taskId, @PathVariable("fillSessionId") String fillSessionId) {
        return getTaskGroup(httpRequest, httpResponse, taskId, fillSessionId, null, false);
    }

    private GroupDescription getTaskGroup(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                          String taskId, String fillSessionId, Map<String, String> fieldValues, boolean next) {

        try {

            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            FormFillerUtil formFillerUtil = null;

            if (fillSessionId != null) {
                formFillerUtil = new FormFillerUtil(fillSessionId);
            } else {
                TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService();

                RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getRuntimeService();

                Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
                ExecutionEntity execution = (ExecutionEntity) runtimeService.createExecutionQuery()
                        .executionId(task.getExecutionId()).singleResult();

                formFillerUtil = new FormFillerUtil(taskId, execution, true);
            }

            GroupDescription groupDescription = next ? formFillerUtil.goToNextGroup(fieldValues)
                    : formFillerUtil.getCurrentGroup();

            return groupDescription;
        } catch (Exception ex) {
            log.error("Exception during getTaskGroup(next=" + next + ")", ex);

            GroupDescription groupDescription = null;

            if (next) {
                groupDescription = getTaskGroup(httpRequest, httpResponse, taskId, fillSessionId, null, false);

                if (ex instanceof ScriptingErrorsException) {
                    groupDescription.getMessages()
                            .addAll(((ScriptingErrorsException) ex).getErrorReport().getMessages());
                } else {
                    groupDescription.getMessages().add(ex.getMessage());
                }
            } else {
                httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

            return groupDescription;
        }
    }

    // Generating Excel-Sheet For Search Result for Non-Completed Tickets

    @RequestMapping(value = {"/task/{id}/{fillSessionId}/command/complete"}, produces = "application/json")
    public TaskCompletionResult completeTask(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                             @PathVariable("id") String taskId, @PathVariable("fillSessionId") String fillSessionId,
                                             @RequestParam(value = "fieldValues", required = false) String fieldValues) {
        try {


            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }
            Map<String, String> mappedValue = parseMap(fieldValues);

            List<String> notifications = null;

            if (fieldValues != null) {


                HashMap<String, String> latLongMap = mappedValue.keySet()
                        .stream()
                        .filter(s -> s.equals("latitude") || s.equals("longitude") || s.equals("processId") || s.equals("feName") || s.equals("city"))
                        .collect(HashMap::new, (hashMap, s) -> hashMap.put(s, mappedValue.get(s)), HashMap::putAll);

                for (Entry<String, String> a : latLongMap.entrySet()) {
                    log.debug(a.getKey() + "-->" + a.getValue());
                }

                if (!latLongMap.isEmpty()) {
                    ExecutorService executorService = Executors.newSingleThreadExecutor();

                    executorService.submit(() ->
                            feTrackingDataProcessor.storeData(latLongMap)
                    );

                }


                GroupDescription groupDescription = getTaskGroup(httpRequest, httpResponse, taskId, fillSessionId,
                        mappedValue, true);

                //filtering lat-long


                if (!groupDescription.getMessages().isEmpty()) {
                    log.error(
                            "Some validation errors. Cannot complete. Error:" + groupDescription.getMessages().get(0));
                    return new TaskCompletionResult(groupDescription);
                }

                if (!groupDescription.getNotifications().isEmpty()) {
                    notifications = groupDescription.getNotifications();
                }
            }

            FormFillerUtil formFillerUtil = new FormFillerUtil(fillSessionId);

            FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getFormService();
            log.debug("<------------------------------------------------------------------>");
            // Fetching varibales from taskId
            Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
            log.debug(task.getAssignee() + " ------------------------------" + task.getExecutionId());
            ExecutionEntity execution = (ExecutionEntity) runtimeService.createExecutionQuery()
                    .executionId(task.getExecutionId()).singleResult();

            // FormFillerUtil formFillerUtil1 = new FormFillerUtil(taskId,
            // execution, true);

            // Iterator<Map.Entry<String, Object>> entryIterator =
            // a.entrySet().iterator();
            // while (entryIterator.hasNext()) {
            // Entry<String, Object> b = entryIterator.next();
            // //log.debug(b.getKey() + "-------->" + b.getKey());
            // }
            // Getting DateTime and changing the format

            formFillerUtil.submitForm(new FormFillerUtil.FormSubmissionAction<Boolean>() {
                @Override
                public Boolean submitForm(Map<String, String> fieldValues) {
                    formService.submitTaskFormData(taskId, fieldValues);
                    return true;
                }
            });

            return new TaskCompletionResult(notifications);
        } catch (Exception ex) {
            log.error("Exception during getNextTaskGroup", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    // Method To Write on Excel-sheet.

    @RequestMapping(value = {"/tasks/find-by-variable-values",
            "/tasks/find-by-variable-values/{firstResult}/{maxResults}"}, produces = "application/json")
    public ProcessTaskWithPagination findByVariableValues(HttpServletRequest httpRequest,
                                                          HttpServletResponse httpResponse, @RequestParam("query") String queryJson,
                                                          @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson,
                                                          @RequestParam(value = "roleCode", required = false) String roleCode,
                                                          @PathVariable Map<String, String> pathVariables) {

        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        ExecutorService executorService = Executors.newWorkStealingPool();
        try {
            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }

            ObjectMapper mapper = new ObjectMapper();
            ArrayList<TaskQueryParameter> query = mapper.readValue(URLDecoder.decode(queryJson),
                    mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            Set<String> variablesOfInterest = parseSet(variablesOfInterestJson);

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery().orderByProcessInstanceId().desc();
            System.out.println("taskQuery : " + query.size());
            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();

                // if(variable.equalsIgnoreCase("retailer")){
                // TaskQueryParameter tq =new TaskQueryParameter();
                // tq.setVariable("ticketCreationDate");
                // tq.setValue(">=12-12-2017");
                // query.add(tq);
                // }
                System.out.println("task Query aftre : " + query.size());
                boolean isNumber = isNumber(value);
                boolean isDate = isDate(value);
                log.debug("number - date" + isNumber + " " + isDate);

                if (isNumber || isDate) {
                    String op = getOp(value);
                    Object valueObj = isNumber ? getNumber(value) : getDate(value);
                    log.debug("value of search obj" + valueObj);
                    if (op == null) {
                        piQuery.variableValueEquals(variable, valueObj);
                        log.debug("Inside equal");
                    } else if (op.equals(">")) {
                        piQuery.variableValueGreaterThan(variable, valueObj);
                        log.debug("inside >");
                    } else if (op.equals(">=")) {
                        piQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                        log.debug("inside >=");
                    } else if (op.equals("<")) {
                        piQuery.variableValueLessThan(variable, valueObj);
                        log.debug("inside <");
                    } else if (op.equals("<=")) {
                        piQuery.variableValueLessThanOrEqual(variable, valueObj);
                        log.debug("inside <=");
                    }
                } else {

                    if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                        piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                        log.debug("Inside * and %");
                    } else {
                        piQuery.variableValueEqualsIgnoreCase(variable, value);
                        log.debug("inside equal ignore cases");
                    }
                }
            }
            // System.out.println("piquery"+piQuery.variableValueEquals("ticketCreationDate"));
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            // RepositoryServiceImpl repositoryService = (RepositoryServiceImpl)
            // ((RLogisticsProcessEngineImpl) (ProcessEngines
            // .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;
            Future<List<ProcessInstance>> listFuture = new CompletableFuture<>();
            if (pathVariables.containsKey("firstResult")) {
                int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                int maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                listFuture = executorService.submit(() -> piQuery.listPage(firstResult, maxResults));
                //processInstances = piQuery.listPage(firstResult, maxResults);
            } else {
                listFuture = executorService.submit(piQuery::list);
                //  processInstances = piQuery.list();
            }
            Future<Long> totalRecordsFuture = executorService.submit(piQuery::count);
            processInstances = listFuture.get(2, TimeUnit.MINUTES);
            long totalRecords = totalRecordsFuture.get(2, TimeUnit.MINUTES);


            // long totalRecords = piQuery.count();
            // System.out.println("qwertyui"+"size: "+processInstances.size()+"
            // "+processInstances.get(0));
            for (ProcessInstance processInstance : processInstances) {

                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    retval.add(new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));
                    // System.out.println("runtimeServics variable :"+
                    // runtimeService.getVariables(processInstance.getProcessInstanceId()));

                } else {

                    ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();

                    }
                    retval.add(new ProcessResultWithVariables(processInstance, jobId,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));
                }
            }

            if (!(processInstances.size() > 0) && roleCode != null && roleCode.equalsIgnoreCase("retailer")) {
                HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService();

                HistoricProcessInstanceQuery hiQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery()
                        .includeProcessVariablesOfInterest(variablesOfInterest).orderByProcessInstanceStartTime().asc();

                for (TaskQueryParameter tqp : query) {
                    String variable = tqp.getVariable();
                    String value = tqp.getValue();

                    boolean isNumber = isNumber(value);
                    boolean isDate = isDate(value);

                    if (isNumber || isDate) {
                        String op = getOp(value);
                        Object valueObj = isNumber ? getNumber(value) : getDate(value);
                        if (op == null) {
                            hiQuery.variableValueEquals(variable, valueObj);
                        } else if (op.equals(">")) {
                            hiQuery.variableValueGreaterThan(variable, valueObj);
                        } else if (op.equals(">=")) {
                            hiQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                        } else if (op.equals("<")) {
                            hiQuery.variableValueLessThan(variable, valueObj);
                        } else if (op.equals("<=")) {
                            hiQuery.variableValueLessThanOrEqual(variable, valueObj);
                        }
                    } else {

                        if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                            hiQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                        } else {
                            hiQuery.variableValueEqualsIgnoreCase(variable, value);
                        }
                    }
                }

                List<HistoricProcessInstance> historicProcessInstances = null;
                Future<List<HistoricProcessInstance>> futureHisProInstances = new CompletableFuture<>();
                if (pathVariables.containsKey("firstResult")) {
                    int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                    int maxResults = Integer.parseInt(pathVariables.get("maxResults"));
                    futureHisProInstances = executorService.submit(() -> hiQuery.listPage(firstResult, maxResults));
                    //historicProcessInstances = hiQuery.listPage(firstResult, maxResults);
                } else {
                    futureHisProInstances = executorService.submit(hiQuery::list);
                    //historicProcessInstances = hiQuery.list();
                }
                Future<Long> totalRecFuture = executorService.submit(hiQuery::count);
                historicProcessInstances = futureHisProInstances.get(2, TimeUnit.MINUTES);
                totalRecords = totalRecFuture.get(2, TimeUnit.MINUTES);
                //totalRecords = hiQuery.count();

                for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
                    log.debug("TicketNo :"
                            + String.valueOf(historicProcessInstance.getProcessVariables().get("rlTicketNo")));
                    retval.add(new ProcessResultWithVariables(historicProcessInstance, variablesOfInterest.isEmpty()
                            ? historicProcessInstance.getProcessVariables()
                            : historicProcessInstance.getProcessVariables().entrySet().stream()
                            .filter(entry -> variablesOfInterest.contains(entry.getKey())).collect(HashMap::new,
                                    (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll)));

                }
            }

            processTaskWithPagination.setData(retval);
            processTaskWithPagination.setTotalRecords(totalRecords);

            return processTaskWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = {"/retailer/ticket-list",
            "/retailer/ticket-list/{firstResult}/{maxResults}"}, produces = "application/json")
    public RestExternalListResult getRetailerTicketList(HttpServletRequest httpRequest,
                                                        HttpServletResponse httpResponse, @RequestParam(value = "query", required = false) String queryJson,
                                                        @PathVariable Map<String, String> pathVariables, @RequestParam(value = "retailerId") String retailerId,
                                                        @RequestParam(value = "apiToken") String apiToken,
                                                        @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        int firstResult = 0;
        int maxResults = 20;
        RestExternalListResult restResult = new RestExternalListResult();

        Retailer retailer = null;
        Calendar c = Calendar.getInstance();
        ArrayList<TaskQueryParameter> query = new ArrayList<>();
        try {
            if (retailerId != null) {
                retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
                if (retailer == null) {
                    restResult.setSuccess(false);
                    throw new Exception("Invalid login");
                }
                if (!retailer.getApiToken().equals(apiToken)) {
                    restResult.setSuccess(false);
                    throw new Exception("Not Authorized for this request");
                }

                Timestamp currentDate = new Timestamp(c.getTime().getTime());
                if (currentDate.after(retailer.getTokenExpiryDate())) {
                    restResult.setSuccess(false);
                    throw new Exception("Authentication expired. Please login again");
                }
            }

            TaskQueryParameter taskQueryParameter = new TaskQueryParameter();
            taskQueryParameter.setVariable("retailer");
            taskQueryParameter.setValue(retailer.getName());
            // query.add(taskQueryParameter);

            ObjectMapper mapper = new ObjectMapper();
            if (queryJson != null) {
                query = mapper.readValue(URLDecoder.decode(queryJson),
                        mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            }
            query.add(taskQueryParameter);

            // Set<String> variablesOfInterest =
            // parseSet(variablesOfInterestJson);

            Set<String> variablesOfInterest = new HashSet<String>(10);
            variablesOfInterest.add("productName");
            variablesOfInterest.add("telephoneNumber");
            variablesOfInterest.add("consumerName");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("natureOfComplaint");
            variablesOfInterest.add("rlTicketNo");
            variablesOfInterest.add("rlProcessStatus");

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery();

            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();

                boolean isNumber = isNumber(value);
                boolean isDate = isDate(value);

                if (isNumber || isDate) {
                    String op = getOp(value);
                    Object valueObj = isNumber ? getNumber(value) : getDate(value);
                    if (op == null) {
                        piQuery.variableValueEquals(variable, valueObj);
                    } else if (op.equals(">")) {
                        piQuery.variableValueGreaterThan(variable, valueObj);
                    } else if (op.equals(">=")) {
                        piQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                    } else if (op.equals("<")) {
                        piQuery.variableValueLessThan(variable, valueObj);
                    } else if (op.equals("<=")) {
                        piQuery.variableValueLessThanOrEqual(variable, valueObj);
                    }
                } else {

                    if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                        piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                    } else {
                        piQuery.variableValueEqualsIgnoreCase(variable, value);
                    }
                }
            }
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;
            if (pathVariables.containsKey("firstResult")) {
                firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                processInstances = piQuery.listPage(firstResult, maxResults);
            } else {
                processInstances = piQuery.list();
            }
            long totalRecords = piQuery.count();

            for (ProcessInstance processInstance : processInstances) {

                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    retval.add(new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));

                } else {

                    ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();

                    }
                    retval.add(new ProcessResultWithVariables(processInstance, jobId,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));
                }
            }

            // form the final response
            ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>();
            for (ProcessResultWithVariables processResultWithVariables : retval) {
                Map<String, String> result = new HashMap<String, String>();
                result.put("processId", processResultWithVariables.getProcess().getProcessId());
                result.put("productName", String.valueOf(processResultWithVariables.getVariables().get("productName")));
                result.put("telephoneNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("telephoneNumber")));
                result.put("consumerName",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerName")));
                result.put("productCategory",
                        String.valueOf(processResultWithVariables.getVariables().get("productCategory")));
                result.put("serviceType",
                        String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint")));
                result.put("rlTicketNo", String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo")));
                result.put("status", String.valueOf(processResultWithVariables.getVariables().get("rlProcessStatus")));
                if (processResultWithVariables.getTask() != null
                        && processResultWithVariables.getTask().getName() != null) {
                    result.put("taskName", processResultWithVariables.getTask().getName());
                } else {
                    result.put("taskName", "");
                }
                data.add(result);
            }
            restResult.setResponse(data);
            restResult.setSuccess(true);
            restResult.setTotalRecords(totalRecords);

            // generate new apiToken
            c.add(Calendar.HOUR, 1);
            if (retailer.getApiUsername() != null) {
                String rawToken = retailer.getApiUsername() + "___"
                        + RandomStringUtils.randomAlphanumeric(8).toLowerCase()
                        + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
                apiToken = AuthUtil.cryptWithMD5(rawToken);
                retailer.setApiToken(apiToken);
                retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
            } else {
                String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
                        + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
                apiToken = AuthUtil.cryptWithMD5(rawToken);
                retailer.setApiToken(apiToken);
                retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
            }
            try {
                masterdataService.saveRetailer(retailer);
                restResult.setApiToken(apiToken);
            } catch (Exception e) {

            }

        } catch (Exception ex) {
            log.error("Exception during retailer tickets list", ex);
            restResult.setSuccess(false);
            restResult.setMessage(ex.getMessage());
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        return restResult;
    }

    // To Find The Location Of Attachment File

    @RequestMapping(value = "/tasks/write-to-excel", produces = "application/json")
    public RestResponse writeToExcel(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                     @RequestParam("retailerId") String retailerId, @RequestParam("startDate") String startDate,
                                     @RequestParam("endDate") String endDate) {
        ArrayList<Map<String, String>> dataArray = new ArrayList<Map<String, String>>();
        RestResponse restResponse = new RestResponse();

        XSSFWorkbook workbook = null;
        XSSFSheet sheet = null;
        Cell cell = null;
        ArrayList<String> orderedFields = new ArrayList<String>();
        try {
            workbook = new XSSFWorkbook();
            sheet = (XSSFSheet) workbook.createSheet("Retailer Tickets");
            Row firstRow = sheet.createRow(0);
            cell = firstRow.createCell(0);
            cell.setCellValue("Consumer");
            orderedFields.add("consumerName");
            cell = firstRow.createCell(1);
            cell.setCellValue("Product");
            orderedFields.add("productName");
            cell = firstRow.createCell(2);
            cell.setCellValue("Ticket No.");
            orderedFields.add("rlTicketNo");
            cell = firstRow.createCell(3);
            cell.setCellValue("Phone");
            orderedFields.add("telephoneNumber");
            cell = firstRow.createCell(4);
            cell.setCellValue("Category");
            orderedFields.add("productCategory");
            cell = firstRow.createCell(5);
            cell.setCellValue("Task");
            orderedFields.add("taskName");
            cell = firstRow.createCell(6);
            cell.setCellValue("Nature of Complaint");
            orderedFields.add("natureOfComplaint");
            cell = firstRow.createCell(7);
            cell.setCellValue("Problem Description");
            orderedFields.add("problemDescription");
            cell = firstRow.createCell(8);
            cell.setCellValue("Current Status");
            orderedFields.add("rlTicketStatus");
            cell = firstRow.createCell(9);
            cell.setCellValue("Complaint No");
            orderedFields.add("consumerComplaintNumber");

            int rowNum = 1;
            System.out.println("Creating excel");
            dataArray = getRetailerTicketsForExcel(retailerId, startDate, endDate);
            for (Map<String, String> data : dataArray) {
                Row row = sheet.createRow(rowNum++);
                int colNum = 0;
                for (String field : orderedFields) {
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) data.get(field));
                }
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            byte[] xlsx = baos.toByteArray();
            baos.flush();

            // write the excel file to database
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();
            RLogisticsAttachment attachment = masterdataService.createRLogisticsAttachmentQuery().key1("RetailerList")
                    .key2(retailerId).singleResult();
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("RetailerList");
                newAttachment.setKey2(retailerId);
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Tickets.xlsx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("RetailerList");
                newAttachment.setKey2(retailerId);
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Tickets.xlsx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }

            restResponse.setResponse(dataArray);

        } catch (

                Exception e) {
            restResponse.setSuccess(false);
            restResponse.setMessage(e.getMessage());
        }

        return restResponse;
    }

    @RequestMapping(value = {"/tasks/generate-excel/find-by-variable-values"}, produces = "application/json")
    public ProcessTaskWithPagination generateExcelSearchResult(HttpServletRequest httpRequest,
                                                               HttpServletResponse httpResponse, @RequestParam("query") String queryJson,
                                                               @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson,
                                                               @RequestParam(value = "roleCode", required = false) String roleCode,
                                                               @PathVariable Map<String, String> pathVariables) {

        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        try {
            if (!beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }
            log.debug("inside /tasks/generate-excel/find-by-variable-values");
            ObjectMapper mapper = new ObjectMapper();
            ArrayList<TaskQueryParameter> query = mapper.readValue(URLDecoder.decode(queryJson),
                    mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            Set<String> variablesOfInterest = parseSet(variablesOfInterestJson);
            //added by prem
            variablesOfInterest.add("stickyAssignee");

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery().orderByProcessInstanceId().asc();

            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();

                boolean isNumber = isNumber(value);
                boolean isDate = isDate(value);

                if (isNumber || isDate) {
                    String op = getOp(value);
                    Object valueObj = isNumber ? getNumber(value) : getDate(value);
                    if (op == null) {
                        piQuery.variableValueEquals(variable, valueObj);
                    } else if (op.equals(">")) {
                        piQuery.variableValueGreaterThan(variable, valueObj);
                    } else if (op.equals(">=")) {
                        piQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                    } else if (op.equals("<")) {
                        piQuery.variableValueLessThan(variable, valueObj);
                    } else if (op.equals("<=")) {
                        piQuery.variableValueLessThanOrEqual(variable, valueObj);
                    }
                } else {

                    if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                        piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                    } else {
                        piQuery.variableValueEqualsIgnoreCase(variable, value);
                    }
                }
            }
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;
            processInstances = piQuery.list();
            long totalRecords = piQuery.count();

            for (ProcessInstance processInstance : processInstances) {

                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    Date ticketCretedDate = tasks.get(tasks.size() - 1).getCreateTime();
                    // String ticketCreationDate =
                    // String.valueOf(runtimeService.getVariable(processInstance.getId(),
                    // "ticketCreationDate"));
                    // ProcessResponseWithVariables processResponseWithVariables
                    // = null;
                    // processResponseWithVariables.setCreatedDate(ticketCretedDate);
                    // retval.add(processResponseWithVariables);
                    // new ProcessResultWithVariables(ticketCretedDate);

                    retval.add(new ProcessResultWithVariables(processInstance, task, variablesOfInterest.isEmpty()
                            ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                            : runtimeService.getVariables(processInstance.getProcessInstanceId(), variablesOfInterest),
                            ticketCretedDate));

                } else {

                    ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();

                    }
                    retval.add(new ProcessResultWithVariables(processInstance, jobId,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));
                }
            }

            if (!(processInstances.size() > 0) && roleCode != null && roleCode.equalsIgnoreCase("retailer")) {
                HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService();

                HistoricProcessInstanceQuery hiQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery()
                        .includeProcessVariablesOfInterest(variablesOfInterest);

                for (TaskQueryParameter tqp : query) {
                    String variable = tqp.getVariable();
                    String value = tqp.getValue();

                    boolean isNumber = isNumber(value);
                    boolean isDate = isDate(value);

                    if (isNumber || isDate) {
                        String op = getOp(value);
                        Object valueObj = isNumber ? getNumber(value) : getDate(value);
                        if (op == null) {
                            hiQuery.variableValueEquals(variable, valueObj);
                        } else if (op.equals(">")) {
                            hiQuery.variableValueGreaterThan(variable, valueObj);
                        } else if (op.equals(">=")) {
                            hiQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                        } else if (op.equals("<")) {
                            hiQuery.variableValueLessThan(variable, valueObj);
                        } else if (op.equals("<=")) {
                            hiQuery.variableValueLessThanOrEqual(variable, valueObj);
                        }
                    } else {

                        if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                            hiQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                        } else {
                            hiQuery.variableValueEqualsIgnoreCase(variable, value);
                        }
                    }
                }

                List<HistoricProcessInstance> historicProcessInstances = null;
                historicProcessInstances = hiQuery.list();
                totalRecords = hiQuery.count();

                for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
                    log.debug("TicketNo :"
                            + String.valueOf(historicProcessInstance.getProcessVariables().get("rlTicketNo")));
                    retval.add(new ProcessResultWithVariables(historicProcessInstance, variablesOfInterest.isEmpty()
                            ? historicProcessInstance.getProcessVariables()
                            : historicProcessInstance.getProcessVariables().entrySet().stream()
                            .filter(entry -> variablesOfInterest.contains(entry.getKey())).collect(HashMap::new,
                                    (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll)));

                }
            }
            log.debug("before calling createExcel()");
            processTaskWithPagination.setMessage(createExcel(retval).getMessage());
            log.debug("AFTER calling createExcel()");
            processTaskWithPagination.setTotalRecords(totalRecords);
            return processTaskWithPagination;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/tasks/find-by-process-id/{processId}", produces = "application/json")
    public ProcessResultWithVariables findByProcessId(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                      @PathVariable("processId") String processId,
                                                      @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson) {

        try {

            ProcessResultWithVariables processResultWithVariables = null;
            ProcessInstance processInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();

            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();

            Set<String> variablesOfInterest = parseSet(variablesOfInterestJson);

            if (processInstance != null) {

                Map<String, Object> allVariables = runtimeService.getVariables(processId);
                if (variablesOfInterest.size() > 0) {
                    for (String variable : allVariables.keySet()) {
                        if (variable.indexOf("_PhotographAfter") != -1 || variable.indexOf("_PhotographBefore") != -1
                                || variable.indexOf("Signature") != -1 || variable.indexOf("_PhotoId") != -1) {
                            variablesOfInterest.add(variable);
                        }
                    }
                }

                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processId).includeProcessVariables()
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    processResultWithVariables = new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest));

                    List<Task> completedTasks = new ArrayList<Task>();
                    List<TaskResult> completedTaskList = new ArrayList<TaskResult>();
                    completedTasks.addAll(tasks);
                    completedTasks.remove(0);
                    for (Task completedTask : completedTasks) {
                        TaskResult taskResult = new TaskResult(completedTask);
                        completedTaskList.add(taskResult);
                    }


                    processResultWithVariables.setCompletedTaskList(completedTaskList);

                } else {

                    ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                            .getDefaultProcessEngine())).getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();
                    }

                    processResultWithVariables = new ProcessResultWithVariables(processInstance, jobId,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest));
                }

            } else {

                HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService();

                HistoricProcessInstance historicProcessInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines
                        .getDefaultProcessEngine())).getHistoryService().createHistoricProcessInstanceQuery()
                        .processInstanceId(processId).includeProcessVariables().singleResult();

                if (historicProcessInstance != null) {

                    Map<String, Object> allVariables = historicProcessInstance.getProcessVariables();
                    if (variablesOfInterest.size() > 0) {
                        for (String variable : allVariables.keySet()) {
                            log.debug("----VARIABLES OF PROCESS----");
                            log.debug(variable);
                            if (variable.indexOf("_PhotographAfter") != -1
                                    || variable.indexOf("_PhotographBefore") != -1
                                    || variable.indexOf("Signature") != -1 || variable.indexOf("_PhotoId") != -1) {
                                variablesOfInterest.add(variable);
                            }
                        }
                    }

                    processResultWithVariables = new ProcessResultWithVariables(historicProcessInstance,
                            variablesOfInterest.isEmpty() ? historicProcessInstance.getProcessVariables()
                                    : historicProcessInstance.getProcessVariables().entrySet().stream()
                                    .filter(entry -> variablesOfInterest.contains(entry.getKey()))
                                    .collect(HashMap::new, (m, v) -> m.put(v.getKey(), v.getValue()),
                                            HashMap::putAll));

                    processResultWithVariables.setCompleted(true);
                } else {
                    log.error("Process Id " + processId + " is neither present not past. It is missing.");
                }
            }

            return processResultWithVariables;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/tasks/find-location/{processId}/{key3}/{key4}", produces = "application/json")
    public RestResponse findAttachmentLocation(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                               @PathVariable("processId") String processId, @PathVariable("key3") String key3,
                                               @PathVariable("key4") String key4) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResponse restResponse = new RestResponse();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            RLogisticsAttachment rLogisticsAttachment = masterdataService.createRLogisticsAttachmentQuery()
                    .key2(processId).key3(key3).key4(key4).singleResult();
            if (rLogisticsAttachment != null) {
                log.debug("Inside if rLogisticsAttachment VALUE : " + rLogisticsAttachment.getLongitude());

                // Only Storing LAT and LONG coz we are getting error while
                // retrieving CONTENT-TYPE.
                // CONTENT-TYPE IS OF BYTE TYPE.We have to follow other way to
                // store it and send it.
                result.put("longitude", rLogisticsAttachment.getLongitude());
                result.put("latitude", rLogisticsAttachment.getLatitude());
                restResponse.setResponse(result);
            }
            log.debug("Outside IF : " + result);
            restResponse.setSuccess(true);
            return restResponse;
        } catch (Exception e) {
            restResponse.setSuccess(false);
            restResponse.setMessage(e.getMessage());
            log.debug("Error in getting Location : " + e);
        }
        log.debug("restresponse Value : " + restResponse);
        return restResponse;

    }

    @RequestMapping(value = "/tasks/delete/{processId}", produces = "application/json")
    public RestResponse deleteProcessInstance(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                              @PathVariable("processId") String processId, @RequestParam("email") String email) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();

        // New API For Deleting the FE Assignee Ticket

        HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getHistoryService();
        ;

        try {
            try {
                deleteFeOpenTicket(processId);
            } catch (Exception e) {
                log.debug("Error While Calling deleteFeOpenTicket()" + e);
                System.out.println("Error While Calling deleteFeOpenTicket()" + e);
            }
            RestResponse restResponse = new RestResponse();
            ProcessInstance processInstance = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();

            DeletedTicket deletedTicket = masterdataService.newDeletedTicket();
            List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService().createTaskQuery().processInstanceId(processId).list();

            if (tasks.size() > 0) {
                Task task = tasks.get(0);
                deletedTicket.setTaskName(task.getName());
            }
            deletedTicket.setProcessId(processId);

            boolean val = false;
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            Map<String, Object> variables = runtimeService.getVariables(processId);
            deletedTicket.setRlTicketNo(String.valueOf(variables.get("rlTicketNo")));
            deletedTicket.setConsumerName(String.valueOf(variables.get("consumerName")));
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            Timestamp timestamp = new Timestamp(c.getTime().getTime());
            deletedTicket.setDeletedDateTime(timestamp);
            deletedTicket.setDeletedBy(email);
            deletedTicket.setCategory(String.valueOf(variables.get("productCategory")));
            deletedTicket.setBrand(String.valueOf(variables.get("brand")));
            deletedTicket.setRetailer(String.valueOf(variables.get("retailer")));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = null;

            out = new ObjectOutputStream(bos);
            out.writeObject(variables);
            out.flush();
            byte[] yourBytes = bos.toByteArray();

            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                    .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
            commandExecutor.execute(new Command<Boolean>() {
                @Override
                public Boolean execute(CommandContext commandContext) {
                    deletedTicket.setVariablesBytearrayId(yourBytes);
                    masterdataService.saveDeletedTicket(deletedTicket);
                    return true;
                }
            });

            try {
                runtimeService.deleteProcessInstance(processId, "not required");
                historyService.deleteHistoricProcessInstance(processId);
                val = true;
                ArrayList<String> variableKeys = new ArrayList<String>();
                for (String key : variables.keySet()) {
                    variableKeys.add(key);
                }
                runtimeService.removeVariables(processId, variableKeys);

            } catch (Exception e) {

            }
            if (val) {
                restResponse.setMessage("Deleted successfully");
            }

            return restResponse;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    public Object getDate(String value) {
        String dateSuffix = value.substring(getStartIndex(value));
        Matcher matcher = datePattern.matcher(dateSuffix);

        matcher.matches();

        Calendar calendar = Calendar.getInstance();

        if (matcher.start(3) >= 0) {
            /* yyyy-mm-dd */
            calendar.set(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)) - 1,
                    Integer.parseInt(matcher.group(5)));

        } else {
            /* dd-mm-yyyy */
            if (getOp(value).equals(">=")) {
                calendar.set(Integer.parseInt(matcher.group(9)), Integer.parseInt(matcher.group(8)) - 1,
                        Integer.parseInt(matcher.group(7)));
                calendar.set(Calendar.HOUR_OF_DAY, 00);
                calendar.set(Calendar.MINUTE, 00);
                calendar.set(Calendar.SECOND, 00);
            } else if (getOp(value).equals("<=")) {
                calendar.set(Integer.parseInt(matcher.group(9)), Integer.parseInt(matcher.group(8)) - 1,
                        Integer.parseInt(matcher.group(7)));
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);

            }

        }

        return calendar.getTime();
    }

    public boolean isDate(String value) {
        return datePattern.matcher(value.substring(getStartIndex(value))).matches();
    }

    public int getStartIndex(String value) {
        int index = 0;
        if (value.charAt(index) == '>' || value.charAt(index) == '<') {
            ++index;

            if (value.charAt(index) == '=') {
                ++index;
            }
        }

        return index;
    }

    public boolean isNumber(String value) {
        return NumberUtils.isNumber(value.substring(getStartIndex(value)));
    }

    public Object getNumber(String value) {
        String numberSuffix = value.substring(getStartIndex(value));
        if (numberSuffix.indexOf('.') >= 0) {
            return new Double(Double.parseDouble(numberSuffix));
        } else {
            return new Long(Long.parseLong(numberSuffix));
        }
    }

    public String getOp(String value) {
        int index = getStartIndex(value);
        return index > 0 ? value.substring(0, index) : null;
    }

    @RequestMapping(value = {"/bulk-pickup/tickets",
            "/bulk-pickup/tickets/{firstResult}/{maxResults}"}, produces = "application/json")
    public RestExternalListResult getTicketsForBulkickup(HttpServletRequest httpRequest,
                                                         HttpServletResponse httpResponse, @RequestParam(value = "query", required = false) String queryJson,
                                                         @PathVariable Map<String, String> pathVariables,
                                                         @RequestParam(value = "pickupLocationId") String pickupLocationId,
                                                         @RequestParam(value = "email", required = false) String email,
                                                         @RequestParam(value = "deliveryBoyEmail", required = false) String deliveryBoyEmail) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        int firstResult = 0;
        int maxResults = 20;
        RestExternalListResult restResult = new RestExternalListResult();

        ArrayList<TaskQueryParameter> query = new ArrayList<>();
        try {
            System.out.println("pickupLocation: " + pickupLocationId);
            TaskQueryParameter taskQueryParameter = new TaskQueryParameter();
            taskQueryParameter.setVariable("rlPickupLocationId");
            taskQueryParameter.setValue(String.valueOf(pickupLocationId));
            query.add(taskQueryParameter);
            if (email == null) {
                taskQueryParameter = new TaskQueryParameter();
                taskQueryParameter.setVariable("rlPickUpProductReady");
                taskQueryParameter.setValue("true");
                query.add(taskQueryParameter);

                taskQueryParameter = new TaskQueryParameter();
                taskQueryParameter.setVariable("rlSellerProductPicked");
                taskQueryParameter.setValue("false");
                query.add(taskQueryParameter);

                taskQueryParameter = new TaskQueryParameter();
                taskQueryParameter.setVariable("rlDeliveryBoyForPickUp");
                taskQueryParameter.setValue(deliveryBoyEmail);
                query.add(taskQueryParameter);
            }

            ObjectMapper mapper = new ObjectMapper();
            if (queryJson != null) {
                query = mapper.readValue(URLDecoder.decode(queryJson),
                        mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            }

            Set<String> variablesOfInterest = new HashSet<String>(10);
            variablesOfInterest.add("productName");
            variablesOfInterest.add("telephoneNumber");
            variablesOfInterest.add("retailer");
            variablesOfInterest.add("problemDescription");
            variablesOfInterest.add("consumerName");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("natureOfComplaint");
            variablesOfInterest.add("rlTicketNo");
            variablesOfInterest.add("rlTicketStatus");
            variablesOfInterest.add("consumerComplaintNumber");
            variablesOfInterest.add("model");
            variablesOfInterest.add("brand");

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery();

            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();
                log.debug("value of variable: " + variable + " : " + value);

                if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                    piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    piQuery.variableValueEqualsIgnoreCase(variable, value);
                }

            }
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;
            if (pathVariables.containsKey("firstResult")) {
                firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                processInstances = piQuery.listPage(firstResult, maxResults);
            } else {
                processInstances = piQuery.list();
            }
            long totalRecords = piQuery.count();

            for (ProcessInstance processInstance : processInstances) {
                System.out.println("processId: " + processInstance.getId());
                log.debug("processId: " + processInstance.getId());
                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = null;
                if (email != null) {
                    tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getTaskService()
                            .createTaskQuery().processInstanceId(processInstance.getId()).taskAssignee(email)
                            .orderByTaskCreateTime().desc().list();
                } else {
                    tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getTaskService()
                            .createTaskQuery().processInstanceId(processInstance.getId()).orderByTaskCreateTime().desc()
                            .list();
                }
                log.debug("Tasks lenght : " + tasks);
                if (tasks != null && !tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    retval.add(new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));

                }
            }

            // form the final response
            ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>();
            for (ProcessResultWithVariables processResultWithVariables : retval) {
                Map<String, String> result = new HashMap<String, String>();
                result.put("processId", processResultWithVariables.getProcess().getProcessId());
                result.put("productName", String.valueOf(processResultWithVariables.getVariables().get("productName")));
                result.put("telephoneNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("telephoneNumber")));
                result.put("retailer", String.valueOf(processResultWithVariables.getVariables().get("retailer")));
                result.put("problemDescription",
                        String.valueOf(processResultWithVariables.getVariables().get("problemDescription")));
                result.put("consumerName",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerName")));
                result.put("productCategory",
                        String.valueOf(processResultWithVariables.getVariables().get("productCategory")));
                result.put("natureOfComplaint",
                        String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint")));
                result.put("rlTicketNo", String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo")));
                result.put("rlTicketStatus",
                        String.valueOf(processResultWithVariables.getVariables().get("rlTicketStatus")));
                result.put("consumerComplaintNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerComplaintNumber")));
                result.put("model", String.valueOf(processResultWithVariables.getVariables().get("model")));
                result.put("brand", String.valueOf(processResultWithVariables.getVariables().get("brand")));
                if (processResultWithVariables.getTask() != null
                        && processResultWithVariables.getTask().getName() != null) {
                    result.put("taskName", processResultWithVariables.getTask().getName());
                    result.put("taskId", processResultWithVariables.getTask().getTaskId());
                } else {
                    result.put("taskName", "");
                    result.put("taskId", "");
                }
                result.put("taskCreatedDate", String.valueOf(processResultWithVariables.getTask().getCreateTime()));

                data.add(result);
            }
            restResult.setResponse(data);
            restResult.setSuccess(true);
            restResult.setTotalRecords(totalRecords);

        } catch (Exception ex) {
            log.error("Exception during retailer tickets list", ex);
            restResult.setSuccess(false);
            restResult.setMessage(ex.getMessage());
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        return restResult;
    }

    /*
     * @RequestMapping(value =
     * {"/bulk-pickup/tickets","/bulk-pickup/tickets/{firstResult}/{maxResults}"
     * },produces = "application/json") public RestExternalListResult
     * getTicketsForBulkickup(HttpServletRequest httpRequest,HttpServletResponse
     * httpResponse,@RequestParam(value = "query", required = false) String
     * queryJson, @PathVariable Map<String,String>
     * pathVariables, @RequestParam(value = "pickupLocationId") String
     * pickupLocationId, @RequestParam(value = "email", required = false) String
     * email, @RequestParam(value = "deliveryBoyEmail", required = false) String
     * deliveryBoyEmail) {
     *
     * MasterdataService masterdataService = ((RLogisticsProcessEngineImpl)
     * (ProcessEngines .getDefaultProcessEngine())).getMasterdataService();
     * ProcessTaskWithPagination processTaskWithPagination = new
     * ProcessTaskWithPagination(); int firstResult = 0; int maxResults = 20;
     * RestExternalListResult restResult = new RestExternalListResult();
     *
     * ArrayList<TaskQueryParameter> query = new ArrayList<>(); try {
     * System.out.println("pickupLocation: " + pickupLocationId);
     * TaskQueryParameter taskQueryParameter = new TaskQueryParameter();
     * taskQueryParameter.setVariable("rlPickupLocationId");
     * taskQueryParameter.setValue(String.valueOf(pickupLocationId));
     * query.add(taskQueryParameter); if(email == null) { taskQueryParameter =
     * new TaskQueryParameter();
     * taskQueryParameter.setVariable("rlPickUpProductReady");
     * taskQueryParameter.setValue("true"); query.add(taskQueryParameter);
     *
     * taskQueryParameter = new TaskQueryParameter();
     * taskQueryParameter.setVariable("rlSellerProductPicked");
     * taskQueryParameter.setValue("false"); query.add(taskQueryParameter);
     *
     * taskQueryParameter = new TaskQueryParameter();
     * taskQueryParameter.setVariable("rlDeliveryBoyForPickUp");
     * taskQueryParameter.setValue(deliveryBoyEmail);
     * query.add(taskQueryParameter); }
     *
     * ObjectMapper mapper = new ObjectMapper(); if(queryJson != null) { query =
     * mapper.readValue(URLDecoder.decode(queryJson),
     * mapper.getTypeFactory().constructParametricType(ArrayList.class,
     * TaskQueryParameter.class)); }
     *
     *
     * Set<String> variablesOfInterest = new HashSet<String>(10);
     * variablesOfInterest.add("productName");
     * variablesOfInterest.add("telephoneNumber");
     * variablesOfInterest.add("retailer");
     * variablesOfInterest.add("problemDescription");
     * variablesOfInterest.add("consumerName");
     * variablesOfInterest.add("productCategory");
     * variablesOfInterest.add("productCategory");
     * variablesOfInterest.add("natureOfComplaint");
     * variablesOfInterest.add("rlTicketNo");
     * variablesOfInterest.add("rlTicketStatus");
     * variablesOfInterest.add("consumerComplaintNumber");
     * variablesOfInterest.add("model"); variablesOfInterest.add("brand");
     *
     * ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl)
     * (ProcessEngines.getDefaultProcessEngine()))
     * .getRuntimeService().createProcessInstanceQuery();
     *
     * for(TaskQueryParameter tqp:query){ String variable = tqp.getVariable();
     * String value = tqp.getValue(); log.debug("value of variable: " + variable
     * + " : " + value);
     *
     * if(value.indexOf('%') >= 0 || value.indexOf('*') >= 0){
     * piQuery.variableValueLikeIgnoreCase(variable,
     * value.replaceAll("\\*","%")); } else {
     * piQuery.variableValueEqualsIgnoreCase(variable, value); }
     *
     * } RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
     * (ProcessEngines.getDefaultProcessEngine())) .getRuntimeService();
     * RepositoryServiceImpl repositoryService =(RepositoryServiceImpl)
     * ((RLogisticsProcessEngineImpl)
     * (ProcessEngines.getDefaultProcessEngine())) .getRepositoryService();
     *
     * List<ProcessResultWithVariables> retval = new ArrayList<>();
     * List<ProcessInstance> processInstances = null;
     * if(pathVariables.containsKey("firstResult")){ firstResult =
     * Integer.parseInt(pathVariables.get("firstResult")); maxResults =
     * Integer.parseInt(pathVariables.get("maxResults"));
     *
     * processInstances = piQuery.listPage(firstResult, maxResults); } else {
     * processInstances = piQuery.list(); } long totalRecords = piQuery.count();
     *
     * for(ProcessInstance processInstance:processInstances){
     * System.out.println("processId: " + processInstance.getId()); log.debug(
     * "processId: " + processInstance.getId()); // ProcessDefinitionImpl
     * processDefinition = (ProcessDefinitionImpl)
     * repositoryService.getDeployedProcessDefinition(processInstance.
     * getProcessDefinitionId());
     *
     * List<Task> tasks = null; if(email != null) { tasks =
     * ((RLogisticsProcessEngineImpl)
     * (ProcessEngines.getDefaultProcessEngine())).getTaskService()
     * .createTaskQuery().processInstanceId(processInstance.getId()).
     * taskAssignee(email) .orderByTaskCreateTime().desc().list(); } else {
     * tasks = ((RLogisticsProcessEngineImpl)
     * (ProcessEngines.getDefaultProcessEngine())).getTaskService()
     * .createTaskQuery().processInstanceId(processInstance.getId())
     * .orderByTaskCreateTime().desc().list(); } log.debug("Tasks lenght : " +
     * tasks); if(tasks!=null && !tasks.isEmpty()){ Task task = tasks.get(0);
     * retval.add( new ProcessResultWithVariables( processInstance, task,
     * variablesOfInterest.isEmpty() ?
     * runtimeService.getVariables(processInstance.getProcessInstanceId()) :
     * runtimeService.getVariables(processInstance.getProcessInstanceId(),
     * variablesOfInterest) ) );
     *
     * } }
     *
     * //form the final response ArrayList<Map<String, String>> data = new
     * ArrayList<Map<String, String>>(); for(ProcessResultWithVariables
     * processResultWithVariables : retval) { Map<String, String> result = new
     * HashMap<String, String>(); result.put("processId",
     * processResultWithVariables.getProcess().getProcessId());
     * result.put("productName",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "productName"))); result.put("telephoneNumber",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "telephoneNumber"))); result.put("retailer",
     * String.valueOf(processResultWithVariables.getVariables().get("retailer"))
     * ); result.put("problemDescription",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "problemDescription"))); result.put("consumerName",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "consumerName"))); result.put("productCategory",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "productCategory"))); result.put("natureOfComplaint",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "natureOfComplaint"))); result.put("rlTicketNo",
     * String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo"
     * ))); result.put("rlTicketStatus",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "rlTicketStatus"))); result.put("consumerComplaintNumber",
     * String.valueOf(processResultWithVariables.getVariables().get(
     * "consumerComplaintNumber"))); result.put("model",
     * String.valueOf(processResultWithVariables.getVariables().get("model")));
     * result.put("brand",
     * String.valueOf(processResultWithVariables.getVariables().get("brand")));
     * if(processResultWithVariables.getTask() != null &&
     * processResultWithVariables.getTask().getName() != null) {
     * result.put("taskName", processResultWithVariables.getTask().getName());
     * result.put("taskId", processResultWithVariables.getTask().getTaskId()); }
     * else { result.put("taskName", ""); result.put("taskId", ""); }
     * result.put("taskCreatedDate",
     * String.valueOf(processResultWithVariables.getTask().getCreateTime()));
     *
     * data.add(result); } restResult.setResponse(data);
     * restResult.setSuccess(true); restResult.setTotalRecords(totalRecords);
     *
     *
     *
     * } catch(Exception ex){ log.error("Exception during retailer tickets list"
     * , ex); restResult.setSuccess(false);
     * restResult.setMessage(ex.getMessage());
     * httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); }
     * return restResult; }
     */

    @RequestMapping(value = "bulk/auth-letter/generate", produces = "application/json")
    public RestExternalResult generateBulkAuthLeter(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                    @RequestParam(required = true, value = "file") MultipartFile file,
                                                    @RequestParam(value = "processIds") String processIds) {
        RestExternalResult result = new RestExternalResult();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        ArrayList<String> ticketList = new ArrayList<String>();
        String ticketString = "";
        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
            return null;
        }

        try {
            ArrayList<String> processIdList = new ObjectMapper().readValue(URLDecoder.decode(processIds),
                    ArrayList.class);
            for (String processId : processIdList) {
                if (runtimeService.getVariable(processId, "rlAuthorizationLetterFileName") != null
                        && !runtimeService.getVariable(processId, "rlAuthorizationLetterFileName").equals("")) {
                    ticketList.add(String.valueOf(runtimeService.getVariable(processId, "rlTicketNo")));
                    if (!ticketString.equals("")) {
                        ticketString += ", " + String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
                    } else {
                        ticketString = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
                    }

                }
            }

            // generate document
            XWPFDocument document = new XWPFDocument();
            XWPFParagraph tmpParagraph = document.createParagraph();
            XWPFRun tmpRun = tmpParagraph.createRun();
            tmpRun.setText("Bulk Authorization Letter");
            tmpRun.setBold(true);
            tmpRun.setFontSize(20);
            XWPFRun tmpRun2 = tmpParagraph.createRun();
            tmpRun2.setText("Ticket Nos: " + ticketString);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            document.write(baos);
            byte[] docx = baos.toByteArray();
            baos.flush();

            // write a document to db
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();
            RLogisticsAttachment attachment = masterdataService.createRLogisticsAttachmentQuery().key1("AuthLetter")
                    .key2("bulk").singleResult();
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("AuthLetter");
                newAttachment.setKey2("bulk");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                newAttachment.setName("BulkAuthorizationLetter.docx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(docx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("AuthLetter");
                newAttachment.setKey2("bulk");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                newAttachment.setName("BulkAuthorizationLetter.docx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(docx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }

        } catch (

                Exception ex) {
            log.error("Exception during upload", ex);
            httpResponse.setStatus(500);
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            return result;
        }

        return result;
    }

    @RequestMapping(value = "/declaration-form/generate", produces = "application/json")
    public RestResult generateDeclarationForm(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                              @RequestParam(value = "processId") String processId) {
        RestResult result = new RestResult();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        // runtimeService.setVariable(processId, "isDeclaired","no");

        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
            return null;
        }

        XSSFWorkbook workbook = null;
        XSSFSheet sheet = null;
        Row row = null;
        Cell cell = null;
        try {

            Map<String, Object> variables = runtimeService.getVariables(processId);

            workbook = new XSSFWorkbook();
            sheet = (XSSFSheet) workbook.createSheet("DeclarationFormSheet");

            // heading
            row = sheet.createRow(0);
            cell = row.createCell(3);
            XSSFCellStyle cellStyle = workbook.createCellStyle();
            cellStyle = workbook.createCellStyle();
            XSSFFont hSSFFont = workbook.createFont();
            hSSFFont.setFontHeightInPoints((short) 48);
            cellStyle.setFont(hSSFFont);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Bizlog");

            // date
            row = sheet.createRow(3);
            cell = row.createCell(7);
            cellStyle = workbook.createCellStyle();
            hSSFFont = workbook.createFont();
            hSSFFont.setFontHeightInPoints((short) 16);
            cellStyle.setFont(hSSFFont);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Date : ");

            // Consumer Name
            row = sheet.createRow(6);
            cell = row.createCell(0);
            cell.setCellValue("Consumer Name : ");
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(variables.get("consumerName")));

            // Ticket Number
            row = sheet.createRow(8);
            cell = row.createCell(0);
            cell.setCellValue("Ticket Number : ");
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(variables.get("rlTicketNo")));

            // Address
            row = sheet.createRow(10);
            cell = row.createCell(0);
            cell.setCellValue("Address : ");
            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(variables.get("addressLine1")));

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            byte[] xlsx = baos.toByteArray();
            baos.flush();

            // write the excel file to database
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();
            RLogisticsAttachment attachment = masterdataService.createRLogisticsAttachmentQuery()
                    .key1("declarationForm").key2(processId).singleResult();
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("declarationForm");
                newAttachment.setKey2(processId);
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("DeclarationForm.xlsx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("declarationForm");
                newAttachment.setKey2(processId);
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("DeclarationForm.xlsx");
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }
            result.setMessage("Declaration form generated successfully");
            runtimeService.setVariable(processId, "isDeclaired", "yes");

        } catch (

                Exception ex) {
            log.error("Exception during generating form", ex);
            httpResponse.setStatus(500);
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            return result;
        }

        return result;
    }

    @RequestMapping(value = {"/finance/tickets",
            "/finance/tickets/{firstResult}/{maxResults}"}, produces = "application/json")
    public RestExternalListResult ticketSearchForFinance(HttpServletRequest httpRequest,
                                                         HttpServletResponse httpResponse, @RequestParam(value = "query", required = false) String queryJson,
                                                         @PathVariable Map<String, String> pathVariables,
                                                         @RequestParam(value = "pickupLocationId") String pickupLocationId) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
        int firstResult = 0;
        int maxResults = 20;
        RestExternalListResult restResult = new RestExternalListResult();

        ArrayList<TaskQueryParameter> query = new ArrayList<>();
        try {
            System.out.println("pickupLocation: " + pickupLocationId);
            TaskQueryParameter taskQueryParameter = new TaskQueryParameter();
            taskQueryParameter.setVariable("rlPickupLocationId");
            taskQueryParameter.setValue(String.valueOf(pickupLocationId));
            query.add(taskQueryParameter);

            ObjectMapper mapper = new ObjectMapper();
            if (queryJson != null) {
                query = mapper.readValue(URLDecoder.decode(queryJson),
                        mapper.getTypeFactory().constructParametricType(ArrayList.class, TaskQueryParameter.class));
            }

            Set<String> variablesOfInterest = new HashSet<String>(10);
            variablesOfInterest.add("productName");
            variablesOfInterest.add("telephoneNumber");
            variablesOfInterest.add("retailer");
            variablesOfInterest.add("problemDescription");
            variablesOfInterest.add("consumerName");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("productCategory");
            variablesOfInterest.add("natureOfComplaint");
            variablesOfInterest.add("rlTicketNo");
            variablesOfInterest.add("rlTicketStatus");
            variablesOfInterest.add("consumerComplaintNumber");
            variablesOfInterest.add("model");
            variablesOfInterest.add("brand");

            ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService().createProcessInstanceQuery();

            for (TaskQueryParameter tqp : query) {
                String variable = tqp.getVariable();
                String value = tqp.getValue();
                log.debug("value of variable: " + variable + " : " + value);

                if (value.indexOf('%') >= 0 || value.indexOf('*') >= 0) {
                    piQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    piQuery.variableValueEqualsIgnoreCase(variable, value);
                }

            }
            RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getRuntimeService();
            RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getRepositoryService();

            List<ProcessResultWithVariables> retval = new ArrayList<>();
            List<ProcessInstance> processInstances = null;
            if (pathVariables.containsKey("firstResult")) {
                firstResult = Integer.parseInt(pathVariables.get("firstResult"));
                maxResults = Integer.parseInt(pathVariables.get("maxResults"));

                processInstances = piQuery.listPage(firstResult, maxResults);
            } else {
                processInstances = piQuery.list();
            }
            long totalRecords = piQuery.count();

            for (ProcessInstance processInstance : processInstances) {
                System.out.println("processId: " + processInstance.getId());
                log.debug("processId: " + processInstance.getId());
                // ProcessDefinitionImpl processDefinition =
                // (ProcessDefinitionImpl)
                // repositoryService.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

                List<Task> tasks = null;

                tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getTaskService()
                        .createTaskQuery().processInstanceId(processInstance.getId()).orderByTaskCreateTime().desc()
                        .list();

                log.debug("Tasks lenght : " + tasks);
                if (tasks != null && !tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    retval.add(new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));

                }
            }

            // form the final response
            ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>();
            for (ProcessResultWithVariables processResultWithVariables : retval) {
                Map<String, String> result = new HashMap<String, String>();
                result.put("processId", processResultWithVariables.getProcess().getProcessId());
                result.put("productName", String.valueOf(processResultWithVariables.getVariables().get("productName")));
                result.put("telephoneNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("telephoneNumber")));
                result.put("retailer", String.valueOf(processResultWithVariables.getVariables().get("retailer")));
                result.put("problemDescription",
                        String.valueOf(processResultWithVariables.getVariables().get("problemDescription")));
                result.put("consumerName",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerName")));
                result.put("productCategory",
                        String.valueOf(processResultWithVariables.getVariables().get("productCategory")));
                result.put("natureOfComplaint",
                        String.valueOf(processResultWithVariables.getVariables().get("natureOfComplaint")));
                result.put("rlTicketNo", String.valueOf(processResultWithVariables.getVariables().get("rlTicketNo")));
                result.put("rlTicketStatus",
                        String.valueOf(processResultWithVariables.getVariables().get("rlTicketStatus")));
                result.put("consumerComplaintNumber",
                        String.valueOf(processResultWithVariables.getVariables().get("consumerComplaintNumber")));
                result.put("model", String.valueOf(processResultWithVariables.getVariables().get("model")));
                result.put("brand", String.valueOf(processResultWithVariables.getVariables().get("brand")));
                if (processResultWithVariables.getTask() != null
                        && processResultWithVariables.getTask().getName() != null) {
                    result.put("taskName", processResultWithVariables.getTask().getName());
                    result.put("taskId", processResultWithVariables.getTask().getTaskId());
                } else {
                    result.put("taskName", "");
                    result.put("taskId", "");
                }
                result.put("taskCreatedDate", String.valueOf(processResultWithVariables.getTask().getCreateTime()));

                data.add(result);
            }
            restResult.setResponse(data);
            restResult.setSuccess(true);
            restResult.setTotalRecords(totalRecords);

        } catch (Exception ex) {
            log.error("Exception during retailer tickets list", ex);
            restResult.setSuccess(false);
            restResult.setMessage(ex.getMessage());
            httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        return restResult;
    }

    @RequestMapping(value = "/update-product/cost", produces = "application/json")
    public RestResult updateProductCost(@RequestParam(value = "processId") String processId,
                                        @RequestParam(value = "newprodCost") String newprodCost) {
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        try {
            ProcessResponseWithVariables processResponseWithVariables = AMPMResource.findByProcessId(processId);
            String assignee = processResponseWithVariables.getTask().getAssignee();

            // set prcoess variable with the upadted valueki
            runtimeService.setVariable(processId, "rlValueOffered", newprodCost);
            runtimeService.setVariable(processId, "isCostUpdated", "yes");

            System.out.println("Updated Value : " + runtimeService.getVariable(processId, "rlValueOffered"));
            String msg = "Updated cost " + newprodCost + ".";
            try {
                DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
                List<DeviceInfo> deviceInfoList = deviceInfoQuery.userId(assignee).list();
                log.debug("*******************" + deviceInfoList);

                String natureOfComplaint = String.valueOf(runtimeService.getVariable(processId, "natureOfComplaint"));
                String type = "";
                if (natureOfComplaint.equalsIgnoreCase("Buy Back")) {
                    type = "buyBack";
                } else if (natureOfComplaint.equalsIgnoreCase("RepairStandBy")) {
                    type = "repairStandBy";
                } else {
                    type = "eWaste";
                }
                for (DeviceInfo deviceInfo : deviceInfoList) {
                    String deviceId = deviceInfo.getDeviceId();
                    AndroidPush.pushFCMNotification(deviceId, processId, type, newprodCost,
                            runtimeService.getVariable(processId, "rlTicketNo").toString());
                }

                restResult.setResponse("success");
                log.debug("*****" + restResult.getResponse());
                restResult.setMessage("Product Cost Is Updated!");

            } catch (Exception s) {
                System.out.println("Error in sending push");
                log.debug("Error in sending push : " + s.getMessage());
            }
        } catch (Exception e) {
            restResult.setResponse("failure");
            restResult.setMessage("Some Error While Updating Product Cost.");
            System.out.println("error in Updating Cost");
            log.debug("error in updating Cost" + e.getMessage());
        }
        return restResult;

    }

    /**
     * API FOR STORING THE DAMAGE COST DETAIL AND MAKING PUSH NOTIFICATION AS
     * WELL AS SENDING EMAIL .
     */
    @RequestMapping(value = "/set-damage/cost", produces = "application/json")
    public RestResult updateDamageCost(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                       @RequestParam(value = "processId") String processId,
                                       @RequestParam(value = "damageCost") String damageCost) {
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        try {
            ProcessResponseWithVariables processResponseWithVariables = AMPMResource.findByProcessId(processId);
            String assignee = processResponseWithVariables.getTask().getAssignee();

            // set prcoess variable with the Damaged Product value
            if (damageCost != "" || damageCost != null) {
                runtimeService.setVariable(processId, "quotedValue", damageCost);
                runtimeService.setVariable(processId, "isDamageCostUpdated", "yes");
            }
            log.debug("Assignee :" + assignee);
            log.debug("Quoted Value : " + damageCost);
            log.debug("Process iD: " + processId);
            System.out.println("Updated Value : " + runtimeService.getVariable(processId, "quotedValue"));
            String msg = "Damage cost " + damageCost + ".";
            try {
                DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
                List<DeviceInfo> deviceInfoList = deviceInfoQuery.userId(assignee).list();

                String natureOfComplaint = String.valueOf(runtimeService.getVariable(processId, "natureOfComplaint"));
                String type = "";
                if (natureOfComplaint.equalsIgnoreCase("RepairStandBy"))
                    type = "repairStandBy";
                for (DeviceInfo deviceInfo : deviceInfoList) {
                    String deviceId = deviceInfo.getDeviceId();
                    log.debug("deviceId : " + deviceId);
                    AndroidPush.pushFCMNotification(deviceId, processId, type, damageCost);
                }

                restResult.setResponse("success");
                restResult.setMessage("Damage Cost Notification Sent!");

            } catch (Exception s) {
                System.out.println("Error in sending push");
                log.debug("Error in sending push : " + s.getMessage());
            }
        } catch (Exception e) {
            restResult.setResponse("failure");
            restResult.setMessage("Some Error While Creating Damage Cost.");
            System.out.println("error in Creating Damage Cost");
            log.debug("error in Creating Damage Cost" + e.getMessage());
        }
        return restResult;

    }

    // API For DateWise open and Closed Tickets.
    @RequestMapping(value = {"/tasks/tickets/statistics",
            "/tasks/tickets/statistics/{firstResult}/{maxResults}"}, produces = "application/json")
    public void listOpenClosedTickets(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                      @PathVariable Map<String, String> pathVariables) {

        MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        MasterdataService masterdataService2 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        List<String> dateList = new ArrayList<String>();
        List<String> nonComList = new ArrayList<String>();
        List<OpenTickets> openTickets = masterdataService1.createOpenTicketsQuery().list();
        for (OpenTickets ot : openTickets) {
            dateList.add(ot.getCreatedDate());
        }
        List<ClosedTicket> closedTicket = masterdataService2.createClosedTicketQuery().list();
        for (ClosedTicket ct : closedTicket) {
            dateList.add(ct.getEndDate());
        }
        Collections.reverse(dateList);
        Set<String> s = new LinkedHashSet<>(dateList);
        System.out.println("dateList : " + s);
        for (OpenTickets ot : openTickets) {
            nonComList.add(ot.getTicketNo());
        }
    }

    // API For Search the IMEI INVENTORY TABLE ON THE BASIS OF SOME FILTERS.
    @RequestMapping(value = {"/search/imei-by-variable",
            "/search/imei-by-variable/{firstResult}/{maxResults}"}, produces = "application/json")
    public ImeiInventoryResultResponse searchImeiByFilter(HttpServletRequest httpRequest,
                                                          HttpServletResponse httpResponse, @RequestParam("query") String queryJson,
                                                          @PathVariable Map<String, String> pathVariables, @RequestParam("firstResult") String firstResult1,
                                                          @RequestParam("maxResults") String maxResults1) {

        // ProcessEngineConfigurationImpl pec = (ProcessEngineConfigurationImpl)
        // ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration();

        // SqlSession session = pec.getSqlSessionFactory().openSession();

        PaginationResponse paginationResponse = new PaginationResponse();
        String location = "";
        String brand = "";
        String imeiNo = "";
        String model = "";
        String year = "";
        String status = "";
        String barcode = "";
        String retailerId = "";
        String productCategory = "";
        String productValue = "";
        try {
            String firstResult = "0";
            String maxResults = "10";
            Map<String, String> queryObj = new HashMap<String, String>();
            try {
                queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (queryObj.containsKey("location")) {
                location = queryObj.get("location").toLowerCase();
            }
            if (queryObj.containsKey("brand")) {
                brand = queryObj.get("brand").toLowerCase();
            }
            if (queryObj.containsKey("imeiNo")) {
                imeiNo = queryObj.get("imeiNo");
            }
            if (queryObj.containsKey("barcode")) {
                barcode = queryObj.get("barcode");
            }
            if (queryObj.containsKey("model")) {
                model = queryObj.get("model");
            }
            if (queryObj.containsKey("year")) {
                year = queryObj.get("year");
            }
            if (queryObj.containsKey("status")) {
                status = queryObj.get("status");
            }
            if (queryObj.containsKey("retailerId")) {
                retailerId = queryObj.get("retailerId");
            }
            if (queryObj.containsKey("productCategory")) {
                productCategory = queryObj.get("productCategory");
            }
            if (queryObj.containsKey("productValue")) {
                productValue = queryObj.get("productValue");
            }

            if (pathVariables.containsKey("firstResult")) {
                firstResult = pathVariables.get("firstResult");
                maxResults = pathVariables.get("maxResults");
            }
            if (!(firstResult1.equals(null) || firstResult1.equals("")))
                firstResult = firstResult1;
            if (!(maxResults1.equals(null) || maxResults1.equals("")))
                maxResults = maxResults1;

            System.out.println("Aboe Quero");
            try {
                String cost1 = "";
                String cost2 = "";
                if (!productValue.equalsIgnoreCase("")) {
                    if (productValue.equalsIgnoreCase("0")) {
                        cost1 = "0";
                        cost2 = "9999";
                    }
                    if (productValue.equalsIgnoreCase("1")) {
                        cost1 = "10000";
                        cost2 = "20000";
                    }
                    if (productValue.equalsIgnoreCase("2")) {
                        cost1 = "20000";
                        cost2 = "30000";
                    }
                    if (productValue.equalsIgnoreCase("3")) {
                        cost1 = "30001";
                        cost2 = "5000000";
                    }
                }
                ImeiInventoryResultResponse inventorySearchResponse = new ImeiInventoryResultResponse();

                // List<ImeiInventoryResult>
                List<ImeiInventoryResult> imeiInventoryResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .findImeiList(location, brand, imeiNo, model, year, status, barcode, retailerId,
                                productCategory, cost1, cost2, firstResult, maxResults);
                List<ImeiInventoryResult> imeiInventoryExcelResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .findImeiListForExcel(location, brand, imeiNo, model, year, status, barcode, retailerId,
                                productCategory, cost1, cost2);
                generateImeiExcel(imeiInventoryExcelResult);

                ResultCount resultCount = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
                        .findImeiInventoryCount(location, brand, imeiNo, model, year, status, barcode, productCategory,
                                retailerId, cost1, cost2);
                System.out.println("After 2nd Query");
                int totalRecords = Integer.valueOf(resultCount.getTotalRecords());

                inventorySearchResponse.setData(imeiInventoryResult);
                inventorySearchResponse.setTotalRecords(totalRecords);

                return inventorySearchResponse;
            } catch (Exception e) {
                log.debug("Error in Quering DB " + e);
                System.out.println("error in DB Query : " + e);
            }

            return null;
        } finally {

        }
    }

    /**
     * API TO WRITE AN EXCEL FOR IMEI SEARCH OPTION AND STORE IT IN ATTACHMENT
     * TABLE
     **/
    @RequestMapping(value = {"/generate/search/imei-excel"}, produces = "application/json")
    public RestResponse generateImeiExcel(List<ImeiInventoryResult> imeiInventoryExcelResult) {

        MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();

        log.debug("inside generateImeiExcel :" + imeiInventoryExcelResult);
        RestResponse restResponse = new RestResponse();
        XSSFWorkbook workbook = null;
        XSSFSheet sheet = null;
        Cell cell = null;
        ArrayList<String> orderedFields = new ArrayList<String>();

        try {
            workbook = new XSSFWorkbook();
            XSSFCellStyle style = workbook.createCellStyle();
            style = workbook.createCellStyle();
            XSSFFont hSSFFont = workbook.createFont();
            hSSFFont.setFontHeightInPoints((short) 13);
            // ((Font) style).setColor(IndexedColors.BLUE.getIndex());
            hSSFFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(hSSFFont);
            sheet = (XSSFSheet) workbook.createSheet("Search_Results");
            Row firstRow = sheet.createRow(0);
            cell = firstRow.createCell(0);
            cell.setCellValue("S.No");
            cell.setCellStyle(style);
            cell = firstRow.createCell(1);
            cell.setCellValue("Retailer Name");
            cell.setCellStyle(style);
            cell = firstRow.createCell(2);
            cell.setCellValue("Warehouse Location");
            cell.setCellStyle(style);
            cell = firstRow.createCell(3);
            cell.setCellValue("Product Category");
            cell.setCellStyle(style);
            cell = firstRow.createCell(4);
            cell.setCellValue("Brand");
            cell.setCellStyle(style);
            cell = firstRow.createCell(5);
            cell.setCellValue("Model");
            cell.setCellStyle(style);
            cell = firstRow.createCell(6);
            cell.setCellValue("Year");
            cell.setCellStyle(style);
            cell = firstRow.createCell(7);
            cell.setCellValue("Product Value");
            cell.setCellStyle(style);
            cell = firstRow.createCell(8);
            cell.setCellValue("Status");
            cell.setCellStyle(style);
            cell = firstRow.createCell(9);
            cell.setCellValue("IMEI NO.");
            cell.setCellStyle(style);
            cell = firstRow.createCell(10);
            cell.setCellValue("Barcode");
            cell.setCellStyle(style);

            int rowNum = 1;
            int sno = 0;
            // dataArray =
            log.debug("HEADER is DONE HERE)");
            List<Retailer> retailers = null;
            try {
                retailers = masterdataService1.createRetailerQuery().list();
            } catch (Exception e) {
                log.debug("Error in Getting Retailer Name");
            }
            try {
                for (ImeiInventoryResult imeiExcelResult : imeiInventoryExcelResult) {
                    Row row = sheet.createRow(rowNum++);
                    int colNum = 0;
                    sno++;
                    cell = row.createCell(colNum++);
                    cell.setCellValue(sno);

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getRetailerId() != null && !imeiExcelResult.getRetailerId().isEmpty())) {
                            int flag = 0;
                            for (Retailer retailer : retailers) {
                                if (retailer.getId().equals(imeiExcelResult.getRetailerId())) {
                                    cell.setCellValue(retailer.getName());
                                    flag++;
                                    break;
                                }
                            }
                            if (flag == 0) {
                                cell.setCellValue("");
                            }
                        }
                    } catch (Exception e) {
                        log.debug("Error reading ReatailerId : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getLocation() != null && !imeiExcelResult.getLocation().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getLocation());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Location NAme : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getProductCategory() != null
                                && !imeiExcelResult.getProductCategory().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getProductCategory());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Product Category : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getBrand() != null && !imeiExcelResult.getBrand().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getBrand());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Brand : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getModel() != null && !imeiExcelResult.getModel().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getModel());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Model : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getYear() != null && !imeiExcelResult.getYear().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getYear());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Year : " + e);
                    }
                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getProductCost() != null && !imeiExcelResult.getProductCost().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getProductCost());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading product Cost : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if (imeiExcelResult.getStatus() < 3) {
                            if (imeiExcelResult.getStatus() == 1)
                                cell.setCellValue("Issued");
                            if (imeiExcelResult.getStatus() == 0)
                                cell.setCellValue("Available");
                            if (imeiExcelResult.getStatus() == 2)
                                cell.setCellValue("Damage");
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Status : " + e);
                    }

                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getImeiNo() != null && !imeiExcelResult.getImeiNo().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getImeiNo());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading IMEI nO : " + e);
                    }
                    cell = row.createCell(colNum++);
                    try {
                        if ((imeiExcelResult.getBarcode() != null && !imeiExcelResult.getBarcode().isEmpty())) {
                            cell.setCellValue(imeiExcelResult.getBarcode());
                        } else {
                            cell.setCellValue("");
                        }
                    } catch (Exception e) {
                        log.debug("Error reading Barcode : " + e);
                    }
                }
            } catch (Exception e) {
                log.debug("Error while writing Data To Excel : " + e);
            }
            log.debug("All DATA IS inserted In EXCEL");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            byte[] xlsx = baos.toByteArray();
            baos.flush();

            // write the excel file to database
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                    .getDefaultProcessEngine())).getMasterdataService();
            log.debug("before quering Attachment Table");
            RLogisticsAttachment attachment = null;
            if (masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("imei").key3("result")
                    .singleResult() != null) {
                attachment = masterdataService.createRLogisticsAttachmentQuery().key1("search").key2("imei")
                        .key3("result").singleResult();
            }
            if (attachment == null) {
                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();
                System.out.println("inside attachemnt if block");
                log.debug("After quering Attachment Table If Block");

                newAttachment.setKey1("search");
                newAttachment.setKey2("imei");
                newAttachment.setKey3("result");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                newAttachment.setName("Search_Results.xlsx");
                /* Copy uploaded bytes into a byte[] */
                log.debug("key value attachment is null here" + newAttachment.getKey3());

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            } else {
                masterdataService.deleteRLogisticsAttachment(attachment.getId());
                System.out.println("inside attachment else block");
                log.debug("After quering Attachment Table ELSE Block");

                RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

                newAttachment.setKey1("search");
                newAttachment.setKey2("imei");
                newAttachment.setKey3("result");
                newAttachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                String date = com.rlogistics.util.DateUtil.getCurrentDate();
                String attachmentName = date + "-Search_Results.xlsx";
                newAttachment.setName(attachmentName);
                log.debug("key value attachment is NOT null here" + newAttachment.getKey3());
                /* Copy uploaded bytes into a byte[] */

                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
                        .getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                commandExecutor.execute(new Command<Boolean>() {

                    @Override
                    public Boolean execute(CommandContext commandContext) {
                        newAttachment.setContent(xlsx);
                        masterdataService.saveRLogisticsAttachment(newAttachment);
                        return true;
                    }
                });

            }

            restResponse.setMessage("successfully created Excel");
            restResponse.setSuccess(true);

        } catch (

                Exception e) {
            restResponse.setSuccess(false);
            restResponse.setMessage(e.getMessage());
        }

        return restResponse;
    }

    @RequestMapping(value = {"/task/update/fe-ticket-status"}, produces = "application/json")
    public RestResult updateFETicketStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                           @RequestParam("query") String queryJson) {

        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        Map<String, String> queryObj = new HashMap<String, String>();
        try {
            queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String processId = "";
        int calling = 0;
        int key = 0;
        if (queryObj.containsKey("processId")) {
            processId = queryObj.get("processId");
        }
        if (queryObj.containsKey("calling")) {
            calling = Integer.parseInt(queryObj.get("calling"));
        }

        if (calling == 1) {
            runtimeService.setVariable(processId, "isBlockToFe", "no");
            restResult.setMessage("Successfully Unblocked");
            restResult.setSuccess(true);
        }
        if (calling == 2) {
            runtimeService.setVariable(processId, "isBlockToFe", "yes");
            restResult.setMessage("Successfully Blocked");
            restResult.setSuccess(true);
        }
        if (calling == 3) {
            String blockKey = String.valueOf(runtimeService.getVariable(processId, "isBlockToFe"));
            if (blockKey.equalsIgnoreCase("yes")) {
                restResult.setMessage("Ticket is Blocked");
                key = 1;
                restResult.setResponse(key);
                restResult.setSuccess(true);
            } else {
                key = 2;
                restResult.setResponse(key);
                restResult.setMessage("Ticket is Un-Blocked");
                restResult.setSuccess(true);
            }
        }
        return restResult;
    }

    /**
     * UPDATING FCM KEY TO SEND PUSH NOTIFICATION ONLY TO THE CURRENT ACCESSING
     * DEVICE
     */
    @RequestMapping(value = {"update/user/fcm-key"}, produces = "application/json")
    public RestResult updateFEFcmKey(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                     @RequestParam("userId") String userId, @RequestParam("fcmKey") String fcmKey) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
        DeviceInfo deviceInfo = deviceInfoQuery.userId(userId).singleResult();
        log.debug("userId : " + userId);
        log.debug("fcmKey : " + fcmKey);
        if (deviceInfo == null) {
            deviceInfo = masterdataService.newDeviceInfo();
            log.debug("Inside Create FCM Key");
            deviceInfo.setId(String.valueOf(UUID.randomUUID()));
            deviceInfo.setDeviceId(fcmKey);
            deviceInfo.setUserId(userId);

            try {
                masterdataService.saveDeviceInfo(deviceInfo);
                restResult.setResponse("Created");
                restResult.setMessage("Successfully Created");
                restResult.setSuccess(true);
            } catch (Exception ex) {
                log.debug("Error in Creating FCM Key :" + ex);
            }
        } else {
            log.debug("Inside Update FCM Key");
            deviceInfo.setDeviceId(fcmKey);
            deviceInfo.setUserId(userId);
            try {
                masterdataService.saveDeviceInfo(deviceInfo);
                restResult.setMessage("Successfully Updated");
                restResult.setResponse("Updated");
                restResult.setSuccess(true);

            } catch (Exception ex) {
                log.debug("Error in Updating FCM Key :" + ex);
            }
        }
        return restResult;
    }

    /**
     * @param assigneeId
     * @return Phone Number of Assinee
     */
    @RequestMapping(value = {"/get/assignee/number"}, produces = "application/json")
    public AssigneeNumber getAssigneeNumber(@RequestParam("id") String assigneeId) {
        // TODO: 30-Jul-18 logic to retrive assignee phone number in web-app
        AssigneeNumber assigneeNumber = new AssigneeNumber();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        try {
            ProcessUser processUser = masterdataService.createProcessUserQuery().email(assigneeId).singleResult();
            assigneeNumber.setData(processUser.getPhone());

        } catch (Exception e) {
            log.debug("Either Multiple or no Phone number available " + e);
            e.printStackTrace();

        }
        log.debug(assigneeNumber.toString());
        return assigneeNumber;
    }

}
