package com.rlogistics.rest;

import lombok.Data;

@Data
public class PincodeValidationResponse {
   private boolean isValid = false;
   private String message;
}
