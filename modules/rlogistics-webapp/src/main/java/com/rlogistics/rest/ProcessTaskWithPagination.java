package com.rlogistics.rest;

import java.util.List;

public class ProcessTaskWithPagination {

	List<ProcessResultWithVariables> data;
	long totalRecords;
	String message;
	boolean success=true;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ProcessResultWithVariables> getData() {
		return data;
	}
	public void setData(List<ProcessResultWithVariables> data) {
		this.data = data;
	}
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
}
