package com.rlogistics.rest.controllers.dto;

import com.rlogistics.master.identity.TicketImages;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ImeiWithImageDetails {
    private String imeiNo;
    private List<TicketImages> images = new ArrayList<>();
    private boolean success = false;
}
