package com.rlogistics.rest.bulkApiForClient.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.BizlogServiceablePincode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("ALL")
public
class DropDetails {

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("productCategory")
    private String productCategory;

    @JsonProperty("productName")
    private String productName;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("model")
    private String model;

    @JsonProperty("identificationNo")
    @NotNull(message = "Imei Number is required!")
    @NotBlank(message = "Imei Number cannot be blank")
    @Pattern(regexp = "^\\d{15}(,\\d{15})*$", message = "Imei Number is invalid!")
    private String identificationNo;

    @JsonProperty("dropLocation")
    private String dropLocation;

    @JsonProperty("dropLocAddress1")
    private String dropLocAddress1;

    @JsonProperty("dropLocAddress2")
    private String dropLocAddress2;

    @JsonProperty("dropLocCity")
    private String dropLocCity;

    @JsonProperty("dropLocState")
    private String dropLocState;

    @JsonProperty("dropLocPincode")
    @BizlogServiceablePincode
    private String dropLocPincode;

    @JsonProperty("dropLocContactPerson")
    private String dropLocContactPerson;

    @JsonProperty("dropLocContactNo")
    private String dropLocContactNo;

    @JsonProperty("physicalEvaluation")
    private String physicalEvaluation;

}
