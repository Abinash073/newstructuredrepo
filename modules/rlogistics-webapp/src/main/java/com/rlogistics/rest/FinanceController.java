
package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rlogistics.master.identity.FinanceDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.task.Task;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.FinanceSearchResultSData;
import com.rlogistics.customqueries.FinanceSearchResults;
import com.rlogistics.customqueries.ImeiInventoryResult;
import com.rlogistics.customqueries.ResultCount;
import com.rlogistics.customqueries.SingleFeTrackResult;
import com.rlogistics.customqueries.TrackFeLocResult;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.TrackFeLocation;

@RestController
public class FinanceController extends RLogisticsResource {

	private static Logger log = LoggerFactory.getLogger(TaskResource.class);

	/**
	 * This API Is Used by ProcessEnd To Store The Payments Details Of The
	 * Respective Retailers.
	 */

	public static void createFinanceDetails(String processId, String ticketNo, String retailerId, String custLoc,
			String activitiType, String collectFrom, String payTO, String modeOfPayment, String amount,
			String processFlow, String locationId) {

		log.debug("processId	 : " + processId);
		log.debug("ticketNo		 : " + ticketNo);
		log.debug("retailerId	 : " + retailerId);
		log.debug("custLoc		 : " + custLoc);
		log.debug("activitiType	 : " + activitiType);
		log.debug("payFrom		 : " + collectFrom);
		log.debug("payTO		 : " + payTO);
		log.debug("modeOfPayment : " + modeOfPayment);
		log.debug("amount		 : " + amount);
		log.debug("processFlow 	 : " + processFlow);
		log.debug("LocationID 	 : " + locationId);

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		FinanceDetails financeDetails = null;
		try {
			// String retailerLoc = "";
			financeDetails = masterdataService.newFinanceDetails();

			Date date = Calendar.getInstance().getTime();

			// Display a date in day, month, year format
			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String today = formatter.format(date);
			financeDetails.setCreatedDate(formatter.parse(today));
			// date = formatter.parse();

			if (processId != null || processId != "")
				financeDetails.setProcessId(processId);

			if (locationId != null || locationId != "")
				financeDetails.setCustomerLocation(locationId);

			if (ticketNo != null || ticketNo != "")
				financeDetails.setTicketNo(ticketNo);
			if (retailerId != null || retailerId != "")
				financeDetails.setRetailerId(retailerId);
			if (custLoc != null || custLoc != "")
				financeDetails.setLocationId(custLoc);
			if (activitiType != null || activitiType != "")
				financeDetails.setActivitiType(activitiType);
			if (collectFrom != null || collectFrom != "")
				financeDetails.setCollectFrom(collectFrom);
			if (payTO != null || payTO != "")
				financeDetails.setPayTo(payTO);
			if (modeOfPayment != null || modeOfPayment != "")
				financeDetails.setModeOfPayment(modeOfPayment);
			if (processFlow != null || processFlow != "")
				financeDetails.setProcessFlow(processFlow);
			if (!(activitiType.equalsIgnoreCase("service charge"))) {
				if (amount != null || amount != "")
					financeDetails.setAmount(Float.valueOf(amount));
			} else {
				// Getting Service Charge From TicketCosting Table
				try {
					float serviceAmount = (float) runtimeService.getVariable(processId, "rlTicketCost");
					financeDetails.setAmount(serviceAmount);
					log.debug("serviceAmount : " + serviceAmount);
					log.debug("For ProcessId : " + processId);
				} catch (Exception e) {
					log.debug("Error in getting rlTicketCost value :" + e);
				}
			}
			try {
				masterdataService.saveFinanceDetails(financeDetails);
			} catch (Exception e) {
				log.debug("Error While Saving Finance Details Date:" + e);
			}
		} catch (Exception e) {
			log.debug("Error in creating financeDetail Object :" + e);
		}

	}

	@RequestMapping(value = { "/search/finance/details" }, produces = "application/json")
	public RestResult searchFinanceDetails(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("query") String queryJson, @RequestParam("firstResult") String firstResult,
			@RequestParam("maxResults") String maxResults) {

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestResult restResult = new RestResult();

		Map<String, String> queryObj = new HashMap<String, String>();

		System.out.println("Query Json :" + queryJson);
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String retailerId = "";
		String locationId = "";
		String startDate = "";
		String endDate = "";
		String firstResult1 = "";
		String maxResults1 = "";
		String formatStartDate = "";
		String formatEndDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		// Date date = dateFormat.parse("1.1.2001");06-02-2018
		if (!(firstResult.equals(null) || firstResult.equals("")))
			firstResult1 = firstResult;
		if (!(maxResults.equals(null) || maxResults.equals("")))
			maxResults1 = maxResults;
		if (queryObj.containsKey("retailerId")) {
			retailerId = queryObj.get("retailerId");
			log.debug("retailerId : " + retailerId);
			System.out.println("retailerId : " + retailerId);
		}
		if (queryObj.containsKey("locationId")) {
			locationId = queryObj.get("locationId");
			System.out.println("loc");
		}
		if (queryObj.containsKey("startDate")) {
			startDate = queryObj.get("startDate");
			log.debug("startDate : " + startDate);
			System.out.println("startDate : " + startDate);
		}
		if (queryObj.containsKey("endDate")) {
			endDate = queryObj.get("endDate");
			log.debug("endDate : " + endDate);
		}
		try {
			log.debug("Start Date : " + locationId);
			log.debug("End Date : " + endDate);
			if (!(endDate.isEmpty() || endDate.equals(""))) {
				startDate = startDate.replace(">", "");
				startDate = startDate.replace("=", "");
				endDate = endDate.replace("<", "");
				endDate = endDate.replace("=", "");
				String[] startSplit = startDate.split("-");
				String[] endSplit = endDate.split("-");
				formatStartDate = startSplit[2] + "-" + startSplit[1] + "-" + startSplit[0];
				formatEndDate = endSplit[2] + "-" + endSplit[1] + "-" + endSplit[0];
				System.out.println("Start Date : " + formatStartDate);
				System.out.println("endDate Date : " + formatEndDate);
				log.debug("Start Date : " + startDate);
				log.debug("endDate Date : " + endDate);
			}
		} catch (Exception e) {
			log.debug("Error in Date :" + e);
			System.out.println("Error in Date :" + e);
		}

		float payTo = 0f;
		float collectFrom = 0f;
		// String retailer = "";
		try {
			List<FinanceSearchResults> list1 = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.searchFinance(retailerId, locationId, formatStartDate, formatEndDate, firstResult1, maxResults1);

			ResultCount resultCount = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getCountOfsearchFinance(retailerId, locationId, formatStartDate, formatEndDate);
			int totalRecords = Integer.valueOf(resultCount.getTotalRecords());
			try {
				// Code From Anand Sir
				Set<FinanceSearchResultSData> finSet = new HashSet<FinanceSearchResultSData>();
				for (FinanceSearchResults fsr1 : list1) { // first loop
					
					log.debug("fsr1 Amount"+fsr1.getCollectFrom());
					log.debug("fsr1.retailer :"+fsr1.getRetailerId());
					log.debug("fsr1 custLoc"+fsr1.getCustomerLocation());
					// create finResult and set other details in it
					FinanceSearchResultSData finResult = new FinanceSearchResultSData();
					finResult.setRetailer(fsr1.getRetailerId());
					// finResult.setModeOfPayment("CASH");
					finResult.setLocationId(fsr1.getCustomerLocation());
//					finResult.setData(list1);
//					finResult.setTotalRecords(totalRecords);
					for (FinanceSearchResults fsr : list1) { // second loop
//						try {
//							if (fsr.getAmount() != null && !(fsr.getAmount().isEmpty())) {
//								// check matching Retailer on Customer
//								// location(city) and get total amount
//								if (fsr1.getRetailerId().equalsIgnoreCase(fsr.getRetailerId())
//										&& fsr1.getCustomerLocation().equalsIgnoreCase(fsr.getCustomerLocation())) {
//
//									if (fsr.getPayTo().equalsIgnoreCase("BIZLOG")
//											&& fsr.getCollectFrom().equalsIgnoreCase("RETAILER")) {
//										collectFrom = Float.parseFloat(collectFrom + fsr.getAmount());
//										// retailer = fsr.getRetailerId();
//									} else if (fsr.getPayTo().equalsIgnoreCase("RETAILER")) {
//										payTo = Float.parseFloat(payTo + fsr.getAmount());
//									}
//								} // if
//							}
////						} catch (Exception e) {
//							e.getMessage();
//						}
					} // second

					// Set total in to finResult
					finResult.setCollectFrom(Float.parseFloat(fsr1.getCollectFrom()));
//					finResult.setPayTo(payTo);
					// add finResult to SET object to avoid duplicate
					finSet.add(finResult);
				} // first
				log.debug("finSet Value : "+finSet);
				restResult.setResponse(finSet);
				restResult.setMessage("Success");
				// Code End Here
			} catch (Exception e) {
				log.debug("Eror in for loop list :" + e);
			}

		} catch (Exception e) {
			log.debug("Error While Assigning " + e);
			System.out.println("Error While Assigning " + e);
			e.printStackTrace();
		}

		return restResult;

	}
	
	/**
	 * THIS API IS USED GET SINGLE FINANCE DETAILS
	 *  ON THE BASIS OF FINANCE SEARCH RESULT.
	 * */
	
	@RequestMapping(value = { "/search/single/finance/details" }, produces = "application/json")
	public FinanceSearchResultSData searchSingleFinanceDetails(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam("query") String queryJson, @RequestParam("startDate") String startDate,
			@RequestParam("EndDate") String endDate,@RequestParam("firstResult") String firstResult,
			@RequestParam("maxResults") String maxResults) {

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		FinanceSearchResultSData fsrd = new FinanceSearchResultSData();

		Map<String, String> queryObj = new HashMap<String, String>();

		log.debug("Query Json :" + queryJson);
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String retailerId = "";
		String locationId = "";
		String startDate1 = "";
		String endDate1 = "";
		String formatStartDate = "";
		String formatEndDate = "";
		String firstResult1 = "";
		String maxResults1 = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		if (queryObj.containsKey("retailerId")) {
			retailerId = queryObj.get("retailerId");
			log.debug("retailerId : " + retailerId);
		}
		if (queryObj.containsKey("locationId")) {
			locationId = queryObj.get("locationId");
		}
		if (queryObj.containsKey("startDate")) {
			startDate = queryObj.get("startDate");
			log.debug("startDate : " + startDate);
			System.out.println("startDate : " + startDate);
		}
		if (queryObj.containsKey("endDate")) {
			endDate = queryObj.get("endDate");
			log.debug("endDate : " + endDate);
		}
		try {
			log.debug("Start Date : " + locationId);
			log.debug("End Date : " + endDate);
			if (!(endDate.isEmpty() || endDate.equals(""))) {
				startDate = startDate.replace(">", "");
				startDate = startDate.replace("=", "");
				endDate = endDate.replace("<", "");
				endDate = endDate.replace("=", "");
				String[] startSplit = startDate.split("-");
				String[] endSplit = endDate.split("-");
				formatStartDate = startSplit[2] + "-" + startSplit[1] + "-" + startSplit[0];
				formatEndDate = endSplit[2] + "-" + endSplit[1] + "-" + endSplit[0];
				log.debug("Start Date : " + startDate);
				log.debug("endDate Date : " + endDate);
			}
		} catch (Exception e) {
			log.debug("Error in Date :" + e);
			System.out.println("Error in Date :" + e);
		}

		float payTo = 0f;
		float collectFrom = 0f;
		// String retailer = "";
		try {
			List<FinanceSearchResults> list1 = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.searchFinanceDetailsByRetailer(retailerId, locationId, formatStartDate, formatEndDate,firstResult1,maxResults1);

			ResultCount resultCount = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getCountOfsearchFinanceDetailsByRetailer(retailerId, locationId, formatStartDate, formatEndDate);
			int totalRecords = Integer.valueOf(resultCount.getTotalRecords());
			fsrd.setData(list1);
			fsrd.setTotalRecords(totalRecords);
			fsrd.setSuccess(true);

		} catch (Exception e) {
			fsrd.setSuccess(false);
			log.debug("Error While Assigning " + e);
			System.out.println("Error While Assigning " + e);
			e.printStackTrace();
		}

		return fsrd;

	}
}
