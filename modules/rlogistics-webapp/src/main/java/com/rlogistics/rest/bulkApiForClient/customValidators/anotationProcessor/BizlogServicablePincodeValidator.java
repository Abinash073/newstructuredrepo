package com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.BizlogServiceablePincode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class BizlogServicablePincodeValidator implements ConstraintValidator<BizlogServiceablePincode, String> {

    private MasterdataService masterdataService;

    @Autowired
    public BizlogServicablePincodeValidator(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    @Override
    public void initialize(BizlogServiceablePincode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return validate(value, context);
    }

    synchronized private boolean validate(String p, ConstraintValidatorContext context) {
        try {
            int a = (int) masterdataService.createLocationPincodesQuery().pincode(p).count();
            return a == 1;
        } catch (Exception e) {
            context.buildConstraintViolationWithTemplate("Server side error!");
            log.debug("Ex" + e);
            return false;
        }
    }
}
