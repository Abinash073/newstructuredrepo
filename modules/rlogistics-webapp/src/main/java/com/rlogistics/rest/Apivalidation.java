package com.rlogistics.rest;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerOdaLocations;
import com.rlogistics.master.identity.RetailerServiceableLocations;
import com.rlogistics.master.identity.RetailerServices;
import com.rlogistics.master.identity.ServiceMaster;
import com.rlogistics.master.identity.MasterdataService;

/**
 * Created By Adarsh Validation For Multiple Fields instantiate This Class To
 * Get Access For All The Methods
 */

public class Apivalidation {
	private Logger log = LoggerFactory.getLogger(Apivalidation.class);
	String errorResponse = "";
	RestExternalResult restResponse = new RestExternalResult();
	Map<String, String> fieldValues;
	String retailerId = "";
	MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
			.getMasterdataService();

	public void setValue(Map<String, String> fieldValues, String retailerId) {

		this.fieldValues = null;
		this.fieldValues = fieldValues;
		log.debug("***************************" + this.fieldValues.get("brand") + "*****************");
		this.retailerId = retailerId;
		log.debug(retailerId);

	}

	// Validate Pincode
	public RestExternalResult pincodeValidation() {
		// restResponse=null;

		log.debug(fieldValues.get("pincode"));
		if (fieldValues.containsKey("pincode")) {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
					.pincode(fieldValues.get("pincode")).singleResult();
			if (locationPincodes != null) {
				String rlLocationId = locationPincodes.getLocationId();
				// GET LOCATION NAME AND OTHER DETAILS
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
						.singleResult();
				if (processLocation == null) {
					log.debug("is not attached to any BizLog branches");
					errorResponse = errorResponse + fieldValues.get("pincode")
							+ " is not attached to any BizLog branches";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

			} else {
				log.debug("is not a valid Pincode or not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode")
						+ " is not a valid Pincode or not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		}
		if (retailerId != null) {

			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("pincode")).singleResult();
			log.debug("81");
			if (retailerServiceableLocations == null) {
				// Check if the pincode falls in the ODA Locations
				RetailerOdaLocations retailerOdaLocations = masterdataService.createRetailerOdaLocationsQuery()
						.retailerId(retailerId).pincode(fieldValues.get("pincode")).singleResult();
				log.debug("86");
				Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailerOdaLocations == null) {
					log.debug("is not a serviceable location of");
					String retailerName = retailer.getName();
					errorResponse = errorResponse + fieldValues.get("pincode") + " is not a serviceable location of "
							+ retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);

				}
			}
		}
		log.debug("***************" + restResponse);
		return restResponse;
	}

	// Validate Category
	public RestExternalResult catagoryValidation() {
		// restResponse=null;

		if (fieldValues.containsKey("productCategory")) {
			ProductCategory productCategory = masterdataService.createProductCategoryQuery()
					.name(fieldValues.get("productCategory")).singleResult();

			if (productCategory != null) {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategoryCode = productCategory.getCode();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

			} else {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			if (productCategory == null) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
				return restResponse;
			} else {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
					log.debug("is not a valid Category for this product.");
					errorResponse = errorResponse + fieldValues.get("productCategory")
							+ " is not a valid Category for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

			}
		}
		return restResponse;
	}
	// Validate Product

	public RestExternalResult productValidation() {

		String productCode = fieldValues.get("productCode").toUpperCase();
		String productName = fieldValues.get("productName").toUpperCase();
		Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
		String retailerName = retailer.getName();
		String rlProductSubCategoryId = "";
		String rlProductBrandId = "";

		Product product = masterdataService.createProductQuery().retailerId(retailerId)
				.model(fieldValues.get("model").toUpperCase()).name(fieldValues.get("productName").toUpperCase())
				.singleResult();
		if (product == null) {
			log.debug("is not a valid Product Code of the Retailer.");
			errorResponse = errorResponse + productName + " is not a valid Product Name of " + retailerName + ".";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		} else {
			rlProductSubCategoryId = product.getSubCategoryId();
			rlProductBrandId = product.getBrandId();

		}

		if ((product.getName().toUpperCase().equalsIgnoreCase(productName.toUpperCase())) == false) {
			// log.debug(product.getName() + productName);
			errorResponse = errorResponse + productCode + " is not a valid Product Name";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);

		}
		/**
		 * prem comment 08-2018 product code validation disabled as per madhu
		 * sir (see mail)
		 */
		// if((product.getCode().toUpperCase().equalsIgnoreCase(productCode.toUpperCase()))==false){
		// errorResponse = errorResponse + productCode + "Is not a valid code";
		// restResponse.setSuccess(false);
		// restResponse.setResponse(errorResponse);
		// }

		ProductSubCategory productSubCategory = masterdataService.createProductSubCategoryQuery()
				.id(rlProductSubCategoryId).singleResult();

		if (productSubCategory != null) {
			// String rlProductSubCategoryCode =
			// productSubCategory.getCode();
			String rlProductCategoryId = productSubCategory.getProductCategoryId();

			// Get the corresponding category name
			ProductCategory productCategory = masterdataService.createProductCategoryQuery().id(rlProductCategoryId)
					.singleResult();

			if (productCategory != null) {
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
					log.debug("is not a valid Category for this product.");
					errorResponse = errorResponse + fieldValues.get("productCategory")
							+ " is not a valid Category for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		}
		// VALIDATE BRAND VS PRODUCT BRAND
		// Get the Brand Name using the BrandId from Product
		Brand brand = masterdataService.createBrandQuery().id(rlProductBrandId).singleResult();
		if (brand != null) {
			// Get the brand name
			String rlProductBrand = brand.getName();
			String rlProductBrandCode = brand.getCode();
			if (!rlProductBrand.equalsIgnoreCase(fieldValues.get("brand"))) {
				log.debug("is not a valid Brand for this product");
				errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand for this product";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);

			}
		}
		return restResponse;
	}

	// Validate The Service type
	public RestExternalResult serviceTypeValidation() {
		// restResponse=null;
		String natureOfComplaint = fieldValues.get("natureOfComplaint");
		ServiceMaster serviceMaster = masterdataService.createServiceMasterQuery().name(natureOfComplaint)
				.singleResult();
		String rlServiceCode = "";
		String rlServiceId = "";
		if (serviceMaster != null) {
			rlServiceCode = serviceMaster.getCode();
			rlServiceId = serviceMaster.getId();
		} else {
			log.debug("is not a valid Service ");
			errorResponse = errorResponse + natureOfComplaint + " is not a valid Service.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);

		}

		//// SERVICE TYPE SHOULD BE AVAILED BY THE RETAILER
		RetailerServices retailerServices = masterdataService.createRetailerServicesQuery().serviceId(rlServiceId)
				.retailerId(retailerId).singleResult();

		if (retailerId != null) {
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			String retailerName = retailer.getName();
			if (retailerServices == null) {
				log.debug("is not a valid service of");
				errorResponse = errorResponse + natureOfComplaint + " is not a valid service of " + retailerName;
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);

			}
		}
		return restResponse;
	}

	public RestExternalResult dropPincodeValidation() {

		// restResponse=null;

		log.debug(fieldValues.get("dropLocPincode"));
		if (fieldValues.containsKey("dropLocPincode")) {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
					.pincode(fieldValues.get("dropLocPincode")).singleResult();
			if (locationPincodes != null) {
				String rlLocationId = locationPincodes.getLocationId();
				// GET LOCATION NAME AND OTHER DETAILS
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
						.singleResult();
				if (processLocation == null) {
					log.debug("is not attached to any BizLog branches");
					errorResponse = errorResponse + fieldValues.get("dropLocPincode")
							+ " is not attached to any BizLog branches";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

			} else {
				log.debug("is not a valid Pincode or not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("dropLocPincode")
						+ " is not a valid Pincode or not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		}
		if (retailerId != null) {

			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("dropLocPincode")).singleResult();
			log.debug("81");
			if (retailerServiceableLocations == null) {
				// Check if the pincode falls in the ODA Locations
				RetailerOdaLocations retailerOdaLocations = masterdataService.createRetailerOdaLocationsQuery()
						.retailerId(retailerId).pincode(fieldValues.get("dropLocPincode")).singleResult();
				log.debug("86");
				Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailerOdaLocations == null) {
					log.debug("is not a serviceable location of");
					String retailerName = retailer.getName();
					errorResponse = errorResponse + fieldValues.get("dropLocPincode")
							+ " is not a serviceable location of " + retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);

				}
			}
		}
		log.debug("***************" + restResponse);
		return restResponse;

	}
	
}