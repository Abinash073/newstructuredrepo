package com.rlogistics.rest;

import java.util.List;

/**
 * Created by prem for Image Links response to retailers
 * @author Bizlog
 *
 */
public class ImagesRestResponse {
	private Boolean success;
	private String message;
	private List<ImagesListResponse> response = null;

	public Boolean getSuccess() {
	return success;
	}

	public void setSuccess(Boolean success) {
	this.success = success;
	}

	public String getMessage() {
	return message;
	}

	public void setMessage(String message) {
	this.message = message;
	}

	public List<ImagesListResponse> getResponse() {
	return response;
	}

	public void setResponse(List<ImagesListResponse> response) {
	this.response = response;
	}
}
