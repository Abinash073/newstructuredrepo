package com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.RetailerId;
import com.rlogistics.rest.bulkApiForClient.exceptions.UserNotValidException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class RetailerIdValidator implements ConstraintValidator<RetailerId, String> {
    private MasterdataService masterdataService;

    @Autowired
    public RetailerIdValidator(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    @Override
    public void initialize(RetailerId constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isItValid(value);
    }


    private synchronized boolean isItValid(String value) {
        log.debug(value);
        int a = (int) masterdataService.createRetailerQuery().code(value).count();
        log.debug(value);
        if (a == 1) return true;
        else {
            log.debug("throwing Exception");
            throw new UserNotValidException("Retailer Id is invalid");
        }

    }


}
