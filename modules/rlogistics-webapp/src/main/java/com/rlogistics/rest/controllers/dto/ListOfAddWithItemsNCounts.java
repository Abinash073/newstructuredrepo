package com.rlogistics.rest.controllers.dto;

import com.rlogistics.rest.services.helper.DropLocationWithCountOfProdDTO;
import lombok.Builder;
import lombok.Data;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


@Data
@Builder
public class ListOfAddWithItemsNCounts {

    private List<DropLocationWithCountOfProdDTO> dropLocDetails = new ArrayList<>();
    @Builder.Default
    private String date = new LocalDateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Calcutta"))).toString();
    @Builder.Default
    private int count = 0;
    private String message;


}
