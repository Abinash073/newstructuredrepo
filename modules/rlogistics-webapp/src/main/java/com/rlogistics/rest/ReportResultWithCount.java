package com.rlogistics.rest;

import lombok.Data;

@Data
public class ReportResultWithCount {
    private Object data;
    private boolean success = false;
    private int totalCount;

}
