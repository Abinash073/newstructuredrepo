package com.rlogistics.rest.bulkApiForClient.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Valid
@SuppressWarnings("All")
public class FieldVal {
    @NotEmpty(message = "Pick-Up details cannot be Empty.")
    @JsonProperty("pickUpDetails")
    @Valid
    private PickUpDetails pickUpDetails;

    @NotEmpty(message = "items cannot be empty.")
    @JsonProperty("items")
    @NotNull(message = "Minimum 1 item is required!")
    @Valid
    private List<DropDetails> items;


}
