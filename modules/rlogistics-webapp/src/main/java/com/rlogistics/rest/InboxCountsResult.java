package com.rlogistics.rest;

public class InboxCountsResult {

	private long numInvolved;
	private long numAssigned;

	public long getNumInvolved() {
		return numInvolved;
	}

	public void setNumInvolved(long numInvolved) {
		this.numInvolved = numInvolved;
	}

	public long getNumAssigned() {
		return numAssigned;
	}

	public void setNumAssigned(long numAssigned) {
		this.numAssigned = numAssigned;
	}
}
