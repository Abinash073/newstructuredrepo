package com.rlogistics.rest.franchise;

public interface FranchiseUser {
    String getEmail();

    String getName();

    String getId();

    String getRoleCode();

    String getContactNumber();

    String getFranchiseLocation();

    String getAgencyName();
}