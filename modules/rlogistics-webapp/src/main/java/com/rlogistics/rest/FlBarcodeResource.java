package com.rlogistics.rest;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.FlBarcodeEntity;
import com.rlogistics.master.identity.FlBarcodeOffset;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.cmd.CreateFlBarcodeEntityQueryCmd;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class FlBarcodeResource extends RLogisticsResource {
    private static Logger log = LoggerFactory.getLogger(FlBarcodeResource.class);

    @GetMapping("/abinash")
    public String welcome() {
        return "Hii Abinash";
    }

    @RequestMapping(value = "/flbarcode/generate", method = RequestMethod.POST)
    public RestResponse generateBarcode(@RequestParam(value = "noOfBarcodes") String noOfBarcodes, HttpServletResponse response) throws Exception {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();

//        log.debug("ssssssssssssssss " + noOfBarcodes);
        int number=0;
        try{
            number = Integer.parseInt(noOfBarcodes);
//            log.debug("llllllllllllllllllll " + number);
        } catch (NumberFormatException exception){
           number = Integer.parseInt( noOfBarcodes.replace("\"", ""));
//            log.debug("22222222222222222222 " + number);
        }
//        int number = Integer.parseInt(noOfBarcodes);
//        log.debug("33333333333333333 " + number);

        FlBarcodeOffset.FlBarcodeOffsetQuery flBarcodeOffsetQuery = masterdataService.createFlBarcodeOffsetQuery();
        FlBarcodeOffset flBarcodeOffset = flBarcodeOffsetQuery.singleResult();
        if (flBarcodeOffset == null) {
            String first = "1000001";
            flBarcodeOffset = masterdataService.newFlBarcodeOffset();
            flBarcodeOffset.setFlBarcodeOffset(Integer.valueOf(first));

            masterdataService.saveFlBarcodeOffset(flBarcodeOffset);
        }

        int offset = flBarcodeOffset.getFlBarcodeOffset();
//        String startingFlBarcode = String.valueOf(offset);
        String endingFlBarcode = "";
        try {
            int status = 0;


//            FlBarcodeEntity generation = masterdataService.newFlBarcodeEntity();
//            generation.setItemCount(number);

            Date date = new Date();
            DateFormat dateStr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String s = dateStr.format(date);

            for (int i = 0; i < number; i++) {
                FlBarcodeEntity generation = masterdataService.newFlBarcodeEntity();

                generation.setFlBarcodeStatus(status);
                generation.setFlBarcode("F20" + String.valueOf(offset));

//                generation.setCreatedOn(String.valueOf(new java.sql.Timestamp(new java.util.Date().getTime())));
                generation.setCreatedOn(s);

                log.debug(generation.getClass().toString());

                masterdataService.saveFlBarcodeEntity(generation);

                if (i == number - 1) {
                    endingFlBarcode = String.valueOf(offset);
                }
                offset++;
            }

            //update barcode offset
            flBarcodeOffset.setFlBarcodeOffset(Integer.valueOf(endingFlBarcode) + 1);
            masterdataService.saveFlBarcodeOffset(flBarcodeOffset);

            restResponse.setMessage("Barcode generated successfully");
            restResponse.setSuccess(true);
        } catch (Exception e) {
            if (e.getMessage().contains("Duplicate")) {
                restResponse.setMessage("Failed to add : Duplicate record");
            } else {
                restResponse.setMessage(e.getMessage());
            }
            response.setStatus(500);
        }
        return restResponse;
    }


    @GetMapping(value = "/get-barcode", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse getAvailableFlBarcode() {

        // 0 for available bar-codes
        int status = 0;

//        getBarcodes(status);

//        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//                .getDefaultProcessEngine())).getMasterdataService();
//        RestResponse restResponse = new RestResponse();
//        int barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(0).count());
//        List<FlBarcodeEntity> flBarcode = null;
////        List<FlBarcodeResponse> list = new ArrayList<>();
//        if (barcodeCount == 0) {
//            restResponse.setMessage("No Bar-codes Available");
//            restResponse.setSuccess(true);
//        } else {
//            restResponse.setMessage("Displaying Bar-codes");
//            restResponse.setSuccess(true);
//
//            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
//                    .getProcessEngineConfiguration())).getCommandExecutor();
//            FlBarcodeEntity.FlBarcodeEntityQuery flBarcodeQuery = commandExecutor.execute(new CreateFlBarcodeEntityQueryCmd());
//            flBarcode = flBarcodeQuery.flBarcodeStatus(0).list();
//
//            for (FlBarcodeEntity barcode : flBarcode) {
//
//                log.debug("===================================" + barcode.getFlBarcode());
//                FlBarcodeResponse flBarcodeResponse = new FlBarcodeResponse();
//                flBarcodeResponse.setId(barcode.getId());
//                flBarcodeResponse.setFlBarcode(barcode.getFlBarcode());
//                flBarcodeResponse.setStatus(barcode.getFlBarcodeStatus());
//
////                list.add(flBarcodeResponse);
//
//
//                log.debug("----------------------");
//            }
//
//        }
//        restResponse.setResponse(flBarcode);
        return getBarcodes(status);
    }

    @GetMapping(value = "/get-agency-barcode")
    public RestResponse getAgencyAvailableBarcode(@RequestParam(value = "franchiseName") String franchiseName,
                                                  @RequestParam(value = "agencyName") String agencyName) {

        String franchise = (franchiseName != null) ? franchiseName : "";
        String agency = (agencyName != null) ? agencyName : "";
        int status = 1;  // status 1 means it available in agency


//        RestResponse restResponse = new RestResponse();
        return getAgencyBarcode(status, franchise, agency);
    }

    private RestResponse getAgencyBarcode(int status, String franchiseName, String agencyName) {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();
        int barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(status)
                .franchiseName(franchiseName).agencyName(agencyName).count());
        List<FlBarcodeEntity> flBarcode = null;
        if (barcodeCount == 0) {
            restResponse.setMessage("No Bar-codes Available");
            restResponse.setSuccess(true);
        } else {


            List<FlBarcodeEntity> flBarcodeEntityList = new ArrayList<>();
            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
                    .getProcessEngineConfiguration())).getCommandExecutor();
            FlBarcodeEntity.FlBarcodeEntityQuery flBarcodeQuery = commandExecutor.execute(new CreateFlBarcodeEntityQueryCmd());
            flBarcode = flBarcodeQuery.flBarcodeStatus(status).franchiseName(franchiseName).agencyName(agencyName).list();

            restResponse.setMessage("Displaying Bar-codes");
            restResponse.setSuccess(true);

        }
        restResponse.setResponse(flBarcode);
        return restResponse;

    }


    @PutMapping("/flbarcode-change-status")
    public RestResponse changeBarcodeStatus(@RequestParam(value = "flBarcode") String flBarcode, HttpServletRequest request, HttpServletResponse response) throws Exception {

//        if (!beforeMethodInvocation(request, response)) {
//            throw new Exception("rltoken null or invalid");
//        }
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();
        String message;

        try {
            int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).count());

            if (count == 1) {
                FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).singleResult();

                flBarcodeEntity.setFlBarcodeStatus(1);
                masterdataService.saveFlBarcodeEntity(flBarcodeEntity);
                message = "Barcode Assigned";
            } else {
                message = "Some issue with barcode, choose a different one";
            }
            restResponse.setSuccess(true);
            restResponse.setMessage(message);

            return restResponse;
        } catch (Exception e) {
            restResponse.setMessage("Issue with barcode");
            restResponse.setSuccess(false);
            return restResponse;
        }

    }

    @PutMapping("/assign-barcode")
    public RestResponse assignBarcode(@RequestParam(value = "assignedBy") @NotNull String userId,
                                      @RequestParam(value = "assignedTo") @NotNull String feId,
                                      @RequestParam(value = "flBarcode") @NotNull String flBarcode,
                                      HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (!beforeMethodInvocation(request, response)) {
            throw new Exception("rltoken null or invalid");
        }

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();
        String message;

        try {
            int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).flBarcodeStatus(0).count());

            if (count == 1) {
                FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).singleResult();

                flBarcodeEntity.setFlBarcodeStatus(1);
                flBarcodeEntity.setAssignedToFeBy(userId);
//                flBarcodeEntity.setAssignedBy(userId);
                flBarcodeEntity.setAssignedFe(feId);
                masterdataService.saveFlBarcodeEntity(flBarcodeEntity);
                message = "Barcode Assigned to " + feId;
            } else {
                message = "Some issue with barcode, choose a different one";
            }
            restResponse.setSuccess(true);
            restResponse.setMessage(message);

            return restResponse;
        } catch (Exception e) {
            restResponse.setMessage("Issue with barcode");
            restResponse.setSuccess(false);
            return restResponse;
        }
    }

    @GetMapping(value = "/get/assigned-barcodes", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse getAssignedFlBarcodes() throws Exception {

        // 1 for assigned bar-codes
        int status = 1;
//        getBarcodes(status);
//        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//                .getDefaultProcessEngine())).getMasterdataService();
//        RestResponse restResponse = new RestResponse();
//        int barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(1).count());
//        List<FlBarcodeEntity> flBarcode = null;
////        List<FlBarcodeResponse> list = new ArrayList<>();
//        if (barcodeCount == 0) {
//            restResponse.setMessage("No Assigned Bar-codes Available");
//            restResponse.setSuccess(true);
//        } else {
//            restResponse.setMessage("Displaying Assigned Bar-codes");
//            restResponse.setSuccess(true);
//
//            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
//                    .getProcessEngineConfiguration())).getCommandExecutor();
//            FlBarcodeEntity.FlBarcodeEntityQuery flBarcodeQuery = commandExecutor.execute(new CreateFlBarcodeEntityQueryCmd());
//            flBarcode = flBarcodeQuery.flBarcodeStatus(0).list();
//
//            for (FlBarcodeEntity barcode : flBarcode) {
//                FlBarcodeResponse flBarcodeResponse = new FlBarcodeResponse();
//                flBarcodeResponse.setId(barcode.getId());
//                flBarcodeResponse.setFlBarcode(barcode.getFlBarcode());
//                flBarcodeResponse.setStatus(barcode.getFlBarcodeStatus());
//
//                log.debug("----------------------");
//            }
//
//        }
        return getBarcodes(status);
    }

    private RestResponse getBarcodes(int status) {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();
        int barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(status).count());
        List<FlBarcodeEntity> flBarcode = null;
        if (barcodeCount == 0) {
            restResponse.setMessage("No Bar-codes Available");
            restResponse.setSuccess(true);
        } else {
            restResponse.setMessage("Displaying Bar-codes");
            restResponse.setSuccess(true);

            CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
                    .getProcessEngineConfiguration())).getCommandExecutor();
            FlBarcodeEntity.FlBarcodeEntityQuery flBarcodeQuery = commandExecutor.execute(new CreateFlBarcodeEntityQueryCmd());
            flBarcode = flBarcodeQuery.flBarcodeStatus(0).list();

        }
        restResponse.setResponse(flBarcode);
        return restResponse;

    }

    @PostMapping("assign-barcode/to/loc")
    public RestResponse assignBarcodeToLoc(@RequestParam(value = "hoCoId") @NotNull String hoCoId,
                                           @RequestParam(value = "franchiseName") @NotNull String franchiseName,
                                           @RequestParam(value = "agencyName") @NotNull String agencyName,
                                           @RequestParam(value = "flBarcodeList") @NotNull List<String> flBarcodeList,
                                           HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (!beforeMethodInvocation(request, response)) {
            throw new Exception("rltoken null or invalid");
        }

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();
        String message = "";
        List<String> faultyBarcodes = new ArrayList<>();


        for (String aFlBarcodeList : flBarcodeList) {

            int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(aFlBarcodeList).flBarcodeStatus(0).count());
            if (count == 1) {
                FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(aFlBarcodeList).singleResult();
                flBarcodeEntity.setFlBarcodeStatus(1);
                flBarcodeEntity.setAssignedToLocBy(hoCoId);
                flBarcodeEntity.setFranchiseName(franchiseName);
                flBarcodeEntity.setAgencyName(agencyName);

                masterdataService.saveFlBarcodeEntity(flBarcodeEntity);
                message = "Barcode Assigned";

            } else {
                faultyBarcodes.add(aFlBarcodeList);
                message = "Some issue with these Barcode";
                restResponse.setResponse(faultyBarcodes);
            }

        }
        restResponse.setMessage(message);
        restResponse.setSuccess(true);
        return restResponse;
    }

//    @PostMapping("assign-barcode/to/fe")
//    public RestResponse assignBarcodeToFe(@RequestParam(value = "coId") @NotNull String coId,
//                                          @RequestParam(value = "feName") @NotNull String feName,
//                                          @RequestParam(value = "flBarcode") @NotNull String flBarcode,
//                                          HttpServletRequest request, HttpServletResponse response) throws Exception {
//        if (!beforeMethodInvocation(request, response)) {
//            throw new Exception("rltoken null or invalid");
//        }
//
//        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//                .getDefaultProcessEngine())).getMasterdataService();
//        RestResponse restResponse = new RestResponse();
//        String message = "";
//        List<String> faultyBarcodes = new ArrayList<>();
//
//
//        int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).flBarcodeStatus(1).count());
//        if (count == 1) {
//            FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(flBarcode).singleResult();
//            flBarcodeEntity.setFlBarcodeStatus(2);
//            flBarcodeEntity.setAssignedToFeBy(coId);
//            flBarcodeEntity.setAssignedFe(feName);
//
//            masterdataService.saveFlBarcodeEntity(flBarcodeEntity);
//            message = "Barcode Assigned to " + feName;
//
//        } else {
//            faultyBarcodes.add(flBarcode);
//            message = "Some issue with these Barcode, Select a different one";
//            restResponse.setResponse(faultyBarcodes);
//        }
//
//        restResponse.setMessage(message);
//        restResponse.setSuccess(true);
//        return restResponse;
//    }


    @GetMapping("/get-barcode1")
    public RestResponse getBarcode(@RequestParam(value = "status") int status,
                                   @RequestParam(value = "franchiseName", required = false) String franchiseName,
                                   @RequestParam(value = "agencyName", required = false) String agencyName,
                                   HttpServletRequest request, HttpServletResponse response) throws Exception {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        RestResponse restResponse = new RestResponse();

        try {

            int barcodeCount;
            if (franchiseName != null && agencyName != null) {
                barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(status)
                        .franchiseName(franchiseName).agencyName(agencyName).count());
            } else {
                barcodeCount = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcodeStatus(status).count());
            }

            List<FlBarcodeEntity> flBarcode;
            if (barcodeCount == 0) {
                restResponse.setMessage("No Bar-codes Available");
                restResponse.setSuccess(true);
            } else {
                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine()
                        .getProcessEngineConfiguration())).getCommandExecutor();
                FlBarcodeEntity.FlBarcodeEntityQuery flBarcodeQuery = commandExecutor.execute(new CreateFlBarcodeEntityQueryCmd());
                flBarcode = flBarcodeQuery.flBarcodeStatus(status).agencyName(agencyName).franchiseName(franchiseName).list();

                restResponse.setMessage("Displaying Bar-codes");
                restResponse.setSuccess(true);
                restResponse.setResponse(flBarcode);

            }
        } catch (Exception e) {
            log.debug(e.getMessage());
            e.printStackTrace();
        }

        return restResponse;
    }


    // Activiti method call to assign Barcode to FE
    public String assignBarcodeToFe(String barcode, String coId, String feName){
        String message="";
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(barcode).flBarcodeStatus(1).count());
        if (count == 1) {
            FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(barcode).singleResult();
            flBarcodeEntity.setFlBarcodeStatus(2);
            flBarcodeEntity.setAssignedToFeBy(coId);
            flBarcodeEntity.setAssignedFe(feName);

            masterdataService.saveFlBarcodeEntity(flBarcodeEntity);
            message = "Barcode Assigned to " + feName;

        } else {

            message = "Some issue with the Barcode, Choose a different one";

        }
        return message;
    }

    // Activiti call when barcode is used by Franchise Fe.
    public void barcodeUsed(String barcode){

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getMasterdataService();
        int count = Math.toIntExact(masterdataService.createFlBarcodeEntityQuery().flBarcode(barcode).flBarcodeStatus(2).count());
        if (count == 1) {
            FlBarcodeEntity flBarcodeEntity = masterdataService.createFlBarcodeEntityQuery().flBarcode(barcode).singleResult();
            flBarcodeEntity.setFlBarcodeStatus(3);
            masterdataService.saveFlBarcodeEntity(flBarcodeEntity);

        }
    }
}
