package com.rlogistics.rest;

import java.util.LinkedHashMap;

public class FeTrackingReportHeader {
    private LinkedHashMap<String, String> header = new LinkedHashMap<>();

    {
        addHeader();
    }

    void addHeader() {
        header.put("S No", "sno");
        header.put("Field Engg. Name", "feName");
        header.put("Location", "location");
        header.put("Total Distance", "totalDistance");
    }

    public LinkedHashMap<String, String> getHeader() {
        return header;
    }
}
