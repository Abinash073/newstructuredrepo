package com.rlogistics.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.*;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;
import com.rlogistics.ui.CSVDataProcessor;

import pojo.ReportPojo;

import org.activiti.engine.*;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.DateTime;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * APIS
 *
 * @author r ticket/create ticket/create/repair 
 *         ticket/create/buyback ticket/create/advanceexchange
 */
@Service
@RestController
public class ExcelProcessResource extends RLogisticsResource {

	// @Autowired
	// ReportingController reportingController;
	// @Autowired
	// MasterdataService masterdataService;
	private static Logger log = LoggerFactory.getLogger(ExcelProcessResource.class);
	int finalLimit = 10;

	public ProcessInstance submitStartForm(FormService formService, ProcessDefinition definition,
			FormFillerUtil formFillerUtil) {

		ProcessInstance processInstance = (ProcessInstance) formFillerUtil
				.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
					@Override
					public ProcessInstance submitForm(Map<String, String> fieldValues) {
						return formService.submitStartFormData(definition.getId(), fieldValues);
					}
				});

		/*
		 * TODO: Leads to bugs -- hence disabling. But will need to be fixed
		 * when people start using start forms elaborately CommandExecutor
		 * commandExecutor =
		 * ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.
		 * getDefaultProcessEngine().getProcessEngineConfiguration())).
		 * getCommandExecutor(); commandExecutor.execute(new Command<Boolean>()
		 * {
		 *
		 * @Override public Boolean execute(CommandContext commandContext) {
		 * execution.setVariables(formFillerUtil.getVariables()); return true; }
		 * });
		 */
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		ReportingController reportingController = new ReportingController();
		try {

			ProcessLocation pincode = masterdataService.createProcessLocationQuery()
					.id(masterdataService.createLocationPincodesQuery()
							.pincode(formFillerUtil.getVariables().get("pincode").toString()).singleResult()
							.getLocationId())
					.singleResult();
			log.debug(pincode.getCity());
			// reportingController.addReportDetails(pincode.getCity(), "add",
			// formFillerUtil.getVariables().get("retailer").toString());
		} catch (Exception e) {
			log.debug("ERROR---------------------" + e);
		}
		return processInstance;
	}

	public String submitStartFormNew(FormService formService, ProcessDefinition definition,
			FormFillerUtil formFillerUtil) {
		ProcessInstance processInstance = (ProcessInstance) formFillerUtil
				.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
					@Override
					public ProcessInstance submitForm(Map<String, String> fieldValues) {
						return formService.submitStartFormData(definition.getId(), fieldValues);
					}
				});

		/*
		 * TODO: Leads to bugs -- hence disabling. But will need to be fixed
		 * when people start using start forms elaborately CommandExecutor
		 * commandExecutor =
		 * ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.
		 * getDefaultProcessEngine().getProcessEngineConfiguration())).
		 * getCommandExecutor(); commandExecutor.execute(new Command<Boolean>()
		 * {
		 *
		 * @Override public Boolean execute(CommandContext commandContext) {
		 * execution.setVariables(formFillerUtil.getVariables()); return true; }
		 * });
		 */
		return processInstance.getId();
	}

	// Common For All The Tickets
	@RequestMapping(value = "/ticket/create", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicket(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		// String header = httpRequest.getHeader("Authorization");
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();
		try {
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					throw new Exception("Invalid login");
				}
				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

//				Timestamp currentDate = new Timestamp(c.getTime().getTime());
//				if (currentDate.after(retailer.getTokenExpiryDate())) {
//					restResult.setSuccess(false);
//					throw new Exception("Authentication expired. Please login again");
//				}
			}
			try {
				restResult = validatingFormData(queryJSON, retailer);
			} catch (Exception e) {
				log.debug("Not Able To Validate Ticket Data : " + e);
			}
			if (restResult.success) {
				restResult = ticketCreation(queryJSON, retailerId, apiToken);
//				if (retailer.getApiUsername() != null) {
//					String rawToken = retailer.getApiUsername() + "___"
//							+ RandomStringUtils.randomAlphanumeric(8).toLowerCase()
//							+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
//					apiToken = AuthUtil.cryptWithMD5(rawToken);
//					retailer.setApiToken(apiToken);
//					retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
//				} else {
//					String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
//							+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
//					apiToken = AuthUtil.cryptWithMD5(rawToken);
//					retailer.setApiToken(apiToken);
//					retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
//				}
				try {
					masterdataService.saveRetailer(retailer);
//					restResult.setApiToken(apiToken);
				} catch (Exception e) {
				}
			} else {
				return restResult;
			}
		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		return restResult;
	}

	@RequestMapping(value = "/ticket/create/repair", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketRepair(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {

		// String header = httpRequest.getHeader("Authorization");
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();
		Map<String, String> fieldValues = null;
		String jsonTicket = "";
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			fieldValues.put("natureOfComplaint", "Repair");
		} catch (Exception e) {
			log.debug("Error in Conversion :" + e);
		}
		try {
			jsonTicket = JSONValue.toJSONString(fieldValues);
		} catch (Exception e) {
			log.debug("Error in Conversoion from MAp to String :" + e);
		}
		try {
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					throw new Exception("Invalid login");
				}
				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

				Timestamp currentDate = new Timestamp(c.getTime().getTime());
				if (currentDate.after(retailer.getTokenExpiryDate())) {
					restResult.setSuccess(false);
					throw new Exception("Authentication expired. Please login again");
				}
				/**
				 * prem code start 20-06-2018 added ->addMobileProduct() for
				 * enable validation for table RetailerOther
				 *
				 */
				try {
					RetailerOther retailerOther = masterdataService.createRetailerOtherQuery().retailerId(retailerId)
							.singleResult();
					if (retailerOther.getIsMobileProductAddAllow() == 1) {
						/**
						 * passing value for fields which we get from retailer
						 */
						RestApi restApi = new RestApi();

						restApi.addMobileProduct(queryJSON, masterdataService, retailerId, fieldValues);

					} else {
						log.info("This Add Mobile Product is not valid for this Retailer");
					}
				} catch (Exception e) {
					log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

				}

				/**
				 * prem code end
				 */
			}
			try {
				restResult = commonValidationFormData(queryJSON, retailer, "Repair");
			} catch (Exception e) {
				log.debug("Not Able To Validate Ticket Data : " + e);
			}
			if (restResult.success) {
				RestExternalResult restResult1 = null;
				try {
					restResult1 = validatingRepair(queryJSON, retailer);
				} catch (Exception e) {
					log.debug("Not Able To Validate Ticket Data on the Basis Of NOC : " + e);
				}
				if (restResult1.success) {
					restResult = ticketCreation(jsonTicket, retailerId, apiToken);
					if (retailer.getApiUsername() != null) {
						String rawToken = retailer.getApiUsername() + "___"
								+ RandomStringUtils.randomAlphanumeric(8).toLowerCase()
								+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
						apiToken = AuthUtil.cryptWithMD5(rawToken);
						retailer.setApiToken(apiToken);
						retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
					} else {
						String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
								+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
						apiToken = AuthUtil.cryptWithMD5(rawToken);
						retailer.setApiToken(apiToken);
						retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
					}
					try {
						masterdataService.saveRetailer(retailer);
						restResult.setApiToken(apiToken);
					} catch (Exception e) {
					}
				} else {
					return restResult1;
				}
			} else {
				return restResult;
			}
		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		return restResult;
	}
	
	/**
	 * created by prem 10-08-2018
	 *
	 * @param httpRequest
	 * @param httpResponse
	 * @param file
	 * @return this aoi is for upload bulk additional sheet which will create
	 *         tickets tickets will create in group wise like : Brand , Retailer
	 *         etc
	 */
	@RequestMapping(value = "/bulk/additional-sheet/upload", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult uploadBrandCommandsExcel(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "file") MultipartFile file) {
		RestExternalResult externalResult = new RestExternalResult();

		try {

			// Creating a Workbook from an Excel file (.xls or .xlsx)
			Workbook workbook = WorkbookFactory.create(file.getInputStream());

			ArrayList<ModelTicketDetails> arrayList = new ArrayList<>();

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			for (Row row : sheet) {
				for (Cell cell : row) {
					String cellValue = dataFormatter.formatCellValue(cell);
					// System.out.print(cellValue + " ");
				}
				// System.out.println();
			}

			int totalRowCount = sheet.getLastRowNum();
			Row firstRow = sheet.getRow(0);
			int recordNo = 1;
			List<String> orderedFields = new ArrayList<String>();

			for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
				orderedFields.add(firstRow.getCell(fr).getStringCellValue());
			}

			for (int r = 1; r <= totalRowCount; r++) {

				recordNo++;

				if (sheet.getRow(r) != null) {

					Row row = sheet.getRow(r);
					Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
					String record = "";

					Cell cell = null;
					for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
						if (row.getCell(i) != null) {
							cell = row.getCell(i);
							fieldValuesPreFilter.put(orderedFields.get(i), dataFormatter.formatCellValue(cell));
						}
					}

					ModelTicketDetails detailer = null;
					for (int i = 0; i < fieldValuesPreFilter.size(); i++) {
						// System.out.println(w + "-" +
						// fieldValuesPreFilter.get(w));
						// System.out.println(
						// fieldValuesPreFilter.get(w));//values
						// System.out.println(w );//keys
						detailer = new ModelTicketDetails();

						detailer.setConsumerName(fieldValuesPreFilter.get("consumerName"));
						detailer.setConsumerComplaintNumber(fieldValuesPreFilter.get("consumerComplaintNumber"));
						detailer.setAddressLine1(fieldValuesPreFilter.get("addressLine1"));
						detailer.setAddressLine2(fieldValuesPreFilter.get("addressLine2"));

						detailer.setCity(fieldValuesPreFilter.get("city"));
						detailer.setPincode(fieldValuesPreFilter.get("pincode"));
						detailer.setLandmark(fieldValuesPreFilter.get("landmark"));
						detailer.setTelephoneNumber(fieldValuesPreFilter.get("telephoneNumber"));
						detailer.setRetailerPhoneNo(fieldValuesPreFilter.get("retailerPhoneNo"));
						detailer.setAlternateTelephoneNumber(fieldValuesPreFilter.get("alternateTelephoneNumber"));
						detailer.setEmailId(fieldValuesPreFilter.get("emailId"));
						detailer.setAlternateEmail(fieldValuesPreFilter.get("alternateEmail"));
						detailer.setOrderNumber(fieldValuesPreFilter.get("orderNumber"));
						detailer.setInvoiceNo(fieldValuesPreFilter.get("invoiceNo"));
						detailer.setDateOfPurchase(fieldValuesPreFilter.get("dateOfPurchase"));
						detailer.setDateOfComplaint(fieldValuesPreFilter.get("dateOfComplaint"));
						detailer.setLevelOfIrritation(fieldValuesPreFilter.get("levelOfIrritation"));
						detailer.setNatureOfComplaint(fieldValuesPreFilter.get("natureOfComplaint"));
						detailer.setIsUnderWarranty(fieldValuesPreFilter.get("isUnderWarranty"));
						detailer.setBrand(fieldValuesPreFilter.get("brand"));
						detailer.setProductCategory(fieldValuesPreFilter.get("productCategory"));
						detailer.setProductName(fieldValuesPreFilter.get("productName"));
						detailer.setProductCode(fieldValuesPreFilter.get("productCode"));
						detailer.setModel(fieldValuesPreFilter.get("model"));
						detailer.setProductDescription(fieldValuesPreFilter.get("productDescription"));
						detailer.setProblemDescription(fieldValuesPreFilter.get("problemDescription"));
						detailer.setIdentificationNo(fieldValuesPreFilter.get("identificationNo"));
						detailer.setDropLocation(fieldValuesPreFilter.get("dropLocation"));
						detailer.setDropLocAddress1(fieldValuesPreFilter.get("dropLocAddress1"));
						detailer.setDropLocAddress2(fieldValuesPreFilter.get("dropLocAddress2"));
						detailer.setDropLocCity(fieldValuesPreFilter.get("dropLocCity"));
						detailer.setDropLocState(fieldValuesPreFilter.get("dropLocState"));
						detailer.setDropLocPincode(fieldValuesPreFilter.get("dropLocPincode"));
						detailer.setDropLocContactPerson(fieldValuesPreFilter.get("dropLocContactPerson"));
						detailer.setDropLocContactNo(fieldValuesPreFilter.get("dropLocContactNo"));
						detailer.setDropLocAlternateNo(fieldValuesPreFilter.get("dropLocAlternateNo"));
						detailer.setPhysicalEvaluation(fieldValuesPreFilter.get("physicalEvaluation"));
						detailer.setBulkMode(fieldValuesPreFilter.get("bulkMode"));
						detailer.setTechEvalRequired(fieldValuesPreFilter.get("TechEvalRequired"));

					}
					arrayList.add(detailer);

				}
			}

			// arrayList.add(new ModelTicketDetails("prem1", "apple", "apple"));

			org.json.JSONObject jsonObject = new org.json.JSONObject();
			Set<String> brandSet = new HashSet<String>();
			for (ModelTicketDetails g : arrayList) {
				brandSet.add(g.getBrand());
			}
			for (String s : brandSet) {
				org.json.JSONArray arrayList1 = new org.json.JSONArray();

				for (ModelTicketDetails g : arrayList) {
					if (s.equals(g.getBrand())) {

						ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
						String json = ow.writeValueAsString(g);
						org.json.JSONObject jsonObject2 = new org.json.JSONObject(json);
						arrayList1.put(jsonObject2);
					}
				}
				try {
					jsonObject.put(s, arrayList1);
					createTicket(s, arrayList1);
				} catch (org.json.JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// System.out.println(jsonObject.toString());

		} catch (Exception ex) {
		}
		return externalResult;
	}

	private void createTicket(String s, org.json.JSONArray arrayList1) throws org.json.JSONException {

		int g = getIndex(arrayList1);
		org.json.JSONArray array = new org.json.JSONArray();
		ArrayList<org.json.JSONObject> arrayList = new ArrayList<>();
		for (int i = 0; i < arrayList1.length(); i++) {
			arrayList.add(arrayList1.getJSONObject(i));
		}
		// System.out.println(g);

		int lee = arrayList1.length();
		for (int i = 0; i < g; i++) {
			List<org.json.JSONObject> sublist;
			List<org.json.JSONObject> sublist1;
			int hh = i;

			if (lee <= finalLimit) {
				sublist = arrayList.subList(hh, lee);
				org.json.JSONArray array2 = new org.json.JSONArray(sublist);
				finalTicketCreation(s, array2);
				arrayList.removeAll(sublist);
				lee = lee - finalLimit;

			} else {
				sublist1 = arrayList.subList(hh, finalLimit);
				org.json.JSONArray array2 = new org.json.JSONArray(sublist1);
				finalTicketCreation(s, array2);
				arrayList.removeAll(sublist1);
				lee = lee - finalLimit;
			}
			hh = i + finalLimit;

		}
	}

	private int getIndex(org.json.JSONArray array) {
		int xx = array.length();
		int limit = finalLimit;
		double w = (double) xx / limit;
		String[] arr = String.valueOf(w).split("\\.");
		int[] intArr = new int[2];
		intArr[0] = Integer.parseInt(arr[0]); // 1
		intArr[1] = Integer.parseInt(arr[1]); // 9
		// System.out.println("1st index: "+intArr[0]+" ,2nd index:
		// "+intArr[1]);

		if (intArr[1] == 0) {

		} else {
			intArr[0]++;
		}
		// System.out.println("1st index: "+intArr[0]+" ,2nd index:
		// "+intArr[1]);
		return intArr[0];
	}

	private void finalTicketCreation(String s, org.json.JSONArray sublist) throws org.json.JSONException {
		// TODO Auto-generated method stub
		System.out.println("final tick " + s + " " + sublist.toString());
		log.debug("final tick " + s + " " + sublist.toString());
		org.json.JSONObject queryJSON = new org.json.JSONObject(sublist.get(0).toString());
		RestExternalResult restResult = ticketCreation(queryJSON.toString(), "5064", "");
		// RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
		// (ProcessEngines.getDefaultProcessEngine()))
		// .getRuntimeService();
		// String ticketNo =
		// String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
		// log.debug(restResult.toString());

	}

//	@RequestMapping(value = "/ticket/create/buyback", method = RequestMethod.POST, produces = "application/json")
//	public RestExternalResult createTicketBuyBack(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
//			@RequestParam(required = true, value = "fields") String queryJSON,
//			@RequestParam(value = "retailerId") String retailerId,
//			@RequestParam(value = "apiToken", required = true) String apiToken) {
//		// String header = httpRequest.getHeader("Authorization");
//
//		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
//				.getMasterdataService();
//		RestExternalResult restResult = new RestExternalResult();
//		Retailer retailer = null;
//		Calendar c = Calendar.getInstance();
//		Map<String, String> fieldValues = null;
//		String jsonTicket = "";
//		try {
//			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
//			fieldValues.put("natureOfComplaint", "Buy Back");
//		} catch (Exception e) {
//			log.debug("Error in Conversion :" + e);
//		}
//		try {
//			/**
//			 * adding random product code while adding new product
//			 */
//			if (fieldValues.get("productCategory").equalsIgnoreCase("mobile")) {
//				fieldValues.put("productCode", randomAlphaNumeric(20));
//			} else {
//				// product is not mobile catagory
//			}
//
//			jsonTicket = JSONValue.toJSONString(fieldValues);
//		} catch (Exception e) {
//			log.debug("Error in Conversoion from MAp to String :" + e);
//		}
//		try {
//			if (retailerId != null) {
//				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
//				if (retailer == null) {
//					restResult.setSuccess(false);
//					throw new Exception("Invalid login");
//				}
//				if (!retailer.getApiToken().equals(apiToken)) {
//					restResult.setSuccess(false);
//					throw new Exception("Not Authorized for this request");
//				}
//
//				/*
//				 * Timestamp currentDate = new Timestamp(c.getTime().getTime());
//				 * if (currentDate.after(retailer.getTokenExpiryDate())) {
//				 * restResult.setSuccess(false); throw new Exception(
//				 * "Authentication expired. Please login again"); }
//				 */
//				/**
//				 * prem code start 20-06-2018 added ->addMobileProduct() for
//				 * enable validation for table RetailerOther
//				 *
//				 */
//				try {
//					RetailerOther retailerOther = masterdataService.createRetailerOtherQuery().retailerId(retailerId)
//							.singleResult();
//					if (retailerOther.getIsMobileProductAddAllow() == 1) {
//						/**
//						 * passing value for fields which we get from retailer
//						 */
//						RestApi restApi = new RestApi();
//
//						restApi.addMobileProduct(queryJSON, masterdataService, retailerId, fieldValues);
//
//					} else {
//						log.info("This Add Mobile Product is not valid for this Retailer");
//					}
//				} catch (Exception e) {
//					log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");
//
//				}
//
//				/**
//				 * prem code end
//				 */
//			}
//			try {
//				restResult = commonValidationFormData(queryJSON, retailer, "Buy Back");
//			} catch (Exception e) {
//				log.debug("Not Able To Validate Ticket Data : " + e);
//			}
//			if (restResult.success) {
//				RestExternalResult restResult1 = null;
//				try {
//					restResult1 = validatingBB(queryJSON, retailer);
//				} catch (Exception e) {
//					log.debug("Not Able To Validate Ticket Data on the Basis Of NOC : " + e);
//				}
//				if (restResult1.success) {
//					restResult = ticketCreation(jsonTicket, retailerId, apiToken);
//					/*
//					 * if (retailer.getApiUsername() != null) { String rawToken
//					 * = retailer.getApiUsername() + "___" +
//					 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() +
//					 * new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
//					 * apiToken = AuthUtil.cryptWithMD5(rawToken);
//					 * retailer.setApiToken(apiToken);
//					 * retailer.setTokenExpiryDate(new
//					 * Timestamp(c.getTime().getTime())); } else { String
//					 * rawToken =
//					 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() +
//					 * new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
//					 * apiToken = AuthUtil.cryptWithMD5(rawToken);
//					 * retailer.setApiToken(apiToken);
//					 * retailer.setTokenExpiryDate(new
//					 * Timestamp(c.getTime().getTime())); }
//					 */
//					try {
//						/**
//						 * comment because there will be constant api for every
//						 * request
//						 */
//						// masterdataService.saveRetailer(retailer);
//						restResult.setApiToken(apiToken);
//					} catch (Exception e) {
//					}
//				} else {
//					return restResult1;
//				}
//			} else {
//				return restResult;
//			}
//		} catch (Exception ex) {
//			log.error("Exception during tracking", ex);
//			restResult.setSuccess(false);
//			restResult.setMessage(ex.getMessage());
//			httpResponse.setStatus(500);
//		}
//		saveToConsoleLogsTable(retailerId, "Buy Back " + " Request Json: " + queryJSON + " Response:"
//				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());
//
//		return restResult;
//	}

	@RequestMapping(value = "/ticket/create/advanceexchange", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketAdvanceExchange(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		// String header = httpRequest.getHeader("Authorization");
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();
		Map<String, String> fieldValues = null;
		String jsonTicket = "";
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			fieldValues.put("natureOfComplaint", "Advance Exchange");
		} catch (Exception e) {
			log.debug("Error in Conversion :" + e);
		}
		try {
			jsonTicket = JSONValue.toJSONString(fieldValues);
		} catch (Exception e) {
			log.debug("Error in Conversoion from MAp to String :" + e);
		}

		try {
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					throw new Exception("Invalid login");
				}
				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}

				/*
				 * Timestamp currentDate = new Timestamp(c.getTime().getTime());
				 * if (currentDate.after(retailer.getTokenExpiryDate())) {
				 * restResult.setSuccess(false); throw new Exception(
				 * "Authentication expired. Please login again"); }
				 */
				/**
				 * prem code start 20-06-2018 added ->addMobileProduct() for
				 * enable validation for table RetailerOther
				 *
				 */
				try {
					RetailerOther retailerOther = masterdataService.createRetailerOtherQuery().retailerId(retailerId)
							.singleResult();
					if (retailerOther.getIsMobileProductAddAllow() == 1) {
						/**
						 * passing value for fields which we get from retailer
						 */
						RestApi restApi = new RestApi();

						restApi.addMobileProduct(queryJSON, masterdataService, retailerId, fieldValues);

					} else {
						log.info("This Add Mobile Product is not valid for this Retailer");
					}
				} catch (Exception e) {
					log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

				}

				/**
				 * prem code end
				 */
			}
			try {
				restResult = commonValidationFormData(queryJSON, retailer, "Advance Exchange");
			} catch (Exception e) {
				log.debug("Not Able To Validate Ticket Data : " + e);
			}
			if (restResult.success) {
				RestExternalResult restResult1 = validatingAE(queryJSON, retailer);
				if (restResult1.success) {
					restResult = ticketCreation(jsonTicket, retailerId, apiToken);
					/*
					 * if (retailer.getApiUsername() != null) { String rawToken
					 * = retailer.getApiUsername() + "___" +
					 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() +
					 * new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
					 * apiToken = AuthUtil.cryptWithMD5(rawToken);
					 * retailer.setApiToken(apiToken);
					 * retailer.setTokenExpiryDate(new
					 * Timestamp(c.getTime().getTime())); } else { String
					 * rawToken =
					 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() +
					 * new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
					 * apiToken = AuthUtil.cryptWithMD5(rawToken);
					 * retailer.setApiToken(apiToken);
					 * retailer.setTokenExpiryDate(new
					 * Timestamp(c.getTime().getTime())); }
					 */
					try {
						// masterdataService.saveRetailer(retailer);
						restResult.setApiToken(apiToken);
					} catch (Exception e) {
					}
				} else {
					return restResult1;
				}
			} else {
				return restResult;
			}
		} catch (Exception ex) {
			log.error("Exception during tracking", ex);
			restResult.setSuccess(false);
			restResult.setMessage(ex.getMessage());
			httpResponse.setStatus(500);
		}
		saveToConsoleLogsTable(retailerId, "Advance Exchange" + " Request Json: " + queryJSON + " Response:"
				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;
	}

	// API ENDS HERE

	@RequestMapping(value = "/process/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProcessCommandsWithRenaming(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "type", required = false, defaultValue = "false") String type) {
		// if (!beforeMethodInvocation(httpRequest, httpResponse)) {
		// return null;
		// }
		try {

			if (type.equals("true")) {
				// TODO add response
				// creationService.processBulkTicket(file.getInputStream(),
				// retailerId);

			} else {
				return new ProcessCommandProcessorWithRenamingExcel(retailerId).processUpload(file.getInputStream(),
						retailerId, file);
			}
		} catch (IOException ex) {
			log.debug("Error" + ex);
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
		return null;
	}

	protected class ProcessCommandProcessorExcel extends CSVDataProcessor {

		private static final String BEGIN = "BEGIN";

		public UploadProcessCommandsResult processUpload(InputStream is, String retailerId, MultipartFile file) {

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			/**
			 * Getting filename Extension
			 */

			try {
				String extension = FilenameUtils.getExtension(file.getOriginalFilename());
				log.debug(extension);
				TicketCreationImpl creation = new TicketCreationImpl();

				// Checking for valid excel format
				if (extension.equals("xls") || extension.equals("xlsx")) {

					Workbook workbook = WorkbookFactory.create(is);
					Cell cell = null;
					Sheet sheet;
					int totalRowCount = 0;
					int recordNo = 0;
					int insertCount = 0;
					List<String> orderedFields = new ArrayList<String>();
					DataFormatter dfmt = new DataFormatter();
					for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

						sheet = workbook.getSheetAt(j);
						totalRowCount = sheet.getLastRowNum();
						Row firstRow = sheet.getRow(0);
						recordNo = 1;

						try {
							/* Header record */
							for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
								orderedFields.add(firstRow.getCell(fr).getStringCellValue());
							}
							if (log.isDebugEnabled()) {
								log.debug("HEADER:" + recordNo + ":" + firstRow.toString());
								for (String f : orderedFields) {
									log.debug("------------" + f);
								}
							}
						} catch (Exception hex) {

						}

						for (int r = 1; r <= totalRowCount; r++) {

							Row row = sheet.getRow(r);
							Cell firstCellOfRow = row.getCell((int) row.getFirstCellNum());
							String firstCellOfRowInString = dfmt.formatCellValue(firstCellOfRow).trim();

							if (!firstCellOfRowInString.equals("") && !firstCellOfRowInString.isEmpty()
									&& firstCellOfRowInString != null) {
								recordNo++;

								log.debug("-------Processing Row num" + r);
								if (sheet.getRow(r) != null) {
									Map<String, String> fieldValues = new HashMap<>();
									for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
										String val;
										if (row.getCell(i) != null && !row.getCell(i).equals("")) {
											cell = row.getCell(i);
											val = dfmt.formatCellValue(cell);
										} else {
											val = "";
										}
										fieldValues.put(orderedFields.get(i), val);

									}
									log.debug(String.valueOf(fieldValues.size()));
									retval = creation.buildTicket(fieldValues, retailerId, r);
									insertCount++;
								} else {
									log.debug("Skipping blank rows");
								}

							} else {
								// TODO Error msgs
								// retval.setErrorMessages("Unable To Upload");
							}

						}
						retval.setNoOfRecordsInserted(insertCount);
					}
				}
				/**
				 * For Csv files
				 */
				if (extension.equals("csv")) {
					int iteration = 0;
					Map<String, String> fieldValues;
					CsvProcessor csvProcessor = new CsvProcessor();
					List<Map<String, String>> listOftickets = csvProcessor.processCsvData(file);
					if (listOftickets != null) {
						ListIterator<Map<String, String>> listIteratorOfTickets = listOftickets.listIterator();
						while (listIteratorOfTickets.hasNext()) {
							fieldValues = listIteratorOfTickets.next();
							log.debug("Value" + fieldValues);
							retval = creation.buildTicket(fieldValues, retailerId, iteration);
							iteration++;
						}
					} else {
						log.debug("Error in Uplaoding CSV");
					}
					retval.setNoOfRecordsInserted(iteration);
				}
			} catch (Exception ex) {
				log.debug("Error while processing" + ex);
				ErrorObject errorObject = new ErrorObject();
				if (ex != null) {
					errorObject.setMessage(ex.getMessage());
					retval.getErrorMessages().add(errorObject);
				} else {
					errorObject.setMessage("Failed to read uploaded content");
					retval.getErrorMessages().add(errorObject);
				}
			}
			return retval;
		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) throws Exception {
			return fieldValues;
		}

	}

	// Changes For Entry In Open Tickets Table;
	// UnComment Above Code Then This One .
	// public void insertIntoOpenTickets(String ticketNo) {
	//
	// MasterdataService masterdataService2 = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	// try {
	// String currentDate = com.rlogistics.util.DateUtil.getCurrentDate();
	// OpenTickets openTickets = masterdataService2.newOpenTickets();
	//
	// System.out.println("current date : " + currentDate+" : "+ticketNo);
	// openTickets.setCreatedDate(currentDate);
	// openTickets.setTicketNo(ticketNo);
	// try {
	// masterdataService2.saveOpenTickets(openTickets);
	// } catch (Exception e) {
	// System.out.println("exception in Savng OpenTickets : " + e);
	// }
	//
	// } catch (Exception e) {
	// System.out.println("Exception in inserting insertIntoOpenTickets" + e);
	// log.debug("Exception in inserting insertIntoOpenTickets" + e);
	// }
	//
	// }

	protected class ProcessCommandProcessorWithRenamingExcel extends ProcessCommandProcessorExcel {
		private Map<String, String> canonicalFieldByRetailerField;

		public ProcessCommandProcessorWithRenamingExcel(String retailerId) {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			List<RetailerProcessField> retailerProcessFieldList = masterdataService.createRetailerProcessFieldQuery()
					.retailerId(retailerId).list();

			// canonicalFieldByRetailerField = new HashMap<String, String>();
			canonicalFieldByRetailerField = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
			log.debug("MAPPING");
			for (RetailerProcessField retailerProcessField : retailerProcessFieldList) {
				canonicalFieldByRetailerField.put(retailerProcessField.getRetailerField(),
						retailerProcessField.getCanonicalField());
				log.debug(retailerProcessField.getRetailerField() + ":" + retailerProcessField.getCanonicalField());
			}
		}

		@Override
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) throws Exception {

			Map<String, String> translatedFieldValues = new HashMap<String, String>();
			for (String incomingField : fieldValues.keySet()) {
				// log.debug(incomingField +
				// canonicalFieldByRetailerField.get(incomingField));
				if (canonicalFieldByRetailerField.get(incomingField) == null) {

					log.debug("Null key for a map " + incomingField);
					throw new Exception(incomingField + " is not a valid column");
				}
				translatedFieldValues.put(canonicalFieldByRetailerField.get(incomingField),
						fieldValues.get(incomingField));

			}

			return translatedFieldValues;
		}
	}

	public synchronized RestExternalResult ticketCreation(String fieldValueJSON, String retailerId, String apiToken) {

		RestExternalResult restResponse = new RestExternalResult();
		Map<String, String> result = new HashMap<String, String>();

		MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMetadataService();
		RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRepositoryService();

		FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getFormService();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Map<String, String> fieldValues = null;

		try {
			System.out.println("fieldValueJSON : " + fieldValueJSON);
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);
			// setvaribaleBeforeTicketCreation(fieldValues, retailerId,
			// apiToken);

		} catch (IOException iox) {

		}
		// Below Codes To Insert Multiple Ticket in Single API Call
		// ArrayList<Map<String, String>> listFieldValues = null;
		// Map<String, String> fieldValues2 = null;
		//
		// try {
		// listFieldValues = new
		// ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON),
		// ArrayList.class);
		// } catch (IOException iox) {
		//
		// }

		String processName = "";
		String dataset = "BizLog";
		
		

//		FranchisePincode franchisePincode = masterdataService.createFranchisePincodeQuery().
//				pincode(fieldValues.get("pincode")).singleResult();
//		log.debug("**************************************"+franchisePincode+"**********************************");
//		if(franchisePincode!=null)
//		{
//			processName="fl_Flow";
//			if (retailerId != null) {
//				Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
//				String retailer = retailerObj.getName();
//				fieldValues.put("retailer", retailer);
//			}
//			ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset)
//					.dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();
//
//			ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
//					.deploymentId(deployment.getDeploymentId()).singleResult();
//
//			FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(), false);
//			GroupDescription groupDescription = null;
//			log.debug("**************************************In Fl Flowww**********************************");
//			while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
//				if (log.isDebugEnabled()) {
//					log.debug("Current groupDescription:" + groupDescription);
//					System.out.println("Current groupDescription:" + groupDescription);
//				}
//
//				if (groupDescription.isLastGroup()) {
//					break;
//				}
//			}
//
//			// submitStartForm(formService, definition, formFillerUtil);
//			ProcessInstance processInstance = submitStartForm(formService, definition, formFillerUtil);
//
//		}
//		else {
		try {
			// Same For Uploading Multiple Tickets
			// for (Map<String, String> fieldValues : listFieldValues) {
			String productCategory = fieldValues.get("productCategory").toLowerCase();
			// String productCategory = "mobile";
			FranchisePincode franchisePincode = masterdataService.createFranchisePincodeQuery().
					pincode(fieldValues.get("pincode")).singleResult();
			log.debug("**************************************"+franchisePincode+"**********************************");
			if(franchisePincode!=null && fieldValues.get("natureOfComplaint").equalsIgnoreCase("Pick And Drop (One Way)"))
			{
				processName="fl_Flow";
			}
			else {
			MasterCategoryResult masterCategoryResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getMasterCategory(productCategory);
			processName = masterCategoryResult.getProcessFlow();
			}
			if (retailerId != null) {
				Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				String retailer = retailerObj.getName();
				fieldValues.put("retailer", retailer);
			}
			ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset)
					.dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();

			ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
					.deploymentId(deployment.getDeploymentId()).singleResult();

			FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(), false);
			GroupDescription groupDescription = null;
			while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
				if (log.isDebugEnabled()) {
					log.debug("Current groupDescription:" + groupDescription);
					System.out.println("Current groupDescription:" + groupDescription);
				}

				if (groupDescription.isLastGroup()) {
					break;
				}
			}

			// submitStartForm(formService, definition, formFillerUtil);
			ProcessInstance processInstance = submitStartForm(formService, definition, formFillerUtil);
			System.out.println("processInstance.getProcessInstanceId() : " + processInstance.getProcessInstanceId());
			String processId = processInstance.getProcessInstanceId();
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();

			String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
			log.debug("TICKET NO " + ticketNo);

			/**
			 * prem created 21-03-2019 calling ReportPojo class to send
			 * variables to report server with new thread
			 * TicketCreationImpl.java create ticket from excel
			 */
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						ReportPojo pojo = new ReportPojo(processId);
						pojo.sentToReportServer(pojo);
						TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(processId),
								processId, "TICKET_CREATED");
					} catch (Exception e) {
						log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

					}
				}
			}).start();

			/**
			 * Setting Report Date For tickets generated by api
			 */

			DateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date date = new Date();
			s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			String tt = s.format(date);
			DateTime dateTimeIndia = new DateTime(tt);
			Date actDate = dateTimeIndia.toDate();
			runtimeService.setVariable(processId, "ticketCreationDate", actDate);
			log.debug("TicketCreation DATE"
					+ String.valueOf(runtimeService.getVariable(processId, "ticketCreationDate")));

			/**
			 * Setting City for report generated by api
			 */
			if ((fieldValues.containsKey("pincode")) && fieldValues.get("pincode") != null) {

				try {
					LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
							.pincode(fieldValues.get("pincode")).singleResult();

					ProcessLocation processLocation = masterdataService.createProcessLocationQuery()
							.id(locationPincodes.getLocationId()).singleResult();

					runtimeService.setVariable(processId, "rlReportingCity", processLocation.getCity());
					log.debug(runtimeService.getVariable(processId, "rlReportingCity").toString());

				} catch (Exception e) {
					log.debug("Error in putting value to reporting city" + e);
				}
			}

			/**
			 * Prem code for payment option and All product addition
			 */
			if (setVariableExternalFields(fieldValues, runtimeService, processId)) {

			} else {
				log.debug("problem in setting external variable in create ticket with volmatric weight etc");

			}

			// runtimeService.setVariable(processId, "ticketCreationDate",
			// com.rlogistics.util.DateUtil.currentDataTime());
			// System.out.println("ticketCreationDate : " +
			// String.valueOf(runtimeService.getVariable(processId,
			// "ticketCreationDate")));
			// log.debug("TicketCreation
			// DATE"+String.valueOf(runtimeService.getVariable(processId,
			// "ticketCreationDate")));
			/**
			 * this if I had applied because if product code is same while
			 * product add this it is showing this
			 * {"response":{"success":"true"},"ticketNo":["null"],"message":
			 * "Ticket created successfully",
			 * "success":true,"apiToken":"f61ec17a92841ac94f648cf6d5b6991c"}
			 */
			saveToConsoleLogsTable(retailerId, ticketNo);
			if (ticketNo.equals("") || ticketNo.equals(null) || ticketNo.equals("null")) {
				restResponse.getTicketNo().add(ticketNo);
				result.put("success", "false");
				restResponse.setResponse(result);
				restResponse.setSuccess(false);
				restResponse.setMessage("Ticket not created. Product code is not valid");
			} else {
				restResponse.getTicketNo().add(ticketNo);

				// Here the For Loop End For Inserting Multiple Tickets
				// }

				result.put("success", "true");
				restResponse.setResponse(result);
				restResponse.setSuccess(true);
				restResponse.setMessage("Ticket created successfully");
			}

			// generate ticket
			// generate new apiToken
			Retailer retailer = null;
			Calendar c = Calendar.getInstance();
			c.add(Calendar.HOUR, 1);
			retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			/*
			 * if (retailer.getApiUsername() != null) { String rawToken =
			 * retailer.getApiUsername() + "___" +
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); } else { String rawToken =
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); }
			 */
			try {
				/**
				 * commented because cuncurrent PAI is not working with the
				 * client 26-09-2018
				 */
				// masterdataService.saveRetailer(retailer);
				restResponse.setApiToken(apiToken);
			} catch (Exception e) {

			}

		} catch (Exception e) {
			System.out.println("Exception : " + e);
			result.put("success", "false");
			restResponse.setResponse(result);
			restResponse.setSuccess(false);
			restResponse.setMessage("Failed to create ticket" + e.getMessage());
		}

//		}
		return restResponse;
	}

	private void setvaribaleBeforeTicketCreation(Map<String, String> fieldValues, String retailerId, String apiToken) {

		// -----------------------------for accept accessories

		try {
			if ((fieldValues.containsKey("BoxPickUp")) && fieldValues.get("BoxPickUp") != null) {
				String value = "false";
				if (fieldValues.get("BoxPickUp").equals("Yes")) {
					value = "true";
				}
				fieldValues.put("rlPouchPickUp", value);
				log.debug("-------------" + value + fieldValues.get("rlPouchPickUp"));

				// runtimeService.setVariable(processId, "rlPouchPickUp",
				// value);
			}
		} catch (Exception e) {
			log.debug("There is no field BoxPickUp" + e);
		}
		try {
			if ((fieldValues.containsKey("BatteryPickUp")) && fieldValues.get("BatteryPickUp") != null) {
				String value = "false";
				if (fieldValues.get("BatteryPickUp").equals("Yes")) {
					value = "true";
				}
				fieldValues.put("rlBatteryPickUp", value);
				// runtimeService.setVariable(processId, "rlBatteryPickUp",
				// fieldValues.get("BatteryPickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field BatteryPickUp" + e);
		}
		try {
			if ((fieldValues.containsKey("HeadPhonePickUp")) && fieldValues.get("HeadPhonePickUp") != null) {
				String value = "false";
				if (fieldValues.get("HeadPhonePickUp").equals("Yes")) {
					value = "true";
				}
				fieldValues.put("rlHeadPhonePickUp", value);
				// runtimeService.setVariable(processId, "rlHeadPhonePickUp",
				// fieldValues.get("HeadPhonePickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field HeadPhonePickUp" + e);
		}
		try {
			if ((fieldValues.containsKey("ChargerPickUp")) && fieldValues.get("ChargerPickUp") != null) {
				String value = "false";
				if (fieldValues.get("ChargerPickUp").equals("Yes")) {
					value = "true";
				}
				fieldValues.put("rlChargerPickUp", value);
				// runtimeService.setVariable(processId, "rlPouchPickUp",
				// fieldValues.get("ChargerPickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field ChargerPickUp" + e);
		}
		try {
			if ((fieldValues.containsKey("OthersPickUp")) && fieldValues.get("OthersPickUp") != null) {
				String value = fieldValues.get("OthersPickUp");
				fieldValues.put("rlOthersPickUp", value);
				// runtimeService.setVariable(processId, "rlOthersPickUp",
				// fieldValues.get("OthersPickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field OthersPickUp" + e);
		}

	}

	private boolean setVariableExternalFields(Map<String, String> fieldValues, RuntimeService runtimeService,
			String processId) {
		try {
			if ((fieldValues.containsKey("amountToBeCollected")) && fieldValues.get("amountToBeCollected") != null) {
				runtimeService.setVariable(processId, "rlAmountToBeCollected", fieldValues.get("amountToBeCollected"));
				// runtimeService.setVariable(processId, "rlCustomerPay",
				// "Yes");

			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volLenght")) && fieldValues.get("volLenght") != null) {
				runtimeService.setVariable(processId, "rlVolLenght", fieldValues.get("volLenght"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volHeight")) && fieldValues.get("volHeight") != null) {
				runtimeService.setVariable(processId, "rlVolHeight", fieldValues.get("volHeight"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volWidth")) && fieldValues.get("volWidth") != null) {
				runtimeService.setVariable(processId, "rlVolWidth", fieldValues.get("volWidth"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("actualWeight")) && fieldValues.get("actualWeight") != null) {
				runtimeService.setVariable(processId, "rlActualWeight", fieldValues.get("actualWeight"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("transactionType")) && fieldValues.get("transactionType") != null) {
				runtimeService.setVariable(processId, "rlTransactionType", fieldValues.get("transactionType"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		// ------------------------ for accessories
		try {
			if ((fieldValues.containsKey("BoxPickUp")) && fieldValues.get("BoxPickUp") != null) {
				if (fieldValues.get("BoxPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlPouchPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlPouchPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlPouchPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("BatteryPickUp")) && fieldValues.get("BatteryPickUp") != null) {
				if (fieldValues.get("BatteryPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlBatteryPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlBatteryPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlBatteryPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("HeadPhonePickUp")) && fieldValues.get("HeadPhonePickUp") != null) {
				if (fieldValues.get("HeadPhonePickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlHeadPhonePickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlHeadPhonePickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field HeadPhonePickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("ChargerPickUp")) && fieldValues.get("ChargerPickUp") != null) {
				if (fieldValues.get("ChargerPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlChargerPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlChargerPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlChargerPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("OthersPickUp")) && fieldValues.get("OthersPickUp") != null) {
				runtimeService.setVariable(processId, "rlOthersPickUp", fieldValues.get("OthersPickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field OthersPickUp be collected" + e);
		}
		// for recycle device OTP
		try {
			if (runtimeService.getVariable(processId, "rlServiceCode").toString().equals("BUYBACK")) {
				if (runtimeService.getVariable(processId, "rlRetailerCode").toString().equals("RCB")) {
					runtimeService.setVariable(processId, "rlOtpVariable", "");
					runtimeService.setVariable(processId, "rlOtp", "Yes");
				} else {
					runtimeService.setVariable(processId, "rlOtpVariable", "");
					runtimeService.setVariable(processId, "rlOtp", "No");
				}

			}

		} catch (Exception e) {
			log.debug("There is no field OthersPickUp be collected" + e);
		}
		// FOR PAYMENT VARIABLE
		try {
			if ((fieldValues.containsKey("amountToBePaid")) && fieldValues.get("amountToBePaid") != null) {
				runtimeService.setVariable(processId, "amountToBePaid", fieldValues.get("amountToBePaid"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountToBePaid" + e);
		}
		try {
			if ((fieldValues.containsKey("modeOfPayment")) && fieldValues.get("modeOfPayment") != null) {
				runtimeService.setVariable(processId, "modeOfPayment", fieldValues.get("modeOfPayment"));
			}
		} catch (Exception e) {
			log.debug("There is no field modeOfPayment" + e);
		}
		return true;

	}

	/**
	 * VALIDATING THE FORM DATA SO THAT IT WILL NOT ASSIGNED TO HO AND GIVING
	 * THE ERROR IN UPLOADING THE TICKET.
	 */

	// General Ticket Validation
	public RestExternalResult validatingFormData(String fieldValueJSON, Retailer retailer) {

		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		Map<String, String> fieldValues = null;
		String errorResponse = "";
		RestExternalResult restResponse = new RestExternalResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		try {
			System.out.println("fieldValueJSON : " + fieldValueJSON);
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);

			// SERVICE TYPE SHOULD BE A VALID SERVICE
			String natureOfComplaint = fieldValues.get("natureOfComplaint").toLowerCase();
			ServiceMaster serviceMaster = masterdataService.createServiceMasterQuery().name(natureOfComplaint)
					.singleResult();
			String rlServiceCode = "";
			String rlServiceId = "";
			if (serviceMaster != null) {
				rlServiceCode = serviceMaster.getCode();
				rlServiceId = serviceMaster.getId();
			} else {
				log.debug("is not a valid Service ");
				errorResponse = errorResponse + natureOfComplaint + " is not a valid Service.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
			// SERVICE TYPE SHOULD BE AVAILED BY THE RETAILER
			RetailerServices retailerServices = masterdataService.createRetailerServicesQuery().retailerId(retailerId)
					.serviceId(rlServiceId).singleResult();
			if (retailerServices == null) {
				log.debug("is not a valid service of");
				errorResponse = errorResponse + natureOfComplaint + " is not a valid service of " + retailerName;
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			// String rlProductSubCategoryId = "";
			// String rlProductBrandId = "";

			// Apart From Buy BACK and Adv Exchange
			if (!rlServiceCode.equalsIgnoreCase("BUYBACK") && !rlServiceCode.equalsIgnoreCase("ADVEXCHANGE")) {
				RestExternalResult rer = validatingAllFlow(fieldValues, retailer);
				if (restResponse.success) {
					log.debug("Error Res : " + rer.getResponse());
					restResponse.setMessage(rer.getMessage());
					restResponse.setResponse(rer.getResponse());
					restResponse.setSuccess(rer.isSuccess());
				} else {
					log.debug("Error Res : " + rer.getResponse());
				}

				// If Service Type Is Buy Back or Advance Exchange

			} else {
				RestExternalResult rer = validatingAEndBB(fieldValues, retailer, rlServiceCode);
				if (restResponse.success) {
					log.debug("Error Res : " + rer.getResponse());
					restResponse.setMessage(rer.getMessage());
					restResponse.setResponse(rer.getResponse());
					restResponse.setSuccess(rer.isSuccess());
				} else {
					log.debug("Error Res : " + rer.getResponse());
				}
			}

			return restResponse;

		} catch (IOException iox) {

		}
		return restResponse;

	}

	// Validating the Advance Exchange And BUY-BACK Ticket

	public RestExternalResult validatingAEndBB(Map<String, String> fieldValues, Retailer retailer,
			String rlServiceCode) {
		// EXCLUSIVELY FOR BUY BACK FLOW, ADVNACE EXCHANGE FLOW & DROP
		// OFF FLOW

		// Map<String, String> fieldValues = null;
		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		RestExternalResult restResponse = new RestExternalResult();
		restResponse.setSuccess(true);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
		if (brand == null) {
			log.debug("is not a valid Brand");
			errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// VALIDATE CATEGORY
		// Newly Added Code
		ProductCategory productCategory = masterdataService.createProductCategoryQuery()
				.name(fieldValues.get("productCategory")).singleResult();

		if (productCategory != null) {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategoryCode = productCategory.getCode();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			// VALIDATE MASTER CATEGORY
			MasterCategory masterCategory = masterdataService.createMasterCategoryQuery().id(rlMasterCategoryId)
					.singleResult();
			String rlMasterCatProcess = "";
			if (masterCategory != null) {
				// GET THE PROCESS ASSOCIATED WITH THE MASTER CATEGORY
				rlMasterCatProcess = masterCategory.getProcessFlow();
			}

			if (!rlMasterCatProcess.equalsIgnoreCase("IT_PRODUCTS")) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse
						+ "Ticket is created under a wrong process. Close this and choose the correct process. \n";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		} else {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// End Here
		// ProductCategory productCategory =
		// masterdataService.createProductCategoryQuery()
		// .name(fieldValues.get("productCategory")).singleResult();
		if (productCategory == null) {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		} else {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
				log.debug("is not a valid Category for this product.");
				errorResponse = errorResponse + fieldValues.get("productCategory")
						+ " is not a valid Category for this product.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			// CATEGORY SHOULD BE A SERVICEABLE CATEGORY OF THE RETAILER
			RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
					.retailerId(retailerId).categoryId(rlProductCategoryId).singleResult();
			if (retailerCategories == null) {
				log.debug("is not a valid category of ");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid category of"
						+ retailerName + ".";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		}

		// PINCODE SHOULD BE VALID

		LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
				.pincode(fieldValues.get("pincode")).singleResult();
		if (locationPincodes != null) {
			String rlLocationId = locationPincodes.getLocationId();
			// GET LOCATION NAME AND OTHER DETAILS
			ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
					.singleResult();
			if (processLocation == null) {
				log.debug("is not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode") + " is not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		} else {
			log.debug("is not a valid Pincode or not attached to any BizLog branches");
			errorResponse = errorResponse + fieldValues.get("pincode")
					+ " is not a valid Pincode or not attached to any BizLog branches";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// PINCODE SHOULD BE A SERVICEABLE LOCATION OF THE RETAILER
		RetailerServiceableLocations retailerServiceableLocations = masterdataService
				.createRetailerServiceableLocationsQuery().retailerId(retailerId).pincode(fieldValues.get("pincode"))
				.singleResult();
		if (retailerServiceableLocations == null) {
			// Check if the pincode falls in the ODA Locations
			RetailerOdaLocations retailerOdaLocations = masterdataService.createRetailerOdaLocationsQuery()
					.retailerId(retailerId).pincode(fieldValues.get("pincode")).singleResult();
			if (retailerOdaLocations == null) {
				log.debug("is not a serviceable location of");
				errorResponse = errorResponse + fieldValues.get("pincode") + " is not a serviceable location of "
						+ retailerName + ".";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);

			}

		}

		// REFUND AMOUNT IS MANDATORY IF THE SERVICE TYPE IS REFUND

		if (rlServiceCode.equalsIgnoreCase("REFUND")) {
			if (fieldValues.get("refundAmt") != null && !"".equalsIgnoreCase(fieldValues.get("refundAmt"))
					&& Integer.valueOf(fieldValues.get("refundAmt")) == 0) {
				log.debug("Invalid Refund amount. Get the exact amount from the Retailer.");
				errorResponse = errorResponse + "Invalid Refund amount. Get the exact amount from the " + retailerName
						+ ".";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		}

		// RETURN SELLER DETAILS AND PICK UP SELLER DETAILS ARE
		// MANDATORY AND SHOULD BE VALID FOR REPLACEMENT AND ADVANCE
		// EXCHANGE
		if (rlServiceCode.equalsIgnoreCase("REPLACEMENT") || rlServiceCode.equalsIgnoreCase("ADVEXCHANGE")
				|| rlServiceCode.equalsIgnoreCase("BUYBACK")) {

			// GET THE RETURN ADDRESS AND DETAILS USING THE RETURN
			// SELLER CODE AND RETURN SELLER PINCODE FROM THE TICKET

			RetailerReturnOrPickupLocations retailerReturnOrPickupLocations = masterdataService
					.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
					.sellerCode(fieldValues.get("returnSellerCode"))
					.sellerPincode(fieldValues.get("returnSellerPincode")).singleResult();
			if (retailerReturnOrPickupLocations == null) {
				log.debug("Return Location details are invalid. Get the correct code and pincode.");
				errorResponse = errorResponse
						+ "Return Location details are invalid. Get the correct code and pincode.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);

			}
			// GET THE PICK UP ADDRESS AND DETAILS USING THE PICKUP
			// SELLER CODE AND PICK UP SELLER PINCODE FROM THE TICKET
			RetailerReturnOrPickupLocations retailerReturnOrPickupLocations2 = masterdataService
					.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
					.sellerCode(fieldValues.get("pickupSellerCode"))
					.sellerPincode(fieldValues.get("pickupSellerPincode")).singleResult();
			if (retailerReturnOrPickupLocations2 == null) {
				log.debug("Pickup Location details are invalid. Get the correct code and pincode");
				errorResponse = errorResponse
						+ "Pickup Location details are invalid. Get the correct code and pincode.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		}
		return restResponse;

	}

	// Validating All Other Flow Apart From AE And BB

	public RestExternalResult validatingAllFlow(Map<String, String> fieldValues, Retailer retailer) {

		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		String rlProductSubCategoryId = "";
		String rlProductBrandId = "";
		RestExternalResult restResponse = new RestExternalResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		String productCode = fieldValues.get("productCode").toLowerCase();
		// VALIDATE PRODUCT
		Product product = masterdataService.createProductQuery().code(productCode).retailerId(retailerId)
				.singleResult();

		if (product == null) {
			log.debug("is not a valid Product Code of the Retailer.");
			errorResponse = errorResponse + productCode + " is not a valid Product Code of " + retailerName + ".";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		} else {
			rlProductSubCategoryId = product.getSubCategoryId();
			rlProductBrandId = product.getBrandId();

		}

		// VALIDATE TICKET CATEGORY VS PRODUCT'S SUBCATEGORY'S CATEGORY

		ProductSubCategory productSubCategory = masterdataService.createProductSubCategoryQuery()
				.id(rlProductSubCategoryId).singleResult();

		if (productSubCategory != null) {
			// String rlProductSubCategoryCode =
			// productSubCategory.getCode();
			String rlProductCategoryId = productSubCategory.getProductCategoryId();

			// Get the corresponding category name
			ProductCategory productCategory = masterdataService.createProductCategoryQuery().id(rlProductCategoryId)
					.singleResult();

			if (productCategory != null) {
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
					log.debug("is not a valid Category for this product.");
					errorResponse = errorResponse + fieldValues.get("productCategory")
							+ " is not a valid Category for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		}
		// VALIDATE BRAND VS PRODUCT BRAND
		// Get the Brand Name using the BrandId from Product
		Brand brand = masterdataService.createBrandQuery().id(rlProductBrandId).singleResult();
		if (brand != null) {
			// Get the brand name
			String rlProductBrand = brand.getName();
			String rlProductBrandCode = brand.getCode();
			if (!rlProductBrand.equalsIgnoreCase(fieldValues.get("brand"))) {
				log.debug("is not a valid Brand for this product");
				errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand for this product";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		}
		return restResponse;

	}

	// Common Validation For All The Tickets

	public RestExternalResult commonValidationFormData(String fieldValueJSON, Retailer retailer,
			String natureOfComplaint) {

		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		Map<String, String> fieldValues = null;
		String errorResponse = "";
		RestExternalResult restResponse = new RestExternalResult();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		try {
			System.out.println("fieldValueJSON : " + fieldValueJSON);
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);

			// SERVICE TYPE SHOULD BE A VALID SERVICE
			// String natureOfComplaint =
			// fieldValues.get("natureOfComplaint").toLowerCase();
			ServiceMaster serviceMaster = masterdataService.createServiceMasterQuery().name(natureOfComplaint)
					.singleResult();
			String rlServiceCode = "";
			String rlServiceId = "";
			if (serviceMaster != null) {
				rlServiceCode = serviceMaster.getCode();
				rlServiceId = serviceMaster.getId();
			} else {
				log.debug("is not a valid Service ");
				errorResponse = errorResponse + natureOfComplaint + " is not a valid Service.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
			// SERVICE TYPE SHOULD BE AVAILED BY THE RETAILER
			RetailerServices retailerServices = masterdataService.createRetailerServicesQuery().retailerId(retailerId)
					.serviceId(rlServiceId).singleResult();
			if (retailerServices == null) {
				log.debug("is not a valid service of");
				errorResponse = errorResponse + natureOfComplaint + " is not a valid service of " + retailerName;
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
			return restResponse;

		} catch (IOException iox) {

		}
		return restResponse;

	}
	// Validation For Repair Tickets

	public RestExternalResult validatingRepair(String fieldValueJSON, Retailer retailer) {

		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		Map<String, String> fieldValues = null;
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);
		} catch (Exception e) {
			log.debug("Error in Fetching fieldValues : " + e);
		}
		RestExternalResult restResponse = new RestExternalResult();
		restResponse.setSuccess(true);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		if (fieldValues.containsKey("brand")) {
			Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
			if (brand == null) {
				log.debug("is not a valid Brand");
				errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		} else {
			log.debug("brand column is Not There");
			errorResponse = errorResponse + "brand column is not there";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// VALIDATE CATEGORY
		// Newly Added Code
		if (fieldValues.containsKey("productCategory")) {
			ProductCategory productCategory = masterdataService.createProductCategoryQuery()
					.name(fieldValues.get("productCategory")).singleResult();

			if (productCategory != null) {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategoryCode = productCategory.getCode();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				// VALIDATE MASTER CATEGORY
				MasterCategory masterCategory = masterdataService.createMasterCategoryQuery().id(rlMasterCategoryId)
						.singleResult();
				String rlMasterCatProcess = "";
				if (masterCategory != null) {
					// GET THE PROCESS ASSOCIATED WITH THE MASTER CATEGORY
					rlMasterCatProcess = masterCategory.getProcessFlow();
				}

				if (!(rlMasterCatProcess.equalsIgnoreCase("APPARELS_SHOES"))) {
					log.debug("is not a valid Category.");
					errorResponse = errorResponse
							+ "Ticket is created under a wrong process. Close this and choose the correct process. \n";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			} else {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			if (productCategory == null) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			} else {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
					log.debug("is not a valid Category for this product.");
					errorResponse = errorResponse + fieldValues.get("productCategory")
							+ " is not a valid Category for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
				Product product = masterdataService.createProductQuery().code(fieldValues.get("productCode"))
						.retailerId(retailerId).singleResult();

				if (product != null && !(product.equals(""))) {
					String productName = product.getName();
					String productId = product.getId();
					String productSubCategoryId = product.getSubCategoryId();
					String productBrandId = product.getBrandId();
				} else {
					log.debug("is not a valid Product Code for this product.");
					errorResponse = errorResponse + fieldValues.get("productCode")
							+ " is not a valid Product Code for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

				// CATEGORY SHOULD BE A SERVICEABLE CATEGORY OF THE RETAILER
				RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
						.retailerId(retailerId).categoryId(rlProductCategoryId).singleResult();
				if (retailerCategories == null) {
					log.debug("is not a valid category of ");
					errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid category of"
							+ retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		} else {
			log.debug("Product Category Column is Missing.");
			errorResponse = errorResponse + " product category column is missing.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}

		// PINCODE SHOULD BE VALID
		if (fieldValues.containsKey("pincode")) {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
					.pincode(fieldValues.get("pincode")).singleResult();
			if (locationPincodes != null) {
				String rlLocationId = locationPincodes.getLocationId();
				// GET LOCATION NAME AND OTHER DETAILS
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
						.singleResult();
				if (processLocation == null) {
					log.debug("is not attached to any BizLog branches");
					errorResponse = errorResponse + fieldValues.get("pincode")
							+ " is not attached to any BizLog branches";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

			} else {
				log.debug("is not a valid Pincode or not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode")
						+ " is not a valid Pincode or not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
			// PINCODE SHOULD BE A SERVICEABLE LOCATION OF THE RETAILER
			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("pincode")).singleResult();
			if (retailerServiceableLocations == null) {

				// Check if the pincode falls in the ODA Locations
				RetailerOdaLocations retailerOdaLocations = masterdataService.createRetailerOdaLocationsQuery()
						.retailerId(retailerId).pincode(fieldValues.get("pincode")).singleResult();
				if (retailerOdaLocations == null) {
					log.debug("is not a serviceable location of");
					errorResponse = errorResponse + fieldValues.get("pincode") + " is not a serviceable location of "
							+ retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		} else {
			log.debug("pincode column is Not There");
			errorResponse = errorResponse + "pincode column is not there";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// RETURN SELLER DETAILS AND PICK UP SELLER DETAILS ARE
		// MANDATORY AND SHOULD BE VALID FOR REPLACEMENT AND BUY
		// BACK

		// GET THE RETURN ADDRESS AND DETAILS USING THE RETURN
		// SELLER CODE AND RETURN SELLER PINCODE FROM THE TICKET

		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("returnSellerCode")).sellerPincode(fieldValues.get("returnSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations == null) {
			log.debug("Return Location details are invalid. Get the correct code and pincode.");
			errorResponse = errorResponse + "Return Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);

		}
		// GET THE PICK UP ADDRESS AND DETAILS USING THE PICKUP
		// SELLER CODE AND PICK UP SELLER PINCODE FROM THE TICKET
		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations2 = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("pickupSellerCode")).sellerPincode(fieldValues.get("pickupSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations2 == null) {
			log.debug("Pickup Location details are invalid. Get the correct code and pincode");
			errorResponse = errorResponse + "Pickup Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}

		return restResponse;
	}

	public RestExternalResult validatingRepairStandBy(String fieldValueJSON, Retailer retailer) {
		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		Map<String, String> fieldValues = null;
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);
		} catch (Exception e) {
			log.debug("Error in Fetching fieldValues : " + e);
		}
		RestExternalResult restResponse = new RestExternalResult();
		restResponse.setSuccess(true);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		if (fieldValues.containsKey("brand")) {
			Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
			if (brand == null) {
				log.debug("is not a valid Brand");
				errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		} else {
			log.debug("brand column is missing");
			errorResponse = errorResponse + " brand column is missing";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// VALIDATE CATEGORY
		// Newly Added Code
		if (fieldValues.containsKey("productCategory")) {
			ProductCategory productCategory = masterdataService.createProductCategoryQuery()
					.name(fieldValues.get("productCategory")).singleResult();

			if (productCategory != null) {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategoryCode = productCategory.getCode();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				// VALIDATE MASTER CATEGORY
				// MasterCategory masterCategory =
				// masterdataService.createMasterCategoryQuery().id(rlMasterCategoryId)
				// .singleResult();
				// String rlMasterCatProcess = "";
				// if (masterCategory != null) {
				// // GET THE PROCESS ASSOCIATED WITH THE MASTER CATEGORY
				// rlMasterCatProcess = masterCategory.getProcessFlow();
				// }

				// if (!(rlMasterCatProcess.equalsIgnoreCase("APPARELS_SHOES")))
				// {
				// log.debug("is not a valid Category.");
				// errorResponse = errorResponse
				// + "Ticket is created under a wrong process. Close this and
				// choose
				// the correct process. \n";
				// restResponse.setSuccess(false);
				// restResponse.setResponse(errorResponse);
				// }
			} else {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			if (productCategory == null) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			} else {
				String rlProductCategoryId = productCategory.getId();
				String rlProductCategory = productCategory.getName();
				String rlMasterCategoryId = productCategory.getMasterCategoryId();

				if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
					log.debug("is not a valid Category for this product.");
					errorResponse = errorResponse + fieldValues.get("productCategory")
							+ " is not a valid Category for this product.";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

				// CATEGORY SHOULD BE A SERVICEABLE CATEGORY OF THE RETAILER
				RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
						.retailerId(retailerId).categoryId(rlProductCategoryId).singleResult();
				if (retailerCategories == null) {
					log.debug("is not a valid category of ");
					errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid category of"
							+ retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		} else {
			log.debug("Product Category Column is Missing.");
			errorResponse = errorResponse + " product category column is missing.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}

		// PINCODE SHOULD BE VALID
		if (fieldValues.containsKey("pincode")) {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
					.pincode(fieldValues.get("pincode")).singleResult();
			if (locationPincodes != null) {
				String rlLocationId = locationPincodes.getLocationId();
				// GET LOCATION NAME AND OTHER DETAILS
				ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
						.singleResult();
				if (processLocation == null) {
					log.debug("is not attached to any BizLog branches");
					errorResponse = errorResponse + fieldValues.get("pincode")
							+ " is not attached to any BizLog branches";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}

			} else {
				log.debug("is not a valid Pincode or not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode")
						+ " is not a valid Pincode or not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
			// PINCODE SHOULD BE A SERVICEABLE LOCATION OF THE RETAILER
			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("pincode")).singleResult();
			if (retailerServiceableLocations == null) {
				// Check if the pincode falls in the ODA Locations
				RetailerOdaLocations retailerOdaLocations = masterdataService.createRetailerOdaLocationsQuery()
						.retailerId(retailerId).pincode(fieldValues.get("pincode")).singleResult();
				if (retailerOdaLocations == null) {
					log.debug("is not a serviceable location of");
					errorResponse = errorResponse + fieldValues.get("pincode") + " is not a serviceable location of "
							+ retailerName + ".";
					restResponse.setSuccess(false);
					restResponse.setResponse(errorResponse);
				}
			}
		} else {
			log.debug("pincode column is Not There");
			errorResponse = errorResponse + "pincode column is not there";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// RETURN SELLER DETAILS AND PICK UP SELLER DETAILS ARE
		// MANDATORY AND SHOULD BE VALID FOR REPLACEMENT AND BUY
		// BACK

		// GET THE RETURN ADDRESS AND DETAILS USING THE RETURN
		// SELLER CODE AND RETURN SELLER PINCODE FROM THE TICKET

		// GET THE PICK UP ADDRESS AND DETAILS USING THE PICKUP
		// SELLER CODE AND PICK UP SELLER PINCODE FROM THE TICKET

		return restResponse;
	}

	// Validating Buy-Back Tickets

	public RestExternalResult validatingBB(String fieldValueJSON, Retailer retailer) {
		// EXCLUSIVELY FOR BUY BACK FLOW, ADVNACE EXCHANGE FLOW & DROP
		// OFF FLOW

		// Map<String, String> fieldValues = null;
		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		Map<String, String> fieldValues = null;
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);
		} catch (Exception e) {
			log.debug("Error in Fetching fieldValues : " + e);
		}
		RestExternalResult restResponse = new RestExternalResult();
		restResponse.setSuccess(true);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
		if (brand == null) {
			log.debug("is not a valid Brand");
			errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// VALIDATE CATEGORY
		// Newly Added Code
		ProductCategory productCategory = masterdataService.createProductCategoryQuery()
				.name(fieldValues.get("productCategory")).singleResult();

		if (productCategory != null) {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategoryCode = productCategory.getCode();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			// VALIDATE MASTER CATEGORY
			MasterCategory masterCategory = masterdataService.createMasterCategoryQuery().id(rlMasterCategoryId)
					.singleResult();
			String rlMasterCatProcess = "";
			if (masterCategory != null) {
				// GET THE PROCESS ASSOCIATED WITH THE MASTER CATEGORY
				rlMasterCatProcess = masterCategory.getProcessFlow();
			}

			if (!(rlMasterCatProcess.equalsIgnoreCase("IT_PRODUCTS")
					|| rlMasterCatProcess.equalsIgnoreCase("ELECTRONICS_FURNITURE"))) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse
						+ "Ticket is created under a wrong process. Close this and choose the correct process. \n";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		} else {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// End Here
		// ProductCategory productCategory =
		// masterdataService.createProductCategoryQuery()
		// .name(fieldValues.get("productCategory")).singleResult();
		if (productCategory == null) {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		} else {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
				log.debug("is not a valid Category for this product.");
				errorResponse = errorResponse + fieldValues.get("productCategory")
						+ " is not a valid Category for this product.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			// CATEGORY SHOULD BE A SERVICEABLE CATEGORY OF THE RETAILER
			RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
					.retailerId(retailerId).categoryId(rlProductCategoryId).singleResult();
			if (retailerCategories == null) {
				log.debug("is not a valid category of ");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid category of"
						+ retailerName + ".";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		}

		// PINCODE SHOULD BE VALID

		LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
				.pincode(fieldValues.get("pincode")).singleResult();
		if (locationPincodes != null) {
			String rlLocationId = locationPincodes.getLocationId();
			// GET LOCATION NAME AND OTHER DETAILS
			ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
					.singleResult();
			if (processLocation == null) {
				log.debug("is not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode") + " is not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		} else {
			log.debug("is not a valid Pincode or not attached to any BizLog branches");
			errorResponse = errorResponse + fieldValues.get("pincode")
					+ " is not a valid Pincode or not attached to any BizLog branches";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}

		// RETURN SELLER DETAILS AND PICK UP SELLER DETAILS ARE
		// MANDATORY AND SHOULD BE VALID FOR REPLACEMENT AND BUY
		// BACK

		// GET THE RETURN ADDRESS AND DETAILS USING THE RETURN
		// SELLER CODE AND RETURN SELLER PINCODE FROM THE TICKET

		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("returnSellerCode")).sellerPincode(fieldValues.get("returnSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations == null) {
			log.debug("Return Location details are invalid. Get the correct code and pincode.");
			errorResponse = errorResponse + "Return Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);

		}
		// GET THE PICK UP ADDRESS AND DETAILS USING THE PICKUP
		// SELLER CODE AND PICK UP SELLER PINCODE FROM THE TICKET
		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations2 = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("pickupSellerCode")).sellerPincode(fieldValues.get("pickupSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations2 == null) {
			log.debug("Pickup Location details are invalid. Get the correct code and pincode");
			errorResponse = errorResponse + "Pickup Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		return restResponse;

	}

	// Validating Advance Exchange Tickets

	public RestExternalResult validatingAE(String fieldValueJSON, Retailer retailer) {
		// EXCLUSIVELY FOR BUY BACK FLOW, ADVNACE EXCHANGE FLOW & DROP
		// OFF FLOW

		// Map<String, String> fieldValues = null;
		String retailerName = retailer.getName();
		String retailerId = retailer.getId();
		String errorResponse = "";
		Map<String, String> fieldValues = null;
		try {
			fieldValues = new ObjectMapper().readValue(URLDecoder.decode(fieldValueJSON), Map.class);
		} catch (Exception e) {
			log.debug("Error in Fetching fieldValues : " + e);
		}
		RestExternalResult restResponse = new RestExternalResult();
		restResponse.setSuccess(true);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
		if (brand == null) {
			log.debug("is not a valid Brand");
			errorResponse = errorResponse + fieldValues.get("brand") + " is not a valid Brand";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// VALIDATE CATEGORY
		// Newly Added Code
		ProductCategory productCategory = masterdataService.createProductCategoryQuery()
				.name(fieldValues.get("productCategory")).singleResult();

		if (productCategory != null) {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategoryCode = productCategory.getCode();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			// VALIDATE MASTER CATEGORY
			MasterCategory masterCategory = masterdataService.createMasterCategoryQuery().id(rlMasterCategoryId)
					.singleResult();
			String rlMasterCatProcess = "";
			if (masterCategory != null) {
				// GET THE PROCESS ASSOCIATED WITH THE MASTER CATEGORY
				rlMasterCatProcess = masterCategory.getProcessFlow();
			}

			if (!rlMasterCatProcess.equalsIgnoreCase("IT_PRODUCTS")) {
				log.debug("is not a valid Category.");
				errorResponse = errorResponse
						+ "Ticket is created under a wrong process. Close this and choose the correct process. \n";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		} else {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		// End Here
		// ProductCategory productCategory =
		// masterdataService.createProductCategoryQuery()
		// .name(fieldValues.get("productCategory")).singleResult();
		if (productCategory == null) {
			log.debug("is not a valid Category.");
			errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid Category.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		} else {
			String rlProductCategoryId = productCategory.getId();
			String rlProductCategory = productCategory.getName();
			String rlMasterCategoryId = productCategory.getMasterCategoryId();

			if (!rlProductCategory.equalsIgnoreCase(fieldValues.get("productCategory"))) {
				log.debug("is not a valid Category for this product.");
				errorResponse = errorResponse + fieldValues.get("productCategory")
						+ " is not a valid Category for this product.";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

			// CATEGORY SHOULD BE A SERVICEABLE CATEGORY OF THE RETAILER
			RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
					.retailerId(retailerId).categoryId(rlProductCategoryId).singleResult();
			if (retailerCategories == null) {
				log.debug("is not a valid category of ");
				errorResponse = errorResponse + fieldValues.get("productCategory") + " is not a valid category of"
						+ retailerName + ".";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}
		}

		// PINCODE SHOULD BE VALID

		LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
				.pincode(fieldValues.get("pincode")).singleResult();
		if (locationPincodes != null) {
			String rlLocationId = locationPincodes.getLocationId();
			// GET LOCATION NAME AND OTHER DETAILS
			ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(rlLocationId)
					.singleResult();
			if (processLocation == null) {
				log.debug("is not attached to any BizLog branches");
				errorResponse = errorResponse + fieldValues.get("pincode") + " is not attached to any BizLog branches";
				restResponse.setSuccess(false);
				restResponse.setResponse(errorResponse);
			}

		} else {
			log.debug("is not a valid Pincode or not attached to any BizLog branches");
			errorResponse = errorResponse + fieldValues.get("pincode")
					+ " is not a valid Pincode or not attached to any BizLog branches";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}

		// RETURN SELLER DETAILS AND PICK UP SELLER DETAILS ARE
		// MANDATORY AND SHOULD BE VALID FOR REPLACEMENT AND ADVANCE
		// EXCHANGE

		// GET THE RETURN ADDRESS AND DETAILS USING THE RETURN
		// SELLER CODE AND RETURN SELLER PINCODE FROM THE TICKET

		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("returnSellerCode")).sellerPincode(fieldValues.get("returnSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations == null) {
			log.debug("Return Location details are invalid. Get the correct code and pincode.");
			errorResponse = errorResponse + "Return Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);

		}
		// GET THE PICK UP ADDRESS AND DETAILS USING THE PICKUP
		// SELLER CODE AND PICK UP SELLER PINCODE FROM THE TICKET
		RetailerReturnOrPickupLocations retailerReturnOrPickupLocations2 = masterdataService
				.createRetailerReturnOrPickupLocationsQuery().retailerId(retailerId)
				.sellerCode(fieldValues.get("pickupSellerCode")).sellerPincode(fieldValues.get("pickupSellerPincode"))
				.singleResult();
		if (retailerReturnOrPickupLocations2 == null) {
			log.debug("Pickup Location details are invalid. Get the correct code and pincode");
			errorResponse = errorResponse + "Pickup Location details are invalid. Get the correct code and pincode.";
			restResponse.setSuccess(false);
			restResponse.setResponse(errorResponse);
		}
		return restResponse;

	}

	// Reading the Device IMEI INFO Excel-Sheet And Updating the IMEI-INVENTORY
	// Table.
	@RequestMapping(value = "/upload/imei/excel", method = RequestMethod.POST, produces = "application/json")
	public RestResponse uploadDeviceIMEI(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(required = true, value = "retailerId") String retailerId) {
		RestResponse restResponse = new RestResponse();
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}

			// String fileName = file.getName();
			Workbook workbook = null;
			try {
				workbook = WorkbookFactory.create(file.getInputStream());
			} catch (Exception e) {
				System.out.println("Error in Creating WorkBook");

			}
			Cell cell = null;
			Sheet sheet;
			int totalRowCount = 0;

			String record = null;
			int recordNo = 0;
			int insertCount = 0;
			DataFormatter dfmt = new DataFormatter();
			List<String> orderedFields = new ArrayList<String>();
			System.out.println("workbook.getNumberOfSheets() :" + workbook.getNumberOfSheets());
			for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

				sheet = workbook.getSheetAt(j);
				totalRowCount = sheet.getLastRowNum();
				System.out.println("totalRowCount : " + totalRowCount);
				Row firstRow = sheet.getRow(0);
				recordNo = 0;
				try {
					/* Header record */
					for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
						orderedFields.add(firstRow.getCell(fr).getStringCellValue());
						// System.out.println("first cell value :
						// "+firstRow.getCell(fr).getStringCellValue());
					}
				} catch (Exception hex) {
					System.out.println("Error in reading First Row");
				}
				System.out.println("totalRowCount : " + totalRowCount);
				for (int r = 1; r <= totalRowCount; r++) {
					MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getMasterdataService();

					ImeiInventory imeiInventory = masterdataService.newImeiInventory();
					Map<String, String> map = new HashMap<String, String>();

					recordNo++;
					Row row = null;
					if (sheet.getRow(r) != null) {
						row = sheet.getRow(r);
						System.out.println("row.getFirstCellNum() : " + row.getFirstCellNum() + " getLastCellNum :"
								+ row.getLastCellNum());
						try {
							for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
								cell = null;
								String val = "";
								cell = row.getCell(i);
								val = dfmt.formatCellValue(cell);
								if (val.isEmpty() || val == "") {
									val = "";
								}
								System.out.print(orderedFields.get(i));
								System.out.println(val);
								map.put(orderedFields.get(i), val);
							}
							try {
								List<ImeiInventory> imei = masterdataService.createImeiInventoryQuery()
										.imeiNo(map.get("imeiNo")).list();
								if (imei.size() == 0) {
									try {
										imeiInventory.setStatus(0);
										imeiInventory.setRetailerId(retailerId);
										if (!(map.get("brand").equals("") || map.get("brand").equals(null)))
											imeiInventory.setBrand(map.get("brand"));
										if (!(map.get("imeiNo").equals("") || map.get("imeiNo").equals(null)))
											imeiInventory.setImeiNo(map.get("imeiNo"));
										if (!(map.get("location").equals("") || map.get("location").equals(null)))
											imeiInventory.setLocation(map.get("location"));
										if (!(map.get("model").equals("") || map.get("model").equals(null)))
											imeiInventory.setModel(map.get("model"));
										if (!(map.get("year").equals("") || map.get("year").equals(null)))
											imeiInventory.setYear(map.get("year"));
										if (!(map.get("productValue").equals("")
												|| map.get("productValue").equals(null)))
											imeiInventory.setProductCost(map.get("productValue"));
										if (!(map.get("productCategory").equals("")
												|| map.get("productCategory").equals(null)))
											imeiInventory.setProductCategory(map.get("productCategory"));
									} catch (Exception e) {
										log.debug("Error in assigning value : " + e);
									}
									try {
										masterdataService.saveImeiInventory(imeiInventory);
										insertCount++;
									} catch (Exception e) {
										log.debug("Error in Updating IMEI Table" + e);
									}
								} else {
									System.out.println("IMEI No is Alredy Present" + imei.size());
								}
							} catch (Exception e) {
								System.out.println("Error in getting value from IMEI Table :" + e);
							}

						} catch (Exception inEx) {
							System.out.println("Error in Reading Each CELL Value" + inEx);
						}
					} else {
						// retval.getErrorMessages().add("Empty Records");
					}

				}
			}
			restResponse.setSuccess(true);
			restResponse.setMessage("Successfully uploaded " + insertCount + " Rows");
			restResponse.setResponse(insertCount);

		} catch (Exception ex) {
			restResponse.setSuccess(false);
			System.out.println("rtyuio " + ex);
		}
		return restResponse;

	}

	public static void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	// private static void buildTicket(Map<String, String> fieldValues,String
	// retailerId) {
	//
	// //Initializing all the objects/Variables required
	// MetadataService metadataService = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines.getDefaultProcessEngine()))
	// .getMetadataService();
	// RepositoryService repositoryService = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getRepositoryService();
	// FormService formService = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines.getDefaultProcessEngine()))
	// .getFormService();
	// MasterdataService masterdataService = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	//
	//
	// try {
	//
	// String dataset = "BizLog";
	// String processName = null;
	//
	//
	// List<RetailerProcessField> retailerProcessFields = masterdataService
	// .createRetailerProcessFieldQuery().retailerId(retailerId).list();
	// for (RetailerProcessField retailerProcessField : retailerProcessFields) {
	// if (!fieldValues.containsKey(retailerProcessField.getCanonicalField())) {
	// fieldValues.put(retailerProcessField.getCanonicalField(), "");
	// }
	// }
	//
	//
	// try {
	//
	// log.debug("Nature Of Complaint :- " +
	// fieldValues.get("natureOfComplaint"));
	// // String productCategory = "mobile";
	// if (fieldValues.containsKey("natureOfComplaint")
	// && fieldValues.get("natureOfComplaint").equalsIgnoreCase("E-Waste")) {
	// log.debug("Inside E-Waste if condition");
	// processName = "e_waste";
	// } else if (fieldValues.get("natureOfComplaint").indexOf("Bulk") != -1
	// || fieldValues.get("natureOfComplaint").indexOf("bulk") != -1) {
	// processName = "bulk_txn";
	// } else if (fieldValues.containsKey("natureOfComplaint")
	// && fieldValues.get("natureOfComplaint").equalsIgnoreCase("BITS")) {
	// processName = "bits_txn";
	// } else if (fieldValues.containsKey("natureOfComplaint")
	// &&
	// fieldValues.get("natureOfComplaint").equalsIgnoreCase("RepairStandBy")) {
	// processName = "repair_standby";
	// } else if (fieldValues.containsKey("natureOfComplaint")
	// && fieldValues.get("natureOfComplaint").equalsIgnoreCase("PND-BULK")) {
	// processName = "itpnd_bulk";
	// } else {
	// log.debug("Inside default else condition");
	// try {
	// String productCategory =
	// fieldValues.get("productCategory").toLowerCase();
	// log.debug(productCategory);
	// MasterCategoryResult masterCategoryResult =
	// com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
	// .getMasterCategory(productCategory);
	// processName = masterCategoryResult.getProcessFlow();
	// log.debug(processName);
	//
	// } catch (Exception e) {
	// log.debug("Exception in masterCategoryResult value :" + e);
	// }
	// }
	//
	// if (retailerId != null) {
	// Retailer retailerObj =
	// masterdataService.createRetailerQuery().id(retailerId)
	// .singleResult();
	// String retailer = retailerObj.getName();
	// fieldValues.put("retailer", retailer);
	// }
	//
	// // CHECKING FOR TECH EVAL FOR ADV EXCHENAGE
	// // AND PICKNDROP(1WAY)
	// /*
	// * if (fieldValues.containsKey(
	// * "natureOfComplaint")) { if
	// * (fieldValues.get("natureOfComplaint").
	// * equalsIgnoreCase("Advance Exchange") ||
	// * fieldValues.get("natureOfComplaint")
	// * .equalsIgnoreCase(
	// * "Pick And Drop (One Way)")) { if
	// * (fieldValues.containsKey(
	// * "TechEvalRequired") &&
	// * !(fieldValues.get("TechEvalRequired").
	// * isEmpty())) {
	// *
	// * } else { continue; } } }
	// */
	//
	// // UPDATING THE IMEI AND BARCODE INVENTORY
	// if (fieldValues.containsKey("IMEINoOfStandByDevice")
	// && !(fieldValues.get("IMEINoOfStandByDevice").isEmpty())
	// && fieldValues.containsKey("barcodeForStandByDevice")
	// && !(fieldValues.get("barcodeForStandByDevice").isEmpty())) {
	// try {
	// // ImeiInventory imeiInventory1 =
	// // masterdataService.newImeiInventory();
	// MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	// PackagingMaterialInventory packagingMaterialInventory =
	// masterdataService1
	// .createPackagingMaterialInventoryQuery()
	// .barcode(fieldValues.get("barcodeForStandByDevice")).status(0)
	// .singleResult();
	// ImeiInventory imeiInventory =
	// masterdataService.createImeiInventoryQuery()
	// .imeiNo(fieldValues.get("IMEINoOfStandByDevice")).status(0)
	// .singleResult();
	// if (imeiInventory != null && packagingMaterialInventory != null) {
	// packagingMaterialInventory.setStatus(1);
	// packagingMaterialInventory
	// .setImeiNo(fieldValues.get("IMEINoOfStandByDevice"));
	// imeiInventory.setStatus(1);
	// imeiInventory.setBarcode(fieldValues.get("barcodeForStandByDevice"));
	// try {
	// masterdataService
	// .savePackagingMaterialInventory(packagingMaterialInventory);
	// masterdataService.saveImeiInventory(imeiInventory);
	// } catch (Exception e) {
	// log.debug("Error in updating Imei or Packaging Material Inventory"
	// + e);
	// System.out.println(
	// "Error in updating Imei or Packaging Inventory" + e);
	// }
	// } else {
	// log.debug("IMEI or Packaging Is not Present to issued");
	// continue;
	// }
	// } catch (Exception e) {
	// log.debug("Error in calling Imei or Packaging Inventory" + e);
	// System.out.println("Error in calling Imei or Packaging Inventory" + e);
	// }
	// }
	// try {
	// log.debug("currentDsVersion(dataset) : "
	// + metadataService.getCurrentDsVersion(dataset));
	// log.debug("dataset(dataset):" + dataset);
	// log.debug("dataset :" + processName);
	// ProcessDeployment deployment =
	// metadataService.createProcessDeploymentQuery()
	// .dataset(dataset)
	// .dsVersion(metadataService.getCurrentDsVersion(dataset))
	// .name(processName).singleResult();
	// log.debug("deployment value :" + deployment);
	//
	// ProcessDefinition definition =
	// repositoryService.createProcessDefinitionQuery()
	// .deploymentId(deployment.getDeploymentId()).singleResult();
	//
	// log.debug("definition : " + definition);
	// FormFillerUtil formFillerUtil = new
	// FormFillerUtil(deployment.getDeploymentId(),
	// false);
	// GroupDescription groupDescription = null;
	// while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) !=
	// null) {
	// if (log.isDebugEnabled()) {
	// log.debug("Current groupDescription:" + groupDescription);
	// }
	//
	// if (groupDescription.isLastGroup()) {
	// break;
	// }
	// }
	//
	// ProcessInstance processInstance = submitStartForm(formService,
	// definition,
	// formFillerUtil);
	// System.out.println("processInstance.getProcessInstanceId() : "
	// + processInstance.getProcessInstanceId());
	// String processId = processInstance.getProcessInstanceId();
	// String taskId = "";
	//
	// // RETRIVING TASKID FOR ASSIGNMENT
	//
	// List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines
	// .getDefaultProcessEngine())).getTaskService().createTaskQuery()
	// .processInstanceId(processId).orderByTaskCreateTime().desc()
	// .list();
	// if (!tasks.isEmpty()) {
	// Task task = tasks.get(0);
	// taskId = task.getId();
	//
	// log.debug("taskId : " + taskId);
	// }
	//
	// RuntimeService runtimeService = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getRuntimeService();
	// String ticketNo = String
	// .valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
	// // Setting Pincode while
	// // processing/uploading a ticket
	//
	// if ((fieldValues.containsKey("pincode"))
	// && fieldValues.get("pincode") != null) {
	//
	// try {
	// LocationPincodes locationPincodes = masterdataService
	// .createLocationPincodesQuery()
	// .pincode(fieldValues.get("pincode")).singleResult();
	//
	// ProcessLocation processLocation = masterdataService
	// .createProcessLocationQuery()
	// .id(locationPincodes.getLocationId()).singleResult();
	//
	// runtimeService.setVariable(processId, "rlReportingCity",
	// processLocation.getCity());
	// log.debug(runtimeService.getVariable(processId, "rlReportingCity")
	// .toString());
	//
	// } catch (Exception e) {
	// log.debug("Error in putting value to reporting city" + e);
	// }
	//
	// // TODO Setting a Date
	//
	// }
	//
	// if (fieldValues.containsKey("ticketPriority")
	// && (fieldValues.get("ticketPriority").isEmpty())) {
	// log.debug("inside Priority");
	// // String ticketPriority = String
	// // .valueOf(runtimeService.getVariable(processId,
	// // "ticketPriority"));
	// // if(ticketPriority==null ||
	// // ticketPriority =="")
	// // {
	// runtimeService.setVariable(processId, "ticketPriority", "Medium");
	// }
	// // }
	// // Manually Setting Assignee
	// if (fieldValues.containsKey("pincode")
	// && !(fieldValues.get("pincode").isEmpty()) && ticketNo.contains("-")
	// && !((ticketNo).isEmpty())) {
	// String pincode = fieldValues.get("pincode");
	// log.debug("Ticket No: " + ticketNo);
	// System.out.println("Ticket No: " + ticketNo);
	// MasterdataService masterdataService2 = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	// MasterdataService masterdataService3 = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	// // runtimeService.getVariable(processId,
	// // "taskId");
	// // CoordinatorServicablePincodesQuery
	// // coordinatorServicablePincodes =
	// //
	// masterdataService2.createCoordinatorServicablePincodesQuery().pincode(pincode);
	// // System.out.println("coordinatorServicablePincodes
	// // COUNT
	// // :"+coordinatorServicablePincodes.count());
	// // System.out.println("coordinatorServicablePincodes:
	// // "+coordinatorServicablePincodes);
	// // ArrayList al = new
	// // ArrayList<>(coordinatorServicablePincodes.list());
	// // System.out.println("ALOIU :
	// // "+al.get(0));
	// log.debug("taskId From GetVariable : "
	// + runtimeService.getVariable(processId, "taskId"));
	// try {
	// if (masterdataService2.createCoordinatorServicablePincodesQuery()
	// .pincode(pincode) != null) {
	// List<CoordinatorServicablePincodes> list = masterdataService3
	// .createCoordinatorServicablePincodesQuery().pincode(pincode)
	// .list();
	// System.out
	// .println("CoordinatorServicablePincodes : " + list.size());
	// log.debug("CoordinatorServicablePincodes list :" + list);
	// if (list.size() >= 1) {
	// String coId = "";
	// for (int i = 0; i < list.size(); i++) {
	// MasterdataService masterdataService4 = ((RLogisticsProcessEngineImpl)
	// (ProcessEngines
	// .getDefaultProcessEngine())).getMasterdataService();
	// ProcessUser userList = masterdataService4
	// .createProcessUserQuery()
	// .email(list.get(i).getCoordinatorEmailId())
	// .status(1).singleResult();
	// if (userList != null) {
	// coId = String.valueOf(userList.getEmail());
	// log.debug("inside Assigmnent assignee is :" + coId);
	// TaskService taskService = ((RLogisticsProcessEngineImpl) (ProcessEngines
	// .getDefaultProcessEngine())).getTaskService();
	//
	// taskService.setAssignee(taskId, coId);
	// break;
	// }
	// }
	//
	// }
	// }
	// } catch (Exception e) {
	// log.debug("multiple record are there fot this Pincode!" + e);
	// }
	// }
	// // Till Here manual assignment
	// // DateFormat dateFormat = new
	// // SimpleDateFormat("dd/MM/yyyy
	// // hh:mm:ss");
	// // Date date = new Date();
	//
	// DateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	// Date date = new Date();
	// s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	// String tt = s.format(date);
	// DateTime dateTimeIndia = new DateTime(tt);
	// Date actDate = dateTimeIndia.toDate();
	// runtimeService.setVariable(processId, "ticketCreationDate", actDate);
	// log.debug("TicketCreation DATE" + String
	// .valueOf(runtimeService.getVariable(processId, "ticketCreationDate")));
	// System.out.println("ticketNo : " + ticketNo);
	//
	// // Below Code For No Of Ticket COmpleted
	// // and
	// // Non Completed On A Date.
	// // try {
	// // insertIntoOpenTickets(ticketNo);
	// // } catch (Exception e) {
	// // System.out.println("Exception in
	// // Calling
	// // insertIntoOpenTickets() :" + e);
	// // }
	// retval.getTicketNo().add(ticketNo);
	//
	// insertCount++;
	// } catch (Exception e) {
	// log.debug("Ecxeption in ProcessDeployment" + e);
	// }
	// } catch (Exception t) {
	// log.debug("Ecxeption in Updating All" + t);
	// ErrorObject errorObject = new ErrorObject();
	// if (t != null && t.getMessage() != null && !t.getMessage().contains("Cell
	// index")) {
	// errorObject.setRowNo(recordNo);
	// errorObject.setMessage("Ignoring line " + recordNo);
	// errorObject.setReason(t.getMessage());
	// retval.getErrorMessages().add(errorObject);
	// }
	//
	// }
	//
	// } catch (Exception inEx) {
	// ErrorObject errorObject = new ErrorObject();
	// if (inEx != null && inEx.getMessage() != null
	// && !inEx.getMessage().contains("Cell index")) {
	// errorObject.setRowNo(recordNo);
	// errorObject.setMessage("Ignoring line " + recordNo);
	// errorObject.setReason(inEx.getMessage());
	// retval.getErrorMessages().add(errorObject);
	// }
	// }
	// }

	private boolean RepairStandByPrimaryValidation(String queryJSON, RestExternalResult restResult, String retailerId) {

		/**
		 * productName model productCode brand identificationNo
		 */
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {
			value = checkForFieldsForReapairStandBy(fieldValues, value, finalErrorMeesage);

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;

	}

	private boolean checkForFieldsForReapairStandBy(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		// fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;
	}

	private boolean secondryValidationPND1W(Map<String, String> fieldValues, String retailerId, String apiToken,
			RestExternalResult restResult) {
		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("dropLocPincode"))) {
			errorMeesage.append(fieldValues.get("dropLocPincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateProductBrand()) {
			errorMeesage.append(fieldValues.get("brand") + " Brand is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;
	}
	public void addMobileProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {

		/**
		 * These value we get from retailer related to product "brand": "Apple",
		 * "productCategory": "mobile", "productName": "Iphone 7",
		 * "productCode": "iPhone 7", "model": "S",
		 */

		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}

				log.debug("22222222222222");

				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					/**
					 * =masterdataService.createProductCategoryQuery().name(
					 * "Mobile" THIS "Mobile" string should be same in
					 * RL_MD_PRODUCT_CATEGORY IN Production server ,dev or in
					 * test
					 */
					ProductCategory productCategory = masterdataService.createProductCategoryQuery().name("Mobile")
							.singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());// SUBCATEGORY ID
																// FROM
																// RL_MD_RETAILER_CATEGORIES
																// TABLE
					/**
					 * THIS PRODUCT CATEGORY SHOULD BE ADD IN
					 * RL_MD_RETAILER_CATEGORIES AND GET PRODUCT_id_ HERE
					 */
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());// Wrap
																		// Box
																		// 35x23x11
																		// from
																		// RL_MD_PACKAGING_TYPE
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Bubble Cover 9x6")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());// Bubble
																			// Cover
																			// 9x6
																			// from
																			// RL_MD_PACKAGING_TYPE
					product.setBrandId(newBrandId);// IT SHOULD BE BRAND ID FROM
													// RL_MD_BRAND
					/**
					 * THIS BRAND SHOULD BE ADD IN RL_MD_BRAND AND GET HERE THE
					 * BARND_ID_
					 */

					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");

					log.info("New Product Added Successfully");
					/**
					 * after add the product this product should be allow to
					 * this retailer this is the error message Status false
					 * iphone 7prem is not a valid Product Code of Demo
					 * Retailer.null
					 */
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}
	public String addNewBrand(MasterdataService masterdataService, JSONObject fieldValues) {
		Brand brand = masterdataService.newBrand();
		String brandId = String.valueOf(UUID.randomUUID());

		brand.setId(brandId);
		brand.setName(fieldValues.getString("brand").toUpperCase());
		brand.setCode(randomAlphaNumeric(20));
		// brand.setDescription("description");

		masterdataService.saveBrand(brand);
		log.debug("*************************New Brand is Added");

		return brandId;
	}



}
