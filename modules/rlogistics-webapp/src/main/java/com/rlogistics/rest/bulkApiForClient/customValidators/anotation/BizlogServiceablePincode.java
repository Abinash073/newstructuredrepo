package com.rlogistics.rest.bulkApiForClient.customValidators.anotation;

import com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor.BizlogServicablePincodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BizlogServicablePincodeValidator.class)
@Target({ElementType.METHOD,ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface BizlogServiceablePincode {
    String message() default "Pincode is not serviceable by Bizlog! ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
