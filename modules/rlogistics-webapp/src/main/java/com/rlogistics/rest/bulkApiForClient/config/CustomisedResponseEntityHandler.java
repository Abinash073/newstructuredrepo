package com.rlogistics.rest.bulkApiForClient.config;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.controller.BulkApiController;
import com.rlogistics.rest.bulkApiForClient.dto.ErrorResponse;
import com.rlogistics.rest.bulkApiForClient.dto.SuccessResponse;
import com.rlogistics.rest.bulkApiForClient.exceptions.UserNotValidException;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

import org.activiti.engine.ProcessEngines;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;

/**
 * This handles all the validation errors in bulk flow and builds a response and wrap them in a response entity and send them
 * back to client!
 * Only for bulk api.
 *
 * @author Adarsh
 */
@SuppressWarnings("unused")
@Slf4j
@var
@RestControllerAdvice(basePackageClasses = {BulkApiController.class})
public class CustomisedResponseEntityHandler extends ResponseEntityExceptionHandler implements ResponseBodyAdvice<Object> {
    private MasterdataService masterdataService;

//    @Autowired
//    public CustomisedResponseEntityHandler(MasterdataService masterdataService) {
//        this.masterdataService = masterdataService;
        public CustomisedResponseEntityHandler() {

        this.masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
		.getMasterdataService();
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    /**
     * Wraps the response to a DTO and then send it back as a response!
     *
     * @param res
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return {@See SuccessResponse}
     */
    @Override
    @ResponseBody
    public SuccessResponse beforeBodyWrite(Object res, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        var retailerId = serverHttpRequest.getHeaders().getFirst("retailerId");
        var retailerName = masterdataService.createRetailerQuery().id(retailerId).singleResult().getName();
        return SuccessResponse.builder().client(retailerName).data(res).build();
    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.debug("Ex -->" + ex);

        var errors = new HashMap<String, String>();
        for (var fieldError : ex.getBindingResult().getFieldErrors()
        ) {
            errors.put(fieldError.getField(), String.valueOf("[ " + fieldError.getRejectedValue() + " ]") + " > " + fieldError.getDefaultMessage());
        }
        for (ObjectError objectError : ex.getBindingResult().getGlobalErrors()
        ) {
            errors.put(objectError.getObjectName(), String.valueOf("[ " + objectError.getObjectName() + " ]") + " > " + objectError.getDefaultMessage());
        }
        var errorResponse =
                ErrorResponse
                        .builder()
                        .status(HttpStatus.BAD_REQUEST.value())
                        .details(errors)
                        .message("Field Validation Error ! Fix it or contact us at : techsupport@bizlog.in")
                        .client(masterdataService.createRetailerQuery().id(request.getHeader("retailerId")).singleResult().getName())
                        .build();

        return handleExceptionInternal(ex, errorResponse, headers, HttpStatus.resolve(errorResponse.getStatus()), request);
    }

    /**
     * Handles all user validation related exception and map them to proper response entity.
     *
     * @param ex
     * @param
     * @return {@See ErrorResponse}
     */
    @ResponseBody
    @ExceptionHandler({UserNotValidException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected ResponseEntity<ErrorResponse> handleUserAuthError(Exception ex, WebRequest webRequest) {

        var errorResponse =
                ErrorResponse
                        .builder()
                        .message(ex.getMessage())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .build();
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.resolve(errorResponse.getStatus()));
    }


    /**
     * Handles all Exceptions which are not already handled by {@Code ExceptionHandler}
     *
     * @param ex
     * @param webRequest
     * @return {@See ErrorResponse}
     */
    @ResponseBody
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ResponseEntity<ErrorResponse> handleAll(Exception ex, WebRequest webRequest) {
        log.debug("Error --> " + ex);
        final var errorResponse =
                ErrorResponse
                        .builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message("Duhh! Something is wrong in Server End.Please Contact support at : techsupport@bizlog.in")
                        .build();
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.resolve(errorResponse.getStatus()));
    }


}
