package com.rlogistics.rest;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.UUID;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailUtil;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.master.identity.FeDistance;
import com.rlogistics.master.identity.Payment;
import com.rlogistics.master.identity.PaymentDetails;
import com.rlogistics.master.identity.RetailerOther;
import com.rlogistics.master.identity.impl.cmd.CreatePaymentQueryCmd;
import com.rlogistics.master.identity.impl.cmd.CreatePaymentDetailsQueryCmd;
import com.rlogistics.master.identity.impl.cmd.SavePaymentDetailsCmd;
import com.rlogistics.master.identity.impl.cmd.SavePaymentCmd;
import com.rlogistics.master.identity.MasterdataService;

import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Mac;
import org.apache.commons.codec.binary.Base64;

@RestController
@RequestMapping("/payment/payout")
public class PaymentPayoutResource {
	public static Logger log = LoggerFactory.getLogger(PaymentPayoutResource.class);
	private String CASHFREE_ADD_BENEFICIARY = "/payout/v1/addBeneficiary";
	private String CASHFREE_FOR_FUND_TRANSFER = "/payout/v1/requestTransfer";
	private String CASHFREE_REMOVE_CENEFICIARY = "/payout/v1/removeBeneficiary";
	private String CASHFREE_CREATE_CASHGRAM = "/payout/v1/createCashgram";
	private String CASHFREE_GET_CASHGRAM_STATUS = "/payout/v1/getCashgramStatus";
	private String CASHFREE_REMOVE_CASHGRAM_ID = "/payout/v1/deactivateCashgram";

	private final String TYPE_ADD = "ADD";
	private final String TYPE_FUND_TRF = "FUNDTRF";
	private final String TYPE_REMOVE_BENEFICIARY = "REMOVEBENEFICIARY";
	private final String TYPE_GET_CASHGRAM_STATUS = "GETCASHGRAMSTATUS";
	private final String TYPE_CREATE_CASHGRAM = "CREATECASHGRAM";
	private final String TYPE_REMOVE_CASHGRAM = "REMOVECASHGRAM";
	private final String PAY_OUT = "PAYOUT";
	private final String BIZLOG_STATUS_SUCCESS = "SUCCESS";
	private final String BIZLOG_STATUS_FAILED = "FAILED";
	private final String BIZLOG_STATUS_PENDING = "PENDING";
	private final String BIZLOG_STATUS_LINK_SENT = "LINKSENT";

	@RequestMapping(value = "/v1/addbeneficiary", method = RequestMethod.POST, produces = "application/json")
	public RestResponse addBeneficiary(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		if (primaryValidation(reqJson, response, TYPE_ADD)) {
			callCashFreeAPIForAddingBeneficiary(reqJson, response);
		}

		return response;
	}

	@RequestMapping(value = "/v1/fundtransfer", method = RequestMethod.POST, produces = "application/json")
	public RestResponse fundTransfer(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		if (primaryValidation(reqJson, response, TYPE_FUND_TRF)) {
			callCashFreeAPIForFundTransfer(reqJson, response);
		}

		return response;
	}

	@RequestMapping(value = "/v1/removebeneficiary", method = RequestMethod.POST, produces = "application/json")
	public RestResponse removeBeneficiary(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		if (primaryValidation(reqJson, response, TYPE_REMOVE_BENEFICIARY)) {
			callCashFreeAPIForRemoveBeneficiary(reqJson, response);
		}

		return response;
	}

	@RequestMapping(value = "/v1/createcashgram", method = RequestMethod.POST, produces = "application/json")
	public RestResponse createCashgram(@RequestBody String reqJson) {

		RestResponse response = new RestResponse();
		String processId = "";
		try {
			processId = new JSONObject(reqJson).getString("processId");
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			Map<String, Object> variables = runtimeService.getVariables(processId);
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
					.retailerId(variables.get("rlRetailerId").toString()).singleResult();
			if (retailerOther.getPayout() == 1) {
				String cashGramRequest = getJsonForCreateCashGram(variables);
				if (primaryValidation(cashGramRequest, response, TYPE_CREATE_CASHGRAM)) {
					callCashFreeAPIForcreateCashgram(cashGramRequest, response, variables);
				}
			} else {
				response.setMessage("PayOut for this Retailer is not allowed.");
				response.setSuccess(false);
			}

		} catch (Exception e) {
			response.setMessage(e.getMessage() + "");
			response.setSuccess(false);
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "ERROR :CASHFREE_CREATE_CASHGRAM: EXCEPTION" + e);
		}

		return response;
	}

	@RequestMapping(value = "/v1/getcashgramstatus", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getCashGramStatus(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		if (primaryValidation(reqJson, response, TYPE_GET_CASHGRAM_STATUS)) {
			callCashFreeAPIForGetCashGramStatus(reqJson, response);
		}

		return response;
	}

	@RequestMapping(value = "/v1/removecashgramid", method = RequestMethod.POST, produces = "application/json")
	public RestResponse removeCashGramId(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		if (primaryValidation(reqJson, response, TYPE_REMOVE_CASHGRAM)) {
			callCashFreeAPIForRemoveCashGramId(reqJson, response);
		}

		return response;
	}

	// THIS IS FOR CASHGRAM STATUS UPDATE
	@RequestMapping(value = "/v1/cashGramNotify", method = RequestMethod.POST, produces = "application/json")
	public RestResponse cashGramNotify(@RequestParam(value = "utr", defaultValue = "") String utr,
			@RequestParam(value = "signature", defaultValue = "") String signature,
			@RequestParam(value = "eventTime", defaultValue = "") String eventTime,
			@RequestParam(value = "event", defaultValue = "") String event,
			@RequestParam(value = "cashgramId", defaultValue = "") String cashgramId,
			@RequestParam(value = "referenceId", defaultValue = "") String referenceId,
			@RequestParam(value = "currentBalance", defaultValue = "") String currentBalance) {
		RestResponse response = new RestResponse();
		RetailerApiCalls.saveToConsoleLogsTable("CASHFREECALL", " utr: " + utr + " eventtime: " + eventTime
				+ " event : " + event + " cashgramId: " + cashgramId + " referenceId: " + referenceId);
		String status = "";
		switch (event) {
		case "CASHGRAM_REDEEMED":
			status = "SUCCESS";
			break;
		// case "CASHGRAM_TRANSFER_REVERSAL":
		// break;
		case "CASHGRAM_EXPIRED":
			status = "EXPIRED";
			break;
		case "LOW_BALANCE_ALERT":
			sendLowBalanceAlert(currentBalance);
			break;
		}
		Map<String, String> map = new HashMap<>();
		map.put("utr", utr);
		map.put("eventTime", eventTime);
		map.put("event", event);
		map.put("cashgramId", cashgramId);
		map.put("referenceId", referenceId);
		Map<String, String> treeMap = new TreeMap<String, String>(map);
		String postData = "";
		for (String key : map.keySet()) {
			postData += map.get(key);
		}
		String data = generateSignature(PaymentAuth.CASHFREE_TEST_CLIENT_SECRET, postData);
		try {
			if (!status.equals("")) {
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();
				Payment payment1 = masterdataService.createPaymentQuery().externalReferenceId(cashgramId)
						.singleResult();

				CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
						.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
				Payment.PaymentQuery paymetQuery = commandExecutor.execute(new CreatePaymentQueryCmd());
				Payment payment = paymetQuery.externalReferenceId(cashgramId).singleResult();
				payment.setStatus(status);
				payment.setUpdateOn(getCurrentTimeStamp());
				commandExecutor.execute(new SavePaymentCmd(payment));

				PaymentDetails.PaymentDetailsQuery paymentDetailsQuery = commandExecutor
						.execute(new CreatePaymentDetailsQueryCmd());
				PaymentDetails paymentDetails = paymentDetailsQuery.cashgramId(cashgramId).singleResult();
				paymentDetails.setStatus(status);
				paymentDetails.setUpdateOn(getCurrentTimeStamp());
				commandExecutor.execute(new SavePaymentDetailsCmd(paymentDetails));
				response.setResponse(map);
				response.setSuccess(true);
				response.setMessage("Ticket Status Updated Successfully");
			}
		} catch (Exception e) {
			try {
				response.setResponse(map);
				response.setSuccess(false);
				response.setMessage("ERROR: " + e);
			} catch (Exception e1) {
				log.debug("ERROR:" + e + e1);
				response.setSuccess(false);
				response.setMessage("ERROR:" + e + e1);
				RetailerApiCalls.saveToConsoleLogsTable("CASHFREECALL", "ERROR WHILE UPDATING STATUS IN CASHGRAM:" + e);

			}
		}
		return response;
	}

	private void sendLowBalanceAlert(String currentBalance) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				ArrayList<String> emailArray = new ArrayList<>();
				emailArray.add("prem.nath@bizlog.in");
				emailArray.add("pavan.kumar@bizlog.in");
				emailArray.add("anand.sinha@bizlog.in");
				emailArray.add("accounts.ho@bizlog.in");
				emailArray.add("krishnanrajan@bizlog.in");
				for (int i = 0; i < emailArray.size(); i++) {
					try{
					EmailUtil.sendMail(emailArray.get(i), "CashFree Low Balance Alert",
							"Remaing balance is " + currentBalance);
					}catch(Exception e){
						log.debug(""+e);
					}
				}
			}
		}).start();

	}

	private boolean callCashFreeAPIForRemoveCashGramId(String reqJson, RestResponse response) {
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_REMOVE_CASHGRAM_ID;
			String token = new PaymentAuth().getCashFreeToken();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);

			httppost.setEntity(new StringEntity(reqJson, "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				response.setMessage(rawJson.getString("message"));
				rtnvalue = true;
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_REMOVE_CASHGRAM_ID: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_REMOVE_CASHGRAM_ID: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private boolean callCashFreeAPIForGetCashGramStatus(String reqJson, RestResponse response) {
		// THIS IS GET REQUEST
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_GET_CASHGRAM_STATUS;
			String token = new PaymentAuth().getCashFreeToken();

			URIBuilder builder = new URIBuilder();
			builder.setPath(url).setParameter("cashgramId", new JSONObject(reqJson).getString("cashgramId"));
			URI uri = builder.build();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(uri);
			httpGet.addHeader("Content-Type", "application/json");
			httpGet.addHeader("Authorization", "Bearer " + token);

			CloseableHttpResponse httpResponse = httpclient.execute(httpGet);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				response.setMessage(rawJson.getString("message"));
				response.setResponse(rawJson.getString("data").toString());
				rtnvalue = true;
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_GET_CASHGRAM_STATUS: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_GET_CASHGRAM_STATUS: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private boolean callCashFreeAPIForcreateCashgram(String reqJson, RestResponse response,
			Map<String, Object> variables) {
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_CREATE_CASHGRAM;
			String token = new PaymentAuth().getCashFreeToken();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);

			httppost.setEntity(new StringEntity(reqJson, "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				// response.setMessage(rawJson.getString("message"));
				response.setMessage("Payment Link is Sent to Customer Mobile/Mail.");
				response.setResponse(rawJson.toString());
				rtnvalue = true;
				saveToDBCreateCashGram(reqJson, rawResponse, variables);
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_CREATE_CASHGRAM: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_CREATE_CASHGRAM: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private void saveToDBCreateCashGram(String requestJson, String responseJson, Map<String, Object> variables)
			throws Exception {
		JSONObject reqJson = new JSONObject(requestJson);
		JSONObject resJson = new JSONObject(responseJson);

		String cashGramRefId = reqJson.getString("cashgramId");
		String amount = reqJson.getString("amount");
		String phone = reqJson.getString("phone");
		String status = BIZLOG_STATUS_LINK_SENT;
		String ticketNo = variables.get("rlTicketNo").toString();
		String processId = variables.get("rlProcessId").toString();
		String retailerId = variables.get("rlRetailerId").toString();
		String location = variables.get("rlLocationName").toString();
		String paymentType = PAY_OUT;
		String remarks = reqJson.getString("remarks");

		String trfId = resJson.getJSONObject("data").getString("referenceId");
		String name = reqJson.getString("name");
		String email = reqJson.getString("email");
		String payoutLink = resJson.getJSONObject("data").getString("cashgramLink");

		new Thread(new Runnable() {

			@Override
			public void run() {
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();

				Payment payment = masterdataService.newPayment();
				UUID id = UUID.randomUUID();
				payment.setId(String.valueOf(id));
				payment.setAmount(amount);
				payment.setExternalReferenceId(cashGramRefId);
				payment.setPaymentProvider("");
				payment.setPaymentRequest(requestJson);
				payment.setPaymentResponse(responseJson);
				payment.setPaymentType(paymentType);
				payment.setPhone(phone);
				payment.setProcessId(processId);
				payment.setRemarks(remarks);
				payment.setRetailerId(retailerId);
				payment.setStatus(status);
				payment.setTicketNo(ticketNo);
				payment.setCreatedOn(getCurrentTimeStamp());
				payment.setLocation(location);
				masterdataService.savePayment(payment);

				PaymentDetails paymentDetails = masterdataService.newPaymentDetails();
				UUID id1 = UUID.randomUUID();
				paymentDetails.setId(String.valueOf(id1));
				paymentDetails.setAcknowledged("");
				paymentDetails.setAmount(amount);
				paymentDetails.setCashfreeReferenceId(trfId);
				paymentDetails.setCashgramId(cashGramRefId);
				paymentDetails.setCreatedOn(getCurrentTimeStamp());
				paymentDetails.setEmail(email);
				paymentDetails.setName(name);
				paymentDetails.setPayoutLink(payoutLink);
				paymentDetails.setPhone(phone);
				paymentDetails.setStatus(status);
				paymentDetails.setTicketNo(ticketNo);
				paymentDetails.setTransferId("");
				paymentDetails.setUtr("");
				masterdataService.savePaymentDetails(paymentDetails);

			}
		}).start();

	}

	private boolean callCashFreeAPIForRemoveBeneficiary(String reqJson, RestResponse response) {
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_REMOVE_CENEFICIARY;
			String token = new PaymentAuth().getCashFreeToken();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);

			httppost.setEntity(new StringEntity(reqJson, "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				response.setMessage(rawJson.getString("message"));
				rtnvalue = true;
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_REMOVE_CENEFICIARY: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_REMOVE_CENEFICIARY: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private boolean callCashFreeAPIForAddingBeneficiary(String reqJson, RestResponse response) {
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_ADD_BENEFICIARY;
			String token = new PaymentAuth().getCashFreeToken();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);

			httppost.setEntity(new StringEntity(reqJson, "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				response.setMessage(rawJson.getString("message"));
				rtnvalue = true;
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_ADD_BENEFICIARY: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_ADD_BENEFICIARY: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private boolean callCashFreeAPIForFundTransfer(String reqJson, RestResponse response) {
		boolean rtnvalue = true;

		try {
			String url = PaymentAuth.getCashFreeBaseUrl() + CASHFREE_FOR_FUND_TRANSFER;
			String token = new PaymentAuth().getCashFreeToken();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("Content-Type", "application/json");
			httppost.addHeader("Authorization", "Bearer " + token);

			httppost.setEntity(new StringEntity(reqJson, "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			JSONObject rawJson = new JSONObject(rawResponse.toString());
			if (rawJson.getString("subCode").toString().equals("200")
					&& rawJson.getString("status").toString().equals("SUCCESS")) {
				response.setMessage(rawJson.getString("message"));
				rtnvalue = true;
			} else {
				rtnvalue = false;
				response.setMessage(rawJson.getString("message"));

			}
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CASHFREE_FOR_FUND_TRANSFER: REQ: " + reqJson + " RESPONSE: " + rawResponse);

		} catch (Exception exception) {
			rtnvalue = false;
			response.setMessage(exception.getMessage());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"ERROR :CASHFREE_FOR_FUND_TRANSFER: REQ: " + reqJson + " RESPONSE: " + exception);
		}

		response.setSuccess(rtnvalue);
		return rtnvalue;
	}

	private boolean primaryValidation(String reqJson, RestResponse response, String type) {
		StringBuilder errorMeesage = new StringBuilder("");
		boolean rtnvalue = true;
		JSONObject fieldValues = null;
		try {
			fieldValues = new JSONObject(reqJson);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		ArrayList<String> fV = new ArrayList<>();
		switch (type) {
		case TYPE_ADD:
			fV.add("beneId");
			fV.add("name");
			fV.add("email");
			fV.add("phone");
			fV.add("bankAccount");
			fV.add("ifsc");
			fV.add("address1");
			// fV.add("address2");
			fV.add("city");
			fV.add("state");
			fV.add("pincode");
			// fV.add("vpa");
			fV.add("pincode");
			break;
		case TYPE_FUND_TRF:
			fV.add("beneId");
			fV.add("amount");
			fV.add("transferId");
			// fV.add("transferMode");// banktransfer by default. Allowed values
			// are banktransfer, upi and paytm.
			// fV.add("remarks");
			break;
		case TYPE_REMOVE_BENEFICIARY:
			fV.add("beneId");
			break;
		case TYPE_GET_CASHGRAM_STATUS:
			fV.add("cashgramId");
			break;
		case TYPE_CREATE_CASHGRAM:
			fV.add("cashgramId");
			fV.add("amount");
			fV.add("name");
			// fV.add("email");
			fV.add("phone");
			fV.add("linkExpiry");
			// fV.add("remarks");
			// fV.add("notifyCustomer");
			break;
		case TYPE_REMOVE_CASHGRAM:
			fV.add("cashgramId");
			break;

		default:
			break;
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					rtnvalue = false;
					errorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				errorMeesage.append(field + " field is missing, ");
				rtnvalue = false;
			}
		}

		response.setSuccess(rtnvalue);
		response.setMessage(errorMeesage.toString());
		return rtnvalue;
	}

	public String generateSignature(String clientSecret, String data) {
		String hash = null;
		try {
			String secret = clientSecret;
			String message = data;
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			hash = Base64.encodeBase64String(sha256_HMAC.doFinal(message.getBytes()));
		} catch (Exception e) {
			// Log it
		}
		return hash;
	}

	public static String getCurrentDate() {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sd = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

	private String getJsonForCreateCashGram(Map<String, Object> variables) throws Exception {
		String ticketNo = variables.get("rlTicketNo").toString();
		String amount = "";
		String remarks = "";

		try {
			amount = variables.get("amountToBePaid").toString();
		} catch (Exception e) {
			try {
				amount = variables.get("rlValueOffered").toString();
			} catch (Exception e2) {
				// try {
				// amount = variables.get("maxValueToBeOffered").toString();
				// } catch (Exception ew) {
				throw new Exception("Amount not found");
				// }
			}
		}
		try {
			remarks = variables.get("retailer").toString();
		} catch (Exception e) {

		}

		String emailId = "";
		try {
			emailId = variables.get("emailId").toString();
		} catch (Exception e) {
		}
		String consumerName = variables.get("consumerName").toString();
		String phone = variables.get("telephoneNumber").toString();

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("cashgramId", ticketNo);
		jsonObject.put("amount", amount);
		jsonObject.put("name", consumerName);
		jsonObject.put("email", emailId);
		jsonObject.put("phone", phone);
		jsonObject.put("linkExpiry", getCurrentDate());
		jsonObject.put("remarks", remarks);
		jsonObject.put("notifyCustomer", 1);
		return jsonObject.toString();
	}

}
