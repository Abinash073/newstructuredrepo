package com.rlogistics.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;

@RestController
public class JobResource extends RLogisticsResource{
	private static Logger log = LoggerFactory.getLogger(TaskResource.class);

	@RequestMapping(value = "/job/{jobId}/command/execute",produces = "application/json")
	JobExecutionResult executeJob(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("jobId") String jobId){
		JobExecutionResult retval = new JobExecutionResult();
		
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return retval;}
			
			ManagementService managementService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getManagementService();
			
			managementService.executeJob(jobId);

			retval.setSuccess(true);
		} catch(Exception ex){
			log.error("Exception during executeJob", ex);
			httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		
		return retval; 
	}

}
