package com.rlogistics.rest;

import java.util.Map;
/*
 * create by prem 21-11-2018 for fields validation
 */

public interface TicketFieldsValidationInter {
	boolean validateReatilerCategory();

	boolean validateRetailerId();

	boolean validateRetailerAPIToken(String retailerApiToken);

	boolean validateDropPincode();

	boolean validatePickupPincode();

	boolean validateBizlogServiceAbleLocation(String pincode);

	boolean validateProductBrand();

	boolean validateProductDetails();

	boolean validateRetailerNatureOfComplaint();

	String getRetailerCode();

	boolean checkRepairStandByDeviceIMEI(String repairStandByIMEI, String pincode);

	boolean checkRetailerCode(String retailerCode, String retailerPincode);
	
	boolean validateRetailerPincode(String pincode);

}
