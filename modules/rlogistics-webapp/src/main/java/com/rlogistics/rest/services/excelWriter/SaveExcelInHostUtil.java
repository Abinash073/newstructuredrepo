package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RLogisticsAttachment;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


/**
 * Utility class to save Excel file in Host as an bite array
 * use this store any kind of excel file
 */
@Service
@Slf4j
@Primary
public class SaveExcelInHostUtil {
    private static final String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private MasterdataService masterdataService;
    private CommandExecutor commandExecutor;

    @Autowired
    public SaveExcelInHostUtil(MasterdataService masterdataService, CommandExecutor commandExecutor) {
        this.masterdataService = masterdataService;
        this.commandExecutor = commandExecutor;
    }

    /**
     * Saves a specific byte array in a specific location inside host and
     * location defined by all the keys
     *
     * @param key1 path
     * @param key2 path
     * @param key3 path
     * @param name name of the File
     * @param data data in Byte Array
     */
    public void saveExcel(String key1, String key2, String key3, String name, byte[] data) {
        RLogisticsAttachment attachment = null;
        if (masterdataService.createRLogisticsAttachmentQuery().key1(key1).key2(key2).key3(key3)
                .singleResult() != null) {
            attachment = masterdataService.createRLogisticsAttachmentQuery().key1(key1).key2(key2).key3(key3)
                    .singleResult();
        }
        if (attachment == null) {
            RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();


            newAttachment.setKey1(key1);
            newAttachment.setKey2(key2);
            newAttachment.setKey3(key3);
            newAttachment.setContentType(contentType);
            newAttachment.setName(name);


            commandExecutor.execute(commandContext -> {
                newAttachment.setContent(data);
                masterdataService.saveRLogisticsAttachment(newAttachment);
                return true;
            });

        } else {
            masterdataService.deleteRLogisticsAttachment(attachment.getId());

            RLogisticsAttachment newAttachment = masterdataService.newRLogisticsAttachment();

            newAttachment.setKey1(key1);
            newAttachment.setKey2(key2);
            newAttachment.setKey3(key3);
            newAttachment.setContentType(contentType);
            newAttachment.setName(name);
            log.debug("key value attachment is NOT null here" + newAttachment.getKey3());


            commandExecutor.execute(commandContext -> {
                newAttachment.setContent(data);
                masterdataService.saveRLogisticsAttachment(newAttachment);
                return true;
            });

        }
    }
}

