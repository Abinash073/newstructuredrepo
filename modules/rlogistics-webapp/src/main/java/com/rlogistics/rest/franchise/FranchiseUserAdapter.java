package com.rlogistics.rest.franchise;

import com.rlogistics.master.identity.FranchiseFe;
import com.rlogistics.ui.login.UIUser;

public class FranchiseUserAdapter implements UIUser {
    protected FranchiseFe franchiseFe;

    public FranchiseUserAdapter(FranchiseFe franchiseFe){
        this.franchiseFe = franchiseFe;
    }

    public FranchiseFe getFranchiseFe() {
        return franchiseFe;
    }

    @Override
    public String getEmail() {
        return franchiseFe.getEmailId();
    }

    @Override
    public String getName() {
        return franchiseFe.getFirstName();
    }

    @Override
    public String getId() {
        return franchiseFe.getId();
    }

    @Override
    public boolean isAdmin() {
        return false;
    }

    @Override
    public String getRoleCode() {
        return null;
    }

    @Override
    public String getLocationId() {
        return null;
    }

    @Override
    public String getRetailerId() {
        return null;
    }

    @Override
    public String getNumber() {
        return null;
    }
}
