package com.rlogistics.rest.bulkApiForClient.customValidators.anotation;

import com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor.RetailerIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = RetailerIdValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
public @interface RetailerId {
    String message() default "Retailer id invalid!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
