package com.rlogistics.rest.services;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.rest.FormFillerUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Creates Ticket for any kind of flow
 * @author Adarsh
 */
@Slf4j
@Service

public class TicketCreationService {
    /**
     * Only for activity method call for ticket creation
     * as beans are autowiring is getting failed(Giving NPE)
     */

    MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();

    MetadataService metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();

    RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();

    FormService formService = ProcessEngines.getDefaultProcessEngine().getFormService();

    private static final String dataset = "BizLog";

    /**
     * Creates activiti ticket for all the flows
     * @param retailerId Retailer id
     * @param data Data that requires to be there for the ticket
     * @param processName Flow Name
     * @return Process instance
     */
    public ProcessInstance createTicket(String retailerId, List<Map<String, String>> data, String processName) {
        Retailer retailer = masterdataService
                .createRetailerQuery()
                .id(retailerId)
                .singleResult();

        data.get(0).put("retailer", retailer.getName());

        log.debug(metadataService.getCurrentDsVersion(dataset) + " - " + dataset + " - " + processName);

        ProcessDeployment processDeployment =
                metadataService
                        .createProcessDeploymentQuery()
                        .dataset(dataset)
                        .dsVersion(metadataService.getCurrentDsVersion(dataset))
                        .name(processName)
                        .singleResult();

        log.debug(processDeployment.getName());

        ProcessDefinition processDefinition =
                repositoryService
                        .createProcessDefinitionQuery()
                        .deploymentId(processDeployment.getDeploymentId())
                        .singleResult();

        FormFillerUtil fillerUtil = new FormFillerUtil(processDeployment.getDeploymentId(), false);


        ProcessInstance processInstance = (ProcessInstance) fillerUtil
                .submitForm(
                        (FormFillerUtil.FormSubmissionAction<ProcessInstance>) data1 ->
                                formService
                                        .submitStartFormData(processDefinition.getId(), data.get(0))
                );

        return processInstance;
    }
}