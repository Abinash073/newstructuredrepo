package com.rlogistics.rest.repository;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.TicketImages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repo to get the images
 * @author Adarsh
 */
@Repository
public class BulkImageRepository {

    private MasterdataService masterdataService;

    @Autowired
    public BulkImageRepository(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    /**
     *
     * Get List of Images Dto based on Imei
     * @param imei Imei Number
     * @return List of DTO
     */
    public List<TicketImages> getImages(String imei) {
        return masterdataService.createTicketImagesQuery().procInstId(imei).list();
    }
}
