package com.rlogistics.rest.services.helper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseForExcel {
    @JsonProperty("process_id")
    private String processId;

    @JsonProperty("data")
    private List<ResponseWithPhysicalEvalData> data = new ArrayList<>();
}
