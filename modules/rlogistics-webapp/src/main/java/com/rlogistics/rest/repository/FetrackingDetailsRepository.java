package com.rlogistics.rest.repository;


import com.rlogistics.customqueries.FeDisatnceCustomQueriesMapper;
import com.rlogistics.customqueries.FeDistanceReportData;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repo to get Distance details of FE
 * @author Adarsh
 */
@Repository
public class FetrackingDetailsRepository {

    private ProcessEngineConfigurationImpl configuration;

    @Autowired
    public FetrackingDetailsRepository(ProcessEngineConfigurationImpl configuration) {
        this.configuration = configuration;
    }

    /**
     * Method to fetch Distance Details of FE
     * @param startDate Start Date
     * @param endDate End Date
     * @param city City of FE
     * @param feName Fe name
     * @return List of DTO containing Distance Data of FE
     */
    public synchronized List<FeDistanceReportData> getFedistanceDetails(String startDate, String endDate, String city, String feName) {
        try (SqlSession session = configuration.getSqlSessionFactory().openSession()) {
            FeDisatnceCustomQueriesMapper mapper = session.getMapper(FeDisatnceCustomQueriesMapper.class);
            return mapper.getFeDistance(feName, startDate, endDate, city);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
