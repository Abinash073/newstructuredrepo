package com.rlogistics.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

public class ProcessResultWithVariables {
	
	private ProcessInstanceResult process;
	private Map<String, Object> variables = new HashMap<>();
	private TaskResult task;
	private String jobId;
	private boolean completed = false;
	private List<TaskResult> completedTaskList;
	private Date processEndTime;
    private Date createdDate;
	public Date getProcessEndTime() {
		return processEndTime;
	}

	public ProcessResultWithVariables(Date createdDate) {
		super();
		this.createdDate = createdDate;
	}

	public ProcessResultWithVariables() {
		super();
	}

	public void setProcessEndTime(Date processEndTime) {
		this.processEndTime = processEndTime;
	}

	public List<TaskResult> getCompletedTaskList() {
		return completedTaskList;
	}

	public void setCompletedTaskList(List<TaskResult> completedTaskList) {
		this.completedTaskList = completedTaskList;
	}
	
	public ProcessResultWithVariables(ProcessInstance processInstance,Task task, Map<String,Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		setVariables(variables);
		this.setTask(new TaskResult(task));
	}
	public ProcessResultWithVariables(ProcessInstance processInstance,Task task, Map<String,Object> variables, Date createdDate) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		setVariables(variables);
		this.setTask(new TaskResult(task));
		this.createdDate = createdDate;
	}

	public ProcessResultWithVariables(ProcessInstance processInstance, String jobId, Map<String, Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		this.setJobId(jobId);
		setVariables(variables);
	}

	public ProcessResultWithVariables(HistoricProcessInstance processInstance, Map<String,Object> variables) {
		this.setProcess(new ProcessInstanceResult(processInstance));
		setVariables(variables);
	}
	
	public TaskResult getTask() {
		return task;
	}

	public void setTask(TaskResult task) {
		this.task = task;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		
		this.variables.clear();
		
		for(String variable:variables.keySet()){
			Object value = variables.get(variable);
			
			if(value instanceof Date){
				value = ((Date)value).getTime();
			}
			
			this.variables.put(variable, value);
		}
	}

	public ProcessInstanceResult getProcess() {
		return process;
	}

	public void setProcess(ProcessInstanceResult process) {
		this.process = process;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public boolean isCompleted(){
		return completed;
	}
	
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
