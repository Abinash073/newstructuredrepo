package com.rlogistics.rest.franchise;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.master.RestResponse;
import com.rlogistics.master.identity.FranchiseFe;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.rest.ProcessUserData;
import com.rlogistics.ui.login.UIUser;
import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
public class FranchiseUserResource {

    private static Logger log = LoggerFactory.getLogger(FranchiseUserResource.class);
    private static String APP_LINK = "http://ec2-54-187-161-0.us-west-2.compute.amazonaws.com:9090/rlogistics-execution/#/reset-password/";

    @RequestMapping(value = "/save/franchise-fe", method = RequestMethod.POST)
    public RestResponse saveFranchiseUser(@RequestParam("franchiseUser") String queryJson, HttpServletResponse response) {

        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
        masterdataService.createFlBarcodeEntityQuery();

        RestResponse restResponse = new RestResponse();
        Map<String, String> userObj;

        try {
            userObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
            log.debug(userObj + "======================================================");
            FranchiseFe franchiseFe = masterdataService.newFranchiseFe();
            ProcessUser processUser = masterdataService.newProcessUser();
            String password = userObj.get("password");
            String encrypPass = AuthUtil.cryptWithMD5(password);
            if (userObj.containsKey("id")) {
                franchiseFe = masterdataService.createFranchiseFeQuery().id(userObj.get("id")).singleResult();
                encrypPass = franchiseFe.getPassword();
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
                        + new SimpleDateFormat("yyyyMMddHHmm").format(calendar.getTime());
                franchiseFe.setId(id);
            }
            franchiseFe.setFirstName(userObj.get("firstName"));
            processUser.setFirstName(userObj.get("firstName"));

            if (userObj.containsKey("emailId")) {
                String email = userObj.get("emailId");
                if ((masterdataService.createFranchiseFeQuery().emailId(email).count() >= 1)
                        || (masterdataService.createProcessUserQuery().email(email).count() >= 1)) {
                    restResponse.setMessage("Email Already Exists");
                    throw new Exception("Email Id Already Exists");
                } else {
                    franchiseFe.setEmailId(userObj.get("emailId"));
                    processUser.setEmail(userObj.get("emailId"));
                }
            } else {
                restResponse.setMessage("Email Id is mandatory");
            }

            if (userObj.containsKey("lastName")) {
                franchiseFe.setLastName(userObj.get("lastName"));
            }
            if (userObj.containsKey("contactNumber")) {
                franchiseFe.setContactNumber(userObj.get("contactNumber"));
                processUser.setPhone(userObj.get("contactNumber"));
            }
            if (userObj.containsKey("status")) {

                log.debug("====================" + userObj);
                log.debug("=========STATUS========== " + String.valueOf(userObj.get("status")));

//                if (userObj.get("status").equals("Active")) {
//                    int statusCode = 0;
//                    franchiseFe.setStatus(statusCode);
//                    processUser.setStatus(1);
//                }
                int status = Integer.valueOf(String.valueOf(userObj.get("status")));
//                franchiseFe.setStatus(Integer.valueOf(String.valueOf(userObj.get("status"))));
//                processUser.setStatus(Integer.valueOf(String.valueOf(userObj.get("status"))));
//                franchiseFe.setStatus(status);
//                processUser.setStatus(status);
                franchiseFe.setStatus(1);
                processUser.setStatus(1);
            } else {
                franchiseFe.setStatus(1);
                processUser.setStatus(1);
            }

            processUser.setRoleCode("field_engineer");
            processUser.setLocationId("5067");

//            franchiseFe.setFranchiseId("abc");
//            franchiseFe.setAgencyId("xyz");
            franchiseFe.setFranchiseId(userObj.get("franchiseId"));
            franchiseFe.setAgencyId(userObj.get("agencyId"));

            franchiseFe.setPassword(encrypPass);
            processUser.setPassword(encrypPass);
            masterdataService.saveFranchiseFe(franchiseFe);
            masterdataService.saveProcessUser(processUser);
        } catch (Exception e) {

            log.debug("==========================================================");
            log.debug(e.getMessage());
            e.printStackTrace();
            restResponse.setMessage("User Exists");
            return restResponse;
        }

        restResponse.setMessage("User Added");
        restResponse.setResponse(userObj);
        return restResponse;
    }


    @PostMapping(value = "/update/franchise-fe")
    public RestResponse updateFranchiseUser(@RequestParam("franchiseUser") String queryJson) {
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getMasterdataService();
        masterdataService.createFlBarcodeEntityQuery();

        RestResponse restResponse = new RestResponse();
        Map<String, String> userObj;


        try {
            userObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson), Map.class);
            log.debug(userObj + "===========================USER Object===========================");
            FranchiseFe savedFe = masterdataService.newFranchiseFe();
            ProcessUser processUser = masterdataService.newProcessUser();
            String password = userObj.get("password");
            String encrypPass = AuthUtil.cryptWithMD5(password);
            if (userObj.containsKey("id")) {
                savedFe = masterdataService.createFranchiseFeQuery().id(userObj.get("id")).singleResult();
//                processUser = masterdataService.createProcessUserQuery().id(userObj.get("id")).singleResult();
//                processUser = masterdataService.createProcessUserQuery().email(userObj.get("emailId")).singleResult();
                encrypPass = savedFe.getPassword();
            } else if (userObj.containsKey("email")) {
                savedFe = masterdataService.createFranchiseFeQuery().id(userObj.get("id")).singleResult();
                encrypPass = savedFe.getPassword();
            }
//            savedFe = masterdataService.createFranchiseFeQuery().id(userObj.get("id")).singleResult();
            log.debug("=========================== " + userObj.get("emailId"));
            processUser = masterdataService.createProcessUserQuery().email(userObj.get("emailId")).singleResult();

            log.debug("Franchise Fe :" + savedFe);
            log.debug("=====================================================================");
            log.debug("Process User : " + processUser);

            // Email id validation
            if (userObj.containsKey("emailId")) {
                String email = userObj.get("emailId");
                if (email.isEmpty()) {
                    restResponse.setMessage("Email can not be empty");
                    throw new Exception("Email Can not be empty");
                }
                if (savedFe.getEmailId().equals(email)) {
                    savedFe.setEmailId(savedFe.getEmailId());
                } else {
                    restResponse.setMessage("Email Id Can not be changed");
                    throw new Exception("Email Id can not be changed");
                }
            } else {
                restResponse.setMessage("Email Id is mandatory");
            }

            // First Name validation
            if (userObj.containsKey("firstName")) {
                String firstName = userObj.get("firstName");

                if (firstName.isEmpty()) {
                    restResponse.setMessage("First Name can not be empty");
                    throw new Exception("First Name Can not be empty");
                }

                if (savedFe.getFirstName().equals(firstName)) {
                    savedFe.setFirstName(savedFe.getFirstName());
                } else {
                    savedFe.setFirstName(userObj.get("firstName"));
                    processUser.setFirstName(userObj.get("firstName"));
                }
            } else {
                restResponse.setMessage("First Name is mandatory");
            }


            // Last Name validation
            if (userObj.containsKey("lastName")) {
                String lastName = userObj.get("lastName");

                if (lastName.isEmpty()) {
                    savedFe.setLastName(savedFe.getLastName());
                } else {
                    savedFe.setLastName(userObj.get("lastName"));
                }
            }


            if (userObj.containsKey("contactNumber")) {
                String contactNo = userObj.get("contactNumber");

                if (contactNo.isEmpty()) {
                    savedFe.setContactNumber(savedFe.getContactNumber());
                    restResponse.setMessage("Contact number is mandatory");
                    throw new Exception("Contact number is mandatory");
                } else {
                    savedFe.setContactNumber(userObj.get("contactNumber"));
                    processUser.setPhone(userObj.get("contactNumber"));
                }

            }

            log.debug("====================" + userObj);
            log.debug("=========STATUS========== " + String.valueOf(userObj.get("status")));
            if (userObj.containsKey("status")) {
                int status = Integer.valueOf(String.valueOf(userObj.get("status")));

                savedFe.setStatus(status);
                processUser.setStatus(status);

//                if (status.equnull) {
//                    restResponse.setMessage("Status is mandatory");
//                    throw new Exception("Status is mandatory");
//                }
//                if (userObj.get("status").equals("Active")) {
//                    int statusCode = 0;
//                    savedFe.setStatus(statusCode);
//                    processUser.setStatus(1);
//                }
//                if (status == 1){
//                    savedFe.setStatus(status);
//                    processUser.setStatus(status);

//                }
//            } else {
//                savedFe.setStatus(0);
//                processUser.setStatus(0);
            } else {
                savedFe.setStatus(1);
                processUser.setStatus(1);
            }

            processUser.setRoleCode("field_engineer");
            processUser.setLocationId("5067");

            if (userObj.containsKey("franchiseId")) {
                savedFe.setFranchiseId(userObj.get("franchiseId"));
            } else {
                restResponse.setMessage("Franchise is mandatory");
                throw new Exception("Franchise is mandatory");
            }

            if (userObj.containsKey("agencyId")) {
                savedFe.setFranchiseId(userObj.get("agencyId"));
            } else {
                restResponse.setMessage("Agency is mandatory");
                throw new Exception("Agency is mandatory");
            }

            processUser.setPassword(encrypPass);
            savedFe.setPassword(encrypPass);
            masterdataService.saveFranchiseFe(savedFe);
            masterdataService.saveProcessUser(processUser);
        } catch (Exception e) {
            log.debug("==========================================================");
            log.debug(e.getMessage());
            e.printStackTrace();
            restResponse.setMessage("User Can not be updated");
            return restResponse;
        }

        restResponse.setMessage("User Updated");
        restResponse.setResponse(userObj);
        return restResponse;
    }


    @RequestMapping(value = "/fl/login", method = {RequestMethod.POST, RequestMethod.GET},
            produces = "application/json")
    public RestResponse flLogin(@RequestParam(value = "id") String id,
                                @RequestParam(value = "password") String password,
                                @RequestParam(required = false, value = "validity", defaultValue = "30") String validity,
                                @RequestParam(required = false, value = "deviceId") String deviceId, HttpServletResponse response) {

        RestResponse restResponse = new RestResponse();
        ProcessUserData user = new ProcessUserData();
        user.setSuccess(false);

        try {
            UIUser uiUser = new FranchiseLoginHandler().authenticate(id, password, response);
            if (uiUser == null) {
                throw new Exception("Invalid/Inactive user");
            }

            if (deviceId != null) {
                if (!uiUser.getRoleCode().equalsIgnoreCase("delivery_boy")
                        && !uiUser.getRoleCode().equalsIgnoreCase("field_engineer")) {
                    throw new Exception("Invalid/Inactive user");
                }
            }

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, Integer.parseInt(validity));
            String rawToken = id + "___" + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
            user.setToken(rawToken + "___" + AuthUtil.generateHMAC(rawToken));

            restResponse.setResponse(user);
            restResponse.setMessage("Login successful");

//            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
//                    .getDefaultProcessEngine())).getMasterdataService();

//            FranchiseFe.FranchiseFeQuery franchiseFeQuery = masterdataService.createFranchiseFeQuery();
//            ProcessUser.ProcessUserQuery processUserQuery = masterdataService.createProcessUserQuery();
//            FranchiseFe franchiseFe = franchiseFeQuery.emailId(id).singleResult();
//            ProcessUser processUser = processUserQuery.email(id).singleResult();
//            String userId = franchiseFe.getEmailId();
        } catch (Exception e) {
            log.error("Exception during login:" + e.getMessage(), e);
            restResponse.setMessage(e.getMessage());

        }
        return restResponse;

    }
}
