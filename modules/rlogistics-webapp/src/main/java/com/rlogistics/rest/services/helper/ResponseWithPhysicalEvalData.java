package com.rlogistics.rest.services.helper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseWithPhysicalEvalData {

    @JsonProperty("question")
    private String question;

    @JsonProperty("question_type")
    private String question_type;


    @JsonProperty("selected_answer")
    private String selected_answer;

    @JsonProperty("answers")
    private String answers;

}
