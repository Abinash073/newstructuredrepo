package com.rlogistics.rest.activiticalls;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.persistence.entity.AdditionalProductDetailsEntity;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngines;

import java.util.HashSet;
import java.util.List;

/**
 * Helper calls for Bulk Process called from Activiti
 *
 * @author Adarsh
 */
@Slf4j
@SuppressWarnings("unused")
public class BulkProcessHelperCalls {

    /**
     * Sub ticket validation by count of box received
     *
     * @param feName      Fe Name
     * @param subTicketNo His Sub ticket No
     * @return Count Of Box
     */


    public static int countOfBox(String feName, String subTicketNo) {
        log.debug("-----" + feName + "----" + subTicketNo);
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) ProcessEngines.getDefaultProcessEngine()).getMasterdataService();
        List<AdditionalProductDetails> details =
                masterdataService
                        .createAdditionalProductDetailsQuery()
                        .feAssigned(feName)
                        .subTicketRef(subTicketNo)
                        .list();


        //Unique Box Number
        HashSet<String> boxNumbers =
                details.stream()
                        .map(AdditionalProductDetails::getBoxNumber)
                        .collect(HashSet::new, HashSet::add, HashSet::addAll);

        return boxNumbers.size();
    }

    /**
     * changing status <b>product_packed_at_pickup_loc</b> if product is received in hub to <b>in_hub</b>
     *
     * @param feName       Fe Name
     * @param subTicketRef Sub Ticket No
     * @return True if Successful
     */
    public static boolean changeStatus(String feName, String subTicketRef) {
        try {
            MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) ProcessEngines.getDefaultProcessEngine()).getMasterdataService();

            List<AdditionalProductDetails> a = masterdataService.createAdditionalProductDetailsQuery()
                    .feAssigned(feName)
                    .subTicketRef(subTicketRef)
                    .status("product_packed_at_pickup_loc")
                    .list();
            a.forEach(b -> {
                AdditionalProductDetailsEntity c = (AdditionalProductDetailsEntity) b;
                c.setStatus("in_hub");
                masterdataService.saveAdditionalProductDetails(c);
            });
            return true;
        } catch (Exception e) {
            log.debug("error" + e);
        }
        return false;
    }

}

