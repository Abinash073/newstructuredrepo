package com.rlogistics.rest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.*;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.ProcessEngines;
import com.rlogistics.ampm.PickupRequest;
import com.rlogistics.ampm.PickupRequestSpecification;

import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.RestController;
import com.rlogistics.http.RLogisticsResource;

//@Slf4j
@RestController
public class RetailerController extends RLogisticsResource {
    private static Logger log;
    private static Pattern datePattern;

    private Object getDate(String value) {
        String dateSuffix = value.substring(getStartIndex(value));
        Matcher matcher = datePattern.matcher(dateSuffix);

        matcher.matches();

        Calendar calendar = Calendar.getInstance();

        if (matcher.start(3) >= 0) {
            /* yyyy-mm-dd */
            calendar.set(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)) - 1,
                    Integer.parseInt(matcher.group(5)));

        } else {
            /* dd-mm-yyyy */
            if (getOp(value).equals(">=")) {
                calendar.set(Integer.parseInt(matcher.group(9)), Integer.parseInt(matcher.group(8)) - 1,
                        Integer.parseInt(matcher.group(7)));
                calendar.set(Calendar.HOUR_OF_DAY, 00);
                calendar.set(Calendar.MINUTE, 00);
                calendar.set(Calendar.SECOND, 00);
            } else if (getOp(value).equals("<=")) {
                calendar.set(Integer.parseInt(matcher.group(9)), Integer.parseInt(matcher.group(8)) - 1,
                        Integer.parseInt(matcher.group(7)));
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);

            }

        }

        return calendar.getTime();
    }

    private Object getNumber(String value) {
        String numberSuffix = value.substring(this.getStartIndex(value));
        if (numberSuffix.indexOf(46) >= 0) {
            return new Double(Double.parseDouble(numberSuffix));
        }
        return new Long(Long.parseLong(numberSuffix));
    }

    private String getOp(String value) {
        int index = this.getStartIndex(value);
        return (index > 0) ? value.substring(0, index) : null;
    }

    private int getStartIndex(String value) {
        int index = 0;
        if (value.charAt(index) == '>' || value.charAt(index) == '<') {
            ++index;
            if (value.charAt(index) == '=') {
                ++index;
            }
        }
        return index;
    }

    private boolean isNumber(String value) {
        return NumberUtils.isNumber(value.substring(this.getStartIndex(value)));
    }

    private boolean isDate(String value) {


        return RetailerController.datePattern.matcher(value.substring(this.getStartIndex(value))).matches();
    }


    @Autowired
    ObjectMapper mapper;

    private CompletedTaskWithCount getCompletedTasksWithVariables(String queryJson, String variablesOfInterestJson, Map<String, String> pathVariables) throws IOException {
        CompletedTaskWithCount completedTaskWithCount = new CompletedTaskWithCount();
        ArrayList<TaskQueryParameter> query = mapper.readValue(URLDecoder.decode(queryJson), mapper.getTypeFactory().constructParametricType(ArrayList.class, new Class[]{TaskQueryParameter.class}));
        Set<String> variablesOfInterest = this.parseSet(variablesOfInterestJson);
        List<ProcessResultWithVariables> retval = new ArrayList<>();
        HistoricProcessInstanceQuery hiQuery = ProcessEngines
                .getDefaultProcessEngine()
                .getHistoryService()
                .createHistoricProcessInstanceQuery()
                .finished();


        RetailerController.log.debug("variableOfInterest : " + variablesOfInterest);

        pathVariables
                .entrySet()
                .stream()
                .forEach(stringStringEntry -> log.debug("Path Variables : " + stringStringEntry.getKey() + "--->" + stringStringEntry.getValue()));


        for (TaskQueryParameter tqp : query) {

            String variable = tqp.getVariable();
            String value = tqp.getValue();
            log.debug(variable);
            log.debug(value);
            boolean isNumber = this.isNumber(value);
            boolean isDate = this.isDate(value);
            if (isNumber || isDate) {
                String op = this.getOp(value);
                log.debug(op);
                Object valueObj = isNumber ? this.getNumber(value) : this.getDate(value);
                if (op == null) {
                    hiQuery.variableValueEquals(variable, valueObj);
                } else if (op.equals(">")) {
                    hiQuery.variableValueGreaterThan(variable, valueObj);
                } else if (op.equals(">=")) {
                    hiQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                } else if (op.equals("<")) {
                    hiQuery.variableValueLessThan(variable, valueObj);
                } else if (op.equals("<=")) {
                    hiQuery.variableValueLessThanOrEqual(variable, valueObj);
                }
            } else {
                if (value.indexOf("%") >= 0 || value.indexOf("*") >= 0) {
                    hiQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    hiQuery.variableValueEqualsIgnoreCase(variable, value);
                }
            }
        }


        List<HistoricProcessInstance> historicProcessInstances;

        /**todo : Bug
         * If i include variableOfInterst inside hiQuery then
         * hiQuery and size is coming different as it is omitting those historicProcessInstance where specific processVariablsOfIntrest is not present
         */

        hiQuery.includeProcessVariablesOfInterest(variablesOfInterest);

        /**
         * including all the processVariables
         * todo  : query will be very slow as we are fetching all the processVariables in a historic process . Alternate workaround is required
         */
        // hiQuery.includeProcessVariables();


        if (pathVariables.containsKey("firstResult")) {
            int firstResult = Integer.parseInt(pathVariables.get("firstResult"));
            int maxResults = Integer.parseInt(pathVariables.get("maxResults"));
            historicProcessInstances = hiQuery.listPage(firstResult, maxResults);
            log.debug(historicProcessInstances.size() + "--------" + hiQuery.count());
        } else {

            historicProcessInstances = hiQuery.list();
            log.debug(historicProcessInstances.size() + "--------" + hiQuery.count());
        }

        long totalRecords = hiQuery.count();
//            historicProcessInstances
//                    .stream()
//                    .forEach(historicProcessInstance -> historicProcessInstance.getProcessVariables()
//                            .entrySet()
//                            .stream()
//                            .forEach(stringObjectEntry -> log.debug(String.valueOf(stringObjectEntry.getValue()))));


        historicProcessInstances
                .forEach(historicProcessInstance -> retval
                        .add(new ProcessResultWithVariables(
                                historicProcessInstance,
                                variablesOfInterest.isEmpty() ? historicProcessInstance.getProcessVariables() : historicProcessInstance.getProcessVariables()
                                        .entrySet()
                                        .stream()
                                        .filter(stringObjectEntry -> variablesOfInterest.contains(stringObjectEntry.getKey()))
                                        .collect(HashMap::new, (stringObjectHashMap, stringObjectEntry) -> stringObjectHashMap.put(stringObjectEntry.getKey(), stringObjectEntry.getValue()), HashMap::putAll))));


        completedTaskWithCount.setProcessResultWithVariables(retval);
        completedTaskWithCount.setTotalCount(totalRecords);
        return completedTaskWithCount;
    }

    @RequestMapping(value = {"/tasks/completed-tickets", "/tasks/completed-tickets/{firstResult}/{maxResults}"}, produces = {"application/json"})
    public ProcessTaskWithPagination findRetailerCompletedTickets(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                                                  @RequestParam("query") String queryJson, @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]")
                                                                          String variablesOfInterestJson, @RequestParam(value = "roleCode", required = false)
                                                                          String roleCode, @PathVariable Map<String, String> pathVariables) {

        try {
//            if (!this.beforeMethodInvocation(httpRequest, httpResponse)) {
//                return null;
//            }
            ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
            CompletedTaskWithCount completedTaskWithCount = this.getCompletedTicketDetails(queryJson, variablesOfInterestJson, pathVariables);


//
//            for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
//                RetailerController.log.debug("TicketNo :" + String.valueOf(historicProcessInstance.getProcessVariables().get("rlTicketNo")));
//                ProcessResultWithVariables processResultWithVariables = new ProcessResultWithVariables(historicProcessInstance, variablesOfInterest.isEmpty() ? historicProcessInstance.getProcessVariables() : ((Map) historicProcessInstance.getProcessVariables().entrySet().stream().filter(entry -> variablesOfInterest.contains(entry.getKey())).collect(HashMap::new, (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll)));
//                processResultWithVariables.setProcessEndTime(historicProcessInstance.getEndTime());
//                retval.add(processResultWithVariables);
//            }
            processTaskWithPagination.setData(completedTaskWithCount.getProcessResultWithVariables());
            processTaskWithPagination.setTotalRecords(completedTaskWithCount.getTotalCount());
            return processTaskWithPagination;
        } catch (Exception ex) {
            RetailerController.log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(500);
            return null;
        }
    }

    /**
     * Rest Api end-point to generate Excel sheet
     *
     * @param httpRequest
     * @param httpResponse
     * @param queryJson
     * @param variablesOfInterestJson
     * @param roleCode
     * @param pathVariables
     * @return
     */
    @RequestMapping(value = {"/tasks/completed-tickets/generate-excel", "/tasks/completed-tickets/generate-excel/{firstResult}/{maxResults}"}, produces = {"application/json"})
    public ProcessTaskWithPagination CompletedTicketsExcel(HttpServletRequest httpRequest, HttpServletResponse httpResponse, @RequestParam("query") String queryJson, @RequestParam(value = "variablesOfInterest", required = false, defaultValue = "[]") String variablesOfInterestJson, @RequestParam(value = "roleCode", required = false) String roleCode, @PathVariable Map<String, String> pathVariables) {
        try {
            if (!this.beforeMethodInvocation(httpRequest, httpResponse)) {
                return null;
            }
            ProcessTaskWithPagination processTaskWithPagination = new ProcessTaskWithPagination();
            CompletedTaskWithCount completedTaskWithCount = this.getCompletedTicketDetails(queryJson, variablesOfInterestJson, pathVariables);
            processTaskWithPagination.setMessage(TaskResource
                    .createExcel(completedTaskWithCount
                            .getProcessResultWithVariables()
                    )
                    .getMessage()
            );
            processTaskWithPagination.setTotalRecords(completedTaskWithCount.getTotalCount());
            processTaskWithPagination.setSuccess(true);
            return processTaskWithPagination;
        } catch (Exception ex) {
            RetailerController.log.error("Exception during listInvolvedTasks", ex);
            httpResponse.setStatus(500);
            return null;
        }
    }

    static {
        RetailerController.log = LoggerFactory.getLogger((Class) TaskResource.class);
        RetailerController.datePattern = Pattern.compile("((([12][90][0-9][0-9])-([01][0-9])-([0123][0-9]))|(([0123][0-9])-([01][0-9])-([12][90][0-9][0-9])))");
    }

    @RequestMapping(value = {"/premtest"}, produces = {"application/json"})
    public PickupRequest testing(HttpServletRequest httpRequest, HttpServletResponse httpResponse
    ) {

        String soapEndpointUrl = "http://www.webservicex.net/uszip.asmx";
        String soapAction = "http://www.webserviceX.NET/GetInfoByCity";

        callSoapWebService(soapEndpointUrl, soapAction);
        PickupRequestSpecification pickupRequestSpecification = new PickupRequestSpecification();
        ;
        PickupRequest pickupRequest = new PickupRequest();
//		try
//		{
//			MessageFactory messageFactory = MessageFactory.newInstance();
//			SOAPMessage soapRequest = messageFactory.createMessage();
//			SOAPBody requestBody = getBody(soapRequest);
//
//			String serverURI = "http://tempuri.org/";
//
//			SOAPElement requestElement = requestBody.addChildElement("GetUserDetails", "", serverURI);
//
//			
//			
//
//			
//				addChildElement(requestElement, "PkpDateTime", "prem");
//			
//
////			if (pickupRequestSpecification.getDeliveryDetails().getDateTime() != null) {
////				addChildElement(requestElement, "DelDateTime", pickupRequestSpecification.getDeliveryDetails().getDateTime());
////			} else {
////				addChildElement(requestElement, "DelDateTime", "");
////			}
//
//			soapRequest.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/GetUserDetails");
//
//			soapRequest.saveChanges();
//
//			log.debug("Server request:" + toString(soapRequest));
//
//			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
//			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
//
//
//
//
//			String url = "https://demo.bizlog.in/TicketStatus/TicketStatus.asmx?";
//			SOAPMessage soapResponse = soapConnection.call(soapRequest, url);
//			log.debug("Server response:" + toString(soapResponse));
//
//			SOAPBody responseBody = getBody(soapResponse);
//
//			Node statusElement = responseBody.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild();
//
//
//
//
//
//
//
//
//
//			if (statusElement.getTextContent().equalsIgnoreCase("SUCCESS")) {
//				log.debug("PickupRequest number : _--> : " + statusElement.getNextSibling().getTextContent());
//				pickupRequest.setPickRequestNumber(statusElement.getNextSibling().getTextContent());
//				pickupRequest.setPickUpCost(statusElement.getNextSibling().getNextSibling().getTextContent());
//			} else {
//				pickupRequest.setSuccess(false);
//				pickupRequest.getMessages().add(statusElement.getTextContent());
//			}
//		}
//		catch (Throwable th) {
//			log.error("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage(), th);
//
//			PickupRequest pickupRequestError = new PickupRequest();
//			pickupRequestError.getMessages().add("Caught exception while processing SOAP parts of generatePickupRequest:" + th.getMessage());
//			return pickupRequestError;
//		}

        return pickupRequest;
    }

    private static void addChildElement(SOAPElement parentElement, String elementName, String elementText) throws SOAPException {
        SOAPElement childElement = parentElement.addChildElement(elementName);

        childElement.addTextNode(elementText);
    }

    public static SOAPBody getBody(SOAPMessage soapMessage) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        SOAPBody soapBody = soapEnvelope.getBody();
        return soapBody;
    }

    private static String toString(SOAPMessage message) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        message.writeTo(out);
        return new String(out.toByteArray());
    }


    /**
     *
     */


    private static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "myNamespace";
        String myNamespaceURI = "http://www.webserviceX.NET";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

            /*
            Constructed SOAP Request Message:
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:myNamespace="http://www.webserviceX.NET">
                <SOAP-ENV:Header/>
                <SOAP-ENV:Body>
                    <myNamespace:GetInfoByCity>
                        <myNamespace:USCity>New York</myNamespace:USCity>
                    </myNamespace:GetInfoByCity>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
            */

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("GetInfoByCity", myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("USCity", myNamespace);
        soapBodyElem1.addTextNode("New York");
    }

    private static void callSoapWebService(String soapEndpointUrl, String soapAction) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    private static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

    private CompletedTaskWithCount getCompletedTicketDetails(String queryJson, String variablesOfInterestJson, Map<String, String> pVariables) throws IOException {

        ArrayList<TaskQueryParameter> query = mapper.readValue(URLDecoder.decode(queryJson), mapper.getTypeFactory().constructParametricType(ArrayList.class, new Class[]{TaskQueryParameter.class}));
        Set<String> variablesOfInterest = this.parseSet(variablesOfInterestJson);
        List<ProcessResultWithVariables> retval = new ArrayList<>();
        CompletedTaskWithCount completedTaskWithCount = new CompletedTaskWithCount();
        HistoricProcessInstanceQuery hiQuery = ProcessEngines
                .getDefaultProcessEngine()
                .getHistoryService()
                .createHistoricProcessInstanceQuery()
                .finished();


        for (TaskQueryParameter tqp : query) {

            String variable = tqp.getVariable();
            String value = tqp.getValue();
            log.debug(variable);
            log.debug(value);
            boolean isNumber = this.isNumber(value);
            boolean isDate = this.isDate(value);
            if (isNumber || isDate) {
                String op = this.getOp(value);
                log.debug(op);
                Object valueObj = isNumber ? this.getNumber(value) : this.getDate(value);
                if (op == null) {

                    hiQuery.variableValueEquals(variable, valueObj);
                } else if (op.equals(">")) {
                    hiQuery.variableValueGreaterThan(variable, valueObj);
                } else if (op.equals(">=")) {
                    hiQuery.variableValueGreaterThanOrEqual(variable, valueObj);
                } else if (op.equals("<")) {
                    hiQuery.variableValueLessThan(variable, valueObj);
                } else if (op.equals("<=")) {
                    hiQuery.variableValueLessThanOrEqual(variable, valueObj);
                    log.info("");
                }
            } else {
                if (value.indexOf("%") >= 0 || value.indexOf("*") >= 0) {
                    hiQuery.variableValueLikeIgnoreCase(variable, value.replaceAll("\\*", "%"));
                } else {
                    hiQuery.variableValueEqualsIgnoreCase(variable, value);
                }
            }
        }


        List<HistoricProcessInstance> historicProcessInstances;

        if (pVariables.containsKey("firstResult")) {
            log.debug("");
            int firstResult = Integer.parseInt(pVariables.get("firstResult"));
            int maxResults = Integer.parseInt(pVariables.get("maxResults"));
            historicProcessInstances = hiQuery.listPage(firstResult, maxResults);
            log.debug(historicProcessInstances.size() + "--------" + hiQuery.count());
        } else {

            historicProcessInstances = hiQuery.list();
            log.debug(historicProcessInstances.size() + "--------" + hiQuery.count());
        }
        long totalRecord = hiQuery.count();
        Set<String> listOfpid = historicProcessInstances
                .stream()
                .map(historicProcessInstance -> historicProcessInstance.getId())
                .collect(Collectors.toSet());
        int listOfPidCount = listOfpid.size();


        HistoricProcessInstanceQuery hiQuery2 = ProcessEngines
                .getDefaultProcessEngine()
                .getHistoryService()
                .createHistoricProcessInstanceQuery()
                .finished();


        List<HistoricProcessInstance> historicProcessInstancesWithVariables =
                hiQuery2
                        .processInstanceIds(listOfpid)
                        .includeProcessVariablesOfInterest(variablesOfInterest)
                        .list();

//                List < HistoricProcessInstance > historicProcessInstancesWithVariables =
//                listOfpid
//                        .stream()
//                        .map(s -> hiQuery2.processInstanceId(s).includeProcessVariablesOfInterest(variablesOfInterest).singleResult()).collect(Collectors.toList());

        int sizeAftVar = historicProcessInstancesWithVariables.size();
        log.debug(totalRecord + "------" + listOfPidCount + "----" + sizeAftVar);

        historicProcessInstancesWithVariables
                .forEach(historicProcessInstance -> retval
                        .add(new ProcessResultWithVariables(
                                historicProcessInstance,
                                variablesOfInterest.isEmpty() ? historicProcessInstance.getProcessVariables() : historicProcessInstance.getProcessVariables()
                                        .entrySet()
                                        .stream()
                                        .filter(stringObjectEntry -> variablesOfInterest.contains(stringObjectEntry.getKey()))
                                        .collect(HashMap::new, (stringObjectHashMap, stringObjectEntry) -> stringObjectHashMap.put(stringObjectEntry.getKey(), stringObjectEntry.getValue()), HashMap::putAll))));

        int endres = retval.size();

        log.debug(totalRecord + "------" + listOfPidCount + "----" + sizeAftVar + "-----" + endres);
        completedTaskWithCount.setProcessResultWithVariables(retval);
        completedTaskWithCount.setTotalCount(totalRecord);

        return completedTaskWithCount;

    }


}