package com.rlogistics.rest;

public class ModelTicketDetails {

	private String consumerName;
	private String consumerComplaintNumber;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String pincode;
	private String landmark;
	private String telephoneNumber;
	private String retailerPhoneNo;
	private String alternateTelephoneNumber;
	private String emailId;
	private String alternateEmail;
	private String orderNumber;
	private String invoiceNo;
	private String dateOfPurchase;
	private String dateOfComplaint;
	private String levelOfIrritation;
	private String natureOfComplaint;
	private String isUnderWarranty;
	private String brand;
	private String productCategory;
	private String productName;
	private String productCode;
	private String model;
	private String productDescription;
	private String problemDescription;
	private String identificationNo;
	private String dropLocation;
	private String dropLocAddress1;
	private String dropLocAddress2;
	private String dropLocCity;
	private String dropLocState;
	private String dropLocPincode;
	private String dropLocContactPerson;
	private String dropLocContactNo;
	private String dropLocAlternateNo;
	private String physicalEvaluation;
	private String bulkMode;
	private String TechEvalRequired;
	
	public String getConsumerName() {
		return consumerName;
	}
	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}
	public String getConsumerComplaintNumber() {
		return consumerComplaintNumber;
	}
	public void setConsumerComplaintNumber(String consumerComplaintNumber) {
		this.consumerComplaintNumber = consumerComplaintNumber;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getRetailerPhoneNo() {
		return retailerPhoneNo;
	}
	public void setRetailerPhoneNo(String retailerPhoneNo) {
		this.retailerPhoneNo = retailerPhoneNo;
	}
	public String getAlternateTelephoneNumber() {
		return alternateTelephoneNumber;
	}
	public void setAlternateTelephoneNumber(String alternateTelephoneNumber) {
		this.alternateTelephoneNumber = alternateTelephoneNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getAlternateEmail() {
		return alternateEmail;
	}
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}
	public String getDateOfComplaint() {
		return dateOfComplaint;
	}
	public void setDateOfComplaint(String dateOfComplaint) {
		this.dateOfComplaint = dateOfComplaint;
	}
	public String getLevelOfIrritation() {
		return levelOfIrritation;
	}
	public void setLevelOfIrritation(String levelOfIrritation) {
		this.levelOfIrritation = levelOfIrritation;
	}
	public String getNatureOfComplaint() {
		return natureOfComplaint;
	}
	public void setNatureOfComplaint(String natureOfComplaint) {
		this.natureOfComplaint = natureOfComplaint;
	}
	public String getIsUnderWarranty() {
		return isUnderWarranty;
	}
	public void setIsUnderWarranty(String isUnderWarranty) {
		this.isUnderWarranty = isUnderWarranty;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getProblemDescription() {
		return problemDescription;
	}
	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}
	public String getIdentificationNo() {
		return identificationNo;
	}
	public void setIdentificationNo(String identificationNo) {
		this.identificationNo = identificationNo;
	}
	public String getDropLocation() {
		return dropLocation;
	}
	public void setDropLocation(String dropLocation) {
		this.dropLocation = dropLocation;
	}
	public String getDropLocAddress1() {
		return dropLocAddress1;
	}
	public void setDropLocAddress1(String dropLocAddress1) {
		this.dropLocAddress1 = dropLocAddress1;
	}
	public String getDropLocAddress2() {
		return dropLocAddress2;
	}
	public void setDropLocAddress2(String dropLocAddress2) {
		this.dropLocAddress2 = dropLocAddress2;
	}
	public String getDropLocCity() {
		return dropLocCity;
	}
	public void setDropLocCity(String dropLocCity) {
		this.dropLocCity = dropLocCity;
	}
	public String getDropLocState() {
		return dropLocState;
	}
	public void setDropLocState(String dropLocState) {
		this.dropLocState = dropLocState;
	}
	public String getDropLocPincode() {
		return dropLocPincode;
	}
	public void setDropLocPincode(String dropLocPincode) {
		this.dropLocPincode = dropLocPincode;
	}
	public String getDropLocContactPerson() {
		return dropLocContactPerson;
	}
	public void setDropLocContactPerson(String dropLocContactPerson) {
		this.dropLocContactPerson = dropLocContactPerson;
	}
	public String getDropLocContactNo() {
		return dropLocContactNo;
	}
	public void setDropLocContactNo(String dropLocContactNo) {
		this.dropLocContactNo = dropLocContactNo;
	}
	public String getDropLocAlternateNo() {
		return dropLocAlternateNo;
	}
	public void setDropLocAlternateNo(String dropLocAlternateNo) {
		this.dropLocAlternateNo = dropLocAlternateNo;
	}
	public String getPhysicalEvaluation() {
		return physicalEvaluation;
	}
	public void setPhysicalEvaluation(String physicalEvaluation) {
		this.physicalEvaluation = physicalEvaluation;
	}
	public String getBulkMode() {
		return bulkMode;
	}
	public void setBulkMode(String bulkMode) {
		this.bulkMode = bulkMode;
	}
	public String getTechEvalRequired() {
		return TechEvalRequired;
	}
	public void setTechEvalRequired(String techEvalRequired) {
		TechEvalRequired = techEvalRequired;
	}
	
	
	

	
	
	




}
