package com.rlogistics.rest.services.lambda;

import com.rlogistics.customqueries.EachFeDistanceDetails;
import com.rlogistics.rest.services.util.DistanceConversionAndCalculationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Extracting lambda for unit testing
 *
 * @author Adarsh
 */
@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GroupingFeData {
    private DistanceConversionAndCalculationUtil util;

    @Autowired
    public GroupingFeData(DistanceConversionAndCalculationUtil util) {
        this.util = util;
    }

    /**
     * Takes input of Each Fe's distance details and groups it by ticket no and adds the distance
     * to big decimal
     *
     * @param a List of EachFedistance
     * @return Map of Email of FE as key and Map of ticket no and Distance as value
     */
    public Map<String, Map<String, BigDecimal>> groupingFeLocationData(List<EachFeDistanceDetails> a) {
        return a
                .stream()
                .filter(e -> e.getTotalDistance().contains("km") || e.getTotalDistance().contains("m")) //take those values who has km and m
                .collect(Collectors.groupingBy(EachFeDistanceDetails::getEmail,   //group by email
                        Collectors.groupingBy( //nested group by
                                EachFeDistanceDetails::getTicketNo, //group by ticket no
                                Collectors.reducing(BigDecimal.ZERO, t -> util.changeStringToBigDecimal(t.getTotalDistance()), BigDecimal::add))
                ));
    }
}