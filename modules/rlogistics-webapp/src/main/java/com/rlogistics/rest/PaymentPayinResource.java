package com.rlogistics.rest;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Payment;
import com.rlogistics.master.identity.PaymentDetails;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerOther;
import com.rlogistics.master.identity.impl.cmd.CreatePaymentQueryCmd;
import com.rlogistics.master.identity.impl.cmd.CreatePaymentDetailsQueryCmd;
import com.rlogistics.master.identity.impl.cmd.SavePaymentDetailsCmd;
import com.rlogistics.master.identity.impl.cmd.SavePaymentCmd;
import com.rlogistics.master.identity.MasterdataService;

@RestController
@RequestMapping("/payment/payin")
public class PaymentPayinResource {
	private static final String CASHFREE_PROD_APP_ID = "98091b1143cddddea9b120359089";
	private static final String CASHFREE_PROD_SECRET_KEY = "f79fba0ca6376d622289516d3a3916e05fdf1dc8";
	private static String BIZLOG_TEST_NOTIFY_URL = "http://52.88.108.9:9080";
	private static String BIZLOG_PROD_NOTIFY_URL = "http://rl.bizlog.in:8080";

	private static final String CASHFREE_TEST_APP_ID = "3184158332f98cab34bbbc664813";
	private static final String CASHFREE_TEST_SECRET_KEY = "63006b8dfcaf35c01b1ac9b63fc05ff36143d61f";
	private static final String TYPE_CREATE_PAYIN = "PAYIN";
	private static final String CASHFREE_CREATE_PAYIN_PATH = "/api/v1/order/create";
	private static final String CASHFREE_PROD_URL = "api.cashfree.com";
	private static final String CASHFREE_TEST_URL = "test.cashfree.com";
	private final String BIZLOG_STATUS_LINK_SENT = "LINKSENT";

	private final String PAY_IN = "PAYIN";

	@RequestMapping(value = "/v1/createrequest", method = RequestMethod.POST, produces = "application/json")
	public RestResponse createPayinRequest(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		String processId = "";
		try {
			processId = new JSONObject(reqJson).getString("processId");
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			Map<String, Object> variables = runtimeService.getVariables(processId);
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getMasterdataService();
			RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
					.retailerId(variables.get("rlRetailerId").toString()).singleResult();
			if(retailerOther.getPayin() == 1){
				String payInRequestRequest = getJsonForCreatePayinRequest(variables);
				// if (primaryValidation(payInRequestRequest, response,
				// TYPE_CREATE_PAYIN)) {
				callCashFreeAPIForcreatePayinRequest(payInRequestRequest, response, variables);
				// }
			}else{
				response.setMessage("PayIn for this Retailer is not allowed.");
				response.setSuccess(false);
			}

		} catch (Exception e) {
			response.setMessage(e + "");
			response.setSuccess(false);
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "ERROR :CREATE_PAYIN_REQUEST: EXCEPTION" + e);
		}

		return response;
	}

	@RequestMapping(value = "/v1/payinNotify", method = RequestMethod.POST, produces = "application/json")
	public RestResponse rtnUrl(@RequestParam(value = "txStatus", defaultValue = "") String txStatus,
			@RequestParam(value = "orderId", defaultValue = "") String orderId,
			@RequestParam(value = "paymentMode", defaultValue = "") String paymentMode) {
		RestResponse response = new RestResponse();
		try {
			CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
					.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
			Payment.PaymentQuery paymetQuery = commandExecutor.execute(new CreatePaymentQueryCmd());
			Payment payment = paymetQuery.externalReferenceId(orderId).singleResult();
			payment.setStatus(txStatus);
			payment.setPaymentProvider(paymentMode);
			payment.setUpdateOn(getCurrentTimeStamp());
			commandExecutor.execute(new SavePaymentCmd(payment));

			PaymentDetails.PaymentDetailsQuery paymentDetailsQuery = commandExecutor
					.execute(new CreatePaymentDetailsQueryCmd());
			PaymentDetails paymentDetails = paymentDetailsQuery.cashgramId(orderId).singleResult();
			paymentDetails.setStatus(txStatus);
			paymentDetails.setUpdateOn(getCurrentTimeStamp());
			commandExecutor.execute(new SavePaymentDetailsCmd(paymentDetails));
		} catch (Exception e) {

			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "ERROR WHILE UPDATING STATUS IN CASHGRAM:" + e);
		}

		return response;
	}

	@RequestMapping(value = "/v1/fe/getpaymentstatus", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getStatusForFE(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		String processId = "";
		try {
			processId = new JSONObject(reqJson).getString("processId");
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			Payment payment = masterdataService.createPaymentQuery().processId(processId).singleResult();
			response.setSuccess(true);
			response.setResponse(payment.getStatus().toString());
			response.setMessage("Payment status is: " + payment.getStatus().toString());

		} catch (Exception e) {
			response.setMessage("Ticket is not present." + e);
			response.setSuccess(false);
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "ERROR :CREATE_PAYIN_REQUEST: EXCEPTION" + e);
		}

		return response;
	}

	private void callCashFreeAPIForcreatePayinRequest(String payInRequestRequest, RestResponse response,
			Map<String, Object> variables) throws Exception {
		String[] authDetails = getCashFreeAuth();
		String appId = authDetails[0];
		String secretKey = authDetails[1];

		String ticketNo = variables.get("rlTicketNo").toString();
		String amount = "";
		try {
			amount = variables.get("rlAmountToBeCollected").toString();
		} catch (Exception e) {
			throw new Exception("Amount not found");
		}

		String emailId = "";
		try {
			emailId = variables.get("emailId").toString();
		} catch (Exception e) {
		}
		String phone = variables.get("telephoneNumber").toString();
		String consumerName = variables.get("consumerName").toString().split(" ")[0];
		String notifyUrl = getBizlogBaseUrl() + "/rlogistics-execution/rlservice/payment/payin/v1/payinNotify";

		try {
			HashMap<String, Object> map = new HashMap<>();
			map.put("appId", appId);
			map.put("secretKey", secretKey);

			URIBuilder builder = new URIBuilder();
			builder.setScheme("https").setHost(getCashFreeBaseUrl()).setPath(CASHFREE_CREATE_PAYIN_PATH);

			URI uri = builder.build();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(uri);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("appId", appId));
			params.add(new BasicNameValuePair("secretKey", secretKey));
			params.add(new BasicNameValuePair("orderId", ticketNo));
			params.add(new BasicNameValuePair("orderAmount", amount));
			params.add(new BasicNameValuePair("orderNote", "Bizlog Note"));
			params.add(new BasicNameValuePair("customerEmail", emailId));
			params.add(new BasicNameValuePair("customerName", consumerName));
			params.add(new BasicNameValuePair("customerPhone", phone));
			params.add(new BasicNameValuePair("returnUrl", "https://bizlog.in/"));
			params.add(new BasicNameValuePair("notifyUrl", notifyUrl));

			httppost.setEntity(new UrlEncodedFormEntity(params));

			httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE",
					"CREATE PAYIN REQUEST:" + httppost + params.toString() + " RESPONSE: " + rawResponse);
			JSONObject jsonObject = new JSONObject(rawResponse);
			if (jsonObject.getString("status").toString().equals("OK")) {
				String PaymentLink = jsonObject.getString("paymentLink");
				response.setResponse(PaymentLink);
				response.setMessage(jsonObject.getString("paymentLink"));
				response.setSuccess(true);
				saveToDBPayinRequest(ticketNo, amount, emailId, consumerName, phone, variables,
						httppost + params.toString(), rawResponse, PaymentLink);
			} else {
				response.setResponse("Payin Link Not generated" + rawResponse.toString());
				response.setSuccess(false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse(e.getMessage() + "");
			response.setSuccess(false);
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE", "ERROR :CREATE PAYIN : EXCEPTION" + e);

		}

	}

	private void saveToDBPayinRequest(String orderId, String amount1, String emailId, String consumerName,
			String phone2, Map<String, Object> variables, String req, String res, String paymentLink) throws Exception {

		String cashGramRefId = orderId;
		String amount = amount1;
		String phone = phone2;
		String status = BIZLOG_STATUS_LINK_SENT;
		String ticketNo = orderId;
		String processId = variables.get("rlProcessId").toString();
		String retailerId = variables.get("rlRetailerId").toString();
		String location = variables.get("rlLocationName").toString();
		String paymentType = PAY_IN;

		String trfId = "";
		String name = variables.get("consumerName").toString();
		String email = emailId;
		String payoutLink = paymentLink;

		new Thread(new Runnable() {

			@Override
			public void run() {
				String remarks = "";
				try {
					remarks = variables.get("retailer").toString();
				} catch (Exception e) {

				}

				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();

				Payment payment = masterdataService.newPayment();
				UUID id = UUID.randomUUID();
				payment.setAmount(amount);
				payment.setExternalReferenceId(cashGramRefId);
				payment.setPaymentProvider("");
				payment.setPaymentRequest(req);
				payment.setPaymentResponse(res);
				payment.setPaymentType(paymentType);
				payment.setPhone(phone);
				payment.setProcessId(processId);
				payment.setRemarks(remarks);
				payment.setRetailerId(retailerId);
				payment.setStatus(status);
				payment.setTicketNo(ticketNo);
				payment.setCreatedOn(getCurrentTimeStamp());
				payment.setLocation(location);
				masterdataService.savePayment(payment);

				PaymentDetails paymentDetails = masterdataService.newPaymentDetails();
				paymentDetails.setAcknowledged("");
				paymentDetails.setAmount(amount);
				paymentDetails.setCashfreeReferenceId(trfId);
				paymentDetails.setCashgramId(cashGramRefId);
				paymentDetails.setCreatedOn(getCurrentTimeStamp());
				paymentDetails.setEmail(email);
				paymentDetails.setName(name);
				paymentDetails.setPayoutLink(payoutLink);
				paymentDetails.setPhone(phone);
				paymentDetails.setStatus(status);
				paymentDetails.setTicketNo(ticketNo);
				paymentDetails.setTransferId("");
				paymentDetails.setUtr("");
				masterdataService.savePaymentDetails(paymentDetails);
			}
		}).start();

	}

	private String getJsonForCreatePayinRequest(Map<String, Object> variables) {
		// TODO Auto-generated method stub
		return "";
	}

	private boolean primaryValidation(String payInRequestRequest, RestResponse response, String typeCreatePayin) {
		StringBuilder errorMeesage = new StringBuilder("");
		boolean rtnvalue = true;
		JSONObject fieldValues = null;
		try {
			fieldValues = new JSONObject(payInRequestRequest);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		ArrayList<String> fV = new ArrayList<>();
		switch (typeCreatePayin) {
		case TYPE_CREATE_PAYIN:
			fV.add("appId");
			fV.add("secretKey");
			fV.add("orderId");
			fV.add("orderAmount");
			// fV.add("orderCurrency");
			// fV.add("orderNote");
			fV.add("customerName");
			fV.add("customerPhone");
			fV.add("customerEmail");
			// fV.add("sellerPhone");
			fV.add("returnUrl");
			// fV.add("notifyUrl");
			// fV.add("paymentModes");
			// fV.add("pc");
			break;

		default:
			break;
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					rtnvalue = false;
					errorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				errorMeesage.append(field + " field is missing, ");
				rtnvalue = false;
			}
		}

		response.setSuccess(rtnvalue);
		response.setMessage(errorMeesage.toString());
		return rtnvalue;
	}

	private String[] getCashFreeAuth() {
		String[] rtnValue = new String[2];
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
				rtnValue[0] = CASHFREE_PROD_APP_ID;
				rtnValue[1] = CASHFREE_PROD_SECRET_KEY;
			} else {
				rtnValue[0] = CASHFREE_TEST_APP_ID;
				rtnValue[1] = CASHFREE_TEST_SECRET_KEY;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

	public static String getCashFreeBaseUrl() {
		String rtnValue = "URL";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
				rtnValue = CASHFREE_PROD_URL;
			} else {
				rtnValue = CASHFREE_TEST_URL;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

	public static String getBizlogBaseUrl() {
		String rtnValue = "URL";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			if (IP.getHostAddress().toString().equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
				rtnValue = BIZLOG_PROD_NOTIFY_URL;
			} else {
				rtnValue = BIZLOG_TEST_NOTIFY_URL;

			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sd = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		return sd.format(date) + "";
	}

}
