package com.rlogistics.rest;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;

public class ProcessInstanceResult {

		private String processId; 
		private String name;
		private String description;

		public ProcessInstanceResult(ProcessInstance processInstance){
			setProcessId(processInstance.getProcessInstanceId());
			setName(processInstance.getName());
			setDescription(processInstance.getDescription());
		}
		
		public ProcessInstanceResult(HistoricProcessInstance processInstance){
			setProcessId(processInstance.getId());
			setName(processInstance.getName());
			setDescription(processInstance.getDescription());
		}
		
		public String getProcessId() {
			return processId;
		}

		public void setProcessId(String processId) {
			this.processId = processId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
}
