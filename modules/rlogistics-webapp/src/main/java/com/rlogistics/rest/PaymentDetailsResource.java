package com.rlogistics.rest;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailUtil;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;
import com.rlogistics.master.identity.RetailerSmsEmailTriggerConfig;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate.RetailerSmsEmailTemplateQuery;
import com.rlogistics.sms.SMSSendCommand;

import pojo.ReportPojo;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RetailerOther;

@RestController
@RequestMapping("/payment/details")
public class PaymentDetailsResource {
	public static Logger log = LoggerFactory.getLogger(PaymentDetailsResource.class);

	@RequestMapping(value = "/v1/getpaymenttype", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getpaymenttype(@RequestBody String reqJson) {
		RestResponse response = new RestResponse();
		String processId = "";
		try {

			Map<String, String> resJson = new HashMap<String, String>();
			processId = new JSONObject(reqJson).getString("processId");
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			Map<String, Object> variables = runtimeService.getVariables(processId);
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
					.retailerId(variables.get("rlRetailerId").toString()).singleResult();
			if (retailerOther.getPayin() == 1 || retailerOther.getPayout() == 1) {
				try {
					String amount = variables.get("amountToBePaid").toString();
					resJson.put("paymentType", "PAYOUT");
					resJson.put("msg", "Bizlog have to pay : " + amount + " /-");
					resJson.put("ticketNo", variables.get("rlTicketNo").toString());
					resJson.put("amt", amount);
					response.setResponse(resJson);
					response.setSuccess(true);
					response.setMessage("Success");
				} catch (Exception e) {
					try {
						String amount = variables.get("rlValueOffered").toString();
						resJson.put("paymentType", "PAYOUT");
						resJson.put("msg", "Bizlog have to pay : " + amount + " /-");
						resJson.put("ticketNo", variables.get("rlTicketNo").toString());
						resJson.put("amt", amount);
						response.setResponse(resJson);
						response.setSuccess(true);
						response.setMessage("Success");
					} catch (Exception e1) {
						// try {
						// String amount =
						// variables.get("maxValueToBeOffered").toString();
						// resJson.put("paymentType", "PAYOUT");
						// resJson.put("msg", "Bizlog have to pay : " + amount +
						// " /-");
						// resJson.put("ticketNo",
						// variables.get("rlTicketNo").toString());
						// resJson.put("amt", amount);
						// response.setResponse(resJson);
						// response.setSuccess(true);
						// response.setMessage("Success");
						// } catch (Exception e2) {
						try {
							String amount = variables.get("rlAmountToBeCollected").toString();
							resJson.put("paymentType", "PAYIN");
							resJson.put("msg", "Bizlog have to receive : " + amount + " /-");
							resJson.put("ticketNo", variables.get("rlTicketNo").toString());
							resJson.put("amt", amount);
							response.setResponse(resJson);
							response.setSuccess(true);
							response.setMessage("Success");
						} catch (Exception e3) {
							response.setSuccess(false);
							response.setMessage("No Amount found with this ticket");

							// }
						}
					}
				}
			} else {
				response.setSuccess(false);
				response.setMessage("Payments for this Retailer is not allowed.");

			}
		} catch (Exception e) {
			log.debug("/v1/getpaymenttype: " + e);
			response.setMessage(e.getMessage() + "");
			response.setSuccess(false);
			RetailerApiCalls.saveToConsoleLogsTable("GETPAYMENTTYPE", "ERROR : EXCEPTION" + e);
		}

		return response;
	}

	private String getReceiveAmount(Map<String, Object> variables) throws Exception {
		String amount = "";
		try {
			amount = variables.get("rlAmountToBeCollected").toString();
		} catch (Exception e) {
			try {
				amount = variables.get("rlValueOffered").toString();
			} catch (Exception e2) {
				// try {
				// amount = variables.get("maxValueToBeOffered").toString();
				// } catch (Exception ew) {
				throw new Exception("Amount not found");
				// }
			}
		}
		return amount;
	}

	@RequestMapping(value = "/v1/test", method = RequestMethod.POST, produces = "application/json")
	public RestResponse test(@RequestParam(value = "rlServiceId") String a,
			@RequestParam(value = "rlProcessId") String b, @RequestParam(value = "rlRetailerId") String c,
			@RequestParam(value = "smscode") String d) {
		RestResponse response = new RestResponse();
		try {
			Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService().getVariables(b);
			ReportPojo pojo = new ReportPojo(b);
			pojo.updateReport(ReportPojo.pickupdone(variables));

			response.setMessage("DONE");
		} catch (Exception e) {
			try {
				ReportPojo pojo1 = new ReportPojo(b);
				pojo1.sentToReportServer(pojo1);
			} catch (Exception e1) {

			}
			log.debug(e.getStackTrace().toString());
			response.setMessage("ERROR" + e);

		}
		return response;
	}

	@RequestMapping(value = "/v1/get-mobile-pay-selection", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getMobileSelection(@RequestParam(required = true, value = "msg") String msg,
			@RequestParam(required = true, value = "processId") String processId) {
		RestResponse response = new RestResponse();
		try {
			RetailerApiCalls.saveToConsoleLogsTable("CASHFREE-FE", "PROCESSID" + processId + "MESSAGE" + msg);
		} catch (Exception e) {
			log.debug(e.getStackTrace().toString());
			response.setMessage("ERROR" + e);

		}
		return response;
	}

}
