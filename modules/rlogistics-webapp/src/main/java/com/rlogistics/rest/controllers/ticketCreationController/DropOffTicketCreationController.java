package com.rlogistics.rest.controllers.ticketCreationController;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.rest.controllers.mainResponseEntity.DropOffData;
import com.rlogistics.rest.services.ticketCreationService.DropOffTicketCreationService;
import com.rlogistics.rest.services.ticketCreationService.responseEntity.DropOffTicketCreationData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * Controller to create drop of ticket
 */
@Slf4j
@RestController
public class DropOffTicketCreationController {

    private DropOffTicketCreationService creationService;
    private ObjectMapper mapper;

    @Autowired
    public DropOffTicketCreationController(DropOffTicketCreationService creationService,
                                           ObjectMapper mapper) {
        this.creationService = creationService;
        this.mapper = mapper;

    }

    /**
     * Creates drop of ticket for a specific item / Multiple item in bulk flow
     *
     * @param query        Bulk item ids
     * @param retailerName Retailer Name
     * @param processId    process Id of main ticket
     * @return
     */
    @RequestMapping(value = "create/drop-off", produces = MediaType.APPLICATION_JSON_VALUE)
    public  DropOffData createDropOffTicket(@RequestParam(value = "query") String query,
                                           @RequestParam(value = "retailer") String retailerName,
                                           @RequestParam(value = "processId") String processId) {
        DropOffData result = new DropOffData();
        try {

            Set<String> ids = mapper.readValue(query, new TypeReference<Set<String>>() {
            });


            DropOffTicketCreationData creationData = creationService.createDropOffTicketByProcessId(processId, retailerName, ids);

            result.setData(creationData);
            result.setSucess(true);

        } catch (Exception e) {
            log.debug("error");
            result.setCount(0);
            result.setErrormsg(e.getMessage());
            result.setSucess(false);
            log.debug("Error" + e);

        }
        log.debug(result.getData().toString());
        return result;
    }
}
