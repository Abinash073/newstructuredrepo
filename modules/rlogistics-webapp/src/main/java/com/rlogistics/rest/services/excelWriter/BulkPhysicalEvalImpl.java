package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.rest.services.PhysicalEvaluationService.PhysicalEvaluationDataDTO;
import com.rlogistics.rest.services.helper.ResponseWithPhysicalEvalData;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of Creating Excel Sheet For Bulk Physical Eval Data
 * @author Adarsh
 */
@Service("bulkPhysicalEvalData")
@Slf4j
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)

public class BulkPhysicalEvalImpl extends ReportExcelUtilImpl {
    private LinkedList<String> headerList = new LinkedList<>();
    private int fRowForData = 1;
    private int dataCol = 0;

    @NotNull
    public static PhysicalEvaluationDataDTO getPhysicalEvaluationDataDTO(String question_type, String answers, String selected_answer, String question) {
        PhysicalEvaluationDataDTO dto = new PhysicalEvaluationDataDTO();
        String finalString = "N/A";
        if (question_type.equals("Options")) {
            String[] array = answers.split("(,\\s)");
            finalString =
                    Arrays
                            .stream(array)
                            .filter(a -> a.startsWith(selected_answer))
                            .map(s -> s.replaceAll("^[^_]*=>", ""))
                            .collect(Collectors.joining());
        } else if (question_type.equals("Text")) {
            finalString = selected_answer;
        }
        dto.setQuestion(question);
        dto.setAnswer(finalString);
        return dto;
    }

    @Override
    public ReportExcelUtil insertData(List<?> data, String type) throws IOException {
        Row dataRow = sheet.createRow(fRowForData++);
        headerList = header.keySet().stream().sequential().collect(LinkedList::new, LinkedList::add, LinkedList::addAll);


        List<ResponseWithPhysicalEvalData> cData = (List<ResponseWithPhysicalEvalData>) data;
        List<PhysicalEvaluationDataDTO> convertedData = cData.stream().map(this::convertToDto).collect(Collectors.toList());

        for (String head : headerList
        ) {
            Cell inscell = dataRow.createCell(dataCol++);
            inscell.setCellValue(type);
            for (PhysicalEvaluationDataDTO dataDTO : convertedData
            ) {
                if (dataDTO.getQuestion().trim().equalsIgnoreCase(head.trim())) {
                    log.debug(dataDTO.getAnswer());
                    inscell.setCellValue(dataDTO.getAnswer());
                }

            }
        }

        dataCol = 0;
        return this;
    }


    private PhysicalEvaluationDataDTO convertToDto(ResponseWithPhysicalEvalData res) {
        return getPhysicalEvaluationDataDTO(res.getQuestion_type(), res.getAnswers(), res.getSelected_answer(), res.getQuestion());
    }
}
