package com.rlogistics.rest;

import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.activiti.engine.ProcessEngines;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class BulkController {
	
	@RequestMapping(value = "/fe/get-product-category", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> getProductCategoryWithMainTicketProcessId(@RequestBody String req)throws Exception {
		Map<String, Object> rtnMap = new HashMap<>();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Map<String, Object> fieldValues = new ObjectMapper().readValue(URLDecoder.decode(req), Map.class);

		if (fieldValues.containsKey("mainTicketRef")) {
			List<AdditionalProductDetails> additionalProductDetails = masterdataService
					.createAdditionalProductDetailsQuery().mainTicketRef(fieldValues.get("mainTicketRef").toString()).list();
			String mainTicketProcessId="";
			if (additionalProductDetails.size() > 0) {
				mainTicketProcessId=additionalProductDetails.get(1).getProcessId();
			}
			List<ProductCategory> productCategogy = masterdataService.createProductCategoryQuery().list();
			List<String> productCategogy1 =new ArrayList<>();
			for(ProductCategory p:productCategogy){
				productCategogy1.add(p.getName());
			}
			rtnMap.put("categoryList", productCategogy1);
			rtnMap.put("mainTicketProcessId", mainTicketProcessId);

		}else{
			rtnMap.put("msg", "mainTicketRef  key is not mention");

		}
		

		return rtnMap;

	}

}
