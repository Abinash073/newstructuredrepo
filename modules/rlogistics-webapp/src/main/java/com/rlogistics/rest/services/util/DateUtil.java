package com.rlogistics.rest.services.util;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


/**
 * Util related to Date conversion
 *
 * @author Adarsh
 */


@Service
@Primary

public class DateUtil {
    /**
     * This method converts {@code String} date to a specific format after adding days.
     *
     * @param date     Angular date
     * @param days     days needs to be added on date
     * @param pattern  date pattern
     * @return  converted date
     */
    public String getDateByAddingDate(String date, int days, String pattern) {

        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.parseDateTime(date).plusDays(days).toString(formatter);

    }

    /**
     * This method converts {@code String} date to a specific format after decreasing days.
     * @param date     Angular date
     * @param days    days needs to be reduced on date
     * @param pattern  date pattern
     * @return  converted date
     */
    public String getDateByReducingDate(String date, int days, String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.parseDateTime(date).minusDays(days).toString(formatter);
    }
}
