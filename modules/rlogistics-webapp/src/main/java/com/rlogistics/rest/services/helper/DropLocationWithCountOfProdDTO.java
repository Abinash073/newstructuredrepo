package com.rlogistics.rest.services.helper;

import com.rlogistics.master.identity.AdditionalProductDetails;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data

public class DropLocationWithCountOfProdDTO {
    int count = 0;
    String dropLocationAddress = "N/A";
    List<AdditionalProductDetails> additionalProductEntities;

    public DropLocationWithCountOfProdDTO() {
        this.additionalProductEntities = new ArrayList<>();
    }
}
