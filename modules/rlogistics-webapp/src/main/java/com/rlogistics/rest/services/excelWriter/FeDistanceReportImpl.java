package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.customqueries.FeDistanceReportData;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation for Generating Excel sheet for Fe distance Data
 *
 * @author Adarsh
 */
@Service("feDistanceReport")
public class FeDistanceReportImpl extends ReportExcelUtilImpl {
    @Override
    public ReportExcelUtil insertData(List<?> data, String type) {
        if (type.equals("distance")) {
            ((List<FeDistanceReportData>) data).forEach(this::insertInCell);
        }
        return this;
    }

    private void insertInCell(FeDistanceReportData data) {
        int colNo = 0;

        //New Row
        Row row = sheet.createRow(rownum++);

        //Index
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Fe Name

        cell = row.createCell(colNo++);
        cell.setCellValue(data.getFeName());


        //Location
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getCity());

        //Total Distance
        cell = row.createCell(colNo);
        cell.setCellValue(data.getTotalDistance() + " " + "km");

    }


}
