package com.rlogistics.rest.services.excelWriter;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Interface which defines rules to create a Excel sheet
 * @author Adarsh
 */
public interface ReportExcelUtil {

    ReportExcelUtil setStyling() throws IOException;

    ReportExcelUtil initExcel(String shetName) throws IOException;

    ReportExcelUtil createHeader(LinkedHashMap<String, String> header) throws IOException;

    ReportExcelUtil insertData(List<?> data, String type) throws IOException;

    ReportExcelUtil saveExcel() throws IOException;

    byte[] getExcel();
}
