package com.rlogistics.rest;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.util.json.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerOther;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class BuyBackController {
	private static Logger log = LoggerFactory.getLogger(BuyBackController.class);

	@RequestMapping(value = "/ticket/create/buyback", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketBuyBack(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Map<String, String> fieldValues = null;
		
		if (BuyBackPrimaryValidation(queryJSON, restResult, retailerId)) {
			String jsonTicket = "";
			ArrayList<RestExternalResult> flag = new ArrayList<RestExternalResult>();
			try {
				fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
				if(!fieldValues.containsKey("consumerComplaintNumber")){
					fieldValues.put("consumerComplaintNumber", "default");
				}
				if(!fieldValues.containsKey("dateOfComplaint")){
					fieldValues.put("dateOfComplaint", RetailerApiCalls.getCurrentDateSQLFormat());
				}
			} catch (Exception e) {
				restResult.setSuccess(false);
				restResult.setResponse("Incorrct Format");
			}
			try {
				jsonTicket = JSONValue.toJSONString(fieldValues);
				
			} catch (Exception e) {
				log.debug("Error in Conversoion from MAp to String :" + e);
			}
			try {
				if (fieldValues.get("productCategory").equals("mobile")) {
					try {
						RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
								.retailerId(retailerId).singleResult();
						if (retailerOther.getIsMobileProductAddAllow() == 1) {
							addMobileProduct(jsonTicket, masterdataService, retailerId, fieldValues);
						} else {
							log.info("This Add Mobile Product is not valid for this Retailer");
						}
					} catch (Exception e) {
						log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

					}
				} else {
					addAllProduct(jsonTicket, masterdataService, retailerId, fieldValues);
				}
				// create ticket

				String newjsonTicket = JSONValue.toJSONString(fieldValues);
				if (secondryValidationBuyBack(fieldValues, retailerId, apiToken, restResult)) {
					ExcelProcessResource epr = new ExcelProcessResource();
					restResult = epr.ticketCreation(newjsonTicket, retailerId, apiToken);
				} else {
					log.debug("secondryValidation fails --------------");
				}

			} catch (Exception e) {
				log.debug("error" + e);
				e.printStackTrace();
				RetailerApiCalls.saveToConsoleLogsTable(retailerId,
						"Buy Back " + " Request Json: " + queryJSON + " Response:" + restResult.getTicketNo().toString()
								+ " Message: " + restResult.getMessage() + "ERROR:" + e);

			}
		} else {
			// PNDOneWayPrimaryValidation got false
		}
		RetailerApiCalls.saveToConsoleLogsTable(retailerId, "Buy Back " + " Request Json: " + queryJSON + " Response:"
				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;
	}

	private boolean BuyBackPrimaryValidation(String queryJSON, RestExternalResult restResult, String retailerId) {

		/**
		 * productName model productCode brand identificationNo
		 */
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;
		String productNameerrorMessage = "";
		String modelerrorMessage = "";
		String productCodeerrorMessage = "";
		String branderrorMessage = "";
		String identificationNomodelerrorMessage = "";
		String productCategoryerrorMessage = "";

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {
			value = checkForFieldsInLoopForOldRetailer(fieldValues, value, finalErrorMeesage);

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;

	}

	private boolean checkForFieldsInLoopForOldRetailer(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		// fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("returnSellerCode");
		fV.add("returnSellerPincode");
		fV.add("pickupSellerCode");
		fV.add("pickupSellerPincode");

		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		
		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}

			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;

	}

	private boolean checkForFields(JSONObject fieldValues, boolean value, StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		fV.add("emailId");
		fV.add("orderNumber");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		fV.add("model");
		fV.add("pickupSellerCode");
		fV.add("pickupSellerPincode");
		fV.add("returnSellerCode");
		fV.add("returnSellerPincode");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}

			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;

	}

	private boolean secondryValidationBuyBack(Map<String, String> fieldValues, String retailerId, String apiToken,
			RestExternalResult restResult) {
		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateProductBrand()) {
			errorMeesage.append(fieldValues.get("brand") + " Brand is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (fieldValues.get("pickupSellerCode").equals(fieldValues.get("returnSellerCode"))) {
			if (!fieldsValidationImp.checkRetailerCode(fieldValues.get("pickupSellerCode"),
					fieldValues.get("pickupSellerPincode"))) {
				errorMeesage.append(fieldValues.get("pickupSellerCode")
						+ ": returnSellerCode or pickupSellerCode is not valid for this Retailer, ");
				retrnValue = false;
			}
		} else {
			errorMeesage.append(": pickupSellerCode or returnSellerCode is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (fieldValues.get("pickupSellerPincode").equals(fieldValues.get("returnSellerPincode"))) {
			if (!fieldsValidationImp.checkRetailerCode(fieldValues.get("returnSellerCode"),
					fieldValues.get("returnSellerPincode"))) {
				errorMeesage.append(fieldValues.get("returnSellerCode")
						+ ": returnSellerCode or pickupSellerCode is not valid for this Retailer, ");
				retrnValue = false;
			}
		} else {
			errorMeesage.append(": pickupSellerPincode or returnSellerPincode is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;
	}

	public void addMobileProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {

		/**
		 * These value we get from retailer related to product "brand": "Apple",
		 * "productCategory": "mobile", "productName": "Iphone 7",
		 * "productCode": "iPhone 7", "model": "S",
		 */

		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}

				log.debug("22222222222222");

				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					/**
					 * =masterdataService.createProductCategoryQuery().name(
					 * "Mobile" THIS "Mobile" string should be same in
					 * RL_MD_PRODUCT_CATEGORY IN Production server ,dev or in
					 * test
					 */
					ProductCategory productCategory = masterdataService.createProductCategoryQuery().name("Mobile")
							.singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());// SUBCATEGORY ID
																// FROM
																// RL_MD_RETAILER_CATEGORIES
																// TABLE
					/**
					 * THIS PRODUCT CATEGORY SHOULD BE ADD IN
					 * RL_MD_RETAILER_CATEGORIES AND GET PRODUCT_id_ HERE
					 */
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());// Wrap
																		// Box
																		// 35x23x11
																		// from
																		// RL_MD_PACKAGING_TYPE
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Bubble Cover 9x6")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());// Bubble
																			// Cover
																			// 9x6
																			// from
																			// RL_MD_PACKAGING_TYPE
					product.setBrandId(newBrandId);// IT SHOULD BE BRAND ID FROM
													// RL_MD_BRAND
					/**
					 * THIS BRAND SHOULD BE ADD IN RL_MD_BRAND AND GET HERE THE
					 * BARND_ID_
					 */

					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");

					log.info("New Product Added Successfully");
					/**
					 * after add the product this product should be allow to
					 * this retailer this is the error message Status false
					 * iphone 7prem is not a valid Product Code of Demo
					 * Retailer.null
					 */
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public String addNewBrand(MasterdataService masterdataService, JSONObject fieldValues) {
		Brand brand = masterdataService.newBrand();
		String brandId = String.valueOf(UUID.randomUUID());

		brand.setId(brandId);
		brand.setName(fieldValues.getString("brand").toUpperCase());
		brand.setCode(randomAlphaNumeric(20));
		// brand.setDescription("description");

		masterdataService.saveBrand(brand);
		log.debug("*************************New Brand is Added");

		return brandId;
	}

	public void addAllProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {
		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (!fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}
				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					ProductCategory productCategory = masterdataService.createProductCategoryQuery()
							.name(fieldValues.getString("productCategory")).singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Other")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());
					product.setBrandId(newBrandId);
					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");
					log.info("New Product Added Successfully");
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

}
