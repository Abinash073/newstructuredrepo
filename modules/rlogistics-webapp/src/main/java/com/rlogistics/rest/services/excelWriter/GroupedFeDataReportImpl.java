package com.rlogistics.rest.services.excelWriter;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of Each Fe Distance Data while grouping it based on location
 * @author Adarsh
 */
@Service("eachFeGroupedReport")
@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Slf4j
//My Random thoughts and imaginations ! Forgive me for for this.
public class GroupedFeDataReportImpl extends ReportExcelUtilImpl {
    // private int index = 0;

    private HashMap<String, XSSFSheet> allSheet = new HashMap<>();
    private String currentFe;
    private XSSFWorkbook wb = new XSSFWorkbook();

    @Override
    @SuppressWarnings("Duplicates")
    public ReportExcelUtil initExcel(String sheetName) throws IOException {
        //As we are using bean <Singleton scope> initialize everything here
        currentFe = sheetName;

        this.font = wb.createFont();
        this.style = wb.createCellStyle();
        outputStream = new ByteArrayOutputStream();
        rownum = 1;
        sNo = 1;
        cel = 0;
        //TODO modify
        try {
            XSSFSheet currentSheet = wb.createSheet(sheetName);
            allSheet.put(sheetName, currentSheet);
        } catch (Exception e) {
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ReportExcelUtil createHeader(LinkedHashMap<String, String> header) throws IOException {
        try {
            //Header Row Style
            XSSFCellStyle headerStyle = style;
            headerStyle.setFillBackgroundColor(IndexedColors.WHITE.getIndex());

            //Header Cell Style
            XSSFFont xssfFont = wb.createFont();
            xssfFont.setColor(IndexedColors.BLACK.getIndex());
            xssfFont.setBold(true);

            headerStyle.setFont(xssfFont);

            this.header = header;
            XSSFSheet sheetToWork = allSheet.get(currentFe);
            firstRow = sheetToWork.createRow(0);
            firstRow.setRowStyle(headerStyle);

            header.forEach((key, value) -> {
                log.debug(key + "-------" + value);
                Cell c = firstRow
                        .createCell(cel++);
                c.setCellValue(key);
                c.setCellStyle(style);
            });
        } catch (Exception e) {
            log.debug("Error in adding header" + e);
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    public ReportExcelUtil insertData(List<?> data, String type) throws IOException {
        ((List<Map.Entry<String, BigDecimal>>) (data)).forEach(this::insertInCell);
        return this;
    }

    private void insertInCell(Map.Entry<String, BigDecimal> data) {
        int colNo = 0;
        //New Row
        XSSFSheet sheetToWork = allSheet.get(currentFe);
        Row row = sheetToWork.createRow(rownum++);

        //Create S. No cell
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Ticket No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getKey());

        //Total Distance
        cell = row.createCell(colNo);
        cell.setCellValue(data.getValue() + " km");
    }

    @Override
    public ReportExcelUtil saveExcel() throws IOException {

        wb.write(outputStream);
        this.bytes = outputStream.toByteArray();
        outputStream.flush();
        return this;
    }

    @Override
    public byte[] getExcel() {
        return bytes;
    }
}
