package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.List;

public class UploadProcessCommandsResult {
	private List<ErrorObject> errorMessages = new ArrayList<>();
	private List<String> ticketNo = new ArrayList<>();
	public List<String> getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(List<String> ticketNo) {
		this.ticketNo = ticketNo;
	}

	private int noOfRecordsInserted;

	
	public int getNoOfRecordsInserted() {
		return noOfRecordsInserted;
	}

	public void setNoOfRecordsInserted(int noOfRecordsInserted) {
		this.noOfRecordsInserted = noOfRecordsInserted;
	}

	public List<ErrorObject> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<ErrorObject> errorMessages) {
		this.errorMessages = errorMessages;
	}

	
}
