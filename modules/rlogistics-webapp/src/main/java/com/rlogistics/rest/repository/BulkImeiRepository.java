package com.rlogistics.rest.repository;

import com.rlogistics.customqueries.BulkImeiCustomQueriesMapper;
import com.rlogistics.customqueries.BulkImeiNoOfRetaier;
import com.rlogistics.rest.services.util.DateUtil;
import lombok.var;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repo to get imei number of bulk tickets
 * @author Adarsh
 */
@Repository
@var
public class BulkImeiRepository {
    private ProcessEngineConfigurationImpl configuration;

    private DateUtil dateUtil;

    @Autowired
    public BulkImeiRepository(ProcessEngineConfigurationImpl configuration, DateUtil dateUtil) {
        this.configuration = configuration;
        this.dateUtil = dateUtil;
    }

    /**
     * Method to get the Imei num of retailer based on filter
     *
     * @param startDate  Start Date
     * @param endDate    End Date
     * @param retailerId RetailerId
     * @return List of DTO containing IMEI number.
     */
    public synchronized List<BulkImeiNoOfRetaier> getFedistanceDetails(String startDate, String endDate, String retailerId) {
        try (SqlSession session = configuration.getSqlSessionFactory().openSession()) {


            var mapper = session.getMapper(BulkImeiCustomQueriesMapper.class);
            return mapper.
                    getImeibyDate(
                            dateUtil.getDateByReducingDate(startDate, 0, "yyyy-MM-dd"),
                            dateUtil.getDateByAddingDate(endDate, 1, "yyyy-MM-dd"),
                            retailerId);

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
