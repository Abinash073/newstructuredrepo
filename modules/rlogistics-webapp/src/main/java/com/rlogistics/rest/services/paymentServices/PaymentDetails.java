package com.rlogistics.rest.services.paymentServices;


import com.rlogistics.customqueries.PaymentDetailsCustomQueriesMapper;
import com.rlogistics.customqueries.PaymentDetailsData;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to fetch payment data
 *
 * @author Adarsh
 */
@Service(value = "paymentDetailsRepository")
@Primary
@Slf4j
public class PaymentDetails {
    private ProcessEngineConfigurationImpl configuration;


    @Autowired
    public PaymentDetails(ProcessEngineConfigurationImpl configuration) {
        this.configuration = configuration;
    }

    /**
     * Get payment data based on filters
     *
     * @param startDate    Start date passed from angular
     * @param endDate      End Date passed from front end
     * @param retailerName Retailer name passed from front End
     * @param ticketNo     ticket no passed from front End
     * @return List of payment details data
     */
    public List<PaymentDetailsData> getPaymentDataByDate(String startDate, String endDate, String retailerName, String ticketNo) {
        try (SqlSession sqlSession = configuration.getSqlSessionFactory().openSession()) {
            return sqlSession
                    .getMapper(PaymentDetailsCustomQueriesMapper.class)
                    .getPaymentDetailsData(startDate, endDate, retailerName, ticketNo);

        } catch (Exception e) {
            log.debug("Error" + e);
            return null;
        }
    }
}
