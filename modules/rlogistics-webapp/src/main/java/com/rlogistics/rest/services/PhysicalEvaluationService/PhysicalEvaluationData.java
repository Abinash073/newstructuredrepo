package com.rlogistics.rest.services.PhysicalEvaluationService;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.rest.services.helper.ResponseForExcel;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.rlogistics.rest.services.excelWriter.BulkPhysicalEvalImpl.getPhysicalEvaluationDataDTO;

/**
 * Service is responsible to fetch the physical eval data  from Validata webservice
 *y
 * @author Adarsh
 */
@Service
@val
@Slf4j
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PhysicalEvaluationData {

    private ObjectMapper mapper;
    private RestTemplate restTemplate;


    @Value("${${spring.profiles.active}-validata-url}")
    private String validataUrl;

    @Value("${${spring.profiles.active}-categoryId}")
    private String categoryId;

    @Value("${${spring.profiles.active}-serviceId-forBulk}")
    private String serviceId;

    @Value("${validata-uri-excel-data}")
    private String uriForExcel;

    @Value("${validata-uri-header-data}")
    private String uriForHeader;

    @Autowired
    public PhysicalEvaluationData(RestTemplate restTemplate, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.mapper = mapper;
    }

    /**
     * Get the physical question and answers from validata  based on processId of ticket
     *
     * @param processId process id of ticket
     * @return DTO of Imei number having questions and there answers
     */
    public ImeiNumberWithQandAnsDTO getData(String processId) {
        ImeiNumberWithQandAnsDTO qandAnsDTO = new ImeiNumberWithQandAnsDTO();
        try {

            UriComponentsBuilder builder =
                    UriComponentsBuilder
                            .fromUriString(validataUrl + "resutlts/all")
                            .queryParam("processId", processId);


            RequestEntity<Void> entity = RequestEntity.get(new URI(builder.toUriString())).accept(MediaType.APPLICATION_JSON).build();

            ResponseEntity<PhysicalEvalReponseEntity> responseEntity = restTemplate.exchange(entity, PhysicalEvalReponseEntity.class);

            if (responseEntity.getStatusCodeValue() == HttpStatus.OK.value()) {
                PhysicalEvalReponseEntity physicalEvalReponseEntity = responseEntity.getBody();

                assert physicalEvalReponseEntity != null;

                List<EnngResponse> enggResponses = physicalEvalReponseEntity.getEngineer_response();

                List<PhysicalEvaluationDataDTO> qAndAnsDataList = enggResponses.stream().map(this::convertToDto).collect(Collectors.toList());

                qandAnsDTO.setData(qAndAnsDataList);
                qandAnsDTO.setProcessId(processId);

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return qandAnsDTO;

    }


    /**
     * This will give the headers <b>[Questions]</b> that needs to be added in Excel sheet
     *
     * @param retailerId retailer Id
     * @return Hashmap of questions and empty value
     * @throws URISyntaxException
     * @throws IOException
     */
    public LinkedHashMap<String, String> getHeader(String retailerId) throws URISyntaxException, IOException {
        LinkedHashMap<String, String> header = new LinkedHashMap<>();
        header.put("IMEI No", "n/a");
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(validataUrl + uriForHeader)
                .queryParam("retailer_id", retailerId)
                .queryParam("category_id", categoryId)
                .queryParam("service_id", serviceId);

        RequestEntity<Void> reqEntity = RequestEntity.get(new URI(builder.toUriString())).accept(MediaType.APPLICATION_JSON).build();

        ResponseEntity<String> responseEntity = restTemplate.exchange(reqEntity, String.class);
        log.debug(responseEntity.toString());

        if (responseEntity.getStatusCodeValue() == HttpStatus.OK.value()) {
            Iterator<JsonNode> i = mapper.readTree(responseEntity.getBody()).findValue("res").elements();
            while (i.hasNext()) {
                header.put(i.next().get("question").asText(), "n/a");
            }
        }
        return header;
    }

    /**
     * Converts Engg response to A specific Domain Object
     *
     * @param enngResponse Engg response from validata
     * @return Dto for physical Eval data
     */
    private PhysicalEvaluationDataDTO convertToDto(EnngResponse enngResponse) {
        return getPhysicalEvaluationDataDTO(enngResponse.getQuestion_type(), enngResponse.getAnswers(), enngResponse.getSelected_answer(), enngResponse.getQuestion());
    }

    /**
     * This will get the question and answers for series of imei numbers
     *
     * @param imeiList List of Imei
     * @return List of question and answer data
     * @throws URISyntaxException
     * @throws IOException
     */
    public List<ResponseForExcel> getDataForExcel(String imeiList) throws URISyntaxException, IOException {

        val uriComponentBuilder =
                UriComponentsBuilder
                        .fromUri(new URI(validataUrl + uriForExcel))
                        .queryParam("processId", imeiList)
                        .build();
        val reqEntity = RequestEntity.get(uriComponentBuilder.toUri()).accept(MediaType.APPLICATION_JSON).build();
        val responseEntity = restTemplate.exchange(reqEntity, String.class);
        assert responseEntity.getStatusCodeValue() == 200;
        JsonNode node = mapper.readTree(responseEntity.getBody());
        JsonNode responseNode = node.get("response");
        log.debug(responseNode.toString());
        return mapper.readValue(responseNode.toString(), new TypeReference<List<ResponseForExcel>>() {
        });


    }

}
