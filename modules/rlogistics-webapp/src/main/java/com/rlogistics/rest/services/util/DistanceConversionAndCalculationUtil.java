package com.rlogistics.rest.services.util;


import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Utility class to convert the distance into a specific unit and them calculate the total distance
 * Builder class ! call it in a builder way//,
 *
 * @author Adarsh
 */
@Service
@Primary
@Scope(value
        = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DistanceConversionAndCalculationUtil {
    private BigDecimal decimal = new BigDecimal("0.0");

    /**
     * converts KM distance to m and then add it to the next value in the list
     *
     * @param distanceData List of all distance data
     * @return Class object
     */
    public DistanceConversionAndCalculationUtil getDistance(ArrayList<String> distanceData) {
        for (String s : distanceData) {
            if (s.contains("km")) {
                decimal = decimal.add(new BigDecimal(s.replaceAll("\\b(km)\\b", "").trim()).multiply(new BigDecimal("1000")));
            } else if (s.contains("m")) {
                decimal = decimal.add(new BigDecimal(s.replaceAll("\\b(m)\\b", "").trim()));
            }
        }
        return this;
    }

    /**
     * Converts the meter distance to KM [ default round mode is 2 ]
     *
     * @return Distance in Big Decimal
     */
    public BigDecimal convertMeterToKm() {
        return decimal.divide(new BigDecimal("1000"), 2);
    }

    /**
     * Casting of string to big decimal
     *
     * @param s Distance data in {@code String}
     * @return Distance Data in {@code Bigdecimal}
     */
    public BigDecimal changeStringToBigDecimal(String s) {
        if (s.contains("km")) {
            return new BigDecimal(s.replaceAll("\\b(km)\\b", "").trim());
        } else if (s.contains("m")) {
            return new BigDecimal(s.replaceAll("\\b(m)\\b", "").trim()).divide(new BigDecimal("1000"), 3, BigDecimal.ROUND_HALF_UP);
        }
        return new BigDecimal(0.0);
    }
}
