package com.rlogistics.rest.constants;


import com.google.maps.model.LatLng;
import org.springframework.stereotype.Component;

import java.util.TreeMap;


/**
 * Class which stores all the locations hub address in lat long format
 * Use this class to add new hub locations lat long details
 * @author Adarsh
 */
@Component(value = "locationWiseLatLongDetails")
public class LocationWiseLatLong {

    private static TreeMap<String, LatLng> _bizlogLocationLatLongDetails = new TreeMap<>(String::compareToIgnoreCase);

    static {
        populateLatLong();
    }

    //TODO Add all the location details
    private static void populateLatLong() {
        _bizlogLocationLatLongDetails.put("Ahmedabad", add(23.0550214, 72.5547739));
        _bizlogLocationLatLongDetails.put("Bangalore", add(12.957092, 77.589971));
        _bizlogLocationLatLongDetails.put("Chandigarh", add(30.720016, 76.833449));
        _bizlogLocationLatLongDetails.put("Delhi", add(28.532731, 77.273386));
        _bizlogLocationLatLongDetails.put("Hubli", add(15.348231, 75.133372));
        _bizlogLocationLatLongDetails.put("Jaipur", add(26.942605, 75.845032));
        _bizlogLocationLatLongDetails.put("Kolkata", add(22.512421, 88.328539));
        _bizlogLocationLatLongDetails.put("Lucknow", add(26.900270, 80.931077));
        _bizlogLocationLatLongDetails.put("Hyderabad", add(17.446476, 78.465932));
    }


    private static LatLng add(Double lat, Double lng) {
        return new LatLng(lat, lng);
    }

    public LatLng getLatLongDetails(String location) {
        return _bizlogLocationLatLongDetails.get(location);
    }

}