package com.rlogistics.rest.services.ticketCreationService;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.exceptions.MySQLTransactionRollbackException;
import com.rlogistics.excel.Filter.ExcelFilter;
import com.rlogistics.excel.Reader.ExcelReader;
import com.rlogistics.excel.Reader.FilterExcelResult;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.ServiceMaster;
import com.rlogistics.master.identity.impl.persistence.entity.AdditionalProductDetailsEntity;
import com.rlogistics.rest.FormFillerUtil;
import com.rlogistics.rest.TicketCreationResult;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Service which creates Bulk ticket
 * 85
 *
 * @author Adarsh
 */
@Slf4j
@Service
@DependsOn({"excelReader", "masterdataService"})
public class BulkTicketCreationService {

    private static final String dataset = "BizLog";
    private ExcelReader excelReader;
    private MasterdataService masterdataService;
    private MetadataService metadataService;
    private RepositoryService repositoryService;
    private FormService formService;
    private RuntimeService runtimeService;

    private ObjectMapper mapper;

    private String processIdOfTicket = "";


    @Autowired
    public BulkTicketCreationService(ExcelReader excelReader, MasterdataService masterdataService, MetadataService metadataService, RepositoryService repositoryService, FormService formService, RuntimeService runtimeService, ObjectMapper mapper) {
        this.excelReader = excelReader;
        this.masterdataService = masterdataService;
        this.metadataService = metadataService;
        this.repositoryService = repositoryService;
        this.formService = formService;
        this.runtimeService = runtimeService;
        this.mapper = mapper;
    }

    /**
     * Creates bulk ticket by calling activti api
     *
     * @param stream     Excel file as input stream
     * @param retailerId Retailer Id
     * @return DTO of Ticket Creation Result
     * @throws IOException
     * @throws InvalidFormatException
     * @throws IllegalAccessException
     */
    public TicketCreationResult processBulkTicket(InputStream stream, String retailerId) throws IOException, InvalidFormatException, IllegalAccessException {
        // TODO: 23-Apr-19 validation of flow! :(

        TicketCreationResult creationResult = new TicketCreationResult();


        //TODO By default reading sheet no 1
        FilterExcelResult result =
                excelReader
                        .initExcel(stream, 0)
                        .readHeader()
                        .readRows()
                        .doFilter(ExcelFilter.FilterPolicy.BULK_FLOW_FILTER);

        if (result.isSuccess()) {
            List<Map<String, String>> dataWithDupImei = excelReader.getObject();


            Set<String> temp = new HashSet<>();
            List<Map<String, String>> data =

                    dataWithDupImei
                            .stream()
                            .filter(s -> temp.add(s.get("identificationNo")))
                            .collect(Collectors.toList());

            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.submit(temp::clear);


            //Grouping by map of drop location
            //Map<String, List<Map<String, String>>> b = data.stream().collect(Collectors.groupingBy(this::groupingOfMap));

            // b.keySet().forEach(log::debug);

            // List<Boolean> passFailList = b.entrySet().stream().map(a -> createBulkTicket(a.getValue(), retailerId)).collect(Collectors.toList());

            try {
                createBulkTicket(data, retailerId);
            } catch (Exception e) {
                log.debug("Error" + e);
                creationResult.setSucess(false);
                creationResult.setMessage("Oops! Some Error Occurred!");
                return creationResult;
            }


            // Map<Boolean, Long> countOfpassFail = passFailList.stream().collect(Collectors.groupingBy(Boolean::booleanValue, Collectors.counting()));

            // int countOfPass = Math.toIntExact(countOfpassFail.get(true) == null ? 0 : countOfpassFail.get(true));

            // int countOfFail = Math.toIntExact(countOfpassFail.get(false) == null ? 0 : countOfpassFail.get(false));


            //          StringBuffer message = new StringBuffer();

//          message = countOfPass > 0 ? message.append("SuccessFully Created ").append(countOfPass).append(" Ticket[s]") : new StringBuffer();

            //          message = countOfFail > 0 ? message.append("Failed To Create ").append(countOfFail).append(" Ticket[s]") : new StringBuffer();


            creationResult.setFilterExcelResult(result);
            creationResult.setSucess(true);
            //creationResult.setMessage(message.toString());

        } else {
            creationResult.setFilterExcelResult(result);
            creationResult.setSucess(false);
            creationResult.setMessage("Oops..Excel Validation failed!");
        }

        return creationResult;
    }

    /**
     * Activiti call to create a mass(Bulk) flow ticket
     *
     * @param data       Excel data converted to List of Map [ key : header ,value :  row data ]  of data
     * @param retailerId Retailer Id
     * @return boolean
     * Added Retry functionality so that can be retried when specific exception occurs
     * Exceptions from SQL and DEADLOCK exception will be retried for 3 times with a gap of 3 second
     */
    @Retryable(value = {SQLException.class, MySQLTransactionRollbackException.class}, backoff = @Backoff(delay = 3000))
    public String createBulkTicket(List<Map<String, String>> data, String retailerId) {

        final String processName = "mass_flow";

        Map<String, Object> variablesToBeset = new HashMap<>();
        Map<String, String> extraData = new HashMap<>();

        variablesToBeset.put("noOfQuantity", data.size());

        Retailer retailer = masterdataService
                .createRetailerQuery()
                .id(retailerId)
                .singleResult();

        data.get(0).put("retailer", retailer.getName());

        log.debug(metadataService.getCurrentDsVersion(dataset) + " - " + dataset + " - " + processName);

        ProcessDeployment processDeployment =
                metadataService
                        .createProcessDeploymentQuery()
                        .dataset(dataset)
                        .dsVersion(metadataService.getCurrentDsVersion(dataset))
                        .name(processName)
                        .singleResult();

        log.debug(processDeployment.getName());

        ProcessDefinition processDefinition =
                repositoryService
                        .createProcessDefinitionQuery()
                        .deploymentId(processDeployment.getDeploymentId())
                        .singleResult();

        FormFillerUtil fillerUtil = new FormFillerUtil(processDeployment.getDeploymentId(), false);


        ProcessInstance processInstance = (ProcessInstance) fillerUtil
                .submitForm(
                        (FormFillerUtil.FormSubmissionAction<ProcessInstance>) data1 ->
                                formService
                                        .submitStartFormData(processDefinition.getId(), data.get(0))
                );


        String processId = processInstance.getProcessInstanceId();


        //Adding service Id and product ID //TODO
        ServiceMaster serviceMaster = masterdataService.createServiceMasterQuery().name("BULK").singleResult();
        runtimeService.setVariable(processId, "rlServiceId", serviceMaster.getId());
//        String productId = masterdataService.createProductCategoryQuery().name("Mobile").singleResult().getId();
        String productId = masterdataService.createProductCategoryQuery().name(runtimeService.getVariable(processId, "productCategory").toString()).singleResult().getId();//prem change

        runtimeService.setVariable(processId, "rlProductCategoryId", productId);


        String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
        extraData.put("processId", processId);
        extraData.put("ticketNo", ticketNo);
        processIdOfTicket = processId;
        log.debug("ProcessId" + "---" + processId);
        variablesToBeset.put("processId", processId);

        log.debug("Ticket No" + "---" + String.valueOf(runtimeService.getVariable(processId, "rlTicketNo")));

        //Setting all the extra variables
        setVariables(processId, variablesToBeset);
        populateAdditionalProductList(data, extraData, retailer);

        return ticketNo;


    }

    private void setVariables(String p, Map<String, Object> v) {
        v.forEach((s, o) -> runtimeService.setVariable(p, s, o));
    }

    private void populateAdditionalProductList(List<Map<String, String>> additionalData, Map<String, String> additinalValue, Retailer retailer) {


        additionalData.forEach(s -> addData(s, additinalValue, retailer));
    }

    /**
     * Adding data of each bulk item to Additional product details table
     *
     * @param m        old data of item that we get from Excel
     * @param value    new data after main ticket creation
     * @param retailer Retailer Entity
     */
    //Not safe  // TODO
    private void addData(Map<String, String> m, Map<String, String> value, Retailer retailer) {
        try {
            //Configuring mapper
            mapper
                    .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
                    .configure(MapperFeature.AUTO_DETECT_FIELDS, true)
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


            AdditionalProductDetailsEntity entity = mapper.convertValue(m, AdditionalProductDetailsEntity.class);
            entity.setId(UUID.randomUUID().toString());
            entity.setRetailerId(retailer.getId());
            entity.setRetailerName(retailer.getName());
            entity.setStatus("new");
            entity.setBoxNumber("empty");
            entity.setMainTicketRef(value.get("ticketNo"));
            entity.setProcessId(value.get("processId"));
            entity.setBarcode("");
            masterdataService.saveAdditionalProductDetails(entity);
        } catch (Exception e) {
            log.debug("Error" + e);
            runtimeService.deleteProcessInstance(processIdOfTicket, "Unable to add excel data to table");

        }
    }

    @SuppressWarnings("unused")
    private String groupingOfMap(Map<String, String> a) {

        String temp = a.get("dropLocation").replaceAll("\\s", "").trim() + a.get("dropLocCity").replaceAll("\\s", "").trim();
        return temp.toLowerCase();

    }


}



