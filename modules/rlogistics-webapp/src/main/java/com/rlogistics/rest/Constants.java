package com.rlogistics.rest;

public enum  Constants{
    BASE_DEV_URL("http://54.69.180.84:9080"),
    BASE_TEST_URL("http://test.bizlog.in:9080"),
    BASE_PROD_URL("http://rl.bizlog.in:8080"),
    EXOTEL_CALL_CONNECT("https://bizlog:72c2bb557c3aba2f12ea4b83945ba69dcf19f097@api.exotel.com/v1/Accounts/bizlog/Calls/connect.json"),
    EXOTEL_NUMBER_WHITELIST("https://bizlog:72c2bb557c3aba2f12ea4b83945ba69dcf19f097@api.exotel.com/v1/Accounts/bizlog/CustomerWhitelist.json"),
    REV_REVERSE_STATUS_CALLBACK(BASE_PROD_URL+"/rlogistics-execution/rlservice/call/status"),
    DEFAULT_EXOTEL_NUMBER("08047493605");

    private String url;

    Constants(String envurl){
        this.url = envurl;

    }

    public String getUrl() {

        return url;
    }

    @Override
    public String toString() {
        return this.url;
    }
}
