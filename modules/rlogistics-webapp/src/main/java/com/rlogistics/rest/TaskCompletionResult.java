package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.List;

import com.rlogistics.rest.FormFillerUtil.GroupDescription;

public class TaskCompletionResult {

	private boolean success = false;
	private GroupDescription groupDescription;
	private List<String> notifications = new ArrayList<>();
	
	public TaskCompletionResult(List<String> notifications){
		
		if((notifications != null) && !notifications.isEmpty()){
			getNotifications().addAll(notifications);
		}
		
		this.setSuccess(true);
	}
	
	public TaskCompletionResult(GroupDescription groupDescription){
		this.setGroupDescription(groupDescription);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public GroupDescription getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(GroupDescription groupDescription) {
		this.groupDescription = groupDescription;
	}

	public List<String> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<String> notifications) {
		this.notifications = notifications;
	}
}
