package com.rlogistics.rest.bulkApiForClient.services;

import com.rlogistics.rest.bulkApiForClient.domain.DropDetails;
import com.rlogistics.rest.bulkApiForClient.domain.FieldVal;
import com.rlogistics.rest.bulkApiForClient.domain.PickUpDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Primary
@Valid
public class DataToListOfMapService {

    private FormattingServiceForBulk formattingServiceForBulk;

    @Autowired
    public DataToListOfMapService(FormattingServiceForBulk formattingServiceForBulk) {
        this.formattingServiceForBulk = formattingServiceForBulk;
    }

    public List<Map<String, String>> getDataInRequiredFormat(@Valid FieldVal fieldVal) {
        PickUpDetails pickUpData = fieldVal.getPickUpDetails();
        HashMap<String, String> formatedPickUpData = formattingServiceForBulk.format(pickUpData);

        List<DropDetails> itemsData = fieldVal.getItems();
        List<Map<String, String>> c = itemsData
                .stream()
                .map(a -> formattingServiceForBulk.format(a))
                .collect(Collectors.toList());
        c.forEach(d -> d.putAll(formatedPickUpData));
        return c;

    }


}
