package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductPacking;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.ServiceProvider;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;
import com.rlogistics.ui.CSVDataProcessor;
import com.rlogistics.util.ValidateUtil;
import com.vaadin.data.Validatable;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.Brand.BrandQuery;
import com.rlogistics.master.identity.MasterCategory;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.MisBulkTransfer;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingType;

@RestController
public class ProductResource extends RLogisticsResource {

	private static Logger log = LoggerFactory.getLogger(ProductResource.class);

	@RequestMapping(value = "/product/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProductCommands(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return new ProductCommandProcessor().productUpload(file.getInputStream());
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	@RequestMapping(value = "/product/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProductCommandsExcel(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "retailerId") String retailerId) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return new ProductCommandProcessorExcel().productUpload(file.getInputStream(), file, retailerId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	@RequestMapping(value = "/additional-file/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult additionalFileUpload(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "processId") String processId) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return new AdditionalFileCommandProcessorExcel().additionalFileUpload(file.getInputStream(), file,
					processId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	@RequestMapping(value = "/product-category/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProductCategoryCommandsExcel(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return new ProductCategoryCommandProcessorExcel().productCategoryUpload(file.getInputStream(), file);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	@RequestMapping(value = "/brand/upload/excel", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadBrandCommandsExcel(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return new BrandCommandProcessorExcel().brandUpload(file.getInputStream(), file);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	protected class ProductCommandProcessor extends CSVDataProcessor {

		public UploadProcessCommandsResult productUpload(InputStream is) {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();

			try {
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String record = null;
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				while ((record = bis.readLine()) != null) {
					recordNo++;

					String[] recordFields = parseLine(record, recordNo);

					if (recordFields == null || recordFields.length == 0)
						continue;

					if (recordNo == 1) {
						/* Header record */
						for (int i = 0; i < recordFields.length; ++i) {
							orderedFields.add(recordFields[i]);
						}
						if (log.isDebugEnabled()) {
							log.debug("HEADER:" + recordNo + ":" + record);
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
						continue;
					}

					if (log.isDebugEnabled()) {
						log.debug("PROCESSING:" + recordNo + ":" + record);
						for (String f : recordFields) {
							log.debug("------------" + f);
						}
					}

					final Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();

					for (int i = 0; i < recordFields.length; ++i) {
						fieldValuesPreFilter.put(orderedFields.get(i), recordFields[i]);
					}

					final Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);

					String subCategory = fieldValues.get("subCategory");
					String brand = fieldValues.get("brand");
					String packagingType = fieldValues.get("packagingType");

					ProductSubCategory productSubCategoryObj = null;
					try {
						productSubCategoryObj = masterdataService.createProductSubCategoryQuery().name(subCategory)
								.singleResult();
					} catch (Exception e) {
						// retval.getErrorMessages().add("Ignoring line " +
						// recordNo + ":" + record + ": " + e.getMessage());
						continue;
					}

					Brand brandObj = null;
					try {
						brandObj = masterdataService.createBrandQuery().name(brand).singleResult();
					} catch (Exception e) {
						// retval.getErrorMessages().add("Ignoring line " +
						// recordNo + ":" + record + ": " + e.getMessage());
						continue;
					}

					PackagingType packagingTypeObj = null;
					try {
						packagingTypeObj = masterdataService.createPackagingTypeQuery().name(packagingType)
								.singleResult();
					} catch (Exception e) {
						// retval.getErrorMessages().add("Ignoring line " +
						// recordNo + ":" + record + ": " + e.getMessage());
						continue;
					}

					String subCategoryId = null;
					String brandId = null;
					String packagingTypeId = null;

					if (productSubCategoryObj != null) {
						subCategoryId = productSubCategoryObj.getId();
					}
					if (brandObj != null) {
						brandId = brandObj.getId();
					}
					if (packagingTypeObj != null) {
						packagingTypeId = packagingTypeObj.getId();
					}

					if (subCategoryId != null) {
						if (brandId != null) {
							if (packagingTypeId != null) {
								Product product = masterdataService.newProduct();
								Calendar c = Calendar.getInstance();
								c.setTime(new Date());
								String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
										+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
								product.setId(id);
								product.setName(fieldValues.get("name"));
								product.setCode(fieldValues.get("code"));

								if (!fieldValues.get("description").equals("")) {
									product.setDescription(fieldValues.get("description"));
								}
								product.setSubCategoryId(subCategoryId);
								product.setModel(fieldValues.get("model"));
								product.setPackagingTypeBoxId(packagingTypeId);
								product.setBrandId(brandId);
								if (!fieldValues.get("volumetricWeight").equals("")) {
									product.setVolumetricWeight(Double.valueOf(fieldValues.get("volumetricWeight")));
								}
								if (!fieldValues.get("approxValue").equals("")) {
									product.setApproxValue(Double.valueOf(fieldValues.get("approxValue")));
								}
								if (fieldValues.get("actualWeight") != null) {
									product.setActualWeight(Double.valueOf(fieldValues.get("actualWeight")));
								}

								try {
									masterdataService.saveProduct(product);
									insertCount++;
								} catch (Exception e) {
									// retval.getErrorMessages().add("Ignoring
									// line " + recordNo + ":" + record + ".
									// Reason: Duplicate record");
									continue;
								}

							} else {
								// retval.getErrorMessages().add("Ignoring line
								// " + recordNo + ":" + record + ". Reason:
								// packagingType does not exist");
							}
						} else {
							// retval.getErrorMessages().add("Ignoring line " +
							// recordNo + ":" + record + ". Reason: brand does
							// not exist");
						}
					} else {
						// retval.getErrorMessages().add("Ignoring line " +
						// recordNo + ":" + record + ". Reason: subCategory does
						// not exist");
					}

				}
				retval.setNoOfRecordsInserted(insertCount);
			} catch (IOException ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}

			return retval;
		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}

	protected class ProductCommandProcessorExcel {

		public UploadProcessCommandsResult productUpload(InputStream is, MultipartFile file, String retailerId) {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {

				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String fileName = file.getName();
				Workbook workbook = null;

				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {

				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();

				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo = 1;

					try {
						/* Header record */
						for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
							orderedFields.add(firstRow.getCell(fr).getStringCellValue());
						}
						if (log.isDebugEnabled()) {
							log.debug("HEADER:" + recordNo + ":" + firstRow.toString());
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
					} catch (Exception hex) {

					}

					for (int r = 1; r <= totalRowCount; r++) {

						recordNo++;

						if (sheet.getRow(r) != null) {

							Row row = sheet.getRow(r);
							final Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
							for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
								if (row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if (!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}

							final Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);

							String subCategory = fieldValues.get("subCategory");
							String brand = fieldValues.get("brand");
							String packagingTypeBox = fieldValues.get("packagingTypeBox");
							String packagingTypeCover = null;
							if (fieldValues.get("packagingTypeCover") != null
									&& !fieldValues.get("packagingTypeCover").equals("")) {
								packagingTypeCover = fieldValues.get("packagingTypeCover");
							}

							ProductCategory productCategory = null;
							ProductSubCategory productSubCategoryObj = null;
							try {
								productCategory = masterdataService.createProductCategoryQuery()
										.name(fieldValues.get("category")).singleResult();
								if (productCategory != null) {
									productSubCategoryObj = masterdataService.createProductSubCategoryQuery()
											.name(subCategory).productCategoryId(productCategory.getId())
											.singleResult();
								} else {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("Category does not exist");
									retval.getErrorMessages().add(errorObject);
								}

							} catch (Exception e) {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason(e.getMessage());
								retval.getErrorMessages().add(errorObject);
								continue;
							}

							Brand brandObj = null;
							try {
								brandObj = masterdataService.createBrandQuery().name(brand).singleResult();
							} catch (Exception e) {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason(e.getMessage());
								retval.getErrorMessages().add(errorObject);
								continue;
							}

							PackagingType packagingTypeBoxObj = null;
							try {
								packagingTypeBoxObj = masterdataService.createPackagingTypeQuery()
										.name(packagingTypeBox).singleResult();
							} catch (Exception e) {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason(e.getMessage());
								retval.getErrorMessages().add(errorObject);
								continue;
							}

							PackagingType packagingTypeCoverObj = null;
							if (packagingTypeCover != null) {
								try {
									packagingTypeCoverObj = masterdataService.createPackagingTypeQuery()
											.name(packagingTypeCover).singleResult();
								} catch (Exception e) {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason(e.getMessage());
									retval.getErrorMessages().add(errorObject);
									continue;
								}
							}

							String subCategoryId = null;
							String brandId = null;
							String packagingTypeBoxId = null;
							String packagingTypeCoverId = null;

							if (productSubCategoryObj != null) {
								subCategoryId = productSubCategoryObj.getId();
							}
							if (brandObj != null) {
								brandId = brandObj.getId();
							}
							if (packagingTypeBoxObj != null) {
								packagingTypeBoxId = packagingTypeBoxObj.getId();
							}
							if (packagingTypeCoverObj != null) {
								packagingTypeCoverId = packagingTypeCoverObj.getId();
							}

							if (subCategoryId != null) {
								if (brandId != null) {
									if (packagingTypeBoxId != null) {
										Product product = masterdataService.newProduct();
										UUID id = UUID.randomUUID();
										product.setId(String.valueOf(id));
										product.setName(fieldValues.get("name"));
										product.setCode(fieldValues.get("code"));

										if (fieldValues.get("description") != null
												&& !fieldValues.get("description").equals("")) {
											product.setDescription(fieldValues.get("description"));
										}
										product.setSubCategoryId(subCategoryId);
										product.setModel(fieldValues.get("model"));
										product.setPackagingTypeBoxId(packagingTypeBoxId);
										product.setRetailerId(retailerId);
										if (packagingTypeCoverId != null) {
											product.setPackagingTypeCoverId(packagingTypeCoverId);
										}
										product.setBrandId(brandId);
										if (fieldValues.get("volumetricWeight") != null) {
											if (!fieldValues.get("volumetricWeight").equals("")) {
												product.setVolumetricWeight(
														Double.valueOf(fieldValues.get("volumetricWeight")));
											}
										}
										if (fieldValues.get("approxValue") != null) {
											if (!fieldValues.get("approxValue").equals("")) {
												product.setApproxValue(Double.valueOf(fieldValues.get("approxValue")));
											}
										}
										if (fieldValues.get("actualWeight") != null) {
											if (!fieldValues.get("actualWeight").equals("")) {
												product.setActualWeight(
														Double.valueOf(fieldValues.get("actualWeight")));
											}
										}

										try {
											masterdataService.saveProduct(product);
											insertCount++;

											// Add the product packing for box
											ProductPacking productPacking = masterdataService.newProductPacking();
											productPacking.setProductId(product.getId());
											productPacking.setPackagingTypeId(packagingTypeBoxId);
											productPacking.setType(0);
											productPacking.setPackingSpec("local");

											try {
												masterdataService.saveProductPacking(productPacking);
											} catch (Exception b) {

											}

											// Add the product packing for cover
											if (packagingTypeCoverId != null) {
												productPacking = masterdataService.newProductPacking();
												productPacking.setProductId(product.getId());
												productPacking.setPackagingTypeId(packagingTypeCoverId);
												productPacking.setType(1);
												productPacking.setPackingSpec("local");

												try {
													masterdataService.saveProductPacking(productPacking);
												} catch (Exception b) {

												}
											}

										} catch (Exception e) {
											ErrorObject errorObject = new ErrorObject();
											errorObject.setRowNo(recordNo);
											errorObject.setMessage("Ignoring line " + recordNo);
											errorObject.setReason("Duplicate Record");
											retval.getErrorMessages().add(errorObject);
											continue;
										}

									} else {
										ErrorObject errorObject = new ErrorObject();
										errorObject.setRowNo(recordNo);
										errorObject.setMessage("Ignoring line " + recordNo);
										errorObject.setReason("packagingType does not exist");
										retval.getErrorMessages().add(errorObject);
									}
								} else {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("brand does not exist");
									retval.getErrorMessages().add(errorObject);
								}
							} else {
								if (productCategory != null) {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("subCategory does not exist");
									retval.getErrorMessages().add(errorObject);
								}
							}

						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Record");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);

			} catch (Exception ex) {
				ErrorObject errorObject = new ErrorObject();
				errorObject.setReason(ex.getMessage());
				retval.getErrorMessages().add(errorObject);
			}
			return retval;
		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}

	protected class ProductCategoryCommandProcessorExcel {
		public UploadProcessCommandsResult productCategoryUpload(InputStream is, MultipartFile file) {

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {

				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String fileName = file.getName();
				Workbook workbook = null;
				/*
				 * if (fileName.toLowerCase().contains("xlsx")) { workbook = new
				 * XSSFWorkbook(is); } else { workbook = new XSSFWorkbook(is); }
				 */
				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {

				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();
				// record = bis.readLine();

				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo = 1;

					/* Header record */
					for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
						orderedFields.add(firstRow.getCell(fr).getStringCellValue());
					}
					if (log.isDebugEnabled()) {
						log.debug("HEADER:" + recordNo + ":" + record);
						for (String f : orderedFields) {
							log.debug("------------" + f);
						}
					}

					for (int r = 1; r <= totalRowCount; r++) {

						recordNo++;

						if (sheet.getRow(r) != null) {

							Row row = sheet.getRow(r);
							final Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
							for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
								if (row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if (!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}

							final Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);
							String masterCategory = fieldValues.get("masterCategory");
							MasterCategory masterCategoryObj = masterdataService.createMasterCategoryQuery()
									.name(masterCategory).singleResult();
							if (masterCategoryObj != null) {
								ProductCategory productCategory = masterdataService.newProductCategory();
								UUID id = UUID.randomUUID();
								productCategory.setId(String.valueOf(id));
								productCategory.setName(fieldValues.get("name"));
								productCategory.setCode(fieldValues.get("code"));
								productCategory.setMasterCategoryId(masterCategoryObj.getId());
								if (fieldValues.get("description") != null) {
									if (!fieldValues.get("description").equals("")) {
										productCategory.setDescription(fieldValues.get("description"));
									}
								}

								try {
									masterdataService.saveProductCategory(productCategory);
									insertCount++;
								} catch (Exception e) {
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("Duplicate Record");
									retval.getErrorMessages().add(errorObject);
									continue;
								}
							} else {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("MasterCategory does not exist");
								retval.getErrorMessages().add(errorObject);

							}

						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Records");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);

			} catch (Exception ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			return retval;
		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}

	protected class BrandCommandProcessorExcel {
		public UploadProcessCommandsResult brandUpload(InputStream is, MultipartFile file) {

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {

				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String fileName = file.getName();
				Workbook workbook = null;

				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {

				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();

				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo = 1;

					/* Header record */
					for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
						orderedFields.add(firstRow.getCell(fr).getStringCellValue());
					}
					if (log.isDebugEnabled()) {
						log.debug("HEADER:" + recordNo + ":" + record);
						for (String f : orderedFields) {
							log.debug("------------" + f);
						}
					}

					for (int r = 1; r <= totalRowCount; r++) {

						recordNo++;

						if (sheet.getRow(r) != null) {

							Row row = sheet.getRow(r);
							final Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
							for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
								if (row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell));
									if (!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}
								}
							}

							final Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);
							Brand brand = masterdataService.newBrand();
							String parentBrandId = null;
							if (fieldValues.get("parentBrand") != null && !fieldValues.get("parentBrand").equals("")) {
								BrandQuery parentBrandQuery = masterdataService.createBrandQuery();
								Brand parentBrand = parentBrandQuery.name(fieldValues.get("parentBrand"))
										.singleResult();
								if (parentBrand != null) {
									parentBrandId = parentBrand.getId();
								}
							}
							brand.setId(String.valueOf(UUID.randomUUID()));
							brand.setCode(fieldValues.get("code"));
							brand.setName(fieldValues.get("name"));
							if (parentBrandId != null) {
								brand.setParentBrandId(parentBrandId);
							}
							if (fieldValues.get("description") != null) {
								if (!fieldValues.get("description").equals("")) {
									brand.setDescription(fieldValues.get("description"));
								}
							}
							try {
								masterdataService.saveBrand(brand);
								insertCount++;
							} catch (Exception e) {
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("Duplicate record");
								retval.getErrorMessages().add(errorObject);
								continue;
							}
						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Records");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				retval.setNoOfRecordsInserted(insertCount);

			} catch (Exception ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			return retval;
		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}

	protected class AdditionalFileCommandProcessorExcel {

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();

		// ArrayList<String> boxNumberList = new ArrayList<String>();
		public UploadProcessCommandsResult additionalFileUpload(InputStream is, MultipartFile file, String processId) {

			// MasterdataService masterdataService =
			// ((RLogisticsProcessEngineImpl) (ProcessEngines
			// .getDefaultProcessEngine())).getMasterdataService();
			ArrayList<String> barCodeList = new ArrayList<String>();

			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			try {
				RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getRuntimeService();
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String fileName = file.getName();
				Workbook workbook = null;

				try {
					workbook = WorkbookFactory.create(is);
				} catch (Exception e) {

				}
				Cell cell = null;
				Sheet sheet;
				int totalRowCount = 0;

				String record = "";
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				DataFormatter dfmt = new DataFormatter();

				for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

					sheet = workbook.getSheetAt(j);
					totalRowCount = sheet.getLastRowNum();
					Row firstRow = sheet.getRow(0);
					recordNo = 1;

					/* Header record */
					for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
						orderedFields.add(firstRow.getCell(fr).getStringCellValue());
					}
					if (log.isDebugEnabled()) {
						log.debug("HEADER:" + recordNo + ":" + record);
						for (String f : orderedFields) {
							log.debug("------------" + f);
						}
					}
					log.debug("TotalRows : " + totalRowCount);
					System.out.println("TotalRows : " + totalRowCount);
					for (int r = 1; r <= totalRowCount; r++) {
						System.out.println("inside loop");
						recordNo++;

						if (sheet.getRow(r) != null) {

							Row row = sheet.getRow(r);
							Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
							for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
								if (row.getCell(i) != null) {
									cell = row.getCell(i);
									fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell).trim());
									System.out.println("added " + orderedFields.get(i));
									System.out.println("Value " + dfmt.formatCellValue(cell).trim());
									/*if (!record.equals("")) {
										record += "," + dfmt.formatCellValue(cell);
									} else {
										record += dfmt.formatCellValue(cell);
									}*/
								}
							}

							Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);

							// populate MIS table
							MisBulkTransfer misBulkTransfer = masterdataService.newMisBulkTransfer();

							misBulkTransfer.setProcessId(processId);
							misBulkTransfer.setAwbAmazon(fieldValues.get("awb"));
//							misBulkTransfer.setConsumerName(fieldValues.get("consumerName"));
							Map<String, Object> variables = runtimeService.getVariables(processId);

							int isValid = 1;
							// validate productCode
							/*
							 * int isValid = 1; List<Product> products = new
							 * ArrayList<Product>(); products =
							 * masterdataService.createProductQuery().retailerId
							 * (String.valueOf(variables.get("rlRetailerId"))).
							 * code(fieldValues.get("productCode")).list();
							 * if(products.size()==0) { isValid = 0; log.debug(
							 * "Product code not available"); ErrorObject
							 * errorObject = new ErrorObject();
							 * errorObject.setRowNo(recordNo);
							 * errorObject.setMessage("Ignoring line " +
							 * recordNo); errorObject.setReason(
							 * "Product Code is invalid");
							 * retval.getErrorMessages().add(errorObject); }
							 */
							misBulkTransfer.setProductCode(fieldValues.get("productCode"));
							misBulkTransfer.setStatus("open");

							if (variables.containsKey("rlRetailerId")) {
								misBulkTransfer.setRetailerId(String.valueOf(variables.get("rlRetailerId")));
								misBulkTransfer.setRetailer(String.valueOf(variables.get("retailer")));
							}
							if (variables.containsKey("rlDropLocationId")) {
								misBulkTransfer.setDropLocationId(String.valueOf(variables.get("rlDropLocationId")));
							}
							misBulkTransfer.setTicketNo(String.valueOf(variables.get("rlTicketNo")));
							if (variables.containsKey("rlDropLocationName")) {
								misBulkTransfer
										.setDropLocationName(String.valueOf(variables.get("rlDropLocationName")));
							}

							if (fieldValues.containsKey("description") && fieldValues.get("description") != null
									&& !fieldValues.get("description").equals("")) {
								misBulkTransfer.setDescription(fieldValues.get("description"));
							}
							if (fieldValues.containsKey("barcode") && fieldValues.get("barcode") != null
									&& !fieldValues.get("barcode").equals("")) {
								String barcode = fieldValues.get("barcode");
								if (fieldValues.get("barcode").length() > 10) {
									barcode = barcode.substring(1, 11);
								}
								misBulkTransfer.setBarcode(barcode);
								barCodeList.add(barcode);
							}
							if (fieldValues.containsKey("value") && fieldValues.get("value") != null
									&& !fieldValues.get("value").equals("")) {
								misBulkTransfer.setValue(fieldValues.get("value"));
							}
							
							if (fieldValues.containsKey("quantity") && fieldValues.get("quantity") != null
									&& !fieldValues.get("quantity").equals("")) {
								misBulkTransfer.setValue(fieldValues.get("quantity"));
							}
                            
							if (fieldValues.containsKey("productCategory") && fieldValues.get("productCategory") != null
									&& !fieldValues.get("productCategory").equals("")) {
								misBulkTransfer.setProductCategory(fieldValues.get("productCategory"));
							}
							
							if (fieldValues.containsKey("consumerName") && fieldValues.get("consumerName") != null
									&& !fieldValues.get("consumerName").equals("")) {
								misBulkTransfer.setConsumerName(fieldValues.get("consumerName"));
							}
							
							
							if (fieldValues.containsKey("dateOfReceipt") && fieldValues.get("dateOfReceipt") != null
									&& !fieldValues.get("dateOfReceipt").equals("")) {
								misBulkTransfer.setDateOfReceipt(fieldValues.get("dateOfReceipt"));
							}
							if (fieldValues.containsKey("address") && fieldValues.get("address") != null
									&& !fieldValues.get("address").equals("")) {
								misBulkTransfer.setAddress(fieldValues.get("address"));
							}
							
							if (fieldValues.containsKey("boxNumber") && fieldValues.get("boxNumber") != null
									&& !fieldValues.get("boxNumber").equals("")) {
								misBulkTransfer.setBoxNumber(fieldValues.get("boxNumber"));
								barCodeList.add(fieldValues.get("boxNumber"));

								String boxBarcodeType = fieldValues.get("boxNumber").substring(3, 5);
								if (boxBarcodeType.equals("40") || boxBarcodeType.equals("41")
										|| boxBarcodeType.equals("42")) {
									misBulkTransfer.setBoxNumber(fieldValues.get("boxNumber"));
								} else {
									isValid = 0;
									ErrorObject errorObject = new ErrorObject();
									errorObject.setRowNo(recordNo);
									errorObject.setMessage("Ignoring line " + recordNo);
									errorObject.setReason("Box Barcode is not of type Bulk Box");
									retval.getErrorMessages().add(errorObject);
								}
							}

							try {
									System.out.println("Before inserting");
									masterdataService.saveMisBulkTransfer(misBulkTransfer);
									insertCount++;
									
							} catch (Exception m) {
								System.out.println("Exception message " + m.getMessage());
								ErrorObject errorObject = new ErrorObject();
								errorObject.setRowNo(recordNo);
								errorObject.setMessage("Ignoring line " + recordNo);
								errorObject.setReason("Duplicate Record");
								retval.getErrorMessages().add(errorObject);
							}
						} else {
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Empty Records");
							retval.getErrorMessages().add(errorObject);
						}

					}
				}
				runtimeService.setVariable(processId, "productQuantity", insertCount);
				retval.setNoOfRecordsInserted(insertCount);
			} catch (Exception ex) {
				ErrorObject errorObject = new ErrorObject();
				errorObject.setMessage("Ignoring file upload");
				errorObject.setReason("Failed to read uploaded content");
				retval.getErrorMessages().add(errorObject);
			}
			try {
				//Use this method to run changeBarcodeStatus in Background
//				ExecutorService service = Executors.newFixedThreadPool(4);
//				service.submit(new Runnable() {
//					public void run() {
//						ValidateUtil.changeBarcodeStatus(barCodeList);
//						}
//					});

				changeBarcodeStatus(barCodeList);
			} catch (Exception e) {
				log.debug("error while Calling PackingInventory Method" + e);
			}
			return retval;
		}

		
		//Look for an option to do it in background(creating Thread)
		private void changeBarcodeStatus(ArrayList<String> barCodeList2) {
			PackagingMaterialInventory pmi = null;
			try {
				for (int i = 0; i < barCodeList2.size(); i++) {
					pmi = masterdataService.createPackagingMaterialInventoryQuery().barcode(barCodeList2.get(i))
							.singleResult();
					if (pmi != null){
						pmi.setStatus(1);
						masterdataService.savePackagingMaterialInventory(pmi);}
				}
				
			} catch (Exception e) {
				log.debug("error while updating Packing Inventory" + e);
			}

		}

		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}

}
