package com.rlogistics.rest.controllers.ticketCreationController;

import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.rest.TicketCreationResult;
import com.rlogistics.rest.services.ticketCreationService.BulkTicketCreationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controls the creation of Bulk Ticket
 * @author Adarsh
 */
@Slf4j
@RestController

public class BulkticketCreationController extends RLogisticsResource {

    private BulkTicketCreationService creationService;

    @Autowired
    public BulkticketCreationController(BulkTicketCreationService creationService) {
        this.creationService = creationService;
    }

    /**
     * Takes the multipart file from angular and creates Ticket
     * @param file Multipart File
     * @param retailerId Retailer id
     * @param request
     * @param response
     * @return DTO for ticket creation process result
     */
    @RequestMapping(value = "process/upload/bulk", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private TicketCreationResult createBulkTicketByExcel(@Param(value = "file") MultipartFile file,
                                                         @Param(value = "retailerId") String retailerId,
                                                         HttpServletRequest request,
                                                         HttpServletResponse response) {
        if (!beforeMethodInvocation(request, response)) {
            return null;
        }

        try {
            return creationService.processBulkTicket(file.getInputStream(), retailerId);


        } catch (Exception e) {
            log.debug("Error" + e.getCause());
            response.setStatus(500);
            return null;
        }

    }
}
