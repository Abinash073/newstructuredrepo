package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.lang.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONException;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.customqueries.ProcessResult;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerCategories;
import com.rlogistics.master.identity.RetailerOdaLocations;
import com.rlogistics.master.identity.RetailerOther;
import com.rlogistics.master.identity.RetailerProcessField;
import com.rlogistics.master.identity.RetailerReturnOrPickupLocations;
import com.rlogistics.master.identity.RetailerServiceableLocations;
import com.rlogistics.master.identity.RetailerServices;
import com.rlogistics.master.identity.ServiceMaster;
import com.rlogistics.master.identity.TicketImages;
import com.rlogistics.master.identity.TicketStatusHistory;
import com.rlogistics.master.identity.Brand.BrandQuery;
import com.rlogistics.master.identity.ConsoleLogs;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;
import com.rlogistics.rest.ProcessResource.ProcessCommandProcessor;
import com.rlogistics.rest.ProcessResource.ProcessCommandProcessorWithRenaming;
import com.rlogistics.ui.CSVDataProcessor;

import pojo.PhotoBeforePaking;
import pojo.ReportPojo;
import pojo.StatusHistory;

import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.CoordinatorServicablePincodes;
import com.rlogistics.master.identity.CoordinatorServicablePincodes.CoordinatorServicablePincodesQuery;
import com.rlogistics.master.identity.ImeiInventory;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.MasterCategory;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.cmd.DeleteProductCmd;
import com.rlogistics.master.identity.MisBulkTransfer;
import com.rlogistics.master.identity.OpenTickets;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.ImagesListResponse;
import com.rlogistics.rest.ImagesRestResponse;

/**
 * APIS
 * 
 * @author r ticket/create/pickanddrop/oneway ticket/create/pickanddrop/twoway
 *         /ticket/cancel
 */
@RestController
public class RestApi extends RLogisticsResource {
	// @Autowired
	// ObjectMapper objectMapper;

	public static Logger log = LoggerFactory.getLogger(RestApi.class);
	static final String PHOTOGRAPHBEFORE = "PhotographBefore";

	public ProcessInstance submitStartForm(FormService formService, ProcessDefinition definition,
			FormFillerUtil formFillerUtil) {
		ProcessInstance processInstance = (ProcessInstance) formFillerUtil
				.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
					@Override
					public ProcessInstance submitForm(Map<String, String> fieldValues) {
						return formService.submitStartFormData(definition.getId(), fieldValues);
					}
				});

		/*
		 * Prod release TODO: Leads to bugs -- hence disabling. But will need to
		 * be fixed when people start using start forms elaborately
		 * CommandExecutor commandExecutor =
		 * ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.
		 * getDefaultProcessEngine().getProcessEngineConfiguration())).
		 * getCommandExecutor(); commandExecutor.execute(new Command<Boolean>()
		 * {
		 *
		 * @Override public Boolean execute(CommandContext commandContext) {
		 * execution.setVariables(formFillerUtil.getVariables()); return true; }
		 * });
		 */
		return processInstance;
	}

	public String submitStartFormNew(FormService formService, ProcessDefinition definition,
			FormFillerUtil formFillerUtil) {
		ProcessInstance processInstance = (ProcessInstance) formFillerUtil
				.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
					@Override
					public ProcessInstance submitForm(Map<String, String> fieldValues) {
						return formService.submitStartFormData(definition.getId(), fieldValues);
					}
				});

		return processInstance.getId();
	}

	private boolean PNDOneWayPrimaryValidation(String queryJSON, RestExternalResult restResult, String retailerId) {

		/**
		 * productName model productCode brand identificationNo
		 */
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;
		String productNameerrorMessage = "";
		String modelerrorMessage = "";
		String productCodeerrorMessage = "";
		String branderrorMessage = "";
		String identificationNomodelerrorMessage = "";
		String productCategoryerrorMessage = "";

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {
			if (checkForNewRetailer(retailerId)) {
				value = checkForFieldsInLoopForNewRetailer(fieldValues, value, finalErrorMeesage);
			} else {
				value = checkForFieldsInLoopForOldRetailer(fieldValues, value, finalErrorMeesage);
			}

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;

	}

	private boolean checkForFieldsInLoopForNewRetailer(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}

			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;

	}

	private boolean checkForNewRetailer(String retailerId) {
		boolean rtnValue = false;
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			ArrayList<String> newRetailerName = new ArrayList<>();
			newRetailerName.add("OA");

			if (newRetailerName.contains(retailer.getCode().toString())) {
				rtnValue = true;
			}
		} catch (Exception e) {

		}

		return rtnValue;
	}

	private boolean checkForFieldsInLoopForOldRetailer(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		// fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;
	}

	/**
	 * This is for testing of bulk
	 * 
	 * @param runtimeService
	 * @param additionalSheet
	 */
	private void additionalSheetInsertion1(MasterdataService runtimeService, String additionalSheet) {
		JSONArray array = new JSONArray(additionalSheet);
		/*
		 * // populate MIS table MisBulkTransfer misBulkTransfer =
		 * runtimeService.newMisBulkTransfer();
		 * 
		 * misBulkTransfer.setProcessId(processId);
		 * misBulkTransfer.setAwbAmazon(fieldValues.get("awb")); //
		 * misBulkTransfer.setConsumerName(fieldValues.get("consumerName"));
		 * Map<String, Object> variables =
		 * runtimeService.getVariables(processId);
		 * 
		 * int isValid = 1; // validate productCode
		 * 
		 * int isValid = 1; List<Product> products = new ArrayList<Product>();
		 * products = masterdataService.createProductQuery().retailerId
		 * (String.valueOf(variables.get("rlRetailerId"))).
		 * code(fieldValues.get("productCode")).list(); if(products.size()==0) {
		 * isValid = 0; log.debug( "Product code not available"); ErrorObject
		 * errorObject = new ErrorObject(); errorObject.setRowNo(recordNo);
		 * errorObject.setMessage("Ignoring line " + recordNo);
		 * errorObject.setReason( "Product Code is invalid");
		 * retval.getErrorMessages().add(errorObject); }
		 * 
		 * misBulkTransfer.setProductCode(fieldValues.get("productCode"));
		 * misBulkTransfer.setStatus("open");
		 * 
		 * if (variables.containsKey("rlRetailerId")) {
		 * misBulkTransfer.setRetailerId(String.valueOf(variables.get(
		 * "rlRetailerId")));
		 * misBulkTransfer.setRetailer(String.valueOf(variables.get("retailer"))
		 * ); } if (variables.containsKey("rlDropLocationId")) {
		 * misBulkTransfer.setDropLocationId(String.valueOf(variables.get(
		 * "rlDropLocationId"))); }
		 * misBulkTransfer.setTicketNo(String.valueOf(variables.get("rlTicketNo"
		 * ))); if (variables.containsKey("rlDropLocationName")) {
		 * misBulkTransfer.setDropLocationName(String.valueOf(variables.get(
		 * "rlDropLocationName"))); }
		 * 
		 * if (fieldValues.containsKey("description") &&
		 * fieldValues.get("description") != null &&
		 * !fieldValues.get("description").equals("")) {
		 * misBulkTransfer.setDescription(fieldValues.get("description")); } if
		 * (fieldValues.containsKey("barcode") && fieldValues.get("barcode") !=
		 * null && !fieldValues.get("barcode").equals("")) { String barcode =
		 * fieldValues.get("barcode"); if (fieldValues.get("barcode").length() >
		 * 10) { barcode = barcode.substring(1, 11); }
		 * misBulkTransfer.setBarcode(barcode); barCodeList.add(barcode); } if
		 * (fieldValues.containsKey("value") && fieldValues.get("value") != null
		 * && !fieldValues.get("value").equals("")) {
		 * misBulkTransfer.setValue(fieldValues.get("value")); }
		 * 
		 * if (fieldValues.containsKey("quantity") &&
		 * fieldValues.get("quantity") != null &&
		 * !fieldValues.get("quantity").equals("")) {
		 * misBulkTransfer.setValue(fieldValues.get("quantity")); }
		 * 
		 * if (fieldValues.containsKey("productCategory") &&
		 * fieldValues.get("productCategory") != null &&
		 * !fieldValues.get("productCategory").equals("")) {
		 * misBulkTransfer.setProductCategory(fieldValues.get("productCategory")
		 * ); }
		 * 
		 * if (fieldValues.containsKey("consumerName") &&
		 * fieldValues.get("consumerName") != null &&
		 * !fieldValues.get("consumerName").equals("")) {
		 * misBulkTransfer.setConsumerName(fieldValues.get("consumerName")); }
		 * 
		 * if (fieldValues.containsKey("dateOfReceipt") &&
		 * fieldValues.get("dateOfReceipt") != null &&
		 * !fieldValues.get("dateOfReceipt").equals("")) {
		 * misBulkTransfer.setDateOfReceipt(fieldValues.get("dateOfReceipt")); }
		 * if (fieldValues.containsKey("address") && fieldValues.get("address")
		 * != null && !fieldValues.get("address").equals("")) {
		 * misBulkTransfer.setAddress(fieldValues.get("address")); }
		 * 
		 * if (fieldValues.containsKey("boxNumber") &&
		 * fieldValues.get("boxNumber") != null &&
		 * !fieldValues.get("boxNumber").equals("")) {
		 * misBulkTransfer.setBoxNumber(fieldValues.get("boxNumber"));
		 * barCodeList.add(fieldValues.get("boxNumber"));
		 * 
		 * String boxBarcodeType = fieldValues.get("boxNumber").substring(3, 5);
		 * if (boxBarcodeType.equals("40") || boxBarcodeType.equals("41") ||
		 * boxBarcodeType.equals("42")) {
		 * misBulkTransfer.setBoxNumber(fieldValues.get("boxNumber")); } else {
		 * isValid = 0; ErrorObject errorObject = new ErrorObject();
		 * errorObject.setRowNo(recordNo); errorObject.setMessage(
		 * "Ignoring line " + recordNo); errorObject.setReason(
		 * "Box Barcode is not of type Bulk Box");
		 * retval.getErrorMessages().add(errorObject); } }
		 * 
		 * runtimeService.saveMisBulkTransfer(misBulkTransfer);
		 */
	}

	@RequestMapping(value = "/ticket/cancel", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult cancelTicket(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		// String header = httpRequest.getHeader("Authorization");
		saveToConsoleLogsTable(retailerId, "Cancel By Retailer: " + ticketNo);

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();

		Retailer retailer = null;
		Calendar c = Calendar.getInstance();

		try {
			// Retailer Validation
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					throw new Exception("Invalid login");
				}
				if (!retailer.getCode().toString().equals(ticketNo.split("-")[0])) {
					restResult.setSuccess(false);
					throw new Exception("This ticket number is not for this retailer");
				}
				// API Token validation
				if (!retailer.getApiToken().equals(apiToken)) {
					restResult.setSuccess(false);
					restResult.setResponse("Not Authorized for this request");
					log.debug(retailer.getApiToken());
					// log.debug(apiToken);
					throw new Exception("Not Authorized for this request");
				}
				// API token expiry validation
				/*
				 * Timestamp currentDate = new Timestamp(c.getTime().getTime());
				 * if (currentDate.after(retailer.getTokenExpiryDate())) {
				 * restResult.setSuccess(false); restResult.setResponse(
				 * "Authentication expired. Please login again"); throw new
				 * Exception("Authentication expired. Please login again"); }
				 */
			}
			/**
			 * prem code start 29-06-2018 ((RLogisticsProcessEngineImpl)
			 * (ProcessEngines .getDefaultProcessEngine())).getHistoryService().
			 * deleteHistoricProcessInstance(processInstanceId); this is delete
			 * data from HI table
			 * 
			 */

			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();

			HistoryService historyService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getHistoryService();

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", ticketNo);
			String processId = null;
			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();
				Map<String, Object> variables = runtimeService.getVariables(processId);

				// itcallcord
				// pickUpDate
				// Customer Signature

				try {
					// variables.get("itp_PD_CustomerSignature");//one way
					// variables.get("itp_VV_CustomerSignature");//buy back

					if (checkForService(variables)) {

						runtimeService.setVariable(processId, "rlTicketStatus", "Ticket Cancel By Retailer By API.");
						runtimeService.setVariable(processId, "runningTicketStatusExternal",
								"CANCEL_BY_RETAILER_BY_API");
						runtimeService.setVariable(processId, "rlProcessStatus", "Ticket Cancel By Retailer By API");

						try {
							org.json.JSONObject jsonObject = new org.json.JSONObject();
							jsonObject.put("processId", processId);
							jsonObject.put("currentStatus", "CANCEL_BY_RETAILER_BY_API");
							jsonObject.put("closedReason", "CANCEL_BY_RETAILER_BY_API");
							jsonObject.put("closedDate", ReportPojo.getCurrentDate());
							jsonObject.put("closedTime", ReportPojo.getCurrentTime());
							ReportPojo.updateReport(jsonObject);
						} catch (Exception e) {
							log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

						}

						// historyService.deleteHistoricProcessInstance(processId);
						ArrayList<String> variableKeys = new ArrayList<String>();
						for (String key : variables.keySet()) {

							variableKeys.add(key);
						}
						/**
						 * here removing the rlTicketNo to delete Cancel ticket
						 * no will be present in HI_ tables
						 */
						variableKeys.remove("rlTicketNo");
						variableKeys.remove("rlProcessId");// In HI table you
															// can
															// get rlProcessId
						runtimeService.deleteProcessInstance(processId, "cancel");
						restResult.setSuccess(true);
						restResult.setMessage("Ticket Cancelled Successfully.");
					} else {
						restResult.setSuccess(false);
						restResult.setMessage("Ticket is already in process, cannot not be cancelled.");
					}
				} catch (Exception e) {

				}

			} else {
				/**
				 * getting value from HI table
				 */
				restResult.setSuccess(false);
				restResult.setMessage("Requested ticket not present in the system.");

				/*
				 * String hiProcessId = ""; List<ProcessResult>
				 * historyProcessResult =
				 * com.rlogistics.customqueries.impl.CustomQueriesService.
				 * ProductCustomQueriesMapper
				 * .getHIProcessFromValue("rlTicketNo", ticketNo); if
				 * (historyProcessResult.size() > 0) { hiProcessId =
				 * historyProcessResult.get(0).getProcessId();
				 * HistoricVariableInstance hjh=((RLogisticsProcessEngineImpl)
				 * (ProcessEngines
				 * .getDefaultProcessEngine())).getHistoryService()
				 * .createHistoricVariableInstanceQuery().processInstanceId(
				 * hiProcessId).variableName("").singleResult();
				 * 
				 * hjh.get
				 * 
				 * } else { restResult.setSuccess(false); restResult.setMessage(
				 * "Requested ticket not present in the system."); }
				 */

			}

		} catch (Exception e) {
			log.debug("error" + e);
			restResult.setSuccess(false);
			restResult.setMessage(e.getMessage().toString());
			e.printStackTrace();
		} finally {
			/*
			 * // generate new apiToken c.add(Calendar.HOUR, 1); retailer =
			 * masterdataService.createRetailerQuery().id(retailerId).
			 * singleResult(); if (retailer.getApiUsername() != null) { String
			 * rawToken = retailer.getApiUsername() + "___" +
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); } else { String rawToken =
			 * RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new
			 * SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()); apiToken =
			 * AuthUtil.cryptWithMD5(rawToken); retailer.setApiToken(apiToken);
			 * retailer.setTokenExpiryDate(new
			 * Timestamp(c.getTime().getTime())); }
			 */
			try {
				// masterdataService.saveRetailer(retailer);
				restResult.setApiToken(apiToken);
			} catch (Exception e) {

			}
		}
		/**
		 * prem code end
		 */
		return restResult;
	}

	public boolean checkForService(Map<String, Object> variables) {
		// variables.get("itp_PD_CustomerSignature");//one way custerm pick up
		// variables.get("itp_MD_ServiceCenterSignature");//one way service
		// center
		// variables.get("itp_VV_CustomerSignature");//buy back

		// itcallcord

		// boolean rtnvalue = true;
		// switch (variables.get("rlServiceCode").toString()) {
		// case "PICKNDROPONEWAY":
		// try {
		// // String v =
		// // variables.get("itp_PD_CustomerSignature").toString();
		//
		// rtnvalue = false;
		// } catch (Exception e) {
		// }
		//
		// break;
		// case "BUYBACK":
		// try {
		// // String v =
		// // variables.get("itp_VV_CustomerSignature").toString();
		//
		// rtnvalue = false;
		// } catch (Exception e) {
		// try {
		// // String v =
		// // variables.get("itp_OO_CustomerSignature").toString();
		// String v = variables.get("itcallcord").toString();
		//
		// rtnvalue = false;
		// } catch (Exception e1) {
		// }
		// }
		// break;

		boolean rtnvalue = true;
		if (variables.containsKey("rlAppointmentDate")) {
			rtnvalue = false;
		}
		return rtnvalue;
	}

	public void addAllProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {
		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (!fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}
				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					ProductCategory productCategory = masterdataService.createProductCategoryQuery()
							.name(fieldValues.getString("productCategory")).singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Other")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());
					product.setBrandId(newBrandId);
					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");
					log.info("New Product Added Successfully");
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

	public void addMobileProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {

		/**
		 * These value we get from retailer related to product "brand": "Apple",
		 * "productCategory": "mobile", "productName": "Iphone 7",
		 * "productCode": "iPhone 7", "model": "S",
		 */

		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}

				log.debug("22222222222222");

				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					/**
					 * =masterdataService.createProductCategoryQuery().name(
					 * "Mobile" THIS "Mobile" string should be same in
					 * RL_MD_PRODUCT_CATEGORY IN Production server ,dev or in
					 * test
					 */
					ProductCategory productCategory = masterdataService.createProductCategoryQuery().name("Mobile")
							.singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());// SUBCATEGORY ID
																// FROM
																// RL_MD_RETAILER_CATEGORIES
																// TABLE
					/**
					 * THIS PRODUCT CATEGORY SHOULD BE ADD IN
					 * RL_MD_RETAILER_CATEGORIES AND GET PRODUCT_id_ HERE
					 */
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());// Wrap
																		// Box
																		// 35x23x11
																		// from
																		// RL_MD_PACKAGING_TYPE
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Bubble Cover 9x6")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());// Bubble
																			// Cover
																			// 9x6
																			// from
																			// RL_MD_PACKAGING_TYPE
					product.setBrandId(newBrandId);// IT SHOULD BE BRAND ID FROM
													// RL_MD_BRAND
					/**
					 * THIS BRAND SHOULD BE ADD IN RL_MD_BRAND AND GET HERE THE
					 * BARND_ID_
					 */

					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");

					log.info("New Product Added Successfully");
					/**
					 * after add the product this product should be allow to
					 * this retailer this is the error message Status false
					 * iphone 7prem is not a valid Product Code of Demo
					 * Retailer.null
					 */
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

	public String addNewBrand(MasterdataService masterdataService, JSONObject fieldValues) {
		Brand brand = masterdataService.newBrand();
		String brandId = String.valueOf(UUID.randomUUID());

		brand.setId(brandId);
		brand.setName(fieldValues.getString("brand").toUpperCase());
		brand.setCode(randomAlphaNumeric(20));
		// brand.setDescription("description");

		masterdataService.saveBrand(brand);
		log.debug("*************************New Brand is Added");

		return brandId;
	}

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	private static final Object CLICKPOST = "CLICKPOST";

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	private void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
		}

	}

	@RequestMapping(value = "/ticket/get/images", method = RequestMethod.POST, produces = "application/json")
	public ImagesRestResponse getImagesByRetailer(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "retailerId") String retailerId) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		ImagesRestResponse restResponse = new ImagesRestResponse();
		List<ImagesListResponse> list = new ArrayList<>();
		try {

			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();

			List<TicketImages> images = masterdataService.createTicketImagesQuery().ticketNo(ticketNo).list();
			if (retailer.getCode().equals(ticketNo.split("-")[0])) {
				if (images.size() > 0) {
					for (TicketImages ticketImage : images) {
						ImagesListResponse jsonObject = new ImagesListResponse();
						jsonObject.setImageType(ticketImage.getImageType());
						jsonObject.setImageUrl(ticketImage.getImageUrl());
						jsonObject.setImageCreation(ticketImage.getCreateTimestamp() + "");
						// jsonObject.setLatitude(ticketImage.getLat());
						// jsonObject.setLongitude(ticketImage.getLongitude());
						// jsonObject.setCity(ticketImage.getLocation());
						list.add(jsonObject);

					}
					restResponse.setResponse(list);
					restResponse.setMessage("Images Link for Ticket Number : " + ticketNo);
					restResponse.setSuccess(true);
				} else {
					restResponse.setResponse(null);
					restResponse.setMessage("Images for Ticket Number: " + ticketNo + " is not present in System.");
					restResponse.setSuccess(false);
				}
			} else {
				restResponse.setResponse(null);
				restResponse.setMessage("Ticket Number: " + ticketNo + " is not for this Retailer");
				restResponse.setSuccess(false);
			}

		} catch (Exception e) {
			log.debug(e.toString());
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);

		}

		return restResponse;
	}

	@RequestMapping(value = "/ticket/create/pickanddrop/oneway", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketPND1W(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Map<String, String> fieldValues = null;
		if (PNDOneWayPrimaryValidation(queryJSON, restResult, retailerId)) {
			String jsonTicket = "";
			ExcelProcessResource epr = new ExcelProcessResource();
			ArrayList<RestExternalResult> flag = new ArrayList<RestExternalResult>();
			try {
				fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
				fieldValues.put("productCode", randomAlphaNumeric(10));
			} catch (Exception e) {
				restResult.setSuccess(false);
				restResult.setResponse("Incorrect Format");
			}
			try {
				jsonTicket = JSONValue.toJSONString(fieldValues);
			} catch (Exception e) {
				log.debug("Error in Conversion from Map to String :" + e);
			}
			try {
				// if (fieldValues.get("productCategory").equals("mobile")) {
				// try {
				// RetailerOther retailerOther =
				// masterdataService.createRetailerOtherQuery()
				// .retailerId(retailerId).singleResult();
				// if (retailerOther.getIsMobileProductAddAllow() == 1) {
				// addMobileProduct(jsonTicket, masterdataService, retailerId,
				// fieldValues);
				// } else {
				// log.info("This Add Mobile Product is not valid for this
				// Retailer");
				// }
				// } catch (Exception e) {
				// log.info("Nullpointer exception This Add Mobile Product is
				// not valid for this Retailer");
				//
				// }
				// } else {
				// addAllProduct(jsonTicket, masterdataService, retailerId,
				// fieldValues);
				// }
				// create ticket

				String newjsonTicket = JSONValue.toJSONString(fieldValues);
				if (secondryValidationPND1W(fieldValues, retailerId, apiToken, restResult)) {
					restResult = epr.ticketCreation(newjsonTicket, retailerId, apiToken);
				} else {
					log.debug("secondryValidation fails --------------");
				}

			} catch (Exception e) {
				log.debug("error" + e);
				e.printStackTrace();
				saveToConsoleLogsTable(retailerId,
						"Pick and Drop One Way " + " Request Json: " + queryJSON + " Response:"
								+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage()
								+ "ERROR:" + e);

			}
		} else {
			// PNDOneWayPrimaryValidation got false
		}
		saveToConsoleLogsTable(retailerId, "Pick and Drop One Way " + " RequestJson: " + queryJSON + " Response:"
				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;
	}

	@RequestMapping(value = "/ticket/create/v1/pickanddrop/oneway", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketPND1WV1(@RequestBody String queryJSON,
			@RequestHeader(value = "retailerId") String retailerId,
			@RequestHeader(value = "apiToken", required = true) String apiToken) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Map<String, String> fieldValues = null;
		if (PNDOneWayPrimaryValidation(queryJSON, restResult, retailerId)) {
			String jsonTicket = "";
			ExcelProcessResource epr = new ExcelProcessResource();
			ArrayList<RestExternalResult> flag = new ArrayList<RestExternalResult>();
			try {
				fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			} catch (Exception e) {
				restResult.setSuccess(false);
				restResult.setResponse("Incorrct Format");
			}
			try {
				jsonTicket = JSONValue.toJSONString(fieldValues);
			} catch (Exception e) {
				log.debug("Error in Conversoion from MAp to String :" + e);
			}
			try {
				if (fieldValues.get("productCategory").equals("mobile")) {
					try {
						RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
								.retailerId(retailerId).singleResult();
						if (retailerOther.getIsMobileProductAddAllow() == 1) {
							addMobileProduct(jsonTicket, masterdataService, retailerId, fieldValues);
						} else {
							log.info("This Add Mobile Product is not valid for this Retailer");
						}
					} catch (Exception e) {
						log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

					}
				} else {
					addAllProduct(jsonTicket, masterdataService, retailerId, fieldValues);
				}
				// create ticket

				String newjsonTicket = JSONValue.toJSONString(fieldValues);
				if (secondryValidationPND1W(fieldValues, retailerId, apiToken, restResult)) {
					restResult = epr.ticketCreation(newjsonTicket, retailerId, apiToken);
				} else {
					log.debug("secondryValidation fails --------------");
				}

			} catch (Exception e) {
				log.debug("error" + e);
				e.printStackTrace();
				saveToConsoleLogsTable(retailerId,
						"Pick and Drop One Way " + " Request Json: " + queryJSON + " Response:"
								+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage()
								+ "ERROR:" + e);

			}
		} else {
			// PNDOneWayPrimaryValidation got false
		}
		saveToConsoleLogsTable(retailerId, "Pick and Drop One Way " + " RequestJson: " + queryJSON + " Response:"
				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;
	}

	// NEW API
	@RequestMapping(value = "/ticket/create/pickanddrop/twoway", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketPND2W(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(required = true, value = "retailerId") String retailerId,
			@RequestParam(required = true, value = "apiToken") String apiToken) {
		saveToConsoleLogsTable(retailerId, queryJSON);
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();
		Map<String, String> fieldValues = null;
		if (PNDTwoWayPrimaryValidation(queryJSON, restResult, retailerId)) {
			String jsonTicket = "";
			ExcelProcessResource epr = new ExcelProcessResource();
			ArrayList<RestExternalResult> flag = new ArrayList<RestExternalResult>();
			try {
				fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
			} catch (Exception e) {
				restResult.setSuccess(false);
				restResult.setResponse("Incorrect Format");
			}
			try {
				jsonTicket = JSONValue.toJSONString(fieldValues);
			} catch (Exception e) {
				log.debug("Error in Conversoion from MAp to String :" + e);
			}
			try {
				if (fieldValues.get("productCategory").equals("mobile")) {
					try {
						RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
								.retailerId(retailerId).singleResult();
						if (retailerOther.getIsMobileProductAddAllow() == 1) {
							addMobileProduct(jsonTicket, masterdataService, retailerId, fieldValues);
						} else {
							log.info("This Add Mobile Product is not valid for this Retailer");
						}
					} catch (Exception e) {
						log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

					}
				} else {
					addAllProduct(jsonTicket, masterdataService, retailerId, fieldValues);
				}
				// create ticket

				String newjsonTicket = JSONValue.toJSONString(fieldValues);
				if (secondryValidationPND2W(fieldValues, retailerId, apiToken, restResult)) {
					restResult = epr.ticketCreation(newjsonTicket, retailerId, apiToken);
				} else {
					log.debug("secondryValidation fails --------------");
				}

			} catch (Exception e) {
				log.debug("error" + e);
				e.printStackTrace();
			}
		} else {
			// PNDTwoWayPrimaryValidation got false
		}
		saveToConsoleLogsTable(retailerId, "Pick and Drop Two Ways " + " Request Json: " + queryJSON + " Response:"
				+ restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;
	}

	private boolean secondryValidationPND2W(Map<String, String> fieldValues, String retailerId, String apiToken,
			RestExternalResult restResult) {
		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		// if (!fieldsValidationImp.validateProductBrand()) {
		// errorMeesage.append(fieldValues.get("brand") + " Brand is not valid,
		// ");
		// retrnValue = false;
		// }
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;
	}

	private boolean PNDTwoWayPrimaryValidation(String queryJSON, RestExternalResult restResult, String retailerId) {
		/**
		 * productName model productCode brand identificationNo
		 */
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;
		String productNameerrorMessage = "";
		String modelerrorMessage = "";
		String productCodeerrorMessage = "";
		String branderrorMessage = "";
		String identificationNomodelerrorMessage = "";
		String productCategoryerrorMessage = "";

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {

			value = checkForFieldsInLoopForTwoWay(fieldValues, value, finalErrorMeesage);

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;
	}

	private boolean checkForFieldsInLoopForTwoWay(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		// fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("physicalEvaluation");
		// fV.add("dropLocation");
		// fV.add("dropLocAddress1");
		// fV.add("dropLocAddress2");
		// fV.add("dropLocCity");
		// fV.add("dropLocState");
		// fV.add("dropLocPincode");
		// fV.add("dropLocContactPerson");
		// fV.add("dropLocContactNo");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		if (!fieldValues.getString("natureOfComplaint").equals("Pick And Drop (Two Ways)")) {
			value = false;
			finalErrorMeesage.append("natureOfComplaint is not correct, ");
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;
	}

	private boolean secondryValidationPND1W(Map<String, String> fieldValues, String retailerId, String apiToken,
			RestExternalResult restResult) {
		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("dropLocPincode"))) {
			errorMeesage.append(fieldValues.get("dropLocPincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		// if (!fieldsValidationImp.validateProductBrand()) {
		// errorMeesage.append(fieldValues.get("brand") + " Brand is not valid,
		// ");
		// retrnValue = false;
		// }
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;
	}

	@RequestMapping(value = "/ticket/get/history", method = RequestMethod.POST, produces = "application/json")
	public RestResponse test(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "retailerId") String retailerId) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		String processId = "";
		int reschcount = 0;

		RestResponse restResponse;

		try {
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();

			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getHIProcessFromValue("rlTicketNo", ticketNo);
			if (retailer.getCode().equals(ticketNo.split("-")[0])) {

				if (processResults.size() > 0) {
					processId = processResults.get(0).getProcessId();
					List<HistoricTaskInstance> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getHistoryService().createHistoricTaskInstanceQuery()
									.processInstanceId(processId).orderByTaskCreateTime().desc().list();
					restResponse = getTicketHistoryInternal(tasks, ticketNo);
				} else {
					restResponse = new RestResponse();
					restResponse.setResponse(null);
					restResponse.setMessage("Ticket is not present in the System.");
					restResponse.setSuccess(false);

				}
			} else {
				restResponse = new RestResponse();
				restResponse.setResponse(null);
				restResponse.setMessage("Ticket Number: " + ticketNo + " is not for this Retailer");
				restResponse.setSuccess(false);
			}
		} catch (Exception e) {
			log.debug(e.toString());
			restResponse = new RestResponse();
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);
		}

		return restResponse;

	}

	@RequestMapping(value = "/website/ticket/get/history", method = RequestMethod.POST, produces = "application/json")
	public RestResponse wwwGetTicketHistory(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		String processId = "";
		int reschcount = 0;

		RestResponse restResponse;

		try {
			List<TicketStatusHistory> ticketStatusHistory = masterdataService.createTicketStatusHistoryQuery()
					.ticketNo(ticketNo).orderByCreateTime().desc().list();

			// List<ProcessResult> processResults =
			// com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
			// .getHIProcessFromValue("rlTicketNo", ticketNo);

			if (ticketStatusHistory.size() > 0) {
				// processId = processResults.get(0).getProcessId();
				// List<HistoricTaskInstance> tasks =
				// ((RLogisticsProcessEngineImpl) (ProcessEngines
				// .getDefaultProcessEngine())).getHistoryService().createHistoricTaskInstanceQuery()
				// .processInstanceId(processId).orderByTaskCreateTime().desc().list();
				restResponse = getTicketHistoryInternalForWebsite(ticketStatusHistory, ticketNo);
			} else {
				restResponse = new RestResponse();
				restResponse.setResponse(null);
				restResponse.setMessage("Ticket is not present in the System.");
				restResponse.setSuccess(false);

			}

		} catch (Exception e) {
			log.debug(e.toString());
			restResponse = new RestResponse();
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);
		}

		return restResponse;

	}

	// CLICKPOST api to get status
	@RequestMapping(value = "/v1/ticket/get/history", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getTicketStatusForVenders(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "token") String token) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		String processId = "";
		int reschcount = 0;
		RetailerApiCalls.saveToConsoleLogsTable("STATUS REQUEST BY VENDERS ", token + " " + ticketNo);

		RestResponse restResponse = new RestResponse();

		try {
			List<TicketStatusHistory> ticketStatusHistory = masterdataService.createTicketStatusHistoryQuery()
					.ticketNo(ticketNo).orderByCreateTime().desc().list();

			// List<ProcessResult> processResults =
			// com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
			// .getHIProcessFromValue("rlTicketNo", ticketNo);

			if (token.equals(CLICKPOST)) {
				if (ticketStatusHistory.size() > 0) {
					// processId = processResults.get(0).getProcessId();
					// List<HistoricTaskInstance> tasks =
					// ((RLogisticsProcessEngineImpl) (ProcessEngines
					// .getDefaultProcessEngine())).getHistoryService().createHistoricTaskInstanceQuery()
					// .processInstanceId(processId).orderByTaskCreateTime().desc().list();
					restResponse = getTicketHistoryInternalForWebsite(ticketStatusHistory, ticketNo);
				} else {
					restResponse.setResponse(null);
					restResponse.setMessage("Ticket is not present in the System.");
					restResponse.setSuccess(false);

				}
			} else {
				restResponse.setResponse(null);
				restResponse.setMessage("Invalid token");
				restResponse.setSuccess(false);
			}

		} catch (Exception e) {
			log.debug(e.toString());
			restResponse = new RestResponse();
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);
		}

		return restResponse;

	}

	RestResponse getTicketHistoryInternal(List<HistoricTaskInstance> tasks, String ticketNo) {
		RestResponse restResponse = new RestResponse();
		ArrayList<Object> histories = new ArrayList<>();
		Set<HistoricTaskInstance> set = new HashSet<HistoricTaskInstance>();
		for (HistoricTaskInstance task : tasks) {
			// log.debug("------" + task.getName());
			if (task.getName().contains("Appointment Fixing")) {
				set.add(task);
			}
			StatusHistory history = new StatusHistory();
			history.setStatus(getMappedStatus(task.getName(), set.size()));
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			Date d = task.getCreateTime();
			String strtime = format.format(d);
			history.setTime(strtime);
			if (!history.getStatus().equals("")) {
				histories.add(history);
			}
		}
		restResponse.setResponse(histories);
		restResponse.setMessage("Ticket history for ticket number:" + ticketNo);
		restResponse.setSuccess(true);
		return restResponse;
	}

	RestResponse getTicketHistoryInternalForWebsite(List<TicketStatusHistory> tasks, String ticketNo) {
		RestResponse restResponse = new RestResponse();
		ArrayList<Object> histories = new ArrayList<>();
		List<TicketStatusHistory> tasks11 = new ArrayList<TicketStatusHistory>();
		Set<String> set = new HashSet<String>();
		for (TicketStatusHistory task1 : tasks) {
			if (set.add(task1.getStatus())) {
				tasks11.add(task1);
			}
		}

		for (TicketStatusHistory task : tasks11) {

			StatusHistory history = new StatusHistory();
			history.setStatus(task.getStatus());

			history.setTime(task.getCreateTime());
			if (!history.getStatus().equals("")) {
				histories.add(history);
			}
		}
		restResponse.setResponse(histories);
		restResponse.setMessage("Ticket history for ticket number:" + ticketNo);
		restResponse.setSuccess(true);
		return restResponse;
	}

	private String getMappedStatus(String name, int appointcount) {
		String rtnStr = "";
		if (name.contains("Initial Call")) {
			rtnStr = "TICKET_CREATED";
		} else if (name.contains("Appointment Fixing")) {

			rtnStr = "APPOINTMENT_FIXED";
		} else if (name.contains("Field Visit Assigned. Get Packing Materials")) {
			rtnStr = "ASSIGNE_TO_FIELD_ENGINEER_FOR_PICKUP";
		} else if (name.contains("Customer Refused Service or Pick Up. Close Ticket")) {
			rtnStr = "CUSTOMER_REFUSED_SERVICE_OR_PICKUP_TICKET_CLOSED";
		} else if (name.contains("Initiate Buy Back Product Return To Retailer Thru FE")) {
			rtnStr = "PRODUCT_PICKEDUP_FROM_CUSTOMER";
		} else if (name.contains("Initiate Buy Back Product Return To Retailer To Drop Location")) {
			rtnStr = "PRODUCT_PICKEDUP_FROM_CUSTOMER_INITIATE_FOR_RETURN_TO_DROP_LOCATION";
		} else if (name.contains("Confirm Buy Back Product Return To Retailer For Ticket Closure")
				|| name.contains("Deliver To Drop Location")) {
			rtnStr = "PRODUCT_DROPPED_TO_DROP_LOCATION";
		} else if (name.contains("Drop Confirmation")) {
			rtnStr = "PRODUCT_DROPPED_CONFIRMATION_TICKET_CLOSED";
		} else if (name.contains("Assign To FE For Drop")) {
			rtnStr = "ASSIGNE_TO_FIELD_ENGINEER_FOR_DROP";
		}
		// else
		// rtnStr = name;

		return rtnStr;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/ticket/get/images-before-packing", method = RequestMethod.POST, produces = "application/json")
	public Map getImagesBeforePacking(@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "retailerId") String retailerId) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		// RestResponse restResponse = new RestResponse();
		Map<String, Object> restResponse = new HashMap<String, Object>();

		try {

			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			List<TicketImages> images = masterdataService.createTicketImagesQuery().ticketNo(ticketNo).list();
			int imageBeforePackingCount = Integer.parseInt(
					(String) runtimeService.getVariable(images.get(0).getProcInstId(), "rlImageCountBeforePacking"));
			// restResponse = getImagesBeforePackingInternal(ticketNo, retailer,
			// images, imageBeforePackingCount);
			List<PhotoBeforePaking> list = new ArrayList<>();

			if (retailer.getCode().equals(ticketNo.split("-")[0])) {
				if (images.size() > 0) {
					for (TicketImages ticketImage : images) {
						if (ticketImage.getImageType().contains(PHOTOGRAPHBEFORE)) {
							PhotoBeforePaking photoBeforePaking = new PhotoBeforePaking();
							photoBeforePaking.setImageType(ticketImage.getImageType());
							photoBeforePaking.setImageUrl(ticketImage.getImageUrl());
							photoBeforePaking.setImageCreation(ticketImage.getCreateTimestamp() + "");
							// photoBeforePaking.setImageCountBeforepacking(imageBeforePackingCount);
							list.add(photoBeforePaking);
						}
					}
					restResponse.put("imageCountBeforepacking", imageBeforePackingCount);
					restResponse.put("message", "Images Link for Ticket Number : " + ticketNo);
					restResponse.put("success", true);
					restResponse.put("response", list);

					// restResponse.setResponse(list);
					// restResponse.setMessage("Images Link for Ticket Number :
					// " + ticketNo);
					// restResponse.setSuccess(true);
				} else {

					restResponse.put("response", null);
					restResponse.put("message", "Images for Ticket Number: " + ticketNo + " is not present in System.");
					restResponse.put("success", false);

					// restResponse.setResponse(null);
					// restResponse.setMessage("Images for Ticket Number: " +
					// ticketNo + " is not present in System.");
					// restResponse.setSuccess(false);
				}
			} else {
				restResponse.put("response", null);
				restResponse.put("message", "Ticket Number: " + ticketNo + " is not for this Retailer");
				restResponse.put("success", false);

				// restResponse.setResponse(null);
				// restResponse.setMessage("Ticket Number: " + ticketNo + " is
				// not for this Retailer");
				// restResponse.setSuccess(false);
			}

			return restResponse;
		} catch (Exception e) {
			log.debug(e.toString());
			restResponse.put("response", null);
			restResponse.put("message", "Invalid Retailer");
			restResponse.put("success", false);
			// restResponse = new RestResponse();
			// restResponse.setResponse(null);
			// restResponse.setMessage("Invalid Retailer");
			// restResponse.setSuccess(false);

		}

		return restResponse;
	}

	// package protected for testing
	RestResponse getImagesBeforePackingInternal(String ticketNo, Retailer retailer, List<TicketImages> images,
			int imageBeforePackingCount) {
		RestResponse restResponse = new RestResponse();
		List<PhotoBeforePaking> list = new ArrayList<>();

		if (retailer.getCode().equals(ticketNo.split("-")[0])) {
			if (images.size() > 0) {
				for (TicketImages ticketImage : images) {
					if (PHOTOGRAPHBEFORE.equals(ticketImage.getImageType())) {
						PhotoBeforePaking photoBeforePaking = new PhotoBeforePaking();
						photoBeforePaking.setImageType(ticketImage.getImageType());
						photoBeforePaking.setImageUrl(ticketImage.getImageUrl());
						photoBeforePaking.setImageCreation(ticketImage.getCreateTimestamp() + "");
						// photoBeforePaking.setImageCountBeforepacking(imageBeforePackingCount);
						list.add(photoBeforePaking);
					}
				}
				restResponse.setResponse(list);
				restResponse.setMessage("Images Link for Ticket Number : " + ticketNo);
				restResponse.setSuccess(true);
			} else {
				restResponse.setResponse(null);
				restResponse.setMessage("Images for Ticket Number: " + ticketNo + " is not present in System.");
				restResponse.setSuccess(false);
			}
		} else {
			restResponse.setResponse(null);
			restResponse.setMessage("Ticket Number: " + ticketNo + " is not for this Retailer");
			restResponse.setSuccess(false);
		}

		return restResponse;
	}

	@RequestMapping(value = "retailer/get/physical-evaluation-result", produces = "application/json")
	public RestResponse getEvaluationResult(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "retailerId") String retailerId) {
		// for more validata api refer DOAFormResource
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestResponse restResponse = new RestResponse();

		try {

			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();

			String processId = "";
			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getHIProcessFromValue("rlTicketNo", ticketNo);
			if (retailer.getCode().equals(ticketNo.split("-")[0])) {
				if (processResults.size() > 0) {
					processId = processResults.get(0).getProcessId();
					CloseableHttpClient httpclient = HttpClients.createDefault();
					String URL = getValidataBaseUrl();
					String url = URL + "/biblog-validata/resutlts/all?processId=" + processId;
					// String url =
					// "http://www.validata.in/admin/user_answers_to_questions?ticket_id="
					// + processId;
					HttpGet httpget = new HttpGet(url);

					httpget.addHeader("Content-Type", "application/json");

					CloseableHttpResponse response = httpclient.execute(httpget);
					String rawResponse = EntityUtils.toString(response.getEntity());
					log.debug("Response from server :" + rawResponse);
					System.out.println("response from server " + rawResponse);

					if (response.getStatusLine().getStatusCode() == 200) {
						restResponse = getPhysicalEvaluationResultArrary(rawResponse, ticketNo);

					} else {
						restResponse.setMessage("Evaluation is not available");
						restResponse.setSuccess(false);
					}

					response.close();
				} else {
					restResponse.setResponse(null);
					restResponse.setMessage("Evaluation is not available for Ticket Number: " + ticketNo
							+ " is not present in System.");
					restResponse.setSuccess(false);
				}
			} else {
				restResponse.setResponse(null);
				restResponse.setMessage("Ticket Number: " + ticketNo + " is not for this Retailer");
				restResponse.setSuccess(false);
			}

		} catch (Exception e) {
			log.debug(e.toString());
			restResponse.setResponse(null);
			restResponse.setMessage("Invalid Retailer");
			restResponse.setSuccess(false);

		}

		return restResponse;
	}

	private String getValidataBaseUrl() {
		// http://54.69.180.84:5000/biblog-validata/resutlts/all?processId=27125017
		String rtnValue = "";
		try {
			InetAddress IP = InetAddress.getLocalHost();
			String ip = IP.getHostAddress().toString();
			if (ip.equals(PaymentAuth.BIZLOG_PRODUCTION_IP)) {
				rtnValue = "http://rl.bizlog.in:5000";
			} else if (ip.equals(PaymentAuth.BIZLOG_TEST_IP)) {
				rtnValue = "http://test.bizlog.in:5000";
			} else {
				rtnValue = "http://dev.bizlog.in:5000";
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return rtnValue;
	}

	public RestResponse getPhysicalEvaluationResultArrary(String rawResponse, String ticketNo) throws IOException {
		RestResponse restResponse = new RestResponse();
		// TODO Auto-generated method stub
		Map<String, Object> resp = new ObjectMapper().readValue(URLDecoder.decode(rawResponse), Map.class);
		System.out.println("REsponse from server : " + resp);
		ArrayList<Map<String, Object>> egnrResponse = (ArrayList<Map<String, Object>>) resp.get("engineer_response");

		for (Map<String, Object> egnrObj : egnrResponse) {
			egnrObj.remove("question_type");
		}

		restResponse.setResponse(egnrResponse);
		restResponse.setMessage("Physical Evaluation result for ticket number:" + ticketNo);
		restResponse.setSuccess(true);
		return restResponse;
	}

	// Abinash Code
	// Update Ticket Status
	@RequestMapping(method = RequestMethod.PUT, value = "update/ticket/status", produces = "application/json")
	public RestExternalResult updateTicketStatus(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			@RequestParam(required = true, value = "ticketNo") String ticketNo,
			@RequestParam(required = true, value = "retailerId") String retailerId,
			@RequestParam(required = true, value = "apiToken") String apiToken) {

		saveToConsoleLogsTable(retailerId, "Update Ticket Status: " + ticketNo);

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		RestExternalResult restResult = new RestExternalResult();

		Retailer retailer = null;
		try {

			// Retailer Validation
			if (retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if (retailer == null) {
					restResult.setSuccess(false);
					restResult.setMessage("Invalid Retailer");
					throw new Exception("Invalid Retailer");
				}
			}
			// API Token Validation
			if (!retailer.getApiToken().equals(apiToken)) {
				restResult.setSuccess(false);
				restResult.setMessage("Unauthorised Request");
				throw new Exception("Unauthorised Request");
			}
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getRuntimeService();
			List<ProcessResult> processResults = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
					.getProcessFromValue("rlTicketNo", ticketNo);
			String processId = null;
			String priority = null;

			if (processResults.size() > 0) {
				processId = processResults.get(0).getProcessId();

				if (processId != null) {

					Map<String, Object> map = runtimeService.getVariables(processId);
					log.debug("Ticket No---------- " + ticketNo + " --------------------");
					runtimeService.setVariable(processId, "ticketPriority", "High");
					runtimeService.setVariable(processId, "stickyAssignee", null);
					java.util.Date date = new java.util.Date();
					Object b = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper
							.updateTicketAssigneeAndCreatedDate(null, date, processId);

					List<String> ticketList = new ArrayList<String>();
					ticketList.add(ticketNo);
					restResult.setTicketNo(ticketList);
					restResult.setSuccess(true);
					restResult.setMessage("Ticket status is updated");
				} else {
					restResult.setSuccess(false);
					throw new Exception("Invalid Ticket Number");
				}

			} else {
				restResult.setTicketNo(Arrays.asList(ticketNo));
				restResult.setSuccess(false);
				restResult.setMessage("Invalid Ticket Number");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return restResult;
	}
	// TVS
	// Abinash Code Ends

}
