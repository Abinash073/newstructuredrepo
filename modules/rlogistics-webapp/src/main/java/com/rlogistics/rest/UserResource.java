package com.rlogistics.rest;

import java.net.URLDecoder;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.email.EmailUtil;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.master.identity.ProcessUser;
import com.rlogistics.master.identity.ProcessUser.ProcessUserQuery;
import com.rlogistics.master.identity.ResetToken;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerPackingCost;
import com.rlogistics.master.identity.RetailerProcessField;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;
import com.rlogistics.master.identity.SmsEmailTemplate;
import com.rlogistics.master.identity.TicketColumns;
import com.rlogistics.ui.login.ProcessUserUIUserAdapter;
import com.rlogistics.ui.login.UIUser;
import com.rlogistics.master.identity.DeviceInfo;
import com.rlogistics.master.identity.DeviceInfo.DeviceInfoQuery;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingType;

@RestController
public class UserResource {

	private static Logger log = LoggerFactory.getLogger(UserResource.class);
	private static String APP_LINK = "http://ec2-54-187-161-0.us-west-2.compute.amazonaws.com:9090/rlogistics-execution/#/reset-password/";
   
	@RequestMapping(value = "/processuser/save", method = RequestMethod.POST)
	public RestResponse saveProcessUser(@RequestParam("processUser") String queryJSON, HttpServletResponse response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RegisterResult resultData = null;
		RestResponse restResponse = new RestResponse();
		Map<String, String> userObj = null;
		try {
			userObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			ProcessUser processUser = masterdataService.newProcessUser();
			String password = userObj.get("password");
			String encryptPass = AuthUtil.cryptWithMD5(password);
			
			if(userObj.containsKey("id")) {
				processUser = masterdataService.createProcessUserQuery().id(userObj.get("id")).singleResult();
				encryptPass = processUser.getPassword();
			} else {
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
				processUser.setId(id);
			}
			
			processUser.setFirstName(userObj.get("firstName"));
			processUser.setEmail(userObj.get("email"));
			if(userObj.containsKey("lastName")) {
				processUser.setLastName(userObj.get("lastName"));
			}
			if(userObj.containsKey("phone")) {
				processUser.setPhone(userObj.get("phone"));
			}
			if(userObj.containsKey("roleCode")) {
				processUser.setRoleCode(userObj.get("roleCode"));
			}
			if(userObj.containsKey("locationId")) {
				processUser.setLocationId(userObj.get("locationId"));
			}
			if(userObj.containsKey("retailerId")) {
				processUser.setRetailerId(userObj.get("retailerId"));
			}
			
			if(userObj.containsKey("status")) {
				processUser.setStatus(Integer.valueOf(String.valueOf(userObj.get("status"))));
			} else {
				processUser.setStatus(1);
			}
			processUser.setPassword(encryptPass);
			
			resultData = new RegisterResult();
			resultData.setFirstName(processUser.getFirstName());
			resultData.setLastName(processUser.getLastName());
			resultData.setEmail(processUser.getEmail());
			resultData.setPhone(processUser.getPhone());
			resultData.setRoleCode(processUser.getRoleCode());
			resultData.setLocationId(processUser.getLocationId());
			resultData.setStatus(processUser.getStatus());
			resultData.setRetailerId(processUser.getRetailerId());
			
			masterdataService.saveProcessUser(processUser);
			
			resultData.setId(processUser.getId());
			
			restResponse.setResponse(resultData);
			if(!userObj.containsKey("id")) {
				restResponse.setMessage("Registration successfull");
			} else {
				restResponse.setMessage("User updated");
			}
			
		} 
		catch(Exception ex){
			log.error("Exception during saving", ex);
			if(ex.getMessage().contains("MySQLIntegrityConstraintViolationException")) {
				restResponse.setMessage("Email id " + userObj.get("email") + " already exists");
			} else {
				restResponse.setMessage(ex.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/retailer/add", method = RequestMethod.POST)
	public RestResponse saveRetailer(@RequestParam("retailer") String queryJSON, HttpServletResponse response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		Map<String,String> resultData = new HashMap<String,String>();
		RestResponse restResponse = new RestResponse();
		Map<String, String> retailerObj = null;
		try {
			retailerObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			Retailer retailer = masterdataService.newRetailer();
			
			UUID id = UUID.randomUUID();
			retailer.setId(String.valueOf(id));
			if(retailerObj.containsKey("code")) {
				retailer.setCode(retailerObj.get("code"));
			}
			if(retailerObj.containsKey("name")) {
				retailer.setName(retailerObj.get("name"));
			}
			if(retailerObj.containsKey("email")) {
				retailer.setEmail(retailerObj.get("email"));
			}
			if(retailerObj.containsKey("address1")) {
				retailer.setAddress1(retailerObj.get("address1"));
			}
			if(retailerObj.containsKey("address2")) {
				retailer.setAddress2(retailerObj.get("address2"));
			}
			if(retailerObj.containsKey("city")) {
				retailer.setCity(retailerObj.get("city"));
			}
			if(retailerObj.containsKey("telephoneNumber")) {
				retailer.setTelephoneNumber(retailerObj.get("telephoneNumber"));
			}
			if(retailerObj.containsKey("altTelephoneNumber")) {
				retailer.setAltTelephoneNumber(retailerObj.get("altTelephoneNumber"));
			}
			if(retailerObj.containsKey("pincode")) {
				retailer.setPincode(retailerObj.get("pincode"));
			}
			
			try {
				
				
				masterdataService.saveRetailer(retailer);
				resultData.put("id", retailer.getId());
				
				//copy the master templates to retailer specific tables
				List<SmsEmailTemplate> masterTemplates = masterdataService.createSmsEmailTemplateQuery().list();
				
				int count = 0;
				for(SmsEmailTemplate masterTemplate:masterTemplates) {
					RetailerSmsEmailTemplate retailerSmsEmailTemplate = masterdataService.newRetailerSmsEmailTemplate();
					retailerSmsEmailTemplate.setId(String.valueOf(UUID.randomUUID()));
					retailerSmsEmailTemplate.setRetailerId(retailer.getId());
					retailerSmsEmailTemplate.setSmsEmailTriggerCode(masterTemplate.getSmsEmailTriggerCode());
					retailerSmsEmailTemplate.setEmailTemplate(masterTemplate.getEmailTemplate());
					if(masterTemplate.getSmsTemplate() != null) {
						retailerSmsEmailTemplate.setSmsTemplate(masterTemplate.getSmsTemplate());
					} 
					
					if(masterTemplate.getParams() != null) {
						retailerSmsEmailTemplate.setParams(masterTemplate.getParams());
					}
					if(masterTemplate.getSubject() != null) {
						retailerSmsEmailTemplate.setSubject(masterTemplate.getSubject());
					}
					if(masterTemplate.getEmailTemplate() != null) {
						retailerSmsEmailTemplate.setEmailTemplate(masterTemplate.getEmailTemplate());
					}
					try {
						masterdataService.saveRetailerSmsEmailTemplate(retailerSmsEmailTemplate);
					} catch (Exception e) {
						
					}
					count++;	
				}
				
				resultData.put("noOfTemplatesCopied", String.valueOf(count));
				
				//add retailer information to retailer process field
				List<TicketColumns> ticketColumns = masterdataService.createTicketColumnsQuery().list();
				for(TicketColumns column:ticketColumns) {
					RetailerProcessField retailerProcessField = masterdataService.newRetailerProcessField();
					
					retailerProcessField.setRetailerField(column.getField());
					retailerProcessField.setCanonicalField(column.getField());
					retailerProcessField.setRetailerId(retailer.getId());
					
					masterdataService.saveRetailerProcessField(retailerProcessField);
				} 
				
				//add initial packing and material cost for a retailer to 0
				List<PackagingType> packagingTypes = masterdataService.createPackagingTypeQuery().list();
				for(PackagingType packagingType : packagingTypes) {
					RetailerPackingCost retailerPackingCost = masterdataService.newRetailerPackingCost();
					retailerPackingCost.setId(String.valueOf(UUID.randomUUID()));
					retailerPackingCost.setRetailerId(retailer.getId());
					retailerPackingCost.setMaterialCost(0);
					retailerPackingCost.setPackingCost(0);
					retailerPackingCost.setMaterialCostInterCity(0);
					retailerPackingCost.setPackingCostInterCity(0);
					retailerPackingCost.setPackingMaterialId(packagingType.getId());
					try {
						masterdataService.saveRetailerPackingCost(retailerPackingCost);
					} catch(Exception e) {
						
					}
				}
				
			} catch (Exception e) {
				throw e;
			}
				
			restResponse.setResponse(resultData);
			restResponse.setMessage("Retailer added. Master sms/email templates copied to retailer");	
		} 
		catch(Exception ex){
			log.error("Exception during saving", ex);
			if(ex.getMessage().contains("Duplicate")) {
				restResponse.setMessage("Retailer already exists");
			} else {
				restResponse.setMessage(ex.getMessage());
			}
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/processuser/generate-reset-token/{email}", method = RequestMethod.POST)
	public RestResponse generateResetToken(@PathVariable("email") String email, HttpServletResponse response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		ResetToken resetToken = null;
		Map<String, String> result = new HashMap<String, String>();
		try {
			lookup(email);
			
			resetToken = masterdataService.createResetTokenQuery().email(email).singleResult();
			if(resetToken==null) {
				resetToken = masterdataService.newResetToken();
				resetToken.setEmail(email);
			}
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			String token = AuthUtil.cryptWithMD5(rawToken);
			resetToken.setToken(token);
			
			c.add(Calendar.DATE, 1);
			Timestamp timestamp = new Timestamp(c.getTime().getTime());
			resetToken.setExpiryDate(timestamp);
			
			masterdataService.saveResetToken(resetToken);
			java.sql.Timestamp date = resetToken.getExpiryDate();
			result.put("expiryDate", String.valueOf(timestamp.getTime()));
			result.put("success", "true");
			restResponse.setResponse(result);
			restResponse.setMessage("Password reset link has been sent to your registered email. Follow the link to reset your password. This link will be active for next 24 hours.");
			
			//send mail
			String subject = "Reset password activation";
			String content = "Click on the below link to reset your password <br>" + APP_LINK + token + "/"+ email;
			EmailUtil.sendMail(email, subject, content);
		} catch(Exception ex){
			log.error("Exception during reset password", ex);
			result.put("success", "false");
			restResponse.setResponse(result);
			restResponse.setMessage(ex.getMessage());
			response.setStatus(500);
		}
		return restResponse;
	}
	
	public static String lookup(String email) throws Exception{
		ProcessUser processUser = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService().createProcessUserQuery().email(email).singleResult();
		
		if(processUser != null){
			if(processUser.getStatus()==1) {
				return processUser.getEmail();
			}
			else {
				throw new Exception("User deactivated");
			}
		} else {
			throw new Exception("User does not exist");
		}
	}
	
	@RequestMapping(value = "/processuser/reset-password", method = RequestMethod.POST)
	public RestResponse resetPassword(@RequestParam("query") String queryJSON, HttpServletResponse response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		ResetToken resetToken = null;
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map<String, String> queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			resetToken = masterdataService.createResetTokenQuery().token(queryObj.get("token")).singleResult();
			if(resetToken==null) {
				throw new Exception("Invalid token");
			}
			Timestamp expiryDate = resetToken.getExpiryDate();
			Calendar c = Calendar.getInstance();
			Timestamp currentDate = new Timestamp(c.getTime().getTime());
			
			if(currentDate.before(expiryDate)) {
				ProcessUser processUser = masterdataService.createProcessUserQuery().email(resetToken.getEmail()).singleResult();
				String encryptPassword = AuthUtil.cryptWithMD5(queryObj.get("password"));
				if(processUser.getPassword().equals(AuthUtil.cryptWithMD5(queryObj.get("password")))) {
					throw new Exception("New password cannot be same as old password");
				}
				processUser.setPassword(encryptPassword);
				
				masterdataService.saveProcessUser(processUser);
				result.put("success", "true");
				restResponse.setResponse(result);
				restResponse.setMessage("Password reset successfull");
				
				//expire the token
				resetToken.setExpiryDate(new Timestamp(c.getTime().getTime()));
				masterdataService.saveResetToken(resetToken);
			} else {
				throw new Exception("Token is already used");
			}
		} catch(Exception ex){
			log.error("Exception during reset password", ex);
			result.put("success", "false");
			restResponse.setResponse(result);
			restResponse.setMessage(ex.getMessage());
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/processuser/change-password", method = RequestMethod.POST)
	public RestExternalResult changePassword(@RequestParam("query") String queryJSON, @RequestParam(value = "retailerId", required = false) String retailerId, @RequestParam(value = "apiToken", required = false) String apiToken,  HttpServletResponse response)throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestExternalResult restResponse = new RestExternalResult();
		Map<String, String> result = new HashMap<String, String>();
		Retailer retailer = null;
		Calendar c = Calendar.getInstance();
		
		try {
			if(retailerId != null) {
				retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
				if(retailer == null) {
					restResponse.setSuccess(false);
					throw new Exception("Invalid login");	
				}
				if(!retailer.getApiToken().equals(apiToken)) {
					restResponse.setSuccess(false);
					throw new Exception("Not Authorized for this request");
				}
				
				Timestamp currentDate = new Timestamp(c.getTime().getTime());
				if(currentDate.after(retailer.getTokenExpiryDate())) {
					restResponse.setSuccess(false);
					throw new Exception("Authentication expired. Please login again");
				}
			}
			
			Map<String, String> queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			
				String oldEncryptPassword = AuthUtil.cryptWithMD5(queryObj.get("oldPassword"));
				String newEncryptPassword = AuthUtil.cryptWithMD5(queryObj.get("newPassword"));
				ProcessUser processUser = masterdataService.createProcessUserQuery().email(queryObj.get("email")).password(oldEncryptPassword).status(1).singleResult();
				if(processUser != null) {
					if(processUser.getPassword().equals(newEncryptPassword)) {
						throw new Exception("New password cannot be same as old password");
					}
					processUser.setPassword(newEncryptPassword);
					
					masterdataService.saveProcessUser(processUser);
					result.put("success", "true");
					restResponse.setResponse(result);
					
					restResponse.setMessage("Password changed successfully");
				} else {
					response.setStatus(401);
					throw new Exception("Wrong current password");
				}
				
				if(retailerId != null) {
					//generate new apiToken
					c.add(Calendar.HOUR, 1);
					if(retailer.getApiUsername() != null) {
						String rawToken = retailer.getApiUsername() + "___" + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
						apiToken = AuthUtil.cryptWithMD5(rawToken);
						retailer.setApiToken(apiToken);
						retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
					} else {
						String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
						apiToken = AuthUtil.cryptWithMD5(rawToken);
						retailer.setApiToken(apiToken);
						retailer.setTokenExpiryDate(new Timestamp(c.getTime().getTime()));
					}
					try {
						masterdataService.saveRetailer(retailer);
						restResponse.setApiToken(apiToken); 
					} catch (Exception e) {
						
					}
				}
						
		} catch(Exception ex){
			log.error("Exception during change password", ex);
			result.put("success", "false");
			restResponse.setResponse(result);
			restResponse.setMessage(ex.getMessage());
			if(!ex.getMessage().contains("Invalid/Inactive")) {
				response.setStatus(500);
			}
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/remove/device-info", method = RequestMethod.POST)
	public RestResponse removeDeviceInfo(@RequestParam("query") String queryJSON, HttpServletResponse response) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map<String, String> queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			
				String email = queryObj.get("email");
				String deviceId = queryObj.get("deviceId");
				
				ProcessUserQuery processUserQuery = masterdataService.createProcessUserQuery();
				ProcessUser processUser = processUserQuery.email(email).singleResult();
				String userId = processUser.getId();
				
				DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
				DeviceInfo deviceInfo = deviceInfoQuery.deviceId(deviceId).userId(userId).singleResult();
				if(deviceInfo != null) {
					masterdataService.deleteDeviceInfo(deviceInfo.getId());
					
					result.put("success", "true");
					restResponse.setMessage("Successfully removed device information");
				}
				restResponse.setResponse(result);
										
		} catch(Exception ex){
			log.error("Exception during change password", ex);
			result.put("success", "false");
			restResponse.setResponse(result);
			restResponse.setMessage(ex.getMessage());
			response.setStatus(500);
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/send/push-notification", method = RequestMethod.POST)
	public RestResponse sendNotificationMessageService(@RequestParam("query") String queryJSON, HttpServletResponse response) {
		Map<String, String> queryObj = null; 
		String userId = "";
		String message = "";
		try {
			queryObj = new ObjectMapper().readValue(URLDecoder.decode(queryJSON),Map.class);
			userId = queryObj.get("userId");
			message = queryObj.get("message");
			return sendNotificationMessage(userId, message);
		} catch(Exception e) {
			RestResponse restResponse = new RestResponse();
			response.setStatus(500);
			return restResponse;
		}
		
	}
	
	public static RestResponse sendNotificationMessage(String userId,String message) throws Exception {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		ArrayList<String> devices = new ArrayList<String>();
		boolean val = false;
		RestResponse restResponse = new RestResponse();
		Map<String, Object> resultObj = new HashMap<String, Object>();
		try {
			DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
			List<DeviceInfo> deviceInfos = deviceInfoQuery.userId(userId).list();
			for(DeviceInfo deviceInfo:deviceInfos) {
				devices.add(deviceInfo.getDeviceId());
			}
			
			if(devices.size()>0) {
				val = AndroidPush.notifyMultipleDevices(devices, message);
				if(val) {
					restResponse.setMessage("Notification sent successfully");
				}
				resultObj.put("success", val);
				restResponse.setResponse(resultObj);
			} else {
				restResponse.setMessage("No devices found to notify");
			}
			
		} catch (Exception e) {
			resultObj.put("success", val);
			restResponse.setMessage(e.getMessage());
			restResponse.setResponse(resultObj);
		}
		return restResponse;
	}
}
