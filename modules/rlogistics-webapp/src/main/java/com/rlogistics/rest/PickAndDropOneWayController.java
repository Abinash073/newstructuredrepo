package com.rlogistics.rest;

import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerOther;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;

import pojo.ReportPojo;

import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingType;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.ProductSubCategory;

@RestController
public class PickAndDropOneWayController extends RLogisticsResource {
	private static Logger log = LoggerFactory.getLogger(PickAndDropOneWayController.class);
	private static String processName = "it_products";

//	@RequestMapping(value = "/ticket/create/pickanddrop/oneway", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketRepairStandBy(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		RestExternalResult restResult = new RestExternalResult();

		try {
			if (PNDOneWayPrimaryValidation(restResult, retailerId, apiToken, queryJSON)) {
				Map<String, String> fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();
				if (fieldValues.get("productCategory").equals("mobile")) {
					try {
						RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
								.retailerId(retailerId).singleResult();
						if (retailerOther.getIsMobileProductAddAllow() == 1) {
							addMobileProduct(queryJSON, masterdataService, retailerId, fieldValues);
						} else {
							log.info("This Add Mobile Product is not valid for this Retailer");
						}
					} catch (Exception e) {
						log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

					}
				} else {
					addAllProduct(queryJSON, masterdataService, retailerId, fieldValues);
				}
				if (secondryValidationPND1W(restResult, retailerId, apiToken, fieldValues)) {

					Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
					String retailer = retailerObj.getName();
					fieldValues.put("retailer", retailer);

					MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getMetadataService();
					RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRepositoryService();

					FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
							.getFormService();

					String dataset = "BizLog";
					ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset)
							.dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();

					ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
							.deploymentId(deployment.getDeploymentId()).singleResult();

					FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(), false);
					GroupDescription groupDescription = null;
					while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
						if (log.isDebugEnabled()) {
							log.debug("Current groupDescription:" + groupDescription);
							System.out.println("Current groupDescription:" + groupDescription);
						}

						if (groupDescription.isLastGroup()) {
							break;
						}
					}

					ProcessInstance processInstance = (ProcessInstance) formFillerUtil
							.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
								@Override
								public ProcessInstance submitForm(Map<String, String> fieldValues) {
									return formService.submitStartFormData(definition.getId(), fieldValues);
								}
							});
					String processId = processInstance.getProcessInstanceId();
					RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRuntimeService();

					String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));

					restResult.setMessage(ticketNo);
					List<String> ticketNoArr = new ArrayList<>();
					Map<String, String> result = new HashMap<String, String>();
					ticketNoArr.add(ticketNo);
					result.put("success", "true");
					restResult.setResponse(result);
					restResult.setTicketNo(ticketNoArr);
					restResult.setMessage("Ticket created successfully");
					restResult.setSuccess(true);
					restResult.setApiToken(apiToken);
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								ReportPojo pojo = new ReportPojo(processId);
								pojo.sentToReportServer(pojo);
								TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(processId),
										processId, "TICKET_CREATED");
							} catch (Exception e) {
								log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

							}
						}
					}).start();
					if (setVariableExternalFields(fieldValues, runtimeService, processId, masterdataService)) {

					} else {
						log.debug("problem in setting external variable in create ticket with volmatric weight etc");

					}
				}
			}
		} catch (Exception e) {
			restResult.setMessage("ERROR " + e);
			restResult.setSuccess(false);
		}
		RetailerApiCalls.saveToConsoleLogsTable(retailerId, "Pick and Drop One Ways " + " Request Json: " + queryJSON
				+ " Response:" + restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;

	}

//	@RequestMapping(value = "/ticket/create/v1/pickanddrop/oneway", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketRepairStandByV1(@RequestBody String queryJSON,
			@RequestHeader(value = "retailerId") String retailerId,
			@RequestHeader(value = "apiToken", required = true) String apiToken) {
		RestExternalResult restResult = new RestExternalResult();

		try {
			if (PNDOneWayPrimaryValidation(restResult, retailerId, apiToken, queryJSON)) {
				Map<String, String> fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
						.getDefaultProcessEngine())).getMasterdataService();
				if (fieldValues.get("productCategory").equals("mobile")) {
					try {
						RetailerOther retailerOther = masterdataService.createRetailerOtherQuery()
								.retailerId(retailerId).singleResult();
						if (retailerOther.getIsMobileProductAddAllow() == 1) {
							addMobileProduct(queryJSON, masterdataService, retailerId, fieldValues);
						} else {
							log.info("This Add Mobile Product is not valid for this Retailer");
						}
					} catch (Exception e) {
						log.info("Nullpointer exception This Add Mobile Product is not valid for this Retailer");

					}
				} else {
					addAllProduct(queryJSON, masterdataService, retailerId, fieldValues);
				}
				if (secondryValidationPND1W(restResult, retailerId, apiToken, fieldValues)) {

					Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
					String retailer = retailerObj.getName();
					fieldValues.put("retailer", retailer);

					MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getMetadataService();
					RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRepositoryService();

					FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
							.getFormService();

					String dataset = "BizLog";
					ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset)
							.dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();

					ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
							.deploymentId(deployment.getDeploymentId()).singleResult();

					FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(), false);
					GroupDescription groupDescription = null;
					while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
						if (log.isDebugEnabled()) {
							log.debug("Current groupDescription:" + groupDescription);
							System.out.println("Current groupDescription:" + groupDescription);
						}

						if (groupDescription.isLastGroup()) {
							break;
						}
					}

					ProcessInstance processInstance = (ProcessInstance) formFillerUtil
							.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
								@Override
								public ProcessInstance submitForm(Map<String, String> fieldValues) {
									return formService.submitStartFormData(definition.getId(), fieldValues);
								}
							});
					String processId = processInstance.getProcessInstanceId();
					RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRuntimeService();

					String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));

					restResult.setMessage(ticketNo);
					List<String> ticketNoArr = new ArrayList<>();
					Map<String, String> result = new HashMap<String, String>();
					ticketNoArr.add(ticketNo);
					result.put("success", "true");
					restResult.setResponse(result);
					restResult.setTicketNo(ticketNoArr);
					restResult.setMessage("Ticket created successfully");
					restResult.setSuccess(true);
					restResult.setApiToken(apiToken);
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								ReportPojo pojo = new ReportPojo(processId);
								pojo.sentToReportServer(pojo);
								TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(processId),
										processId, "TICKET_CREATED");
							} catch (Exception e) {
								log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

							}
						}
					}).start();
					if (setVariableExternalFields(fieldValues, runtimeService, processId, masterdataService)) {

					} else {
						log.debug("problem in setting external variable in create ticket with volmatric weight etc");

					}
				}
			}
		} catch (Exception e) {
			restResult.setMessage("ERROR " + e);
			restResult.setSuccess(false);
		}
		RetailerApiCalls.saveToConsoleLogsTable(retailerId, "Pick and Drop One Ways " + " Request Json: " + queryJSON
				+ " Response:" + restResult.getTicketNo().toString() + " Message: " + restResult.getMessage());

		return restResult;

	}

	private boolean setVariableExternalFields(Map<String, String> fieldValues, RuntimeService runtimeService,
			String processId, MasterdataService masterdataService) {
		try {
			if ((fieldValues.containsKey("amountToBeCollected")) && fieldValues.get("amountToBeCollected") != null) {
				runtimeService.setVariable(processId, "rlAmountToBeCollected", fieldValues.get("amountToBeCollected"));
				// runtimeService.setVariable(processId, "rlCustomerPay",
				// "Yes");

			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volLenght")) && fieldValues.get("volLenght") != null) {
				runtimeService.setVariable(processId, "rlVolLenght", fieldValues.get("volLenght"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volHeight")) && fieldValues.get("volHeight") != null) {
				runtimeService.setVariable(processId, "rlVolHeight", fieldValues.get("volHeight"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("volWidth")) && fieldValues.get("volWidth") != null) {
				runtimeService.setVariable(processId, "rlVolWidth", fieldValues.get("volWidth"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("actualWeight")) && fieldValues.get("actualWeight") != null) {
				runtimeService.setVariable(processId, "rlActualWeight", fieldValues.get("actualWeight"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("transactionType")) && fieldValues.get("transactionType") != null) {
				runtimeService.setVariable(processId, "rlTransactionType", fieldValues.get("transactionType"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountto be collected" + e);
		}
		// ------------------------ for accessories
		try {
			if ((fieldValues.containsKey("BoxPickUp")) && fieldValues.get("BoxPickUp") != null) {
				if (fieldValues.get("BoxPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlPouchPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlPouchPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlPouchPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("BatteryPickUp")) && fieldValues.get("BatteryPickUp") != null) {
				if (fieldValues.get("BatteryPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlBatteryPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlBatteryPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlBatteryPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("HeadPhonePickUp")) && fieldValues.get("HeadPhonePickUp") != null) {
				if (fieldValues.get("HeadPhonePickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlHeadPhonePickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlHeadPhonePickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field HeadPhonePickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("ChargerPickUp")) && fieldValues.get("ChargerPickUp") != null) {
				if (fieldValues.get("ChargerPickUp").equals("Yes")) {
					runtimeService.setVariable(processId, "rlChargerPickUp", "true");
				} else {
					runtimeService.setVariable(processId, "rlChargerPickUp", "false");
				}
			}
		} catch (Exception e) {
			log.debug("There is no field rlChargerPickUp be collected" + e);
		}
		try {
			if ((fieldValues.containsKey("OthersPickUp")) && fieldValues.get("OthersPickUp") != null) {
				runtimeService.setVariable(processId, "rlOthersPickUp", fieldValues.get("OthersPickUp"));
			}
		} catch (Exception e) {
			log.debug("There is no field OthersPickUp be collected" + e);
		}
		// for recycle device OTP
		try {
			if (runtimeService.getVariable(processId, "rlServiceCode").toString().equals("BUYBACK")) {
				if (runtimeService.getVariable(processId, "rlRetailerCode").toString().equals("RCB")) {
					runtimeService.setVariable(processId, "rlOtpVariable", "");
					runtimeService.setVariable(processId, "rlOtp", "Yes");
				} else {
					runtimeService.setVariable(processId, "rlOtpVariable", "");
					runtimeService.setVariable(processId, "rlOtp", "No");
				}

			}

		} catch (Exception e) {
			log.debug("There is no field OthersPickUp be collected" + e);
		}
		// FOR PAYMENT VARIABLE
		try {
			if ((fieldValues.containsKey("amountToBePaid")) && fieldValues.get("amountToBePaid") != null) {
				runtimeService.setVariable(processId, "amountToBePaid", fieldValues.get("amountToBePaid"));
			}
		} catch (Exception e) {
			log.debug("There is no field amountToBePaid" + e);
		}
		try {
			if ((fieldValues.containsKey("modeOfPayment")) && fieldValues.get("modeOfPayment") != null) {
				runtimeService.setVariable(processId, "modeOfPayment", fieldValues.get("modeOfPayment"));
			}
		} catch (Exception e) {
			log.debug("There is no field modeOfPayment" + e);
		}

		/**
		 * Setting Report Date For tickets generated by api
		 */

		DateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date = new Date();
		s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		String tt = s.format(date);
		DateTime dateTimeIndia = new DateTime(tt);
		Date actDate = dateTimeIndia.toDate();
		runtimeService.setVariable(processId, "ticketCreationDate", actDate);
		log.debug("TicketCreation DATE" + String.valueOf(runtimeService.getVariable(processId, "ticketCreationDate")));

		/**
		 * Setting City for report generated by api
		 */
		if ((fieldValues.containsKey("pincode")) && fieldValues.get("pincode") != null) {

			try {
				LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery()
						.pincode(fieldValues.get("pincode")).singleResult();

				ProcessLocation processLocation = masterdataService.createProcessLocationQuery()
						.id(locationPincodes.getLocationId()).singleResult();

				runtimeService.setVariable(processId, "rlReportingCity", processLocation.getCity());
				log.debug(runtimeService.getVariable(processId, "rlReportingCity").toString());

			} catch (Exception e) {
				log.debug("Error in putting value to reporting city" + e);
			}
		}
		return true;

	}

	private boolean secondryValidationPND1W(RestExternalResult restResult, String retailerId, String apiToken,
			Map<String, String> fieldValues) {
		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("dropLocPincode"))) {
			errorMeesage.append(fieldValues.get("dropLocPincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateProductBrand()) {
			errorMeesage.append(fieldValues.get("brand") + " Brand is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;
	}

	private boolean PNDOneWayPrimaryValidation(RestExternalResult restResult, String retailerId, String apiToken,
			String queryJSON) {

		/**
		 * productName model productCode brand identificationNo
		 */
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;
		String productNameerrorMessage = "";
		String modelerrorMessage = "";
		String productCodeerrorMessage = "";
		String branderrorMessage = "";
		String identificationNomodelerrorMessage = "";
		String productCategoryerrorMessage = "";

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {
			if (checkForNewRetailer(retailerId)) {
				value = checkForFieldsInLoopForNewRetailer(fieldValues, value, finalErrorMeesage);
			} else {
				value = checkForFieldsInLoopForOldRetailer(fieldValues, value, finalErrorMeesage);
			}

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;

	}

	private boolean checkForFieldsInLoopForNewRetailer(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}

			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;

	}

	private boolean checkForNewRetailer(String retailerId) {
		boolean rtnValue = false;
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			ArrayList<String> newRetailerName = new ArrayList<>();
			newRetailerName.add("OA");

			if (newRetailerName.contains(retailer.getCode().toString())) {
				rtnValue = true;
			}
		} catch (Exception e) {

		}

		return rtnValue;
	}

	private boolean checkForFieldsInLoopForOldRetailer(JSONObject fieldValues, boolean value,
			StringBuilder finalErrorMeesage) {
		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		// fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}

		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}
			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;
	}

	public void addMobileProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {

		/**
		 * These value we get from retailer related to product "brand": "Apple",
		 * "productCategory": "mobile", "productName": "Iphone 7",
		 * "productCode": "iPhone 7", "model": "S",
		 */

		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}

				log.debug("22222222222222");

				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					/**
					 * =masterdataService.createProductCategoryQuery().name(
					 * "Mobile" THIS "Mobile" string should be same in
					 * RL_MD_PRODUCT_CATEGORY IN Production server ,dev or in
					 * test
					 */
					ProductCategory productCategory = masterdataService.createProductCategoryQuery().name("Mobile")
							.singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());// SUBCATEGORY ID
																// FROM
																// RL_MD_RETAILER_CATEGORIES
																// TABLE
					/**
					 * THIS PRODUCT CATEGORY SHOULD BE ADD IN
					 * RL_MD_RETAILER_CATEGORIES AND GET PRODUCT_id_ HERE
					 */
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());// Wrap
																		// Box
																		// 35x23x11
																		// from
																		// RL_MD_PACKAGING_TYPE
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Bubble Cover 9x6")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());// Bubble
																			// Cover
																			// 9x6
																			// from
																			// RL_MD_PACKAGING_TYPE
					product.setBrandId(newBrandId);// IT SHOULD BE BRAND ID FROM
													// RL_MD_BRAND
					/**
					 * THIS BRAND SHOULD BE ADD IN RL_MD_BRAND AND GET HERE THE
					 * BARND_ID_
					 */

					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");

					log.info("New Product Added Successfully");
					/**
					 * after add the product this product should be allow to
					 * this retailer this is the error message Status false
					 * iphone 7prem is not a valid Product Code of Demo
					 * Retailer.null
					 */
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

	public void addAllProduct(String jsonValue, MasterdataService masterdataService, String retailerId,
			Map<String, String> fieldValues1) {
		try {
			JSONObject fieldValues = new JSONObject(jsonValue);
			Brand brandObj = null;
			String newBrandId = null;
			/**
			 * checking this product is added with this retailer or not
			 */
			if (!fieldValues.getString("productCategory").equalsIgnoreCase("mobile")) {

				try {
					brandObj = masterdataService.createBrandQuery().name(fieldValues.getString("brand").toUpperCase())
							.singleResult();
					newBrandId = brandObj.getId();
					log.debug("Brand ID " + brandObj.getId() + " Brand Name" + brandObj.getName());
				} catch (Exception e) {
					log.debug("111222" + e.getMessage());
					newBrandId = addNewBrand(masterdataService, fieldValues);
				}
				try {
					Product checkProduct = masterdataService.createProductQuery().retailerId(retailerId)
							.brandId(newBrandId).model(fieldValues.getString("model").toUpperCase())
							.name(fieldValues.getString("productName").toUpperCase())
							// .code(fieldValues.getString("productCode").toUpperCase())
							.singleResult();
					log.debug("******************** product isalready there in db search in db "
							+ checkProduct.getName() + "  " + checkProduct.getId());
					fieldValues1.put("productCode", checkProduct.getCode());

				} catch (Exception e) {

					log.debug("*************ADDING NWE PRODUCT");
					Product product = masterdataService.newProduct();
					UUID id = UUID.randomUUID();
					product.setId(String.valueOf(id));
					product.setName(fieldValues.getString("productName").toUpperCase());
					String productCode = randomAlphaNumeric(20);
					product.setCode(productCode);
					fieldValues1.put("productCode", productCode);
					// product.setDescription("description");
					ProductCategory productCategory = masterdataService.createProductCategoryQuery()
							.name(fieldValues.getString("productCategory")).singleResult();
					ProductSubCategory category = masterdataService.createProductSubCategoryQuery()
							.productCategoryId(productCategory.getId()).singleResult();
					product.setSubCategoryId(category.getId());
					product.setModel(fieldValues.getString("model").toUpperCase());
					PackagingType packagingTye = masterdataService.createPackagingTypeQuery().name("Wrap Box 35x23x11")
							.singleResult();
					product.setPackagingTypeBoxId(packagingTye.getId());
					product.setRetailerId(retailerId);
					PackagingType packagingType = masterdataService.createPackagingTypeQuery().name("Other")
							.singleResult();
					product.setPackagingTypeCoverId(packagingType.getId());
					product.setBrandId(newBrandId);
					product.setVolumetricWeight(Double.valueOf("150"));

					product.setApproxValue(Double.valueOf("10000"));

					product.setActualWeight(Double.valueOf("150"));
					masterdataService.saveProduct(product);
					log.debug("******************** product added in table");
					log.info("New Product Added Successfully");
				}

			} else {
				log.info("Product is not Mobile");
			}

		} catch (Exception e) {
			log.error("****************Some Value is missing in product add field: " + e);
			log.error("***************Mobile Produvt is Not added " + e);

		}

	}

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public String addNewBrand(MasterdataService masterdataService, JSONObject fieldValues) {
		Brand brand = masterdataService.newBrand();
		String brandId = String.valueOf(UUID.randomUUID());

		brand.setId(brandId);
		brand.setName(fieldValues.getString("brand").toUpperCase());
		brand.setCode(randomAlphaNumeric(20));
		// brand.setDescription("description");

		masterdataService.saveBrand(brand);
		log.debug("*************************New Brand is Added");

		return brandId;
	}

}
