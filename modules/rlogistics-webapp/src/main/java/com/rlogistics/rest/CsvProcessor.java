package com.rlogistics.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * Reads data from CSV file
 * @author Adarsh
 */
@Slf4j
public class CsvProcessor {
    /**
     * @param file = Multipart File
     * @return MappingIterator in itirating Object
     */
    //TODO not working due to Fasterxml CSV dependency mismatch with its transitive Databind dependency
//    public MappingIterator<Map<String, String>> processCsvData(MultipartFile file) {
//        CsvMapper csvMapper = new CsvMapper();
//        CsvSchema csvSchema = CsvSchema.emptySchema().withHeader();
//        MappingIterator<Map<String, String>> mapMappingIterator;
//        try {
//            mapMappingIterator = csvMapper.readerFor(Map.class).with(csvSchema).readValues(file.getInputStream());
//            return mapMappingIterator;
//
//        } catch (Exception e) {
//            log.debug("Failed In processing CSV" + e);
//            return null;
//        }
//

    /**
     * Reads multipart file of CSV and converts them to Java collections type
     * @param file Multipart File
     * @return List<Map       <       String       ,       String>>
     */
    public List<Map<String, String>> processCsvData(MultipartFile file) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            //Reading the header as first line of String and splitting
            String[] headers = bufferedReader.readLine().split(",");
            /**
             * λ to map and collect elements from inputStream to List<Map<String,String>>
             */
            List<Map<String, String>> records =
                    bufferedReader.lines().map(s -> s.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")) //regex  : not to split "," inside double quote
                            .map(t -> IntStream.range(0, t.length)
                                    .boxed()
                                    .collect(toMap(i -> headers[i], i -> t[i])))//Collecting as map
                            .collect(toList()); //collecting map as list
            return records;

        } catch (IOException e) {
            log.debug("Error" + e);
            return null;
        }

    }
}
