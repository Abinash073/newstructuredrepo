package com.rlogistics.rest;

import java.util.Map;
import java.util.UUID;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.TicketStatusHistory;
import com.rlogistics.master.identity.MasterdataService;

import pojo.ReportPojo;

public class ApparelsActivitiCalls {
	private static Logger log = LoggerFactory.getLogger(ApparelsActivitiCalls.class);


	public static void pndOneWay(String processId, String status) {
		new Thread(new Runnable() {
			public void run() {
				try {
					RetailerApiCalls.saveToConsoleLogsTable(processId, status);
					Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRuntimeService().getVariables(processId);
					insertIntoHistoryTable(variables, processId, status);
				} catch (Exception e) {
					log.debug(e+"");
				}
			}
		}).start();
	}

	public static void pndTwoWay(String processId, String status) {
		new Thread(new Runnable() {
			public void run() {
				try {
					RetailerApiCalls.saveToConsoleLogsTable(processId, status);
					Map<String, Object> variables = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRuntimeService().getVariables(processId);
					insertIntoHistoryTable(variables, processId, status);
				} catch (Exception e) {
					log.debug(e+"");
				}
			}
		}).start();

	}

	public static void insertIntoHistoryTable(Map<String, Object> variables, String rlProcessId, String statusString) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			TicketStatusHistory statusHistory = masterdataService.newTicketStatusHistory();
			UUID id = UUID.randomUUID();
			statusHistory.setId(String.valueOf(id));
			statusHistory.setProcInstId(rlProcessId);
			statusHistory.setTicketNo(variables.get("rlTicketNo").toString());
			String sta = getMappedStatus(variables, statusString, rlProcessId);
			statusHistory.setStatus(sta);
			statusHistory.setCreateTime(ReportPojo.getCurrectTimeStamp());
			if (!sta.equals(""))
				masterdataService.saveTicketStatusHistory(statusHistory);
		} catch (Exception e) {
		}
	}

	private static String getMappedStatus(Map<String, Object> variables, String statusString, String rlProcessId) {

		String rtnString = "";
		switch (variables.get("rlServiceCode").toString()) {
		case "PICKNDROPONEWAY":
			switch (statusString) {
			case "TICKET_CREATED":
				rtnString = "Ticket Created";

				break;
			case "APPOINTMENT_FIXING":
				rtnString = "Appointment Fixed";

				break;
			case "PRODUCT_PICKED_UP":
				rtnString = "Product Picked up";

				break;
			case "PRODUCT_DELIVERED":
				rtnString = "Product Delivered to Drop Location";

				break;
			case "CUSTOMER_REJECTED_TO_COORDINATOR":
				rtnString = "Rescheduled from Customer While Drop";

				break;
			case "CUSTOMER_NEEDS_RESCHEDULE_FE_DOORSTEP":
				rtnString = "Product Delivered to Drop Location";

				break;

			}
		case "PICKNDROPTWOWAYS":
			switch (statusString) {
			case "TICKET_CREATED":
				rtnString = "Ticket Created";

				break;
			case "APPOINTMENT_FIXING":
				rtnString = "Appointment Fixed";

				break;
			case "PRODUCT_PICKED_UP":
				rtnString = "Product Picked up";

				break;
			case "PRODUCT_DELIVERED":
				rtnString = "Product Delivered to Drop Location";

				break;
			case "CUSTOMER_REJECTED_TO_COORDINATOR":
				rtnString = "Rescheduled from Customer While Drop";

				break;
			case "CUSTOMER_NEEDS_RESCHEDULE_FE_DOORSTEP":
				rtnString = "Product Delivered to Drop Location";

				break;

			}
		}
		return rtnString;

	}

}
