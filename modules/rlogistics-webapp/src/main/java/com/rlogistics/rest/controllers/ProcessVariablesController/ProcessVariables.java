package com.rlogistics.rest.controllers.ProcessVariablesController;

import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * To get the process variable
 */
@RestController
public class ProcessVariables {

    private RuntimeService runtimeService;

    @Autowired
    public ProcessVariables(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    /**
     * Controller to fetch process variable
     *
     * @param processId Process Id of ticket
     * @param variable  Variable Name
     * @return Variable Value
     */
    @RequestMapping(value = "/get/process-variables", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getVariables(@RequestParam(value = "processId") String processId,
                               @RequestParam(value = "variableName") String variable) {
        return String.valueOf(runtimeService.getVariable(processId, variable));
    }
}
