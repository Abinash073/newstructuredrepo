package com.rlogistics.rest.franchise;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public interface RlFranchiseLoginHandler extends Serializable {


    FranchiseUser authenticate(String userName, String password, HttpServletResponse response) throws Exception;


    FranchiseUser authenticate(HttpServletRequest request, HttpServletResponse response);


    void logout(FranchiseUser userTologout);


    void onRequestStart(HttpServletRequest request, HttpServletResponse response);


    void onRequestEnd(HttpServletRequest request, HttpServletResponse response);
}
