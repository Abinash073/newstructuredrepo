package com.rlogistics.rest;

import com.rlogistics.http.RLogisticsResource;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.ProcessUser;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;


import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class UiComponetLogic extends RLogisticsResource {


    @Autowired
    MasterdataService masterdataService;
    RuntimeService runtimeService;


    private UserDetails getUser(String userEmail) {
        UserDetails userDetails = new UserDetails();
        ProcessUser processUser = masterdataService.createProcessUserQuery().email(userEmail).singleResult();
        ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(processUser.getLocationId()).singleResult();
        userDetails.setFirstName(processUser.getFirstName());
        userDetails.setLastName(processUser.getLastName());
        userDetails.setLocation(processLocation.getCity());
        userDetails.setRoleCode(processUser.getRoleCode());
        userDetails.setEmail(processUser.getEmail());
        return userDetails;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "get/involved-user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseWithCount getUserDetails(@RequestParam(value = "processId") String processId, HttpServletRequest request, HttpServletResponse response) {
        if (!beforeMethodInvocation(request, response)) {
            return null;
        }
        List<UserDetails> detailsInlist;
        ResponseWithCount responseWithCount = new ResponseWithCount();
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray;
        try {

            //Fething and coverting it to json array
            jsonArray = (JSONArray) jsonParser.parse(String.valueOf(ExotelForm.findByProcessIdandvariable(processId, "rlInvolvedUser")));

            //noinspection unchecked
            detailsInlist = (ArrayList<UserDetails>) jsonArray.stream().map(o -> getUser(o.toString())).collect(Collectors.toList());
            responseWithCount.setData(detailsInlist);
            responseWithCount.setSuccess(true);
            responseWithCount.setMessage("Fetched");
            responseWithCount.setTotalCount(detailsInlist.size());
            return responseWithCount;


        } catch (ParseException e) {
            log.debug("Error while parsing" + e);

        } catch (Exception e) {
            log.debug("Error while geting involved user" + e);
        }


        return responseWithCount;
    }

}
