package com.rlogistics.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.RetailerReturnOrPickupLocations;
import com.rlogistics.master.identity.City;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.MasterdataService;

@RestController
public class SellerCodeController {
	@RequestMapping(value="/get-seller-code",method = RequestMethod.POST, produces = "application/json")
	public Map<String, Object> getSellerCode(@RequestBody String req) {
		Map<String, Object> rtnMap = new HashMap<>();

		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			JSONObject jsonObject = new JSONObject(req);
			if (jsonObject.length() == 0) {
				List<City> cities = masterdataService.createCityQuery().list();
				rtnMap.put("response", cities);
				rtnMap.put("success", true);
				rtnMap.put("msg", "City List");

			} else {

				String city = jsonObject.getString("city");
				List<RetailerReturnOrPickupLocations> locations = masterdataService
						.createRetailerReturnOrPickupLocationsQuery().city(city).list();
				rtnMap.put("response", locations);
				rtnMap.put("success", true);
				rtnMap.put("msg", "Seller  List");
			}

		} catch (Exception e) {
			rtnMap.put("success", false);
			rtnMap.put("msg", "ERROR : " + e);

		}

		return rtnMap;

	}

}
