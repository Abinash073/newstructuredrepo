package com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.RetailerName;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RetailerNameValidator implements ConstraintValidator<RetailerName, String> {
    private MasterdataService masterdataService;

    @Autowired
    public RetailerNameValidator(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    @Override
    public void initialize(RetailerName constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isRnameValid(value);
    }

    private synchronized boolean isRnameValid(String v) {
        return (int) masterdataService.createRetailerQuery().name(v).count() == 1;
    }
}
