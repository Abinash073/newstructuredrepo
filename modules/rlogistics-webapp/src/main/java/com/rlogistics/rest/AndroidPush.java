package com.rlogistics.rest;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class AndroidPush {
	private static final Logger log = Logger.getLogger(AndroidPush.class);
	public final static String AUTH_KEY_FCM = "AAAADA4eElo:APA91bH7qSn1eomCWPsv3c1bRGkzCeOkxXJInMGxKf5C9zNcscChFobk18E9HnNW9MvMxot7f6JKs4N58bRW6OiJXPQoWSWMUdPehkHVlSWeVE8ZSrTzF3UQlPQfRsThwJUrIdbU9gak";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

	public static boolean notifySingleDevice(String deviceId, String content) {
		boolean res = false;
		try {
			Sender sender = new Sender("AIzaSyATkpslaL-CLhhMcrjIbqw0t5iTDlrniSs");
			Message message = new Message.Builder().collapseKey("1").timeToLive(30).delayWhileIdle(true)
					.addData("message", content).build();
			Result result = sender.send(message, deviceId, 2);

			if (StringUtils.isEmpty(result.getErrorCodeName())) {
				res = true;
			}
		} catch (Exception e) {
			log.error("Exception", e);
			log.trace("Exception Occured", e);
		}
		return res;
	}

	public static boolean notifyMultipleDevices(ArrayList<String> deviceList, String content) throws Exception {
		boolean res = false;
		try {
			Sender sender = new Sender("AIzaSyATkpslaL-CLhhMcrjIbqw0t5iTDlrniSs");
			Message message = new Message.Builder().collapseKey("1").timeToLive(30).delayWhileIdle(true)
					.addData("message", content).build();
			log.debug("Message object---->" + message);
			MulticastResult result = sender.send(message, deviceList, 2);

			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
					res = true;
					log.debug("Successfully sent");
				} else {
					log.error("Notification not sent");
				}
			}
		} catch (Exception e) {
			log.error("Exception during sending notification : " + e.getMessage(), e);
			log.trace("Exception Occured", e);
			throw e;
		}
		return res;
	}

	public static void pushFCMNotification(String userDeviceIdKey, String processId, String type, String value)
			throws Exception {

		String authKey = AUTH_KEY_FCM; // You FCM AUTH key
		String FMCurl = API_URL_FCM;

		URL url = new URL(FMCurl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");
		JSONObject json = new JSONObject();
		json.put("to", userDeviceIdKey.trim());
		JSONObject info = new JSONObject();
		info.put("noti_type", type); // Notification title
		info.put("noti_value", value); // Notification body
		info.put("processId", processId);
		json.put("data", info);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();
	}

	public static void pushFCMNotification(String userDeviceIdKey, String processId, String type, String value,
			String ticketNo) throws Exception {
		String authKey = AUTH_KEY_FCM; // You FCM AUTH key
		String FMCurl = API_URL_FCM;

		URL url = new URL(FMCurl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");
		JSONObject json = new JSONObject();
		json.put("to", userDeviceIdKey.trim());
		JSONObject info = new JSONObject();
		info.put("noti_type", type); // Notification title
		info.put("noti_value", value); // Notification body
		info.put("processId", processId);
		info.put("ticketNo", ticketNo);
		json.put("data", info);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();

	}

	/*
	 * public static void main(String[] args) { ArrayList<String> deviceList =
	 * new ArrayList<String>(); String deviceId =
	 * "e3wlga1CIpU:APA91bG7Ffc9A5mvxUDMwMFJFR8zKGwE182_OglHyN60on0La0pIJCtJbsBmiHQ9iwj-JqlZUEXfjaoY329R_3pJZvmaJqSX8XbAulvj2-u8WkVyt4LhEKLRz6ow_nXBHfgcjbJ-dZOE";
	 * deviceList.add(deviceId); String message = "This is a test message";
	 * 
	 * try { pushFCMNotification(deviceId, "Test message"); } catch (Exception
	 * e) { // TODO Auto-generated catch block e.printStackTrace(); } }
	 */
}
