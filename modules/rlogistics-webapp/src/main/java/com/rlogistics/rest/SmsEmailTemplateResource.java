package com.rlogistics.rest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.SmsEmailTemplate;
import com.rlogistics.master.identity.TicketColumns;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RetailerProcessField;
import com.rlogistics.master.identity.RetailerSmsEmailTemplate;

@RestController
public class SmsEmailTemplateResource extends RLogisticsResource{

@RequestMapping(value = {"/copy/master-templates/{retailerId}"}, produces = "application/json")
private RestResponse copyMasterTemplates(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable(value = "retailerId") String retailerId) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		int count = 0;
		Map<String, Integer> result = new HashMap<String, Integer>();
		try {
			
			if(!beforeMethodInvocation(httpRequest,httpResponse)) {
				throw new RuntimeException("rlToken null/invalid");
			}
		List<SmsEmailTemplate> masterTemplates = masterdataService.createSmsEmailTemplateQuery().list();
		
		for(SmsEmailTemplate masterTemplate:masterTemplates) {
			RetailerSmsEmailTemplate retailerSmsEmailTemplate = masterdataService.newRetailerSmsEmailTemplate();
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			String id = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			retailerSmsEmailTemplate.setId(id);
			retailerSmsEmailTemplate.setRetailerId(retailerId);
			retailerSmsEmailTemplate.setSmsEmailTriggerCode(masterTemplate.getSmsEmailTriggerCode());
			retailerSmsEmailTemplate.setEmailTemplate(masterTemplate.getEmailTemplate());
			if(masterTemplate.getSmsTemplate() != null) {
				retailerSmsEmailTemplate.setSmsTemplate(masterTemplate.getSmsTemplate());
			} 
			/*if(masterTemplate.getSetupScript() != null) {
				retailerSmsEmailTemplate.setSetupScript(masterTemplate.getSetupScript());
			}*/
			if(masterTemplate.getParams() != null) {
				retailerSmsEmailTemplate.setParams(masterTemplate.getParams());
			}
			if(masterTemplate.getSubject() != null) {
				retailerSmsEmailTemplate.setSubject(masterTemplate.getSubject());
			}
			try {
				masterdataService.saveRetailerSmsEmailTemplate(retailerSmsEmailTemplate);
			} catch (Exception e) {
				
			}
			count++;
			result.put("noOfRecordsCopied", count);
			restResponse.setResponse(result);
			restResponse.setMessage("Master templates copied to Retailer");
			
		}
		
		/*//add retailer information to retailer process field
		List<TicketColumns> ticketColumns = masterdataService.createTicketColumnsQuery().list();
		for(TicketColumns column:ticketColumns) {
			RetailerProcessField retailerProcessField = masterdataService.newRetailerProcessField();
			c.setTime(new Date());
			String uId = RandomStringUtils.randomAlphanumeric(8).toLowerCase() + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			retailerProcessField.setId(id);
			retailerProcessField.setRetailerField(column.getField());
			retailerProcessField.setCanonicalField(column.getField());
			retailerProcessField.setRetailerId(retailerId);
			
			masterdataService.saveRetailerProcessField(retailerProcessField);
		}*/
			
		} catch(RuntimeException ex){
			restResponse.setMessage(ex.getMessage());
		}
		return restResponse;
	}
}
