package com.rlogistics.rest;

import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Brand;
import com.rlogistics.master.identity.ImeiInventory;
import com.rlogistics.master.identity.LocationPincodes;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.Product;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerCategories;
import com.rlogistics.master.identity.RetailerReturnOrPickupLocations;
import com.rlogistics.master.identity.RetailerServiceableLocations;
import com.rlogistics.master.identity.RetailerServices;
import com.rlogistics.master.identity.ServiceMaster;

/*
 * create by prem 21-11-2018 for fields validation
 */
public class TicketFieldsValidationImp implements TicketFieldsValidationInter {

	private MasterdataService masterdataService;
	private Map<String, String> fieldValues;
	private String retailerId;
	private static Logger log = LoggerFactory.getLogger(TicketFieldsValidationImp.class);

	TicketFieldsValidationImp(Map<String, String> fieldValues, String retailerId) {
		masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		this.fieldValues = fieldValues;
		this.retailerId = retailerId;
	}

	@Override
	public boolean validateReatilerCategory() {
		boolean returnValue = false;
		try {
			ProductCategory productCategory = masterdataService.createProductCategoryQuery()
					.name(fieldValues.get("productCategory")).singleResult();
			RetailerCategories retailerCategories = masterdataService.createRetailerCategoriesQuery()
					.retailerId(retailerId).categoryId(productCategory.getId()).singleResult();
			if (productCategory != null && retailerCategories != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateRetailerId() {
		boolean returnValue = false;
		try {
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			if (retailer != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateRetailerAPIToken(String retailerApiToken) {
		boolean returnValue = false;
		try {
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			if (retailer != null && retailer.getApiToken().equals(retailerApiToken)) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateDropPincode() {
		boolean returnValue = false;
		try {
			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("dropLocPincode")).singleResult();
			if (retailerServiceableLocations != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validatePickupPincode() {
		boolean returnValue = false;
		try {
			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(fieldValues.get("pincode")).singleResult();
			if (retailerServiceableLocations != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateProductBrand() {
		boolean returnValue = false;
		try {
			Brand brand = masterdataService.createBrandQuery().name(fieldValues.get("brand")).singleResult();
			if (brand != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateProductDetails() {
		boolean returnValue = false;
		try {
			Product product = masterdataService.createProductQuery().retailerId(retailerId)
					.model(fieldValues.get("model").toUpperCase()).name(fieldValues.get("productName").toUpperCase())
					.singleResult();
			if (product != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateRetailerNatureOfComplaint() {
		boolean returnValue = false;
		try {
			ServiceMaster serviceMaster = masterdataService.createServiceMasterQuery()
					.name(fieldValues.get("natureOfComplaint")).singleResult();
			RetailerServices retailerServices = masterdataService.createRetailerServicesQuery().retailerId(retailerId)
					.serviceId(serviceMaster.getId()).singleResult();
			if (serviceMaster != null && retailerServices != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean validateBizlogServiceAbleLocation(String pincode) {
		boolean returnValue = false;
		try {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery().pincode(pincode)
					.singleResult();
			if (locationPincodes != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public String getRetailerCode() {
		String returnValue = "";
		try {
			Retailer retailer = masterdataService.createRetailerQuery().id(retailerId).singleResult();
			if (retailer != null) {
				returnValue = retailer.getCode();
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

	@Override
	public boolean checkRepairStandByDeviceIMEI(String repairStandByIMEI, String pincode) {
		boolean returnValue = false;
		try {
			LocationPincodes locationPincodes = masterdataService.createLocationPincodesQuery().pincode(pincode)
					.singleResult();
			ProcessLocation processLocation = masterdataService.createProcessLocationQuery()
					.id(locationPincodes.getLocationId()).singleResult();
			ImeiInventory imeiInventory = masterdataService.createImeiInventoryQuery().retailerId(retailerId)
					.location(processLocation.getCity()).imeiNo(repairStandByIMEI).status(0).singleResult();
			if (imeiInventory != null) {
				returnValue = true;
			}
		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}

	@Override
	public boolean checkRetailerCode(String retailerCode, String retailerPincode) {
		boolean returnValue = false;
		try {
			RetailerReturnOrPickupLocations retailer = masterdataService.createRetailerReturnOrPickupLocationsQuery()
					.retailerId(retailerId).sellerCode(retailerCode).sellerPincode(retailerPincode).singleResult();
			if (retailer != null) {
				returnValue = true;
			}
		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}
	@Override
	public boolean validateRetailerPincode(String pincode) {
		boolean returnValue = false;
		try {
			RetailerServiceableLocations retailerServiceableLocations = masterdataService
					.createRetailerServiceableLocationsQuery().retailerId(retailerId)
					.pincode(pincode).singleResult();
			if (retailerServiceableLocations != null) {
				returnValue = true;
			}
		} catch (Exception e) {
		}
		return returnValue;
	}

}
