package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ReportExcelUtilHelper {

  private LinkedHashMap<String,String> cvMap= new LinkedHashMap<>();
    public ReportExcelUtilHelper() {
        //No use of value : just storing for reference
       cvMap.put("S.No",String.valueOf(cvMap.size()));
       cvMap.put("Retailer Name","retailer");
       cvMap.put("Service Type","natureOfComplaint");
       cvMap.put("Ticket Creation Date","ticketCreationDate");
       cvMap.put("Ticket Creation Time","");
       cvMap.put("Consumer Name","consumerName");
       cvMap.put("Phone No.","telephoneNumber");
       cvMap.put("ProcessId","processId");
       cvMap.put("Consumer Request No","consumerComplaintNumber");
       cvMap.put("Ticket No.","rlTicketNo");
       cvMap.put("Product","product");
       cvMap.put("Product Category","productCategory");
       cvMap.put("Code","productCode");
       cvMap.put("Brand","brand");
       cvMap.put("Model","model");
       cvMap.put("Pick-Up Location","addressline");
       cvMap.put("Drop Location","dropLocation");
       cvMap.put("Mode Of Payment","rlModeOfPayment");
       cvMap.put("Status","rlTicketStatus");
       cvMap.put("City","rlReportingCity");
       cvMap.put("Involved Users","rlInvolvedUser");
       cvMap.put("Offer Amount","rlValueOffered");
       cvMap.put("Drop Contact Person","dropLocContactPerson");
       cvMap.put("IMEI number","identificationNo");
       cvMap.put("Ticket Closer Date","ticketCloserDate");
       cvMap.put("Drop Location City","");






    }
    public LinkedHashMap<String,String> getHeader(){
        return cvMap;
    }
}
