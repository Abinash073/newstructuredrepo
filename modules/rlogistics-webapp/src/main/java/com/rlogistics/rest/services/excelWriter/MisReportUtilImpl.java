package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.customqueries.MisResult;
import org.apache.poi.ss.usermodel.Row;

import java.io.IOException;
import java.util.List;
/**
 * Implementation of MIS report
 * @author Adarsh
 */
public class MisReportUtilImpl extends ReportExcelUtilImpl {

    @Override
    public ReportExcelUtil insertData(List<?> data, String type) throws IOException {
        try {
            if (type.equals("mis")) {

                //TRY to generify this method more :(

                ((List<MisResult>) data).stream()
                        .forEach(this::insertIncell);
            }
        } catch (Exception e) {
            wb.write(outputStream);
        }
        return this;
    }

    private void insertIncell(MisResult data) {
        int colNo = 0;

        //New Row
        Row row = sheet.createRow(rownum++);

        //Create S. No cell
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Ticket No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getTicketNo());

        //City
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getCity());

        //ProcessId
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getProcessId());

        //Retailer
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRetailer());

        //StartDate
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getStartDate());

        //EndDate
        cell = row.createCell(colNo);
        cell.setCellValue(data.getEndDate());
    }

}
