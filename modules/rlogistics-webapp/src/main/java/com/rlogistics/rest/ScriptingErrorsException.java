package com.rlogistics.rest;

import com.rlogistics.scripting.ErrorReport;

public class ScriptingErrorsException extends RuntimeException {
	private ErrorReport errorReport;

	public ScriptingErrorsException(ErrorReport errorReport) {
		this.errorReport = errorReport;
	}

	public ErrorReport getErrorReport() {
		return errorReport;
	}

	public void setErrorReport(ErrorReport errorReport) {
		this.errorReport = errorReport;
	}
}
