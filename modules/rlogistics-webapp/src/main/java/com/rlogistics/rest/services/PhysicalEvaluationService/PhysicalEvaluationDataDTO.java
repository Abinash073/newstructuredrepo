package com.rlogistics.rest.services.PhysicalEvaluationService;

import lombok.Data;

@Data
public class PhysicalEvaluationDataDTO {

    private String question;

    private String answer;
}
