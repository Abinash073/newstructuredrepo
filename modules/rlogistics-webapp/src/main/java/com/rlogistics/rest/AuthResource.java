package com.rlogistics.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.DeviceBrandInfoResult;
import com.rlogistics.customqueries.DeviceDetailsCustomQueriesMapper;
import com.rlogistics.customqueries.DeviceInfoResult;
import com.rlogistics.customqueries.DeviceVersionInfoResult;
import com.rlogistics.http.AuthUtil;
import com.rlogistics.http.RLogisticsRestAuthFilter;
import com.rlogistics.master.identity.*;
import com.rlogistics.master.identity.DeviceInfo.DeviceInfoQuery;
import com.rlogistics.master.identity.ProcessUser.ProcessUserQuery;
import com.rlogistics.master.identity.impl.cmd.DeleteDeviceInfoCmd;
import com.rlogistics.master.identity.impl.persistence.entity.DeviceDetailsEntity;
import com.rlogistics.ui.login.DefaultRLogisticsLoginHandler;
import com.rlogistics.ui.login.UIUser;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.MetaUser;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class AuthResource {
	private static final int MOBILEVERSION = 32;

	private static Logger log = LoggerFactory.getLogger(AuthResource.class);

	@Autowired
	protected IdentityService identityService;

	@RequestMapping(value = "/auth/metalogin", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = "application/json")
	public MetaUserData loginMetaUser(@RequestParam(required = true, value = "id") String id,
			@RequestParam(required = true, value = "password") String password) {
		MetaUserData ssoData = new MetaUserData();

		ssoData.setSuccess(false);

		MetaUser user = identityService.createUserQuery().userId(id).singleResult();

		if (user == null)
			return ssoData;

		if (!user.getPassword().equals(password))
			return ssoData;

		ssoData.setId(id);
		ssoData.setSuccess(true);

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, 30);
		String rawToken = id + "-" + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
		ssoData.setToken(rawToken + "-" + AuthUtil.generateHMAC(rawToken));

		return ssoData;
	}

	@RequestMapping(value = "/auth/login", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = "application/json")
	public RestResponse loginProcessUser(@RequestParam(required = true, value = "id") String id,
			@RequestParam(required = true, value = "password") String password,
			@RequestParam(required = false, value = "validity", defaultValue = "30") String validity,
			@RequestParam(required = false, value = "deviceId") String deviceId, HttpServletResponse response) {

		RestResponse restResponse = new RestResponse();
		ProcessUserData ssoData = new ProcessUserData();

		System.out.println("1111111111111");
		ssoData.setSuccess(false);

		try {
			UIUser uiUser = new DefaultRLogisticsLoginHandler().authenticate(id, password, response);

			if (uiUser == null) {
				throw new Exception("Invalid/Inactive user");
			}

			if (deviceId != null) {
				if (!uiUser.getRoleCode().equalsIgnoreCase("delivery_boy")
						&& !uiUser.getRoleCode().equalsIgnoreCase("field_engineer")) {
					throw new Exception("Invalid/Inactive user");
				}
			}

			ssoData.setId(id);
			ssoData.setSuccess(true);
			ssoData.setAdmin(uiUser.isAdmin());
			ssoData.setRoleCode(uiUser.getRoleCode());
			ssoData.setLocationId(uiUser.getLocationId());
			ssoData.setRetailerId(uiUser.getRetailerId());
			ssoData.setPhone(uiUser.getNumber());

			ProcessLocation processLocation = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getMasterdataService().createProcessLocationQuery().id(uiUser.getLocationId()).singleResult();

			if (processLocation != null) {
				ssoData.setLocationName(processLocation.getName());
			}

			if (uiUser.getRetailerId() != null) {
				Retailer retailer = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getMasterdataService().createRetailerQuery().id(uiUser.getRetailerId()).singleResult();
				if (retailer != null) {
					ssoData.setRetailerName(retailer.getName());
				}
			}

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, Integer.parseInt(validity));
			String rawToken = id + "___" + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			ssoData.setToken(rawToken + "___" + AuthUtil.generateHMAC(rawToken));

			restResponse.setResponse(ssoData);
			restResponse.setMessage("Login successful");

			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			ProcessUserQuery processUserQuery = masterdataService.createProcessUserQuery();
			ProcessUser processUser = processUserQuery.email(id).singleResult();
			String userId = processUser.getEmail();

			// Store the deviceId if present for sending push notification
			if (deviceId != null) {

				DeviceInfoQuery deviceInfoQuery = masterdataService.createDeviceInfoQuery();
				int count = (int) deviceInfoQuery.count();
				log.debug("Total Count Of Login device" + count);
				List<DeviceInfo> deviceInfo = deviceInfoQuery.userId(userId).list();
				if (count > 1) {
					for (int i = 0; i < deviceInfo.size(); i++) {
						CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines
								.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
						// log.debug("first id" +
						// deviceInfo.get(0).getId().toString());
						commandExecutor.execute(new DeleteDeviceInfoCmd(deviceInfo.get(i).getId()));
						// log.debug("Id" + (int)
						// masterdataService.createDeviceInfoQuery().count());
					}
					// DeviceInfoQuery
					// deviceInfoQuery1=masterdataService.createDeviceInfoQuery();
					DeviceInfo deviceInfoInsert = masterdataService.newDeviceInfo();
					String uuid = String.valueOf(UUID.randomUUID());
					deviceInfoInsert.setId(uuid);
					log.debug("uuid" + " " + uuid);
					deviceInfoInsert.setDeviceId(deviceId);
					deviceInfoInsert.setUserId(userId);

					try {
						masterdataService.saveDeviceInfo(deviceInfoInsert);
					} catch (Exception ex) {
						log.debug("Failed To Create Device Id");
						ex.printStackTrace();
					}
				} else {
					// DeviceInfoQuery
					// deviceInfoQuery1=masterdataService.createDeviceInfoQuery();
					DeviceInfo deviceInfoInsert = masterdataService.newDeviceInfo();

					// deviceInfoInsert.setId(String.valueOf(UUID.randomUUID()));
					deviceInfoInsert.setDeviceId(deviceId);
					deviceInfoInsert.setUserId(userId);

					try {
						masterdataService.saveDeviceInfo(deviceInfoInsert);
					} catch (Exception ex) {
						log.debug("Failed To Update");
						ex.printStackTrace();
					}

				}

				// deviceInfo.getId()
				/*
				 * if (deviceInfo == null && count < 1) {
				 *
				 * deviceInfo = masterdataService.newDeviceInfo();
				 *
				 * deviceInfo.setId(String.valueOf(UUID.randomUUID()));
				 * deviceInfo.setDeviceId(deviceId);
				 * deviceInfo.setUserId(userId);
				 *
				 * try { masterdataService.saveDeviceInfo(deviceInfo); } catch
				 * (Exception ex) {
				 *
				 * } } else{ deviceInfo.setDeviceId(deviceId);
				 * deviceInfo.setUserId(userId); try {
				 * masterdataService.saveDeviceInfo(deviceInfo); } catch
				 * (Exception ex) {
				 *
				 * }
				 *
				 * }
				 */
				// DeviceInfo deviceInfo =
				// deviceInfoQuery.deviceId(deviceId).userId(userId).singleResult();
				// if(deviceInfo==null) {
				// deviceInfo = masterdataService.newDeviceInfo();
				//
				// deviceInfo.setId(String.valueOf(UUID.randomUUID()));
				// deviceInfo.setDeviceId(deviceId);
				// deviceInfo.setUserId(userId);
				//
				// try {
				// masterdataService.saveDeviceInfo(deviceInfo);
				// } catch (Exception ex) {
				//
				// }
				// }
			} else {
				DeviceInfo deviceInfoInsert = masterdataService.newDeviceInfo();
				String uuid = String.valueOf(UUID.randomUUID());
				deviceInfoInsert.setId(uuid);
				log.debug("uuid" + " " + uuid);
				deviceInfoInsert.setDeviceId(deviceId);
				deviceInfoInsert.setUserId(userId);

				try {
					masterdataService.saveDeviceInfo(deviceInfoInsert);
				} catch (Exception ex) {
					log.debug("Failed To Create Device Id For First Time Log In");
					ex.printStackTrace();
				}
			}

		} catch (Exception e) {
			log.error("Exception during login:" + e.getMessage(), e);
			restResponse.setMessage(e.getMessage());
			restResponse.setSuccess(false);
		}
		return restResponse;
	}

	@RequestMapping(value = "/auth/retailer/login", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = "application/json")
	public RestExternalResult loginOutsideRetailerUser(@RequestParam(required = true, value = "id") String id,
			@RequestParam(required = true, value = "password") String password,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(required = false, value = "validity", defaultValue = "30") String validity,
			HttpServletResponse response) {

		RestExternalResult restResponse = new RestExternalResult();
		ProcessUserDataRet ssoData = new ProcessUserDataRet();
		String apiToken = "";
		ssoData.setSuccess(false);

		try {
			UIUser uiUser = new DefaultRLogisticsLoginHandler().authenticate(id, password, response);

			if (uiUser == null) {
				throw new Exception("Invalid/Inactive user");
			}

			ssoData.setId(id);
			ssoData.setSuccess(true);
			ssoData.setAdmin(uiUser.isAdmin());
			ssoData.setRoleCode(uiUser.getRoleCode());
			ssoData.setLocationId(uiUser.getLocationId());
			ssoData.setRetailerId(uiUser.getRetailerId());

			ProcessLocation processLocation = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getMasterdataService().createProcessLocationQuery().id(uiUser.getLocationId()).singleResult();

			if (processLocation != null) {
				ssoData.setLocationName(processLocation.getName());
			}

			if (uiUser.getRetailerId() != null) {
				Retailer retailer = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getMasterdataService().createRetailerQuery().id(uiUser.getRetailerId()).singleResult();
				if (retailer != null) {
					ssoData.setRetailerName(retailer.getName());
					ssoData.setRetailerCode(retailer.getCode());

					// generate API Token
					Calendar c = Calendar.getInstance();
					c.setTime(new Date());
					c.add(Calendar.HOUR, 1);
					if (retailer.getApiToken().equals("")) {
						if (retailer.getApiUsername() != null) {
							String rawToken = retailer.getApiUsername() + "___"
									+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
							apiToken = AuthUtil.cryptWithMD5(rawToken);
							retailer.setApiToken(apiToken);
							// retailer.setTokenExpiryDate(new
							// Timestamp(c.getTime().getTime()));
						} else {
							String rawToken = RandomStringUtils.randomAlphanumeric(8).toLowerCase()
									+ new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
							apiToken = AuthUtil.cryptWithMD5(rawToken);
							retailer.setApiToken(apiToken);
							// retailer.setTokenExpiryDate(new
							// Timestamp(c.getTime().getTime()));
						}
					} else {
						retailer.setApiToken(retailer.getApiToken());
						apiToken = retailer.getApiToken();

					}
					try {
						((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
								.getMasterdataService().saveRetailer(retailer);
					} catch (Exception e) {

					}

				}
			}

			restResponse.setApiToken(apiToken);

			restResponse.setResponse(ssoData);
			restResponse.setMessage("Login successful.");

		} catch (Exception e) {
			log.error("Exception during login:" + e.getMessage(), e);

			restResponse.setMessage(e.getMessage());
			restResponse.setSuccess(false);
		}
//		saveToConsoleLogsTable("RETAILERLOGIN",
//				"RetailerId:" + id + " Password:" + password + " Response: " + restResponse.getMessage());

		return restResponse;
	}

	@RequestMapping(value = "/auth/refresh-token", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = "application/json")
	public ProcessUserData refreshToken(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		try {
			if (!RLogisticsRestAuthFilter.processRltoken(httpRequest)) {
				httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}

			if (Authentication.getAuthenticatedUserId() == null) {
				throw new RuntimeException("counts can be invoked only by a loggged in user");
			}

			ProcessUserData ssoData = new ProcessUserData();

			UIUser user = new DefaultRLogisticsLoginHandler().lookup(Authentication.getAuthenticatedUserId());

			ssoData.setId(user.getEmail());
			ssoData.setSuccess(true);
			ssoData.setAdmin(user.isAdmin());
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, 30);
			String rawToken = user.getEmail() + "___" + new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime());
			ssoData.setToken(rawToken + "___" + AuthUtil.generateHMAC(rawToken));

			return ssoData;
		} catch (Exception ex) {
			log.error("Exception during refreshToken", ex);
			return null;
		}
	}

	/**
	 * API to store user device info
	 *
	 * @Adarsh : 16_oct_2018
	 */
	@Autowired
	ObjectMapper mapper;
	@Autowired
	MasterdataService masterdataService;

	/**
	 * @param deviceInfo
	 *            : Json Object Sent From Android
	 * @param id
	 *            : userId
	 */
	@RequestMapping(value = "/save/device-info", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	private RestResponse saveDeviceData(@Param("deviceInfo") String deviceInfo, @Param("id") String id) {
		RestResponse restResponse = new RestResponse();
		try {

			Map<String, Object> info = mapper.readValue(deviceInfo, Map.class);

			int countOfRecords = (int) masterdataService.createDeviceDetailsQuery().imei1(info.get("imei1").toString())
					.imei2(info.get("imei2").toString()).osName(info.get("osName").toString())
					.version(info.get("version").toString()).versionRelease(info.get("versionRelease").toString())
					.userId(id).rlappVersion(String.valueOf(info.get("rlappVersion"))).count();

			if (countOfRecords == 0) {
				DeviceDetailsEntity deviceDetailsEntity = mapper
						.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
						.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
						.readValue(deviceInfo, DeviceDetailsEntity.class);
				deviceDetailsEntity.setUserId(id);
				deviceDetailsEntity.setId(UUID.randomUUID().toString());
				masterdataService.saveDeviceDetails(deviceDetailsEntity);
				restResponse.setSuccess(true);
				restResponse.setMessage("Stored");
				restResponse.setResponse("Inserted In DB");

			} else {
				restResponse.setSuccess(true);
				restResponse.setMessage("Record Already Present");
			}

		} catch (Exception e) {
			log.debug("error  " + e);
			restResponse.setResponse("Failed" + e);
			restResponse.setMessage("Failed");
			restResponse.setSuccess(false);

		}
		return restResponse;
	}

	/**
	 * @param groupBy
	 *            = on which we want to group by and view
	 * @param first
	 * @param last
	 * @return
	 * @Author : Adarsh
	 */
	@RequestMapping(value = "/get/device-info", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public RestResponse getDeviceData(@RequestParam(value = "groupBy") String groupBy,
			@RequestParam(value = "first", required = false) String first,
			@RequestParam(value = "last", required = false) String last) {
		ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) ProcessEngines
				.getDefaultProcessEngine().getProcessEngineConfiguration();
		SqlSession sqlSession = processEngineConfiguration.getSqlSessionFactory().openSession();
		RestResponse restResponse = new RestResponse();
		try {
			DeviceDetailsCustomQueriesMapper mapper = sqlSession.getMapper(DeviceDetailsCustomQueriesMapper.class);

			if (groupBy.equalsIgnoreCase("version")) {
				log.debug(groupBy);
				List<DeviceInfoResult> detailsByVersion = mapper.getDetailsByVersion();
				restResponse.setMessage("fetched");
				restResponse.setSuccess(true);
				restResponse.setResponse(detailsByVersion);
			} else if (groupBy.equals("device")) {
				List<DeviceBrandInfoResult> detailsBybrand = mapper.getDetailsByBrand();
				restResponse.setMessage("fetched");
				restResponse.setSuccess(true);
				restResponse.setResponse(detailsBybrand);
			} else if (groupBy.equals("appVersion")) {
				List<DeviceVersionInfoResult> detailsByAppVersion = mapper.getDetailsByAppVersion();
				restResponse.setMessage("fetched");
				restResponse.setSuccess(true);
				restResponse.setResponse(detailsByAppVersion);
			}

		} catch (Exception e) {
			restResponse.setSuccess(false);
			restResponse.setResponse("Error" + e);
		} finally {
			sqlSession.close();
			return restResponse;
		}
	}

	private void saveToConsoleLogsTable(String string1, String string) {
		try {
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			ConsoleLogs consoleLogs = masterdataService.newConsoleLogs();
			UUID id = UUID.randomUUID();
			consoleLogs.setId(String.valueOf(id));
			consoleLogs.setProcInstId(string1);
			consoleLogs.setData(string);

			masterdataService.saveConsoleLogs(consoleLogs);
		} catch (Exception e) {
			log.debug("NOT SSAVE IN CONSOLE LOG TABLE: " + e);
		}

	}

	// It will check the version at before login
	@RequestMapping(value = "/auth/mobile-version-check", method =  RequestMethod.POST , produces = "application/json")
	public RestResponse checkMobileVersion(@RequestBody String reqBody) {
		RestResponse restResponse = new RestResponse();
		try {
			JSONObject jsonObject = new JSONObject(reqBody);
			int verison = jsonObject.getInt("version");
			DeviceDetails deviceDetails = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getMasterdataService().createDeviceDetailsQuery().userId("LATESTMOBILEVERSION").singleResult();
			int MOBILEVERSION = Integer.parseInt(deviceDetails.getRlappVersion());
			if (verison == MOBILEVERSION) {
				restResponse.setMessage("");
				restResponse.setSuccess(true);
			} else {
				restResponse.setMessage("Please Update From Google Play Store.");
				restResponse.setSuccess(false);
			}
		} catch (Exception e) {
			restResponse.setMessage("LATESTMOBILEVERSION is not mention on DeviceDetails table" + e);
			restResponse.setSuccess(false);
		}
		return restResponse;
	}

}
