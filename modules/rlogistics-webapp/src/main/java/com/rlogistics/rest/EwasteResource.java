package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mxgraph.util.svg.ParseException;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.BagTypeList;
import com.rlogistics.master.identity.EwasteMisBulkTransfer;
import com.rlogistics.master.identity.MisBulkTransfer;
import com.rlogistics.master.identity.MisBulkTransfer.MisBulkTransferQuery;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.master.identity.PackagingMaterialInventory.PackagingMaterialInventoryQuery;
import com.rlogistics.master.identity.PackagingMaterialMaster;
import com.rlogistics.master.identity.RLogisticsAttachment;
import com.rlogistics.master.identity.RLogisticsAttachment.RLogisticsAttachmentQuery;
import com.rlogistics.master.identity.BagTypeList.BagTypeListQuery;
import com.rlogistics.master.identity.EwasteBagQuantity;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.ProductResource.AdditionalFileCommandProcessorExcel;
import com.rlogistics.util.ValidateUtil;

@RestController
public class EwasteResource extends RLogisticsResource {
	private static Logger log = LoggerFactory.getLogger(EwasteResource.class);
	MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
			.getMasterdataService();

	@RequestMapping(value = "/ewaste-additional-file/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult ewasteFileUploaded(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "processId") String processId) {
		try {
//			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
//				return null;
//			}
			return ewasteFileUpload(file.getInputStream(), file, processId);
		} catch (IOException ex) {
		    log.debug("error"+ex);
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	@RequestMapping(value = "/bits/barcode/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult bitsBarcodeUpload(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "file") MultipartFile file,
			@RequestParam(value = "processId") String processId) {
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			return bitsBarcodeUpload(file.getInputStream(), file, processId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	public UploadProcessCommandsResult bitsBarcodeUpload(InputStream is, MultipartFile file, String processId) {

//		ArrayList<String> barCodeList = new ArrayList<String>();
		String startingBarcode = "";
		String endingBarcode = "";
		ArrayList<Map<String, String>> fieldValuesList = new ArrayList<Map<String, String>>();

		UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
		
		try {
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getRuntimeService();
			BufferedReader bis = new BufferedReader(new InputStreamReader(is));

			String fileName = file.getName();
			Workbook workbook = null;

			try {
				workbook = WorkbookFactory.create(is);
			} catch (Exception e) {

			}
			Cell cell = null;
			Sheet sheet;
			int totalRowCount = 0;

			String record = "";
			int recordNo = 0;
			int insertCount = 0;
			List<String> orderedFields = new ArrayList<String>();
			DataFormatter dfmt = new DataFormatter();
			
			Map<String, String> barcodeMap = new HashMap<String, String>();
			Map<String, String> boxMap = new HashMap<String, String>();
			MisBulkTransferQuery misBulkTransferQuery = masterdataService.createMisBulkTransferQuery();
			MisBulkTransferQuery misBulkTransferQuery2 = masterdataService.createMisBulkTransferQuery();
			List<MisBulkTransfer> misBulkTransferList = misBulkTransferQuery.actionType("bitsBox").processId(processId).list();
			for(MisBulkTransfer misBulkTransfer : misBulkTransferList) {
				String box = misBulkTransfer.getBoxNumber();
				if(box != null) {
					String [] boxes = box.split(",");
					for(String boxBarcode : boxes) {
						barcodeMap.put(boxBarcode, processId);
					}
					boxMap.put(box, misBulkTransfer.getId());
				}
			}
			
			ArrayList<String> usedBarcodes = new ArrayList<String>();
			
			for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

				sheet = workbook.getSheetAt(j);
				totalRowCount = sheet.getLastRowNum();
				Row firstRow = sheet.getRow(0);
				recordNo = 1;

				/* Header record */
				for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
					orderedFields.add(firstRow.getCell(fr).getStringCellValue());
				}
				if (log.isDebugEnabled()) {
					log.debug("HEADER:" + recordNo + ":" + record);
					for (String f : orderedFields) {
						log.debug("------------" + f);
					}
				}
				log.debug("TotalRows : " + totalRowCount);
				System.out.println("TotalRows : " + totalRowCount);
				for (int r = 1; r <= totalRowCount; r++) {
					System.out.println("inside loop");
					recordNo++;

					if (sheet.getRow(r) != null) {

						Row row = sheet.getRow(r);
						Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
						for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
							if (row.getCell(i) != null) {
								cell = row.getCell(i);
								fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell).trim());
								System.out.println("added " + orderedFields.get(i));
								System.out.println("Value " + dfmt.formatCellValue(cell).trim());
								if (!record.equals("")) {
									record += "," + dfmt.formatCellValue(cell);
									System.out.println("inside If");
								} else {
									record += dfmt.formatCellValue(cell);
									System.out.println("inside else");
								}
							}
						}

						Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);
						String barocdeValue = fieldValues.get("barcode");
						String barcode = "";
						
						if (barocdeValue.length() == 12) {
							barcode = barocdeValue.substring(1, 11);
						} else if (barocdeValue.length() == 11) {
							barcode = barocdeValue.substring(0, 10);
							if (!barcodeMap.containsKey(barcode)) {
								barcode = barocdeValue.substring(1);
							}
						} else {
							barcode = barocdeValue;
						}
							//check against inventory table to find out the startingBarcode
	//						PackagingMaterialInventory packagingMaterialInventory = masterdataService.createPackagingMaterialInventoryQuery().barcode(barcode).singleResult();
							
							//Check for Box Number
//						List<MisBulkTransfer> boxNumber = misBulkTransferQuery.boxNumber(barcode).list();
//						if(boxNumber.equals(null) || boxNumber.equals("")){}
						
							//if barcode exists
							if(barcodeMap.containsKey(barcode)) {
								if (startingBarcode.equals("")) {
									System.out.println("Found startingBarcode " + startingBarcode);
									startingBarcode = barcode;
									barcodeMap.remove(barcode);
									usedBarcodes.add(startingBarcode);
								} else {
									System.out.println("Found endingBarcode " + endingBarcode);
									endingBarcode = barcode;
									barcodeMap.remove(barcode);
									usedBarcodes.add(endingBarcode);
									
									//Mine Code for not inserting duplicate
									
									String checkBoxNumber = startingBarcode + "," + endingBarcode;
									List<MisBulkTransfer> checkMisBulkTransferList = misBulkTransferQuery2.subTicketNo(boxMap.get(checkBoxNumber)).list();
									log.debug("checkMisBulkTransferList value is : "+checkMisBulkTransferList);
									log.debug("checkBoxNumber value is : "+boxMap.get(checkBoxNumber));
									if(checkMisBulkTransferList.size()<=0){
									//add all the UPC barcodes to MIS table
									for (Map<String, String> singleFieldValues : fieldValuesList) {
										MisBulkTransfer misBulkTransferadd = masterdataService.newMisBulkTransfer();
										
										misBulkTransferadd.setId(String.valueOf(UUID.randomUUID()));
										misBulkTransferadd.setActionType("bitsBarcode");
										misBulkTransferadd.setProcessId(processId);
										misBulkTransferadd.setProductCategory("productCategory");
										misBulkTransferadd.setBarcode(singleFieldValues.get("barcode"));
										misBulkTransferadd.setBoxNumber(startingBarcode + "," + endingBarcode);
										misBulkTransferadd.setValue("1");
										misBulkTransferadd.setStatus("open");
										if(singleFieldValues.containsKey("status")) {
											misBulkTransferadd.setItemQuality(singleFieldValues.get("status"));
										}
										
										String boxNumber = startingBarcode + "," + endingBarcode;
										if(boxMap.containsKey(boxNumber)) {
											misBulkTransferadd.setSubTicketNo(boxMap.get(boxNumber));
										}
										
										try {
											masterdataService.saveMisBulkTransfer(misBulkTransferadd);
											insertCount++;
										} catch (Exception e) {
											
										}
									}}
									startingBarcode = "";
									endingBarcode = "";
									fieldValuesList = new ArrayList<Map<String, String>>();
								}
							} 
							//add the UPC codes as individual barcodes to a list
							else {
								fieldValuesList.add(fieldValues);
							}
						
						
						
					} else {
						ErrorObject errorObject = new ErrorObject();
						errorObject.setRowNo(recordNo);
						errorObject.setMessage("Ignoring line " + recordNo);
						errorObject.setReason("Empty Records");
						retval.getErrorMessages().add(errorObject);
						System.out.println("getErrorMessages().add(errorObject)"+retval.getErrorMessages().add(errorObject));
					}

				}
			}
			
			retval.setNoOfRecordsInserted(insertCount);
			
			//change the used barcode status to issued
			try {
				changeBarcodeStatus(usedBarcodes);
				//Here is 2 Method for running this code in backgroung.use it in future
//				ValidateUtil.changeBarcodeStatus(usedBarcodes);
	//			ExecutorService service = Executors.newFixedThreadPool(4);
//				Runnable r = new Runnable() {
//					
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						ValidateUtil.changeBarcodeStatus(usedBarcodes);
//						
//					}
//				};
				
				/*service.submit(new Runnable() {
					public void run() {
						ValidateUtil.changeBarcodeStatus(usedBarcodes);
						Thread.currentThread() == Looper.getMainLooper().getThread()
						}
					});*/
			} catch (Exception p) {
				
			}
			
		} catch (Exception ex) {
			ErrorObject errorObject = new ErrorObject();
			errorObject.setMessage("Ignoring file upload");
			errorObject.setReason("Failed to read uploaded content");
			retval.getErrorMessages().add(errorObject);
			System.out.println("2retval.getErrorMessages().add(errorObject)"+errorObject);
		}
		
		return retval;
	}

	public UploadProcessCommandsResult ewasteFileUpload(InputStream is, MultipartFile file, String processId) {

//		ArrayList<String> barCodeList = new ArrayList<String>();
		int kg25 = 0;
		int kg10 = 0;
		int kg5 = 0;
		int kg2 = 0;

		UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
		try {
			RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getRuntimeService();
			BufferedReader bis = new BufferedReader(new InputStreamReader(is));

			String fileName = file.getName();
			Workbook workbook = null;

			try {
				workbook = WorkbookFactory.create(is);
			} catch (Exception e) {

			}
			Cell cell = null;
			Sheet sheet;
			int totalRowCount = 0;

			String record = "";
			int recordNo = 0;
			int insertCount = 0;
			List<String> orderedFields = new ArrayList<String>();
			DataFormatter dfmt = new DataFormatter();
			BagTypeListQuery bagTypeListQuery = masterdataService.createBagTypeListQuery();
			List<BagTypeList> bagTypeLists = bagTypeListQuery.orderByBagType().desc().list();
			
            Map<String,Integer> noOfBags = new HashMap<String,Integer>(); 
            for(BagTypeList bagTypeList : bagTypeLists) {
            	noOfBags.put(String.valueOf(bagTypeList.getBagType()), 0);
            }
            
			for (int j = 0; j < workbook.getNumberOfSheets(); j++) {

				sheet = workbook.getSheetAt(j);
				totalRowCount = sheet.getLastRowNum();
				Row firstRow = sheet.getRow(0);
				recordNo = 1;

				/* Header record */
				for (int fr = firstRow.getFirstCellNum(); fr < firstRow.getLastCellNum(); fr++) {
					orderedFields.add(firstRow.getCell(fr).getStringCellValue());
				}
				if (log.isDebugEnabled()) {
					log.debug("HEADER:" + recordNo + ":" + record);
					for (String f : orderedFields) {
						log.debug("------------" + f);
					}
				}
				log.debug("TotalRows : " + totalRowCount);
				System.out.println("TotalRows : " + totalRowCount);
				for (int r = 1; r <= totalRowCount; r++) {
					System.out.println("inside loop");
					recordNo++;

					if (sheet.getRow(r) != null) {

						Row row = sheet.getRow(r);
						Map<String, String> fieldValuesPreFilter = new HashMap<String, String>();
						for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
							if (row.getCell(i) != null) {
								cell = row.getCell(i);
								fieldValuesPreFilter.put(orderedFields.get(i), dfmt.formatCellValue(cell).trim());
								System.out.println("added " + orderedFields.get(i));
								System.out.println("Value " + dfmt.formatCellValue(cell).trim());
								if (!record.equals("")) {
									record += "," + dfmt.formatCellValue(cell);
									System.out.println("inside If");
								} else {
									record += dfmt.formatCellValue(cell);
									System.out.println("inside else");
								}
							}
						}

						Map<String, String> fieldValues = filterFieldValues(fieldValuesPreFilter);

						// populate MIS table
						EwasteMisBulkTransfer ewasteMisBulkTransfer = masterdataService.newEwasteMisBulkTransfer();

						ewasteMisBulkTransfer.setProcessId(processId);

						Map<String, Object> variables = runtimeService.getVariables(processId);

						int isValid = 1;

						ewasteMisBulkTransfer.setProductCategory(fieldValues.get("productCategory"));
						ewasteMisBulkTransfer.setStatus("open");
						ewasteMisBulkTransfer.setTicketNo(String.valueOf(variables.get("rlTicketNo")));

						if (variables.containsKey("rlRetailerId")) {
							ewasteMisBulkTransfer.setRetailerId(String.valueOf(variables.get("rlRetailerId")));
//							misBulkTransfer.setRetailer(String.valueOf(variables.get("retailer")));
						}

						if (fieldValues.containsKey("estimatedWeight") && fieldValues.get("estimatedWeight") != null
								&& !fieldValues.get("estimatedWeight").equals("")) {
							ewasteMisBulkTransfer.setQuantity(fieldValues.get("estimatedWeight"));
						}
						if(fieldValues.containsKey("barcode") && fieldValues.get("barcode")!=null && !fieldValues.get("barcode").isEmpty() && fieldValues.get("barcode").equalsIgnoreCase("")){
							ewasteMisBulkTransfer.setBarcode(fieldValues.get("barcode"));
						}
						
						//Calculate no of bags to be used
						int givenQuantity = Integer.valueOf(fieldValues.get("estimatedWeight"));
						boolean flag = true;
						for(BagTypeList bagTypeList : bagTypeLists) {
							int bagType = bagTypeList.getBagType(); //25kg, 10kg, 5kg, 2kg
							if(givenQuantity>=bagType){
								int noOfBagTypes = 0;  //estimatedWeight
								noOfBagTypes = givenQuantity/bagType;
								
								//Cal quotient to get value with decimal.
								double remainingQuantitiy = (double)givenQuantity/bagType;
								
								//Cal after decimal value to check with thrashhold
								double decimalValue1 = remainingQuantitiy - Math.floor(remainingQuantitiy);
								double decimalValue = Math.round(decimalValue1 * 100.0) / 100.0;
								if(decimalValue==0) {
									flag = false;
								} else if(remainingQuantitiy > 0 && decimalValue > 0.6) {
									noOfBagTypes++;
									givenQuantity =0;
									flag=false;
								}else if(remainingQuantitiy > 0 && decimalValue <= 0.6) {
									//Here need to Cal actual remainder to process further
									 givenQuantity = givenQuantity%bagType;
                                }	
								
								//Setting the values to MAP.
								noOfBagTypes += noOfBags.get(String.valueOf(bagType));
								noOfBags.put(String.valueOf(bagType), noOfBagTypes);
								if(!flag) {
									break;
								}
							}
						}
						
						if(flag){
							BagTypeList bagTypeList = bagTypeLists.get(bagTypeLists.size()-1);
							System.out.println("Smallest unit " + bagTypeLists.get(bagTypeLists.size()-1).getBagType());
							int bagType = bagTypeList.getBagType();
							int noOfBagTypes = noOfBags.get(String.valueOf(bagType)) + 1;
							noOfBags.put(String.valueOf(bagType), noOfBagTypes);
						}
						
						
						if (fieldValues.containsKey("grade") && fieldValues.get("grade") != null
								&& !fieldValues.get("grade").equals("")) {
							ewasteMisBulkTransfer.setGrade(fieldValues.get("grade"));
						}
						try {
							System.out.println("Before inserting");
							masterdataService.saveEwasteMisBulkTransfer(ewasteMisBulkTransfer);
							insertCount++;
						} catch (Exception m) {
							log.debug("Error"+m);
							ErrorObject errorObject = new ErrorObject();
							errorObject.setRowNo(recordNo);
							errorObject.setMessage("Ignoring line " + recordNo);
							errorObject.setReason("Duplicate Record");
							retval.getErrorMessages().add(errorObject);
						}
					} else {
						ErrorObject errorObject = new ErrorObject();
						errorObject.setRowNo(recordNo);
						errorObject.setMessage("Ignoring line " + recordNo);
						errorObject.setReason("Empty Records");
						retval.getErrorMessages().add(errorObject);
						System.out.println("getErrorMessages().add(errorObject)"+retval.getErrorMessages().add(errorObject));
					}

				}
			}
			//Iterating Map
			
			
//			if(ewasteBagQuantity1!=null)
//			{System.out.println("IF ewasteBagQuantity1"+ewasteBagQuantity1);}
//			else{System.out.println("else else ewasteBagQuantity1"+ewasteBagQuantity1);}
//			}catch(Exception e){System.out.println(e.getMessage());}
			for (Entry<String, Integer> entry : noOfBags.entrySet()) {
				
			    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			    log.debug("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//			    EwasteBagQuantity ewasteBagQuantity = masterdataService.newEwasteBagQuantity();
			    
			    EwasteBagQuantity ewasteBagQuantity = masterdataService.createEwasteBagQuantityQuery().processId(processId).bagType(entry.getKey()).singleResult();
			    if(ewasteBagQuantity!=null){
			    	ewasteBagQuantity.setQuantity(ewasteBagQuantity.getQuantity()+entry.getValue());
   			    } else {
   			    	ewasteBagQuantity = masterdataService.newEwasteBagQuantity();
   			    	ewasteBagQuantity.setProcessId(processId);
   				    ewasteBagQuantity.setBagType(entry.getKey());
   				    ewasteBagQuantity.setQuantity(entry.getValue());
   			    }
			    
			    try {
			    	masterdataService.saveEwasteBagQuantity(ewasteBagQuantity);
			    } catch (Exception m) {
			    	
			    }
			    
				
			}
			retval.setNoOfRecordsInserted(insertCount);
		} catch (Exception ex) {
			ErrorObject errorObject = new ErrorObject();
			errorObject.setMessage("Ignoring file upload");
			errorObject.setReason("Failed to read uploaded content");
			retval.getErrorMessages().add(errorObject);
			System.out.println("2retval.getErrorMessages().add(errorObject)"+errorObject);
		}
		
		return retval;
	}
		
	protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
		return fieldValues;
	}
	
	@RequestMapping(value = "/get/photos", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getMisPhotos(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam("query") String queryJson , @RequestParam("firstResult") String firstResult, @RequestParam("maxResults") String maxResults) {
		RestResponse restResponse = new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		try {
			if (!beforeMethodInvocation(httpRequest, httpResponse)) {
				return null;
			}
			
			List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
			RLogisticsAttachmentQuery rlAttachmentquery = masterdataService.createRLogisticsAttachmentQuery();
			List<RLogisticsAttachment> rlAttachment = null;
			Map<String,Object> attachmentObj = null;
			try {
				attachmentObj = new ObjectMapper().readValue(URLDecoder.decode(queryJson),Map.class);
				rlAttachment = rlAttachmentquery.key1(String.valueOf(attachmentObj.get("key1"))).key2(String.valueOf(attachmentObj.get("key2"))).list();
				
				for(RLogisticsAttachment attachment : rlAttachment) {
					//masterdataService.createTicketImagesQuery().procInstId(attachment.getKey2()).imageType(pathVariables.get("key3")).list()
					Map<String, String> resultObj = new HashMap<String, String>();
					resultObj.put("id", attachment.getId());
					resultObj.put("key1", attachment.getKey1());
					resultObj.put("key2", attachment.getKey2());
					resultObj.put("key3", attachment.getKey3());
					resultObj.put("key4", attachment.getKey4());
					resultObj.put("latitude", attachment.getLatitude());
					resultObj.put("longitude", attachment.getLongitude());
					resultObj.put("location", attachment.getLocation());
					
					resultList.add(resultObj);
					restResponse.setResponse(resultList);
				}
				
			} catch (Exception p) {
				
			}
			
			
		} catch (Exception ex) {
	
		}
		return restResponse;
	}
	
	
	//Look for an option to do it in background(creating Thread)
			private void changeBarcodeStatus(ArrayList<String> barCodeList2) {
				PackagingMaterialInventory pmi = null;
				try {
					for (int i = 0; i < barCodeList2.size(); i++) {
						pmi = masterdataService.createPackagingMaterialInventoryQuery().barcode(barCodeList2.get(i))
								.singleResult();
						if (pmi != null){
							pmi.setStatus(1);
							masterdataService.savePackagingMaterialInventory(pmi);}
					}
					
				} catch (Exception e) {
					log.debug("error while updating Packing Inventory" + e);
				}

			}
			
			@RequestMapping(value = "/issue/barcodes",produces = "application/json")
			public RestResponse issueBarcodeRest(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam("quantity") Long quantity, @RequestParam("processId") String processId) {
				RestResponse restResponse = new RestResponse();
				String response = "";
				try {
					response = issueBarcodes(processId, quantity);
				} catch (Exception ex) {
					log.error("Exception during sendEmail", ex);
					response = ex.getMessage();
				}
				restResponse.setResponse(response);
				return restResponse;
			}
			
			public static String issueBarcodes(String processId, Long quantity) {
				
				MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getMasterdataService();
				RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
						.getRuntimeService();
				String response = "";
				
				
				try {
					String locationId = String.valueOf(runtimeService.getVariable(processId, "rlLocationId"));
					String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));
					String assignee = String.valueOf(runtimeService.getVariable(processId, "rlFieldVisitAssignedTo"));
					
					PackagingMaterialInventoryQuery packagingMaterialInventoryQuery = masterdataService.createPackagingMaterialInventoryQuery();
					List<PackagingMaterialInventory> packagingMaterialInventoryList = packagingMaterialInventoryQuery.locationId(locationId).packagingTypeCode("99").status(0).orderByBarcode().asc().listPage(0, quantity.intValue());
					
					if(packagingMaterialInventoryList.size()==0) {
						
						
						Map<String, String> query = new HashMap<String, String>();
						query.put("locationId", locationId);
						query.put("packagingTypeCode", "99");
						query.put("itemCount", "100");
						
						try {
							PackagingMaterialResource.generateBarcodeMethod(query);
						} catch (Exception b) {
							
						}
						
						packagingMaterialInventoryList = packagingMaterialInventoryQuery.locationId(locationId).packagingTypeCode("99").status(0).orderByBarcode().asc().listPage(0, quantity.intValue());
					}
					
					for (PackagingMaterialInventory packagingMaterialInventory : packagingMaterialInventoryList) {
						packagingMaterialInventory.setStatus(1);
						packagingMaterialInventory.setTicketNumber(ticketNo);
						if(assignee != null) {
							packagingMaterialInventory.setUserId(assignee);
						}
						
						masterdataService.savePackagingMaterialInventory(packagingMaterialInventory);
						
						
						try {
							PackagingMaterialMaster packagingMaterialMaster = masterdataService.createPackagingMaterialMasterQuery().locationId(packagingMaterialInventory.getLocationId()).packagingTypeCode(packagingMaterialInventory.getPackagingTypeCode()).singleResult();
							if(packagingMaterialMaster != null && packagingMaterialMaster.getAvailableQuantity()>0) {
								packagingMaterialMaster.setAvailableQuantity(packagingMaterialMaster.getAvailableQuantity()-1);
								masterdataService.savePackagingMaterialMaster(packagingMaterialMaster);
							}
						
						} catch (Exception m) {
							
						}
						
					}
					
				} catch (Exception e) {
					log.debug("Exception while issuing barcode " + e.getMessage());
					throw e;
				}
				
				return quantity + " barcodes issued";
			}
}
