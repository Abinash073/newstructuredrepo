package com.rlogistics.rest.controllers.payment;

import com.rlogistics.customqueries.PaymentDetailsData;
import com.rlogistics.rest.RestResult;
import com.rlogistics.rest.services.PaymentDetailsExcelHeader;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import com.rlogistics.rest.services.excelWriter.SaveExcelInHostUtil;
import com.rlogistics.rest.services.paymentServices.PaymentDetails;
import com.rlogistics.rest.services.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller to get details for payment
 *
 * @author Adarsh
 */
@Slf4j
@RestController
@RequestMapping(value = "get/", produces = MediaType.APPLICATION_JSON_VALUE)
public class PaymentDetailsController {
    private PaymentDetails paymentDetails;
    private ReportExcelUtil excelUtil;
    private SaveExcelInHostUtil saveExcelInHostUtil;
    private DateUtil dateUtil;

    public PaymentDetailsController(@Qualifier("paymentDetailsRepository") PaymentDetails paymentDetails, @Qualifier("paymentReport") ReportExcelUtil excelUtil, SaveExcelInHostUtil saveExcelInHostUtil, DateUtil dateUtil) {
        this.paymentDetails = paymentDetails;
        this.excelUtil = excelUtil;
        this.saveExcelInHostUtil = saveExcelInHostUtil;
        this.dateUtil = dateUtil;
    }

    /**
     * Fetch the Payment details after processing all the filters
     *
     * @param startDate    startDate passed from Angular
     * @param endDate      End Date passed from Angular
     * @param retailerName Retailer Name Passed from Angular
     * @param ticketNo     Ticket no passed from angular
     * @return DTO containing Payment details to be displayed by Angular as view
     */
    @RequestMapping(
            value = "payment-details",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    private RestResult getPaymentDetails(@RequestParam("startDate") String startDate,
                                         @RequestParam("endDate") String endDate,
                                         @RequestParam(value = "retailerName", required = false, defaultValue = "") String retailerName,
                                         @RequestParam(value = "ticketNo", required = false, defaultValue = "") String ticketNo
    ) {
        RestResult restResult = new RestResult();
        restResult
                .setResponse(paymentDetails
                        .getPaymentDataByDate(
                                dateUtil.getDateByReducingDate(startDate, 1, "yyyy-MM-dd"),
                                dateUtil.getDateByAddingDate(endDate, 1, "yyyy-MM-dd"),
                                retailerName,
                                ticketNo)
                );
        restResult.setSuccess(true);
        return restResult;

    }

    /**
     * Crates a Excel file based on filters passed from Angular
     *
     * @param startDate    startDate passed from Angular
     * @param endDate      End Date passed from Angular
     * @param retailerName Retailer Name Passed from Angular
     * @param ticketNo     Ticket no passed from angular
     * @return DTO containing Excel url
     */
    @RequestMapping(value = "payment-details/excel",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult getPaymentDetailsExcel(@RequestParam("startDate") String startDate,
                                              @RequestParam("endDate") String endDate,
                                              @RequestParam(value = "retailerName", required = false, defaultValue = "") String retailerName,
                                              @RequestParam(value = "ticketNo", required = false, defaultValue = "") String ticketNo
    ) {
        RestResult restResult = new RestResult();
        try {
            PaymentDetailsExcelHeader header = new PaymentDetailsExcelHeader();
            List<PaymentDetailsData> paymentDetailsData = paymentDetails
                    .getPaymentDataByDate(
                            dateUtil.getDateByReducingDate(startDate, 1, "yyyy-MM-dd"),
                            dateUtil.getDateByAddingDate(endDate, 1, "yyyy-MM-dd"),
                            retailerName,
                            ticketNo); //Get Details

            byte[] excel =
                    excelUtil
                            .initExcel("Payment Details")
                            .setStyling()
                            .createHeader(header.getHeader())
                            .insertData(paymentDetailsData, "payment")
                            .saveExcel().getExcel();                          //Convert them to excel byte array

            saveExcelInHostUtil.saveExcel
                    (
                            "get",
                            "payment",
                            "details",
                            "Payment_Details.xlsx",
                            excel
                    );                                                  //Save them
            restResult.setResponse("server/attachment/download/get/payment/details");
            restResult.setSuccess(true);
            restResult.setMessage("Successfully Created Excel Data! ");

        } catch (Exception e) {
            restResult.setSuccess(false);
            restResult.setMessage("Some Error Occurred!");
            log.debug("Error" + e);
        }
        return restResult;

    }
}
