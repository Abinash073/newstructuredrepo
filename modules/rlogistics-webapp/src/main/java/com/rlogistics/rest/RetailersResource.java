package com.rlogistics.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.rlogistics.master.identity.MasterdataService;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerOther;

@RestController
@RequestMapping("/retailer")
public class RetailersResource {
	public static Logger log = LoggerFactory.getLogger(RestApi.class);
	private static String RECYCLE_DEVICE_GENERATE_OTP = "https://api.recycledevice.com/bizlog/shipment/otp/send";
	private static String RECYCLE_DEVICE_VARIFY_OTP = "https://api.recycledevice.com/bizlog/shipment/otp/verify";
	public static String RECYCLE_DEVICE_USER_NAME = "rdbiz_user";
	public static String RECYCLE_DEVICE_PASSWORD = "Orange_2829";

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/generate/otp-pickup", method = RequestMethod.POST, produces = "application/json")
	public Map generateOtp(@RequestParam(value = "processId") String processId) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Map<String, Object> responce = new HashMap<>();

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		String ticketNo = "";
		try {
			ticketNo = runtimeService.getVariable(processId, "rlTicketNo").toString();
			JSONObject requestJson = getSendOtpJson(ticketNo);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(RECYCLE_DEVICE_GENERATE_OTP);
			httppost.addHeader("Content-Type", "application/json");
			httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			RetailerApiCalls.saveToConsoleLogsTable(ticketNo,
					"OTPGEN:REQUEST: " + requestJson + ":" + "RESPONSE: " + rawResponse);
			if (new JSONObject(rawResponse.toString()).getBoolean("error")) {
				responce.put("success", false);
				responce.put("msg", "OTP not generated");
			} else {
				responce.put("success", true);
				responce.put("msg", "OTP sent to customer's device.");
			}
		} catch (Exception e) {
			RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "OTPGEN:ERROR: " + e);
			responce.put("success", false);
			responce.put("msg", "Something went wrong.");
		}

		return responce;
	}

	private JSONObject getSendOtpJson(String ticketNo) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("username", RECYCLE_DEVICE_USER_NAME);
		jsonObject.put("password", RECYCLE_DEVICE_PASSWORD);
		jsonObject.put("ticket_number", ticketNo);
		return jsonObject;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/validate/otp-pickup", method = RequestMethod.POST, produces = "application/json")
	public Map validateOtp(@RequestParam(required = true, value = "processId") String processId,
			@RequestParam(required = true, value = "otp") String otp) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		Map<String, Object> responce = new HashMap<>();

		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		String ticketNo = "";
		try {
			ticketNo = runtimeService.getVariable(processId, "rlTicketNo").toString();
			JSONObject requestJson = getVarifyOtpJson(ticketNo, otp);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(RECYCLE_DEVICE_VARIFY_OTP);
			httppost.addHeader("Content-Type", "application/json");
			httppost.setEntity(new StringEntity(requestJson.toString(), "UTF8"));
			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			String rawResponse = EntityUtils.toString(httpResponse.getEntity());
			RetailerApiCalls.saveToConsoleLogsTable(ticketNo,
					"OTPVER:REQUEST: " + requestJson + ":" + "RESPONSE: " + rawResponse);
			JSONObject jsonObject=new JSONObject(rawResponse);
			if (jsonObject.getBoolean("error")) {
				responce.put("success", false);
				responce.put("msg", "Wrong OTP");
			} else {
				responce.put("success", true);
				responce.put("msg", "OTP verified successfully");
			}
		} catch (Exception e) {
			RetailerApiCalls.saveToConsoleLogsTable(ticketNo, "OTPVER:ERROR: " + e);
			responce.put("success", false);
			responce.put("msg", "Something went wrong.");
		}

		return responce;
	}

	private JSONObject getVarifyOtpJson(String ticketNo, String optSting) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("username", RECYCLE_DEVICE_USER_NAME);
		jsonObject.put("password", RECYCLE_DEVICE_PASSWORD);
		jsonObject.put("ticket_number", ticketNo);
		jsonObject.put("received_otp", optSting);
		return jsonObject;
	}
}
