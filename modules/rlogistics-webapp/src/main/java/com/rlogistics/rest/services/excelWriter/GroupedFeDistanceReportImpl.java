package com.rlogistics.rest.services.excelWriter;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of All Fe Distance Data while grouping it based on location
 * @author Adarsh
 */
@Service("groupedFeDataReport")
@Scope(value = "prototype")

public class GroupedFeDistanceReportImpl extends ReportExcelUtilImpl {
    @Override
    public ReportExcelUtil
    insertData(List<?> data, String type) throws IOException {
        try {
            if (type.equals("groupedFeData")) {
                ((ArrayList<Map<String, BigDecimal>>) data).forEach(this::insertInCell);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            wb.write(outputStream);
        }
        return this;
    }

    private void insertInCell(Map<String, BigDecimal> data) {
        int colNo = 0;

        //New Row
        Row row = sheet.createRow(rownum++);

        //Create S. No cell
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Ticket No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.keySet().iterator().next());

        //Total Distance
        cell = row.createCell(colNo);
        cell.setCellValue(data.entrySet().iterator().next().getValue().toString() + " km");
    }
}
