package com.rlogistics.rest;

import java.util.List;

public class CompletedTaskWithCount {
    private List<ProcessResultWithVariables> processResultWithVariables;
    private long totalCount;

    public List<ProcessResultWithVariables> getProcessResultWithVariables() {
        return processResultWithVariables;
    }

    public void setProcessResultWithVariables(List<ProcessResultWithVariables> processResultWithVariables) {
        this.processResultWithVariables = processResultWithVariables;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
