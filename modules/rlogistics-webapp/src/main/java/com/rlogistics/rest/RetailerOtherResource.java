package com.rlogistics.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.RetailerOther;

@RestController
public class RetailerOtherResource {
	public static Logger log = LoggerFactory.getLogger(RestApi.class);

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/get/retailers-other-setting", method = RequestMethod.POST, produces = "application/json")
	public List getRetailerSetting(@RequestParam(value = "ticketNo") String ticketNo) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getMasterdataService();
		List<Map<String, Object>> responce = new ArrayList<>();
		List<RetailerOther> retailerOther = masterdataService.createRetailerOtherQuery().isMobileProductAddAllow(1)
				.list();
		for (RetailerOther other : retailerOther) {
			Map<String, Object> map = new HashMap<String, Object>();
			Retailer retailer = masterdataService.createRetailerQuery().id(other.getRetailerId()).singleResult();
			map.put("retailerCode", retailer.getCode().toString());
			map.put("otp", 1);
			responce.add(map);
		}
		return responce;
	}

}
