package com.rlogistics.rest.services.PhysicalEvaluationService;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ImeiNumberWithQandAnsDTO {

    private String processId;
    private List<PhysicalEvaluationDataDTO> data = new ArrayList<>();
}
