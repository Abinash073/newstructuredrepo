package com.rlogistics.rest.controllers.BulkUtilControllers;

import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.TicketImages;
import com.rlogistics.rest.RestResponse;
import com.rlogistics.rest.controllers.dto.ImeiWithImageDetails;
import com.rlogistics.rest.repository.BulkImageRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to get Images for Bulk flow
 * @author Adarsh
 */
@var
@RestController
public class BulkImageController {

    private MasterdataService masterdataService;

    private BulkImageRepository imageRepository;

    @Autowired
    public BulkImageController(MasterdataService masterdataService, BulkImageRepository imageRepository) {
        this.masterdataService = masterdataService;
        this.imageRepository = imageRepository;
    }

    /**
     * Fetching the Bulk images based on processId
     * @param processId Process id of ticket
     * @return DTO with image urls
     */
    @RequestMapping(value = "/get/bulk/images", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResponse getImage(@RequestParam("processId") String processId) {
        RestResponse response = new RestResponse();
        List<AdditionalProductDetails> productDetails = masterdataService.createAdditionalProductDetailsQuery().processId(processId).list();


        HashMap<String, String> modifiedId =
                productDetails
                        .stream()
                        .collect(HashMap::new,
                                (b, c) -> b.put(c.getIdentificationNo(),
                                        c.getProcessId() + c.getIdentificationNo()),
                                HashMap::putAll);


        List<ImeiWithImageDetails> value = modifiedId.entrySet().stream().map(a -> this.getS3Url(a.getKey(), a.getValue())).collect(Collectors.toList());
        response.setResponse(value);
        response.setSuccess(true);

        return response;

    }

    /**
     * Fetching the Images based on imei number of device
     * @param imei Imei number of phone
     * @return DTO with S3 image url
     */
    @RequestMapping(value = "/get/bulk/images/by-imei", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private ImeiWithImageDetails getImagesFromImei(@RequestParam("imei") String imei) {

        ImeiWithImageDetails imeiWithImageDetails = new ImeiWithImageDetails();
        try {
            List<TicketImages> images = imageRepository.getImages(imei);
            imeiWithImageDetails.setImeiNo(imei);
            imeiWithImageDetails.setImages(images);
            imeiWithImageDetails.setSuccess(true);
        } catch (Exception e) {
            imeiWithImageDetails.setSuccess(false);
        }
        return imeiWithImageDetails;
    }


    private ImeiWithImageDetails getS3Url(String imei, String processId) {
        List<TicketImages> data = masterdataService.createTicketImagesQuery().procInstId(processId).list();
        ImeiWithImageDetails details = new ImeiWithImageDetails();
        details.setImeiNo(imei);
        details.setImages(data);
        return details;


    }
}
