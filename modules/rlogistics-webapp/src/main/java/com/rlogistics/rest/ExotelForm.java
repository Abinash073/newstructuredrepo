package com.rlogistics.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.*;
import com.rlogistics.master.identity.impl.cmd.CreateExotelLogQueryCmd;
import com.rlogistics.master.identity.impl.cmd.SaveExotelLogCmd;
import com.rlogistics.master.identity.impl.persistence.entity.ExotelLogEntity;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.Task;
import org.apache.http.NameValuePair;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * Exotel Integration with Rlogictics
 *
 * @author Adarsh
 */
@RestController
public class ExotelForm extends RLogisticsResource {


    private static Logger log = LoggerFactory.getLogger(ExotelForm.class);

    /**
     * Passes the data to exotel
     *
     * @param processId      process Id
     * @param custContactNo  Cust no
     * @param assigneeNumber Fe / Co number
     * @return DTO of calling operation result
     */
    public static RestResult callCustomer(String processId, String custContactNo, String assigneeNumber) {
        RestResult result = new RestResult();

        log.debug(processId + " " + custContactNo + "" + assigneeNumber);
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        MasterdataService masterdataService1 = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        Map<String, String> logvariables = new HashMap<>();
        logvariables.put("processId", processId);
        //ProcessResponseWithVariables variables = findByProcessId(processId);


        List<NameValuePair> nameValuePairs = new ArrayList<>(1);
        List<NameValuePair> nameValuePairs1 = new ArrayList<>(1);
        List<NameValuePair> nameValuePairs2 = new ArrayList<>(1);
        final String[] taskAssignee = {""};
        String fromContactNo = "";
        String city = "";
        String exotelnumber = "";
        String status = "";
        String exotelnumber1 = "";
        //String defultExotelNumber = "08030456042";

        // TODO Mapping of assignee Number and pincode


       /* try {
            // RETRIVING TASK ASSIGNEE FOR FROM CONTACT NO

            List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                    .getTaskService().createTaskQuery().processInstanceId(processId).orderByTaskCreateTime().desc()
                    .list();
            if (!tasks.isEmpty()) {
                Task task = tasks.get(0);
                taskAssignee = task.getAssignee();
                log.debug("task Assignee : " + taskAssignee);
                logvariables.put("Assignee", taskAssignee);
            }
        } catch (Exception e) {
            result.setMessage("Error in Getting Task Variables");
            result.setSuccess(false);
            result.setResponse(e);
            log.debug("Error in getting Task Variables :" + e);
            return result;
        }*/


        /**
         *Retriving City of customer from Process Id and Pincode
         */
        String pincode = String.valueOf(findByProcessIdandvariable(processId, "pincode"));
        if (pincode != null) {
            LocationPincodes locationPincodes;
            try {
                locationPincodes = masterdataService.createLocationPincodesQuery().pincode(pincode).singleResult();//needs to change this code
            } catch (Exception e) {
                log.debug("Multiple Pincode/Not Found" + e);
                result.setMessage("Invalid Pincode");
                return result;
            }
            ProcessLocation processLocation = masterdataService.createProcessLocationQuery().id(locationPincodes.getLocationId()).singleResult();
            city = processLocation.getCity();
            log.debug("City Name" + city);
            logvariables.put("City", city);
        } else {
            log.debug(" Pincode Not found");
        }


        try {
            if (assigneeNumber.length() == 10) {
                assigneeNumber = 0 + assigneeNumber;
                logvariables.put("AssigneeNumber", assigneeNumber);
                log.debug("From Cont No :" + assigneeNumber);

            } else {
                result.setMessage("Assignee Number is Invalid");
                return result;
            }


            if (custContactNo != null && !custContactNo.isEmpty() && assigneeNumber != null
                    && !assigneeNumber.isEmpty()) {
                if (custContactNo.length() == 10) {
                    custContactNo = 0 + custContactNo;

                } else {
                    //log.debug("is country code present  -> "+checkIfNumberHasCountryCode(custContactNo));
                    if (checkIfNumberHasCountryCode(custContactNo)) {

                        custContactNo = 0 + custContactNo.replaceAll("\\s+", "").replaceFirst("((\\+91)|0|(\\+1)|1?)", "").replaceFirst("91", "").trim();
                        log.debug(custContactNo + " " + "after removing cust contry code");

                    } else if (custContactNo.startsWith("0")) {

                        log.debug("Number format is accurate");

                    } else {
                        result.setMessage("Customer Number is invalid");
                        return result;
                    }
                }
                logvariables.put("CustomerNumber", custContactNo);
                log.debug("To Cont No :" + custContactNo);


                try {
                    //Query to get the city from RL_MD_DYNAMIC_EXOTEL_TABLE
                    if (city != null) {
                        DynamicExotelMapping cityWisEexotelNumber = masterdataService1.createDynamicExotelMappingQuery().location(city).singleResult();

                        if (cityWisEexotelNumber != null) {

                            exotelnumber = cityWisEexotelNumber.getExotelPhone();
                            status = cityWisEexotelNumber.getStatus();
                            log.debug("Inside Regional number Query" + status + exotelnumber);

                        } else {
                            try {
                                City city1 = masterdataService.createCityQuery().name(city).singleResult();
                                String state = city1.getState();
                                DynamicExotelMapping exotelMapping = masterdataService.createDynamicExotelMappingQuery().location(state).singleResult();
                                exotelnumber = exotelMapping.getExotelPhone();
                                status = exotelMapping.getStatus();
                            } catch (Exception e) {
                                log.debug("City Not found.Will Call from default number" + e);
                            }
                        }


                        if (exotelnumber != null && status.equalsIgnoreCase("active")) {
                            exotelnumber1 = exotelnumber;
                            log.debug("Inside Regional number" + status + exotelnumber1);

                        } else {
                            exotelnumber1 = Constants.DEFAULT_EXOTEL_NUMBER.toString();
                            Constants.BASE_DEV_URL.ordinal();
                            log.debug("Inside Defult Number" + exotelnumber1);
                        }
                        logvariables.put("exotelNumber", exotelnumber1);


                        /**
                         * If request fails **exotelApicallToConnect** throws exception
                         * Handled in catch block
                         */
                        ResponseEntity<String> response = exotelApiCallToConnect(assigneeNumber, custContactNo, exotelnumber1);

                        /**
                         * Executes only if Api request to Exotel is sucessfull i.e HttpStatus:200
                         */
                        List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                                .getTaskService().createTaskQuery().processInstanceId(processId).orderByTaskCreateTime().desc()
                                .list();
                        Task task = tasks.get(0);
                        log.debug(task.toString());
                        taskAssignee[0] = task.getAssignee();
                        log.debug(taskAssignee[0]);
                        logvariables.put("Assignee", taskAssignee[0]);
                        String ticketNo = String.valueOf(findByProcessIdandvariable(processId, "rlTicketNo"));
                        logvariables.put("TicketNo", ticketNo);
                        storeCallData(response, logvariables);


                        if (response.getStatusCode().is2xxSuccessful()) {

                            result.setMessage("Call is connecting...");
                            result.setSuccess(true);
                            return result;

                        }
                        //Keeping this!! May be required later
                        else if (response.getStatusCode().value() == 400) {
                            result.setMessage("Number is invalid");
                            result.setSuccess(false);
                            return result;
                        } else {
                            ResponseEntity<String> custWhitelistResponse = whiteList(custContactNo);
                            ResponseEntity<String> assigneeWhitelistResponse = whiteList(assigneeNumber);
                            result.setMessage("Try Again");
                            result.setSuccess(false);
                            return result;
                        }


                    } else {
                        result.setMessage("City Name Is Not Present");
                        result.setSuccess(false);
                    }
                } catch (HttpClientErrorException e) {
                    log.debug("Exception" + e.getStatusCode());
                    //for 403
                    if (e.getStatusCode() == HttpStatus.FORBIDDEN) {


                        try {
                            whiteList(custContactNo);
                            log.debug("Converted Cust No");

                        } catch (Exception e1) {
                            log.debug("Unable to Convert Cust No");
                        }
                        try {
                            whiteList(assigneeNumber);
                            log.debug("Converted Assignee Number");
                            result.setMessage("PLEASE TRY AGAIN");
                            result.setSuccess(true);
                            return result;
                        } catch (Exception e2) {
                            log.debug("Unable to Convert Assignee Number");

                        }

                    }
                    //for 400
                    if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                        result.setMessage("Number is invalid");
                        result.setSuccess(false);
                        return result;
                    }


                } catch (Exception ex) {
                    log.error("Exception during Calling Exotel API", ex);
                    whiteList(custContactNo);
                    whiteList(assigneeNumber);
                    result.setMessage("Error in Calling Exotel API");
                    result.setResponse(ex);
                    result.setSuccess(false);
                    return result;
                }
            } else {
                result.setMessage("Either To or From is Empty");
                result.setSuccess(false);
                return result;

            }

        } catch (Exception e) {
            log.debug("Error in getting Phone No From Assignee :" + e);
        }

        return result;
    }

    /**
     * @param processId = Activiti Process ID
     * @return processResponseWithVariables
     * @Adarsh To Get The processId From Running Task Of History Task Service
     */
    public static ProcessResponseWithVariables findByProcessId(String processId) {

        try {
            Task task = null;
            ProcessResponseWithVariables processResultWithVariables = null;
            ProcessInstance processInstance = ProcessEngines.getDefaultProcessEngine()
                    .getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();

            RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine()
                    .getRuntimeService();

            if (processInstance != null) {
                List<Task> tasks = ProcessEngines.getDefaultProcessEngine()
                        .getTaskService().createTaskQuery().processInstanceId(processId).includeProcessVariables()
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    task = tasks.get(0);
                    processResultWithVariables = new ProcessResponseWithVariables(processInstance, task,
                            runtimeService.getVariables(processId)

                    );
                } else {

                    ManagementService managementService = ProcessEngines
                            .getDefaultProcessEngine().getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();
                    }

                    processResultWithVariables = new ProcessResponseWithVariables(processInstance, jobId,
                            runtimeService.getVariables(processId)

                    );
                }
            } else {

                HistoricProcessInstance historicProcessInstance = ProcessEngines
                        .getDefaultProcessEngine().getHistoryService().createHistoricProcessInstanceQuery()
                        .processInstanceId(processId).includeProcessVariables().singleResult();

                if (historicProcessInstance != null) {
                    processResultWithVariables = new ProcessResponseWithVariables(historicProcessInstance,

                            historicProcessInstance.getProcessVariables()

                    );

                    processResultWithVariables.setCompleted(true);
                } else {
                    processResultWithVariables = new ProcessResponseWithVariables(

                            runtimeService.getVariables(processId)

                    );

                    log.error("Process Id " + processId + " is neither present not past. It is missing.");
                }
            }

            return processResultWithVariables;
        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            return null;
        }
    }

    /**
     * To get <object>processVariable</object> value
     * from given processId
     */
    public static Object findByProcessIdandvariable(@NotNull String processId, String processVariable) {

        try {
            Task task = null;
            ProcessResponseWithVariables processResultWithVariables = null;
            ProcessInstance processInstance = ProcessEngines.getDefaultProcessEngine()
                    .getRuntimeService().createProcessInstanceQuery().processInstanceId(processId).singleResult();

            RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine()
                    .getRuntimeService();

            if (processInstance != null) {
                List<Task> tasks = ProcessEngines.getDefaultProcessEngine()
                        .getTaskService().createTaskQuery().processInstanceId(processId).includeProcessVariables()
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    task = tasks.get(0);
                    processResultWithVariables = new ProcessResponseWithVariables(processInstance, task,
                            runtimeService.getVariables(processId)

                    );
                } else {

                    ManagementService managementService = ProcessEngines
                            .getDefaultProcessEngine().getManagementService();

                    List<Job> jobs = managementService.createJobQuery()
                            .processInstanceId(processInstance.getProcessInstanceId()).list();

                    String jobId = null;
                    /* TODO Handle if more than one job */
                    if (!jobs.isEmpty()) {
                        jobId = jobs.get(0).getId();
                    }

                    processResultWithVariables = new ProcessResponseWithVariables(processInstance, jobId,
                            runtimeService.getVariables(processId)

                    );
                }
            } else {

                HistoricProcessInstance historicProcessInstance = ProcessEngines
                        .getDefaultProcessEngine().getHistoryService().createHistoricProcessInstanceQuery()
                        .processInstanceId(processId).includeProcessVariables().singleResult();

                if (historicProcessInstance != null) {
                    processResultWithVariables = new ProcessResponseWithVariables(historicProcessInstance,

                            historicProcessInstance.getProcessVariables()

                    );

                    processResultWithVariables.setCompleted(true);
                } else {
                    processResultWithVariables = new ProcessResponseWithVariables(

                            runtimeService.getVariables(processId)

                    );

                    log.error("Process Id " + processId + " is neither present not past. It is missing.");
                }
            }

            Map<String, Object> processVariableMap = processResultWithVariables.getVariables();
            return processVariableMap.get(processVariable);

        } catch (Exception ex) {
            log.error("Exception during listInvolvedTasks", ex);
            return null;
        }
    }

    /**
     * To get first processId from the MobileNumber
     * Returns <String>processId</String>
     */
    public static String findByNumber(String from) {
        log.debug("Inside find by number");
        ProcessInstanceQuery piQuery = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService().createProcessInstanceQuery().orderByProcessInstanceId().asc();
        RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getRuntimeService();
        RepositoryServiceImpl repositoryService = (RepositoryServiceImpl) ((RLogisticsProcessEngineImpl) (ProcessEngines
                .getDefaultProcessEngine())).getRepositoryService();
        List<ProcessInstance> processInstances = null;
        List<ProcessResultWithVariables> retval = new ArrayList<>();
        Set<String> variablesOfInterest = new HashSet<>();
        variablesOfInterest.add("rlProcessId");
        variablesOfInterest.add("processId");
        String variable = "telephoneNumber";
        String processid1 = "";
        if (from != null) {
            piQuery.variableValueEquals(variable, from);
            processInstances = piQuery.list();
            for (ProcessInstance processInstance : processInstances) {
                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                        .orderByTaskCreateTime().desc().list();

                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    retval.add(new ProcessResultWithVariables(processInstance, task,
                            variablesOfInterest.isEmpty()
                                    ? runtimeService.getVariables(processInstance.getProcessInstanceId())
                                    : runtimeService.getVariables(processInstance.getProcessInstanceId(),
                                    variablesOfInterest)));
                    log.debug("A" + retval.toString() + retval);
                    log.debug(retval.get(0).toString());
                    for (ProcessResultWithVariables value : retval) {
                        log.debug("Value of element " + value.getVariables().get("rlProcessId"));
                        log.debug("value" + value.getVariables().get("retailer"));
                        processid1 = value.getVariables().get("rlProcessId").toString();

                    }
                    //return processid1;
                } else {
                    return null;
                }

            }


        } else {
            return null;
        }
        log.debug(processid1 + "Returning Process ID ");
        return processid1;
    }


    /**
     * Exotel API call to connect
     *
     * @param fromContactNo       Fe / Co number
     * @param toContactNumber     Cust Number
     * @param exotelVirtualNumber Exotel Virtual Number   | Can be found from my.exotel.in
     * @return <String>ResponseEntity</String>
     */
    // TODO - DELETE IT //for test only
    private static ResponseEntity<String> exotelApiCallToConnect(@RequestParam(value = "from") String fromContactNo, @RequestParam(value = "to") String toContactNumber, @RequestParam(value = "exotel") String exotelVirtualNumber) {

        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();

        requestBody.add("From", fromContactNo);

        requestBody.add("To", toContactNumber);

        requestBody.add("CallerId", exotelVirtualNumber);

        requestBody.add("CallType", "trans");

        requestBody.add("StatusCallback", Constants.REV_REVERSE_STATUS_CALLBACK.toString());
        requestBody.forEach((s, strings) -> log.debug(s, strings));

        ResponseEntity<String> response = httpRequestToServer(Constants.EXOTEL_CALL_CONNECT.toString(), requestBody);

        log.debug("Status" + response.getStatusCode().toString() + " " + "response" + response.toString());

        return response;

    }

    /**
     * Exotel API to change number to whitelist
     *
     * @param numToBeWhitelisted Num that needs to be white listed
     * @return <String>ResponseEntity</String>
     */

    private static ResponseEntity<String> whiteList(String numToBeWhitelisted) {

        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();

        requestBody.add("VirtualNumber", "08030456042");

        requestBody.add("Number", numToBeWhitelisted);

        return httpRequestToServer(Constants.EXOTEL_NUMBER_WHITELIST.toString(), requestBody);
    }

    /**
     * RestTemplate to execute a Http Post Request.Can be configured to accept different Http methods
     *
     * @param url                                        URL to be connected
     * @param /MultiValueMap/requestBody</MultiValueMap>
     * @return <String>ResponseEntity</String>
     */

    @SuppressWarnings("unchecked")
    public static ResponseEntity<String> httpRequestToServer(@NotNull String url, MultiValueMap<String, String> requestBody) {

        //CloseableHttpClient httpClient = HttpClients.createDefault();

        //--TODO--//
        /**
         * Disabling SSL verification
         * Highly Insecure
         * Needs to be fixed
         */
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);


        //Setting header so exotel should not block it
        httpHeaders.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        log.debug("Apache httpclient factory applied");
        @SuppressWarnings("unchecked") HttpEntity formEntity = new HttpEntity(requestBody, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, formEntity, String.class);
        log.debug("response.. " + response);
        return response;
    }

    /**
     * @param response : ExotelLog HTTP Response
     * @return boolean
     * Work Pending :  Saving Response When StatusCode is Not 200
     */
    public static Boolean storeCallData(ResponseEntity<String> response, Map<String, String> logVariables) {
        log.debug(response.toString());
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();

        try {
            if (response.getStatusCode().is2xxSuccessful()) {
                //String textResponse = EntityUtils.toString(response.);
                //log.debug("Response from server :" + textResponse);

                JSONObject jsonObject = new JSONObject(String.valueOf(response.getBody()));
                //JSONObject jsonObject1=new JSONObject(callArray);
                log.debug("Response In String" + jsonObject.getString("Call"));
                ExotelLogEntity exotelLogEntity = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true).readValue(jsonObject.getString("Call"), ExotelLogEntity.class);
                exotelLogEntity.setId(UUID.randomUUID().toString());
                exotelLogEntity.setProcessId(logVariables.get("processId"));
                exotelLogEntity.setTicketNumber(logVariables.get("TicketNo"));
                exotelLogEntity.setCustomerNumber(logVariables.get("CustomerNumber"));
                exotelLogEntity.setAssignee(logVariables.get("Assignee"));
                exotelLogEntity.setCity(logVariables.get("City"));
                exotelLogEntity.setExotelNumber(logVariables.get("exotelNumber"));
                masterdataService.saveExotelLog(exotelLogEntity);
                return true;


            } else {

                //String textResponse = EntityUtils.toString(response.getEntity());
                //log.debug("Response from server :" + textResponse);
                JSONObject jsonObject = new JSONObject(response.getBody());
                String restExceptionArray = jsonObject.getString("RestException").toString();
                JSONObject jsonObject1 = new JSONObject(restExceptionArray);

                ExotelLogEntity exotelLogEntity = new ExotelLogEntity();
                exotelLogEntity.setId(UUID.randomUUID().toString());
                exotelLogEntity.setStatus(jsonObject1.getString("Message"));
                exotelLogEntity.setProcessId(logVariables.get("ProcessId"));
                exotelLogEntity.setCustomerNumber(logVariables.get("CustomerNumber"));
                exotelLogEntity.setAssignee(logVariables.get("Assignee"));
                masterdataService.saveExotelLog(exotelLogEntity);
                return true;


            }
        } catch (Exception e) {
            log.debug("Failed in Saving Call log" + e.toString());
            return false;
        }
    }

    /**
     * To check if the number has country code or not
     * [Supported Coutry Code] +91/91/ 91
     *
     * @param number = Phone No
     * @return boolean
     */
    private static boolean checkIfNumberHasCountryCode(String number) {

        if (number.length() == 12 || number.length() == 13) {
            log.debug("Number length" + number.length() + number);
            number = number.trim();
            if (number.startsWith("91")) {
                return true;
            } else if (number.startsWith("+91")) {
                return true;
            } else if (number.startsWith("91", 1)) {
                return true;
            } else {

                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * @param httpRequest    =request
     * @param httpResponse   = Response <text/Plain>
     * @param callSid
     * @param from
     * @param to
     * @param callType
     * @param dialWhomNumber
     * @return = In Progress
     */
    @RequestMapping(value = "/call/checknumber", produces = "text/plain", method = RequestMethod.GET)
    public String checknumber(HttpServletRequest httpRequest, HttpServletResponse httpResponse, @RequestParam(value = "CallSid") String callSid,
                              @RequestParam(value = "From") String from,
                              @RequestParam(value = "To") String to,
                              @RequestParam(value = "CallType") String callType,
                              @RequestParam(value = "DialWhomNumber") String dialWhomNumber
    ) {
        log.debug("String" + " " + callSid + from + to + callType + dialWhomNumber);
        httpResponse.setContentType("text/plain");
        httpResponse.setCharacterEncoding("UTF-8");
        String taskAssignee;
        String taskAssigneeNumber = null;

        try {
            log.debug(from);
            String processId = ExotelForm.findByNumber(from);
            log.debug(processId);
            if (processId != null) {
                // RETRIVING TASK ASSIGNEE FOR FROM CONTACT NO

                List<Task> tasks = ((RLogisticsProcessEngineImpl)
                        (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService()
                        .createTaskQuery()
                        .processInstanceId(processId)
                        .orderByTaskCreateTime()
                        .desc()
                        .list();
                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    taskAssignee = task.getAssignee();
                    log.debug("task Assignee : " + taskAssignee);
                    //To retrive the task assignee number
                    MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                            .getMasterdataService();
                    ProcessUser processUser = masterdataService.createProcessUserQuery().email(taskAssignee).singleResult();
                    String rolecode = processUser.getRoleCode();
                    if (rolecode == "field_engineer" || rolecode == "coordinator") {
                        taskAssigneeNumber = processUser.getPhone();
                        return taskAssigneeNumber;
                    } else {
                        httpResponse.sendError(300, "not field engg");
                        httpResponse.setStatus(300);
                    }
                } else {
                    log.debug("No task Assigne" + "Forwarding it to defult location" + to);
                    httpResponse.sendError(300, "not found");
                    httpResponse.setStatus(300);
                }

            } else {
                // processId not found
                httpResponse.sendError(300, "not found");
                httpResponse.setStatus(300);
            }
        } catch (Exception e) {
            log.debug("blah");
            log.debug("" + e);
        }
        return taskAssigneeNumber;
    }

    /**
     * To Call Customer from angular
     *
     * @param httpRequest
     * @param httpResponse
     * @param processId      process Id of ticket
     * @param csnumber       Current Number / Alternate Number of costumer
     * @param assigneeNumber Fe /Co number
     * @param customerNumber Customer Number
     * @return RestResult
     */
    @RequestMapping(value = "/call/customer/{processId}", produces = "application/json")
    public RestResult makeCallToCustomer(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                                         @PathVariable(value = "processId") String processId,
                                         @RequestParam(value = "to") String csnumber,
                                         @RequestParam(value = "assigneeNumber", required = false) String assigneeNumber,
                                         @RequestParam(value = "customerNumber", required = false) String customerNumber) {
        String contNo = null;
        log.debug(csnumber + " " + assigneeNumber + " " + customerNumber);
        RestResult result = new RestResult();
//        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
//
//            return null;
//        }


        if (assigneeNumber != null && customerNumber != null && assigneeNumber.trim() != "null") {
            log.debug("If Assignee Number & customer number is not null");
            return callCustomer(processId, customerNumber, assigneeNumber);

        } else if (customerNumber == null) {

            if (csnumber.equalsIgnoreCase("currentNumber")) {
                log.debug("if customer current number is null");
                contNo = String.valueOf(findByProcessIdandvariable(processId, "telephoneNumber"));
            } else {
                log.debug("if customer alternate number is null");
                contNo = String.valueOf(findByProcessIdandvariable(processId, "alternateTelephoneNumber"));

            }

        }


        if (assigneeNumber == null || assigneeNumber == "null") {
            log.debug("if assignee number is  null");

            try {
                String taskAssignee;
                List<Task> tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                        .getTaskService().createTaskQuery().processInstanceId(processId).orderByTaskCreateTime().desc()
                        .list();
                Task task = tasks.get(0);
                taskAssignee = task.getAssignee();
                log.debug("task Assignee : " + taskAssignee);
                // logvariables.put("Assignee", taskAssignee);
                TaskResource taskResource = new TaskResource();

                assigneeNumber = taskResource.getAssigneeNumber(taskAssignee).getData();


            } catch (Exception e) {
                result.setMessage("Error in Getting Task Variables");
                result.setSuccess(false);
                result.setResponse(e);
                log.debug("Error in getting Task Variables :" + e);
                return result;
            }

        }
        if (contNo != null && assigneeNumber != null) {
            return callCustomer(processId, contNo, assigneeNumber);
        } else {
            result.setSuccess(false);
            result.setMessage("Customer Number or Assignee Number is not available");
            result.setResponse("Customer Number or Assignee Number is not available");
            return result;

        }
    }

    @RequestMapping(value = "/call/customer", produces = "application/json")
    public RestResult makeCall(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                               @RequestParam(value = "processId") String processId, @RequestParam(value = "contNo") String contNo) {
        log.debug("Call Customer api");

        if (!beforeMethodInvocation(httpRequest, httpResponse)) {
            return null;
        }
        return callCustomer(processId, contNo, "null");
    }

    /**
     * Web hook for Exotel so that they can pass call details and status after the call ends
     *
     * @param callSid      Call sid provided by Exotel unique for all the calls
     * @param status       Status of that call
     * @param recordingUrl Recording url if available
     * @param dateUpdated  last Date updated
     * @return response
     */
    @RequestMapping(value = "call/status", produces = "application/json", method = RequestMethod.POST)
    public RestResult callBackStatus(
            @RequestParam(value = "CallSid", required = false) String callSid,
            @RequestParam(value = "Status", required = false) String status,
            @RequestParam(value = "RecordingUrl", required = false) String recordingUrl,
            @RequestParam(value = "DateUpdated", required = false) String dateUpdated) {
        log.debug("Executing the call status after call ends");
        String testStatus = callSid + " " + status + " " + recordingUrl;
        log.debug("status" + "  " + testStatus);

        ExotelForm exotelForm = new ExotelForm();
        MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
                .getMasterdataService();
        RestResult restResult = new RestResult();
        if (callSid != null) {
            int countOfSid = Math.toIntExact(masterdataService.createExotelLogQuery().sid(callSid).count());
            log.debug("Count of SID" + countOfSid);
            if (countOfSid == 0) {
                restResult.setSuccess(false);
                restResult.setMessage("Sid not found");
                restResult.setResponse("Sid not found");
                return restResult;
            } else if (countOfSid == 1) {
                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                ExotelLog.ExotelLogQuery exotelLogQuery1 = commandExecutor.execute(new CreateExotelLogQueryCmd());
                ExotelLog exotelLogInstance = exotelLogQuery1.sid(callSid).singleResult();
                if (exotelLogInstance == null) {
                    throw new RuntimeException("Could not find ExotelLog with Sid" + callSid);
                }

                exotelLogInstance.setRecordingUrl(recordingUrl);
                exotelLogInstance.setSid(callSid);
                exotelLogInstance.setStatus(status);
                exotelLogInstance.setDateUpdated(dateUpdated != null ? dateUpdated : null);
                try {
                    commandExecutor.execute(new SaveExotelLogCmd(exotelLogInstance));
                    restResult.setMessage("Sucesfully Updated");
                } catch (Exception e) {
                    log.debug("Failed In Updating Exotel Log" + e);
                    restResult.setMessage("Failed To Update Exotel Log");
                    return restResult;
                }

                return restResult;
            } else {


                CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) (ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
                ExotelLog.ExotelLogQuery exotelLogQuery1 = commandExecutor.execute(new CreateExotelLogQueryCmd());
                List<ExotelLog> exotelLogInstance = exotelLogQuery1.sid(callSid).list();
                if (exotelLogInstance == null) {
                    throw new RuntimeException("Could not find ExotelLog with Sid" + callSid);
                }
                for (ExotelLog exotelLog : exotelLogInstance) {
                    exotelLog.setRecordingUrl(recordingUrl);
                    exotelLog.setSid(callSid);
                    exotelLog.setStatus(status);
                    exotelLog.setDateUpdated(dateUpdated);
                    try {
                        commandExecutor.execute(new SaveExotelLogCmd(exotelLog));
                        restResult.setMessage("Sucessfully Updated");

                    } catch (Exception e) {
                        log.debug("Failed In Updating Exotel Log" + e);
                        restResult.setMessage("Failed In Update Exotel Log");
                        return restResult;

                    }
                }

            }

        } else {
            /**
             * Duhh duhh to be written if sid or recording url is not present in exotel response
             *
             *Implement It according to the requirement
             */
            log.debug("SID Is Not Availabe So No Point Of Updating The Log");
            restResult.setMessage("Sid not available");
            restResult.setSuccess(false);
            return restResult;

        }
        return restResult;
    }

    @RequestMapping(value = "/call/fe", produces = "application/json")
    public RestResult callFe(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                             @RequestParam(value = "assigneeNumber", required = false) String assigneeNumber,
                             @RequestParam(value = "feNumber", required = false) String feNumber,
                             @RequestParam(value = "feEmail", required = false) String feEmail) {
        RestResult restResult = new RestResult();
        if (assigneeNumber != null && feNumber != null) {
            try {

                ResponseEntity<String> response = exotelApiCallToConnect(assigneeNumber, feNumber, Constants.DEFAULT_EXOTEL_NUMBER.toString());
                //TODO LOGIC TO SAVE RESPONSE


                if (response.getStatusCode().is2xxSuccessful()) {
                    restResult.setMessage("Call is Connecting..");
                    restResult.setResponse("Call Connecting..");
                    restResult.setSuccess(true);
                } else if (response.getStatusCode().value() == 400) {
                    restResult.setMessage("Number Is Invalid");
                    restResult.setResponse("Numebr Is Invalid");
                    restResult.setSuccess(true);
                } else {
                    whiteList(assigneeNumber);
                    whiteList(feNumber);
                    restResult.setMessage("Try Again");
                    restResult.setResponse("Try Again");
                    restResult.setSuccess(false);
                }

                return restResult;

            } catch (HttpClientErrorException e) {
                try {
                    whiteList(assigneeNumber);
                    restResult.setMessage("Try Again");
                } catch (Exception f) {
                    log.debug("Error changing assignee number" + e);
                }
                try {
                    whiteList(feNumber);
                    restResult.setMessage("Try Again");
                    return restResult;
                } catch (Exception g) {
                    log.debug("Error changing Fe Number");
                }

            } catch (Exception e) {
                restResult.setMessage("Contact Tech Support");
                restResult.setResponse("ERROR OCCURED" + e);
                restResult.setSuccess(false);
                return restResult;
            }
        }
        restResult.setMessage("Number Not Found");
        restResult.setResponse("Numebr Not Found");
        restResult.setSuccess(false);
        return restResult;
    }

}


