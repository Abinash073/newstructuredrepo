package com.rlogistics.rest.bulkApiForClient.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.AdditionalProductDetails;
import com.rlogistics.master.identity.ProductCategory;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.domain.FieldVal;
import com.rlogistics.rest.bulkApiForClient.dto.TicketNumber;
import com.rlogistics.rest.bulkApiForClient.services.DataToListOfMapService;
import com.rlogistics.rest.services.ticketCreationService.BulkTicketCreationService;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

import org.activiti.engine.ProcessEngines;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Handles client side interaction with Bizlog for BULK flow
 *
 * @author Adarsh
 */
@RestController
@RequestMapping("${base_bulk_uri}")
@Slf4j
@Valid
@var
@SuppressWarnings("unused")
public class BulkApiController {

	private DataToListOfMapService dataToListOfMapService;

	private BulkTicketCreationService bulkTicketCreationService;

	@Autowired
	public BulkApiController(DataToListOfMapService dataToListOfMapService,
			BulkTicketCreationService bulkTicketCreationService) {
		this.dataToListOfMapService = dataToListOfMapService;
		this.bulkTicketCreationService = bulkTicketCreationService;
	}

	/**
	 * URI/API for clients to create ticket for BULK flow
	 *
	 * @param retailerId
	 * @param apiToken
	 * @param fieldVal
	 * @param request
	 * @param response
	 * @return Ticket Number DTO
	 */
	@RequestMapping(value = "/create-ticket", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	private TicketNumber createBulkTicketByApi(@RequestHeader(value = "retailerId") String retailerId,
			@RequestHeader(value = "apiToken") String apiToken, @Valid @RequestBody FieldVal fieldVal,
			HttpServletRequest request, HttpServletResponse response) {

		var data = dataToListOfMapService.getDataInRequiredFormat(fieldVal);
		var ticketNumber = bulkTicketCreationService.createBulkTicket(data, retailerId);
		return TicketNumber.builder().ticketNumber(ticketNumber).build();
	}

	
}
