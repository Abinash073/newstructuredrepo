package com.rlogistics.rest;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.ProcessLocation;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;

import pojo.ReportPojo;

import com.rlogistics.master.identity.MasterdataService;

@RestController
public class RepairStandByController extends RLogisticsResource {
	private static Logger log = LoggerFactory.getLogger(RepairStandByController.class);

	@RequestMapping(value = "/ticket/create/repairstandby", method = RequestMethod.POST, produces = "application/json")
	public RestExternalResult createTicketRepairStandBy(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse, @RequestParam(required = true, value = "fields") String queryJSON,
			@RequestParam(value = "retailerId") String retailerId,
			@RequestParam(value = "apiToken", required = true) String apiToken) {
		RestExternalResult restResult = new RestExternalResult();

		try {
			if (repairStandByPrimaryValidation(restResult, retailerId, apiToken, queryJSON)) {
				Map<String, String> fieldValues = new ObjectMapper().readValue(URLDecoder.decode(queryJSON), Map.class);
				if (repairStandBySecondaryValidation(restResult, retailerId, apiToken, fieldValues)) {

					MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getMasterdataService();

					Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
					String retailer = retailerObj.getName();
					fieldValues.put("retailer", retailer);

					MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getMetadataService();
					RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRepositoryService();

					FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
							.getFormService();

					String processName = "repair_standby";

					String dataset = "BizLog";
					ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset)
							.dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();

					ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
							.deploymentId(deployment.getDeploymentId()).singleResult();

					FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(), false);
					GroupDescription groupDescription = null;
					while ((groupDescription = formFillerUtil.goToNextGroup(fieldValues)) != null) {
						if (log.isDebugEnabled()) {
							log.debug("Current groupDescription:" + groupDescription);
							System.out.println("Current groupDescription:" + groupDescription);
						}

						if (groupDescription.isLastGroup()) {
							break;
						}
					}

					ProcessInstance processInstance = (ProcessInstance) formFillerUtil
							.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
								@Override
								public ProcessInstance submitForm(Map<String, String> fieldValues) {
									return formService.submitStartFormData(definition.getId(), fieldValues);
								}
							});
					String processId = processInstance.getProcessInstanceId();
					RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines
							.getDefaultProcessEngine())).getRuntimeService();

					String ticketNo = String.valueOf(runtimeService.getVariable(processId, "rlTicketNo"));

					restResult.setMessage(ticketNo);
					List<String> ticketNoArr = new ArrayList<>();
					ticketNoArr.add(ticketNo);
					restResult.setTicketNo(ticketNoArr);
					restResult.setMessage("Ticket created successfully");
					restResult.setSuccess(true);
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								ReportPojo pojo = new ReportPojo(processId);
								pojo.sentToReportServer(pojo);
								TicketStatusHistoryClass.insertIntoHistoryTable(runtimeService.getVariables(processId),
										processId, "TICKET_CREATED");
							} catch (Exception e) {
								log.debug("ERROE WHILE SEND DATA TO REPOST SERVER" + e);

							}
						}
					}).start();
				}
				restResult.setSuccess(false);
			}
			restResult.setSuccess(false);
		} catch (Exception e) {
			restResult.setMessage("ERROR " + e);
			restResult.setSuccess(false);
		}
		return restResult;

	}

	private boolean repairStandBySecondaryValidation(RestExternalResult restResult, String retailerId, String apiToken,
			Map<String, String> fieldValues) {

		boolean retrnValue = true;
		StringBuilder errorMeesage = new StringBuilder("");
		TicketFieldsValidationImp fieldsValidationImp = new TicketFieldsValidationImp(fieldValues, retailerId);

		if (!fieldsValidationImp.validateRetailerId()) {
			errorMeesage.append("Retailer is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerAPIToken(apiToken)) {
			errorMeesage.append("Retailer ApiToken is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("pincode"))) {
			errorMeesage.append(fieldValues.get("pincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateBizlogServiceAbleLocation(fieldValues.get("dropLocPincode"))) {
			errorMeesage.append(fieldValues.get("dropLocPincode") + " Pincode is not Serviceable to Bizlog, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateProductBrand()) {
			errorMeesage.append(fieldValues.get("brand") + " Brand is not valid, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateReatilerCategory()) {
			errorMeesage.append(fieldValues.get("productCategory") + " Category is not valid for this Retailer, ");
			retrnValue = false;
		}
		if (!fieldsValidationImp.validateRetailerNatureOfComplaint()) {
			errorMeesage.append(fieldValues.get("natureOfComplaint") + " Service is not valid for this Retailer, ");
			retrnValue = false;
		}

		restResult.setSuccess(retrnValue);
		restResult.setMessage(errorMeesage.toString());
		return retrnValue;

	}

	private boolean repairStandByPrimaryValidation(RestExternalResult restResult, String retailerId, String apiToken,
			String queryJSON) {
		StringBuilder finalErrorMeesage = new StringBuilder("");

		JSONObject fieldValues = null;
		String jsonNameerrorMessage = "";

		boolean value = true;
		boolean jsonFormatvalue = true;
		String productNameerrorMessage = "";
		String modelerrorMessage = "";
		String productCodeerrorMessage = "";
		String branderrorMessage = "";
		String identificationNomodelerrorMessage = "";
		String productCategoryerrorMessage = "";

		try {
			fieldValues = new JSONObject(queryJSON);
		} catch (Exception e) {
			jsonNameerrorMessage = "JSON format error, ";
			finalErrorMeesage.append(jsonNameerrorMessage);
			jsonFormatvalue = false;
			value = false;

		}
		if (jsonFormatvalue) {
			value = checkForFieldsInLoop(fieldValues, value, finalErrorMeesage);

		} else {
			// error in json format
		}

		if (value) {

		} else {
			if (value) {

			} else {
				restResult.setApiToken(null);
				restResult.setSuccess(false);
				restResult.setMessage(finalErrorMeesage.toString() + "");
			}
		}
		return value;
	}

	private boolean checkForFieldsInLoop(JSONObject fieldValues, boolean value, StringBuilder finalErrorMeesage) {

		ArrayList<String> fV = new ArrayList<>();
		fV.add("consumerName");
		// fV.add("consumerComplaintNumber");
		fV.add("addressLine1");
		fV.add("addressLine2");
		fV.add("city");
		fV.add("pincode");
		fV.add("telephoneNumber");
		// fV.add("retailerPhoneNo");
		// fV.add("alternateTelephoneNumber");
		fV.add("emailId");
		fV.add("orderNumber");
		// fV.add("dateOfPurchase");
		// fV.add("dateOfComplaint");
		fV.add("natureOfComplaint");
		fV.add("isUnderWarranty");
		fV.add("brand");
		fV.add("productCategory");
		fV.add("productName");
		// fV.add("productCode");
		fV.add("model");
		fV.add("identificationNo");
		fV.add("dropLocation");
		fV.add("dropLocAddress1");
		fV.add("dropLocAddress2");
		fV.add("dropLocCity");
		fV.add("dropLocState");
		fV.add("dropLocPincode");
		fV.add("dropLocContactPerson");
		fV.add("dropLocContactNo");
		// fV.add("dropLocAlternateNo");
		fV.add("physicalEvaluation");
//		fV.add("IMEINoOfStandByDevice");
		// fV.add("TechEvalRequired");

		if (fieldValues.getString("productCategory").equals("Mobile")) {
			value = false;
			finalErrorMeesage.append("use catagory as : mobile ");
		}
		for (String field : fV) {
			try {
				String fieldValue = fieldValues.getString(field);
				if (fieldValue.equals(null) || fieldValue.equals("") || fieldValue.equals("null")) {
					value = false;
					finalErrorMeesage.append(field + " should not be empty or null, ");
				}

			} catch (Exception e) {
				finalErrorMeesage.append(field + " field is missing, ");
				value = false;
			}
		}

		return value;
	}

}
