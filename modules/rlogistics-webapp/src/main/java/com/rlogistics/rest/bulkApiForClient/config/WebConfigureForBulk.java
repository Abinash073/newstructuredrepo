package com.rlogistics.rest.bulkApiForClient.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.MappedInterceptor;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Configuring WebApp for bulk
 *
 * @author Adarshk[
 */
@Configuration
@EnableRetry
@PropertySource(value = "classpath:api.properties", ignoreResourceNotFound = true)

public class WebConfigureForBulk implements WebMvcConfigurer {

    @Value(value = "${base_bulk_uri}")
    String baseBulkUri;


    private HandlerInterceptor handlerInterceptor;

    private ObjectMapper mapper;

    @Autowired
    public WebConfigureForBulk(@Qualifier("BulkInterceptor") HandlerInterceptor handlerInterceptor,
                               ObjectMapper mapper) {
        this.handlerInterceptor = handlerInterceptor;
        this.mapper = mapper;
    }

    /**
     * configuring mapper to not add any null property in response!
     */
    @PostConstruct
    void init() {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    /**
     * Added jackson to serialize response in json for application/json
     *
     * @param converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(0, new MappingJackson2HttpMessageConverter(mapper));
    }


    /**
     * Added Bulk interceptor
     *
     * @return
     */
    @Bean
    public MappedInterceptor mappedBulkInterceptor() {
        return new MappedInterceptor(new String[]{baseBulkUri + "/**"}, handlerInterceptor);
    }

}

