package com.rlogistics.rest.bulkApiForClient.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
@Builder
public class SuccessResponse {

    private final String _timestamp = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Kolkata")).format(DateTimeFormatter.ofPattern("MM-dd-yyyy 'at' hh:mma z"));
    @Builder.Default
    @Value("${bulk_api_version}")
    private final int __v = 1;
    @Builder.Default
    private String client = "N/A";
    private Object data;
}
