package com.rlogistics.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.customqueries.MasterCategoryResult;
import com.rlogistics.customqueries.ProductCustomQueriesMapper;
import com.rlogistics.http.RLogisticsResource;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.RetailerProcessField;
import com.rlogistics.rest.FormFillerUtil.GroupDescription;
import com.rlogistics.ui.CSVDataProcessor;

@RestController
public class ProcessResource extends RLogisticsResource{
	private static Logger log = LoggerFactory.getLogger(ProcessResource.class);

	@RequestMapping(value="/process/cleanup-old-processes", method = RequestMethod.POST, produces = "application/json")
	public Map<String,Long> cleanupOldProcesses(HttpServletRequest httpRequest,HttpServletResponse httpResponse){
		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMetadataService();
		RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getRepositoryService();
		
		Map<String,String> dsVersionByName = metadataService.getCurrentDsVersions();
		
		List<ProcessDeployment> deployments = metadataService.createProcessDeploymentQuery().list();
		
		Map<String,Long> retval = new LinkedHashMap<>();
		
		for(ProcessDeployment deployment:deployments){
			String dsVersion = dsVersionByName.get(deployment.getDataset());
			
			if(!retval.containsKey(deployment.getName())){
				retval.put(deployment.getName(),0L);
			}
			
			if(dsVersion.equals(deployment.getDsVersion())){
				log.info("Keeping deployment " + deployment.getId() + ":" + deployment.getName() + ":" + deployment.getDsVersion());
			} else {
				log.warn("Removing deployment " + deployment.getId() + ":" + deployment.getName() + ":" + deployment.getDsVersion());

				metadataService.deleteProcessDeployment(deployment.getId());
				try{
					repositoryService.deleteDeployment(deployment.getDeploymentId(), true);
				} catch(Exception ex){
					log.error("Error while removing deployment:" + deployment.getDeploymentId(),ex);
				}
				
				retval.put(deployment.getName(),retval.get(deployment.getName())+1);
			}
		}
		
		return retval;
	}

	@RequestMapping(value="/process/list-deployed-processes", method = RequestMethod.POST, produces = "application/json")
	public List<ProcessDeploymentData> listDeployedProcesses(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(value="dataset", required=false,defaultValue="") String dataset){
		if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
		
		List<ProcessDeploymentData> retval = new ArrayList<>();
		
		MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMetadataService();

		Map<String,String> currentDsVersions = metadataService.getCurrentDsVersions();
		List<String> datasets = new ArrayList<>();
		if(dataset == null || (dataset.equals(""))){
			datasets.addAll(currentDsVersions.keySet());
		} else {
			
		}
		
		for(String dataset2:datasets){
			List<ProcessDeployment> deployments = metadataService.createProcessDeploymentQuery().dataset(dataset2).dsVersion(currentDsVersions.get(dataset2)).orderByName().asc().list();
			for(ProcessDeployment deployment:deployments){
				retval.add(new ProcessDeploymentData(deployment));
			}
		}
		
		return retval;
	}

	@RequestMapping(value = "/process/{deploymentId}/command/firstgroup", produces = "application/json")
	public GroupDescription getFirstStartGroup(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("deploymentId") String deploymentId) {
		return getStartGroup(httpRequest,httpResponse,deploymentId,null,null,true);
	}
	
	@RequestMapping(value = "/process/{deploymentId}/{fillSessionId}/command/nextgroup", produces = "application/json")
	public GroupDescription getNextStartGroup(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("deploymentId") String deploymentId,@PathVariable("fillSessionId") String fillSessionId,@RequestParam("fieldValues") String fieldValues) {
		return getStartGroup(httpRequest,httpResponse,deploymentId,fillSessionId,parseMap(fieldValues),true);
	}
	
	@RequestMapping(value = "/process/{deploymentId}/{fillSessionId}/command/currentgroup", produces = "application/json")
	public GroupDescription getCurrentStartGroup(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("deploymentId") String deploymentId,@PathVariable("fillSessionId") String fillSessionId) {
		return getStartGroup(httpRequest,httpResponse,deploymentId,fillSessionId,null,false);
	}
		
	private GroupDescription getStartGroup(HttpServletRequest httpRequest,HttpServletResponse httpResponse,String deploymentId,String fillSessionId,Map<String,String> fieldValues,boolean next) {
		
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
				FormFillerUtil formFillerUtil = null;
				
				if(fillSessionId != null){
					formFillerUtil = new FormFillerUtil(fillSessionId);
				} else {
					formFillerUtil = new FormFillerUtil(deploymentId,false);
				}
				
				GroupDescription groupDescription = next ? formFillerUtil.goToNextGroup(fieldValues) : formFillerUtil.getCurrentGroup();
								
				return groupDescription;
		} catch(Exception ex){
			log.error("Exception during getStartEventGroup(next=" + next + ")", ex);
			
			GroupDescription groupDescription = null;
			
			if(next){
				groupDescription = getStartGroup(httpRequest, httpResponse, deploymentId, fillSessionId, null,false);
				
				if(ex instanceof ScriptingErrorsException){
					groupDescription.getMessages().addAll(((ScriptingErrorsException)ex).getErrorReport().getMessages());
				} else {
					groupDescription.getMessages().add(ex.getMessage());
				}
			} else {
				httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);				
			}
			
			return groupDescription;
		}
	}

	@RequestMapping(value = {"/process/{deploymentId}/{fillSessionId}/command/start-process"}, produces = "application/json")
	public ProcessStartResult startProcess(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("deploymentId") String deploymentId,@PathVariable("fillSessionId") String fillSessionId,@RequestParam(value="fieldValues",required=false) String fieldValues) {
		try {

			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
	
			List<String> notifications = null;
			
			if(fieldValues != null){
				GroupDescription groupDescription = getStartGroup(httpRequest,httpResponse,deploymentId,fillSessionId,parseMap(fieldValues),true);
				
				if(!groupDescription.getMessages().isEmpty()){
					log.error("Some validation errors. Cannot complete. Error:" + groupDescription.getMessages().get(0));
					return new ProcessStartResult(groupDescription);
				}
				
				if(!groupDescription.getNotifications().isEmpty()){
					notifications = groupDescription.getNotifications();
				}
			}
			
			FormFillerUtil formFillerUtil = new FormFillerUtil(fillSessionId);
			
			FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
					.getFormService();
			RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getRepositoryService();

			ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).singleResult();

			submitStartForm(formService, definition, formFillerUtil);
			
			return new ProcessStartResult(notifications);
		} catch(Exception ex){
			log.error("Exception during startProcess", ex);
			httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
	}

	public void submitStartForm(FormService formService, ProcessDefinition definition,FormFillerUtil formFillerUtil) {
		ProcessInstance processInstance = (ProcessInstance) formFillerUtil.submitForm(new FormFillerUtil.FormSubmissionAction<ProcessInstance>() {
			@Override
			public ProcessInstance submitForm(Map<String, String> fieldValues) {
				return formService.submitStartFormData(definition.getId(), fieldValues);
			}
		});
		
		/* TODO: Leads to bugs -- hence disabling. But will need to be fixed when people start using start forms elaborately
		CommandExecutor commandExecutor = ((RLogisticsSpringProcessEngineConfiguration)(ProcessEngines.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor();
		commandExecutor.execute(new Command<Boolean>() {
			@Override
			public Boolean execute(CommandContext commandContext) {
				execution.setVariables(formFillerUtil.getVariables());
				return true;
			}
		});*/
	}

	@RequestMapping(value = "/process/{processId}/get-my-task", method = RequestMethod.POST, produces = "application/json")
	public RestResponse getMyTask(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@PathVariable("processId") String processId) {
		List<Task> tasks = new ArrayList<Task>(); 
		RestResponse restResponse = new RestResponse();
		RuntimeService runtimeService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine()))
				.getRuntimeService();
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){
				httpResponse.setStatus(401);
				throw new Exception ("rltoken null/Invalid");
			}

			tasks = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getTaskService()
				.createTaskQuery().processInstanceId(processId).taskAssignee(Authentication.getAuthenticatedUserId())
				.orderByTaskCreateTime().desc().list();
			System.out.println("tasks value : "+tasks);

			if(tasks.size()>0){
				TaskResult taskResult = new TaskResult(tasks.get(0));
				log.debug("taskResult value:"+taskResult);
				if(taskResult.getCategory() != null) {
					restResponse.setResponse(taskResult);
				} else {
					String category = String.valueOf(runtimeService.getVariable(processId, "productCategory"));
					String retailerName = String.valueOf(runtimeService.getVariable(processId, "retailer"));
					taskResult.setCategory(category);
					taskResult.setRetailer(retailerName);
					log.debug("Retailer Name : "+retailerName);
					restResponse.setResponse(taskResult);
					log.debug("taskResult response : "+taskResult);
				}
				
			} else {
				Map<String, String> result = new HashMap<String, String>();
				restResponse.setResponse(result);
				restResponse.setMessage("");
			}

		} catch(Exception ex){
			log.error("Exception during get-my-task", ex);
			httpResponse.setStatus(500);
			restResponse.setMessage(ex.getMessage());
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/process/upload-without-retailer", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProcessCommands(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			String retailerId = null;
			return new ProcessCommandProcessor().processUpload(file.getInputStream(),retailerId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}

	@RequestMapping(value = "/process/upload", method = RequestMethod.POST, produces = "application/json")
	public UploadProcessCommandsResult uploadProcessCommandsWithRenaming(HttpServletRequest httpRequest,HttpServletResponse httpResponse,@RequestParam(required = true, value = "file") MultipartFile file, @RequestParam(value = "retailerId") String retailerId) {
		try {
			if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}
			return new ProcessCommandProcessorWithRenaming(retailerId).processUpload(file.getInputStream(),retailerId);
		} catch (IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	protected class ProcessCommandProcessor extends CSVDataProcessor {

		private static final String BEGIN = "BEGIN";

		public UploadProcessCommandsResult processUpload(InputStream is,String retailerId) {
			MetadataService metadataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMetadataService();
			RepositoryService repositoryService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getRepositoryService();
			FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getFormService();
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();
			
			UploadProcessCommandsResult retval = new UploadProcessCommandsResult();
			
			try {
				BufferedReader bis = new BufferedReader(new InputStreamReader(is));

				String record = null;
				int recordNo = 0;
				int insertCount = 0;
				List<String> orderedFields = new ArrayList<String>();
				while ((record = bis.readLine()) != null) {
					recordNo++;

					String[] recordFields = parseLine(record, recordNo);

					if (recordFields == null || recordFields.length == 0)
						continue;

					if(recordNo == 1){
						
						/* Header record*/
						/* For now assume first three to be command,dataset,process*/
						for(int i = 0; i< recordFields.length;++i){
							orderedFields.add(recordFields[i]);
						}
						if(log.isDebugEnabled()){
							log.debug("HEADER:" + recordNo + ":" + record);
							for (String f : orderedFields) {
								log.debug("------------" + f);
							}
						}
						continue;
					}
					
					String op = BEGIN;

					if(log.isDebugEnabled()){
						log.debug("PROCESSING:" + recordNo + ":" + record);
						for (String f : recordFields) {
							log.debug("------------" + f);
						}
					}

					/*if(recordFields.length < 3){
						retval.getErrorMessages().add("Ignoring line " + recordNo + ":" + record + ". Reason: less than 3 fields. The first three fields are expected to be command,dataset,process.");
						continue;
					}*/
					
					if (recordFields.length > 1) {
						String dataset = "BizLog";
						String processName = null;
						
						final Map<String,String> fieldValuesPreFilter = new HashMap<String, String>();
						
						for(int i = 0;i < recordFields.length;++i){
							fieldValuesPreFilter.put(orderedFields.get(i), recordFields[i]);
						}

						Map<String,String> fieldValues = filterFieldValues(fieldValuesPreFilter);
						String productCategory = fieldValues.get("productCategory").toLowerCase();
						MasterCategoryResult masterCategoryResult = com.rlogistics.customqueries.impl.CustomQueriesService.ProductCustomQueriesMapper.getMasterCategory(productCategory);
						processName = masterCategoryResult.getProcessFlow();
						if(retailerId != null) {
							Retailer retailerObj = masterdataService.createRetailerQuery().id(retailerId).singleResult();
							String retailer = retailerObj.getName();
							fieldValues.put("retailer", retailer);
						}
						ProcessDeployment deployment = metadataService.createProcessDeploymentQuery().dataset(dataset).dsVersion(metadataService.getCurrentDsVersion(dataset)).name(processName).singleResult();
						
						ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getDeploymentId()).singleResult();

						FormFillerUtil formFillerUtil = new FormFillerUtil(deployment.getDeploymentId(),false);
						GroupDescription groupDescription = null;
						while((groupDescription  = formFillerUtil.goToNextGroup(fieldValues)) != null){
							if(log.isDebugEnabled()){
								log.debug("Current groupDescription:" + groupDescription);
							}
							
							if(groupDescription.isLastGroup()){
								break;
							}
						}
					
						submitStartForm(formService, definition, formFillerUtil);
						
						insertCount ++;
					} else {
//						retval.getErrorMessages().add("Ignoring line " + recordNo + ":" + record + ". Reason: Empty Records");
					}
					retval.setNoOfRecordsInserted(insertCount);
				}
			} catch (IOException ex) {
				throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
			}
			
			return retval;
		}
		
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {
			return fieldValues;
		}
	}
	
	protected class ProcessCommandProcessorWithRenaming extends ProcessCommandProcessor {
		private Map<String, String> canonicalFieldByRetailerField;
		
		public ProcessCommandProcessorWithRenaming(String retailerId){
			MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
					.getDefaultProcessEngine())).getMasterdataService();

			List<RetailerProcessField> retailerProcessFieldList = masterdataService
					.createRetailerProcessFieldQuery().retailerId(retailerId).list();

			canonicalFieldByRetailerField = new HashMap<String, String>();
			for (RetailerProcessField retailerProcessField : retailerProcessFieldList) {
				canonicalFieldByRetailerField.put(retailerProcessField.getRetailerField(),
						retailerProcessField.getCanonicalField());
			}
		}
		
		@Override
		protected Map<String, String> filterFieldValues(Map<String, String> fieldValues) {

			Map<String, String> translatedFieldValues = new HashMap<String, String>();
			for (String incomingField : fieldValues.keySet()) {
				translatedFieldValues.put(canonicalFieldByRetailerField.get(incomingField),
						fieldValues.get(incomingField));
			}

			return translatedFieldValues;
		}
	}
}
