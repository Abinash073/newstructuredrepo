package com.rlogistics.rest.services.excelWriter;

import com.rlogistics.customqueries.HistoricQueryData;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Slf4j
public class ReportExcelUtilImpl implements ReportExcelUtil {
    //Default Format
    private static final String datePattern = "MM/dd/yyyy";
    private static final String timePattern = "HH:mm:ss";
    private static final String dateAndTimePattern = "MM/dd/yyyy HH:mm:ss";
    ByteArrayOutputStream outputStream;
    XSSFWorkbook wb;
    XSSFSheet sheet;
    Cell cell;
    XSSFCellStyle style;
    XSSFFont font;
    Row firstRow;
    int cel;
    LinkedHashMap<String, String> header;
    byte[] bytes;
    //TODO  append date
    int rownum;
    int sNo;

    @Override
    public ReportExcelUtil setStyling() throws IOException {
        try {
            font.setFontHeightInPoints((short) 13);
            font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);


        } catch (Exception e) {
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    public ReportExcelUtil initExcel(String sheetName) throws IOException {
        //As we are using bean <Singleton scope> initialize everything here
        wb = new XSSFWorkbook();
        this.font = wb.createFont();
        this.style = wb.createCellStyle();
        outputStream = new ByteArrayOutputStream();
        rownum = 1;
        sNo = 1;
        cel = 0;
        //TODO modify
        try {
            sheet = wb.createSheet(sheetName);
        } catch (Exception e) {
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    public ReportExcelUtil createHeader(LinkedHashMap<String, String> header) throws IOException {
        try {
            //Header Row Style
            XSSFCellStyle headerStyle = style;
            headerStyle.setFillBackgroundColor(IndexedColors.WHITE.getIndex());

            //Header Cell Style
            XSSFFont xssfFont = wb.createFont();
            xssfFont.setColor(IndexedColors.BLACK.getIndex());
            xssfFont.setBold(true);

            headerStyle.setFont(xssfFont);

            this.header = header;
            firstRow = sheet.createRow(0);
            firstRow.setRowStyle(headerStyle);

            header.forEach((key, value) -> {
                log.debug(key + "---------" + value);
                Cell c = firstRow
                        .createCell(cel++);
                c.setCellValue(key);
                c.setCellStyle(style);
            });
        } catch (Exception e) {
            log.debug("Error in adding header" + e);
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    public ReportExcelUtil insertData(List<?> data, String type) throws IOException {
        try {
            if (type.equals("completed")) {

                //TRY to generify this method more :(

                ((List<HistoricQueryData>) data).stream()
                        .forEach(this::insertIncell);
            }
        } catch (Exception e) {
            log.debug("error" + e);
            wb.write(outputStream);
        }
        return this;
    }

    @Override
    public ReportExcelUtil saveExcel() throws IOException {

        wb.write(outputStream);
        this.bytes = outputStream.toByteArray();
        outputStream.flush();
        return this;
    }

    @Override
    public byte[] getExcel() {
        return bytes;
    }

    private void insertIncell(HistoricQueryData data) {
        int colNo = 0;

        //New Row
        Row row = sheet.createRow(rownum++);

        //Create S. No cell
        cell = row.createCell(colNo++);
        cell.setCellValue(sNo);
        sNo++;

        //Retailer
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRetailer());

        //Service Type
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getNatureOfComplaint());

        //Ticket Creation Date
        cell = row.createCell(colNo++);
        cell.setCellValue(this.changeDate(data.getTicketCreationDate(), datePattern));

        //Ticket Creation Time
        cell = row.createCell(colNo++);
        cell.setCellValue(this.changeDate(data.getTicketCreationDate(), timePattern));

        //Consumer Name
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getConsumerName());

        //Phone No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getTelephoneNumber());

        //Process Id
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getProcessId());

        //Consumer Request No
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getConsumerComplaintNumber());

        //Ticket No

        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRlTicketNo());

        //Product
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getProduct());

        //Product Category
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getProductCategory());

        //Code
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getProductCode());

        //Brand
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getBrand());

        //Model
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getModel());

        //Pick-Up location
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getAddressLine1() +
                "," + data.getAddressLine2()
                + data.getCity() + "," +
                data.getPincode()
        );


        //Drop Location
        cell = row.createCell(colNo++);
        cell.setCellValue(getDropLocation(data));

        //Mode Of Payment

        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRlModeOfPayment());

        //Status
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRlTicketStatus());

        //City
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRlReportingCity());

        //Involved User
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getRlInvolvedUser());

        //Offer Amount
        cell = row.createCell(colNo++);
        cell.setCellValue(data
                .getRlValueOffered().equals("") || data.getRlValueOffered().equals(" ") || data.getRlValueOffered().equals("N/A")
                ? data.getMaxValueToBeOffered()
                : data.getRlValueOffered());

        //Drop Contact Person
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getDropLocContactPerson());

        //Imei Number
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getIdentificationNo());

        //Ticket Closer Date
        cell = row.createCell(colNo++);
        cell.setCellValue(changeDbDate(data.getTicketCloserDate(), dateAndTimePattern));


        //Drop Location City
        cell = row.createCell(colNo++);
        cell.setCellValue(data.getDropLocCity());


    }

    //From long
    private synchronized String changeDate(String date, String timePattern) {
        try {
            Long l = Long.parseLong(date);
            Timestamp ts = new Timestamp(l);
            DateFormat formatter = new SimpleDateFormat(timePattern);
            Date d = new Date(ts.getTime());
            return formatter.format(d);
        } catch (Exception e) {
            return "N/A";
        }
    }

    //From ending date format stored in db to string
    private String changeDbDate(Object date, String format) {
        try {
            DateTime dt = DateTime.parse(String.valueOf(date), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS"));
            return dt.toString(format);
        } catch (Exception e) {
            return "N/A";
        }

    }

    //Drop location related logic
    private String getDropLocation(HistoricQueryData data) {

        StringBuffer buffer = new StringBuffer();

        if (!data.getDropLocAddress1().equals("") && !data.getDropLocAddress1().equals("N/A"))
            buffer.append(data.getDropLocAddress1()).append(",");

        if (!data.getDropLocCity().equals("") && !data.getDropLocCity().equals("N/A")) {
            buffer.append(data.getDropLocCity()).append(",");
        }

        if (!data.getRlReturnLocationAddress().equals("") && !data.getRlReturnLocationAddress().equals("N/A"))
            buffer.append(data.getRlReturnLocationAddress()).append(",");

        if (buffer.toString().trim().equals("")) buffer.append("N/A");

        return buffer.toString().trim();
    }


}
