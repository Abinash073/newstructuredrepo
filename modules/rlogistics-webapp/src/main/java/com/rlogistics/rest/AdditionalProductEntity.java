package com.rlogistics.rest;

public class AdditionalProductEntity {
    String id;

    String retailerName;

    String mainTicketRef;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getMainTicketRef() {
        return mainTicketRef;
    }

    public void setMainTicketRef(String mainTicketRef) {
        this.mainTicketRef = mainTicketRef;
    }

    public String getSubTicketRef() {
        return subTicketRef;
    }

    public void setSubTicketRef(String subTicketRef) {
        this.subTicketRef = subTicketRef;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public int getIdentificationNo() {
        return identificationNo;
    }

    public void setIdentificationNo(int identificationNo) {
        this.identificationNo = identificationNo;
    }

    public String getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getDropLocAddress1() {
        return dropLocAddress1;
    }

    public void setDropLocAddress1(String dropLocAddress1) {
        this.dropLocAddress1 = dropLocAddress1;
    }

    public String getDropLocAddress2() {
        return dropLocAddress2;
    }

    public void setDropLocAddress2(String dropLocAddress2) {
        this.dropLocAddress2 = dropLocAddress2;
    }

    public String getDropLocCity() {
        return dropLocCity;
    }

    public void setDropLocCity(String dropLocCity) {
        this.dropLocCity = dropLocCity;
    }

    public String getDropLocState() {
        return dropLocState;
    }

    public void setDropLocState(String dropLocState) {
        this.dropLocState = dropLocState;
    }

    public int getDropLocPincode() {
        return dropLocPincode;
    }

    public void setDropLocPincode(int dropLocPincode) {
        this.dropLocPincode = dropLocPincode;
    }

    public String getDropLocContactPerson() {
        return dropLocContactPerson;
    }

    public void setDropLocContactPerson(String dropLocContactPerson) {
        this.dropLocContactPerson = dropLocContactPerson;
    }

    public String getDropLocContactNo() {
        return dropLocContactNo;
    }

    public void setDropLocContactNo(String dropLocContactNo) {
        this.dropLocContactNo = dropLocContactNo;
    }

    public String getPhotosAfter() {
        return photosAfter;
    }

    public void setPhotosAfter(String photosAfter) {
        this.photosAfter = photosAfter;
    }

    public String getPhotographsBefore() {
        return photographsBefore;
    }

    public void setPhotographsBefore(String photographsBefore) {
        this.photographsBefore = photographsBefore;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getEvaluationTicketReferance() {
        return evaluationTicketReferance;
    }

    public void setEvaluationTicketReferance(String evaluationTicketReferance) {
        this.evaluationTicketReferance = evaluationTicketReferance;
    }

    public int getPhotosBeforeCount() {
        return photosBeforeCount;
    }

    public void setPhotosBeforeCount(int photosBeforeCount) {
        this.photosBeforeCount = photosBeforeCount;
    }

    public int getPhotosAfterCount() {
        return photosAfterCount;
    }

    public void setPhotosAfterCount(int photosAfterCount) {
        this.photosAfterCount = photosAfterCount;
    }

    public String getFeAssigned() {
        return feAssigned;
    }

    public void setFeAssigned(String feAssigned) {
        this.feAssigned = feAssigned;
    }

    public String getBoxNumber() {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber) {
        this.boxNumber = boxNumber;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getExtraVariables() {
        return extraVariables;
    }

    public void setExtraVariables(String extraVariables) {
        this.extraVariables = extraVariables;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    String subTicketRef;

    String brand;

    String productCategory;

    String productName;

    String productCode;

    String model;

    String problemDescription;

    int identificationNo;

    String dropLocation;

    String dropLocAddress1;

    String dropLocAddress2;

    String dropLocCity;

    String dropLocState;

    int dropLocPincode;

    String dropLocContactPerson;

    String dropLocContactNo;

    String photosAfter;

    String photographsBefore;

    String retailerId;

    String status;

    String barcode;

    String evaluationTicketReferance;

    int photosBeforeCount;

    int photosAfterCount;

    String feAssigned;

    String boxNumber;

    String processId;

    String createdBy;

    String extraVariables;

    int revision;
}
