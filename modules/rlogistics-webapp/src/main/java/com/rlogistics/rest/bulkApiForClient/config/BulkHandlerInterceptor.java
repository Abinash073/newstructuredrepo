package com.rlogistics.rest.bulkApiForClient.config;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.rest.bulkApiForClient.exceptions.UserNotValidException;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component(value = "BulkInterceptor")
@DependsOn("masterdataService")
@Slf4j
@var
public class BulkHandlerInterceptor implements HandlerInterceptor {
    private MasterdataService masterdataService;

    @Autowired
    public BulkHandlerInterceptor(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    /**
     * @param request
     * @param response
     * @param handler
     * @return boolean
     * @apiNote Validating request before it get mapped to its handler method
     * Only way this might fail is if there is duplicate retailerId inside persistence.
     * @ImplBy Adarsh
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        var id = request.getHeader("retailerId");
        var apiToken = request.getHeader("apiToken");
        if (id == null || apiToken == null) throw new UserNotValidException("Auth Error! Client info is missing.");
        var retailer = masterdataService.createRetailerQuery().id(id).singleResult();
        if (retailer != null) {
            if (retailer.getApiToken().equals(apiToken)) {
                return true;
            } else {
                throw new UserNotValidException("Auth Error! Client validation Failed.");
            }
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            throw new UserNotValidException("Auth Error! Client not found.");
        }


    }
}
