package com.rlogistics.rest.bulkApiForClient.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.BizlogServiceType;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.BizlogServiceablePincode;
import com.rlogistics.rest.bulkApiForClient.customValidators.anotation.RetailerName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Valid
@SuppressWarnings("ALL")
public
class PickUpDetails {

    @NotBlank(message = "Consumer Name cannot be empty")
    @NotNull(message = "Consumer Name cannot be null")
    @JsonProperty("consumerName")
    @Pattern(regexp = "^[A-Za-z\\s]+[.]?[A-Za-z\\s]*$", message = "Name is not in valid format!")
    @Size(min = 4, message = "Name is too small! Min : 4")
    @Size(max = 50, message = "Name is too large! Max: 50")
    private String consumerName;


    @NotBlank(message = "Consumer Complaint Number Cannot be empty")
    @JsonProperty("consumerComplaintNumber")
    @Size(max = 50, message = "Complaint number is too large! max : 50")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Not in Valid format!")
    private String consumerComplaintNumber;

    @NotBlank(message = "can not be Empty!")
    @NotNull(message = "can not be Null!")
    @Size(max = 100, message = "Address is to Long! max : 100")
    @JsonProperty("addressLine1")
    private String addressLine1;

    @JsonProperty("addressLine2")
    private String addressLine2;

    @JsonProperty("city")
    private String city;

    @JsonProperty("pincode")
    @Pattern(regexp = "^[1-9][0-9]{5}$", message = "Pin code is in invalid format!")
    @BizlogServiceablePincode
    private String pincode;


    @JsonProperty("telephoneNumber")
    @Pattern(regexp = "^[6-9]\\d{9}$", message = "Customer Number is not valid!")
    private String telephoneNumber;

    @Pattern(regexp = "^[6-9]\\d{9}$", message = " Retailer Number is not valid!")
    @JsonProperty("retailerPhoneNo")
    private String retailerPhoneNo;

    @JsonProperty("emailId")
    @Email(message = "Email Id is not valid!")
    private String emailId;

    @JsonProperty("orderNumber")
    private String orderNumber;

    @JsonProperty("dateOfPurchase")
    private String dateOfPurchase;

    @JsonProperty("dateOfComplaint")
    private String dateOfComplaint;

    @JsonProperty("natureOfComplaint")
    @BizlogServiceType
    private String natureOfComplaint;

    @JsonProperty("retailerName")
    @NotNull(message = "Client Name cannot be null!")
    @NotBlank(message = "Client Name is required!")
    @RetailerName
    private String retailerName;

}
