package com.rlogistics.rest.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.customqueries.FeDisatnceCustomQueriesMapper;
import com.rlogistics.customqueries.LastDistanceDetails;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.persistence.entity.LatLongEntity;
import com.rlogistics.rest.Component.DistanceBetweenTwoLatLong;
import com.rlogistics.rest.services.util.RuntimeServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Service which will be storing lat long details inside database
 *
 * @author Adarsh
 */
@Slf4j
@Service
@DependsOn({"googleApiDistanceMatrix", "runtimeServiceUtil"})
public class FeTrackingDataProcessor {

    private MasterdataService masterdataService;
    private ProcessEngineConfigurationImpl configuration;
    private DistanceBetweenTwoLatLong distanceBetweenTwoLatLong;
    private RuntimeServiceUtil runtimeServiceUtil;


    private ObjectMapper mapper;

    @Autowired
    public FeTrackingDataProcessor(MasterdataService masterdataService,
                                   ProcessEngineConfigurationImpl configuration,
                                   DistanceBetweenTwoLatLong distanceBetweenTwoLatLong,
                                   RuntimeServiceUtil runtimeServiceUtil,
                                   ObjectMapper mapper) {
        this.masterdataService = masterdataService;
        this.configuration = configuration;
        this.distanceBetweenTwoLatLong = distanceBetweenTwoLatLong;
        this.runtimeServiceUtil = runtimeServiceUtil;
        this.mapper = mapper;
    }


    @PostConstruct
    void init() {
        configMapper();
    }

    /**
     * Takes new distance details when a ticket gets closed and stores inside db
     *
     * @param newDistanceDetails New lat long
     */
    public void storeData(Map<String,String> newDistanceDetails) {

        try (SqlSession s = configuration.getSqlSessionFactory().openSession()) {
            LatLongEntity latLongEntity = mapper.convertValue(newDistanceDetails, LatLongEntity.class);
            FeDisatnceCustomQueriesMapper mapper = s.getMapper(FeDisatnceCustomQueriesMapper.class);

            LastDistanceDetails lastDistanceDetails = mapper.getLastDistance(newDistanceDetails.get("feName"), getDateWithoutHour());

            latLongEntity.setTotalDistance(
                    distanceBetweenTwoLatLong
                            .getUpdatedTotalDistance(lastDistanceDetails, newDistanceDetails)
            );

            latLongEntity.setTime(getDate());
            latLongEntity.setId(UUID.randomUUID().toString());
            latLongEntity.setTicketNo(String.valueOf
                    (
                            runtimeServiceUtil.getVaribale
                                    (newDistanceDetails.get("processId"),
                                            "rlTicketNo")
                    )
            );
            masterdataService.saveLatLong(latLongEntity);


        } catch (Exception e) {
            log.debug(" Error", e);
        }


    }


    private void configMapper() {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }


    private String getDate() {
        DateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        Date date = new Date();
        return s.format(date);
    }

    private String getDateWithoutHour() {
        DateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        s.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        Date date = new Date();
        return s.format(date);
    }


}
