package com.rlogistics.rest.bulkApiForClient.customValidators.anotation;

import com.rlogistics.rest.bulkApiForClient.customValidators.anotationProcessor.RetailerNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RetailerNameValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RetailerName {
    String message() default "Client Name is not valid!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
