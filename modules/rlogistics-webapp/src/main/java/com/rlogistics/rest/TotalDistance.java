package com.rlogistics.rest;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TotalDistance {
    private BigDecimal distance;
    private String feName;
    private String feId;

}
