package com.rlogistics.rest;


import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngines;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rlogistics.master.identity.MasterdataService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.Franchiselocation;
import com.rlogistics.master.identity.Franchiselocation.FranchiselocationQuery;
import com.rlogistics.master.identity.Retailer;

@RestController
public class FranchiseLocController {
	
private static Logger log = LoggerFactory.getLogger(UserResource.class);
	
	@RequestMapping(value="/addFranchiseLoc",method=RequestMethod.POST)
	public RestResponse saveFranchiseLoc(@RequestParam( required=true,value="franchiseName") String  franchiseName ,@RequestParam( required=true,value="parentlocation") String parentlocation , 
			HttpServletResponse response,@RequestParam( required=true,value="address") String address,@RequestParam( required=true,value="contactperson") String contactperson,
			@RequestParam( required=true,value="contactno") String contactno,@RequestParam( required=true,value="emailid") String emailid) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Franchiselocation franchiselocation = masterdataService.newFranchiselocation();
		if(franchiseName!=null)
		{
			franchiselocation.setName(franchiseName);
		}
		else {
			restResponse.setMessage(franchiseName+" should not be Null");
		}
		if(parentlocation!=null)
		{
			franchiselocation.setParentlocation(parentlocation);
		}
		else {
			restResponse.setMessage(parentlocation+" should not be Null");
		}
		if(address!=null)
		{
			franchiselocation.setAddress(address);;
		}
		else {
			restResponse.setMessage(address+" should not be Null");
		}
		if(contactperson!=null)
		{
			franchiselocation.setContactperson(contactperson);
		}
		else {
			restResponse.setMessage(contactperson+" should not be Null");
		}
		if(contactno!=null)
		{
			franchiselocation.setContactno(contactno);
		}
		else {
			restResponse.setMessage(contactno+" should not be Null");
		}
		if(emailid!=null)
		{
			franchiselocation.setEmailid(emailid);
		}
		else {
			restResponse.setMessage(emailid+" should not be Null");
		}
		
		try {
			
			
			masterdataService.saveFranchiselocation(franchiselocation);
			restResponse.setMessage("Successfully Added");	
			
		}
		catch (Exception e) {
			restResponse.setMessage("Duplicate Record");
			restResponse.setResponse(200);
		}
		
		return restResponse;
		
	}
	
	
	@RequestMapping(value="/deleteFranchiseLoc/{id}",method=RequestMethod.DELETE)
	public RestResponse deleteFranchiseLoc(@PathVariable String id) {
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		
		
		try {
			Franchiselocation franchiselocation=masterdataService.createFranchiselocationQuery().id(id).singleResult();
			masterdataService.deleteFranchiselocation(franchiselocation.getId());
			restResponse.setMessage("Deleted Successfully");
			
		} catch (Exception e) {
			restResponse.setMessage(   e+"  Error in Deleting");
			
		}
		
		return restResponse;
		}
	
	@RequestMapping(value="/getFranchiseloc",method=RequestMethod.GET)
	public RestResponse getFranchiseloc(HttpServletResponse response ) {
		RestResponse restResponse=new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		try {
			List<Franchiselocation> franchiselocations=masterdataService.createFranchiselocationQuery().list();
			restResponse.setResponse(franchiselocations);
			restResponse.setSuccess(true);
			
		
			
		} catch (Exception e) {
			restResponse.setResponse(e);
			
		}
		return restResponse;
		
	}
	
	
	@RequestMapping(value="/getFranchiseloc/{id}",method=RequestMethod.GET)
	public RestResponse getFranchiseloc1(HttpServletResponse response,@PathVariable String id) {
		RestResponse restResponse=new RestResponse();
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		try {
			Franchiselocation franchiselocations=masterdataService.createFranchiselocationQuery().id(id).singleResult();
			
			String s1=franchiselocations.toString();
			log.debug(franchiselocations.getId());
			restResponse.setResponse(franchiselocations);
		
			restResponse.setSuccess(true);
			
		} catch (Exception e) {
			restResponse.setResponse(e);
			
		}
		return restResponse;
		
	}
	
	
	@RequestMapping(value="/updateFranchiseloc",method=RequestMethod.PUT)
	public RestResponse deleteFranchiseloc(HttpServletResponse response,@RequestParam( required=true) String  franchiseName ,@RequestParam( required=true) String parentlocation , 
			@RequestParam( required=true) String address,@RequestParam( required=true) String contactperson,
			@RequestParam( required=true) String contactno,@RequestParam( required=true) String emailid,@RequestParam( required=true) String id) {
     
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		RestResponse restResponse = new RestResponse();
		Franchiselocation franchiselocation = masterdataService.newFranchiselocation();
		
		if(id!=null)
		{
			franchiselocation.setId(id);
		}
		else {
			restResponse.setMessage(id+" should not be Null");
		}
		if(franchiseName!=null)
		{
			franchiselocation.setName(franchiseName);
		}
		else {
			restResponse.setMessage(franchiseName+" should not be Null");
		}
		if(parentlocation!=null)
		{
			franchiselocation.setParentlocation(parentlocation);
		}
		else {
			restResponse.setMessage(parentlocation+" should not be Null");
		}
		if(address!=null)
		{
			franchiselocation.setAddress(address);;
		}
		else {
			restResponse.setMessage(address+" should not be Null");
		}
		if(contactperson!=null)
		{
			franchiselocation.setContactperson(contactperson);
		}
		else {
			restResponse.setMessage(contactperson+" should not be Null");
		}
		if(contactno!=null)
		{
			franchiselocation.setContactno(contactno);
		}
		else {
			restResponse.setMessage(contactno+" should not be Null");
		}
		if(emailid!=null)
		{
			franchiselocation.setEmailid(emailid);
		}
		else {
			restResponse.setMessage(emailid+" should not be Null");
		}
		
		try {
			
			
			masterdataService.saveFranchiselocation(franchiselocation);
			
			restResponse.setMessage("Successfully Updated");	
			
		}
		catch (Exception e) {
			restResponse.setMessage("Error in Updating "+e);
			restResponse.setResponse(200);
		}
		
		return restResponse;
		
	}
	
	
	
}
