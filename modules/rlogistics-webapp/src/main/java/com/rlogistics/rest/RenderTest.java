package com.rlogistics.rest;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class RenderTest {
    @RequestMapping(value = "test/startFormData", method = RequestMethod.POST, produces = "application/json")
    public List<FormProperty> startForm(@RequestParam(value = "processDefinitionId") String processDefinitionId) {
        //Fetching The Form Properties to Render Outside defult engine view
        List<FormProperty> formProperties = new ArrayList<>();
        FormService formService = ((RLogisticsProcessEngineImpl) (ProcessEngines.getDefaultProcessEngine())).getFormService();
        StartFormData startFormData = formService.getStartFormData(processDefinitionId);
       // log.debug("Inside " + startFormData + startFormData.getFormProperties());
        for (int i = 0; i < startFormData.getFormProperties().size(); i++) {
            formProperties.add(startFormData.getFormProperties().get(i));
        }
        return formProperties;

    }
}
