package com.rlogistics.rest.services.util;

import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("runtimeServiceUtil")
@Scope(value = "prototype")
public class RuntimeServiceUtil {
    private RuntimeService runtimeService;

    @Autowired
    public RuntimeServiceUtil(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    public Object getVaribale(String processId,String variableName){
       return runtimeService.getVariable(processId, variableName);
    }
}
