package com.rlogistics.rest.bulkApiForClient.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Data
@Builder
public class ErrorResponse {
    private final String _timestamp = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Kolkata")).format(DateTimeFormatter.ofPattern("MM-dd-yyyy 'at' hh:mma z"));
    @Builder.Default
    private String client = "N/A";
    private String message;
    private Map<String, String> details;
    private int status;
    @Builder.Default
    @Value("${bulk_api_version}")
    private int _version = 1;

}
