package com.rlogistics.rest.services.util;

import com.rlogistics.customqueries.FeDistanceReportData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Util Related to Fe distance calculation
 */
@Service
@Slf4j
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class FeDistanceCalculatorUtil {
    /**
     * Groups data by fe name and add distances
     *
     * @param data all Fe wise distance data
     * @return grouped by Fe name and distance added
     */
    public List<FeDistanceReportData> getFeWiseCalcualtedDistance(List<FeDistanceReportData> data) {
        Map<String, List<FeDistanceReportData>> groupedData =
                data
                        .stream()
                        .collect(Collectors.groupingBy(FeDistanceReportData::getFeName, Collectors.toList()));
        return groupedData.entrySet().stream().map(Map.Entry::getValue).map(this::addData).collect(Collectors.toList());

    }

    private FeDistanceReportData addData(List<FeDistanceReportData> a) {
        BigDecimal totalDistance = new BigDecimal(0);
        for (FeDistanceReportData b : a) {
            String distance = b.getTotalDistance();
            if (distance.contains("km")) {
                totalDistance = totalDistance.add(new BigDecimal(distance.replaceAll("\\b(km)\\b", "").trim()).multiply(new BigDecimal(1000)));
            } else if (distance.contains("m")) {
                totalDistance = totalDistance.add(new BigDecimal(distance.replaceAll("\\b(m)\\b", "").trim()));
            }

        }
        FeDistanceReportData c = new FeDistanceReportData();
        c.setFeName(a.get(0).getFeName());
        c.setCity(a.get(0).getCity());
        c.setTotalDistance(totalDistance.divide(new BigDecimal(1000), 2).toPlainString());
        return c;
    }
}
