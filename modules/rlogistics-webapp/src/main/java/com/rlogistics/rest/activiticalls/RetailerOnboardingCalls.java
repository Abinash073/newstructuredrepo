package com.rlogistics.rest.activiticalls;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.RetailerOther;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Acitiviti call to check for various settings enabled by Retailer at the time of unboarding.
 * @author Adarsh
 */
@Component
@Slf4j
public class RetailerOnboardingCalls {

    private static MasterdataService masterdataService;

    @Autowired
    public RetailerOnboardingCalls(MasterdataService masterdataService) {
        RetailerOnboardingCalls.masterdataService = masterdataService;
    }

    /**
     * Check weather Packing is required or not
     * @param id Retailer Id
     * @return True or false
     */
    public static String isPackingRequired(String id) {
        log.debug(id);
        RetailerOther retailerOther = masterdataService.createRetailerOtherQuery().retailerId(id).singleResult();
        if (retailerOther == null) return "false";
        return retailerOther.getIsPackingMaterialRequired() == 0 ? "false" : "true";
    }

    /**
     * Check weather Tech Eval required or not by retailer
     * @param id Retailer Id
     * @return True or false
     */
    public static String isTechEvalRequired(@NotNull String id) {
        log.debug(id);
        RetailerOther retailerOther = masterdataService.createRetailerOtherQuery().retailerId(id).singleResult();
        if (retailerOther == null) return "false";
        return retailerOther.getIsTechEvalRequired() == 0 ? "false" : "true";
    }


}