package com.rlogistics.rest.controllers.PackagingController;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.PackagingMaterialInventory;
import com.rlogistics.rest.RestResult;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controls the packing material allocation to bulk flow items
 * @author Adarsh
 */
@RestController
@Slf4j
@var
public class BulkPackagingController {
    private MasterdataService masterdataService;

    @Autowired
    public BulkPackagingController(MasterdataService masterdataService) {
        this.masterdataService = masterdataService;
    }

    /**
     * Checks if box present in inventory or not
     * @param box  box number
     * @return true  add box | false : don't  add
     */
    @RequestMapping(value = "/check/box", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private boolean checkBox(@RequestParam(value = "box") String box) {
        var a = 0;
        try {
            a = Math.toIntExact(masterdataService.createAdditionalProductDetailsQuery().boxNumber(box).count());
        } catch (Exception e) {
            log.debug("Error" + e);
        }
        return a == 0;
    }

    /**
     * Checks if cover is present in inventory or not [ checks if its available or not also ]
     * @param cover  Cover Number
     * @return boolean true :Add cover  | false :  don't add
     */
    @RequestMapping(value = "/check/cover", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private boolean checkCover(@RequestParam(value = "cover") String cover) {
        var a = 0;
        try {
            a = Math.toIntExact(masterdataService.createPackagingMaterialInventoryQuery().status(0).barcode(cover).count());
        } catch (Exception e) {
            log.debug("Error" + e);
        }
        return a == 1;
    }


    /**
     * Changes the status of cover after its being used
     * @param cover Cover no
     * @param ticketNo Ticket no
     * @param feName FE name
     * @return DTO for if its changed or not
     */
    @RequestMapping(value = "/change/status/cover", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult changeStatusOfCover(@RequestParam(value = "cover") String cover,
                                           @RequestParam(value = "ticketNo") String ticketNo,
                                           @RequestParam(value = "feName") String feName) {
        RestResult result = new RestResult();
        try {

            PackagingMaterialInventory inventory = masterdataService.createPackagingMaterialInventoryQuery().barcode(cover).singleResult();
            inventory.setStatus(2);
            inventory.setTicketNumber(ticketNo);
            inventory.setUserId(feName);
            masterdataService.savePackagingMaterialInventory(inventory);
            result.setSuccess(true);
            result.setMessage("Status Changed for" + cover);
            return result;

        } catch (Exception e) {
            result.setMessage("Issue with barcode : Contact Coordinator ");
            result.setSuccess(false);
            return result;
        }
    }

    /**
     * Adds a box to the inventory
     * @param box box no
     * @param feName FeName
     * @param ticketNo Ticket no
     * @return DTO for if it has been added or not
     */
    @RequestMapping(value = "add/box", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    private RestResult addBox(@RequestParam(value = "box") String box,
                              @RequestParam(value = "feName") String feName,
                              @RequestParam(value = "ticketNo") String ticketNo
    ) {
        RestResult result = new RestResult();
        int a;
        try {
            a = Math.toIntExact(masterdataService.createPackagingMaterialInventoryQuery().barcode(box).count());
            if (a == 0) {
                PackagingMaterialInventory inventory = masterdataService.newPackagingMaterialInventory();
                inventory.setBarcode(box);
                inventory.setStatus(2);
                inventory.setUserId(feName);
                inventory.setTicketNumber(ticketNo);
                masterdataService.savePackagingMaterialInventory(inventory);
                result.setSuccess(true);
                result.setMessage("Added" + box);
                return result;
            } else {
                result.setMessage("Issue with Box Barcode : " + box + " Contact coordinator");
                result.setSuccess(false);
                return result;
            }
        } catch (Exception e) {
            result.setMessage("Some Error Occurred :(");
            result.setSuccess(false);
            return result;
        }
    }
}
