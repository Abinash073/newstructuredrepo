package com.rlogistics.rest.services.PhysicalEvaluationService;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
class PhysicalEvalReponseEntity {
    @JsonProperty("customer_response")
    private String[] customer_response;
    @JsonProperty(value = "engineer_response",defaultValue = "N/A")
    private List<EnngResponse> engineer_response = new ArrayList<>();

}
