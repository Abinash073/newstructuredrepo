package com.rlogistics.rest.controllers.physicalEvaluationcontroller;

import com.rlogistics.customqueries.BulkImeiNoOfRetaier;
import com.rlogistics.rest.RestResult;
import com.rlogistics.rest.repository.BulkImeiRepository;
import com.rlogistics.rest.services.BulkExcelWriterService;
import com.rlogistics.rest.services.PhysicalEvaluationService.ImeiNumberWithQandAnsDTO;
import com.rlogistics.rest.services.PhysicalEvaluationService.PhysicalEvaluationData;
import com.rlogistics.rest.services.excelWriter.ReportExcelUtil;
import com.rlogistics.rest.services.excelWriter.SaveExcelInHostUtil;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for PhysicalEval data for Bulk
 *
 * @author : Adarsh
 */
@RestController
@RequestMapping(value = "/physical-evaluation")
@Slf4j
@val
public class PhysicalEvaluationForBulkController {

    private PhysicalEvaluationData evaluationData;

    private ReportExcelUtil excelUtil;

    private BulkImeiRepository imeiRepository;

    private SaveExcelInHostUtil saveExcelInHostUtil;

    private BulkExcelWriterService bulkExcelWriterService;

    @Autowired
    public PhysicalEvaluationForBulkController(PhysicalEvaluationData evaluationData,
                                               @Qualifier("bulkPhysicalEvalData") ReportExcelUtil excelUtil,
                                               BulkImeiRepository imeiRepository,
                                               SaveExcelInHostUtil saveExcelInHostUtil,
                                               BulkExcelWriterService bulkExcelWriterService) {
        this.evaluationData = evaluationData;
        this.excelUtil = excelUtil;
        this.imeiRepository = imeiRepository;
        this.saveExcelInHostUtil = saveExcelInHostUtil;
        this.bulkExcelWriterService = bulkExcelWriterService;
    }


    @SuppressWarnings("unused")
    //Old Api to generate Excel for bulk
    private RestResult getPhysicalEvalExcel(@RequestParam(value = "startDate") String startDate,
                                            @RequestParam(value = "retailerId") String retailerId,
                                            @RequestParam(value = "endDate") String endDate) {
        RestResult restResult = new RestResult();
        try {

            List<BulkImeiNoOfRetaier> imeiNos = imeiRepository.getFedistanceDetails(startDate, endDate, retailerId);

            List<ImeiNumberWithQandAnsDTO> imeiNumberWithQandAnsDTOS = imeiNos
                    .stream()
                    .map(a -> evaluationData.getData(a.getImeiNumber()))
                    .filter(b -> b.getData().size() > 0)
                    .collect(Collectors.toList());
            excelUtil.initExcel("Bulk-Data").setStyling();


            for (ImeiNumberWithQandAnsDTO dto :
                    imeiNumberWithQandAnsDTOS) {

                excelUtil.insertData(dto.getData(), dto.getProcessId());
            }
            byte[] exceldata = excelUtil.saveExcel().getExcel();

            saveExcelInHostUtil
                    .saveExcel(
                            "get",
                            "physical_eval",
                            "data",
                            "bulk_eval_data.xlsx",
                            exceldata
                    );
            restResult.setSuccess(true);
            restResult.setResponse("server/attachment/download/get/physical_eval/data");
            return restResult;


        } catch (Exception e) {
            log.debug("error" + e);
        }
        return restResult;
    }

    /**
     * Physical Eval data by imei no
     *
     * @param imei Imei number
     * @return DTO containing Physical Eval Result
     */
    @RequestMapping(value = "/bulk/by-imei", method = RequestMethod.POST)
    private RestResult getPhysicalEvalByImei(@RequestParam(value = "imei") String imei) {
        val restResult = new RestResult();
        try {
            val qandAns = evaluationData.getData(imei);
            restResult.setSuccess(true);
            restResult.setResponse(qandAns);
        } catch (Exception e) {
            restResult.setSuccess(false);
            restResult.setMessage("Error while fetching data" + e);

        }
        return restResult;

    }

    /**
     * Generates excel sheet to be provided to retailer based on the filter
     *
     * @param startDate  Start date provided by angular
     * @param retailerId Retailer Id provided by angular
     * @param endDate    End Date
     * @return DTO containing url of Excel
     */
    @RequestMapping(value = "/bulk/excel-by-date", method = RequestMethod.POST)
    private RestResult getExcelFromListOfImei(@RequestParam(value = "startDate") String startDate,
                                              @RequestParam(value = "retailerId") String retailerId,
                                              @RequestParam(value = "endDate") String endDate) {
        val restResult = new RestResult();
        try {

            val imeiNos = imeiRepository.getFedistanceDetails(startDate, endDate, retailerId);
            val imeiString = imeiNos.stream().map(a -> a.getProcessId() + "__" + a.getImeiNumber()).collect(Collectors.joining(","));
            val data = evaluationData.getDataForExcel(imeiString);
            val headerData = evaluationData.getHeader(retailerId);
            val url = bulkExcelWriterService.creatExcel(headerData, data);
            restResult.setResponse(url);
            restResult.setMessage("Excel Sheet Generated");
            restResult.setSuccess(true);
            return restResult;


        } catch (Exception e) {
            log.debug("Error" + e);
            restResult.setMessage("Failed");
            restResult.setSuccess(false);
            return restResult;
        }

    }
}
