/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.form;

import java.util.ArrayList;
import java.util.Map;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.DynamicEnumFormType;
import org.activiti.explorer.Messages;

import com.rlogistics.ui.task.ScriptExecutor;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

/**
 * @author Frederik Heremans
 * @author Joram Barrez
 */
public class DynamicEnumFormPropertyRenderer extends AbstractFormPropertyRenderer {

  public DynamicEnumFormPropertyRenderer() {
    super(DynamicEnumFormType.class);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Field getPropertyField(ScriptExecutor scriptExecutor,FormProperty formProperty) {
    ComboBox comboBox = new ComboBox(getPropertyLabel(formProperty));
    comboBox.setRequired(formProperty.isRequired());
    comboBox.setRequiredError(getMessage(Messages.FORM_FIELD_REQUIRED, getPropertyLabel(formProperty)));
    comboBox.setEnabled(formProperty.isWritable());
    comboBox.setNullSelectionAllowed(false);

    Object firstItemId = null;
    Object itemToSelect = null;
    
    String controllingPV = ((DynamicEnumFormType)formProperty.getType()).getControllingPV();
    
    Map<String,String> values = DynamicEnumFormType.ensureMap(scriptExecutor.execute(controllingPV));
    
    if (values != null) {
      for (String value: values.keySet()) {
        // Add value and label (if any)
        comboBox.addItem(value);
        
        if (firstItemId == null) {
          firstItemId = value; // select first element
        }
        
        String selectedValue = formProperty.getValue();
        if (selectedValue != null && selectedValue.equals(value)) {
          itemToSelect = value; // select first element
        }
        
        if (value != null) {
          comboBox.setItemCaption(value,values.get(value));
        }
      }
    }
    
    // Select value or first element
    if (itemToSelect != null) {
      comboBox.select(itemToSelect);
      
    } else if (firstItemId != null) {
      comboBox.select(firstItemId);
    }
    
    return comboBox;
  }
}
