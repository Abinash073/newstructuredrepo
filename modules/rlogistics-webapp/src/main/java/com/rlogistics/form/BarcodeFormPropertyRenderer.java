/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.form;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.BarcodeFormType;

import com.rlogistics.ui.task.ScriptExecutor;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;

/**
 * @author Frederik Heremans
 */
public class BarcodeFormPropertyRenderer extends AbstractFormPropertyRenderer {

	public BarcodeFormPropertyRenderer() {
		super(BarcodeFormType.class);
	}

	@Override
	public Field getPropertyField(ScriptExecutor scriptExecutor, FormProperty formProperty) {
		TextField label = new TextField("This component can acquire barcode in a barcode context");
		label.setReadOnly(true);
		return label;
	}

}
