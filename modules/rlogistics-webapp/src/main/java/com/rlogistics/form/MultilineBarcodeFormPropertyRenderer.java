package com.rlogistics.form;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.impl.form.MultilineBarcodeFormType;
import org.activiti.engine.impl.form.MultilineStringFormType;
import org.activiti.explorer.Messages;

import com.rlogistics.ui.task.ScriptExecutor;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextArea;

public class MultilineBarcodeFormPropertyRenderer extends AbstractFormPropertyRenderer{

	public MultilineBarcodeFormPropertyRenderer() {
		super(MultilineBarcodeFormType.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Field getPropertyField(ScriptExecutor scriptExecutor, FormProperty formProperty) {
		TextArea textField = new TextArea(getPropertyLabel(formProperty));
	    textField.setRequired(formProperty.isRequired());
	    textField.setEnabled(formProperty.isWritable());
	    textField.setRequiredError(getMessage(Messages.FORM_FIELD_REQUIRED, getPropertyLabel(formProperty)));

	    if (formProperty.getValue() != null) {
	      textField.setValue(formProperty.getValue());
	    }
	    
	    textField.setColumns(40);
	    textField.setRows(20);

	    return textField;
	}

	
}
