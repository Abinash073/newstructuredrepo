/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rlogistics.form;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.MultilineStringFormType;
import org.activiti.explorer.Messages;

import com.rlogistics.ui.task.ScriptExecutor;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextArea;

/**
 * @author Frederik Heremans
 */
public class MultilineStringFormPropertyRenderer extends AbstractFormPropertyRenderer {

  public MultilineStringFormPropertyRenderer() {
    super(MultilineStringFormType.class);
  }

  @Override
  public Field getPropertyField(ScriptExecutor scriptExecutor, FormProperty formProperty) {
    TextArea textField = new TextArea(getPropertyLabel(formProperty));
    textField.setRequired(formProperty.isRequired());
    textField.setEnabled(formProperty.isWritable());
    textField.setRequiredError(getMessage(Messages.FORM_FIELD_REQUIRED, getPropertyLabel(formProperty)));

    if (formProperty.getValue() != null) {
      textField.setValue(formProperty.getValue());
    }
    
    textField.setColumns(40);
    textField.setRows(20);

    return textField;
  }

}
