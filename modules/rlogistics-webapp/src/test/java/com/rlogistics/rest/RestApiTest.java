package com.rlogistics.rest;

import com.rlogistics.master.identity.Retailer;
import com.rlogistics.master.identity.TicketImages;
import com.rlogistics.master.identity.impl.persistence.entity.RetailerEntity;
import com.rlogistics.master.identity.impl.persistence.entity.TicketImagesEntity;

import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.HistoricTaskInstanceEntity;
import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.PhotoBeforePaking;
import pojo.ReportPojo;
import software.amazon.ion.SystemSymbols;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static com.rlogistics.rest.RestApi.PHOTOGRAPHBEFORE;
import static org.junit.Assert.*;

public class RestApiTest {
	public static Logger log = LoggerFactory.getLogger(RestApiTest.class);

	@Test
	public void getImagesBeforePacking_ticketNotInSystem() {
		RestApi restApi = new RestApi();
		Retailer retailer = new RetailerEntity();
		String ticketNo = "ticket";
		retailer.setCode(ticketNo);
		List<TicketImages> images = new ArrayList<>();
		RestResponse imagesBeforePacking = restApi.getImagesBeforePackingInternal(ticketNo, retailer, images, 10);
		assertNull(imagesBeforePacking.getResponse());
		assertFalse(imagesBeforePacking.isSuccess());
		assertEquals("Images for Ticket Number: ticket is not present in System.", imagesBeforePacking.getMessage());
	}

	@Test
	public void getImagesBeforePacking_ticketNotForRetailer() {
		RestApi restApi = new RestApi();
		Retailer retailer = new RetailerEntity();
		String ticketNo = "ticket";
		String anotherTicketNo = "ticketAnother";
		retailer.setCode(ticketNo);
		List<TicketImages> images = new ArrayList<>();
		RestResponse imagesBeforePacking = restApi.getImagesBeforePackingInternal(anotherTicketNo, retailer, images,
				10);
		assertNull(imagesBeforePacking.getResponse());
		assertFalse(imagesBeforePacking.isSuccess());
		assertEquals("Ticket Number: ticketAnother is not for this Retailer", imagesBeforePacking.getMessage());
	}

	@Test
	public void getImagesBeforePacking_hasTicketsButDoesNotMatchTicketType_willReturnEmptyList() {
		RestApi restApi = new RestApi();
		Retailer retailer = new RetailerEntity();
		String ticketNo = "ticket";
		retailer.setCode(ticketNo);
		List<TicketImages> images = new ArrayList<>();
		TicketImages ticketImage = new TicketImagesEntity();
		ticketImage.setImageType("dummyImageType");
		images.add(ticketImage);
		RestResponse imagesBeforePacking = restApi.getImagesBeforePackingInternal(ticketNo, retailer, images, 10);
		List response = (List) imagesBeforePacking.getResponse();
		assertEquals(0, response.size());
		assertTrue(imagesBeforePacking.isSuccess());
		assertEquals("Images Link for Ticket Number : ticket", imagesBeforePacking.getMessage());
	}

	@Test
	public void getImagesBeforePacking_hasTicketsWithMatchingTicketType() {
		RestApi restApi = new RestApi();
		Retailer retailer = new RetailerEntity();
		String ticketNo = "ticket";
		retailer.setCode(ticketNo);
		List<TicketImages> images = new ArrayList<>();
		TicketImages ticketImage = new TicketImagesEntity();
		// ticketImage.setImageType(PHOTOGRAPHBEFORE);
		ticketImage.setImageUrl("http://bizlog.in");
		ticketImage.setCreateTimestamp("10/10/2010");
		images.add(ticketImage);
		RestResponse imagesBeforePacking = restApi.getImagesBeforePackingInternal(ticketNo, retailer, images, 10);
		List response = (List) imagesBeforePacking.getResponse();
		PhotoBeforePaking photoBeforePaking = (PhotoBeforePaking) response.get(0);
		// assertEquals(PHOTOGRAPHBEFORE, photoBeforePaking.getImageType());
		// assertEquals("http://bizlog.in", photoBeforePaking.getImageUrl());
		// assertEquals("10/10/2010", photoBeforePaking.getImageCreation());
		// assertEquals(10, photoBeforePaking.getImageCountBeforepacking());
		// assertTrue(imagesBeforePacking.isSuccess());
		// assertEquals("Images Link for Ticket Number : ticket",
		// imagesBeforePacking.getMessage());
	}

	@Test
	public void getTicketHistory_withTicketPresent() {
		RestApi restApi = new RestApi();
		String ticketNo = "TicketNo";
		RestResponse restResponse = new RestResponse();

		List<HistoricTaskInstance> tasks = new ArrayList<>();
		HistoricTaskInstanceEntity historicTaskInstance = new HistoricTaskInstanceEntity();
		historicTaskInstance.setName("Appointment Fixing");

		HistoricTaskInstance historicTaskInstance2 = new HistoricTaskInstanceEntity();
		// historicTaskInstance2.setName("Appointment Fixing");
		tasks.add(historicTaskInstance);
		tasks.add(historicTaskInstance2);
		restResponse = restApi.getTicketHistoryInternal(tasks, ticketNo);
		// assertEquals("APPOINTMENT_FIXED", restResponse.getResponse());
		// assertEquals("APPOINTMENT_RESCHEDULED_BY_CUSTOMER",
		// tasks.get(1).getName());

	}

	@Test
	public void PNDOneWayPrimaryValidation_AllFieldValidation() {

		Map<String, Object> variables = new HashMap<String, Object>();
		try {

			if (variables.get("itp_PD_PhotoId").toString().equals(null)) {
				System.out.println(variables.get("1111"));

			} else {
				System.out.println(variables.get("1222222"));

			}
			variables.put("itp_PD_PhotoId", "1");
			System.out.println(variables.get("itp_PD_PhotoId"));
		} catch (Exception e) {

		}

	}

	@Test
	public void checkForService() {
		// its working
		// variables.get("itp_PD_CustomerSignature");//one way
		// variables.get("itp_MD_ServiceCenterSignature");//one way service
		// center
		// variables.get("itp_VV_CustomerSignature");//buy back
		RestApi api = new RestApi();
		Map<String, Object> variables = new HashMap<>();
		// variables.put("rlServiceCode", "PICKNDROPONEWAY");
		// variables.put("itp_PD_CustomerSignature", "PICKNDROPONEWAY");

		variables.put("rlServiceCode", "BUYBACK");
		// variables.put("itp_VV_CustomerSignature", "PICKNDROPONEWAY");
		variables.put("itp_OO_CustomerSignature", "PICKNDROPONEWAY");
		System.out.println(new JSONObject(variables).toString());


	}

	@Test
	public void getIndianTimeStamp() {
		SimpleDateFormat sd = new SimpleDateFormat("YYYY-MM-dd HH:MM:ss");
		Date date = new Date();
		sd.setTimeZone(TimeZone.getTimeZone("IST"));
		System.out.println(sd.format(date) + "111");
		
	}
	@Test
	public void getTicketHistoryInternalForWebsite_test()throws Exception{
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date date = dateFormatter .parse("2019-04-08 16:01:45");
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {


			  }
			}, date,2*60*1000);	
		
		
		
		
		
//		System.out.println(getAmtrustJsonRequest().toString()+"1111111111111111111111111111111111");
		System.out.println(new JSONObject(return2().toString()).toString()+"1111111111111111111111111111111111");

		
		
	}
	private static JSONObject getAmtrustJsonRequest()
			throws Exception {
		

		JSONObject returnJson = new JSONObject();
		returnJson.put("UserName", "AMTRUST_HEADER_USER_NAME");
		returnJson.put("Password", "AMTRUST_HEADER_PASSWORD");
		returnJson.put("CustomerId", "AMTRUST_HEADER_CUSTOMER_ID");
		returnJson.put("LSPAlias", "AMTRUST_HEADER_LSP");
		returnJson.put("DocketNo", "ticketNo");
		returnJson.put("ReferenceNo", "");
		returnJson.put("ConsignorName", "");
		returnJson.put("ConsigneeName", "");
		returnJson.put("CurrentStatus", "status");
		returnJson.put("Origin", "location");
		returnJson.put("Destination", "dropLocation");
		returnJson.put("StatusDate", ReportPojo.getCurrentDate());
		returnJson.put("CurrentLocation", "");
		returnJson.put("StatusCode", "statusCode");
		returnJson.put("DeliveryStatus", "status");
		JSONArray jsonArray = new JSONArray();
		Map<String,Object> statuss = new HashMap<>();
		statuss.put("Status", "status");
		statuss.put("Location", "status");
		statuss.put("StatusDate", ReportPojo.getCurrentDate());
		statuss.put("StatusCode", "status");
		jsonArray.put(statuss);
		returnJson.put("Events", jsonArray);
		
		

		return returnJson;
	}
	public static Map<String,Object> return2(){
		
		Map<String,Object> returnMap=new HashMap<>();
		returnMap.put("", "");

		Map<String,Object> eventMap=new HashMap<>();
		eventMap.put("key", "prem");
		
		List<Map<String,Object>> eventList = new ArrayList<Map<String,Object>>();
		eventList.add(eventMap);
		
		returnMap.put("event", eventList);

		
		return returnMap;
		
		
		
	}
	@Test
	public void httpRequestClientTest() throws URISyntaxException, IOException {
//		String AMTRUST_URL = "https://webxpress.azure-api.net/api/pushTracking/PushTracking";
		String AMTRUST_URL = "g";

		
		String AMTRUST_HEADER_LSP = "BIZ";
		String AMTRUST_HEADER_CUSTOMER_ID = "C00163";
		String AMTRUST_HEADER_USER_NAME = "api@bizlog.com";
		String AMTRUST_HEADER_PASSWORD = "Admin123!@@#";
		String AMTRUST_HEADER_SUBSCRIPTION_KEY = "de39179c98c04d3ebb0ab6e6a6ce5841";
		
		JSONObject requestObject = new JSONObject();

		
		// API call
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost(AMTRUST_URL);
		httppost.addHeader("Content-Type", "application/json");
		httppost.addHeader("Ocp-Apim-Subscription-Key", AMTRUST_HEADER_SUBSCRIPTION_KEY);
		httppost.setEntity(new StringEntity(requestObject.toString(), "UTF8"));
		
		CloseableHttpResponse httpResponse = httpclient.execute(httppost);
		String rawResponse1 = EntityUtils.toString(httpResponse.getEntity());
//		System.out.println(rawResponse1);
	
	}
	@Test
	public void report_data_cancel_ticket() throws Exception{
		try{
//			System.out.println("111111111111"+ReportPojo.cancelTicket("123"));
			ReportPojo.updateReport(ReportPojo.cancelTicket("123"));
		}catch(Exception e){
//			System.out.println(e+"2222222222");
		}
		
	}
	
	@Test
	public void dataformat_for_Amtrust(){
		try{
			JSONObject jsonObject = new JSONObject();
//			System.out.println(jsonObject.length()+" 12345");
			
		}catch(Exception e){
			
		}
	}
	@Test
	public void dataformat_for_Amtrust1(){
		List<String> list= new ArrayList<>();
		list.add("prem");
		list.add("nath");
		System.out.println(list.toString().replace("[", "").replace("]", ""));
		
		try{			
		}catch(Exception e){
			
		}
	}


}
