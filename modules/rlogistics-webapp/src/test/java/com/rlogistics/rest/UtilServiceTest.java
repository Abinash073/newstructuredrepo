package com.rlogistics.rest;
import com.rlogistics.rest.services.util.DateUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
public class UtilServiceTest {
    private DateUtil dateUtil;

    @Before
    public void setUp() throws Exception{
        dateUtil = new DateUtil();
    }
    @Test
    public void testDate() throws Exception{
        String date =dateUtil.getDateByAddingDate("14-02-2019",1,"dd-MM-yyyy");
        assertEquals("15-02-2019",date);
    }
    @Test
    public void testDateBefore(){
        String date = dateUtil.getDateByReducingDate("14-02-2019",1,"dd-MM-yyyy");
        assertEquals("13-02-2019",date);
    }
}
