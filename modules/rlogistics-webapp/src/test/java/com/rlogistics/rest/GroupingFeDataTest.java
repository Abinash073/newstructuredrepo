package com.rlogistics.rest;

import com.rlogistics.customqueries.EachFeDistanceDetails;
import com.rlogistics.rest.services.lambda.GroupingFeData;
import com.rlogistics.rest.services.util.DistanceConversionAndCalculationUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GroupingFeDataTest {
    private DistanceConversionAndCalculationUtil calculationUtil;
    private GroupingFeData groupingFeData;
    List<EachFeDistanceDetails> a = new ArrayList<>();

    @Before
    public void beforeInit() {
        calculationUtil = new DistanceConversionAndCalculationUtil();
        groupingFeData = new GroupingFeData(calculationUtil);

        EachFeDistanceDetails data1 = new EachFeDistanceDetails();
        data1.setEmail("adarsh");
        data1.setTotalDistance("1.1 km");
        data1.setTicketNo("abc");
        EachFeDistanceDetails data2 = new EachFeDistanceDetails();
        data2.setEmail("adarsh");
        data2.setTotalDistance("1 m");
        data2.setTicketNo("abc");
        EachFeDistanceDetails data3 = new EachFeDistanceDetails();
        data3.setEmail("adarsh");
        data3.setTotalDistance("2.1 m");
        data3.setTicketNo("abc");
        EachFeDistanceDetails data4 = new EachFeDistanceDetails();
        data4.setEmail("adarsh");
        data4.setTotalDistance("1 m");
        data4.setTicketNo("abcd");
        EachFeDistanceDetails data5 = new EachFeDistanceDetails();
        data5.setEmail("adarsh");
        data5.setTotalDistance("1.1 m");
        data5.setTicketNo("abcd");
        a.add(data1);
        a.add(data2);
        a.add(data3);
        a.add(data4);
        a.add(data5);


    }

    @Test
    public void afterInit() {
        Map<String, Map<String, BigDecimal>> output = groupingFeData.groupingFeLocationData(a);
        Assert.assertEquals(1,output.size());
        Assert.assertEquals("1.103",output.get("adarsh").get("abc").toString());
    }
}
