package com.rlogistics.rest;

import com.rlogistics.rest.services.util.DistanceConversionAndCalculationUtil;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;


public class DistanceConversionAndCalculationUtilTest {

    private DistanceConversionAndCalculationUtil calculationUtil;
    private ArrayList<String> arrayList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

        calculationUtil = new DistanceConversionAndCalculationUtil();
        arrayList.add("1.2 km");
        arrayList.add("12.11 km");
        arrayList.add("12 m");
        arrayList.add("10.3 km");
        arrayList.add("122 m");
    }


    @Test
    public void test() {
        assertEquals(new BigDecimal("23.75"), calculationUtil.getDistance(arrayList).convertMeterToKm());
    }

}
