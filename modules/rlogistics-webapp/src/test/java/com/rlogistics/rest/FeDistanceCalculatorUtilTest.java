package com.rlogistics.rest;

import com.rlogistics.customqueries.FeDistanceReportData;
import com.rlogistics.rest.services.util.FeDistanceCalculatorUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FeDistanceCalculatorUtilTest {
    private FeDistanceCalculatorUtil util;
    List<FeDistanceReportData> data = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        util = new FeDistanceCalculatorUtil();
        FeDistanceReportData data1 = new FeDistanceReportData();
        data1.setCity("bangalore");
        data1.setFeName("adarsh.demofe@yopmail.com");
        data1.setTotalDistance("1.1 km");

        FeDistanceReportData data2 = new FeDistanceReportData();

        data2.setCity("bangalore");
        data2.setFeName("adarsh.demofe@yopmail.com");
        data2.setTotalDistance("2.2 km");


        FeDistanceReportData data3 = new FeDistanceReportData();
        data3.setCity("bangalore");
        data3.setTotalDistance("5.3 km");
        data3.setFeName("manoj.demofe@yopmail.com");

        FeDistanceReportData data4 = new FeDistanceReportData();
        data4.setCity("bangalore");
        data4.setTotalDistance("3 m");
        data4.setFeName("nitesh.demofe@yopmail.com");
        FeDistanceReportData data5 = new FeDistanceReportData();

        data5.setCity("bangalore");
        data5.setTotalDistance("1.3 km");
        data5.setFeName("nitesh.demofe@yopmail.com");


        data.add(data1);
        data.add(data2);
        data.add(data3);
        data.add(data4);
        data.add(data5);

    }

    @Test
    public void test() throws Exception {

        List<FeDistanceReportData> a = util.getFeWiseCalcualtedDistance(data);
        assertEquals(3, a.size());
    }

}
