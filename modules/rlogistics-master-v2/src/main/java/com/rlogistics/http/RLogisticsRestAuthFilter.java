package com.rlogistics.http;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.impl.identity.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RLogisticsRestAuthFilter implements Filter {
	private static Logger log = LoggerFactory.getLogger(RLogisticsRestAuthFilter.class);
	
	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		
		processRltoken(httpRequest);
		
		chain.doFilter(request, response);
	}

	public static boolean processRltoken(HttpServletRequest httpRequest) {
		String rltoken = null;
		
		Cookie rltokenCookie = findCookie(httpRequest.getCookies(),"rltoken");
		if(rltokenCookie != null){
			rltoken = rltokenCookie.getValue();
			if(log.isDebugEnabled()){
				log.debug("Using cookie rltoken:" + rltoken);
			}
		} else {
			if(log.isDebugEnabled()){
				log.debug("NO cookie rltoken");
			}
		}
		
		if(rltoken == null){
			Map<String, String[]> params = httpRequest.getParameterMap();
			if(params.containsKey("rltoken")){
				rltoken = params.get("rltoken")[0];
				if(log.isDebugEnabled()){
					log.debug("Using parameter rltoken:" + rltoken);
				}
			} else {
				if(log.isDebugEnabled()){
					log.debug("NO param rltoken");
				}
			}			
		}
		
		if(rltoken != null){
			rltoken = URLDecoder.decode(rltoken);
		}
		
		if(rltoken != null && AuthUtil.isValid(rltoken)){
			Authentication.setAuthenticatedUserId(AuthUtil.getUserId(rltoken));
			return true;
		} else {
			log.error("rltoken " + (rltoken != null ? rltoken : "NULL") + " is not valid.");
			return false;
		}
	}

	private static Cookie findCookie(Cookie[] cookies, String name) {
		if(cookies != null) {
			for(Cookie cookie:cookies){
				if(cookie.getName().equals(name)){
					return cookie;
				}
			}
		}
		
		return null;
	}

	public void destroy() {
	}
}
