package com.rlogistics.http;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class AuthUtil {

	public static String generateHMAC(String data){
		try {
			SecretKeySpec sks = new SecretKeySpec(key.getBytes(),algo);
			Mac mac = Mac.getInstance(algo);
			mac.init(sks);
			return Base64.getUrlEncoder().encodeToString(mac.doFinal(data.getBytes()));
		} catch(Exception ex){
			throw new RuntimeException("Failed to generate HMAC",ex);
		}
	}

	public static boolean isValid(String token) {
		int lastSep = token.lastIndexOf("___");
		String rawToken = token.substring(0,lastSep);
		String hmacIn = token.substring(lastSep+3);
		
		return hmacIn.equals(generateHMAC(rawToken));
	}

	public static String getUserId(String token) {
		int firstSep = token.indexOf("___");
		return token.substring(0,firstSep);
	}
	
	public static String cryptWithMD5(String password){
		MessageDigest md;
	    try {
	        md = MessageDigest.getInstance("MD5");
	        byte[] passBytes = password.getBytes();
	        md.reset();
	        byte[] digested = md.digest(passBytes);
	        StringBuffer sb = new StringBuffer();
	        for(int i=0;i<digested.length;i++){
	            sb.append(Integer.toHexString(0xff & digested[i]));
	        }
	        return sb.toString();
	    } catch (NoSuchAlgorithmException ex) {
	        
	    }
	        return null;
	   }

	private static final String key = "rlogistics-secret-key";
	private static final String algo = "HmacSHA1";
}
