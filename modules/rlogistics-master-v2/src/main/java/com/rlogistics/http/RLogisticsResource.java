package com.rlogistics.http;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.identity.Authentication;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RLogisticsResource {

	protected boolean beforeMethodInvocation(HttpServletRequest httpRequest,HttpServletResponse httpResponse){
		if(!RLogisticsRestAuthFilter.processRltoken(httpRequest)){
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return false;
		}
	
		if (Authentication.getAuthenticatedUserId() == null) {
			throw new RuntimeException("REST method can be invoked only by a loggged in user");
		}
		
		return true;
	}
	
	protected void afterMethodInvocation(){
		Authentication.setAuthenticatedUserId(null);
	}

	protected static Map<String, String> parseMap(String fieldValuesJson) {
		Map<String,String> retval = new HashMap<String,String>();

		try{
			HashMap raw = new ObjectMapper().readValue(URLDecoder.decode(fieldValuesJson), HashMap.class);
			for(Object k: raw.keySet()){
				retval.put(k.toString(), raw.get(k).toString());
			}
		} catch(Exception ex){
			throw new RuntimeException("Error parsing JSON",ex);
		}

		return retval;
	}
	
	protected Set<String> parseSet(String membersJson) {
		Set<String> retval = new HashSet<String>();

		try{
			HashSet raw = new ObjectMapper().readValue(URLDecoder.decode(membersJson), HashSet.class);
			for(Object m: raw){
				retval.add(m.toString());
			}
		} catch(Exception ex){
			throw new RuntimeException("Error parsing JSON",ex);
		}

		return retval;
	}
}
