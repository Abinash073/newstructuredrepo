package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.ServiceMaster.ServiceMasterQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerCategories {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getCategoryId();
	
	public void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerCategoriesQuery extends Query<RetailerCategoriesQuery,RetailerCategories>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerCategoriesQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerCategoriesQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerCategoriesQuery categoryId(String categoryId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerCategoriesQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerCategoriesQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="categoryId")
		public RetailerCategoriesQuery orderByCategoryId();		
	}
}
