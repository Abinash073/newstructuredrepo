package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerServices {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getServiceId();
	
	public void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerServicesQuery extends Query<RetailerServicesQuery,RetailerServices>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerServicesQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerServicesQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute="serviceId")
		public RetailerServicesQuery serviceId(String serviceId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerServicesQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerServicesQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="serviceId")
		public RetailerServicesQuery orderByServiceId();		
	}
}
