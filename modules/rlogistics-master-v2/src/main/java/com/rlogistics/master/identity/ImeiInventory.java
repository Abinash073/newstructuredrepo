package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ImeiInventory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getLocation();

	void setLocation(String location);

	@MasterEntityAttribute
	String getImeiNo();

	void setImeiNo(String imeiNo);
	
	@MasterEntityAttribute
	String getBarcode();

	void setBarcode(String barcode);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int Status);
	
	@MasterEntityAttribute
	String getModel();

	void setModel(String model);
	
	@MasterEntityAttribute
	String getBrand();

	void setBrand(String brand);
	
	@MasterEntityAttribute
	String getProductCost();

	void setProductCost(String productCost);
	
	@MasterEntityAttribute
	String getProductCategory();

	void setProductCategory(String productCategory);
	
	@MasterEntityAttribute
	String getYear();

	void setYear(String year);

	

	@MasterEntityQuery
	public interface ImeiInventoryQuery extends Query<ImeiInventoryQuery, ImeiInventory> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		ImeiInventoryQuery id(String id);

		@MasterEntityQueryParameter(attribute = "location")
		ImeiInventoryQuery location(String location);
		
		@MasterEntityQueryParameter(attribute = "retailerId")
		ImeiInventoryQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute = "imeiNo")
		ImeiInventoryQuery imeiNo(String imeiNo);
		
		@MasterEntityQueryParameter(attribute = "barcode")
		ImeiInventoryQuery barcode(String barcode);

		@MasterEntityQueryParameter(attribute = "status")
		ImeiInventoryQuery status(int status);
		
		@MasterEntityQueryParameter(attribute = "model")
		ImeiInventoryQuery model(String model);
		
		@MasterEntityQueryParameter(attribute = "brand")
		ImeiInventoryQuery brand(String brand);
		
		@MasterEntityQueryParameter(attribute = "productCost")
		ImeiInventoryQuery productCost(String productCost);
		
		@MasterEntityQueryParameter(attribute = "productCategory")
		ImeiInventoryQuery productCategory(String productCategory);
		
		@MasterEntityQueryParameter(attribute = "year")
		ImeiInventoryQuery year(String year);


		@MasterEntityQuerySort(attribute = "id")
		ImeiInventoryQuery orderById();

		@MasterEntityQuerySort(attribute = "model")
		ImeiInventoryQuery orderByUserName();

		@MasterEntityQuerySort(attribute = "year")
		ImeiInventoryQuery orderByYear();
		
		@MasterEntityQuerySort(attribute = "imeiNo")
		ImeiInventoryQuery orderByImeiNo();
		
		@MasterEntityQuerySort(attribute = "barcode")
		ImeiInventoryQuery orderByBracode();
		
		@MasterEntityQuerySort(attribute = "location")
		ImeiInventoryQuery orderByLocation();
	}
}
