package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FlBarcodeOffset {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    int getFlBarcodeOffset();

    void setFlBarcodeOffset(int flBarcodeOffset);

    @MasterEntityQuery
    interface FlBarcodeOffsetQuery extends Query<FlBarcodeOffsetQuery, FlBarcodeOffset> {

        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        FlBarcodeOffsetQuery id(String id);

        @MasterEntityQueryParameter(attribute = "flBarcodeOffset")
        FlBarcodeOffsetQuery flBarcodeOffset(int flBarcodeOffset);
    }
}
