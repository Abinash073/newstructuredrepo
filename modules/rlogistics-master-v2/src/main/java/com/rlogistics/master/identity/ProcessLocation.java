/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.util.Date;

@MasterEntity(tableName = "RL_MD_LOCATION")
@MasterEntityLeadsToRestController
public interface ProcessLocation {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getName();

    void setName(String name);

    @MasterEntityAttribute
    String getCode();

    void setCode(String code);

    @MasterEntityAttribute
    String getParentLocationId();

    void setParentLocationId(String parentLocationId);

    @MasterEntityAttribute
    String getAddress1();

    void setAddress1(String address1);

    @MasterEntityAttribute
    String getAddress2();

    void setAddress2(String address2);

    @MasterEntityAttribute
    String getCity();

    void setCity(String city);

    @MasterEntityAttribute
    String getCityId();

    void setCityId(String cityId);

    @MasterEntityAttribute
    String getPincode();

    void setPincode(String pincode);

    @MasterEntityAttribute
    String getPhoneNumber();

    void setPhoneNumber(String phoneNumber);

    @MasterEntityAttribute
    String getAlternatePhoneNumber();

    void setAlternatePhoneNumber(String alternatePhoneNumber);

    @MasterEntityAttribute
    String getContactPerson();

    void setContactPerson(String contactPerson);

    @MasterEntityAttribute
    String getEmail();

    void setEmail(String email);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityQuery
    public interface ProcessLocationQuery extends Query<ProcessLocationQuery, ProcessLocation> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        ProcessLocationQuery id(String id);

        @MasterEntityQueryParameter(attribute = "name")
        ProcessLocationQuery name(String name);

        @MasterEntityQueryParameter(attribute = "code", codeAttribute = true)
        ProcessLocationQuery code(String code);

        @MasterEntityQueryParameter(attribute = "address1")
        ProcessLocationQuery address1(String address1);

        @MasterEntityQueryParameter(attribute = "address2")
        ProcessLocationQuery address2(String address2);

        @MasterEntityQueryParameter(attribute = "city")
        ProcessLocationQuery city(String city);

        @MasterEntityQueryParameter(attribute = "cityId")
        ProcessLocationQuery cityId(String cityId);

        @MasterEntityQueryParameter(attribute = "pincode")
        ProcessLocationQuery pincode(String pincode);

        @MasterEntityQueryParameter(attribute = "phoneNumber")
        ProcessLocationQuery phoneNumber(String phoneNumber);

        @MasterEntityQueryParameter(attribute = "alternatePhoneNumber")
        ProcessLocationQuery alternatePhoneNumber(String alternatePhoneNumber);

        @MasterEntityQueryParameter(attribute = "contactPerson")
        ProcessLocationQuery contactPerson(String contactPerson);

        @MasterEntityQueryParameter(attribute = "email")
        ProcessLocationQuery email(String email);

        @MasterEntityQuerySort(attribute = "id")
        ProcessLocationQuery orderById();

        @MasterEntityQuerySort(attribute = "name")
        ProcessLocationQuery orderByName();

        @MasterEntityQuerySort(attribute = "code")
        ProcessLocationQuery orderByCode();

        @MasterEntityQuerySort(attribute = "parentLocationId")
        ProcessLocationQuery orderByParentLocationId();

        @MasterEntityQuerySort(attribute = "cityId")
        ProcessLocationQuery orderByCityId();
    }
}
