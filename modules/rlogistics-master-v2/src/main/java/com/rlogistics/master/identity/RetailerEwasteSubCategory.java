package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.ProductSubCategory.ProductSubCategoryQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerEwasteSubCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getRetailerEwasteCategoryId();

	void setRetailerEwasteCategoryId(String retailerEwasteCategoryId);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	double getVolumetricWeight();
	
	void setVolumetricWeight(double volumetricWeight);
	
	@MasterEntityAttribute
	double getActualWeight();
	
	void setActualWeight(double actualWeight);
	
	@MasterEntityQuery
	public interface RetailerEwasteSubCategoryQuery extends Query<RetailerEwasteSubCategoryQuery,RetailerEwasteSubCategory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerEwasteSubCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		RetailerEwasteSubCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerEwasteSubCategoryQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		RetailerEwasteSubCategoryQuery code(String code);
		@MasterEntityQueryParameter(attribute = "retailerEwasteCategoryId")
		RetailerEwasteSubCategoryQuery retailerEwasteCategoryId(String retailerEwasteCategoryId);

		@MasterEntityQuerySort(attribute = "id")
		RetailerEwasteSubCategoryQuery orderById();
	}
}
