package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ConsoleLogs {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getProcInstId();
	
	void setProcInstId(String procInstId);
	
	@MasterEntityAttribute
	String getData();
	
	void setData(String data);
	

	
	@MasterEntityQuery
	public interface ConsoleLogsQuery extends Query<ConsoleLogsQuery,ConsoleLogs>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public ConsoleLogsQuery id(String id);

		@MasterEntityQueryParameter(attribute="procInstId")
		public ConsoleLogsQuery procInstId(String procInstId);
		
		@MasterEntityQueryParameter(attribute="data")
		public ConsoleLogsQuery data(String data);
	}
}
