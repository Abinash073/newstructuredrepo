package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerReplacementPolicy {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getProductId();
	
	void setProductId(String productId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface RetailerReplacementPolicyQuery extends Query<RetailerReplacementPolicyQuery,RetailerReplacementPolicy>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerReplacementPolicyQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerReplacementPolicyQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "productId")
		RetailerReplacementPolicyQuery productId(String productId);
			
		@MasterEntityQuerySort(attribute = "id")
		RetailerReplacementPolicyQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerReplacementPolicyQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "productId")
		RetailerReplacementPolicyQuery orderByProductId();
	}
}
