package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ServiceProvider {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ServiceProviderQuery extends Query<ServiceProviderQuery,ServiceProvider>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ServiceProviderQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		ServiceProviderQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		ServiceProviderQuery code(String code);
		@MasterEntityQuerySort(attribute = "id")
		ServiceProviderQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		ServiceProviderQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		ServiceProviderQuery orderByCode();
	}
}
