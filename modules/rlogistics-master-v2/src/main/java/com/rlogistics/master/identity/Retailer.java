package com.rlogistics.master.identity;

import java.sql.Timestamp;
import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Retailer {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getEmail();

	void setEmail(String email);
	
	@MasterEntityAttribute
	String getAddress1();

	void setAddress1(String address1);
	
	@MasterEntityAttribute
	String getAddress2();

	void setAddress2(String address2);
	
	@MasterEntityAttribute
	String getCity();

	void setCity(String city);
	
	@MasterEntityAttribute
	String getTelephoneNumber();

	void setTelephoneNumber(String telephoneNumber);
	
	@MasterEntityAttribute
	String getAltTelephoneNumber();

	void setAltTelephoneNumber(String altTelephoneNumber);
	
	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	
	@MasterEntityAttribute
	String getApiUsername();

	void setApiUsername(String apiUsername);
	
	@MasterEntityAttribute
	String getApiPassword();

	void setApiPassword(String apiPassword);
	
	@MasterEntityAttribute
	String getApiToken();

	void setApiToken(String apiToken);
	
	@MasterEntityAttribute
	Timestamp getTokenExpiryDate();

	void setTokenExpiryDate(Timestamp tokenExpiryDate);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerQuery extends Query<RetailerQuery,Retailer>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		RetailerQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		RetailerQuery code(String code);
		@MasterEntityQueryParameter(attribute = "email")
		RetailerQuery email(String email);
		@MasterEntityQueryParameter(attribute = "address1")
		RetailerQuery address1(String address1);
		@MasterEntityQueryParameter(attribute = "address2")
		RetailerQuery address2(String address2);
		@MasterEntityQueryParameter(attribute = "city")
		RetailerQuery city(String city);
		@MasterEntityQueryParameter(attribute = "telephoneNumber")
		RetailerQuery telephoneNumber(String telephoneNumber);
		@MasterEntityQueryParameter(attribute = "altTelephoneNumber")
		RetailerQuery altTelephoneNumber(String altTelephoneNumber);
		@MasterEntityQueryParameter(attribute = "pincode")
		RetailerQuery pincode(String pincode);

		@MasterEntityQuerySort(attribute = "id")
		RetailerQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		RetailerQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		RetailerQuery orderByCode();
		@MasterEntityQuerySort(attribute = "email")
		RetailerQuery orderByEmail();
	}
}
