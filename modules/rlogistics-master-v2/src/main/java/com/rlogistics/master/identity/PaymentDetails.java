package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.Payment.PaymentQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PaymentDetails {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCashfreeReferenceId();

	void setCashfreeReferenceId(String cashfreeReferenceId);

	@MasterEntityAttribute
	String getCashgramId();

	void setCashgramId(String cashgramId);

	@MasterEntityAttribute
	String getUtr();

	void setUtr(String utr);

	@MasterEntityAttribute
	String getTransferId();

	void setTransferId(String transferId);

	@MasterEntityAttribute
	String getAmount();

	void setAmount(String amount);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getEmail();

	void setEmail(String email);

	@MasterEntityAttribute
	String getPhone();

	void setPhone(String phone);

	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);

	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);

	@MasterEntityAttribute
	String getAcknowledged();

	void setAcknowledged(String acknowledged);

	@MasterEntityAttribute
	String getPayoutLink();

	void setPayoutLink(String payoutLink);

	@MasterEntityAttribute
	String getCreatedOn();
	
	void setCreatedOn(String createdOn);


	@MasterEntityAttribute
	String getUpdateOn();
	
	void setUpdateOn(String updateOn);


	@MasterEntityQuery
	public interface PaymentDetailsQuery extends Query<PaymentDetailsQuery, PaymentDetails> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		PaymentDetailsQuery id(String id);

		@MasterEntityQueryParameter(attribute = "cashfreeReferenceId")
		PaymentDetailsQuery cashfreeReferenceId(String cashfreeReferenceId);

		@MasterEntityQueryParameter(attribute = "cashgramId")
		PaymentDetailsQuery cashgramId(String cashgramId);

		@MasterEntityQueryParameter(attribute = "utr")
		PaymentDetailsQuery utr(String utr);

		@MasterEntityQueryParameter(attribute = "transferId")
		PaymentDetailsQuery transferId(String transferId);

		@MasterEntityQueryParameter(attribute = "amount")
		PaymentDetailsQuery amount(String amount);

		@MasterEntityQueryParameter(attribute = "name")
		PaymentDetailsQuery name(String name);

		@MasterEntityQueryParameter(attribute = "email")
		PaymentDetailsQuery email(String email);

		@MasterEntityQueryParameter(attribute = "status")
		PaymentDetailsQuery status(String status);

		@MasterEntityQueryParameter(attribute = "phone")
		PaymentDetailsQuery phone(String phone);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		PaymentDetailsQuery productInfo(String productInfo);
		
		@MasterEntityQueryParameter(attribute = "acknowledged")
		PaymentDetailsQuery acknowledged(String acknowledged);
		
		@MasterEntityQueryParameter(attribute = "payoutLink")
		PaymentDetailsQuery payoutLink(String payoutLink);

		@MasterEntityQuerySort(attribute = "updateOn")
		PaymentDetailsQuery orderByUpdateOn();

		@MasterEntityQuerySort(attribute = "createdOn")
		PaymentDetailsQuery orderByCreatedOn();
	}
}
