package com.rlogistics.master.identity;


import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.util.Date;

@MasterEntity(tableName = "RL_MD_FE_DISTANCE")
@MasterEntityLeadsToRestController
public interface FeDistance {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getFeId();

    void setFeId(String feId);

    @MasterEntityAttribute
    String getDate();

    void setDate(String date);

    @MasterEntityAttribute
    String getDistance();

    void setDistance(String distance);

    @MasterEntityAttribute
    int getStatus();

    void setStatus(int status);

    @MasterEntityAttribute
    String getLocation();

    void setLocation(String location);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityAttribute
    String getLocationName();

    void setLocationName(String locationName);

    @MasterEntityAttribute
    String getFeName();

    void setFeName(String feName);

    @MasterEntityAttribute
    String getStartLatitude();

    void setStartLatitude(String startLatitude);
    @MasterEntityAttribute
    String getStartLongitude();

    void setStartLongitude(String startLongitude);
    @MasterEntityAttribute
    String getStopLatitude();

    void setStopLatitude(String stopLatitude);
    @MasterEntityAttribute
    String getStopLongitude();

    void setStopLongitude( String stopLongitude);



    @MasterEntityQuery
    public interface FeDistanceQuery extends Query<FeDistanceQuery, FeDistance> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        FeDistanceQuery id(String id);

        @MasterEntityQueryParameter(attribute = "feId")
        FeDistanceQuery feId(String feId);

        @MasterEntityQueryParameter(attribute = "date")
        FeDistanceQuery date(String date);

        @MasterEntityQueryParameter(attribute = "distance")
        FeDistanceQuery distance(String distance);

        @MasterEntityQueryParameter(attribute = "status")
        FeDistanceQuery status(int status);

        @MasterEntityQueryParameter(attribute = "location")
        FeDistanceQuery location(String location);

        @MasterEntityQueryParameter(attribute = "locationName")
        FeDistanceQuery locationName(String locationName);

        @MasterEntityQueryParameter(attribute = "feName")
        FeDistanceQuery feName(String feName);

        @MasterEntityQueryParameter(attribute = "startLatitude")
        FeDistanceQuery startLatitude(String startLatitude);

        @MasterEntityQueryParameter(attribute = "startLongitude")
        FeDistanceQuery startLongitude(String startLongitude);

        @MasterEntityQueryParameter(attribute = "stopLatitude")
        FeDistanceQuery stopLatitude(String stopLatitude);

        @MasterEntityQueryParameter(attribute = "stopLongitude")
        FeDistanceQuery stopLongitude(String stopLongitude);

    }


}
