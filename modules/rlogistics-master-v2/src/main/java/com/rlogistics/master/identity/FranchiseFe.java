package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.util.Date;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FranchiseFe {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getFirstName();

    void setFirstName(String firstName);

    @MasterEntityAttribute
    String getLastName();

    void setLastName(String lastName);

    @MasterEntityAttribute
    String getEmailId();

    void setEmailId(String emailId);

    @MasterEntityAttribute
    String getContactNumber();

    void setContactNumber(String contactNumber);

    @MasterEntityAttribute
    int getStatus();

    void setStatus(int status);

    @MasterEntityAttribute
    String getFeAddress();

    void setFeAddress(String feAddress);

    @MasterEntityAttribute
    String getAgencyId();

    void setAgencyId(String agencyId);

    @MasterEntityAttribute
    String getFranchiseId();

    void setFranchiseId(String franchiseId);

    @MasterEntityAttribute
    String getPassword();

    void setPassword(String password);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityQuery
    interface FranchiseFeQuery extends Query<FranchiseFeQuery, FranchiseFe> {

        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        FranchiseFeQuery id(String id);

        @MasterEntityQueryParameter(attribute = "firstName")
        FranchiseFeQuery firstName(String firstName);

        @MasterEntityQueryParameter(attribute = "lastName")
        FranchiseFeQuery lastName(String lastName);

        @MasterEntityQueryParameter(attribute = "emailId")
        FranchiseFeQuery emailId(String emailId);

        @MasterEntityQueryParameter(attribute = "password")
        FranchiseFeQuery password(String password);

        @MasterEntityQueryParameter(attribute = "contactNumber")
        FranchiseFeQuery contactNumber(String contactNumber);

        @MasterEntityQueryParameter(attribute = "status")
        FranchiseFeQuery status(int status);

        @MasterEntityQueryParameter(attribute = "feAddress")
        FranchiseFeQuery feAddress(String feAddress);

        @MasterEntityQueryParameter(attribute = "franchiseId")
        FranchiseFeQuery franchiseId(String franchiseId);

        @MasterEntityQueryParameter(attribute = "agencyId")
        FranchiseFeQuery agencyId(String agencyId);

        @MasterEntityQuerySort(attribute = "id")
        FranchiseFeQuery orderById();
    }
}
