package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.util.Date;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerOther {

    @MasterEntityAttribute
    public String getId();

    public void setId(String id);

    @MasterEntityAttribute
    public String getRetailerId();

    public void setRetailerId(String retailerId);

    @MasterEntityAttribute
    public int getIsAllowOnlyASP();

    public void setIsAllowOnlyASP(int isAllowOnlyASP);

    @MasterEntityAttribute
    public int getOnlyRetailerAuthForPickNDrop();

    public void setOnlyRetailerAuthForPickNDrop(int onlyRetailerAuthForPickNDrop);

    @MasterEntityAttribute
    public int getAdvPymtToServiceCenter();

    public void setAdvPymtToServiceCenter(int advPymtToServiceCenter);

    @MasterEntityAttribute
    public int getGetApprovalBeforeBuyBack();

    public void setGetApprovalBeforeBuyBack(int getApprovalBeforeBuyBack);

    @MasterEntityAttribute
    public int getTechEvaluationForBuyBack();

    public void setTechEvaluationForBuyBack(int techEvaluationForBuyBack);

    @MasterEntityAttribute
    public int getTechEvaluationForAdvanceExchange();

    public void setTechEvaluationForAdvanceExchange(int techEvaluationForAdvanceExchange);

    @MasterEntityAttribute
    public int getDeliveryProofRequired();

    public void setDeliveryProofRequired(int deliveryProofRequired);

    @MasterEntityAttribute
    public int getEnableTicketUpload();

    public void setEnableTicketUpload(int enableTicketUpload);

    @MasterEntityAttribute
    public int getVolumetricWeightDenomination();

    public void setVolumetricWeightDenomination(int volumetricWeightDenomination);

    @MasterEntityAttribute
    double getCashHandlingPerc();

    void setCashHandlingPerc(double cashHandlingPerc);

    @MasterEntityAttribute
    double getWhiteLabelingCharge();

    void setWhiteLabelingCharge(double whiteLabelingCharge);

    @MasterEntityAttribute
    double getOtherFinancialPerc();

    void setOtherFinancialPerc(double otherFinancailPerc);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityAttribute
    String getTurnAroundTime();

    void setTurnAroundTime(String turnAroundTime);

    @MasterEntityAttribute
    String getTransportMode();

    void setTransportMode(String transportMode);

    /**
     * prem code 20-06-2018
     *
     * @author r IS_MOBILE_PRODUCT_ADD_ALLOW_ ->Added new column in Database
     */

    @MasterEntityAttribute
    int getIsMobileProductAddAllow();

    void setIsMobileProductAddAllow(int isMobileProductAddAllow);

    @MasterEntityAttribute
    int getPayin();

    void setPayin(int payin);

    @MasterEntityAttribute
    int getPayout();

    void setPayout(int payout);

    @MasterEntityAttribute
    int getIsPackingMaterialRequired();

    void setIsPackingMaterialRequired(int isPackingMaterialRequired);

    @MasterEntityAttribute
    int getIsTechEvalRequired();

    void setIsTechEvalRequired(int isTechEvalRequired);

    @MasterEntityQuery
    public interface RetailerOtherQuery extends Query<RetailerOtherQuery, RetailerOther> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        public RetailerOtherQuery id(String id);

        @MasterEntityQueryParameter(attribute = "retailerId")
        public RetailerOtherQuery retailerId(String retailerId);

        @MasterEntityQueryParameter(attribute = "isTechEvalRequired")
        public RetailerOtherQuery isTechEvalRequired(int isTechEvalRequired);

        @MasterEntityQueryParameter(attribute = "isPackingMaterialRequired")
        public RetailerOtherQuery isPackingMaterialRequired(int isPackingMaterialRequired);

        @MasterEntityQueryParameter(attribute = "isAllowOnlyASP")
        public RetailerOtherQuery isAllowOnlyASP(int isAllowOnlyASP);

        @MasterEntityQueryParameter(attribute = "onlyRetailerAuthForPickNDrop")
        public RetailerOtherQuery onlyRetailerAuthForPickNDrop(int onlyRetailerAuthForPickNDrop);

        @MasterEntityQueryParameter(attribute = "advPymtToServiceCenter")
        public RetailerOtherQuery advPymtToServiceCenter(int advPymtToServiceCenter);

        @MasterEntityQueryParameter(attribute = "getApprovalBeforeBuyBack")
        public RetailerOtherQuery getApprovalBeforeBuyBack(int getApprovalBeforeBuyBack);

        @MasterEntityQueryParameter(attribute = "deliveryProofRequired")
        public RetailerOtherQuery deliveryProofRequired(int deliveryProofRequired);

        @MasterEntityQueryParameter(attribute = "techEvaluationForBuyBack")
        public RetailerOtherQuery techEvaluationForBuyBack(int techEvaluationForBuyBack);

        @MasterEntityQueryParameter(attribute = "techEvaluationForAdvanceExchange")
        public RetailerOtherQuery techEvaluationForAdvanceExchange(int techEvaluationForAdvanceExchange);

        @MasterEntityQueryParameter(attribute = "turnAroundTime")
        public RetailerOtherQuery turnAroundTime(String turnAroundTime);

        @MasterEntityQueryParameter(attribute = "transportMode")
        public RetailerOtherQuery transportMode(String transportMode);

        /**
         * prem code 20-06-2018
         *
         * @return added this column-> isMobileProductAddAllow
         */
        @MasterEntityQueryParameter(attribute = "isMobileProductAddAllow")
        public RetailerOtherQuery isMobileProductAddAllow(int isMobileProductAddAllow);

        @MasterEntityQueryParameter(attribute = "payin")
        public RetailerOtherQuery payin(int payin);

        @MasterEntityQueryParameter(attribute = "payout")
        public RetailerOtherQuery payout(int payout);

        @MasterEntityQuerySort(attribute = "id")
        public RetailerOtherQuery orderById();

        @MasterEntityQuerySort(attribute = "retailerId")
        public RetailerOtherQuery orderByRetailerId();

        @MasterEntityQuerySort(attribute = "isAllowOnlyASP")
        public RetailerOtherQuery orderByIsAllowOnlyASP();

        @MasterEntityQuerySort(attribute = "onlyRetailerAuthForPickNDrop")
        public RetailerOtherQuery orderByOnlyRetailerAuthForPickNDrop();

        @MasterEntityQuerySort(attribute = "turnAroundTime")
        public RetailerOtherQuery orderByTurnAroundTime();

        @MasterEntityQuerySort(attribute = "transportMode")
        public RetailerOtherQuery orderByTransportMode();

    }
}
