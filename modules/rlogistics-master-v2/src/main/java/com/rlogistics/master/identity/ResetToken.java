package com.rlogistics.master.identity;

import java.sql.Timestamp;
import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ResetToken {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getEmail();

	void setEmail(String email);
	
	@MasterEntityAttribute
	String getToken();

	void setToken(String token);

	@MasterEntityAttribute
	Timestamp getExpiryDate();

	void setExpiryDate(Timestamp expiryDate);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ResetTokenQuery extends Query<ResetTokenQuery,ResetToken>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		ResetTokenQuery id(String id);
		@MasterEntityQueryParameter(attribute="email")
		ResetTokenQuery email(String email);
		@MasterEntityQueryParameter(attribute="token")
		ResetTokenQuery token(String token);
			
		@MasterEntityQuerySort(attribute="id")
		ResetTokenQuery orderById();
		@MasterEntityQuerySort(attribute="email")
		ResetTokenQuery orderByEmail();
	}
}
