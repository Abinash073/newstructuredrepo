package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface EwasteMisBulkTransfer {
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getProductCategory();

	void setProductCategory(String productCategory);

	@MasterEntityAttribute
	String getQuantity();

	void setQuantity(String quantity);

	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);
	
	@MasterEntityAttribute
	String getGrade();

	void setGrade(String grade);
	
	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);


	@MasterEntityAttribute
	String getBarcode();

	void setBarcode(String barcode);
	@MasterEntityQuery
	public interface EwasteMisBulkTransferQuery extends Query<EwasteMisBulkTransferQuery,EwasteMisBulkTransfer>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		EwasteMisBulkTransferQuery id(String id);
		@MasterEntityQueryParameter(attribute="processId")
		EwasteMisBulkTransferQuery processId(String processId);
		@MasterEntityQueryParameter(attribute="retailerId")
		EwasteMisBulkTransferQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="ticketNo")
		EwasteMisBulkTransferQuery ticketNo(String ticketNo);
		@MasterEntityQueryParameter(attribute="productCategory")
		EwasteMisBulkTransferQuery productCategory(String productCategory);		
		@MasterEntityQueryParameter(attribute="quantity")
		EwasteMisBulkTransferQuery quantity(String quantity);
		@MasterEntityQueryParameter(attribute="grade")
		EwasteMisBulkTransferQuery grade(String grade);
		@MasterEntityQueryParameter(attribute="status")
		EwasteMisBulkTransferQuery status(String status);
		@MasterEntityQueryParameter(attribute = "barcode")
		EwasteMisBulkTransferQuery barcode(String barcode);
		
		@MasterEntityQuerySort(attribute="id")
		EwasteMisBulkTransferQuery orderById();

	}

}
