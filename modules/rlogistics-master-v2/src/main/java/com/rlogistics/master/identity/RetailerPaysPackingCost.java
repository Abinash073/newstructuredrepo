package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerPaysPackingCost {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getCategoryId();
	
	public void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	public String getBrandId();
	
	public void setBrandId(String brandId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerPaysPackingCostQuery extends Query<RetailerPaysPackingCostQuery,RetailerPaysPackingCost>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerPaysPackingCostQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerPaysPackingCostQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerPaysPackingCostQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="brandId")
		public RetailerPaysPackingCostQuery brandId(String brandId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerPaysPackingCostQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerPaysPackingCostQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="categoryId")
		public RetailerPaysPackingCostQuery orderByCategoryId();	
		@MasterEntityQuerySort(attribute="brandId")
		public RetailerPaysPackingCostQuery orderByBrandId();	
	}
}
