package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface LspDetail {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getCityId();

	void setCityId(String cityId);

	@MasterEntityAttribute
	String getAddress();

	void setAddress(String address);

	@MasterEntityAttribute
	String getContactNumber();

	void setContactNumber(String contactNumber);

	@MasterEntityQuery
	public interface LspDetailQuery extends Query<LspDetailQuery, LspDetail> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		LspDetailQuery id(String id);

		@MasterEntityQueryParameter(attribute = "name")
		LspDetailQuery name(String name);

		@MasterEntityQueryParameter(attribute = "code")
		LspDetailQuery code(String code);

		@MasterEntityQueryParameter(attribute = "cityId")
		LspDetailQuery cityId(String cityId);

		@MasterEntityQueryParameter(attribute = "address")
		LspDetailQuery address(String address);

		@MasterEntityQueryParameter(attribute = "contactNumber")
		LspDetailQuery contactNumber(String contactNumber);

		@MasterEntityQuerySort(attribute = "id")
		LspDetailQuery orderById();

		@MasterEntityQuerySort(attribute = "name")
		LspDetailQuery orderByName();

		@MasterEntityQuerySort(attribute = "code")
		LspDetailQuery orderByCode();

		@MasterEntityQuerySort(attribute = "cityId")
		LspDetailQuery orderByCityId();

	}
}
