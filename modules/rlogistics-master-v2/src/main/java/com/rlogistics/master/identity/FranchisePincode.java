package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
@MasterEntity
@MasterEntityLeadsToRestController
public interface FranchisePincode {
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	
	
	@MasterEntityAttribute
	String getPincode();
	
	void setPincode(String pincode);
	
	@MasterEntityAttribute
	String getFranchiseName();
	
	void setFranchiseName(String franchiseName);
	
	
	@MasterEntityAttribute
	String getAgencyName();
	
	void setAgencyName(String agencyName);

	
	
	@MasterEntityAttribute
	String getParentLocation();
	
	void setParentLocation(String parentLocation);
	
	
	
	@MasterEntityQuery
	
	public interface FranchisePincodeQuery extends Query<FranchisePincodeQuery,FranchisePincode>
	{
		
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		FranchisePincodeQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="pincode")
		FranchisePincodeQuery pincode(String pincode);
		
		@MasterEntityQueryParameter(attribute="franchiseName")
		FranchisePincodeQuery franchiseName(String franchiseName);
		
		
		@MasterEntityQueryParameter(attribute="agencyName")
		FranchisePincodeQuery agencyName(String agencyName);
		
		@MasterEntityQueryParameter(attribute="parentLocation")
		FranchisePincodeQuery parentLocation(String parentLocation);
	}
	
	
	
	

}
 