package com.rlogistics.master.identity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.util.Date;
import java.util.Queue;

@MasterEntity(tableName ="RL_MD_EXOTEL_LOG")
@MasterEntityLeadsToRestController
public interface ExotelLog {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getProcessId();

    void setProcessId(String processId);

    @MasterEntityAttribute
    String getCustomerNumber();

    void setCustomerNumber(String customerNumber);

    @MasterEntityAttribute
    String getAssignee();

    void setAssignee(String assignee);

    @MasterEntityAttribute
    String getSid();

    void setSid(String sid);

    @MasterEntityAttribute
    String getStatus();

    void setStatus(String status);

    @MasterEntityAttribute
    String getExotelNumber();

    void setExotelNumber(String exotelNumber);

    @MasterEntityAttribute
    String getTicketNumber();

    void setTicketNumber(String ticketNumber);

    @MasterEntityAttribute
    String getDateCreated();

    void setDateCreated(String dateCreated);

    @MasterEntityAttribute
    String getRecordingUrl();

    void setRecordingUrl(String RecordingUrl);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    Date getCreatedOn();

    void setCreatedOn(Date createdOn);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date modifiedOn);

    @MasterEntityAttribute
    String getCity();

    void setCity(String city);

    @MasterEntityAttribute
    String getDateUpdated();

    void setDateUpdated(String dateUpdated);

    @MasterEntityQuery
    public interface ExotelLogQuery extends Query<ExotelLogQuery,ExotelLog>{

        @MasterEntityQueryParameter(attribute = "id",idAttribute = true)
        ExotelLogQuery id(String Id);
        @MasterEntityQueryParameter(attribute = "processId",codeAttribute = true)
        ExotelLogQuery processId(String processId);
        @MasterEntityQueryParameter(attribute = "customerNumber")
        ExotelLogQuery customerNumber(String customerNumber);
        @MasterEntityQueryParameter(attribute = "assignee" )
        ExotelLogQuery assignee(String assignee);
        @MasterEntityQueryParameter(attribute = "sid")
        ExotelLogQuery sid(String sid);
        @MasterEntityQueryParameter(attribute  = "exotelNumber")
        ExotelLogQuery exotelNumber(String exotelNumber);
        @MasterEntityQueryParameter(attribute = "dateCreated")
        ExotelLogQuery dateCreated(String dateCreated);
        @MasterEntityQueryParameter(attribute = "recordingUrl")
        ExotelLogQuery recordingUrl(String recordingUrl);
        @MasterEntityQueryParameter(attribute =  "ticketNumber")
        ExotelLogQuery ticketNumber(String ticketNumber);
        @MasterEntityQueryParameter(attribute = "city")
        ExotelLogQuery city(String city);
        @MasterEntityQueryParameter(attribute = "dateUpdated")
        ExotelLogQuery dateUpdated(String dateUpdated);

        @MasterEntityQuerySort(attribute = "createdOn")
        ExotelLogQuery orderByCreatedOn();





    }




}
