package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Payment {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getExternalReferenceId();

	void setExternalReferenceId(String externalReferenceId);

	@MasterEntityAttribute
	String getPaymentProvider();

	void setPaymentProvider(String paymentProvider);

	@MasterEntityAttribute
	String getAmount();

	void setAmount(String amount);

	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);

	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getRemarks();

	void setRemarks(String remarks);

	@MasterEntityAttribute
	String getPhone();

	void setPhone(String phone);

	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);

	@MasterEntityAttribute
	String getPaymentRequest();

	void setPaymentRequest(String paymentRequest);

	@MasterEntityAttribute
	String getPaymentResponse();

	void setPaymentResponse(String paymentResponse);

	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);

	@MasterEntityAttribute
	String getPaymentType();

	void setPaymentType(String paymentType);
	
	@MasterEntityAttribute
	String getLocation();
	
	void setLocation(String location);

	@MasterEntityAttribute
	String getCreatedOn();
	
	void setCreatedOn(String createdOn);


	@MasterEntityAttribute
	String getUpdateOn();
	
	void setUpdateOn(String updateOn);

	@MasterEntityQuery
	public interface PaymentQuery extends Query<PaymentQuery, Payment> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		PaymentQuery id(String id);

		@MasterEntityQueryParameter(attribute = "externalReferenceId")
		PaymentQuery externalReferenceId(String externalReferenceId);

		@MasterEntityQueryParameter(attribute = "paymentProvider")
		PaymentQuery paymentProvider(String paymentProvider);

		@MasterEntityQueryParameter(attribute = "amount")
		PaymentQuery amount(String amount);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		PaymentQuery ticketNo(String ticketNo);

		@MasterEntityQueryParameter(attribute = "retailerId")
		PaymentQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute = "remarks")
		PaymentQuery remarks(String remarks);

		@MasterEntityQueryParameter(attribute = "phone")
		PaymentQuery phone(String phone);

		@MasterEntityQueryParameter(attribute = "paymentRequest")
		PaymentQuery paymentRequest(String paymentRequest);

		@MasterEntityQueryParameter(attribute = "paymentResponse")
		PaymentQuery paymentResponse(String paymentResponse);

		@MasterEntityQueryParameter(attribute = "processId")
		PaymentQuery processId(String processId);

		@MasterEntityQueryParameter(attribute = "status")
		PaymentQuery status(String status);

		@MasterEntityQueryParameter(attribute = "paymentType")
		PaymentQuery paymentType(String paymentType);
		
		@MasterEntityQueryParameter(attribute = "location")
		PaymentQuery location(String location);

		@MasterEntityQuerySort(attribute = "id")
		PaymentQuery orderById();

		@MasterEntityQuerySort(attribute = "updateOn")
		PaymentQuery orderByUpdateOn();

		@MasterEntityQuerySort(attribute = "createdOn")
		PaymentQuery orderByCreatedOn();
	}
}
