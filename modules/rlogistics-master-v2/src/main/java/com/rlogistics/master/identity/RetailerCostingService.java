package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerCostingService {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getCostingServiceCode();

	void setCostingServiceCode(String costingServiceCode);
	
	@MasterEntityAttribute
	float getCost();

	void setCost(float cost);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int status);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerCostingServiceQuery extends Query<RetailerCostingServiceQuery,RetailerCostingService>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerCostingServiceQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerCostingServiceQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute="costingServiceCode")
		public RetailerCostingServiceQuery costingServiceCode(String costingServiceCode);
		
		@MasterEntityQueryParameter(attribute="cost")
		public RetailerCostingServiceQuery cost(float cost);
		
		@MasterEntityQueryParameter(attribute="status")
		public RetailerCostingServiceQuery status(int status);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerCostingServiceQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerCostingServiceQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="costingServiceCode")
		public RetailerCostingServiceQuery orderByCostingServiceCode();		
		
		@MasterEntityQuerySort(attribute="cost")
		public RetailerCostingServiceQuery orderByCost();		
	}
}
