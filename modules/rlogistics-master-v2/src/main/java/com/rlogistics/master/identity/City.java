package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface City {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getState();

	void setState(String state);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface CityQuery extends Query<CityQuery,City>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		CityQuery id(String id);
		@MasterEntityQueryParameter(attribute="name")
		CityQuery name(String name);
		@MasterEntityQueryParameter(attribute="state")
		CityQuery state(String state);
		
		@MasterEntityQuerySort(attribute="id")
		CityQuery orderById();
		@MasterEntityQuerySort(attribute="name")
		CityQuery orderByName();
	}
}
