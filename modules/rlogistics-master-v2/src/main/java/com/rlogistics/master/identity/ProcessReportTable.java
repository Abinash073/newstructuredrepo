package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ProcessReportTable {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getProcessDeploymentId();
	
	void setProcessDeploymentId(String processDeploymentId);
	
	@MasterEntityAttribute
	String getReportTable();
	
	void setReportTable(String reportTable);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ProcessReportTableQuery extends Query<ProcessReportTableQuery,ProcessReportTable>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProcessReportTableQuery id(String id);
		@MasterEntityQueryParameter(attribute = "processDeploymentId")
		ProcessReportTableQuery processDeploymentId(String processDeploymentId);

		@MasterEntityQuerySort(attribute = "id")
		ProcessReportTableQuery orderById();
	}
}
