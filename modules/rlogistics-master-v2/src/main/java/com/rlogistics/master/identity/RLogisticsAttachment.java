package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_ATTACHMENT")
@MasterEntityLeadsToRestController
public interface RLogisticsAttachment {
	@MasterEntityAttribute
	String getId();
	void setId(String id);

	@MasterEntityAttribute
	String getName();
	void setName(String name);
	
	@MasterEntityAttribute
	String getKey1();
	void setKey1(String key1);
	
	@MasterEntityAttribute
	String getKey2();
	void setKey2(String key2);
	
	@MasterEntityAttribute
	String getKey3();
	void setKey3(String key3);
	
	@MasterEntityAttribute
	String getKey4();
	void setKey4(String key4);
	
	@MasterEntityAttribute
	String getLatitude();
	void setLatitude(String latitude);
	
	@MasterEntityAttribute
	String getLongitude();
	void setLongitude(String location);
	
	@MasterEntityAttribute
	String getLocation();
	void setLocation(String location);
	
	@MasterEntityAttribute
	byte[] getContent();
	void setContent(byte[] bytes);
	
	@MasterEntityAttribute
	String getContentType();
	void setContentType(String contentType);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RLogisticsAttachmentQuery extends Query<RLogisticsAttachmentQuery,RLogisticsAttachment>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RLogisticsAttachmentQuery id(String id);

		@MasterEntityQueryParameter(attribute = "name")
		RLogisticsAttachmentQuery name(String name);
		
		@MasterEntityQueryParameter(attribute = "key1")
		RLogisticsAttachmentQuery key1(String key1);
		
		@MasterEntityQueryParameter(attribute = "key2")
		RLogisticsAttachmentQuery key2(String key2);
		
		@MasterEntityQueryParameter(attribute = "key3")
		RLogisticsAttachmentQuery key3(String key3);
		
		@MasterEntityQueryParameter(attribute = "key4")
		RLogisticsAttachmentQuery key4(String key4);
		
		@MasterEntityQueryParameter(attribute = "latitude")
		RLogisticsAttachmentQuery latitude(String latitude);
		
		@MasterEntityQueryParameter(attribute = "longitude")
		RLogisticsAttachmentQuery longitude(String longitude);
		
		@MasterEntityQueryParameter(attribute = "location")
		RLogisticsAttachmentQuery location(String location);
		
		@MasterEntityQuerySort(attribute = "id")
		RLogisticsAttachmentQuery orderById();

		@MasterEntityQuerySort(attribute = "name")
		RLogisticsAttachmentQuery orderByName();	

		@MasterEntityQuerySort(attribute = "key1")
		RLogisticsAttachmentQuery orderByKey1();	

		@MasterEntityQuerySort(attribute = "key2")
		RLogisticsAttachmentQuery orderByKey2();	
		
		@MasterEntityQuerySort(attribute = "key3")
		RLogisticsAttachmentQuery orderByKey3();
		
		@MasterEntityQuerySort(attribute = "key4")
		RLogisticsAttachmentQuery orderByKey4();
	}
}
