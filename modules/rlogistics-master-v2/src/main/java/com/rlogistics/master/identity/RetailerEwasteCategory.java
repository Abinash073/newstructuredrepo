package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.ProductCategory.ProductCategoryQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerEwasteCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	String getMasterCategoryName();

	void setMasterCategoryName(String masterCategoryName);
	
	@MasterEntityQuery
	public interface RetailerEwasteCategoryQuery extends Query<RetailerEwasteCategoryQuery,RetailerEwasteCategory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerEwasteCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		RetailerEwasteCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerEwasteCategoryQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		RetailerEwasteCategoryQuery code(String code);
		@MasterEntityQueryParameter(attribute = "masterCategoryName")
		RetailerEwasteCategoryQuery masterCategoryName(String masterCategoryName);

		@MasterEntityQuerySort(attribute = "id")
		RetailerEwasteCategoryQuery orderById();
	}
}
