package com.rlogistics.master.identity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.sql.Timestamp;
import java.util.Date;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FlBarcodeEntity {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getFlBarcode();

    void setFlBarcode(String flBarcode);

    @MasterEntityAttribute
    int getFlBarcodeStatus();

    void setFlBarcodeStatus(int flBarcodeStatus);

    @JsonIgnore
    @MasterEntityAttribute
    Timestamp getGeneratedDate();

    void setGeneratedDate(Timestamp generatedDate);

    @JsonIgnore
    @MasterEntityAttribute
    int getItemCount();

    void setItemCount(int itemCount);

    @MasterEntityAttribute
    String getAssignedToLocBy();

    void setAssignedToLocBy(String assignedToLocBy);

    @MasterEntityAttribute
    String getAssignedLoc();

    void setAssignedLoc(String assignedLoc);

    @MasterEntityAttribute
    String getAssignedToFeBy();

    void setAssignedToFeBy(String assignedToFeBy);

    @MasterEntityAttribute
    String getAssignedFe();

    void setAssignedFe(String assignedFe);

//    @MasterEntityAttribute
//    String getAssignedBy();
//
//    void setAssignedBy(String assignedById);
//
//    @MasterEntityAttribute
//    String getAssignedTo();
//
//    void setAssignedTo(String assignedToId);

    @MasterEntityAttribute
    String getStartingFlBarcode();

    void setStartingFlBarcode(String startingBarcode);

    @MasterEntityAttribute
    String getEndingFlBarcode();

    void setEndingFlBarcode(String endingBarcode);

    @MasterEntityAttribute
    int getPrintStatus();

    void setPrintStatus(int printStatus);

    @MasterEntityAttribute
    String getStartingFlBarcodeRange();

    void setStartingFlBarcodeRange(String startingBarcodeRange);

    @MasterEntityAttribute
    String getEndingFlBarcodeRange();

    void setEndingFlBarcodeRange(String endingBarcodeRange);

    @MasterEntityAttribute
    String getFranchiseName();

    void setFranchiseName(String franchiseName);

    @MasterEntityAttribute
    String getAgencyName();

    void setAgencyName(String agencyName);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getCreatedOn();

    void setCreatedOn(String createdOn);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);


    @MasterEntityQuery
    interface FlBarcodeEntityQuery extends Query<FlBarcodeEntityQuery, FlBarcodeEntity> {

        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        FlBarcodeEntityQuery id(String id);

        @MasterEntityQueryParameter(attribute = "flBarcode")
        FlBarcodeEntityQuery flBarcode(String flBarcode);

        @MasterEntityQueryParameter(attribute = "flBarcodeStatus")
        FlBarcodeEntityQuery flBarcodeStatus(int flBarcodeStatus);

        @MasterEntityQueryParameter(attribute = "startingFlBarcode")
        FlBarcodeEntityQuery startingFlBarcode(String startingFlBarcode);

        @MasterEntityQueryParameter(attribute = "endingFlBarcode")
        FlBarcodeEntityQuery endingFlBarcode(String endingFlBarcode);

        @MasterEntityQueryParameter(attribute = "itemCount")
        FlBarcodeEntityQuery itemCount(int itemCount);

        @MasterEntityQueryParameter(attribute = "assignedToLocBy")
        FlBarcodeEntityQuery assignedToLocBy(String assignedToLocBy);

        @MasterEntityQueryParameter(attribute = "assignedLoc")
        FlBarcodeEntityQuery assignedLoc(String assignedLoc);

        @MasterEntityQueryParameter(attribute = "assignedToFeBy")
        FlBarcodeEntityQuery assignedToFeBy(String assignedToFeBy);

        @MasterEntityQueryParameter(attribute = "assignedFe")
        FlBarcodeEntityQuery assignedFe(String assignedFe);

//        @MasterEntityQueryParameter(attribute = "assignedBy")
//        FlBarcodeEntityQuery assignedBy(String assignedBy);
//
//        @MasterEntityQueryParameter(attribute = "assignedTo")
//        FlBarcodeEntityQuery assignedTo(String assignedTo);

        @MasterEntityQueryParameter(attribute = "printStatus")
        FlBarcodeEntityQuery printStatus(int printStatus);

        @MasterEntityQueryParameter(attribute = "startingFlBarcodeRange")
        FlBarcodeEntityQuery startingFlBarcodeRange(String startingFlBarcodeRange);

        @MasterEntityQueryParameter(attribute = "endingFlBarcodeRange")
        FlBarcodeEntityQuery endingFlBarcodeRange(String endingFlBarcodeRange);

        @MasterEntityQueryParameter(attribute = "franchiseName")
        FlBarcodeEntityQuery franchiseName(String franchiseName);

        @MasterEntityQueryParameter(attribute = "agencyName")
        FlBarcodeEntityQuery agencyName(String agencyName);

        @MasterEntityQueryParameter(attribute = "createdOn")
        FlBarcodeEntityQuery createdOn(String createdOn);


        @MasterEntityQuerySort(attribute = "id")
        FlBarcodeEntityQuery orderById();


    }
}
