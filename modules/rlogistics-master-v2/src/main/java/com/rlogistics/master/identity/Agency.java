package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Agency {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	 String getAgencyName();
	
	void setAgencyName(String agencyName);
	
	
	@MasterEntityAttribute
	String getFranchiseName();
	void setFranchiseName(String franchiseName );
	
	
	@MasterEntityAttribute
	String getAgencyAddress();
	 
	void setAgencyAddress(String agencyAddress);
	
	
	@MasterEntityAttribute
	String getAgencyContactPerson();
	
	void setAgencyContactPerson(String agencyContactPerson);
	
	
	@MasterEntityAttribute
	String getAgencyContactNumber();
	
	void setAgencyContactNumber(String agencyContactNumber);
	
	@MasterEntityAttribute
	String getNoOfBarcodesAssigned();
	
	void setNoOfBarcodesAssigned(String noOfBarcodesAssigned);
	
	@MasterEntityQuery
	public interface AgencyQuery extends Query<AgencyQuery,Agency>
	{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		AgencyQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="agencyName")
		AgencyQuery agencyName(String agencyName);
		
		@MasterEntityQueryParameter(attribute="franchiseName")
		AgencyQuery franchiseName(String franchiseName);
		
		@MasterEntityQueryParameter(attribute="agencyAddress")
		AgencyQuery agencyAddress(String agencyAddress);
		
		@MasterEntityQueryParameter(attribute="agencyContactPerson")
		AgencyQuery agencyContactPerson(String agencyContactPerson);
		
		@MasterEntityQueryParameter(attribute="agencyContactNumber")
		AgencyQuery agencyContactNumber(String agencyContactNumber);
		
		
		@MasterEntityQueryParameter(attribute="noOfBarcodesAssigned")
		AgencyQuery noOfBarcodesAssigned(String noOfBarcodesAssigned);
		
		
		
	}
}
