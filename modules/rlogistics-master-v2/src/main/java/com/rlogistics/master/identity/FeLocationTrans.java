package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FeLocationTrans {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getLatitude();
	
	void setLatitude(String latitude);
	
	@MasterEntityAttribute
	String getLongitude();
	
	void setLongitude(String longitude);
	
	@MasterEntityAttribute
	String getFeName();
	
	void setFeName(String feName);
	
	@MasterEntityAttribute
	String getDate();
	
	void setDate(String date);

	@MasterEntityAttribute
	String getTotalDistance();

	void setTotalDistance(String totalDistance);

	@MasterEntityAttribute
	String getTicket();

	void setTicket(String ticket);

	@MasterEntityAttribute
	String getCity();

	void setCity(String city);
	
	@MasterEntityQuery
	public interface FeLocationTransQuery extends Query<FeLocationTransQuery,FeLocationTrans>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public FeLocationTransQuery id(String id);

		@MasterEntityQueryParameter(attribute="latitude")
		public FeLocationTransQuery latitude(String latitude);
		
		@MasterEntityQueryParameter(attribute="longitude")
		public FeLocationTransQuery longitude(String longitude);
		
		@MasterEntityQueryParameter(attribute="feName")
		public FeLocationTransQuery feName(String feName);
		
		@MasterEntityQueryParameter(attribute="date")
		public FeLocationTransQuery date(String date);

		@MasterEntityQueryParameter(attribute = "totalDistance")
		public FeLocationTransQuery totalDistance(String totalDistance);
		
		@MasterEntityQuerySort(attribute="date")
		public FeLocationTransQuery orderByDate();

		@MasterEntityQueryParameter(attribute = "ticket")
		public FeLocationTransQuery ticket(String ticket);

		@MasterEntityQueryParameter(attribute = "city")
		public FeLocationTransQuery city(String city);
	}
}
