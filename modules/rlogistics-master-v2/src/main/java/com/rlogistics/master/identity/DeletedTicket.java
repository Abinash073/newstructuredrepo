package com.rlogistics.master.identity;


import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.sql.Timestamp;

@MasterEntity
@MasterEntityLeadsToRestController
public interface DeletedTicket {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getProcessId();

    void setProcessId(String processId);

    @MasterEntityAttribute
    String getRlTicketNo();

    void setRlTicketNo(String rlTicketNo);

    @MasterEntityAttribute
    String getTaskName();

    void setTaskName(String taskName);

    @MasterEntityAttribute
    String getConsumerName();

    void setConsumerName(String consumerName);

    @MasterEntityAttribute
    Timestamp getDeletedDateTime();

    void setDeletedDateTime(Timestamp deletedDateTime);

    @MasterEntityAttribute
    String getDeletedBy();

    void setDeletedBy(String deletedBy);

    @MasterEntityAttribute
    String getCategory();

    void setCategory(String category);

    @MasterEntityAttribute
    String getBrand();

    void setBrand(String brand);

    @MasterEntityAttribute
    String getRetailer();

    void setRetailer(String retailer);

    @MasterEntityAttribute
    byte[] getVariablesBytearrayId();

    void setVariablesBytearrayId(byte[] variablesBytearrayId);

    @MasterEntityQuery
    public interface DeletedTicketQuery extends Query<DeletedTicketQuery, DeletedTicket> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        DeletedTicketQuery id(String id);

        @MasterEntityQueryParameter(attribute = "processId")
        DeletedTicketQuery processId(String processId);

        @MasterEntityQueryParameter(attribute = "rlTicketNo")
        DeletedTicketQuery rlTicketNo(String rlTicketNo);

        @MasterEntityQuerySort(attribute = "id")
        DeletedTicketQuery orderById();
    }
}
