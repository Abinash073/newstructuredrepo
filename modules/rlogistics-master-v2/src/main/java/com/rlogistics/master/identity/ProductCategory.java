package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ProductCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getMasterCategoryId();

	void setMasterCategoryId(String masterCategoryId);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface ProductCategoryQuery extends Query<ProductCategoryQuery,ProductCategory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProductCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		ProductCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		ProductCategoryQuery code(String code);
		@MasterEntityQueryParameter(attribute = "masterCategoryId")
		ProductCategoryQuery masterCategoryId(String masterCategoryId);

		@MasterEntityQuerySort(attribute = "id")
		ProductCategoryQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		ProductCategoryQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		ProductCategoryQuery orderByCode();
		@MasterEntityQuerySort(attribute = "masterCategoryId")
		ProductCategoryQuery orderByMasterCategoryId();
	}

}
