package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.Product.ProductQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ProductPacking {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProductId();

	void setProductId(String productId);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String productId);
	
	@MasterEntityAttribute
	String getPackagingTypeId();

	void setPackagingTypeId(String packagingTypeId);
	
	@MasterEntityAttribute
	int getType();

	void setType(int type);
	
	@MasterEntityAttribute
	String getPackingSpec();

	void setPackingSpec(String packingSpec);
	
	@MasterEntityQuery
	public interface ProductPackingQuery extends Query<ProductPackingQuery,ProductPacking>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProductPackingQuery id(String id);
		@MasterEntityQueryParameter(attribute = "productId")
		ProductPackingQuery productId(String productId);
		@MasterEntityQueryParameter(attribute = "packagingTypeId")
		ProductPackingQuery packagingTypeId(String packagingTypeId);
		@MasterEntityQueryParameter(attribute = "type")
		ProductPackingQuery type(int type);
		@MasterEntityQueryParameter(attribute = "packingSpec")
		ProductPackingQuery packingSpec(String packingSpec);
		@MasterEntityQueryParameter(attribute = "retailerId")
		ProductPackingQuery retailerId(String retailerId);
		
		@MasterEntityQuerySort(attribute = "id")
		ProductPackingQuery orderById();
	}
}
