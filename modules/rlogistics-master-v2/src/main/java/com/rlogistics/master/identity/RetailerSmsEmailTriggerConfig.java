package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerSmsEmailTriggerConfig {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getSmsEmailTriggerCode();

	void setSmsEmailTriggerCode(String smsEmailTriggerCode);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	int getIsEmailRequired();

	void setIsEmailRequired(int isEmailRequired);
	
	@MasterEntityAttribute
	int getIsSmsRequired();

	void setIsSmsRequired(int isSmsRequired);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerSmsEmailTriggerConfigQuery extends Query<RetailerSmsEmailTriggerConfigQuery,RetailerSmsEmailTriggerConfig>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		RetailerSmsEmailTriggerConfigQuery id(String id);
		@MasterEntityQueryParameter(attribute="smsEmailTriggerCode",codeAttribute=true)
		RetailerSmsEmailTriggerConfigQuery smsEmailTriggerCode(String smsEmailTriggerCode);
		@MasterEntityQueryParameter(attribute="serviceId")
		RetailerSmsEmailTriggerConfigQuery serviceId(String serviceId);
		@MasterEntityQueryParameter(attribute="retailerId")
		RetailerSmsEmailTriggerConfigQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="isEmailRequired")
		RetailerSmsEmailTriggerConfigQuery isEmailRequired(int isEmailRequired);
		@MasterEntityQueryParameter(attribute="isSmsRequired")
		RetailerSmsEmailTriggerConfigQuery isSmsRequired(int isSmsRequired);
		
		
		@MasterEntityQuerySort(attribute="id")
		RetailerSmsEmailTriggerConfigQuery orderById();
		@MasterEntityQuerySort(attribute="smsEmailTriggerCode")
		RetailerSmsEmailTriggerConfigQuery orderBySmsEmailTriggerCode();
		@MasterEntityQuerySort(attribute="retailerId")
		RetailerSmsEmailTriggerConfigQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="serviceId")
		RetailerSmsEmailTriggerConfigQuery orderByServiceId();
	}
}
