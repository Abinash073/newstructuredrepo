package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface BarcodeOffset {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);
	
	@MasterEntityAttribute
	String getPackagingTypeCode();

	void setPackagingTypeCode(String packagingTypeCode);
	
	@MasterEntityAttribute
	int getOffset();

	void setOffset(int offset);
	
	@MasterEntityQuery
	public interface BarcodeOffsetQuery extends Query<BarcodeOffsetQuery,BarcodeOffset>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		BarcodeOffsetQuery id(String id);
		@MasterEntityQueryParameter(attribute = "locationId")
		BarcodeOffsetQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "packagingTypeCode")
		BarcodeOffsetQuery packagingTypeCode(String packagingTypeCode);
		@MasterEntityQueryParameter(attribute = "offset")
		BarcodeOffsetQuery offset(int offset);
		
		@MasterEntityQuerySort(attribute = "id")
		BarcodeOffsetQuery orderById();
	}
}
