package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface LocationPincodes {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);

	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface LocationPincodesQuery extends Query<LocationPincodesQuery,LocationPincodes>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		LocationPincodesQuery id(String id);
		@MasterEntityQueryParameter(attribute = "locationId")
		LocationPincodesQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "pincode")
		LocationPincodesQuery pincode(String pincode);
		
		@MasterEntityQuerySort(attribute = "id")
		LocationPincodesQuery orderById();
		@MasterEntityQuerySort(attribute = "locationId")
		LocationPincodesQuery orderByLocationId();
		@MasterEntityQuerySort(attribute = "pincode")
		LocationPincodesQuery orderByPincode();
	}
}
