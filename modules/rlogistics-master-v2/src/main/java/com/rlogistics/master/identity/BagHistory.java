package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface BagHistory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);

	@MasterEntityAttribute
	String getDate();

	void setDate(String date);
	
	@MasterEntityAttribute
	String getBagsUsed();

	void setBagsUsed(String bagsUsed);
	
	@MasterEntityQuery
	public interface BagHistoryQuery extends Query<BagHistoryQuery,BagHistory>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		BagHistoryQuery id(String id);
		@MasterEntityQueryParameter(attribute="date")
		BagHistoryQuery date(String date);
		@MasterEntityQueryParameter(attribute="bagsUsed")
		BagHistoryQuery bagsUsed(String bagsUsed);
		@MasterEntityQueryParameter(attribute="processId")
		BagHistoryQuery processId(String processId);
		
		@MasterEntityQuerySort(attribute="id")
		BagHistoryQuery orderById();
	}
}

