package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_FAQ")
@MasterEntityLeadsToRestController
public interface FAQ {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getProductId();
	
	void setProductId(String productId);
	
	@MasterEntityAttribute
	String getBrandId();
	
	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	String getCategoryId();
	
	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getSubCategoryId();
	
	void setSubCategoryId(String subCategoryId);
	
	@MasterEntityAttribute
	String getFaq();

	void setFaq(String faq);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface FAQQuery extends Query<FAQQuery,FAQ>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		FAQQuery id(String id);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		FAQQuery code(String code);
		@MasterEntityQueryParameter(attribute="productId")
		FAQQuery productId(String productId);
		@MasterEntityQueryParameter(attribute="brandId")
		FAQQuery brandId(String brandId);
		@MasterEntityQueryParameter(attribute="categoryId")
		FAQQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="subCategoryId")
		FAQQuery subCategoryId(String subCategoryId);
		
		@MasterEntityQuerySort(attribute="id")
		FAQQuery orderById();
		@MasterEntityQuerySort(attribute="code")
		FAQQuery orderByCode();
		@MasterEntityQuerySort(attribute="productId")
		FAQQuery orderByProductId();
		@MasterEntityQuerySort(attribute="brandId")
		FAQQuery orderByBrandId();
		@MasterEntityQuerySort(attribute="categoryId")
		FAQQuery orderByCategoryId();
		@MasterEntityQuerySort(attribute="subCategoryId")
		FAQQuery orderBySubCategoryId();
	}
}
