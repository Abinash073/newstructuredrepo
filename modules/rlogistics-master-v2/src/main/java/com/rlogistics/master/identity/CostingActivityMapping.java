package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface CostingActivityMapping {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCostingActivityId();

	void setCostingActivityId(String costingActivtyId);
	
	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface CostingActivityMappingQuery extends Query<CostingActivityMappingQuery,CostingActivityMapping>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public CostingActivityMappingQuery id(String id);
		@MasterEntityQueryParameter(attribute="costingActivityId")
		public CostingActivityMappingQuery costingActivityId(String costingActivityId);
		@MasterEntityQueryParameter(attribute="categoryId")
		public CostingActivityMappingQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="serviceId")
		public CostingActivityMappingQuery serviceId(String serviceId);
		
		@MasterEntityQuerySort(attribute="id")
		public CostingActivityMappingQuery orderById();
		@MasterEntityQuerySort(attribute="costingActivityId")
		public CostingActivityMappingQuery orderByCostingActivityId();
		@MasterEntityQuerySort(attribute="categoryId")
		public CostingActivityMappingQuery orderByCategoryId();
		@MasterEntityQuerySort(attribute="serviceId")
		public CostingActivityMappingQuery orderByServiceId();
	}
}
