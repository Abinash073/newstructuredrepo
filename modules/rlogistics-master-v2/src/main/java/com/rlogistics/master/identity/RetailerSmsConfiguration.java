package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerSmsConfiguration {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getUsername();

	void setUsername(String username);
	
	@MasterEntityAttribute
	String getHash();

	void setHash(String hash);
	
	@MasterEntityAttribute
	String getSender();

	void setSender(String sender);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerSmsConfigurationQuery extends Query<RetailerSmsConfigurationQuery,RetailerSmsConfiguration>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerSmsConfigurationQuery id(String id);
		@MasterEntityQueryParameter(attribute = "username")
		RetailerSmsConfigurationQuery username(String username);
		@MasterEntityQueryParameter(attribute = "hash")
		RetailerSmsConfigurationQuery hash(String hash);
		@MasterEntityQueryParameter(attribute = "sender")
		RetailerSmsConfigurationQuery sender(String sender);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerSmsConfigurationQuery retailerId(String retailerId);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerSmsConfigurationQuery orderById();
	}
}
