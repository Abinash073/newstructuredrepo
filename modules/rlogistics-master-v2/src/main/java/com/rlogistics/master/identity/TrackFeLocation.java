package com.rlogistics.master.identity;

	import org.activiti.engine.query.Query;
	import com.rlogistics.master.MasterEntity;
	import com.rlogistics.master.MasterEntityAttribute;
	import com.rlogistics.master.MasterEntityLeadsToRestController;
	import com.rlogistics.master.MasterEntityQuery;
	import com.rlogistics.master.MasterEntityQueryParameter;
	import com.rlogistics.master.MasterEntityQuerySort;

	@MasterEntity
	@MasterEntityLeadsToRestController
	public interface TrackFeLocation {

		@MasterEntityAttribute
		String getId();

		void setId(String id);

		@MasterEntityAttribute
		String getTicketNo();

		void setTicketNo(String ticketNo);

		@MasterEntityAttribute
		String getFeName();

		void setFeName(String feName);
		
		@MasterEntityAttribute
		String getDbName();

		void setDbName(String dbName);
		
		@MasterEntityAttribute
		String getAppointmentDate();

		void setAppointmentDate(String appointmentDate);
		
		@MasterEntityAttribute
		String getCurrentLatitude();

		void setCurrentLatitude(String latitude);
		
		@MasterEntityAttribute
		String getCurrentLongitude();

		void setCurrentLongitude(String longitude);
		
		@MasterEntityAttribute
		String getTimeToLocation();

		void setTimeToLocation(String timeToLocation);
		
		@MasterEntityAttribute
		String getPriority();

		void setPriority(String priority);
		
		@MasterEntityAttribute
		String getCompletedDate();

		void setCompletedDate(String completedDate);
		
		@MasterEntityAttribute
		String getFeCity();

		void setFeCity(String feCity);
		
		@MasterEntityAttribute
		String getDelayStatus();

		void setDelayStatus(String delayStatus);
		
		@MasterEntityAttribute
		String getDestination();

		void setDestination(String destination);
		
		@MasterEntityAttribute
		String getLastAssign();

		void setLastAssign(String lastAssign);
		
		@MasterEntityAttribute
		String getDistance();

		void setDistance(String distance);

		

		@MasterEntityQuery
		public interface TrackFeLocationQuery extends Query<TrackFeLocationQuery, TrackFeLocation> {
			@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
			TrackFeLocationQuery id(String id);

			@MasterEntityQueryParameter(attribute = "ticketNo")
			TrackFeLocationQuery ticketNo(String ticketNo);

			@MasterEntityQueryParameter(attribute = "feName")
			TrackFeLocationQuery feName(String feName);
			
			@MasterEntityQueryParameter(attribute = "dbName")
			TrackFeLocationQuery dbName(String dbName);
			
			@MasterEntityQueryParameter(attribute = "appointmentDate")
			TrackFeLocationQuery appointmentDate(String appointmentDate);

			@MasterEntityQueryParameter(attribute = "currentLatitude")
			TrackFeLocationQuery currentLatitude(String currentLatitude);
			
			@MasterEntityQueryParameter(attribute = "currentLongitude")
			TrackFeLocationQuery currentLongitude(String currentLongitude);
			
			@MasterEntityQueryParameter(attribute = "timeToLocation")
			TrackFeLocationQuery timeToLocation(String timeToLocation);
			
			@MasterEntityQueryParameter(attribute = "priority")
			TrackFeLocationQuery priority(String priority);
			
			@MasterEntityQueryParameter(attribute = "completedDate")
			TrackFeLocationQuery completedDate(String completedDate);

			@MasterEntityQueryParameter(attribute = "feCity")
			TrackFeLocationQuery feCity(String feCity);
			
			@MasterEntityQueryParameter(attribute = "delayStatus")
			TrackFeLocationQuery delayStatus(String delayStatus);
			
			@MasterEntityQueryParameter(attribute = "lastAssign")
			TrackFeLocationQuery lastAssign(String lastAssign);
			
			@MasterEntityQueryParameter(attribute = "destination")
			TrackFeLocationQuery destination(String destination);
			
			@MasterEntityQueryParameter(attribute = "distance")
			TrackFeLocationQuery distance(String distance);
			
			@MasterEntityQuerySort(attribute = "id")
			TrackFeLocationQuery orderById();
	}


}
