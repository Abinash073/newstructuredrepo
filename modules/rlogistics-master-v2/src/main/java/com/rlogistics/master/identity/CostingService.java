package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface CostingService {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface CostingServiceQuery extends Query<CostingServiceQuery,CostingService>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public CostingServiceQuery id(String id);

		@MasterEntityQueryParameter(attribute="name")
		public CostingServiceQuery name(String name);
		
		@MasterEntityQueryParameter(attribute="code")
		public CostingServiceQuery code(String code);
		
		@MasterEntityQuerySort(attribute="id")
		public CostingServiceQuery orderById();
		
		@MasterEntityQuerySort(attribute="name")
		public CostingServiceQuery orderByName();
		
		@MasterEntityQuerySort(attribute="code")
		public CostingServiceQuery orderByCode();
	}
}
