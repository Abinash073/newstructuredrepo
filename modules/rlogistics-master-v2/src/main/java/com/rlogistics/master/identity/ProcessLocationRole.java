/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_LOCATION_ROLE")
@MasterEntityLeadsToRestController
public interface ProcessLocationRole{
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);

	@MasterEntityAttribute
	String getRoleId();

	void setRoleId(String roleId);

	@MasterEntityAttribute
	String getUsers();

	void setUsers(String users);

	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface ProcessLocationRoleQuery extends Query<ProcessLocationRoleQuery,ProcessLocationRole>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProcessLocationRoleQuery id(String id);
		@MasterEntityQueryParameter(attribute = "id")
		ProcessLocationRoleQuery locationRoleId(String locationRole);
		@MasterEntityQueryParameter(attribute = "roleId")
		ProcessLocationRoleQuery roleId(String roleId);
		@MasterEntityQueryParameter(attribute = "locationId")
		ProcessLocationRoleQuery locationId(String locationId);

		@MasterEntityQuerySort(attribute = "id")
		ProcessLocationRoleQuery orderByLocationRoleId();
		@MasterEntityQuerySort(attribute = "locationId")
		ProcessLocationRoleQuery orderByLocationId();
		@MasterEntityQuerySort(attribute = "roleId")
		ProcessLocationRoleQuery orderByRoleId();
	}

}
