package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Product {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);

	@MasterEntityAttribute
	String getSubCategoryId();
	
	void setSubCategoryId(String subCategoryId);
	
	@MasterEntityAttribute
	String getBrandId();
	
	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getModel();
	
	void setModel(String model);
	
	@MasterEntityAttribute
	String getPackagingTypeBoxId();
	
	void setPackagingTypeBoxId(String packagingTypeBoxId);
	
	@MasterEntityAttribute
	String getPackagingTypeCoverId();
	
	void setPackagingTypeCoverId(String packagingTypeCoverId);
	
	@MasterEntityAttribute
	String getPackagingTypeOtherId();
	
	void setPackagingTypeOtherId(String packagingTypeOtherId);
	
	@MasterEntityAttribute
	double getVolumetricWeight();
	
	void setVolumetricWeight(double volumetricWeight);
	
	@MasterEntityAttribute
	double getApproxValue();
	
	void setApproxValue(double approxValue);
	
	@MasterEntityAttribute
	double getActualWeight();
	
	void setActualWeight(double actualWeight);
	
	@MasterEntityAttribute
	double getLength();
	
	void setLength(double length);
	
	@MasterEntityAttribute
	double getBreadth();
	
	void setBreadth(double breadth);
	
	@MasterEntityAttribute
	double getHeight();
	
	void setHeight(double height);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface ProductQuery extends Query<ProductQuery,Product>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProductQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		ProductQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		ProductQuery code(String code);
		@MasterEntityQueryParameter(attribute = "subCategoryId")
		ProductQuery subCategoryId(String subCategoryId);
		@MasterEntityQueryParameter(attribute = "brandId")
		ProductQuery brandId(String brandId);
		@MasterEntityQueryParameter(attribute = "retailerId")
		ProductQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "model")
		ProductQuery model(String model);
		@MasterEntityQueryParameter(attribute = "packagingTypeBoxId")
		ProductQuery packagingTypeBoxId(String packagingTypeBoxId);
		@MasterEntityQueryParameter(attribute = "packagingTypeCoverId")
		ProductQuery packagingTypeCoverId(String packagingTypeCoverId);
		@MasterEntityQueryParameter(attribute = "volumetricWeight")
		ProductQuery volumetricWeight(double volumetricWeight);
		@MasterEntityQueryParameter(attribute = "approxValue")
		ProductQuery approxValue(double approxValue);
		@MasterEntityQueryParameter(attribute = "actualWeight")
		ProductQuery actualWeight(double actualWeight);
	
		@MasterEntityQuerySort(attribute = "id")
		ProductQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		ProductQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		ProductQuery orderByCode();
		@MasterEntityQuerySort(attribute = "subCategoryId")
		ProductQuery orderBySubCategoryId();
		@MasterEntityQuerySort(attribute = "brandId")
		ProductQuery orderByBrandId();
		@MasterEntityQuerySort(attribute = "retailerId")
		ProductQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "model")
		ProductQuery orderByModel();
		@MasterEntityQuerySort(attribute = "packageTypeBoxId")
		ProductQuery orderByPackagingTypeBoxId();
		@MasterEntityQuerySort(attribute = "volumetricWeight")
		ProductQuery orderByVolumetricWeight();
		@MasterEntityQuerySort(attribute = "approxValue")
		ProductQuery orderByApproxValue();
		@MasterEntityQuerySort(attribute = "actualWeight")
		ProductQuery orderByActualWeight();
	}
}
