package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PackagingMaterialMaster {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);
	
	@MasterEntityAttribute
	String getPackagingTypeCode();

	void setPackagingTypeCode(String packagingTypeCode);
	
	@MasterEntityAttribute
	int getReorderLevel();

	void setReorderLevel(int reorderLevel);
	
	@MasterEntityAttribute
	int getMaximumQuantityAllowed();

	void setMaximumQuantityAllowed(int maximumQuantityAllowed);
	
	@MasterEntityAttribute
	int getAvailableQuantity();

	void setAvailableQuantity(int availableQuantity);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface PackagingMaterialMasterQuery extends Query<PackagingMaterialMasterQuery,PackagingMaterialMaster>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		PackagingMaterialMasterQuery id(String id);
		@MasterEntityQueryParameter(attribute = "locationId")
		PackagingMaterialMasterQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "packagingTypeCode")
		PackagingMaterialMasterQuery packagingTypeCode(String packagingTypeCode);
		@MasterEntityQueryParameter(attribute = "reorderLevel")
		PackagingMaterialMasterQuery reorderLevel(int reorderLevel);
		@MasterEntityQueryParameter(attribute = "maximumQuantityAllowed")
		PackagingMaterialMasterQuery maximumQuantityAllowed(int maximumQuantityAllowed);
		@MasterEntityQueryParameter(attribute = "availableQuantity")
		PackagingMaterialMasterQuery availableQuantity(int availableQuantity);


		@MasterEntityQuerySort(attribute = "id")
		PackagingMaterialMasterQuery orderById();
		@MasterEntityQuerySort(attribute = "locationId")
		PackagingMaterialMasterQuery orderByLcationId();
		@MasterEntityQuerySort(attribute = "packagingTypeCode")
		PackagingMaterialMasterQuery orderByPackagingTypeCode();
		@MasterEntityQuerySort(attribute = "reorderLevel")
		PackagingMaterialMasterQuery orderByReorderLevel();
		@MasterEntityQuerySort(attribute = "maximumQuantityAllowed")
		PackagingMaterialMasterQuery orderByMaximumQuantityAllowed();
		@MasterEntityQuerySort(attribute = "availableQuantity")
		PackagingMaterialMasterQuery orderByAvailableQuantity();
	}
}
