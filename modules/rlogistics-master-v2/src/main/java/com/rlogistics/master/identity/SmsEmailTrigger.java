package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface SmsEmailTrigger {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getDescription();

	void setDescription(String description);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface SmsEmailTriggerQuery extends Query<SmsEmailTriggerQuery,SmsEmailTrigger>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		SmsEmailTriggerQuery id(String id);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		SmsEmailTriggerQuery code(String code);
		@MasterEntityQueryParameter(attribute="description")
		SmsEmailTriggerQuery description(String description);
		
		
		@MasterEntityQuerySort(attribute="id")
		SmsEmailTriggerQuery orderById();
		@MasterEntityQuerySort(attribute="code")
		SmsEmailTriggerQuery orderByCode();
	}
}
