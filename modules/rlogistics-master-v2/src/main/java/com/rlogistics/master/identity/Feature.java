package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Feature {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	int getOrder();

	void setOrder(int order);
	
	@MasterEntityAttribute
	String getMenuId();

	void setMenuId(String menuId);
	
	@MasterEntityAttribute
	String getLink();

	void setLink(String link);
	
	@MasterEntityAttribute
	String getParent();

	void setParent(String parent);
	
	@MasterEntityAttribute
	String getLevel();

	void setLevel(String level);
	
	@MasterEntityAttribute
	int getHasSubMenu();

	void setHasSubMenu(int hasSubMenu);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface FeatureQuery extends Query<FeatureQuery,Feature>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		FeatureQuery id(String id);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		FeatureQuery code(String code);
		@MasterEntityQueryParameter(attribute="name")
		FeatureQuery name(String name);
		@MasterEntityQueryParameter(attribute="order")
		FeatureQuery order(int order);
		@MasterEntityQueryParameter(attribute="menuId")
		FeatureQuery menuId(String menuId);
		@MasterEntityQueryParameter(attribute="link")
		FeatureQuery link(String link);
		@MasterEntityQueryParameter(attribute="parent")
		FeatureQuery parent(String parent);
		@MasterEntityQueryParameter(attribute="level")
		FeatureQuery level(String level);
		@MasterEntityQueryParameter(attribute="hasSubMenu")
		FeatureQuery hasSubMenu(int hasSubMenu);
		
		
		@MasterEntityQuerySort(attribute="id")
		FeatureQuery orderById();
		@MasterEntityQuerySort(attribute="code")
		FeatureQuery orderByCode();
		@MasterEntityQuerySort(attribute="name")
		FeatureQuery orderByName();
		@MasterEntityQuerySort(attribute="order")
		FeatureQuery orderByOrder();
		@MasterEntityQuerySort(attribute="menuId")
		FeatureQuery orderByMenuId();
		@MasterEntityQuerySort(attribute="link")
		FeatureQuery orderByLink();
	}
}
