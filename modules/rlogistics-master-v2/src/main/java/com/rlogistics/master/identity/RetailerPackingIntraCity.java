package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerPackingIntraCity {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getProductId();
	
	public void setProductId(String productId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerPackingIntraCityQuery extends Query<RetailerPackingIntraCityQuery,RetailerPackingIntraCity>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerPackingIntraCityQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerPackingIntraCityQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute="productId")
		public RetailerPackingIntraCityQuery productId(String productId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerPackingIntraCityQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerPackingIntraCityQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="productId")
		public RetailerPackingIntraCityQuery orderByProductId();		
	}
}
