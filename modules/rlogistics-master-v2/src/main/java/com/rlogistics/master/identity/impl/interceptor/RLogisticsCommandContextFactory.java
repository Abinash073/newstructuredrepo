package com.rlogistics.master.identity.impl.interceptor;

import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandContextFactory;

public class RLogisticsCommandContextFactory extends CommandContextFactory {

	@Override
	public CommandContext createCommandContext(Command<?> cmd) {
		return new RLogisticsCommandContext(cmd, processEngineConfiguration);
	}

}
