package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface DeviceInfo {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getUserId();

	void setUserId(String userId);
	
	@MasterEntityAttribute
	String getDeviceId();

	void setDeviceId(String deviceId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface DeviceInfoQuery extends Query<DeviceInfoQuery,DeviceInfo>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public DeviceInfoQuery id(String id);

		@MasterEntityQueryParameter(attribute="userId")
		public DeviceInfoQuery userId(String userId);
		
		@MasterEntityQueryParameter(attribute="deviceId")
		public DeviceInfoQuery deviceId(String deviceId);
		
		@MasterEntityQuerySort(attribute="id")
		public DeviceInfoQuery orderById();
		
		@MasterEntityQuerySort(attribute="userId")
		public DeviceInfoQuery orderByUserId();
		
		@MasterEntityQuerySort(attribute="deviceId")
		public DeviceInfoQuery orderByDeviceId();
	}
}
