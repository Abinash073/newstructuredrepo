package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface DeviceLocationDetail {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getDeviceId();

	void setDeviceId(String deviceId);
	
	@MasterEntityAttribute
	String getUserId();

	void setUserId(String userId);
	
	@MasterEntityAttribute
	String getImeiNo();

	void setImeiNo(String imeiNo);
	
	@MasterEntityAttribute
	double getLatitude();

	void setLatitude(double latitude);
	
	@MasterEntityAttribute
	double getLongitude();

	void setLongitude(double longitude);
	
	@MasterEntityQuery
	public interface DeviceLocationDetailQuery extends Query<DeviceLocationDetailQuery,DeviceLocationDetail>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		DeviceLocationDetailQuery id(String id);
		@MasterEntityQueryParameter(attribute="deviceId")
		DeviceLocationDetailQuery deviceId(String deviceId);
		@MasterEntityQueryParameter(attribute="userId")
		DeviceLocationDetailQuery userId(String userId);
		@MasterEntityQueryParameter(attribute="imeiNo")
		DeviceLocationDetailQuery imeiNo(String imeiNo);
		
		@MasterEntityQueryParameter(attribute="latitude")
		DeviceLocationDetailQuery latitude(double latitude);
		@MasterEntityQueryParameter(attribute="longitude")
		DeviceLocationDetailQuery longitude(double longitude);
		
		@MasterEntityQuerySort(attribute="id")
		DeviceLocationDetailQuery orderById();
		
	}
}
