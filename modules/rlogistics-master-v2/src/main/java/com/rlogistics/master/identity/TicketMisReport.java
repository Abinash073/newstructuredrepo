package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketMisReport {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	public String getConsumerName();

	public void setConsumerName(String consumerName);

	@MasterEntityAttribute
	public String getConsumerComplaintNumber();

	public void setConsumerComplaintNumber(String consumerComplaintNumber);

	@MasterEntityAttribute
	public String getAddressLine1();

	public void setAddressLine1(String addressLine1);

	@MasterEntityAttribute
	public String getAddressLine2();

	public void setAddressLine2(String addressLine2);

	@MasterEntityAttribute
	public String getCity();

	public void setCity(String city);

	@MasterEntityAttribute
	public String getPincode();

	public void setPincode(String pincode);

	@MasterEntityAttribute
	public String getTelephoneNumber();

	public void setTelephoneNumber(String telephoneNumber);

	@MasterEntityAttribute
	public String getEmailId();

	public void setEmailId(String emailId);

	@MasterEntityAttribute
	public String getOrderNumber();

	public void setOrderNumber(String orderNumber);

	@MasterEntityAttribute
	public String getDateOfComplaint();

	public void setDateOfComplaint(String dateOfComplaint);

	@MasterEntityAttribute
	public String getNatureOfComplaint();

	public void setNatureOfComplaint(String natureOfComplaint);

	@MasterEntityAttribute
	public String getIsUnderWarranty();

	public void setIsUnderWarranty(String isUnderWarranty);

	@MasterEntityAttribute
	public String getRetailer();

	public void setRetailer(String retailer);

	@MasterEntityAttribute
	public String getBrand();

	public void setBrand(String brand);

	@MasterEntityAttribute
	public String getProductCategory();

	public void setProductCategory(String productCategory);

	@MasterEntityAttribute
	public String getProductName();

	public void setProductName(String productName);

	@MasterEntityAttribute
	public String getModel();

	public void setModel(String model);

	@MasterEntityAttribute
	public String getIdentificationNo();

	public void setIdentificationNo(String identificationNo);

	@MasterEntityAttribute
	public String getDropLocation();

	public void setDropLocation(String dropLocation);

	@MasterEntityAttribute
	public String getDropLocContactPerson();

	public void setDropLocContactPerson(String dropLocContactPerson);

	@MasterEntityAttribute
	public String getDropLocContactNo();

	public void setDropLocContactNo(String dropLocContactNo);

	@MasterEntityAttribute
	public String getCurrentStatus();

	public void setCurrentStatus(String currentStatus);

	@MasterEntityAttribute
	public String getProcessId();

	public void setProcessId(String processId);

	@MasterEntityAttribute
	public String getRlTicketNo();

	public void setRlTicketNo(String rlTicketNo);

	@MasterEntityAttribute
	public String getRlLocationName();

	public void setRlLocationName(String rlLocationName);

	@MasterEntityAttribute
	public String getCreateDate();

	public void setCreateDate(String createDate);

	@MasterEntityAttribute
	public String getPickupDate();

	public void setPickupDate(String pickupDate);

	@MasterEntityAttribute
	public String getDropDate();

	public void setDropDate(String dropDate);

	@MasterEntityAttribute
	public String getCreateTime();

	public void setCreateTime(String createTime);

	@MasterEntityAttribute
	public String getPickupTime();

	public void setPickupTime(String pickupTime);

	@MasterEntityAttribute
	public String getDropTime();

	public void setDropTime(String dropTime);

	@MasterEntityAttribute
	public String getTimeStamp();

	public void setTimeStamp(String timeStamp);

	@MasterEntityAttribute
	public String getDropCity();

	public void setDropCity(String dropCity);
	
	@MasterEntityAttribute
	public String getClosedDate();

	public void setClosedDate(String closedDate);
	
	@MasterEntityAttribute
	public String getClosedReason();

	public void setClosedReason(String closedReason);
	
	@MasterEntityAttribute
	public String getClosedTime();

	public void setClosedTime(String closedTime);
	
	@MasterEntityAttribute
	public String getDocketNo();

	public void setDocketNo(String docketNo);
	
	@MasterEntityAttribute
	public String getRemark();

	public void setRemark(String remark);

	// Abinash Changes
	@MasterEntityAttribute
	public String getFeTypeForPickup();
	public void setFeTypeForPickup(String feTypeForPickup);

	@MasterEntityAttribute
	public String getFeNameForPickup();
	public void setFeNameForPickup(String feNameForPickup);

	@MasterEntityAttribute
	public String getAgencyNameForPickup();
	public void setAgencyNameForPickup(String agencyNameForPickup);

	@MasterEntityAttribute
	public String getFeTypeForDrop();
	public void setFeTypeForDrop(String feTypeForDrop);

	@MasterEntityAttribute
	public String getFeNameForDrop();
	public void setFeNameForDrop(String feNameForDrop);

	@MasterEntityAttribute
	public String getAgencyNameForDrop();
	public void setAgencyNameForDrop(String agencyNameForDrop);


	@MasterEntityQuery
	public interface TicketMisReportQuery extends Query<TicketMisReportQuery, TicketMisReport> {
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		public TicketMisReportQuery id(String id);

		@MasterEntityQueryParameter(attribute = "consumerComplaintNumber")
		public TicketMisReportQuery consumerComplaintNumber(String consumerComplaintNumber);

		@MasterEntityQueryParameter(attribute = "addressLine1")
		public TicketMisReportQuery addressLine1(String addressLine1);

		@MasterEntityQueryParameter(attribute = "addressLine2")
		public TicketMisReportQuery addressLine2(String addressLine2);

		@MasterEntityQueryParameter(attribute = "city")
		public TicketMisReportQuery city(String city);

		@MasterEntityQueryParameter(attribute = "pincode")
		public TicketMisReportQuery pincode(String pincode);

		@MasterEntityQueryParameter(attribute = "telephoneNumber")
		public TicketMisReportQuery telephoneNumber(String telephoneNumber);

		@MasterEntityQueryParameter(attribute = "emailId")
		public TicketMisReportQuery emailId(String emailId);

		@MasterEntityQueryParameter(attribute = "orderNumber")
		public TicketMisReportQuery orderNumber(String orderNumber);

		@MasterEntityQueryParameter(attribute = "dateOfComplaint")
		public TicketMisReportQuery dateOfComplaint(String dateOfComplaint);

		@MasterEntityQueryParameter(attribute = "natureOfComplaint")
		public TicketMisReportQuery natureOfComplaint(String natureOfComplaint);

		@MasterEntityQueryParameter(attribute = "isUnderWarranty")
		public TicketMisReportQuery isUnderWarranty(String isUnderWarranty);

		@MasterEntityQueryParameter(attribute = "retailer")
		public TicketMisReportQuery retailer(String retailer);

		@MasterEntityQueryParameter(attribute = "brand")
		public TicketMisReportQuery brand(String brand);

		@MasterEntityQueryParameter(attribute = "productCategory")
		public TicketMisReportQuery productCategory(String productCategory);

		@MasterEntityQueryParameter(attribute = "productName")
		public TicketMisReportQuery productName(String productName);

		@MasterEntityQueryParameter(attribute = "model")
		public TicketMisReportQuery model(String model);

		@MasterEntityQueryParameter(attribute = "identificationNo")
		public TicketMisReportQuery identificationNo(String identificationNo);

		@MasterEntityQueryParameter(attribute = "dropLocation")
		public TicketMisReportQuery dropLocation(String dropLocation);

		@MasterEntityQueryParameter(attribute = "dropLocContactPerson")
		public TicketMisReportQuery dropLocContactPerson(String dropLocContactPerson);

		@MasterEntityQueryParameter(attribute = "dropLocContactNo")
		public TicketMisReportQuery dropLocContactNo(String dropLocContactNo);

		@MasterEntityQueryParameter(attribute = "currentStatus")
		public TicketMisReportQuery currentStatus(String currentStatus);

		@MasterEntityQueryParameter(attribute = "processId")
		public TicketMisReportQuery processId(String processId);

		@MasterEntityQueryParameter(attribute = "rlTicketNo")
		public TicketMisReportQuery rlTicketNo(String rlTicketNo);

		@MasterEntityQueryParameter(attribute = "rlLocationName")
		public TicketMisReportQuery rlLocationName(String rlLocationName);

		@MasterEntityQueryParameter(attribute = "createDate")
		public TicketMisReportQuery createDate(String createDate);

		@MasterEntityQueryParameter(attribute = "pickupDate")
		public TicketMisReportQuery pickupDate(String pickupDate);

		@MasterEntityQueryParameter(attribute = "dropDate")
		public TicketMisReportQuery dropDate(String dropDate);

		@MasterEntityQueryParameter(attribute = "createTime")
		public TicketMisReportQuery createTime(String createTime);

		@MasterEntityQueryParameter(attribute = "pickupTime")
		public TicketMisReportQuery pickupTime(String pickupTime);

		@MasterEntityQueryParameter(attribute = "dropTime")
		public TicketMisReportQuery dropTime(String dropTime);

		@MasterEntityQueryParameter(attribute = "timeStamp")
		public TicketMisReportQuery timeStamp(String timeStamp);

		@MasterEntityQueryParameter(attribute = "dropCity")
		public TicketMisReportQuery dropCity(String dropCity);
		
		@MasterEntityQueryParameter(attribute = "closedReason")
		public TicketMisReportQuery closedReason(String closedReason);
		
		@MasterEntityQueryParameter(attribute = "closedDate")
		public TicketMisReportQuery closedDate(String closedDate);
		
		@MasterEntityQueryParameter(attribute = "closedTime")
		public TicketMisReportQuery closedTime(String closedTime);
		
		@MasterEntityQueryParameter(attribute = "docketNo")
		public TicketMisReportQuery docketNo(String docketNo);
		
		@MasterEntityQueryParameter(attribute = "remark")
		public TicketMisReportQuery remark(String remark);

		//Abinash

		@MasterEntityQueryParameter(attribute = "feTypeForPickup")
		public TicketMisReportQuery feTypeForPickup(String feTypeForPickup);

		@MasterEntityQueryParameter(attribute = "feNameForPickup")
		public TicketMisReportQuery feNameForPickup(String feNameForPickup);

		@MasterEntityQueryParameter(attribute = "agencyNameForPickup")
		public TicketMisReportQuery agencyNameForPickup(String agencyNameForPickup);

		@MasterEntityQueryParameter(attribute = "feTypeForDrop")
		public TicketMisReportQuery feTypeForDrop(String feTypeForDrop);

		@MasterEntityQueryParameter(attribute = "feNameForDrop")
		public TicketMisReportQuery feNameForDrop(String feNameForDrop);

		@MasterEntityQueryParameter(attribute = "agencyNameForDrop")
		public TicketMisReportQuery agencyNameForDrop(String agencyNameForDrop);

	}

}
