
package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PaymentAcknowledgement {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getTxnId();

	void setTxnId(String txnId);

	@MasterEntityAttribute
	String getAccHolderName();

	void setAccHolderName(String accHolderName);

	@MasterEntityAttribute
	String getAccNo();

	void setAccNo(String accNo);

	@MasterEntityAttribute
	String getIfsc();

	void setAmount(float amount);
	
	@MasterEntityAttribute
	float getAmount();

	void setIfsc(String ifsc);

	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);

	@MasterEntityAttribute
	String getPaymentRef();

	void setPaymentRef(String paymentRef);

	@MasterEntityAttribute
	String getEnquiryResponse();

	void setEnquiryResponse(String enquiryResponse);

	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	String getMessage();

	void setMessage(String message);

	@MasterEntityAttribute
	String getRequestId();

	void setRequestId(String requestId);

	@MasterEntityAttribute
	String getTxnDate();

	void setTxnDate(String txnDate);

	@MasterEntityQuery
	public interface PaymentAcknowledgementQuery extends Query<PaymentAcknowledgementQuery, PaymentAcknowledgement> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		PaymentAcknowledgementQuery id(String id);

		@MasterEntityQueryParameter(attribute = "txnId")
		PaymentAcknowledgementQuery txnId(String txnId);

		@MasterEntityQueryParameter(attribute = "accHolderName")
		PaymentAcknowledgementQuery accHolderName(String accHolderName);

		@MasterEntityQueryParameter(attribute = "amount")
		PaymentAcknowledgementQuery amount(float amount);

		@MasterEntityQueryParameter(attribute = "accNo")
		PaymentAcknowledgementQuery accNo(String accNo);

		@MasterEntityQueryParameter(attribute = "ifsc")
		PaymentAcknowledgementQuery ifsc(String ifsc);

		@MasterEntityQueryParameter(attribute = "paymentRef")
		PaymentAcknowledgementQuery paymentRef(String paymentRef);

		@MasterEntityQueryParameter(attribute = "enquiryResponse")
		PaymentAcknowledgementQuery enquiryResponse(String enquiryResponse);

		@MasterEntityQueryParameter(attribute = "message")
		PaymentAcknowledgementQuery message(String message);

		@MasterEntityQueryParameter(attribute = "txnDate")
		PaymentAcknowledgementQuery txnDate(String txnDate);

		@MasterEntityQueryParameter(attribute = "processId")
		PaymentAcknowledgementQuery processId(String processId);

		@MasterEntityQueryParameter(attribute = "status")
		PaymentAcknowledgementQuery status(String status);

		@MasterEntityQueryParameter(attribute = "requestId")
		PaymentAcknowledgementQuery requestId(String requestId);

		@MasterEntityQuerySort(attribute = "id")
		PaymentAcknowledgementQuery orderById();
		
		@MasterEntityQuerySort(attribute = "txnDate")
		PaymentAcknowledgementQuery orderByTxnDate();
	}
}
