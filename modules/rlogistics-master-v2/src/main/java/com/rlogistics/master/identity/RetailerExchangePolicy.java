package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerExchangePolicy {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getProductId();

	void setProductId(String productId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerExchangePolicyQuery extends Query<RetailerExchangePolicyQuery,RetailerExchangePolicy>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerExchangePolicyQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerExchangePolicyQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "productId")
		RetailerExchangePolicyQuery productId(String productId);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerExchangePolicyQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerExchangePolicyQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "productId")
		RetailerExchangePolicyQuery orderByProductId();
	}
}
