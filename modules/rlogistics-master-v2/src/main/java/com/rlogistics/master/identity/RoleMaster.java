package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RoleMaster {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RoleMasterQuery extends Query<RoleMasterQuery,RoleMaster>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		RoleMasterQuery id(String id);
		@MasterEntityQueryParameter(attribute="name")
		RoleMasterQuery name(String name);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		RoleMasterQuery code(String code);
		
		
		@MasterEntityQuerySort(attribute="id")
		RoleMasterQuery orderById();
		@MasterEntityQuerySort(attribute="name")
		RoleMasterQuery orderByName();
		@MasterEntityQuerySort(attribute="code")
		RoleMasterQuery orderByCode();
	}
}
