package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

@MasterEntity(tableName = "RL_MD_DEVICE_DETAILS")
@MasterEntityLeadsToRestController
public interface DeviceDetails {
    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getVersion();

    void setVersion(String version);

    @MasterEntityAttribute
    String getModel();

    void setModel(String model);

    @MasterEntityAttribute
    String getDevice();

    void setDevice(String device);

    @MasterEntityAttribute
    String getBrand();

    void setBrand(String brand);

    @MasterEntityAttribute
    String getManufacturer();

    void setManufacturer(String manufacturer);

    @MasterEntityAttribute
    String getVersionRelease();

    void setVersionRelease(String versionRelease);

    @MasterEntityAttribute
    String getUserId();

    void setUserId(String userId);

    @MasterEntityAttribute
    String getOs();

    void setOs(String os);

    @MasterEntityAttribute
    String getOsName();

    void setOsName(String osName);

    @MasterEntityAttribute
    String getImei1();

    void setImei1(String imei1);

    @MasterEntityAttribute
    String getImei2();

    void setImei2(String imei2);

    @MasterEntityAttribute
    String getDateCreated();

    void setDateCreated(String dateCreated);
    @MasterEntityAttribute
    String getRlappVersion();

    void setRlappVersion(String rlappVersion);

    @MasterEntityQuery
    public interface DeviceDetailsQuery extends Query<DeviceDetailsQuery, DeviceDetails> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        DeviceDetailsQuery id(String id);

        @MasterEntityQueryParameter(attribute = "version")
        DeviceDetailsQuery version(String version);

        @MasterEntityQueryParameter(attribute = "model")
        DeviceDetailsQuery model(String model);

        @MasterEntityQueryParameter(attribute = "device")
        DeviceDetailsQuery device(String device);

        @MasterEntityQueryParameter(attribute = "brand")
        DeviceDetailsQuery brand(String brand);

        @MasterEntityQueryParameter(attribute = "manufacturer")
        DeviceDetailsQuery manufacturer(String manufacturer);

        @MasterEntityQueryParameter(attribute = "versionRelease")
        DeviceDetailsQuery versionRelease(String versionRelease);

        @MasterEntityQueryParameter(attribute = "userId")
        DeviceDetailsQuery userId(String userId);

        @MasterEntityQueryParameter(attribute = "os")
        DeviceDetailsQuery os(String os);

        @MasterEntityQueryParameter(attribute = "osName")
        DeviceDetailsQuery osName(String osName);

        @MasterEntityQueryParameter(attribute = "imei1")
        DeviceDetailsQuery imei1(String imei1);

        @MasterEntityQueryParameter(attribute = "imei2")
        DeviceDetailsQuery imei2(String imei2);

        @MasterEntityQueryParameter(attribute = "rlappVersion")
        DeviceDetailsQuery rlappVersion(String rlappVersion);

    }
}
