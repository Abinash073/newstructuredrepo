package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import org.joda.time.DateTime;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketImages {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getTicketNo();
	
	void setTicketNo(String ticketNo);
	
	@MasterEntityAttribute
	String getProcInstId();
	
	void setProcInstId(String procInstId);
	
	@MasterEntityAttribute
	String getImageType();
	
	void setImageType(String imageType);
	
	@MasterEntityAttribute
	String getImageUrl();
	
	void setImageUrl(String imageUrl);
	
	@MasterEntityAttribute
	String getCreateOn();
	
	void setCreateOn(String createOn);
	
	@MasterEntityAttribute
	String getLat();
	
	void setLat(String lat);
	
	@MasterEntityAttribute
	String getLongitude();
	
	void setLongitude(String longitude);
	
	@MasterEntityAttribute
	String getLocation();
	
	void setLocation(String location);
	
	@MasterEntityAttribute
	String getCreateTimestamp();
	
	void setCreateTimestamp(String location);
	
	@MasterEntityQuery
	public interface TicketImagesQuery extends Query<TicketImagesQuery,TicketImages>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public TicketImagesQuery id(String id);

		@MasterEntityQueryParameter(attribute="ticketNo")
		public TicketImagesQuery ticketNo(String ticketNo);
		
		@MasterEntityQueryParameter(attribute="procInstId")
		public TicketImagesQuery procInstId(String procInstId);
		
		@MasterEntityQueryParameter(attribute="imageType")
		public TicketImagesQuery imageType(String imageType);
		
		@MasterEntityQueryParameter(attribute="imageUrl")
		public TicketImagesQuery imageUrl(String imageUrl);
		
		@MasterEntityQueryParameter(attribute="createOn")
		public TicketImagesQuery createOn(String createOn);
		
		@MasterEntityQueryParameter(attribute="lat")
		public TicketImagesQuery lat(String lat);
		
		@MasterEntityQueryParameter(attribute="longitude")
		public TicketImagesQuery longitude(String longitude);
		
		@MasterEntityQueryParameter(attribute="location")
		public TicketImagesQuery location(String location);
	}
}

