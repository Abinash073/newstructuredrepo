package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface LocalPacking {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getBox();

	void setBox(String box);
	
	@MasterEntityAttribute
	String getCover();

	void setCover(String cover);
	
	@MasterEntityAttribute
	String getOther();

	void setOther(String other);
	
	@MasterEntityQuery
	public interface LocalPackingQuery extends Query<LocalPackingQuery,LocalPacking>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		LocalPackingQuery id(String id);
		@MasterEntityQueryParameter(attribute="categoryId")
		LocalPackingQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="box")
		LocalPackingQuery box(String box);
		@MasterEntityQueryParameter(attribute="cover")
		LocalPackingQuery cover(String cover);
		@MasterEntityQueryParameter(attribute="other")
		LocalPackingQuery other(String other);
		
		@MasterEntityQuerySort(attribute="id")
		LocalPackingQuery orderById();
		
	}
}
