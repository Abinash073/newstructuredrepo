package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerProcessField {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getRetailerField();

	void setRetailerField(String retailerField);
	
	@MasterEntityAttribute
	String getCanonicalField();

	void setCanonicalField(String canonicalField);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerProcessFieldQuery extends Query<RetailerProcessFieldQuery,RetailerProcessField>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerProcessFieldQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerProcessFieldQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "retailerField")
		RetailerProcessFieldQuery retailerField(String retailerField);
		@MasterEntityQueryParameter(attribute = "canonicalField")
		RetailerProcessFieldQuery canonicalField(String canonicalField);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerProcessFieldQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerProcessFieldQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "retailerField")
		RetailerProcessFieldQuery orderByRetailerField();
		@MasterEntityQuerySort(attribute = "canonicalField")
		RetailerProcessFieldQuery orderByCanonicalField();
	}
}
