package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FinanceDetails {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);

	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);

	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String ProcessId);

	@MasterEntityAttribute
	String getActivitiType();

	void setActivitiType(String activitiType);

	@MasterEntityAttribute
	String getPayTo();

	void setPayTo(String payTo);

	@MasterEntityAttribute
	String getCollectFrom();

	void setCollectFrom(String collectFrom);

	@MasterEntityAttribute
	String getModeOfPayment();

	void setModeOfPayment(String modeOfPayment);

	@MasterEntityAttribute
	float getAmount();

	void setAmount(float amount);

	@MasterEntityAttribute
	String getProcessFlow();

	void setProcessFlow(String ProcessFlow);
	
	@MasterEntityAttribute
	Date getCreatedDate();

	void setCreatedDate(Date createdDate);
	
	@MasterEntityAttribute
	String getCustomerLocation();

	void setCustomerLocation(String customerLocation);

	@MasterEntityQuery
	public interface FinanceDetailsQuery extends Query<FinanceDetailsQuery, FinanceDetails> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		FinanceDetailsQuery id(String id);

		@MasterEntityQueryParameter(attribute = "locationId")
		FinanceDetailsQuery locationId(String locationId);

		@MasterEntityQueryParameter(attribute = "retailerId")
		FinanceDetailsQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		FinanceDetailsQuery ticketNo(String ticketNo);

		@MasterEntityQueryParameter(attribute = "processId")
		FinanceDetailsQuery processId(String processId);

		@MasterEntityQueryParameter(attribute = "activitiType")
		FinanceDetailsQuery activitiType(String activitiType);

		@MasterEntityQueryParameter(attribute = "payTo")
		FinanceDetailsQuery payTo(String payTo);

		@MasterEntityQueryParameter(attribute = "collectFrom")
		FinanceDetailsQuery collectFrom(String collectFrom);

		@MasterEntityQueryParameter(attribute = "modeOfPayment")
		FinanceDetailsQuery modeOfPayment(String modeOfPayment);

		@MasterEntityQueryParameter(attribute = "amount")
		FinanceDetailsQuery amount(float amount);
		
		@MasterEntityQueryParameter(attribute = "createdDate")
		FinanceDetailsQuery createdDate(Date createdDate);
		
		@MasterEntityQueryParameter(attribute = "customerLocation")
		FinanceDetailsQuery customerLocation(String customerLocation);

		@MasterEntityQuerySort(attribute = "id")
		FinanceDetailsQuery orderById();

		@MasterEntityQuerySort(attribute = "amount")
		FinanceDetailsQuery orderByAmount();

		
	}

}
