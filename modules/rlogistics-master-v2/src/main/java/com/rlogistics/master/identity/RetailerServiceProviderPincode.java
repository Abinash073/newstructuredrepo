package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerServiceProviderPincode {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getRetailerServiceProviderId();

	void setRetailerServiceProviderId(String retailerServiceProviderId);

	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerServiceProviderPincodeQuery extends Query<RetailerServiceProviderPincodeQuery,RetailerServiceProviderPincode>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerServiceProviderPincodeQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerServiceProviderId")
		RetailerServiceProviderPincodeQuery retailerServiceProviderId(String retailerServiceProviderId);
		@MasterEntityQueryParameter(attribute = "pincode")
		RetailerServiceProviderPincodeQuery pincode(String pincode);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerServiceProviderPincodeQuery retailerId(String retailerId);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerServiceProviderPincodeQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerServiceProviderId")
		RetailerServiceProviderPincodeQuery orderByRetailerServiceProviderId();
		@MasterEntityQuerySort(attribute = "pincode")
		RetailerServiceProviderPincodeQuery orderByPincode();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerServiceProviderPincodeQuery orderByRetailerId();
	}
}
