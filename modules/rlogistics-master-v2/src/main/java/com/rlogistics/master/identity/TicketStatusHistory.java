package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.PaymentDetails.PaymentDetailsQuery;
import com.rlogistics.master.identity.TicketImages.TicketImagesQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketStatusHistory {
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);
	
	@MasterEntityAttribute
	String getProcInstId();
	
	void setProcInstId(String procInstId);
	
	@MasterEntityAttribute
	String getStatus();
	
	void setStatus(String status);
	
	@MasterEntityAttribute
	String getCreateTime();
	
	void setCreateTime(String createTime);
	
	
	@MasterEntityQuery
	public interface TicketStatusHistoryQuery extends Query<TicketStatusHistoryQuery,TicketStatusHistory>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public TicketStatusHistoryQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="ticketNo")
		public TicketStatusHistoryQuery ticketNo(String ticketNo);
		
		@MasterEntityQueryParameter(attribute="procInstId")
		public TicketStatusHistoryQuery procInstId(String procInstId);
		
		@MasterEntityQueryParameter(attribute="status")
		public TicketStatusHistoryQuery status(String status);
		
		@MasterEntityQueryParameter(attribute="createTime")
		public TicketStatusHistoryQuery createTime(String createTime);
		
		@MasterEntityQuerySort(attribute = "createTime")
		TicketStatusHistoryQuery orderByCreateTime();

	}

}
