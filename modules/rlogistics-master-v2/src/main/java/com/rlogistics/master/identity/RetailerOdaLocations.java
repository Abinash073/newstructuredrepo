package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerOdaLocations {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getPincode();
	
	public void setPincode(String pincode);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerOdaLocationsQuery extends Query<RetailerOdaLocationsQuery,RetailerOdaLocations>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerOdaLocationsQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerOdaLocationsQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="pincode")
		public RetailerOdaLocationsQuery pincode(String pincode);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerOdaLocationsQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerOdaLocationsQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="pincode")
		public RetailerOdaLocationsQuery orderByPincode();			
	}
}
