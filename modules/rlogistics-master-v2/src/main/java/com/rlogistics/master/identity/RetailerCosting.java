package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerCosting {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getPackagingMaterialId();

	void setPackagingMaterialId(String packagingMaterialId);
	
	@MasterEntityAttribute
	float getPackagingMaterialCost();

	void setPackagingMaterialCost(float packagingMaterialCost);
	
	@MasterEntityAttribute
	float getPackagingCost();

	void setPackagingCost(float packagingCost);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerCostingQuery extends Query<RetailerCostingQuery,RetailerCosting>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerCostingQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerCostingQuery retailerId(String retailerId);

		@MasterEntityQueryParameter(attribute="packagingMaterialId")
		public RetailerCostingQuery packagingMaterialId(String packagingMaterialId);
		
		@MasterEntityQueryParameter(attribute="packagingMaterialCost")
		public RetailerCostingQuery packagingMaterialCost(float packagingMaterialCost);
		
		@MasterEntityQueryParameter(attribute="packagingCost")
		public RetailerCostingQuery packagingCost(float packagingCost);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerCostingQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerCostingQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="packagingMaterialId")
		public RetailerCostingQuery orderByPackagingMaterialId();		
		
		@MasterEntityQuerySort(attribute="packagingMaterialCost")
		public RetailerCostingQuery orderByPackagingMaterialCost();	
		
		@MasterEntityQuerySort(attribute="packagingCost")
		public RetailerCostingQuery orderByPackagingCost();		
	}
}
