package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RlProcessStatus {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getProcessStatus();

	void setProcessStatus(String processStatus);


	@MasterEntityQuery
	public interface RlProcessStatusQuery extends Query<RlProcessStatusQuery, RlProcessStatus> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		RlProcessStatusQuery id(String id);

		@MasterEntityQueryParameter(attribute = "processStatus")
		RlProcessStatusQuery processStatus(String userName);


		@MasterEntityQuerySort(attribute = "id")
		RlProcessStatusQuery orderById();

		@MasterEntityQuerySort(attribute = "processStatus")
		RlProcessStatusQuery orderByprocessStatus();		

	}
}

