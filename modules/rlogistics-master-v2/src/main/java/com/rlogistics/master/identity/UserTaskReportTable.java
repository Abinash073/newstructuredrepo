package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface UserTaskReportTable {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getProcessDeploymentId();
	
	void setProcessDeploymentId(String processDeploymentId);
	
	@MasterEntityAttribute
	String getUserTaskId();

	void setUserTaskId(String userTaskId);

	@MasterEntityAttribute
	String getReportTable();
	
	void setReportTable(String reportTable);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface UserTaskReportTableQuery extends Query<UserTaskReportTableQuery,UserTaskReportTable>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		UserTaskReportTableQuery id(String id);
		@MasterEntityQueryParameter(attribute = "userTaskId")
		UserTaskReportTableQuery userTaskId(String userTaskId);
		@MasterEntityQueryParameter(attribute = "processDeploymentId")
		UserTaskReportTableQuery processDeploymentId(String processDeploymentId);

		@MasterEntityQuerySort(attribute = "id")
		UserTaskReportTableQuery orderById();
	}
}
