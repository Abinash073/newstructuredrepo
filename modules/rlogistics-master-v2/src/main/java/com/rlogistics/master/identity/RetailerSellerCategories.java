package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerSellerCategories {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getCategoryId();
	
	public void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	public String getSellerId();
	
	public void setSellerId(String sellerId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerSellerCategoriesQuery extends Query<RetailerSellerCategoriesQuery,RetailerSellerCategories>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerSellerCategoriesQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerSellerCategoriesQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerSellerCategoriesQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="sellerId")
		public RetailerSellerCategoriesQuery sellerId(String sellerId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerSellerCategoriesQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerSellerCategoriesQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="categoryId")
		public RetailerSellerCategoriesQuery orderByCategoryId();	
		@MasterEntityQuerySort(attribute="sellerId")
		public RetailerSellerCategoriesQuery orderBySellerId();	
	}

}
