package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketCosting {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	String getCostingActivityCode();

	void setCostingActivityCode(String costingActivityCode);
	
	@MasterEntityAttribute
	String getTransactionType();

	void setTransactionType(String transactionType);
	
	@MasterEntityAttribute
	String getActor();

	void setActor(String actor);
	
	@MasterEntityAttribute
	String getComments();

	void setComments(String comments);
	
	@MasterEntityAttribute
	float getCost();

	void setCost(float cost);
	
	@MasterEntityQuery
	public interface TicketCostingQuery extends Query<TicketCostingQuery,TicketCosting>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public TicketCostingQuery id(String id);

		@MasterEntityQueryParameter(attribute="processId")
		public TicketCostingQuery processId(String processId);
		
		@MasterEntityQueryParameter(attribute="retailerId")
		public TicketCostingQuery retailerId(String retailerId);
		
		@MasterEntityQueryParameter(attribute="categoryId")
		public TicketCostingQuery categoryId(String categoryId);
		
		@MasterEntityQueryParameter(attribute="serviceId")
		public TicketCostingQuery serviceId(String serviceId);

		@MasterEntityQueryParameter(attribute="costingActivityCode")
		public TicketCostingQuery costingActivityCode(String costingActivityCode);
		
		@MasterEntityQueryParameter(attribute="transactionType")
		public TicketCostingQuery transactionType(String transactionType);
		
		@MasterEntityQueryParameter(attribute="actor")
		public TicketCostingQuery actor(String actor);
		
		@MasterEntityQueryParameter(attribute="comments")
		public TicketCostingQuery comments(String comments);
		
		@MasterEntityQueryParameter(attribute="cost")
		public TicketCostingQuery cost(float cost);
				
	}
}
