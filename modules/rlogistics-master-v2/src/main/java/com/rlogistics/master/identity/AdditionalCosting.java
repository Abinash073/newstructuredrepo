package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface AdditionalCosting {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCostingType();

	void setCostingType(String costingType);
	
	@MasterEntityAttribute
	float getAmount();

	void setAmount(float amount);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int status);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface AdditionalCostingQuery extends Query<AdditionalCostingQuery,AdditionalCosting>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public AdditionalCostingQuery id(String id);

		@MasterEntityQueryParameter(attribute="costingType")
		public AdditionalCostingQuery costingType(String costingType);
		
		@MasterEntityQueryParameter(attribute="amount")
		public AdditionalCostingQuery amount(float amount);
		
		@MasterEntityQueryParameter(attribute="status")
		public AdditionalCostingQuery status(int status);

		@MasterEntityQuerySort(attribute="id")
		public AdditionalCostingQuery orderById();
	}
}
