package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerDoorStepService {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getBrandId();
	
	public void setBrandId(String brandId);
	
	@MasterEntityAttribute
	public int getIsInstallationAvailable();
	
	public void setIsInstallationAvailable(int isInstallationAvailable);
	
	@MasterEntityAttribute
	public int getIsRepairAvailable();
	
	public void setIsRepairAvailable(int isRepairAvailable);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerDoorStepServiceQuery extends Query<RetailerDoorStepServiceQuery,RetailerDoorStepService>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerDoorStepServiceQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerDoorStepServiceQuery retailerId(String retailerId);
		
		@MasterEntityQueryParameter(attribute="brandId")
		public RetailerDoorStepServiceQuery brandId(String brandId);

		@MasterEntityQueryParameter(attribute="isInstallationAvailable")
		public RetailerDoorStepServiceQuery isInstallationAvailable(int isInstallationAvailable);

		@MasterEntityQueryParameter(attribute="isRepairAvailable")
		public RetailerDoorStepServiceQuery isRepairAvailable(int isRepairAvailable);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerDoorStepServiceQuery orderById();
	}
}
