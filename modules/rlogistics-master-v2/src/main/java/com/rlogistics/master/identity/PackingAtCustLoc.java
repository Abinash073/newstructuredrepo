package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PackingAtCustLoc {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityQuery
	public interface PackingAtCustLocQuery extends Query<PackingAtCustLocQuery,PackingAtCustLoc>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		PackingAtCustLocQuery id(String id);
		@MasterEntityQueryParameter(attribute="categoryId")
		PackingAtCustLocQuery categoryId(String categoryId);
		
		@MasterEntityQuerySort(attribute="id")
		PackingAtCustLocQuery orderById();
	}
}
