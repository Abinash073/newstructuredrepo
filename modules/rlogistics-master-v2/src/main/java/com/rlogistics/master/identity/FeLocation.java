package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FeLocation {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getTotalDistance();
	
	void setTotalDistance(String totalDistance);
	
	@MasterEntityAttribute
	String getFeName();
	
	void setFeName(String feName);
	
	@MasterEntityAttribute
	String getDate();
	
	void setDate(String date);
	
	@MasterEntityQuery
	public interface FeLocationQuery extends Query<FeLocationQuery,FeLocation>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public FeLocationQuery id(String id);

		@MasterEntityQueryParameter(attribute="feName")
		public FeLocationQuery feName(String feName);
		
		@MasterEntityQuerySort(attribute="feName")
		public FeLocationQuery orderByFeName();
		
		@MasterEntityQueryParameter(attribute="date")
		public FeLocationQuery date(String date);
		
		@MasterEntityQuerySort(attribute="date")
		public FeLocationQuery orderByDate();
	}
}
