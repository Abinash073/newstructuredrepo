package com.rlogistics.master.identity;


import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Franchiselocation {
	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	String getParentlocation();
	
	void setParentlocation(String parentlocation);
	
	
	@MasterEntityAttribute
	String getAddress();
	
	void setAddress(String address);
	
	
	@MasterEntityAttribute
	String getContactperson();
	
	void setContactperson(String contactperson);
	
	@MasterEntityAttribute
	String getContactno();
	
	void setContactno(String contactno);
	
	@MasterEntityAttribute
	String getEmailid();
	
	void setEmailid(String emailid);
	
	@MasterEntityQuery
	public interface FranchiselocationQuery extends Query<FranchiselocationQuery,Franchiselocation>{
		
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		FranchiselocationQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="name")
		FranchiselocationQuery name(String name);
		
		@MasterEntityQueryParameter(attribute="parentlocation")
		FranchiselocationQuery parentlocation(String parentlocation);
		
		@MasterEntityQueryParameter(attribute="address")
		FranchiselocationQuery address(String address);
		
		@MasterEntityQueryParameter(attribute="contactperson")
		FranchiselocationQuery contactperson(String contactperson);
		
		@MasterEntityQueryParameter(attribute="contactno")
		FranchiselocationQuery contactno(String contactno);
		
		@MasterEntityQueryParameter(attribute="emailid")
		FranchiselocationQuery emailid(String emailid);
		
		
		
		
		
		
	}

}
