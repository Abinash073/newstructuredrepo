package com.rlogistics.master.identity.impl.interceptor;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

@MasterEntity(tableName = "RL_MD_REPORT")
@MasterEntityLeadsToRestController
public interface Report {
    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getLocation();

    void setLocation(String location);

    @MasterEntityAttribute
    String getDate();

    void setDate(String date);

    @MasterEntityAttribute
    int getTicketCreated();

    void setTicketCreated(int ticketCreated);

    @MasterEntityAttribute
    int getClosedTicket();

    void setClosedTicket(int closedTicket);

    @MasterEntityAttribute
    int getOpenTicket();

    void setOpenTicket(int openTicket);

    @MasterEntityAttribute
    String getCreationTime();

    void setCreationTime(String creationTime);

    @MasterEntityAttribute
    String getRetailer();

    void setRetailer(String retailer);


    @MasterEntityQuery
    public interface ReportQuery extends Query<ReportQuery, Report> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        ReportQuery id(String id);

        @MasterEntityQueryParameter(attribute = "location")
        ReportQuery location(String location);

        @MasterEntityQueryParameter(attribute = "date")
        ReportQuery date(String date);

        @MasterEntityQueryParameter(attribute = "ticketCreated")
        ReportQuery ticketCreated(int ticketCreated);

        @MasterEntityQueryParameter(attribute = "closedTicket")
        ReportQuery closedTicket(int closedTicket);

        @MasterEntityQueryParameter(attribute = "openTicket")
        ReportQuery openTicket(int openTicket);

        @MasterEntityQueryParameter(attribute = "retailer")
        ReportQuery retailer(String retailer);

    }

}
