package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface CoordinatorServicablePincodes {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);

	@MasterEntityAttribute
	String getCoordinatorEmailId();

	void setCoordinatorEmailId(String coordinatorEmailId);
	
	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	

	@MasterEntityQuery
	public interface CoordinatorServicablePincodesQuery extends Query<CoordinatorServicablePincodesQuery, CoordinatorServicablePincodes> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		CoordinatorServicablePincodesQuery id(String id);

		@MasterEntityQueryParameter(attribute = "locationId")
		CoordinatorServicablePincodesQuery locationId(String locationId);

		@MasterEntityQueryParameter(attribute = "coordinatorEmailId")
		CoordinatorServicablePincodesQuery coordinatorEmailId(String coordinatorEmailId);
		
		@MasterEntityQueryParameter(attribute = "pincode")
		CoordinatorServicablePincodesQuery pincode(String pincode);


		@MasterEntityQuerySort(attribute = "id")
		CoordinatorServicablePincodesQuery orderById();

		@MasterEntityQuerySort(attribute = "locationId")
		CoordinatorServicablePincodesQuery orderByLocationId();

		@MasterEntityQuerySort(attribute = "pincode")
		CoordinatorServicablePincodesQuery orderByPincode();

		
	}
}

