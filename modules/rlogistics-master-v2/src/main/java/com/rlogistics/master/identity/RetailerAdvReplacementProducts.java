package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerAdvReplacementProducts {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getProductId();
	
	void setProductId(String productId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface RetailerAdvReplacementProductsQuery extends Query<RetailerAdvReplacementProductsQuery,RetailerAdvReplacementProducts>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerAdvReplacementProductsQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerAdvReplacementProductsQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "productId")
		RetailerAdvReplacementProductsQuery productId(String productId);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerAdvReplacementProductsQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerAdvReplacementProductsQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "productId")
		RetailerAdvReplacementProductsQuery orderByProductId();
	}

}
