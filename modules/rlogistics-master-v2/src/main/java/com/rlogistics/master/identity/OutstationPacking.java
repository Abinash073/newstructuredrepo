package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.LocalPacking.LocalPackingQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface OutstationPacking {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getBox();

	void setBox(String box);
	
	@MasterEntityAttribute
	String getCover();

	void setCover(String cover);
	
	@MasterEntityAttribute
	String getOther();

	void setOther(String other);
	
	@MasterEntityQuery
	public interface OutstationPackingQuery extends Query<OutstationPackingQuery,OutstationPacking>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		OutstationPackingQuery id(String id);
		@MasterEntityQueryParameter(attribute="categoryId")
		OutstationPackingQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="box")
		OutstationPackingQuery box(String box);
		@MasterEntityQueryParameter(attribute="cover")
		OutstationPackingQuery cover(String cover);
		@MasterEntityQueryParameter(attribute="other")
		OutstationPackingQuery other(String other);
		
		@MasterEntityQuerySort(attribute="id")
		OutstationPackingQuery orderById();
		
	}
}
