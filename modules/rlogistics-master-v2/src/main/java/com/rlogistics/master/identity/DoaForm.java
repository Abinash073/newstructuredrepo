package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface DoaForm {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getAttachmentId();

	void setAttachmentId(String attachmentId);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getBrandId();

	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface DoaFormQuery extends Query<DoaFormQuery,DoaForm>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		DoaFormQuery id(String id);
		@MasterEntityQueryParameter(attribute="attachmentId")
		DoaFormQuery attachmentId(String attachmentId);
		@MasterEntityQueryParameter(attribute="categoryId")
		DoaFormQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="brandId")
		DoaFormQuery brandId(String brandId);
		
		
		@MasterEntityQuerySort(attribute="id")
		DoaFormQuery orderById();
	}
}
