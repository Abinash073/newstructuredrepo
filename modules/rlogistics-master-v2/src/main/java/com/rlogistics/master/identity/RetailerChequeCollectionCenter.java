package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerChequeCollectionCenter {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getEmail();

	void setEmail(String email);
	
	@MasterEntityAttribute
	String getAddress();

	void setAddress(String address);
	
	@MasterEntityAttribute
	String getPhoneNumber();

	void setPhoneNumber(String PhoneNumber);
	
	@MasterEntityAttribute
	String getAlternatePhoneNumber();

	void setAlternatePhoneNumber(String alternatePhoneNumber);
	
	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	
	@MasterEntityAttribute
	String getArea();

	void setArea(String area);
	
	@MasterEntityAttribute
	String getContactPerson();

	void setContactPerson(String contactPerson);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerChequeCollectionCenterQuery extends Query<RetailerChequeCollectionCenterQuery,RetailerChequeCollectionCenter>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerChequeCollectionCenterQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerChequeCollectionCenterQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "name")
		RetailerChequeCollectionCenterQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		RetailerChequeCollectionCenterQuery code(String code);
		@MasterEntityQueryParameter(attribute = "email")
		RetailerChequeCollectionCenterQuery email(String email);
		@MasterEntityQueryParameter(attribute = "address")
		RetailerChequeCollectionCenterQuery address(String address);
		@MasterEntityQueryParameter(attribute = "phoneNumber")
		RetailerChequeCollectionCenterQuery phoneNumber(String phoneNumber);
		@MasterEntityQueryParameter(attribute = "alternatePhoneNumber")
		RetailerChequeCollectionCenterQuery alternatePhoneNumber(String alternatePhoneNumber);
		@MasterEntityQueryParameter(attribute = "pincode")
		RetailerChequeCollectionCenterQuery pincode(String pincode);
		@MasterEntityQueryParameter(attribute = "contactPerson")
		RetailerChequeCollectionCenterQuery contactPerson(String contactPerson);
		@MasterEntityQueryParameter(attribute = "area")
		RetailerChequeCollectionCenterQuery area(String area);

		@MasterEntityQuerySort(attribute = "id")
		RetailerChequeCollectionCenterQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerChequeCollectionCenterQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "name")
		RetailerChequeCollectionCenterQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		RetailerChequeCollectionCenterQuery orderByCode();
		@MasterEntityQuerySort(attribute = "email")
		RetailerChequeCollectionCenterQuery orderByEmail();
	}
}
