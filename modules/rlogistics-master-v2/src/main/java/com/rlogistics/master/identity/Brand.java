package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Brand {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getParentBrandId();

	void setParentBrandId(String parentBrandId);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface BrandQuery extends Query<BrandQuery,Brand>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		BrandQuery id(String id);
		@MasterEntityQueryParameter(attribute="name")
		BrandQuery name(String name);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		BrandQuery code(String code);
		@MasterEntityQueryParameter(attribute="parentBrandId")
		BrandQuery parentBrandId(String parentBrandId);
		@MasterEntityQuerySort(attribute="id")
		BrandQuery orderById();
		@MasterEntityQuerySort(attribute="name")
		BrandQuery orderByName();
		@MasterEntityQuerySort(attribute="code")
		BrandQuery orderByCode();
		@MasterEntityQuerySort(attribute="parentBrandId")
		BrandQuery orderByParentBrandId();
	}
}
