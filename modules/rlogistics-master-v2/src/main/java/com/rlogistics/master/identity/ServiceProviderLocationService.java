package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ServiceProviderLocationService {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getServiceProviderLocationId();

	void setServiceProviderLocationId(String serviceProviderLocationId);
	
	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getSubCategoryId();

	void setSubCategoryId(String subCategoryId);

	@MasterEntityAttribute
	String getBrandId();

	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	String getIsBizlogAuthorized();

	void setIsBizlogAuthorized(String isBizlogAuthorized);
	
	@MasterEntityAttribute
	String getIsAuthorizedServiceProvider();

	void setIsAuthorizedServiceProvider(String isAuthorizedServiceProvider);
	
	@MasterEntityAttribute
	String getKindOfService();

	void setKindOfService(String kindOfService);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ServiceProviderLocationServiceQuery extends Query<ServiceProviderLocationServiceQuery,ServiceProviderLocationService>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ServiceProviderLocationServiceQuery id(String id);
		@MasterEntityQueryParameter(attribute = "serviceProviderLocationId")
		ServiceProviderLocationServiceQuery serviceProviderLocationId(String serviceProviderLocationId);
		@MasterEntityQueryParameter(attribute = "categoryId")
		ServiceProviderLocationServiceQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute = "subCategoryId")
		ServiceProviderLocationServiceQuery subCategoryId(String subCategoryId);
		@MasterEntityQueryParameter(attribute = "brandId")
		ServiceProviderLocationServiceQuery brandId(String brandId);
		@MasterEntityQueryParameter(attribute = "isBizlogAuthorized")
		ServiceProviderLocationServiceQuery isBizlogAuthorized(String isBizlogAuthorized);
		@MasterEntityQueryParameter(attribute = "isAuthorizedServiceProvider")
		ServiceProviderLocationServiceQuery isAuthorizedServiceProvider(String isAuthorizedServiceProvider);
		@MasterEntityQueryParameter(attribute = "kindOfService")
		ServiceProviderLocationServiceQuery kindOfService(String kindOfService);
		
		@MasterEntityQuerySort(attribute = "id")
		ServiceProviderLocationServiceQuery orderById();
		@MasterEntityQuerySort(attribute = "serviceProviderLocationId")
		ServiceProviderLocationServiceQuery orderByServiceProviderLocationId();
		@MasterEntityQuerySort(attribute = "categoryId")
		ServiceProviderLocationServiceQuery orderByCategoryId();
		@MasterEntityQuerySort(attribute = "subCategoryId")
		ServiceProviderLocationServiceQuery orderBySubCategoryId();
		@MasterEntityQuerySort(attribute = "brandId")
		ServiceProviderLocationServiceQuery orderByBrandId();
		@MasterEntityQuerySort(attribute = "isBizlogAuthorized")
		ServiceProviderLocationServiceQuery orderByIsBizlogAuthorized();
		@MasterEntityQuerySort(attribute = "isAuthorizedServiceProvider")
		ServiceProviderLocationServiceQuery orderByIsAuthorizedServiceProvider();
		@MasterEntityQuerySort(attribute = "kindOfService")
		ServiceProviderLocationServiceQuery orderByKindOfService();
	}

}
