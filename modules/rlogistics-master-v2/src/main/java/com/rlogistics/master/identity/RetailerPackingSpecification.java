package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerPackingSpecification {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	String getNewPickupLocal();

	void setNewPickupLocal(String newPickupLocal);
	
	@MasterEntityAttribute
	String getNewPickupOutstation();

	void setNewPickupOutstation(String newPickupOutstation);
	
	@MasterEntityAttribute
	String getOldPickupLocal();

	void setOldPickupLocal(String oldPickupLocal);
	
	@MasterEntityAttribute
	String getOldPickupOutstation();

	void setOldPickupOutstation(String oldPickupOutstation);
	
	@MasterEntityQuery
	public interface RetailerPackingSpecificationQuery extends Query<RetailerPackingSpecificationQuery,RetailerPackingSpecification>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		RetailerPackingSpecificationQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="retailerId")
		RetailerPackingSpecificationQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="categoryId")
		RetailerPackingSpecificationQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="serviceId")
		RetailerPackingSpecificationQuery serviceId(String serviceId);
		
		@MasterEntityQueryParameter(attribute="newPickupLocal")
		RetailerPackingSpecificationQuery newPickupLocal(String newPickupLocal);
		@MasterEntityQueryParameter(attribute="oldPickupLocal")
		RetailerPackingSpecificationQuery oldPickupLocal(String oldPickupLocal);
		@MasterEntityQueryParameter(attribute="newPickupOutstation")
		RetailerPackingSpecificationQuery newPickupOutstation(String newPickupOutstation);
		@MasterEntityQueryParameter(attribute="oldPickupOutstation")
		RetailerPackingSpecificationQuery oldPickupOutstation(String oldPickupOutstation);
		
		@MasterEntityQuerySort(attribute="id")
		RetailerPackingSpecificationQuery orderById();
		
	}
}
