package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface Menu {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	int getOrder();

	void setOrder(int order);
	
	@MasterEntityAttribute
	String getLink();

	void setLink(String id);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface MenuQuery extends Query<MenuQuery,Menu>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		MenuQuery id(String id);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		MenuQuery code(String code);
		@MasterEntityQueryParameter(attribute="name")
		MenuQuery name(String name);
		@MasterEntityQueryParameter(attribute="order")
		MenuQuery order(int order);
		@MasterEntityQueryParameter(attribute="link")
		MenuQuery link(String link);
		
		
		@MasterEntityQuerySort(attribute="id")
		MenuQuery orderById();
		@MasterEntityQuerySort(attribute="code")
		MenuQuery orderByCode();
		@MasterEntityQuerySort(attribute="name")
		MenuQuery orderByName();
		@MasterEntityQuerySort(attribute="order")
		MenuQuery orderByOrder();
		@MasterEntityQuerySort(attribute="link")
		MenuQuery orderByLink();
	}
}
