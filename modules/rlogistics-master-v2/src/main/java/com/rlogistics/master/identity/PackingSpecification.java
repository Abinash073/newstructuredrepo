package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.RetailerPackingSpecification.RetailerPackingSpecificationQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PackingSpecification {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	String getNewPickupLocal();

	void setNewPickupLocal(String newPickupLocal);
	
	@MasterEntityAttribute
	String getNewPickupOutstation();

	void setNewPickupOutstation(String newPickupOutstation);
	
	@MasterEntityAttribute
	String getOldPickupLocal();

	void setOldPickupLocal(String oldPickupLocal);
	
	@MasterEntityAttribute
	String getOldPickupOutstation();

	void setOldPickupOutstation(String oldPickupOutstation);
	
	@MasterEntityQuery
	public interface PackingSpecificationQuery extends Query<PackingSpecificationQuery,PackingSpecification>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		PackingSpecificationQuery id(String id);
		
		@MasterEntityQueryParameter(attribute="categoryId")
		PackingSpecificationQuery categoryId(String categoryId);
		
		@MasterEntityQueryParameter(attribute="serviceId")
		PackingSpecificationQuery serviceId(String serviceId);
		
		@MasterEntityQueryParameter(attribute="newPickupLocal")
		PackingSpecificationQuery newPickupLocal(String newPickupLocal);
		@MasterEntityQueryParameter(attribute="oldPickupLocal")
		PackingSpecificationQuery oldPickupLocal(String oldPickupLocal);
		@MasterEntityQueryParameter(attribute="newPickupOutstation")
		PackingSpecificationQuery newPickupOutstation(String newPickupOutstation);
		@MasterEntityQueryParameter(attribute="oldPickupOutstation")
		PackingSpecificationQuery oldPickupOutstation(String oldPickupOutstation);
		
		@MasterEntityQuerySort(attribute="id")
		PackingSpecificationQuery orderById();
		
	}
}
