package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ClosedTicket {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);
	
	@MasterEntityAttribute
	String getEndDate();

	void setEndDate(String endDate);

	

	@MasterEntityQuery
	public interface ClosedTicketQuery extends Query<ClosedTicketQuery, ClosedTicket> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		ClosedTicketQuery id(String id);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		ClosedTicketQuery ticketNo(String ticketNo);
		
		@MasterEntityQueryParameter(attribute = "endDate")
		ClosedTicketQuery endDate(String endDate);


		@MasterEntityQuerySort(attribute = "id")
		ClosedTicketQuery orderById();
		
		@MasterEntityQuerySort(attribute = "endDate")
		ClosedTicketQuery orderByEndDate();


		

	}
}

