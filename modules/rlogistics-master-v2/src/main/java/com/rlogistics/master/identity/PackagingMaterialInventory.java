package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PackagingMaterialInventory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);
	
	@MasterEntityAttribute
	String getLocationCode();

	void setLocationCode(String locationCode);
	
	@MasterEntityAttribute
	String getPackagingTypeCode();

	void setPackagingTypeCode(String packagingTypeCode);
	
	@MasterEntityAttribute
	String getBarcode();

	void setBarcode(String barcode);
	
	@MasterEntityAttribute
	String getBarcodeNumber();

	void setBarcodeNumber(String barcodeNumber);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int status);
	
	@MasterEntityAttribute
	String getTicketNumber();

	void setTicketNumber(String ticketNumber);
	
	@MasterEntityAttribute
	String getUserId();

	void setUserId(String userId);
	
	@MasterEntityAttribute
	String getAdditionalDetails();

	void setAdditionalDetails(String additionalDetails);
	
	@MasterEntityAttribute
	int getPrintStatus();

	void setPrintStatus(int printStatus);
	
	@MasterEntityAttribute
	String getImeiNo();

	void setImeiNo(String imeiNo);
	
	
	
	@MasterEntityQuery
	public interface PackagingMaterialInventoryQuery extends Query<PackagingMaterialInventoryQuery,PackagingMaterialInventory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		PackagingMaterialInventoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "locationId")
		PackagingMaterialInventoryQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "locationCode")
		PackagingMaterialInventoryQuery locationCode(String locationCode);
		@MasterEntityQueryParameter(attribute = "packagingTypeCode")
		PackagingMaterialInventoryQuery packagingTypeCode(String packagingTypeCode);
		@MasterEntityQueryParameter(attribute = "barcode")
		PackagingMaterialInventoryQuery barcode(String barcode);
		@MasterEntityQueryParameter(attribute = "barcodeNumber")
		PackagingMaterialInventoryQuery barcodeNumber(String barcodeNumber);
		@MasterEntityQueryParameter(attribute = "status")
		PackagingMaterialInventoryQuery status(int status);
		@MasterEntityQueryParameter(attribute = "ticketNumber")
		PackagingMaterialInventoryQuery ticketNumber(String ticketNumber);
		@MasterEntityQueryParameter(attribute = "userId")
		PackagingMaterialInventoryQuery userId(String userId);
		@MasterEntityQueryParameter(attribute = "additionalDetails")
		PackagingMaterialInventoryQuery additionalDetails(String additionalDetails);
		@MasterEntityQueryParameter(attribute = "printStatus")
		PackagingMaterialInventoryQuery printStatus(int printStatus);
		@MasterEntityQueryParameter(attribute = "imeiNo")
		PackagingMaterialInventoryQuery imeiNo(String imeiNo);
		
		@MasterEntityQuerySort(attribute = "id")
		PackagingMaterialInventoryQuery orderById();
		@MasterEntityQuerySort(attribute = "locationId")
		PackagingMaterialInventoryQuery orderByLcationId();
		@MasterEntityQuerySort(attribute = "packagingTypeCode")
		PackagingMaterialInventoryQuery orderByPackagingTypeCode();
		@MasterEntityQuerySort(attribute = "barcode")
		PackagingMaterialInventoryQuery orderByBarcode();
		@MasterEntityQuerySort(attribute = "status")
		PackagingMaterialInventoryQuery orderByStatus();
		@MasterEntityQuerySort(attribute = "ticketNumber")
		PackagingMaterialInventoryQuery orderByTicketNumber();
		@MasterEntityQuerySort(attribute = "userId")
		PackagingMaterialInventoryQuery orderByUserId();
		@MasterEntityQuerySort(attribute = "additionalDetails")
		PackagingMaterialInventoryQuery orderByAdditionalDetails();
	}
}
