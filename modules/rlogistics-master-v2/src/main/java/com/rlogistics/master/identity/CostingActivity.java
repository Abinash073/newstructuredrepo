package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface CostingActivity {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int status);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface CostingActivityQuery extends Query<CostingActivityQuery,CostingActivity>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public CostingActivityQuery id(String id);

		@MasterEntityQueryParameter(attribute="name")
		public CostingActivityQuery name(String name);
		
		@MasterEntityQueryParameter(attribute="code")
		public CostingActivityQuery code(String code);
		
		@MasterEntityQueryParameter(attribute="status")
		public CostingActivityQuery status(int status);
		
		@MasterEntityQuerySort(attribute="id")
		public CostingActivityQuery orderById();
		
		@MasterEntityQuerySort(attribute="name")
		public CostingActivityQuery orderByName();
		
		@MasterEntityQuerySort(attribute="code")
		public CostingActivityQuery orderByCode();
	}
}
