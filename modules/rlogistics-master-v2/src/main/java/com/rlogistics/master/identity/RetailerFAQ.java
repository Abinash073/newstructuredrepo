package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_RETAILER_FAQ")
@MasterEntityLeadsToRestController
public interface RetailerFAQ {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getFaq();
	
	void setFaq(String faq);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface RetailerFAQQuery extends Query<RetailerFAQQuery,RetailerFAQ>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerFAQQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerFAQQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "faq")
		RetailerFAQQuery faq(String faq);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerFAQQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerFAQQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "faq")
		RetailerFAQQuery orderByFaq();
	}

}
