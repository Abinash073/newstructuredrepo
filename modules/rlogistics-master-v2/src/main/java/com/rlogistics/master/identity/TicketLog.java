package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketLog {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getConsumerName();

	void setConsumerName(String consumerName);

	@MasterEntityAttribute
	String getRetailerName();

	void setRetailerName(String retailerName);

	@MasterEntityAttribute
	String getServiceType();
	
	void setServiceType(String serviceType);

	@MasterEntityAttribute
	byte[] getMapValue();

	void setMapValue(byte[] MapValue);
	

	@MasterEntityQuery
	public interface TicketLogQuery extends Query<TicketLogQuery,TicketLog>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		TicketLogQuery id(String id);
		@MasterEntityQueryParameter(attribute = "consumerName")
		TicketLogQuery consumerName(String consumerName);
		@MasterEntityQueryParameter(attribute = "retailerName")
		TicketLogQuery retailerName(String retailerName);
		@MasterEntityQueryParameter(attribute = "serviceType")
		TicketLogQuery serviceType(String serviceType);
//		@MasterEntityQueryParameter(attribute = "mapValue")
//		TicketLogQuery mapValue(String mapValue);
		
	
		@MasterEntityQuerySort(attribute = "id")
		TicketLogQuery orderById();
		@MasterEntityQuerySort(attribute = "consumerName")
		TicketLogQuery orderByConsumerName();
		@MasterEntityQuerySort(attribute = "retailerName")
		TicketLogQuery orderByRetailerName();
		@MasterEntityQuerySort(attribute = "serviceType")
		TicketLogQuery orderByServiceType();
		
	}
}
