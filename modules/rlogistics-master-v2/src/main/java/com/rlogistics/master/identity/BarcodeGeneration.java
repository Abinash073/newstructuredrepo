package com.rlogistics.master.identity;

import java.sql.Timestamp;
import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface BarcodeGeneration {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getLocationId();

	void setLocationId(String locationId);
	
	@MasterEntityAttribute
	String getPackagingTypeCode();

	void setPackagingTypeCode(String packagingTypeCode);
	
	@MasterEntityAttribute
	Timestamp getGeneratedDate();

	void setGeneratedDate(Timestamp generatedDate);
	
	@MasterEntityAttribute
	int getItemCount();

	void setItemCount(int itemCount);
	
	@MasterEntityAttribute
	String getStartingBarcode();

	void setStartingBarcode(String startingBarcode);
	
	@MasterEntityAttribute
	String getEndingBarcode();

	void setEndingBarcode(String endingBarcode);
	
	@MasterEntityAttribute
	int getPrintStatus();

	void setPrintStatus(int printStatus);
	
	@MasterEntityAttribute
	String getStartingBarcodeRange();

	void setStartingBarcodeRange(String startingBarcodeRange);
	
	@MasterEntityAttribute
	String getEndingBarcodeRange();

	void setEndingBarcodeRange(String endingBarcodeRange);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface BarcodeGenerationQuery extends Query<BarcodeGenerationQuery,BarcodeGeneration>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		BarcodeGenerationQuery id(String id);
		@MasterEntityQueryParameter(attribute = "locationId")
		BarcodeGenerationQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "packagingTypeCode")
		BarcodeGenerationQuery packagingTypeCode(String packagingTypeCode);
		@MasterEntityQueryParameter(attribute = "startingBarcode")
		BarcodeGenerationQuery startingBarcode(String startingBarcode);
		@MasterEntityQueryParameter(attribute = "endingBarcode")
		BarcodeGenerationQuery endingBarcode(String endingBarcode);
		@MasterEntityQueryParameter(attribute = "itemCount")
		BarcodeGenerationQuery itemCount(int itemCount);
		@MasterEntityQueryParameter(attribute = "printStatus")
		BarcodeGenerationQuery printStatus(int printStatus);
		@MasterEntityQueryParameter(attribute = "startingBarcodeRange")
		BarcodeGenerationQuery startingBarcodeRange(String startingBarcodeRange);
		@MasterEntityQueryParameter(attribute = "endingBarcodeRange")
		BarcodeGenerationQuery endingBarcodeRange(String endingBarcodeRange);
		
		@MasterEntityQuerySort(attribute = "id")
		BarcodeGenerationQuery orderById();
		@MasterEntityQuerySort(attribute = "locationId")
		BarcodeGenerationQuery orderByLcationId();
		@MasterEntityQuerySort(attribute = "packagingTypeCode")
		BarcodeGenerationQuery orderByPackagingTypeCode();
	}
}
