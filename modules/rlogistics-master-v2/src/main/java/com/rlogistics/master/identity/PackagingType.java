package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface PackagingType {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	float getCost();

	void setCost(float cost);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	int getType();
	
	void setType(int type);
	
	@MasterEntityAttribute
	float getVolumetricSizeMin();
	
	void setVolumetricSizeMin(float volumetricSizeMin);
	
	@MasterEntityAttribute
	float getVolumetricSizeMax();
	
	void setVolumetricSizeMax(float volumetricSizeMax);
	
	@MasterEntityAttribute
	float getLengthMin();
	
	void setLengthMin(float lengthMin);
	
	@MasterEntityAttribute
	float getLengthMax();
	
	void setLengthMax(float lengthMax);
	
	@MasterEntityAttribute
	float getHeightMin();
	
	void setHeightMin(float heightMin);
	
	@MasterEntityAttribute
	float getHeightMax();
	
	void setHeightMax(float heightMax);
	
	@MasterEntityAttribute
	float getBreadthMin();
	
	void setBreadthMin(float breadthMin);
	
	@MasterEntityAttribute
	float getBreadthMax();
	
	void setBreadthMax(float breadthMax);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface PackagingTypeQuery extends Query<PackagingTypeQuery,PackagingType>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		PackagingTypeQuery id(String id);
		@MasterEntityQueryParameter(attribute="name")
		PackagingTypeQuery name(String name);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		PackagingTypeQuery code(String code);
		@MasterEntityQueryParameter(attribute="cost")
		PackagingTypeQuery cost(float cost);
		@MasterEntityQueryParameter(attribute="type")
		PackagingTypeQuery type(int type);
		@MasterEntityQueryParameter(attribute="lengthMin")
		PackagingTypeQuery lengthMin(float lengthMin);
		@MasterEntityQueryParameter(attribute="lengthMax")
		PackagingTypeQuery lengthMax(float lengthMax);
		@MasterEntityQueryParameter(attribute="heightMin")
		PackagingTypeQuery heightMin(float heightMin);
		@MasterEntityQueryParameter(attribute="heightMax")
		PackagingTypeQuery heightMax(float heightMax);
		@MasterEntityQueryParameter(attribute="breadthMin")
		PackagingTypeQuery breadthMin(float breadthMin);
		@MasterEntityQueryParameter(attribute="breadthMax")
		PackagingTypeQuery breadthMax(float breadthMax);
		@MasterEntityQueryParameter(attribute="volumetricSizeMin")
		PackagingTypeQuery volumetricSizeMin(float volumetricSizeMin);
		@MasterEntityQueryParameter(attribute="volumetricSizeMax")
		PackagingTypeQuery volumetricSizeMax(float volumetricSizeMax);

		@MasterEntityQuerySort(attribute="id")
		PackagingTypeQuery orderById();
		@MasterEntityQuerySort(attribute="name")
		PackagingTypeQuery orderByName();
		@MasterEntityQuerySort(attribute="code")
		PackagingTypeQuery orderByCode();
		@MasterEntityQuerySort(attribute="cost")
		PackagingTypeQuery orderByCost();
		@MasterEntityQuerySort(attribute="type")
		PackagingTypeQuery orderByType();
		@MasterEntityQuerySort(attribute="lengthMin")
		PackagingTypeQuery orderByLengthMin();
		@MasterEntityQuerySort(attribute="lengthMax")
		PackagingTypeQuery orderByLengthMax();
		@MasterEntityQuerySort(attribute="heightMin")
		PackagingTypeQuery orderByHeightMin();
		@MasterEntityQuerySort(attribute="heightMax")
		PackagingTypeQuery orderByHeightMax();
		@MasterEntityQuerySort(attribute="breadthMin")
		PackagingTypeQuery orderByBreadthMin();
		@MasterEntityQuerySort(attribute="breadthMax")
		PackagingTypeQuery orderByBreadthMax();
		@MasterEntityQuerySort(attribute="volumetricSizeMin")
		PackagingTypeQuery orderByVolumetricSizeMin();
		@MasterEntityQuerySort(attribute="volumetricSizeMax")
		PackagingTypeQuery orderByVolumetricSizeMax();
	}

}
