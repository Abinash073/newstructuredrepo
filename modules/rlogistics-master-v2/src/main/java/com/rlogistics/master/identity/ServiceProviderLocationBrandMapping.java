package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ServiceProviderLocationBrandMapping {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getServiceProviderLocationId();
	
	void setServiceProviderLocationId(String serviceProviderLocationId);
	
	@MasterEntityAttribute
	String getBrandId();
	
	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	String getAuthorizationType();
	
	void setAuthorizationType(String authorizationType);
	
	@MasterEntityAttribute
	String getSubCategoryId();
	
	void setSubCategoryId(String subCategoryId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ServiceProviderLocationBrandMappingQuery extends Query<ServiceProviderLocationBrandMappingQuery,ServiceProviderLocationBrandMapping>{
		@MasterEntityQueryParameter(attribute = "id")
		ServiceProviderLocationBrandMappingQuery id(String id);
		@MasterEntityQueryParameter(attribute = "serviceProviderLocationId")
		ServiceProviderLocationBrandMappingQuery serviceProviderLocationId(String serviceProviderLocationId);
		@MasterEntityQueryParameter(attribute = "brandId")
		ServiceProviderLocationBrandMappingQuery brandId(String brandId);
		@MasterEntityQuerySort(attribute = "id")
		ServiceProviderLocationBrandMappingQuery orderById();
		@MasterEntityQuerySort(attribute = "serviceProviderLocationId")
		ServiceProviderLocationBrandMappingQuery orderByServiceProviderLocationId();
		@MasterEntityQuerySort(attribute = "brandId")
		ServiceProviderLocationBrandMappingQuery orderByBrandId();
	}
}
