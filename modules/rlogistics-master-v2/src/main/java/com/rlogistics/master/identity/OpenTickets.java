package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface OpenTickets {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);
	
	@MasterEntityAttribute
	String getCreatedDate();

	void setCreatedDate(String createdDate);

	

	@MasterEntityQuery
	public interface OpenTicketsQuery extends Query<OpenTicketsQuery, OpenTickets> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		OpenTicketsQuery id(String id);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		OpenTicketsQuery ticketNo(String ticketNo);
		
		@MasterEntityQueryParameter(attribute = "createdDate")
		OpenTicketsQuery createdDate(String createdDate);


		@MasterEntityQuerySort(attribute = "id")
		OpenTicketsQuery orderById();
		
		@MasterEntityQuerySort(attribute = "createdDate")
		OpenTicketsQuery orderByCreatedDate();


		

	}
}
