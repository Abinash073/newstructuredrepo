package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface FeOpenTicketStatus {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getUserName();

	void setUserName(String userName);

	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);

	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);

	

	@MasterEntityQuery
	public interface FeOpenTicketStatusQuery extends Query<FeOpenTicketStatusQuery, FeOpenTicketStatus> {
		@MasterEntityQueryParameter(attribute = "id", idAttribute = true)
		FeOpenTicketStatusQuery id(String id);

		@MasterEntityQueryParameter(attribute = "userName")
		FeOpenTicketStatusQuery userName(String userName);

		@MasterEntityQueryParameter(attribute = "processId")
		FeOpenTicketStatusQuery processId(String processId);

		@MasterEntityQueryParameter(attribute = "ticketNo")
		FeOpenTicketStatusQuery ticketNo(String ticketNo);


		@MasterEntityQuerySort(attribute = "id")
		FeOpenTicketStatusQuery orderById();

		@MasterEntityQuerySort(attribute = "userName")
		FeOpenTicketStatusQuery orderByUserName();

		@MasterEntityQuerySort(attribute = "processId")
		FeOpenTicketStatusQuery orderByProcessId();

		

	}
}
