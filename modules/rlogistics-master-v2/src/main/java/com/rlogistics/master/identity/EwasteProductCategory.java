package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface EwasteProductCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityQuery
	public interface EwasteProductCategoryQuery extends Query<EwasteProductCategoryQuery,EwasteProductCategory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		EwasteProductCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		EwasteProductCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		EwasteProductCategoryQuery code(String code);
		
		@MasterEntityQuerySort(attribute = "id")
		EwasteProductCategoryQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		EwasteProductCategoryQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		EwasteProductCategoryQuery orderByCode();
	}

}
