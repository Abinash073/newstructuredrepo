package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_RETAILER_EMAIL_TEMPLATE")
@MasterEntityLeadsToRestController
public interface RetailerEmailTemplate {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getName();
	
	void setName(String name);
	
	@MasterEntityAttribute
	String getTemplate();
	
	void setTemplate(String template);

	@MasterEntityAttribute
	String getSetupScript();
	
	void setSetupScript(String setupScript);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface RetailerEmailTemplateQuery extends Query<RetailerEmailTemplateQuery,RetailerEmailTemplate>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerEmailTemplateQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerEmailTemplateQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "name")
		RetailerEmailTemplateQuery name(String name);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerEmailTemplateQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerEmailTemplateQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "name")
		RetailerEmailTemplateQuery orderByName();
	}
}
