package com.rlogistics.master.identity.impl.interceptor;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;


public class RLogisticsCommandContext extends CommandContext {
	public RLogisticsCommandContext(Command<?> command, ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(command, processEngineConfiguration);
	}

	public <T> T getIdentityManager(Class<T> clazz) {
		return getSession(clazz);
	}	
}
