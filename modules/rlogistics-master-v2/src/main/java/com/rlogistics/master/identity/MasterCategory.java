package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface MasterCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	String getProcessFlow();
	
	void setProcessFlow(String processFlow);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface MasterCategoryQuery extends Query<MasterCategoryQuery,MasterCategory>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		MasterCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute="name")
		MasterCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		MasterCategoryQuery code(String code);
		@MasterEntityQueryParameter(attribute="processFlow")
		MasterCategoryQuery processFlow(String processFlow);
		
		@MasterEntityQuerySort(attribute="id")
		MasterCategoryQuery orderById();
		@MasterEntityQuerySort(attribute="name")
		MasterCategoryQuery orderByName();
		@MasterEntityQuerySort(attribute="code")
		MasterCategoryQuery orderByCode();
	}
}
