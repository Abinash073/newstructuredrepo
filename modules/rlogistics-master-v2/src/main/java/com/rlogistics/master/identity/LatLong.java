package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

@MasterEntity(tableName = "RL_MD_LAT_LONG")
@MasterEntityLeadsToRestController
public interface LatLong {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getLatitude();

    void setLatitude(String latitude);

    @MasterEntityAttribute
    String getLongitude();

    void setLongitude(String longitude);

    @MasterEntityAttribute
    String getProcessId();

    void setProcessId(String processId);

    @MasterEntityAttribute
    String getFeName();

    void setFeName(String feName);

    @MasterEntityAttribute
    String getTime();

    void setTime(String time);

    @MasterEntityAttribute
    String getTotalDistance();

    void setTotalDistance(String totalDistance);

    @MasterEntityAttribute
    String getCity();

    void setCity(String city);

    @MasterEntityAttribute
    String getTicketNo();

    void setTicketNo(String ticketNo);

    @MasterEntityQuery

    public interface LatLongQuery extends Query<LatLongQuery, LatLong> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        LatLongQuery id(String id);

        @MasterEntityQueryParameter(attribute = "city")
        LatLongQuery city(String city);

        @MasterEntityQueryParameter(attribute = "latitude")
        LatLongQuery latitude(String latitude);

        @MasterEntityQueryParameter(attribute = "longitude")
        LatLongQuery longitude(String longitude);

        @MasterEntityQueryParameter(attribute = "processId")
        LatLongQuery processId(String processId);

        @MasterEntityQueryParameter(attribute = "feName")
        LatLongQuery feName(String feName);

        @MasterEntityQueryParameter(attribute = "time")
        LatLongQuery time(String time);

        @MasterEntityQueryParameter(attribute = "ticketNo")
        LatLongQuery ticketNo(String ticketNo);

        @MasterEntityQuerySort(attribute = "time")
        LatLongQuery orderByTime();

        @MasterEntityQueryParameter(attribute = "totalDistance")
        LatLongQuery totalDistance(String totalDistance);
    }


}
