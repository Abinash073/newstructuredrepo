package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerServiceProvider {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getServiceProviderLocationId();

	void setServiceProviderLocationId(String serviceProviderLocationId);
	
	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getBrandId();

	void setBrandId(String brandId);
	
	@MasterEntityAttribute
	String getSubCategoryId();

	void setSubCategoryId(String subCategoryId);
	
	@MasterEntityAttribute
	String getCityId();

	void setCityId(String cityId);
	
	@MasterEntityAttribute
	String getCity();

	void setCity(String city);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerServiceProviderQuery extends Query<RetailerServiceProviderQuery,RetailerServiceProvider>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerServiceProviderQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerServiceProviderQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="serviceProviderLocationId")
		public RetailerServiceProviderQuery serviceProviderLocationId(String serviceProviderLocationId);
		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerServiceProviderQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="brandId")
		public RetailerServiceProviderQuery brandId(String brandId);
		@MasterEntityQueryParameter(attribute="subCategoryId")
		public RetailerServiceProviderQuery subCategoryId(String subCategoryId);
		@MasterEntityQueryParameter(attribute="cityId")
		public RetailerServiceProviderQuery cityId(String cityId);
		@MasterEntityQueryParameter(attribute="city")
		public RetailerServiceProviderQuery city(String city);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerServiceProviderQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerServiceProviderQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="serviceProviderLocationId")
		public RetailerServiceProviderQuery orderByServiceProviderLocationId();
		@MasterEntityQuerySort(attribute="categoryId")
		public RetailerServiceProviderQuery orderByCategoryId();	
		@MasterEntityQuerySort(attribute="brandId")
		public RetailerServiceProviderQuery orderByBrandId();
		@MasterEntityQuerySort(attribute="subCategoryId")
		public RetailerServiceProviderQuery orderBySubCategoryId();
		@MasterEntityQuerySort(attribute="cityId")
		public RetailerServiceProviderQuery orderByCityId();
		@MasterEntityQuerySort(attribute="city")
		public RetailerServiceProviderQuery orderByCity();
	}

}
