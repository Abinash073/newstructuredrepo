package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface TicketColumns {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	
	@MasterEntityAttribute
	String getField();

	void setField(String field);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface TicketColumnsQuery extends Query<TicketColumnsQuery,TicketColumns>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		TicketColumnsQuery id(String id);
		@MasterEntityQueryParameter(attribute = "field")
		TicketColumnsQuery field(String field);
		
		@MasterEntityQuerySort(attribute = "id")
		TicketColumnsQuery orderById();
		@MasterEntityQuerySort(attribute = "field")
		TicketColumnsQuery orderByField();
	}
}
