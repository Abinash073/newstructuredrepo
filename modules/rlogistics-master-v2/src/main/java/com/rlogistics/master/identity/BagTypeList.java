package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface BagTypeList {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	int getBagType();

	void setBagType(int bagType);
	
	@MasterEntityQuery
	public interface BagTypeListQuery extends Query<BagTypeListQuery,BagTypeList>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		BagTypeListQuery id(String id);
		@MasterEntityQueryParameter(attribute="bagType")
		BagTypeListQuery bagType(int bagType);
		
		@MasterEntityQuerySort(attribute="bagType")
		BagTypeListQuery orderByBagType();
	}

}
