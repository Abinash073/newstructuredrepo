package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ServiceProviderLocation {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getServiceProviderId();

	void setServiceProviderId(String serviceProviderId);
	
	@MasterEntityAttribute
	String getServiceProviderCode();

	void setServiceProviderCode(String serviceProviderCode);

	@MasterEntityAttribute
	String getAddress1();

	void setAddress1(String address1);

	@MasterEntityAttribute
	String getAddress2();

	void setAddress2(String address2);

	@MasterEntityAttribute
	String getCity();

	void setCity(String city);

	@MasterEntityAttribute
	String getPhone1();

	void setPhone1(String phone1);

	@MasterEntityAttribute
	String getEmail();

	void setEmail(String email);

	@MasterEntityAttribute
	String getContactPerson();

	void setContactPerson(String contactPerson);
	
	@MasterEntityAttribute
	String getPincode();

	void setPincode(String pincode);
	
	@MasterEntityAttribute
	String getPhone2();

	void setPhone2(String phone2);
	
	@MasterEntityAttribute
	String getArea();

	void setArea(String area);
	
	@MasterEntityAttribute
	String getCityId();

	void setCityId(String cityId);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ServiceProviderLocationQuery extends Query<ServiceProviderLocationQuery,ServiceProviderLocation>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ServiceProviderLocationQuery id(String id);
		@MasterEntityQueryParameter(attribute = "serviceProviderId")
		ServiceProviderLocationQuery serviceProviderId(String serviceProviderId);
		@MasterEntityQueryParameter(attribute = "serviceProviderCode")
		ServiceProviderLocationQuery serviceProviderCode(String serviceProviderCode);
		@MasterEntityQueryParameter(attribute = "address1")
		ServiceProviderLocationQuery address1(String address1);
		@MasterEntityQueryParameter(attribute = "address2")
		ServiceProviderLocationQuery address2(String address2);
		@MasterEntityQueryParameter(attribute = "city")
		ServiceProviderLocationQuery city(String city);
		@MasterEntityQueryParameter(attribute = "phone1")
		ServiceProviderLocationQuery phone1(String phone1);
		@MasterEntityQueryParameter(attribute = "phone2")
		ServiceProviderLocationQuery phone2(String phone2);
		@MasterEntityQueryParameter(attribute = "email")
		ServiceProviderLocationQuery email(String email);
		@MasterEntityQueryParameter(attribute = "contactPerson")
		ServiceProviderLocationQuery contactPerson(String contactPerson);
		@MasterEntityQueryParameter(attribute = "pincode")
		ServiceProviderLocationQuery pincode(String pincode);
		@MasterEntityQueryParameter(attribute = "area")
		ServiceProviderLocationQuery area(String area);
		@MasterEntityQueryParameter(attribute = "cityId")
		ServiceProviderLocationQuery cityId(String cityId);
		
		@MasterEntityQuerySort(attribute = "id")
		ServiceProviderLocationQuery orderById();
		@MasterEntityQuerySort(attribute = "serviceProviderId")
		ServiceProviderLocationQuery orderByServiceProviderId();
		@MasterEntityQuerySort(attribute = "serviceProviderCode")
		ServiceProviderLocationQuery orderByServiceProviderCode();
	}
}
