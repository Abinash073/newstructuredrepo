package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerPackingCost {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getPackingMaterialId();
	
	public void setPackingMaterialId(String packingMaterialId);
	
	@MasterEntityAttribute
	public double getPackingCost();
	
	public void setPackingCost(double packingCost);
	
	@MasterEntityAttribute
	public double getMaterialCost();
	
	public void setMaterialCost(double materialCost);
	
	@MasterEntityAttribute
	public double getPackingCostInterCity();
	
	public void setPackingCostInterCity(double packingCostInterCity);
	
	@MasterEntityAttribute
	public double getMaterialCostInterCity();
	
	public void setMaterialCostInterCity(double materialCostInterCity);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerPackingCostQuery extends Query<RetailerPackingCostQuery,RetailerPackingCost>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerPackingCostQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerPackingCostQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="packingMaterialId")
		public RetailerPackingCostQuery packingMaterialId(String packingMaterialId);
		@MasterEntityQueryParameter(attribute="packingCost")
		public RetailerPackingCostQuery packingCost(double packingCost);
		@MasterEntityQueryParameter(attribute="packingCostInterCity")
		public RetailerPackingCostQuery packingCostInterCity(double packingCostInterCity);
		@MasterEntityQueryParameter(attribute="materialCost")
		public RetailerPackingCostQuery materialCost(double materialCost);
		@MasterEntityQueryParameter(attribute="materialCostInterCity")
		public RetailerPackingCostQuery materialCostInterCity(double materialCostInterCity);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerPackingCostQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerPackingCostQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="packingMaterialId")
		public RetailerPackingCostQuery orderByPackingMaterialId();	
		@MasterEntityQuerySort(attribute="packingCost")
		public RetailerPackingCostQuery orderByPackingCost();	
		@MasterEntityQuerySort(attribute="materialCost")
		public RetailerPackingCostQuery orderByMaterialCost();	
	}
}
