package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RoleAccess {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRoleCode();

	void setRoleCode(String roleCode);
	
	@MasterEntityAttribute
	String getFeatureId();

	void setFeatureId(String featureId);
	
	@MasterEntityAttribute
	String getAccess();

	void setAccess(String access);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RoleAccessQuery extends Query<RoleAccessQuery,RoleAccess>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		RoleAccessQuery id(String id);
		@MasterEntityQueryParameter(attribute="roleCode")
		RoleAccessQuery roleCode(String roleCode);
		@MasterEntityQueryParameter(attribute="featureId")
		RoleAccessQuery featureId(String featureId);
		@MasterEntityQueryParameter(attribute="access")
		RoleAccessQuery access(String access);
		
		@MasterEntityQuerySort(attribute="id")
		RoleAccessQuery orderById();
		@MasterEntityQuerySort(attribute="roleCode")
		RoleAccessQuery orderByRoleCode();
		@MasterEntityQuerySort(attribute="featureId")
		RoleAccessQuery orderByFeatureId();
		@MasterEntityQuerySort(attribute="access")
		RoleAccessQuery orderByAccess();
	}
}
