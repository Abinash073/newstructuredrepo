package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_RETAILER_EMAIL_CONFIGURATION")
@MasterEntityLeadsToRestController
public interface RetailerEmailConfiguration {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();
	
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getHost();
	
	void setHost(String host);
	
	@MasterEntityAttribute
	String getUsername();
	
	void setUsername(String username);
	
	@MasterEntityAttribute
	String getPassword();
	
	void setPassword(String password);

	@MasterEntityAttribute
	String getPort();
	
	void setPort(String port);
	
	@MasterEntityAttribute
	String getSocket();
	
	void setSocket(String socket);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerEmailConfigurationQuery extends Query<RetailerEmailConfigurationQuery,RetailerEmailConfiguration>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerEmailConfigurationQuery id(String id);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerEmailConfigurationQuery retailerId(String retailerId);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerEmailConfigurationQuery orderById();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerEmailConfigurationQuery orderByRetailerId();
	}
}
