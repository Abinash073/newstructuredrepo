package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface EwasteBagQuantity {
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	String getBagType();

	void setBagType(String bagType);

	@MasterEntityAttribute
	int getQuantity();

	void setQuantity(int quantity);
	
	@MasterEntityQuery
	public interface EwasteBagQuantityQuery extends Query<EwasteBagQuantityQuery,EwasteBagQuantity>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		EwasteBagQuantityQuery id(String id);
		@MasterEntityQueryParameter(attribute="processId")
		EwasteBagQuantityQuery processId(String processId);
		@MasterEntityQueryParameter(attribute="bagType")
		EwasteBagQuantityQuery bagType(String bagType);		
		@MasterEntityQueryParameter(attribute="quantity")
		EwasteBagQuantityQuery quantity(int quantity);
		
		@MasterEntityQuerySort(attribute="id")
		EwasteBagQuantityQuery orderById();
		@MasterEntityQuerySort(attribute="bagType")
		EwasteBagQuantityQuery orderByBagType();
	}

}
