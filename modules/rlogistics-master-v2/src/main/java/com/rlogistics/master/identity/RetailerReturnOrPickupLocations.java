package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerReturnOrPickupLocations {

	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getSellerCode();
	
	public void setSellerCode(String sellerCode);
	
	@MasterEntityAttribute
	public String getSellerName();
	
	public void setSellerName(String sellerName);
	
	@MasterEntityAttribute
	public String getSellerLocation();
	
	public void setSellerLocation(String sellerLocation);
	
	@MasterEntityAttribute
	public String getSellerAddress();
	
	public void setSellerAddress(String sellerAddress);
	
	@MasterEntityAttribute
	public String getSellerPhoneNumber();
	
	public void setSellerPhoneNumber(String sellerPhoneNumber);
	
	@MasterEntityAttribute
	public String getSellerAlternatePhoneNumber();
	
	public void setSellerAlternatePhoneNumber(String sellerAlternatePhoneNumber);
	
	@MasterEntityAttribute
	public String getSellerEmail();
	
	public void setSellerEmail(String sellerEmail);
	
	@MasterEntityAttribute
	public String getSellerContactPerson();
	
	public void setSellerContactPerson(String sellerContactPerson);
	
	@MasterEntityAttribute
	public String getSellerPincode();
	
	public void setSellerPincode(String sellerPincode);
	
	@MasterEntityAttribute
	public String getCity();
	
	public void setCity(String city);
	
	@MasterEntityAttribute
	public String getCityId();
	
	public void setCityId(String cityId);
	
	@MasterEntityQuery
	public interface RetailerReturnOrPickupLocationsQuery extends Query<RetailerReturnOrPickupLocationsQuery,RetailerReturnOrPickupLocations>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerReturnOrPickupLocationsQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerReturnOrPickupLocationsQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="sellerCode")
		public RetailerReturnOrPickupLocationsQuery sellerCode(String sellerCode);
		@MasterEntityQueryParameter(attribute="sellerName")
		public RetailerReturnOrPickupLocationsQuery sellerName(String sellerName);
		@MasterEntityQueryParameter(attribute="sellerLocation")
		public RetailerReturnOrPickupLocationsQuery sellerLocation(String sellerLocation);
		@MasterEntityQueryParameter(attribute="sellerAddress")
		public RetailerReturnOrPickupLocationsQuery sellerAddress(String sellerAddress);
		@MasterEntityQueryParameter(attribute="sellerPhoneNumber")
		public RetailerReturnOrPickupLocationsQuery sellerPhoneNumber(String sellerPhoneNumber);
		@MasterEntityQueryParameter(attribute="sellerAlternatePhoneNumber")
		public RetailerReturnOrPickupLocationsQuery sellerAlternatePhoneNumber(String sellerAlternatePhoneNumber);
		@MasterEntityQueryParameter(attribute="sellerEmail")
		public RetailerReturnOrPickupLocationsQuery sellerEmail(String sellerEmail);
		@MasterEntityQueryParameter(attribute="sellerContactPerson")
		public RetailerReturnOrPickupLocationsQuery sellerContactPerson(String sellerContactPerson);
		@MasterEntityQueryParameter(attribute="sellerPincode")
		public RetailerReturnOrPickupLocationsQuery sellerPincode(String sellerPincode);
		@MasterEntityQueryParameter(attribute="city")
		public RetailerReturnOrPickupLocationsQuery city(String city);
		@MasterEntityQueryParameter(attribute="cityId")
		public RetailerReturnOrPickupLocationsQuery cityId(String cityId);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerReturnOrPickupLocationsQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerReturnOrPickupLocationsQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="sellerCode")
		public RetailerReturnOrPickupLocationsQuery orderBySellerCode();	
		@MasterEntityQuerySort(attribute="sellerName")
		public RetailerReturnOrPickupLocationsQuery orderBySellerName();
		@MasterEntityQuerySort(attribute="cityId")
		public RetailerReturnOrPickupLocationsQuery orderByCityId();
	}
}
