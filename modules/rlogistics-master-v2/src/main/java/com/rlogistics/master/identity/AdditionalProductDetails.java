package com.rlogistics.master.identity;


import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

import java.math.BigInteger;

@MasterEntity(tableName = "RL_MD_ADDITIONAL_PRODUCT_DETAILS")
@MasterEntityLeadsToRestController
public interface AdditionalProductDetails {


    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getRetailerName();

    void setRetailerName(String retailerName);

    @MasterEntityAttribute
    String getMainTicketRef();

    void setMainTicketRef(String mainTicketRef);

    @MasterEntityAttribute
    String getSubTicketRef();

    void setSubTicketRef(String subTicketRef);

    @MasterEntityAttribute
    String getBrand();

    void setBrand(String brand);

    @MasterEntityAttribute
    String getProductCategory();

    void setProductCategory(String productCategory);

    @MasterEntityAttribute
    String getProductName();

    void setProductName(String productName);

    @MasterEntityAttribute
    String getProductCode();

    void setProductCode(String productCode);

    @MasterEntityAttribute
    String getModel();

    void setModel(String model);

    @MasterEntityAttribute
    String getProblemDescription();

    void setProblemDescription(String problemDescription);

    @MasterEntityAttribute
    String getIdentificationNo();

    void setIdentificationNo(String identificationNo);

    @MasterEntityAttribute
    String getDropLocation();

    void setDropLocation(String dropLocation);

    @MasterEntityAttribute
    String getDropLocAddress1();

    void setDropLocAddress1(String dropLocAddress1);

    @MasterEntityAttribute
    String getDropLocAddress2();

    void setDropLocAddress2(String DropLocAddress2);

    @MasterEntityAttribute
    String getDropLocCity();

    void setDropLocCity(String dropLocCity);

    @MasterEntityAttribute
    String getDropLocState();

    void setDropLocState(String dropLocState);

    @MasterEntityAttribute
    int getDropLocPincode();

    void setDropLocPincode(int dropLocPincode);

    @MasterEntityAttribute
    String getDropLocContactPerson();

    void setDropLocContactPerson(String dropLocContactPerson);

    @MasterEntityAttribute
    String getDropLocContactNo();

    void setDropLocContactNo(String dropLocContactNo);


    @MasterEntityAttribute
    String getPhotosAfter();

    void setPhotosAfter(String photosAfter);

    @MasterEntityAttribute
    String getPhotographsBefore();

    void setPhotographsBefore(String photographsBefore);


    @MasterEntityAttribute
    String getRetailerId();

    void setRetailerId(String retailerId);


    @MasterEntityAttribute
    String getStatus();

    void setStatus(String status);


    @MasterEntityAttribute
    String getBarcode();

    void setBarcode(String barcode);

    @MasterEntityAttribute
    String getEvaluationTicketReferance();


    void setEvaluationTicketReferance(String evaluationTicketReferance);

    @MasterEntityAttribute
    int getPhotosBeforeCount();

    void setPhotosBeforeCount(int photosBeforeCount);

    @MasterEntityAttribute
    int getPhotosAfterCount();

    void setPhotosAfterCount(int photosAfterCount);


    @MasterEntityAttribute
    String getFeAssigned();

    void setFeAssigned(String feAssigned);

    @MasterEntityAttribute
    String getBoxNumber();

    void setBoxNumber(String boxNumber);


    @MasterEntityAttribute
    String getProcessId();

    void setProcessId(String processId);


    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);


    @MasterEntityAttribute
    String getExtraVariables();

    void setExtraVariables(String extraVariables);

    @MasterEntityQuery
    public interface AdditionalProductDetailsQuery extends Query<AdditionalProductDetailsQuery, AdditionalProductDetails> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        AdditionalProductDetailsQuery id(String id);

        @MasterEntityQueryParameter(attribute = "mainTicketRef")
        AdditionalProductDetailsQuery mainTicketRef(String mainTicketRef);

        @MasterEntityQueryParameter(attribute = "retailerName")
        AdditionalProductDetailsQuery retailerName(String retailerName);

        @MasterEntityQueryParameter(attribute = "subTicketRef")
        AdditionalProductDetailsQuery subTicketRef(String subTicketRef);

        @MasterEntityQueryParameter(attribute = "brand")
        AdditionalProductDetailsQuery brand(String brand);

        @MasterEntityQueryParameter(attribute = "productCategory")
        AdditionalProductDetailsQuery productCategory(String productCategory);

        @MasterEntityQueryParameter(attribute = "productName")
        AdditionalProductDetailsQuery productName(String productName);

        @MasterEntityQueryParameter(attribute = "productCode")
        AdditionalProductDetailsQuery productCode(String productCode);

        @MasterEntityQueryParameter(attribute = "model")
        AdditionalProductDetailsQuery model(String model);

        @MasterEntityQueryParameter(attribute = "problemDescription")
        AdditionalProductDetailsQuery problemDescription(String problemDescription);

        @MasterEntityQueryParameter(attribute = "identificationNo")
        AdditionalProductDetailsQuery identificationNo(String identificationNo);

        @MasterEntityQueryParameter(attribute = "dropLocation")
        AdditionalProductDetailsQuery dropLocation(String dropLocation);

        @MasterEntityQueryParameter(attribute = "dropLocAddress1")
        AdditionalProductDetailsQuery dropLocAddress1(String dropLocAddress1);

        @MasterEntityQueryParameter(attribute = "dropLocAddress2")
        AdditionalProductDetailsQuery dropLocAddress2(String DropLocAddress2);

        @MasterEntityQueryParameter(attribute = "dropLocCity")
        AdditionalProductDetailsQuery dropLocCity(String dropLocCity);

        @MasterEntityQueryParameter(attribute = "dropLocState")
        AdditionalProductDetailsQuery dropLocState(String dropLocState);


        @MasterEntityQueryParameter(attribute = "dropLocPincode")
        AdditionalProductDetailsQuery dropLocPincode(int dropLocPincode);

        @MasterEntityQueryParameter(attribute = "dropLocContactPerson")
        AdditionalProductDetailsQuery dropLocContactPerson(String dropLocContactPerson);

        @MasterEntityQueryParameter(attribute = "dropLocContactNo")
        AdditionalProductDetailsQuery dropLocContactNo(String dropLocContactNo);


        @MasterEntityQueryParameter(attribute = "photosAfter")
        AdditionalProductDetailsQuery photosAfter(String photosAfter);

        @MasterEntityQueryParameter(attribute = "photographsBefore")
        AdditionalProductDetailsQuery photosBefore(String photosBefore);

        @MasterEntityQueryParameter(attribute = "retailerId")
        AdditionalProductDetailsQuery retailerId(String retailerId);

        @MasterEntityQueryParameter(attribute = "status")
        AdditionalProductDetailsQuery status(String status);

        @MasterEntityQueryParameter(attribute = "barcode")
        AdditionalProductDetailsQuery barcode(String barcode);

        @MasterEntityQueryParameter(attribute = "evaluationTicketReferance")
        AdditionalProductDetailsQuery evaluationTicketReferance(String evaluationTicketReferance);

        @MasterEntityQueryParameter(attribute = "photosBeforeCount")
        AdditionalProductDetailsQuery photosBeforeCount(int photosBeforeCount);

        @MasterEntityQueryParameter(attribute = "photosAfterCount")
        AdditionalProductDetailsQuery photosAfterCount(int photosAfterCount);

        @MasterEntityQueryParameter(attribute = "feAssigned")
        AdditionalProductDetailsQuery feAssigned(String feAssigned);

        @MasterEntityQueryParameter(attribute = "boxNumber")
        AdditionalProductDetailsQuery boxNumber(String boxNumber);

        @MasterEntityQueryParameter(attribute = "processId")
        AdditionalProductDetailsQuery processId(String processId);

        @MasterEntityQueryParameter(attribute = "extraVariables")
        AdditionalProductDetailsQuery extraVariables(String extraVariables);

    }


}

