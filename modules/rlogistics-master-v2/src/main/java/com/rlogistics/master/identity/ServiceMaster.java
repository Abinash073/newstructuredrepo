package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ServiceMaster {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getCode();

	void setCode(String code);
	
	@MasterEntityAttribute
	String getName();

	void setName(String name);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface ServiceMasterQuery extends Query<ServiceMasterQuery,ServiceMaster>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public ServiceMasterQuery id(String id);

		@MasterEntityQueryParameter(attribute="code",codeAttribute=true)
		public ServiceMasterQuery code(String code);

		@MasterEntityQueryParameter(attribute="name")
		public ServiceMasterQuery name(String name);
		
		@MasterEntityQuerySort(attribute="id")
		public ServiceMasterQuery orderById();
		
		@MasterEntityQuerySort(attribute="code")
		public ServiceMasterQuery orderByCode();
		
		@MasterEntityQuerySort(attribute="name")
		public ServiceMasterQuery orderByName();		
	}
}
