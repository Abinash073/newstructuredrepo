package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface SmsEmailTriggerConfig {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getSmsEmailTriggerCode();

	void setSmsEmailTriggerCode(String smsEmailTriggerCode);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	int getIsEmailRequired();

	void setIsEmailRequired(int isEmailRequired);
	
	@MasterEntityAttribute
	int getIsSmsRequired();

	void setIsSmsRequired(int isSmsRequired);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface SmsEmailTriggerConfigQuery extends Query<SmsEmailTriggerConfigQuery,SmsEmailTriggerConfig>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		SmsEmailTriggerConfigQuery id(String id);
		@MasterEntityQueryParameter(attribute="smsEmailTriggerCode",codeAttribute=true)
		SmsEmailTriggerConfigQuery smsEmailTriggerCode(String smsEmailTriggerCode);
		@MasterEntityQueryParameter(attribute="serviceId")
		SmsEmailTriggerConfigQuery serviceId(String serviceId);
		@MasterEntityQueryParameter(attribute="isEmailRequired")
		SmsEmailTriggerConfigQuery isEmailRequired(int isEmailRequired);
		@MasterEntityQueryParameter(attribute="isSmsRequired")
		SmsEmailTriggerConfigQuery isSmsRequired(int isSmsRequired);
		
		
		@MasterEntityQuerySort(attribute="id")
		SmsEmailTriggerConfigQuery orderById();
		@MasterEntityQuerySort(attribute="smsEmailTriggerCode")
		SmsEmailTriggerConfigQuery orderBySmsEmailTriggerCode();
	}
}
