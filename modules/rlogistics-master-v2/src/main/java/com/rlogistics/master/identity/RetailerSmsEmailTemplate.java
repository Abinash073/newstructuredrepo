package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerSmsEmailTemplate {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getSmsEmailTriggerCode();

	void setSmsEmailTriggerCode(String smsEmailTriggerCode);
	
	@MasterEntityAttribute
	String getSmsTemplate();

	void setSmsTemplate(String smsTemplate);
	
	@MasterEntityAttribute
	String getEmailTemplate();

	void setEmailTemplate(String emailTemplate);
	
	@MasterEntityAttribute
	String getSetupScript();
	
	void setSetupScript(String setupScript);
	
	@MasterEntityAttribute
	String getParams();
	
	void setParams(String params);
	
	@MasterEntityAttribute
	String getSubject();
	
	void setSubject(String subject);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerSmsEmailTemplateQuery extends Query<RetailerSmsEmailTemplateQuery,RetailerSmsEmailTemplate>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		RetailerSmsEmailTemplateQuery id(String id);
		@MasterEntityQueryParameter(attribute="smsEmailTriggerCode",codeAttribute=true)
		RetailerSmsEmailTemplateQuery smsEmailTriggerCode(String smsEmailTriggerCode);
		@MasterEntityQueryParameter(attribute="smsTemplate")
		RetailerSmsEmailTemplateQuery smsTemplate(String smsTemplate);
		@MasterEntityQueryParameter(attribute="emailTemplate")
		RetailerSmsEmailTemplateQuery emailTemplate(String emailTemplate);
		@MasterEntityQueryParameter(attribute="retailerId")
		RetailerSmsEmailTemplateQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="params")
		RetailerSmsEmailTemplateQuery params(String params);
		@MasterEntityQueryParameter(attribute="subject")
		RetailerSmsEmailTemplateQuery subject(String subject);
		
		@MasterEntityQuerySort(attribute="id")
		RetailerSmsEmailTemplateQuery orderById();
		@MasterEntityQuerySort(attribute="smsEmailTriggerCode")
		RetailerSmsEmailTemplateQuery orderBySmsEmailTriggerCode();
		@MasterEntityQuerySort(attribute="retailerId")
		RetailerSmsEmailTemplateQuery orderByRetailerId();
	}
}
