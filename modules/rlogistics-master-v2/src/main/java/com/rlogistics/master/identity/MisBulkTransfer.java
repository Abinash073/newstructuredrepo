package com.rlogistics.master.identity;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface MisBulkTransfer {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getRetailer();

	void setRetailer(String retailer);
	
	@MasterEntityAttribute
	String getDropLocationId();

	void setDropLocationId(String dropLocationId);
	
	@MasterEntityAttribute
	String getDropLocationName();

	void setDropLocationName(String dropLocationName);
	
	@MasterEntityAttribute
	String getAwbAmazon();

	void setAwbAmazon(String awbAmazon);
	
	@MasterEntityAttribute
	String getConsumerName();

	void setConsumerName(String consumerName);
	
	@MasterEntityAttribute
	String getAddress();

	void setAddress(String address);
	
	@MasterEntityAttribute
	String getProductCode();

	void setProductCode(String productCode);
	
	@MasterEntityAttribute
	String getDescription();

	void setDescription(String description);
	
	@MasterEntityAttribute
	String getValue();

	void setValue(String value);
	
	@MasterEntityAttribute
	String getDateOfReceipt();

	void setDateOfReceipt(String dateOfReceipt);
	
	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);
	
	@MasterEntityAttribute
	String getTicketNo();

	void setTicketNo(String ticketNo);
	
	@MasterEntityAttribute
	String getBarcode();

	void setBarcode(String barcode);
	
	@MasterEntityAttribute
	String getBoxNumber();

	void setBoxNumber(String boxNumber);
	@MasterEntityAttribute
	String getProductCategory();

	void setProductCategory(String productCategory);
	@MasterEntityAttribute
	String getActionType();

	void setActionType(String actionType);
	@MasterEntityAttribute
	String getSubTicketNo();

	void setSubTicketNo(String subTicketNo);
	
	@MasterEntityAttribute
	String getItemQuality();

	void setItemQuality(String itemQuality);
	
	@MasterEntityQuery
	public interface MisBulkTransferQuery extends Query<MisBulkTransferQuery,MisBulkTransfer>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		MisBulkTransferQuery id(String id);
		@MasterEntityQueryParameter(attribute="processId")
		MisBulkTransferQuery processId(String processId);
		@MasterEntityQueryParameter(attribute="retailerId")
		MisBulkTransferQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="dropLocationId")
		MisBulkTransferQuery dropLocationId(String dropLocationId);
		@MasterEntityQueryParameter(attribute="dropLocationName")
		MisBulkTransferQuery dropLocationName(String dropLocationName);
		@MasterEntityQueryParameter(attribute="retailer")
		MisBulkTransferQuery retailer(String retailer);
		@MasterEntityQueryParameter(attribute="ticketNo")
		MisBulkTransferQuery ticketNo(String ticketNo);
		@MasterEntityQueryParameter(attribute="barcode")
		MisBulkTransferQuery barcode(String barcode);
		@MasterEntityQueryParameter(attribute="boxNumber")
		MisBulkTransferQuery boxNumber(String boxNumber);
		
		@MasterEntityQueryParameter(attribute="awbAmazon")
		MisBulkTransferQuery awbAmazon(String awbAmazon);
		@MasterEntityQueryParameter(attribute="consumerName")
		MisBulkTransferQuery consumerName(String consumerName);
		@MasterEntityQueryParameter(attribute="address")
		MisBulkTransferQuery address(String address);
		@MasterEntityQueryParameter(attribute="productCode")
		MisBulkTransferQuery productCode(String productCode);
		
		@MasterEntityQueryParameter(attribute="productCategory")
		MisBulkTransferQuery productCategory(String productCategory);
		@MasterEntityQueryParameter(attribute="actionType")
		MisBulkTransferQuery actionType(String actionType);
		@MasterEntityQueryParameter(attribute="subTicketNo")
		MisBulkTransferQuery subTicketNo(String subTicketNo);
		
		@MasterEntityQueryParameter(attribute="description")
		MisBulkTransferQuery description(String description);
		@MasterEntityQueryParameter(attribute="value")
		MisBulkTransferQuery value(String value);
		@MasterEntityQueryParameter(attribute="dateOfReceipt")
		MisBulkTransferQuery dateOfReceipt(String dateOfReceipt);
		@MasterEntityQueryParameter(attribute="status")
		MisBulkTransferQuery status(String status);
		@MasterEntityQueryParameter(attribute="itemQuality")
		MisBulkTransferQuery itemQuality(String itemQuality);
		
		@MasterEntityQuerySort(attribute="id")
		MisBulkTransferQuery orderById();
		@MasterEntityQuerySort(attribute="awbAmazon")
		MisBulkTransferQuery orderByAwbAmazon();
	}
}