package com.rlogistics.master.identity;

import com.rlogistics.master.*;
import org.activiti.engine.query.Query;

@MasterEntity
@MasterEntityLeadsToRestController
public interface CurrentLocation {
    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getFeId();

    void setFeId(String feId);

    @MasterEntityAttribute
    String getLatitude();

    void setLatitude(String latitude);

    @MasterEntityAttribute
    String getLongitude();

    void setLongitude(String longitude);

    @MasterEntityAttribute
    String getDate();

    void setDate(String date);

    @MasterEntityQuery
    public interface CurrentLocationQuery extends Query<CurrentLocationQuery, CurrentLocation> {
        @MasterEntityQueryParameter(attribute = "id", idAttribute = true)
        public CurrentLocationQuery id(String id);

        @MasterEntityQueryParameter(attribute = "feId")
        public CurrentLocationQuery feId(String feId);

        @MasterEntityQueryParameter(attribute = "latitude")
        public CurrentLocationQuery latitude(String latitude);

        @MasterEntityQueryParameter(attribute = "longitude")
        public CurrentLocationQuery longitude(String longitude);

        @MasterEntityQueryParameter(attribute = "date")
        public CurrentLocationQuery date(String date);
    }
}
