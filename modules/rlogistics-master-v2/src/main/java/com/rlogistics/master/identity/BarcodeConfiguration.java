package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.rlogistics.master.identity.BarcodeOffset.BarcodeOffsetQuery;

@MasterEntity
@MasterEntityLeadsToRestController
public interface BarcodeConfiguration {
	
	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getFile();

	void setFile(String file);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface BarcodeConfigurationQuery extends Query<BarcodeConfigurationQuery,BarcodeConfiguration>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		BarcodeConfigurationQuery id(String id);
		@MasterEntityQueryParameter(attribute = "file")
		BarcodeConfigurationQuery file(String file);
		
		@MasterEntityQuerySort(attribute = "id")
		BarcodeConfigurationQuery orderById();
	}
}
