package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface ProductSubCategory {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getName();

	void setName(String name);

	@MasterEntityAttribute
	String getCode();

	void setCode(String code);

	@MasterEntityAttribute
	String getProductCategoryId();

	void setProductCategoryId(String masterCategoryId);

	@MasterEntityAttribute
	String getDescription();
	
	void setDescription(String description);
	
	@MasterEntityAttribute
	double getVolumetricWeight();
	
	void setVolumetricWeight(double volumetricWeight);
	
	@MasterEntityAttribute
	double getActualWeight();
	
	void setActualWeight(double actualWeight);
	
	@MasterEntityAttribute
	double getLength();
	
	void setLength(double length);
	
	@MasterEntityAttribute
	double getBreadth();
	
	void setBreadth(double breadth);
	
	@MasterEntityAttribute
	double getHeight();
	
	void setHeight(double height);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);

	@MasterEntityQuery
	public interface ProductSubCategoryQuery extends Query<ProductSubCategoryQuery,ProductSubCategory>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProductSubCategoryQuery id(String id);
		@MasterEntityQueryParameter(attribute = "name")
		ProductSubCategoryQuery name(String name);
		@MasterEntityQueryParameter(attribute = "code",codeAttribute=true)
		ProductSubCategoryQuery code(String code);
		@MasterEntityQueryParameter(attribute = "productCategoryId")
		ProductSubCategoryQuery productCategoryId(String productCategoryId);

		@MasterEntityQuerySort(attribute = "id")
		ProductSubCategoryQuery orderById();
		@MasterEntityQuerySort(attribute = "name")
		ProductSubCategoryQuery orderByName();
		@MasterEntityQuerySort(attribute = "code")
		ProductSubCategoryQuery orderByCode();
		@MasterEntityQuerySort(attribute = "productCategoryId")
		ProductSubCategoryQuery orderByProductCategoryId();
	}

}
