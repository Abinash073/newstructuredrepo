/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_USER")
@MasterEntityLeadsToRestController
public interface ProcessUser{

	@MasterEntityAttribute
	String getId();
	void setId(String id);

	@MasterEntityAttribute(columnName="FIRST_")
	String getFirstName();
	void setFirstName(String firstName);

	@MasterEntityAttribute(columnName="LAST_")
	String getLastName();
	void setLastName(String lastName);

	@MasterEntityAttribute
	String getEmail();
	void setEmail(String email);

	@MasterEntityAttribute(columnName="PWD_")
	String getPassword();
	void setPassword(String string);

	@MasterEntityAttribute
	String getPhone();
	void setPhone(String phone);
	
	@MasterEntityAttribute
	String getRoleCode();
	void setRoleCode(String roleCode);
	
	@MasterEntityAttribute
	String getLocationId();
	void setLocationId(String locationId);
	
	@MasterEntityAttribute
	String getRetailerId();
	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	int getStatus();
	void setStatus(int status);

	@MasterEntityAttribute
	String getAgencyId();
	void setAgencyId(String agencyId);
	
	@MasterEntityQuery
	public interface ProcessUserQuery extends Query<ProcessUserQuery,ProcessUser>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		ProcessUserQuery id(String id);
		@MasterEntityQueryParameter(attribute = "email")
		ProcessUserQuery userId(String userId);
		@MasterEntityQueryParameter(attribute = "email",codeAttribute=true)
		ProcessUserQuery email(String email);
		@MasterEntityQueryParameter(attribute = "roleCode")
		ProcessUserQuery roleCode(String roleCode);
		@MasterEntityQueryParameter(attribute = "locationId")
		ProcessUserQuery locationId(String locationId);
		@MasterEntityQueryParameter(attribute = "firstName")
		ProcessUserQuery firstName(String firstName);
		@MasterEntityQueryParameter(attribute = "lastName")
		ProcessUserQuery lastName(String lastName);
		@MasterEntityQueryParameter(attribute = "retailerId")
		ProcessUserQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "status")
		ProcessUserQuery status(int status);
		@MasterEntityQueryParameter(attribute = "password")
		ProcessUserQuery password(String password);
		@MasterEntityQueryParameter(attribute = "agencyId")
		ProcessUserQuery agencyId(String agencyId);
		
		@MasterEntityQuerySort(attribute = "lastName")
		ProcessUserQuery orderByUserLastName();

		@MasterEntityQuerySort(attribute = "firstName")
		ProcessUserQuery orderByUserFirstName();

		@MasterEntityQuerySort(attribute = "email")
		ProcessUserQuery orderByUserEmail();

		@MasterEntityQuerySort(attribute = "email")
		ProcessUserQuery orderByUserId();

		@MasterEntityQuerySort(attribute = "phone")
		ProcessUserQuery orderByUserPhone();
		
		@MasterEntityQuerySort(attribute = "roleCode")
		ProcessUserQuery orderByRoleCode();
		
		@MasterEntityQuerySort(attribute = "locationId")
		ProcessUserQuery orderByLocationId();

		@MasterEntityQuerySort(attribute = "agencyId")
		ProcessUserQuery orderByAgencyId();
	}

}
