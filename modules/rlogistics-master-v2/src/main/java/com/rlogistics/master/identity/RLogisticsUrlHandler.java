package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity(tableName="RL_MD_URL_HANDLER")
@MasterEntityLeadsToRestController
public interface RLogisticsUrlHandler {

	@MasterEntityAttribute
	String getId();
	void setId(String id);

	@MasterEntityAttribute
	String getName();
	void setName(String name);
	
	@MasterEntityAttribute
	String getContextDefiner();
	void setContextDefiner(String contextDefiner);
	
	@MasterEntityAttribute
	String getHandler();
	void setHandler(String handler);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RLogisticsUrlHandlerQuery extends Query<RLogisticsUrlHandlerQuery,RLogisticsUrlHandler>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RLogisticsUrlHandlerQuery id(String id);

		@MasterEntityQueryParameter(attribute = "name")
		RLogisticsUrlHandlerQuery name(String name);
		
		@MasterEntityQuerySort(attribute = "id")
		RLogisticsUrlHandlerQuery orderById();

		@MasterEntityQuerySort(attribute = "name")
		RLogisticsUrlHandlerQuery orderByName();	
	}
}
