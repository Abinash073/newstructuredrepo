package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerChequeCollectionCenterCity {

	@MasterEntityAttribute
	String getId();

	void setId(String id);

	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getChequeCollectionCenterId();

	void setChequeCollectionCenterId(String chequeCollectionCenterId);
	
	@MasterEntityAttribute
	String getCityId();

	void setCityId(String cityId);
	
	@MasterEntityAttribute
	String getCityName();

	void setCityName(String cityName);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerChequeCollectionCenterCityQuery extends Query<RetailerChequeCollectionCenterCityQuery,RetailerChequeCollectionCenterCity>{
		@MasterEntityQueryParameter(attribute = "id",idAttribute=true)
		RetailerChequeCollectionCenterCityQuery id(String id);
		@MasterEntityQueryParameter(attribute = "chequeCollectionCenterId")
		RetailerChequeCollectionCenterCityQuery chequeCollectionCenterId(String chequeCollectionCenterId);
		@MasterEntityQueryParameter(attribute = "cityId")
		RetailerChequeCollectionCenterCityQuery cityId(String cityId);
		@MasterEntityQueryParameter(attribute = "retailerId")
		RetailerChequeCollectionCenterCityQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute = "cityName")
		RetailerChequeCollectionCenterCityQuery cityName(String cityName);
		
		@MasterEntityQuerySort(attribute = "id")
		RetailerChequeCollectionCenterCityQuery orderById();
		@MasterEntityQuerySort(attribute = "chequeCollectionCenterId")
		RetailerChequeCollectionCenterCityQuery orderByChequeCollectionCenterId();
		@MasterEntityQuerySort(attribute = "cityId")
		RetailerChequeCollectionCenterCityQuery orderByCityId();
		@MasterEntityQuerySort(attribute = "retailerId")
		RetailerChequeCollectionCenterCityQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute = "cityName")
		RetailerChequeCollectionCenterCityQuery orderByCityName();
	}
}
