package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerServicesCost {
	
	@MasterEntityAttribute
	public String getId();
	
	public void setId(String id);
	
	@MasterEntityAttribute
	public String getRetailerId();
	
	public void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	public String getCategoryId();
	
	public void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	public String getServiceId();
	
	public void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	public double getCost();
	
	public void setCost(double cost);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerServicesCostQuery extends Query<RetailerServicesCostQuery,RetailerServicesCost>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerServicesCostQuery id(String id);
		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerServicesCostQuery retailerId(String retailerId);
		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerServicesCostQuery categoryId(String categoryId);
		@MasterEntityQueryParameter(attribute="serviceId")
		public RetailerServicesCostQuery serviceId(String serviceId);
		@MasterEntityQueryParameter(attribute="cost")
		public RetailerServicesCostQuery cost(double cost);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerServicesCostQuery orderById();
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerServicesCostQuery orderByRetailerId();
		@MasterEntityQuerySort(attribute="categoryId")
		public RetailerServicesCostQuery orderByCategoryId();		
		@MasterEntityQuerySort(attribute="serviceId")
		public RetailerServicesCostQuery orderByServiceId();
		@MasterEntityQuerySort(attribute="cost")
		public RetailerServicesCostQuery orderByCost();
	}
}
