package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface SmsEmailLog {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getProcessId();

	void setProcessId(String processId);
	
	@MasterEntityAttribute
	int getType();

	void setType(int type);
	
	@MasterEntityAttribute
	String getStatus();

	void setStatus(String status);
	
	@MasterEntityAttribute
	String getMessage();

	void setMessage(String message);
	
	@MasterEntityAttribute
	String getRecipient();

	void setRecipient(String recipient);
	
	@MasterEntityAttribute
	String getApiMessage();

	void setApiMessage(String errorMessage);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface SmsEmailLogQuery extends Query<SmsEmailLogQuery,SmsEmailLog>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		SmsEmailLogQuery id(String id);
		@MasterEntityQueryParameter(attribute="processId")
		SmsEmailLogQuery processId(String processId);
		@MasterEntityQueryParameter(attribute="status")
		SmsEmailLogQuery status(String status);
		@MasterEntityQueryParameter(attribute="type")
		SmsEmailLogQuery type(int type);
		@MasterEntityQueryParameter(attribute="recipient")
		SmsEmailLogQuery recipient(String recipient);
		
		@MasterEntityQuerySort(attribute="id")
		SmsEmailLogQuery orderById();
		@MasterEntityQuerySort(attribute="processId")
		SmsEmailLogQuery orderByProcessId();
		@MasterEntityQuerySort(attribute="status")
		SmsEmailLogQuery orderByStatus();
		@MasterEntityQuerySort(attribute="type")
		SmsEmailLogQuery orderByType();
	}
}
