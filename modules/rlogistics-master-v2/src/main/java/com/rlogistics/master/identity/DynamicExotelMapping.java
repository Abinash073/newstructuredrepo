package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface DynamicExotelMapping {

    @MasterEntityAttribute
    String getId();

    void setId(String id);

    @MasterEntityAttribute
    String getLocation();

    void setLocation(String location);

    @MasterEntityAttribute
    String getExotelPhone();

    void setExotelPhone(String exotelPhone);

    @MasterEntityAttribute
    String getStatus();

    void setStatus(String status);

    @MasterEntityAttribute
    Date getModifiedOn();

    void setModifiedOn(Date date);

    @MasterEntityAttribute
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    @MasterEntityAttribute
    String getModifiedBy();

    void setModifiedBy(String modifiedBy);

    @MasterEntityQuery
    public interface DynamicExotelMappingQuery extends Query<DynamicExotelMappingQuery,DynamicExotelMapping>{
        @MasterEntityQueryParameter(attribute="id",idAttribute=true)
        DynamicExotelMappingQuery id(String id);
        @MasterEntityQueryParameter(attribute="location"/*codeAttribute=true*/)
        DynamicExotelMappingQuery location(String location);
        @MasterEntityQueryParameter(attribute="exotelPhone")
        DynamicExotelMappingQuery exotelPhone(String exotelPhone);
        @MasterEntityQueryParameter(attribute="status")
        DynamicExotelMappingQuery status(String status);

        @MasterEntityQuerySort(attribute="id")
        DynamicExotelMappingQuery orderById();
        /*@MasterEntityQuerySort(attribute="state")
        DynamicExotelMappingQuery orderByState();
        @MasterEntityQuerySort(attribute="productId")
        DynamicExotelMappingQuery orderByProductId();
        @MasterEntityQuerySort(attribute="brandId")
        DynamicExotelMappingQuery orderByBrandId();
        @MasterEntityQuerySort(attribute="categoryId")
        DynamicExotelMappingQuery orderByCategoryId();
        @MasterEntityQuerySort(attribute="subCategoryId")
        DynamicExotelMappingQuery orderBySubCategoryId();
        */
    }
}
