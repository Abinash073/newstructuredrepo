package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface SmsEmailTemplate {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getSmsEmailTriggerCode();

	void setSmsEmailTriggerCode(String smsEmailTriggerCode);
	
	@MasterEntityAttribute
	String getSmsTemplate();

	void setSmsTemplate(String smsTemplate);
	
	@MasterEntityAttribute
	String getEmailTemplate();

	void setEmailTemplate(String emailTemplate);
	
	@MasterEntityAttribute
	String getSetupScript();
	
	void setSetupScript(String setupScript);
	
	@MasterEntityAttribute
	String getParams();
	
	void setParams(String params);
	
	@MasterEntityAttribute
	String getSubject();
	
	void setSubject(String subject);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface SmsEmailTemplateQuery extends Query<SmsEmailTemplateQuery,SmsEmailTemplate>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		SmsEmailTemplateQuery id(String id);
		@MasterEntityQueryParameter(attribute="smsEmailTriggerCode",codeAttribute=true)
		SmsEmailTemplateQuery smsEmailTriggerCode(String smsEmailTriggerCode);
		@MasterEntityQueryParameter(attribute="smsTemplate")
		SmsEmailTemplateQuery smsTemplate(String smsTemplate);
		@MasterEntityQueryParameter(attribute="emailTemplate")
		SmsEmailTemplateQuery emailTemplate(String emailTemplate);
		@MasterEntityQueryParameter(attribute="params")
		SmsEmailTemplateQuery params(String params);
		@MasterEntityQueryParameter(attribute="subject")
		SmsEmailTemplateQuery subject(String subject);
		
		@MasterEntityQuerySort(attribute="id")
		SmsEmailTemplateQuery orderById();
		@MasterEntityQuerySort(attribute="smsEmailTriggerCode")
		SmsEmailTemplateQuery orderBySmsEmailTriggerCode();
	}
}
