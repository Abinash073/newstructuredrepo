package com.rlogistics.master.identity;

import java.util.Date;

import org.activiti.engine.query.Query;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;

@MasterEntity
@MasterEntityLeadsToRestController
public interface RetailerCostingActivity {

	@MasterEntityAttribute
	String getId();

	void setId(String id);
	
	@MasterEntityAttribute
	String getRetailerId();

	void setRetailerId(String retailerId);
	
	@MasterEntityAttribute
	String getCategoryId();

	void setCategoryId(String categoryId);
	
	@MasterEntityAttribute
	String getServiceId();

	void setServiceId(String serviceId);
	
	@MasterEntityAttribute
	String getCostingActivityCode();

	void setCostingActivityCode(String costingActivityCode);
	
	@MasterEntityAttribute
	float getCost();

	void setCost(float cost);
	
	@MasterEntityAttribute
	int getStatus();

	void setStatus(int status);
	
	@MasterEntityAttribute
	Date getModifiedOn();

	void setModifiedOn(Date date);
	
	@MasterEntityAttribute
	String getCreatedBy();
	
	void setCreatedBy(String createdBy);
	
	@MasterEntityAttribute
	String getModifiedBy();
	
	void setModifiedBy(String modifiedBy);
	
	@MasterEntityQuery
	public interface RetailerCostingActivityQuery extends Query<RetailerCostingActivityQuery,RetailerCostingActivity>{
		@MasterEntityQueryParameter(attribute="id",idAttribute=true)
		public RetailerCostingActivityQuery id(String id);

		@MasterEntityQueryParameter(attribute="retailerId")
		public RetailerCostingActivityQuery retailerId(String retailerId);
		
		@MasterEntityQueryParameter(attribute="categoryId")
		public RetailerCostingActivityQuery categoryId(String categoryId);
		
		@MasterEntityQueryParameter(attribute="serviceId")
		public RetailerCostingActivityQuery serviceId(String serviceId);

		@MasterEntityQueryParameter(attribute="costingActivityCode")
		public RetailerCostingActivityQuery costingActivityCode(String costingActivityCode);
		
		@MasterEntityQueryParameter(attribute="cost")
		public RetailerCostingActivityQuery cost(float cost);
		
		@MasterEntityQueryParameter(attribute="status")
		public RetailerCostingActivityQuery status(int status);
		
		@MasterEntityQuerySort(attribute="id")
		public RetailerCostingActivityQuery orderById();
		
		@MasterEntityQuerySort(attribute="retailerId")
		public RetailerCostingActivityQuery orderByRetailerId();
		
		@MasterEntityQuerySort(attribute="cost")
		public RetailerCostingActivityQuery orderByCost();		
	}
}
