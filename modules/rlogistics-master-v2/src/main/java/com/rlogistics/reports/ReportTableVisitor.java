package com.rlogistics.reports;

import org.activiti.bpmn.model.UserTask;

public interface ReportTableVisitor{
	void beforeVisit();
	boolean visitProcessReportTable(String processTableName);
	boolean visitUserTaskReportTable(String userTaskTableName,UserTask userTask);
	void afterVisit();
}