package com.rlogistics.reports;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.reports.UserTaskUtil.Column;

public class ReportsDDLStatementsProvider {
	static Logger log = LoggerFactory.getLogger(ReportsDDLStatementsProvider.class);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String provideProcessReportDDLStatement(Map params){
		String processReportTable = (String) params.get("processReportTable");
		List<Column> columns = (List<Column>) params.get("columns");
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("CREATE TABLE " + processReportTable + "(");
		
		buffer.append("EXECUTION_ID_ VARCHAR(64) NOT NULL");
		buffer.append(",PROCESS_DEPLOYMENT_ID_ VARCHAR(64) NOT NULL");
		buffer.append(",NAME_ VARCHAR(256) NOT NULL");
		for(Column column:columns){
			buffer.append(
				","+
				column.getName() +
				" BIGINT UNSIGNED REFERENCES RL_RPT_VALUES(ID_)" 
			);
		}
		
		buffer.append(", PRIMARY KEY (EXECUTION_ID_)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin");
		
		return buffer.toString();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String provideUserTaskReportDDLStatement(Map params){
		String userTaskReportTable = (String) params.get("userTaskReportTable");
		List<Column> columns = (List<Column>) params.get("columns");
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("CREATE TABLE " + userTaskReportTable + "(");
		
		buffer.append("EXECUTION_ID_ VARCHAR(64) NOT NULL");
		buffer.append(",TASK_ID_ VARCHAR(64) NOT NULL");
		buffer.append(",NAME_ VARCHAR(256) NOT NULL");
		for(Column column:columns){
			buffer.append(
				","+
				column.getName() +
				" BIGINT UNSIGNED REFERENCES RL_RPT_VALUES(ID_)" 
			);
		}
		
		buffer.append(", PRIMARY KEY (EXECUTION_ID_)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin");
		
		return buffer.toString();
	}
}
