package com.rlogistics.reports;

import java.util.ArrayList;
import java.util.List;

import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.commons.lang3.text.translate.AggregateTranslator;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.ProcessReportTable;
import com.rlogistics.master.identity.UserTaskReportTable;
import com.rlogistics.master.processor.CustomQueriesServiceUtil;
import com.rlogistics.reports.UserTaskUtil.Column;
import com.rlogistics.customqueries.impl.CustomQueriesService;

public class ReportTableCreatingVisitor implements ReportTableVisitor
{
	static Logger log = LoggerFactory.getLogger(ReportTableCreatingVisitor.class);
	
	private ProcessDeployment processDeployment;
	private boolean dontCreate = false;

	private String processTableName;
	private List<Column> columnsAcrossTasks = new ArrayList<UserTaskUtil.Column>();
	
	public ReportTableCreatingVisitor(ProcessDeployment processDeployment) {
		this.processDeployment = processDeployment;

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		ProcessReportTable reportTable = masterdataService.createProcessReportTableQuery()
				.processDeploymentId(processDeployment.getId()).singleResult();
		
		dontCreate = (reportTable != null);
	}

	@Override
	public void beforeVisit() {
	}

	@Override
	public boolean visitProcessReportTable(String processTableName) {
		if(dontCreate){
			return false;
		}

		this.processTableName = processTableName;
		
		return true;
	}

	@Override
	public boolean visitUserTaskReportTable(String userTaskTableName,UserTask userTask) {
		if(dontCreate){
			return false;
		}

		List<Column> columns = UserTaskUtil.listColumns(userTask);
		
		if(columns.isEmpty()){
			return false;
		}
		
		log.info("UserTaskReport Table " + userTaskTableName + " will be created");
		
		CustomQueriesService.ReportsDDLStatementsMapper.createUserTaskReportTable(userTaskTableName, columns);

		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		
		UserTaskReportTable userTaskReportTable = masterdataService.newUserTaskReportTable();
		userTaskReportTable.setProcessDeploymentId(processDeployment.getId());
		userTaskReportTable.setUserTaskId(userTask.getId());
		userTaskReportTable.setReportTable(userTaskTableName);
		
		masterdataService.saveUserTaskReportTable(userTaskReportTable);

		columnsAcrossTasks.addAll(columns);
		
		return true;
	}

	@Override
	public void afterVisit() {
		log.info("ProcessReport Table " + processTableName + " will be created");
		
		CustomQueriesService.ReportsDDLStatementsMapper.createProcessReportTable(processTableName, UserTaskUtil.filterDuplicates(columnsAcrossTasks));
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		
		ProcessReportTable reportTable = masterdataService.newProcessReportTable();
		reportTable.setProcessDeploymentId(processDeployment.getId());
		reportTable.setReportTable(processTableName);
		masterdataService.saveProcessReportTable(reportTable);
	}
}
