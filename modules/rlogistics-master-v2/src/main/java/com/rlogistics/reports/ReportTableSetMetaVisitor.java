package com.rlogistics.reports;

import java.io.ByteArrayInputStream;
import java.util.Map;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowElementsContainer;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.persistence.entity.DeploymentEntity;
import org.activiti.engine.impl.persistence.entity.ResourceEntity;
import org.activiti.engine.impl.util.io.InputStreamSource;

import com.rlogistics.activiti.RLogisticsProcessEngineImpl;
import com.rlogistics.master.identity.MasterdataService;

public class ReportTableSetMetaVisitor {
	private ProcessDeployment processDeployment;
	private ReportTableVisitor visitor;

	public ReportTableSetMetaVisitor(ProcessDeployment processDeployment,ReportTableVisitor visitor) {
		this.processDeployment = processDeployment;
		this.visitor = visitor;
	}

	public void visitTables() {
		
		visitor.beforeVisit();
		
		MasterdataService masterdataService = ((RLogisticsProcessEngineImpl) (ProcessEngines
				.getDefaultProcessEngine())).getMasterdataService();
		ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();

		DeploymentEntity deploymentEntity = Context.getCommandContext().getDeploymentEntityManager()
				.findDeploymentById(processDeployment.getDeploymentId());

		Map<String, ResourceEntity> resources = deploymentEntity.getResources();

		for (String resourceName : resources.keySet()) {
			if (isBpmnResource(resourceName)) {
				ResourceEntity resource = resources.get(resourceName);
				byte[] bytes = resource.getBytes();
				ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

				BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(new InputStreamSource(inputStream), true, processEngineConfiguration.isEnableSafeBpmnXml());
				for(FlowElement flowElement:bpmnModel.getMainProcess().getFlowElements()){
					visitUserTasks(flowElement);
				}
			}
		}

		visitor.visitProcessReportTable(makeProcessReportTableName());			

		visitor.afterVisit();
	}

	private void visitUserTasks(FlowElement flowElement) {
		if(flowElement instanceof FlowElementsContainer){
			for(FlowElement childFlowElement:((FlowElementsContainer)flowElement).getFlowElements()){
				visitUserTasks(childFlowElement);
			}
		} else if(flowElement instanceof UserTask){
			UserTask userTask = (UserTask) flowElement;

			visitor.visitUserTaskReportTable(makeUserTaskReportTableName(userTask.getId()),userTask);				
		}
	}

	private String makeUserTaskReportTableName(String taskId) {
		long hc = taskId.hashCode();
		hc += Integer.MAX_VALUE;
		return ("RL_RPT" + "_" + processDeployment.getName() + "_" + processDeployment.getDeploymentId() + "_" + hc)
			.replaceAll("-","_").replaceAll(" ", "").toUpperCase();
	}

	private String makeProcessReportTableName() {
		return ("RL_RPT" + "_" + processDeployment.getName() + "_" + processDeployment.getDeploymentId())
			.replaceAll(" ", "").toUpperCase();
	}

	private static final String[] BPMN_RESOURCE_SUFFIXES = new String[] { "bpmn20.xml", "bpmn" };

	protected boolean isBpmnResource(String resourceName) {
		for (String suffix : BPMN_RESOURCE_SUFFIXES) {
			if (resourceName.endsWith(suffix)) {
				return true;
			}
		}
		return false;
	}
}