package com.rlogistics.reports;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;
import com.rlogistics.reports.UserTaskUtil.Column;

@CustomQueriesMapper
public interface ReportsDDLStatementsMapper {

	@SelectProvider(type=ReportsDDLStatementsProvider.class,method="provideProcessReportDDLStatement")
	void createProcessReportTable(@Param("processReportTable") String processReportTable,@Param("columns") List<Column> columns);

	@SelectProvider(type=ReportsDDLStatementsProvider.class,method="provideUserTaskReportDDLStatement")
	void createUserTaskReportTable(@Param("userTaskReportTable") String userTaskReportTable,@Param("columns") List<Column> columns);

}
