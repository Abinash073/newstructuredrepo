package com.rlogistics.reports;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.UserTask;

public class UserTaskUtil {

	public static List<Column> listColumns(UserTask userTask){
		List<Column> columns = new ArrayList<>();
		for(FormProperty property:userTask.getFormProperties()){
			String propertyType = property.getType();

			String dbType = null;
			
			if(propertyType.equals("long")){
				dbType = "INT";
			} else if(propertyType.equals("double")){
				dbType = "FLOAT";
			} else if(propertyType.equals("boolean")){
				dbType = "SMALLINT";
			} else if(propertyType.equals("date")){
				dbType = "DATETIME";
			} else if(propertyType.equals("photo")){
				/* skip */
			} else if(propertyType.equals("signature")){
				/* skip */
			} else {
				dbType = "VARCHAR";
			}
			
			if(dbType == null){
				/* Skip this column */
			}
			
			Column column = new Column(property.getVariable() != null ? property.getVariable() : property.getId(),dbType);
			
			columns.add(column);
		}
		
		return filterDuplicates(columns);
	}
	
	public static List<Column> filterDuplicates(List<Column> in){
		List<Column> out = in.stream().filter(new Predicate<Column>() {
			private HashSet<String> columnNames = new HashSet<String>();
			@Override
			public boolean test(Column column) {
				if(columnNames.contains(column.getName())){
					return false;
				}
				
				columnNames.add(column.getName());
				return true;
			}
		}).collect(Collectors.toCollection(ArrayList<Column>::new));
		return out;
	}
	public static class Column{
		private String name;
		private String type;
		
		public Column(String name,String type){
			setName(name);
			setType(type);
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		
	}
}
