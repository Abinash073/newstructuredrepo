package com.rlogistics.customqueries;

import java.util.Map;

public class TicketMisCustomQueryProvider {
	// UPDATE `rlogistics_e1`.`RL_MD_TICKET_MIS_REPORT` SET `ADDRESS_LINE1_` =
	// 'Yelahanka3' WHERE (`RL_TICKET_NO_` IN (
	// 'DMR-MOB-113-WMJH3','DMR-MOB-113-TWNEC'));
	// UPDATE `rlogistics_e1`.`RL_MD_TICKET_MIS_REPORT` SET `CLOSED_TIME_` =
	// '1', `CLOSED_REASON_` = '1' WHERE (`PROCESS_ID_` = '73314742');
	// update t set columnname=concat(columnname, 'a');

	public String getMisDetails(Map<String, String> params) {
		StringBuffer rtnString = new StringBuffer();
		rtnString
				.append("SELECT m.CONSUMER_NAME_ AS consumerName,m.CONSUMER_COMPLAINT_NUMBER_ AS consumerComplaintNumber,m.PROCESS_ID_ AS processId,m.ADDRESS_LINE1_ AS addressLine1,m.ADDRESS_LINE2_ AS addressLine2,m.CITY_ AS city,m.PINCODE_ AS pincode,m.TELEPHONE_NUMBER_ AS telephoneNumber,m.EMAIL_ID_ AS emailId,m.DATE_OF_COMPLAINT_ AS dateOfComplaint,m.ORDER_NUMBER_ AS orderNumber,m.NATURE_OF_COMPLAINT_ AS natureOfComplaint,m.IS_UNDER_WARRANTY_ AS isUnderWarranty,m.RETAILER_ AS retailer,m.BRAND_ AS brand,m.PRODUCT_CATEGORY_ AS productCategory,m.PRODUCT_NAME_ AS productName,m.MODEL_ AS model,m.IDENTIFICATION_NO_ AS identificationNo,m.DROP_LOC_CONTACT_PERSON_ AS dropLocContactPerson,m.DROP_LOC_CONTACT_NO_ AS dropLocContactNo,m.CURRENT_STATUS_ AS currentStatus,m.DROP_LOCATION_ AS dropLocation,m.RL_TICKET_NO_ AS rlTicketNo,m.RL_LOCATION_NAME_ AS rlLocationName,m.CREATE_DATE_ AS createDate,m.CREATE_TIME_ AS createTime,m.PICKUP_DATE_ AS pickupDate,m.PICKUP_TIME_ AS pickupTime,m.DROP_DATE_ AS dropDate,m.DROP_TIME_ AS dropTime,m.TIME_STAMP_ AS timeStamp,m.DROP_CITY_ dropCity,m.CLOSED_DATE_ AS closedDate,m.CLOSED_TIME_ AS closedTime,m.CLOSED_REASON_ AS closedReason from rlogistics_e1.RL_MD_TICKET_MIS_REPORT m "
						+ "WHERE TIME_STAMP_ >= '" + params.get("from").toString() + "' AND TIME_STAMP_ <= '"
						+ params.get("till").toString() + "'");
		if (params.containsKey("retailer") && !params.get("retailer").toString().equals("")) {
			rtnString.append(" AND RETAILER_ = '" + params.get("retailer").toString() + "'");
		}
		if (params.containsKey("location") && !params.get("location").toString().equals("")
				&& params.get("location").toString().length() > 3) {
			rtnString.append(" AND RL_LOCATION_NAME_ IN (" + params.get("location").toString() + ")");
		}
		return rtnString.toString();

	}

	public String generateManifists(Map<String, String> params) {

		StringBuffer rtnString = new StringBuffer();
		rtnString
				.append("SELECT m.CONSUMER_NAME_ AS consumerName,m.CONSUMER_COMPLAINT_NUMBER_ AS consumerComplaintNumber,m.PROCESS_ID_ AS processId,m.ADDRESS_LINE1_ AS addressLine1,m.ADDRESS_LINE2_ AS addressLine2,m.CITY_ AS city,m.PINCODE_ AS pincode,m.TELEPHONE_NUMBER_ AS telephoneNumber,m.EMAIL_ID_ AS emailId,m.DATE_OF_COMPLAINT_ AS dateOfComplaint,m.ORDER_NUMBER_ AS orderNumber,m.NATURE_OF_COMPLAINT_ AS natureOfComplaint,m.IS_UNDER_WARRANTY_ AS isUnderWarranty,m.RETAILER_ AS retailer,m.BRAND_ AS brand,m.PRODUCT_CATEGORY_ AS productCategory,m.PRODUCT_NAME_ AS productName,m.MODEL_ AS model,m.IDENTIFICATION_NO_ AS identificationNo,m.DROP_LOC_CONTACT_PERSON_ AS dropLocContactPerson,m.DROP_LOC_CONTACT_NO_ AS dropLocContactNo,m.CURRENT_STATUS_ AS currentStatus,m.DROP_LOCATION_ AS dropLocation,m.RL_TICKET_NO_ AS rlTicketNo,m.RL_LOCATION_NAME_ AS rlLocationName,m.CREATE_DATE_ AS createDate,m.CREATE_TIME_ AS createTime,m.PICKUP_DATE_ AS pickupDate,m.PICKUP_TIME_ AS pickupTime,m.DROP_DATE_ AS dropDate,m.DROP_TIME_ AS dropTime,m.TIME_STAMP_ AS timeStamp,m.DROP_CITY_ dropCity,m.CLOSED_DATE_ AS closedDate,m.CLOSED_TIME_ AS closedTime,m.CLOSED_REASON_ AS closedReason from rlogistics_e1.RL_MD_TICKET_MIS_REPORT m "
						+ "WHERE TIME_STAMP_ >= '" + params.get("from").toString() + "' AND TIME_STAMP_ <= '"
						+ params.get("till").toString() + "'");
		if (params.containsKey("retailer") && !params.get("retailer").toString().equals("")) {
			rtnString.append(" AND RETAILER_ = '" + params.get("retailer").toString() + "'");
		}
		if (params.containsKey("pickupLocation") && !params.get("pickupLocation").toString().equals("")
				&& params.get("pickupLocation").toString().length() > 3 && params.containsKey("dropLocation")
				&& !params.get("dropLocation").toString().equals("")
				&& params.get("dropLocation").toString().length() > 3) {
			rtnString.append(" AND RL_LOCATION_NAME_ = '" + params.get("pickupLocation").toString() + "'");
			rtnString.append(" AND DROP_CITY_ = '" + params.get("dropLocation").toString() + "'");
			rtnString.append(" AND CHAR_LENGTH(DOCKET_NO_) < 3");
			// rtnString.append(" AND CURRENT_STATUS_ = 'PICK_UP_DONE'");

		}
		return rtnString.toString();

	}

	public String updateManifist(Map<String, String> params) {
		StringBuffer rtnString = new StringBuffer();
		String str = "SHIPMENT_DISPATCHED";
		rtnString.append("UPDATE rlogistics_e1.RL_MD_TICKET_MIS_REPORT SET DOCKET_NO_ = '"
				+ params.get("docketNo").toString() + "' , REMARK_ = '" + str + "'"+
				", VEHICLE_NO_ = '"+params.get("vehicleNo".toString())+"'"+
				", WEIGHT_ = '"+params.get("weight".toString())+"'"
						+ " WHERE (PROCESS_ID_ IN ("
				+ params.get("processIdArray").toString() + "))");

		return rtnString.toString();

	}

	public String incomingMenifist(Map<String, String> params) {
		StringBuffer rtnString = new StringBuffer();
		String receiveMsg = " , SHIPMENT_RECEIVED";
		rtnString.append("UPDATE rlogistics_e1.RL_MD_TICKET_MIS_REPORT SET REMARK_ = CONCAT(REMARK_ , '" + receiveMsg
				+ "') WHERE (PROCESS_ID_ IN (" + params.get("processIdArray").toString() + "))");

		return rtnString.toString();
	}

	public String showManifist(Map<String, String> params) {
		StringBuffer rtnString = new StringBuffer();
		rtnString
				.append("SELECT m.CONSUMER_NAME_ AS consumerName,m.CONSUMER_COMPLAINT_NUMBER_ AS consumerComplaintNumber,m.PROCESS_ID_ AS processId,m.ADDRESS_LINE1_ AS addressLine1,m.ADDRESS_LINE2_ AS addressLine2,m.CITY_ AS city,m.PINCODE_ AS pincode,m.TELEPHONE_NUMBER_ AS telephoneNumber,m.EMAIL_ID_ AS emailId,m.DATE_OF_COMPLAINT_ AS dateOfComplaint,m.ORDER_NUMBER_ AS orderNumber,m.NATURE_OF_COMPLAINT_ AS natureOfComplaint,m.IS_UNDER_WARRANTY_ AS isUnderWarranty,m.RETAILER_ AS retailer,m.BRAND_ AS brand,m.PRODUCT_CATEGORY_ AS productCategory,m.PRODUCT_NAME_ AS productName,m.MODEL_ AS model,m.IDENTIFICATION_NO_ AS identificationNo,m.DROP_LOC_CONTACT_PERSON_ AS dropLocContactPerson,m.DROP_LOC_CONTACT_NO_ AS dropLocContactNo,m.CURRENT_STATUS_ AS currentStatus,m.DROP_LOCATION_ AS dropLocation,m.RL_TICKET_NO_ AS rlTicketNo,m.RL_LOCATION_NAME_ AS rlLocationName,m.CREATE_DATE_ AS createDate,m.CREATE_TIME_ AS createTime,m.PICKUP_DATE_ AS pickupDate,m.PICKUP_TIME_ AS pickupTime,m.DROP_DATE_ AS dropDate,m.DROP_TIME_ AS dropTime,m.TIME_STAMP_ AS timeStamp,m.DROP_CITY_ dropCity,m.CLOSED_DATE_ AS closedDate,m.CLOSED_TIME_ AS closedTime,m.CLOSED_REASON_ AS closedReason from rlogistics_e1.RL_MD_TICKET_MIS_REPORT m "
						+ "WHERE DOCKET_NO_ = '" + params.get("docketNo").toString() + "'");
		return rtnString.toString();

	}

	public String downloadManifist(Map<String, String> params) {
		StringBuffer rtnString = new StringBuffer();
		rtnString
				.append("SELECT m.CONSUMER_NAME_ AS consumerName,m.CONSUMER_COMPLAINT_NUMBER_ AS consumerComplaintNumber,m.PROCESS_ID_ AS processId,m.ADDRESS_LINE1_ AS addressLine1,m.ADDRESS_LINE2_ AS addressLine2,m.CITY_ AS city,m.PINCODE_ AS pincode,m.TELEPHONE_NUMBER_ AS telephoneNumber,m.EMAIL_ID_ AS emailId,m.DATE_OF_COMPLAINT_ AS dateOfComplaint,m.ORDER_NUMBER_ AS orderNumber,m.NATURE_OF_COMPLAINT_ AS natureOfComplaint,m.IS_UNDER_WARRANTY_ AS isUnderWarranty,m.RETAILER_ AS retailer,m.BRAND_ AS brand,m.PRODUCT_CATEGORY_ AS productCategory,m.PRODUCT_NAME_ AS productName,m.MODEL_ AS model,m.IDENTIFICATION_NO_ AS identificationNo,m.DROP_LOC_CONTACT_PERSON_ AS dropLocContactPerson,m.DROP_LOC_CONTACT_NO_ AS dropLocContactNo,m.CURRENT_STATUS_ AS currentStatus,m.DROP_LOCATION_ AS dropLocation,m.RL_TICKET_NO_ AS rlTicketNo,m.RL_LOCATION_NAME_ AS rlLocationName,m.CREATE_DATE_ AS createDate,m.CREATE_TIME_ AS createTime,m.PICKUP_DATE_ AS pickupDate,m.PICKUP_TIME_ AS pickupTime,m.DROP_DATE_ AS dropDate,m.DROP_TIME_ AS dropTime,m.TIME_STAMP_ AS timeStamp,m.DROP_CITY_ dropCity,m.CLOSED_DATE_ AS closedDate,m.CLOSED_TIME_ AS closedTime,m.CLOSED_REASON_ AS closedReason,m.REMARK_ AS remark,m.DOCKET_NO_ AS docketNo,m.VEHICLE_NO_ AS vehicleNo,m.WEIGHT_ AS weight from rlogistics_e1.RL_MD_TICKET_MIS_REPORT m "
						+ "WHERE DOCKET_NO_ = '" + params.get("docketNo").toString() + "'");
		return rtnString.toString();
	}
	public String updateManifistStatus(Map<String, String> params){
		StringBuffer rtnString = new StringBuffer();
		rtnString.append("UPDATE rlogistics_e1.RL_MD_TICKET_MIS_REPORT SET CURRENT_STATUS_ = '" + params.get("status").toString()
				+ "' WHERE RL_TICKET_NO_ = '" + params.get("ticketNo").toString() + "'");

		return rtnString.toString();
		
	}

}
