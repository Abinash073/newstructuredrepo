package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface FAQCustomQueriesMapper {

	@Select("SELECT f.id_ as id, f.product_id_ as productId, f.brand_id_ as brandId, f.category_id_ as categoryId, f.sub_category_id_ as subCategoryId, f.faq_ as FAQ, c.name_ as categoryName, sc.name_ as subCategoryName, p.name_ as productName, b.name_ as brandName from RL_MD_FAQ f LEFT JOIN RL_MD_PRODUCT p ON(f.product_id_ = p.id_) LEFT JOIN RL_MD_PRODUCT_CATEGORY c ON(f.category_id_ = c.id_) LEFT JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (f.sub_category_id_ = sc.id_) LEFT JOIN RL_MD_BRAND b ON (f.brand_id_ = b.id_) ORDER BY c.name_, sc.name_, b.name_, p.name_ limit #{firstResult},#{maxResults}")
	List<FAQCustomResult> findFAQs(@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
	
	@Select("SELECT count(*) as totalRecords from RL_MD_FAQ f LEFT JOIN RL_MD_PRODUCT p ON(f.product_id_ = p.id_) LEFT JOIN RL_MD_PRODUCT_CATEGORY c ON(f.category_id_ = c.id_) LEFT JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (f.sub_category_id_ = sc.id_) LEFT JOIN RL_MD_BRAND b ON (f.brand_id_ = b.id_)")
	ResultCount findFAQsCount();
}
