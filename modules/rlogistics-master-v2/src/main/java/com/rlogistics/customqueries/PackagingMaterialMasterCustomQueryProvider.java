package com.rlogistics.customqueries;

import java.util.Map;

public class PackagingMaterialMasterCustomQueryProvider {

	public static String provideFindPackagingMaterialsQuery(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("select pm.id_ as id, pm.location_id_ as locationId, pm.packaging_type_code_ as packagingTypeCode, pm.reorder_level_ as reorderLevel, pm.maximum_quantity_allowed_ as maximumQuantityAllowed, pm.available_quantity_ as availableQuantity, l.name_ as locationName, p.name_ as packagingTypeName, p.type_ as packagingType from RL_MD_PACKAGING_MATERIAL_MASTER pm INNER JOIN RL_MD_LOCATION l ON (pm.location_id_ = l.id_) INNER JOIN RL_MD_PACKAGING_TYPE p ON (pm.packaging_type_code_ = p.code_) where 1=1");
		
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and pm.location_id_ = #{locationId}");
		}
		
		if(params.containsKey("packagingTypeCode") && !params.get("packagingTypeCode").equals("")) {
			buffer.append(" ");
			buffer.append("and pm.packaging_type_code_ = #{packagingTypeCode}");
		}
		
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and p.type_ = " + "'" + Integer.valueOf(params.get("packagingType")) + "'");
		}
		buffer.append(" ");
		buffer.append("order by pm.location_id_, pm.packaging_type_code_ limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
		
		return buffer.toString();		
	}
	
	public static String provideFindPackagingMaterialsQueryCount(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("select count(*) as totalRecords from RL_MD_PACKAGING_MATERIAL_MASTER pm INNER JOIN RL_MD_LOCATION l ON (pm.location_id_ = l.id_) INNER JOIN RL_MD_PACKAGING_TYPE p ON (pm.packaging_type_code_ = p.code_) where 1=1");
		
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and pm.location_id_ = #{locationId}");
		}
		
		if(params.containsKey("packagingTypeCode") && !params.get("packagingTypeCode").equals("")) {
			buffer.append(" ");
			buffer.append("and pm.packaging_type_code_ = #{packagingTypeCode}");
		}
		
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and p.type_ = " + "'" + Integer.valueOf(params.get("packagingType")) + "'");
		}
		
		return buffer.toString();		
	}
	
	public static String providePackagingInventorySearch(Map<String,String> params) {
		
		StringBuffer buffer =  new StringBuffer();
		
		buffer.append("SELECT pi.id_ as id, pi.location_id_ as locationId, pi.packaging_type_code_ as packagingTypeCode, pi.barcode_ as barcode, pi.status_ as status, pi.ticket_number_ as ticketNumber, pi.user_id_ as userId, pi.additional_details_ as additionalDetails, pi.imei_no_ as imeiNo, p.name_ as packagingTypeName, p.type_ as packagingType, l.name_ as locationName, l.code_ as locationCode, l.city_ as city, u.first_ as firstName, u.last_ as lastName, u.email_ as email FROM RL_MD_PACKAGING_MATERIAL_INVENTORY pi INNER JOIN RL_MD_PACKAGING_TYPE p ON (pi.packaging_type_code_ = p.code_) INNER JOIN RL_MD_LOCATION l ON (pi.location_id_ = l.id_) LEFT JOIN RL_MD_USER u ON (pi.user_id_ = u.id_) where 1=1");
		
		if(params.containsKey("barcode") && !params.get("barcode").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.barcode_ = #{barcode}");
		}
		
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.location_id_ = #{locationId}");
		}
		
		if(params.containsKey("packagingTypeCode") && !params.get("packagingTypeCode").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.packaging_type_code_ = #{packagingTypeCode}");
		}
		
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and p.type_ = " + Integer.valueOf(params.get("packagingType")));
		}
		
		if(params.containsKey("status") && !params.get("status").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.status_ = " + Integer.valueOf(params.get("status")));
		}
		if(params.containsKey("ticketNumber") && !params.get("ticketNumber").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.ticket_number_ = #{ticketNumber}");
		}
		if(params.containsKey("imeiNo") && !params.get("imeiNo").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.imei_no_ = #{imeiNo}");
		}
		buffer.append(" ");
		buffer.append("limit" + " " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
		
		return buffer.toString();		
	}
	
	public static String providePackagingInventorySearchCount(Map<String,String> params) {
		
		StringBuffer buffer =  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords FROM RL_MD_PACKAGING_MATERIAL_INVENTORY pi INNER JOIN RL_MD_PACKAGING_TYPE p ON (pi.packaging_type_code_ = p.code_) INNER JOIN RL_MD_LOCATION l ON (pi.location_id_ = l.id_) LEFT JOIN RL_MD_USER u ON (pi.user_id_ = u.id_) where 1=1");
		
		if(params.containsKey("barcode") && !params.get("barcode").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.barcode_ = #{barcode}");
		}
		
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.location_id_ = #{locationId}");
		}
		
		if(params.containsKey("packagingTypeCode") && !params.get("packagingTypeCode").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.packaging_type_code_ = #{packagingTypeCode}");
		}
		
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and p.type_ = " + Integer.valueOf(params.get("packagingType")));
		}
		
		if(params.containsKey("status") && !params.get("status").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.status_ = " + Integer.valueOf(params.get("status")));
		}
		if(params.containsKey("ticketNumber") && !params.get("ticketNumber").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.ticket_number_ = #{ticketNumber}");
		}
		if(params.containsKey("imeiNo") && !params.get("imeiNo").equals("")) {
			buffer.append(" ");
			buffer.append("and pi.imei_no_ = #{imeiNo}");
		}
		
		return buffer.toString();		
	}
}
