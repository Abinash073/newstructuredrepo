package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface TicketMisCustomQueryMapper {
	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "getMisDetails")
	List<TicketMisReportPojo> getMisDetails(@Param("from") String from, @Param("till") String till,
			@Param("retailer") String retailer, @Param("location") String location);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "generateManifists")
	List<TicketMisReportPojo> generateManifist(@Param("from") String from, @Param("till") String till,
			@Param("pickupLocation") String pickupLocation, @Param("dropLocation") String dropLocation);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "updateManifist")
	List<TicketMisReportPojo> updateManifist(@Param("processIdArray") String processIdArray,
			@Param("docketNo") String docketNo, @Param("vehicleNo") String vehicalNo, @Param("weight") String weight);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "showManifist")
	List<TicketMisReportPojo> showManifist(@Param("docketNo") String from);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "incomingMenifist")
	List<TicketMisReportPojo> incomingMenifist(@Param("processIdArray") String processIdArray);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "downloadManifist")
	List<TicketMisReportPojo> downloadManifist(@Param("docketNo") String from);

	@SelectProvider(type = TicketMisCustomQueryProvider.class, method = "updateManifistStatus")
	List<TicketMisReportPojo> updateManifistStatus(@Param("ticketNo") String ticketNo,
			@Param("status") String status);

}
