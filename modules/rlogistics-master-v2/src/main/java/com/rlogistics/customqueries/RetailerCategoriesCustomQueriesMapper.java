package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface RetailerCategoriesCustomQueriesMapper {

	@Select("SELECT c.id_ as categoryId,c.name_ as categoryName from RL_MD_PRODUCT_CATEGORY c, RL_MD_RETAILER_CATEGORIES r where c.id_ = r.category_id_ and r.retailer_id_ = #{retailer}")
	List<RetailerCategoriesCustomQuery1Result> findCategories(@Param("retailer") String retailer);
	
	@Select("SELECT c.id_ as categoryId,c.master_category_id_ as masterCategoryId,c.name_ as categoryName, r.id_ as retailerCategoryId, r.retailer_id_ as retailerId from RL_MD_PRODUCT_CATEGORY c LEFT JOIN RL_MD_RETAILER_CATEGORIES r ON (c.id_ = r.category_id_ and r.retailer_id_ = #{retailer})")
	List<RetailerCategoriesCustomQuery1Result> findProductCategories(@Param("retailer") String retailer);
}
