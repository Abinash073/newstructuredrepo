package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@CustomQueriesMapper
public interface FeLocationByLocationCustomQueryMapper {
    @Select("select table1.feId,table1.distance,table2.FIRST_ as firstName,table2.LAST_ as lastName from (select FE_ID_ AS feId,sum(DISTANCE_) as distance from RL_MD_FE_DISTANCE as d where d.DATE_ between #{firstDate} and #{lastDate} and d.LOCATION_NAME_= #{location} group by FE_ID_) as table1,RL_MD_USER as table2 where table1.feId = table2.EMAIL_")
    List<FeNameWithDistance> getFeDistanceByLocation(@Param("firstDate")String firstDate,@Param("lastDate")String lastDate,@Param("location")String location);
}
