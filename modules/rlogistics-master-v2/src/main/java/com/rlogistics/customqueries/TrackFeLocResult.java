	package com.rlogistics.customqueries;

	public class TrackFeLocResult {
		private String id;
		private String feName;
		private String dbName;
		private String appointmentDate;
		private String ticketNo;
		private String currentLatitude;
		private String currentLongitude;
		private String priority;
		private String timeToLocation;
		private String feCity;
		private String delayStatus;
		private String lastAssign;
		private String destination;
		private String distance;
		public String getDistance() {
			return distance;
		}
		public void setDistance(String distance) {
			this.distance = distance;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getFeName() {
			return feName;
		}
		public void setFeName(String feName) {
			this.feName = feName;
		}
		public String getDbName() {
			return dbName;
		}
		public void setDbName(String dbName) {
			this.dbName = dbName;
		}
		public String getAppointmentDate() {
			return appointmentDate;
		}
		public void setAppointmentDate(String appointmentDate) {
			this.appointmentDate = appointmentDate;
		}
		public String getTicketNo() {
			return ticketNo;
		}
		public void setTicketNo(String ticketNo) {
			this.ticketNo = ticketNo;
		}
		public String getCurrentLatitude() {
			return currentLatitude;
		}
		public void setCurrentLatitude(String currentLatitude) {
			this.currentLatitude = currentLatitude;
		}
		public String getCurrentLongitude() {
			return currentLongitude;
		}
		public void setCurrentLongitude(String currentLongitude) {
			this.currentLongitude = currentLongitude;
		}
		public String getPriority() {
			return priority;
		}
		public void setPriority(String priority) {
			this.priority = priority;
		}
		public String getTimeToLocation() {
			return timeToLocation;
		}
		public void setTimeToLocation(String time) {
			this.timeToLocation = time;
		}
		public String getFeCity() {
			return feCity;
		}
		public void setFeCity(String feCity) {
			this.feCity = feCity;
		}
		public String getDelayStatus() {
			return delayStatus;
		}
		public void setDelayStatus(String delayStatus) {
			this.delayStatus = delayStatus;
		}
		public String getLastAssign() {
			return lastAssign;
		}
		public void setLastAssign(String lastAssign) {
			this.lastAssign = lastAssign;
		}
		public String getDestination() {
			return destination;
		}
		public void setDestination(String destination) {
			this.destination = destination;
		}

	}
