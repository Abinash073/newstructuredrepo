package com.rlogistics.customqueries;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface RoleAccessCustomQueriesMapper {

	@SelectProvider(type=RoleAccessCustomQueryProvider.class,method="provideGetRoleAccessQuery")
	List<RoleAccessResult> getRoleAccess(@Param("roleCode") String roleCode, @Param("orderBy") ArrayList<String> orderBy);
	
//	Newly Created
	@SelectProvider(type=RoleAccessCustomQueryProvider.class,method="provideGetRetailerRoleAccessQuery")
	List<RoleAccessResult> getRoleAccessRetailer(@Param("roleId") String roleId,@Param("roleCode") String roleCode, @Param("orderBy") ArrayList<String> orderBy);
	
	@Delete("DELETE FROM RL_MD_ROLE_ACCESS where LOWER(role_code_) = #{roleCode}")
	boolean deleteRoleAccess(@Param("roleCode") String roleCode);
	
	@Select("SELECT f.id_ as id, f.code_ as code, f.name_ as name, f.order_ as featureOrder, f.menu_id_ as menuId, f.link_ as link, f.level_ as level, f.parent_ as parent, m.name_ as menuName, f.parent_ as parent, f.level_ as level, f.has_sub_menu_ as hasSubMenu, m.link_ as menuLink from RL_MD_MENU m INNER JOIN RL_MD_FEATURE f ON (m.id_ = f.menu_id_) limit #{firstResult},#{maxResults}")
	List<FeatureCustomResult> getFeatureListing(@Param("firstResult") int firstResult, @Param("maxResults") int maxResults);
	
	@Select("SELECT count(*) as totalRecords from RL_MD_MENU m INNER JOIN RL_MD_FEATURE f ON (m.id_ = f.menu_id_)")
	ResultCount getFeatureListingCount();
}
