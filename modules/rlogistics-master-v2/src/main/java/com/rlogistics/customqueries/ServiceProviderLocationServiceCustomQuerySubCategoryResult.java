package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductSubCategoryRestData;

public class ServiceProviderLocationServiceCustomQuerySubCategoryResult extends  ProductSubCategoryRestData{

	private String subCategoryId;
	private String subCategoryName;
	
	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
}
