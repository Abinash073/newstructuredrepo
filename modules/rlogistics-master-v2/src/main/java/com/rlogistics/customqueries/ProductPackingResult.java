package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductPackingRestData;

public class ProductPackingResult extends ProductPackingRestData {
	
	private String productName;
	private String packagingTypeName;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPackagingTypeName() {
		return packagingTypeName;
	}
	public void setPackagingTypeName(String packagingTypeName) {
		this.packagingTypeName = packagingTypeName;
	}
}
