package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.BrandRestData;

public class ServiceProviderLocationServiceCustomQueryBrandResult extends  BrandRestData {

	private String brandId;
	private String brandName;
	
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
}
