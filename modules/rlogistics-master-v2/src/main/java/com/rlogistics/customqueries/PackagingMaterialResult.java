package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.PackagingMaterialMasterRestData;

public class PackagingMaterialResult extends PackagingMaterialMasterRestData{

	private String locationName;
	private String packagingTypeName;
	private String packagingType;
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getPackagingTypeName() {
		return packagingTypeName;
	}
	public void setPackagingTypeName(String packagingTypeName) {
		this.packagingTypeName = packagingTypeName;
	}
	public String getPackagingType() {
		return packagingType;
	}
	public void setPackagingType(String packagingType) {
		this.packagingType = packagingType;
	}
	
}
