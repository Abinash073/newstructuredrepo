package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface BulkImeiCustomQueriesMapper {
    @SelectProvider(type = BulkImeiCustomQueryProvider.class,method = "getBulkImeiQueryByDate")
    List<BulkImeiNoOfRetaier> getImeibyDate(@Param("startDate")String startDate, @Param("endDate")String endDate, @Param("retailerId")String retailerId);
}
