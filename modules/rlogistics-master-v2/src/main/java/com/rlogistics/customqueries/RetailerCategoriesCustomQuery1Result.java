package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerCategoriesRestData;

public class RetailerCategoriesCustomQuery1Result extends RetailerCategoriesRestData {

	private String categoryId;
	private String categoryName;
	private String retailerCategoryId;
	private String masterCategoryId;
	
	public String getMasterCategoryId() {
		return masterCategoryId;
	}
	public void setMasterCategoryId(String masterCategoryId) {
		this.masterCategoryId = masterCategoryId;
	}
	public String getRetailerCategoryId() {
		return retailerCategoryId;
	}
	public void setRetailerCategoryId(String retailerCategoryId) {
		this.retailerCategoryId = retailerCategoryId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
