package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface PackingSpecificationServiceMapper {
	
	@Select("SELECT P.ID_ as id,P.CATEGORY_ID_ as categoryId,P.SERVICE_ID_ as serviceId,P.OLD_PICKUP_LOCAL_ as oldPickupLocal,P.OLD_PICKUP_OUTSTATION_ as oldPickupOutstation,P.NEW_PICKUP_LOCAL_ as newPickupLocal,P.NEW_PICKUP_OUTSTATION_ as newPickupOutstation,S.NAME_ as serviceName,C.NAME_ as categoryName FROM RL_MD_PACKING_SPECIFICATION P INNER JOIN RL_MD_SERVICE_MASTER S ON (P.SERVICE_ID_= S.ID_) INNER JOIN RL_MD_PRODUCT_CATEGORY C ON (C.ID_= P.CATEGORY_ID_) limit #{firstResult},#{maxResults}")
	List<PackingSpecificationResult> getPackingSpecificationServiceDetails(@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
	
	@Select("select count(*) as totalRecords from RL_MD_PACKING_SPECIFICATION P INNER JOIN RL_MD_SERVICE_MASTER S ON (P.SERVICE_ID_=S.ID_) INNER JOIN RL_MD_PRODUCT_CATEGORY C ON (C.ID_=P.CATEGORY_ID_)")
	ResultCount getPackingSpecificationCount();

}