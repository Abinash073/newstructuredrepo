package com.rlogistics.customqueries;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
@Slf4j

public class ReportingMapperProvider {
    public String getReportingQuery(Map<Object, Object> params) {
        StringBuffer buffer = new StringBuffer("select a.LOCATION_ as location, a.DATE_ as date , a.OPEN_TICKET_ as openedTicket, a.CLOSED_TICKET_ as closedTicket, a.TICKET_CREATED_ as ticketCreated, a.RETAILER_ as retailer  from RL_MD_REPORT a WHERE 1=1");
        if (params.containsKey("location") && !params.get("location").equals("")) {
            buffer.append(" ");
            buffer.append("and a.LOCATION_=#{location}");
        }
        if (params.containsKey("retailer") && !params.get("retailer").equals("")) {
            buffer.append(" ");
            buffer.append("and a.RETAILER_=#{retailer}");
        }
        if (params.containsKey("startDate") && params.containsKey("endDate") && !params.get("startDate").equals("") && !params.get("endDate").equals("")){
            buffer.append(" ");
            buffer.append("and a.DATE_ between #{startDate} and #{endDate}");
        }
        if(params.containsKey("first") && params.containsKey("last") && !params.get("first").equals("") && !params.get("last").equals("")){
            buffer.append(" ");
            buffer.append("limit "+Integer.valueOf(params.get("first").toString())+","+Integer.valueOf(params.get("last").toString()));
        }
       // log.debug(buffer.toString());
        return buffer.toString();
    }
    public String getReportingCountQuery(Map<Object, Object> params) {
        StringBuffer buffer = new StringBuffer("select count(*) from RL_MD_REPORT a WHERE 1=1");
        if (params.containsKey("location") && !params.get("location").equals("")) {
            buffer.append(" ");
            buffer.append("and a.LOCATION_=#{location}");
        }
        if (params.containsKey("retailer") && !params.get("retailer").equals("")) {
            buffer.append(" ");
            buffer.append("and a.RETAILER_=#{retailer}");
        }
        if (params.containsKey("startDate") && params.containsKey("endDate") && !params.get("startDate").equals("") && !params.get("endDate").equals("")){
            buffer.append(" ");
            buffer.append("and a.DATE_ between #{startDate} and #{endDate}");
        }
//        if(params.containsKey("first") && params.containsKey("last") && !params.get("first").equals("") && !params.get("last").equals("")){
//            buffer.append(" ");
//            buffer.append(" limit #{first},#{last}");
//        }
        //log.debug(buffer.toString());
        return buffer.toString();
    }

}
