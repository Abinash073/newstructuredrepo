package com.rlogistics.customqueries;

import com.rlogistics.master.identity.FeDistance;

import java.util.List;

public class ListFeDistanceResult {
	List<FeDistance> data;
	int totalRecords;

	
	public List<FeDistance> getData() {
		return data;
	}
	public void setData(List<FeDistance> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

}
