package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface FeLocationFetchMapper {

    @Select("SELECT table1.fe_name as feId, table1.recent_date as date, LATITUDE_ as latitude, LONGITUDE_ as longitude,table2.FIRST_ as feName FROM ( SELECT FE_NAME_ AS fe_name, MAX(DATE_) as recent_date FROM RL_MD_FE_LOCATION_TRANS WHERE DATE_ LIKE '%'||#{date}||'%' GROUP BY FE_NAME_ ) AS table1, RL_MD_FE_LOCATION_TRANS, RL_MD_USER as table2 where table1.fe_name = table2.EMAIL_ and DATE_ = table1.recent_date AND FE_NAME_ =table1.fe_name AND CITY_ =#{city}")
    List<FeLocationResult> fetchFeLocation(@Param("city") String city,@Param("date")String date);

}
