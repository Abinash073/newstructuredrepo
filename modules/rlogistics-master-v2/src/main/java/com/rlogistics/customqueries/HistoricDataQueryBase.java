package com.rlogistics.customqueries;

import java.util.Map;
import java.util.Set;

public interface HistoricDataQueryBase {

    HistoricDataQueryBase initBaseImpl(Set<String> variable, Map<String, String> param,Map<String, String> PathVariables);
    //To Drop Temp Table if exists
    HistoricDataQueryBase dropTempTableStatement();

    //Create Temp Table  for each variables
    HistoricDataQueryBase createTempTableStatement();

    //Create Index to temp table
    HistoricDataQueryBase createIndexToTempTable();

    //initialise select statement
    HistoricDataQueryBase initSelect();

    HistoricDataQueryBase initFrom();

    HistoricDataQueryBase initWhere();

    HistoricDataQueryBase initLimit();

    HistoricDataQueryBase initCount();

    StringBuffer getQuery();

}
