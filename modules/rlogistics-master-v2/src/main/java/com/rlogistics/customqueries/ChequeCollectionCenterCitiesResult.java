package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerChequeCollectionCenterCityRestData;

public class ChequeCollectionCenterCitiesResult extends RetailerChequeCollectionCenterCityRestData{

	private String cityId;
	private String cityName;
	
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
