package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.FAQRestData;

public class FAQCustomResult extends FAQRestData{
	
	private String FAQ;
	private String categoryName;
	private String subCategoryName;
	private String brandName;
	private String productName;
	
	public String getFAQ() {
		return FAQ;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setFAQ(String fAQ) {
		FAQ = fAQ;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
}
