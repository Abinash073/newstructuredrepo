package com.rlogistics.customqueries;

import java.util.List;

public class FinanceSearchResultSData {
	@Override
	public String toString() {
		return "FinanceSearchResultSData [payTo=" + payTo + ", collectFrom=" + collectFrom + ", retailer=" + retailer
				+ ", modeOfPayment=" + modeOfPayment + ", locationId=" + locationId + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
		result = prime * result + ((retailer == null) ? 0 : retailer.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinanceSearchResultSData other = (FinanceSearchResultSData) obj;
		if (locationId == null) {
			if (other.locationId != null)
				return false;
		} else if (!locationId.equals(other.locationId))
			return false;
		if (retailer == null) {
			if (other.retailer != null)
				return false;
		} else if (!retailer.equals(other.retailer))
			return false;
		return true;
	}
	List<FinanceSearchResults> data;
	int totalRecords;
	float payTo;
	float collectFrom;
	String retailer;
	String modeOfPayment;
	String locationId;
	boolean isSuccess;
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getRetailer() {
		return retailer;
	}
	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}
	public float getPayTo() {
		return payTo;
	}
	public void setPayTo(float payTo) {
		this.payTo = payTo;
	}
	public float getCollectFrom() {
		return collectFrom;
	}
	public void setCollectFrom(float collectFrom) {
		this.collectFrom = collectFrom;
	}
	public List<FinanceSearchResults> getData() {
		return data;
	}
	public void setData(List<FinanceSearchResults> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

}
