package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface RetailerChequeCollectionCenterCustomQueriesMapper {

	@Select("SELECT id_ as cityId, name_ as cityName from RL_MD_CITY where id_ NOT IN (select city_id_ from RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER_CITY where retailer_id_ = #{retailerId})")
	List<ChequeCollectionCenterCitiesResult> findCities(@Param("retailerId") String retailerId);
	
	@Select("SELECT r.id_ as id, r.retailer_id_ as retailerId, r.code_ as code, r.name_ as name, r.area_ as area, r.address_ as address, r.phone_number_ as phoneNumber, r.alternate_phone_number_ as alternatePhoneNumber, r.email_ as email, r.pincode_ as pincode, r.contact_person_ as contactPerson, rc.city_name_ as city, rc.city_id_ as cityId from RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER r INNER JOIN RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER_CITY rc ON (r.id_ = rc.cheque_collection_center_id_) where rc.retailer_id_ = #{retailerId} and rc.city_id_ = #{cityId}")
	List<ChequeCollectionResult> findRetailerCollectionCenters(@Param("retailerId") String retailerId, @Param("cityId") String cityId);
}
