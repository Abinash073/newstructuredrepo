package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.OutstationPackingRestData;

public class OutstationPackingResult extends OutstationPackingRestData {

	private String categoryName;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
