package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.CostingActivityMappingRestData;

public class RetailerCostingActivityMappingResult extends CostingActivityMappingRestData{

	private String costingActivityName;
	private String costingActivityCode;
	private String productCategoryName;
	private String serviceName;
	private String retailerId;
	private String cost;
	private String status;
	private String retailerName;
	private String mappingId;
	private String retailerCostingActivityId;
	
	public String getCostingActivityName() {
		return costingActivityName;
	}
	public void setCostingActivityName(String costingActivityName) {
		this.costingActivityName = costingActivityName;
	}
	public String getCostingActivityCode() {
		return costingActivityCode;
	}
	public void setCostingActivityCode(String costingActivityCode) {
		this.costingActivityCode = costingActivityCode;
	}
	public String getProductCategoryName() {
		return productCategoryName;
	}
	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public String getMappingId() {
		return mappingId;
	}
	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}
	public String getRetailerCostingActivityId() {
		return retailerCostingActivityId;
	}
	public void setRetailerCostingActivityId(String retailerCostingActivityId) {
		this.retailerCostingActivityId = retailerCostingActivityId;
	}
}
