package com.rlogistics.customqueries;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.rlogistics.master.CustomQueriesMapper;
import com.rlogistics.master.identity.FinanceDetails;
import com.rlogistics.master.identity.TrackFeLocation;

@CustomQueriesMapper
public interface ProductCustomQueriesMapper {

	@Select("SELECT p.id_ as id,p.name_ as name,p.code_ as code,p.brand_id_ as brand,b.name_ as brandName from RL_MD_PRODUCT p, RL_MD_BRAND b where p.brand_id_ = b.code_ and p.id_ = #{id}")
	ProductCustomQuery1Result findProduct(@Param("id") String id);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "provideFindProduct2Query")
	ProductCustomQuery1Result findProduct2(@Param("id") String id);

	@Select("SELECT p.id_ as id,p.name_ as name,p.code_ as code,p.brand_ as brand,b.name_ as brandName from RL_MD_PRODUCT p, RL_MD_BRAND b where p.brand_ = b.code_")
	List<ProductCustomQuery1Result> findProduct3();

	@Select("SELECT p.id_ as productId,p.name_ as productName,p.code_ as productCode,p.retailer_id_ as retailerId,p.sub_category_id_ as subCategoryId,p.brand_id_ as brandId, b.name_ as brandName, c.name_ as categoryName, s.name_ as subCategoryName from RL_MD_PRODUCT p, RL_MD_PRODUCT_SUB_CATEGORY s, RL_MD_PRODUCT_CATEGORY c, RL_MD_BRAND b where p.sub_category_id_ = s.id_ and p.brand_id_ = b.id_ and s.product_category_id_ = c.id_ and c.id_ = #{categoryId} and b.id_ = #{brandId} and p.retailer_id_ = #{retailerId}")
	List<ProductCustomQuery1Result> findProductsByCategoryBrand(@Param("categoryId") String categoryId,
			@Param("brandId") String brandId, @Param("retailerId") String retailerId);

	@Select("SELECT p.id_ as id,p.name_ as name,p.code_ as code, p.model_ as model, p.description_ as description, p.sub_category_id_ as subCategoryId,p.brand_id_ as brandId, sc.name_ as subCategoryName, c.id_ as categoryId, c.name_ as categoryName, rp.id_ as retailerAdvReplacementId, rp.retailer_id_ as retailerId from RL_MD_PRODUCT p INNER JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (p.sub_category_id_ = sc.id_) inner join RL_MD_PRODUCT_CATEGORY c ON (sc.product_category_id_ = c.id_) left join RL_MD_RETAILER_ADV_REPLACEMENT_PRODUCTS rp ON (p.id_ = rp.product_id_ and rp.retailer_id_ = #{retailerId})")
	List<ProductCustomQuery2Result> findRetailerAdvanceReplacementProducts(@Param("retailerId") String retailerId);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "provideFindProductListQuery")
	List<ProductListingResult> findProductList(@Param("retailerId") String retailerId, @Param("name") String name,
			@Param("code") String code, @Param("category") String category, @Param("subCategory") String subCategory,
			@Param("brand") String brand, @Param("packagingType") String packagingType,
			@Param("firstResult") String firstResult, @Param("maxResults") String maxResults);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "provideFindProductListQueryCount")
	ResultCount findProductListCount(@Param("retailerId") String retailerId, @Param("name") String name,
			@Param("code") String code, @Param("category") String category, @Param("subCategory") String subCategory,
			@Param("brand") String brand, @Param("packagingType") String packagingType);

	@Select("SELECT s.id_ as id , s.name_ as name, s.code_ as code, s.product_category_id_ as productCategoryId, s.description_ as description, c.id_ as categoryId, c.name_ as categoryName FROM RL_MD_PRODUCT_SUB_CATEGORY s INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (s.product_category_id_ = c.id_) ORDER BY s.name_, s.code_, c.name_ limit #{firstResult},#{maxResults};")
	List<CategoryResult> getSubCategories(@Param("firstResult") int firstResult, @Param("maxResults") int maxResults);

	@Select("SELECT count(*) as totalRecords FROM RL_MD_PRODUCT_SUB_CATEGORY s INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (s.product_category_id_ = c.id_);")
	ResultCount getSubCategoriesCount();

	@Select("select m.id_ as masterCategoryId, m.name_ as masterCategoryName, m.process_flow_ as processFlow from RL_MD_MASTER_CATEGORY m INNER JOIN RL_MD_PRODUCT_CATEGORY p ON (m.id_ = p.master_category_id_) where LOWER(p.name_) = #{productCategory}")
	MasterCategoryResult getMasterCategory(@Param("productCategory") String productCategory);

	// Written For Deleting FEOPen TIcket Status
	@Select("DELETE from RL_MD_FE_OPEN_TICKET_STATUS where LOWER(process_id_) = #{processId}")
	MasterCategoryResult deleteFeOpenTicket(@Param("processId") String processId);

	@Select("select p.id_ as id, p.name_ as name, p.code_ as code, p.master_category_id_ as masterCategoryId, p.description_ as description, m.name_ as masterCategoryName, m.id_ as masterCategoryId from RL_MD_PRODUCT_CATEGORY p INNER JOIN RL_MD_MASTER_CATEGORY m ON (m.id_ = p.master_category_id_) order by p.name_, p.code_ , m.name_ limit #{firstResult},#{maxResults}")
	List<ProductCategoryResult> getCategories(@Param("firstResult") int firstResult,
			@Param("maxResults") int maxResults);

	@Select("select count(*) as totalRecords from RL_MD_PRODUCT_CATEGORY p INNER JOIN RL_MD_MASTER_CATEGORY m ON (m.id_ = p.master_category_id_)")
	ResultCount getCategoriesCount();

	@Select("select proc_inst_id_ as processId from ACT_RU_VARIABLE where NAME_ = #{variable}")
	List<ProcessResult> getProcess(@Param("variable") String variable);

	@Select("select proc_inst_id_ as processId from ACT_RU_VARIABLE where NAME_ = #{variable} and TEXT_ = #{value}")
	List<ProcessResult> getProcessFromValue(@Param("variable") String variable, @Param("value") String value);

	@Select("select proc_inst_id_ as processId from ACT_HI_VARINST where NAME_ = #{variable} and TEXT_ = #{value}")
	List<ProcessResult> getHIProcessFromValue(@Param("variable") String variable, @Param("value") String value);

	@Select("select proc_inst_id_ as processId from ACT_RU_VARIABLE where NAME_ = #{variable} and TEXT_ = #{value}")
	List<ProcessResult> getProcessFromValueRetailer(@Param("variable") String variable, @Param("value") String value);

	@Update("UPDATE ACT_HI_VARINST SET TEXT_ = #{value} where NAME_ = 'rlProductPickUpDocketNo' and PROC_INST_ID_ = #{processId}")
	boolean updateDocketVariable(@Param("value") String value, @Param("processId") String processId);

	@Update("UPDATE ACT_RU_VARIABLE SET TEXT_ = #{value} where NAME_ = 'rlProductPickUpDocketNo' and PROC_INST_ID_ = #{processId}")
	boolean updateDocketVariable2(@Param("value") String value, @Param("processId") String processId);

	@Select("select i.id_ as id, i.retailer_id_ as retailerId, i.product_id_ as productId, p.name_ as productName from RL_MD_RETAILER_PACKING_INTRA_CITY i INNER JOIN RL_MD_PRODUCT p ON (p.id_ = i.product_id_) where i.retailer_id_ = #{retailerId} ORDER BY p.name_ limit #{firstResult},#{maxResults}")
	List<ProductIntraCityResult> getIntraCityProducts(@Param("retailerId") String retailerId,
			@Param("firstResult") int firstResult, @Param("maxResults") int maxResults);

	@Select("select count(*) as totalRecords from RL_MD_RETAILER_PACKING_INTRA_CITY i INNER JOIN RL_MD_PRODUCT p ON (p.id_ = i.product_id_) where i.retailer_id_ = #{retailerId}")
	ResultCount getIntraCityProductsCount(@Param("retailerId") String retailerId);

	@Select("select l.phone_number_ as phoneNumber , l.email_ as email from RL_MD_LOCATION_PINCODES p INNER JOIN RL_MD_LOCATION l ON (l.id_ = p.location_id_) where p.pincode_ = #{pincode}")
	List<LocationResult> getContactDetails(@Param("pincode") String pincode);

	@Select("select l.id_ as id, l.category_id_ as categoryId, l.box_ as box, l.cover_ as cover, l.other_ as other, c.name_ as categoryName from RL_MD_LOCAL_PACKING l INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = l.category_id_) limit #{firstResult},#{maxResults}")
	List<LocalPackingResult> getLocalPackings(@Param("firstResult") int firstResult,
			@Param("maxResults") int maxResults);

	@Select("select count(*) as totalRecords from RL_MD_LOCAL_PACKING l INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = l.category_id_)")
	ResultCount getLocalPackingsCount();

	@Select("select o.id_ as id, o.category_id_ as categoryId, o.box_ as box, o.cover_ as cover, o.other_ as other, c.name_ as categoryName from RL_MD_OUTSTATION_PACKING o INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = o.category_id_) limit #{firstResult},#{maxResults}")
	List<OutstationPackingResult> getOutstationPackings(@Param("firstResult") int firstResult,
			@Param("maxResults") int maxResults);

	@Select("select count(*) as totalRecords from RL_MD_OUTSTATION_PACKING o INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = o.category_id_)")
	ResultCount getOutstationPackingsCount();

	@Select("select pp.id_ as id, pp.product_id_ as productId, pp.retailer_id_ as retailerId, pp.packaging_type_id_ as packagingTypeId, pp.type_ as type, pp.packing_spec_ as packingSpec, p.name_ as productName , pt.name_ as packagingTypeName from RL_MD_PRODUCT_PACKING pp INNER JOIN RL_MD_PRODUCT p ON (pp.product_id_ = p.id_) INNER JOIN RL_MD_PACKAGING_TYPE pt ON (pp.packaging_type_id_ = pt.id_) where pp.retailer_id_ = #{retailerId} limit #{firstResult},#{maxResults}")
	List<ProductPackingResult> getProductPackings(@Param("retailerId") String retailerId,
			@Param("firstResult") int firstResult, @Param("maxResults") int maxResults);

	@Select("select count(*) as totalRecords from RL_MD_PRODUCT_PACKING pp INNER JOIN RL_MD_PRODUCT p ON (pp.product_id_ = p.id_) INNER JOIN RL_MD_PACKAGING_TYPE pt ON (pp.packaging_type_id_ = pt.id_) where pp.retailer_id_ = #{retailerId}")
	ResultCount getProductPackingsCount(@Param("retailerId") String retailerId);

	// Writing the ImeiInventory Query Here For Some Error reason try to make it
	// in diiffrent Page
	@SelectProvider(type = ImeiInventoryCustomQueryProvider.class, method = "provideImeiInventoryQuery")
	List<ImeiInventoryResult> findImeiList(@Param("location") String location, @Param("brand") String brand,
			@Param("imeiNo") String imeiNo, @Param("model") String model, @Param("year") String year,
			@Param("status") String status, @Param("barcode") String barcode, @Param("retailerId") String retailerId,
			@Param("productCategory") String productCategory, @Param("cost1") String cost1,
			@Param("cost2") String cost2, @Param("firstResult") String firstResult,
			@Param("maxResults") String maxResults);

	@SelectProvider(type = ImeiInventoryCustomQueryProvider.class, method = "provideImeiInventoryForExcel")
	List<ImeiInventoryResult> findImeiListForExcel(@Param("location") String location, @Param("brand") String brand,
			@Param("imeiNo") String imeiNo, @Param("model") String model, @Param("year") String year,
			@Param("status") String status, @Param("barcode") String barcode, @Param("retailerId") String retailerId,
			@Param("productCategory") String productCategory, @Param("cost1") String cost1,
			@Param("cost2") String cost2);

	@SelectProvider(type = ImeiInventoryCustomQueryProvider.class, method = "provideImeiInventoryQueryCount")
	ResultCount findImeiInventoryCount(@Param("location") String location, @Param("brand") String brand,
			@Param("imeiNo") String imeiNo, @Param("model") String model, @Param("year") String year,
			@Param("status") String status, @Param("barcode") String barcode,
			@Param("productCategory") String productCategory, @Param("retailerId") String retailerId,
			@Param("cost1") String cost1, @Param("cost2") String cost2);

	@Select("SELECT t.id_ as id,t.ticket_no_ as ticketNo,t.fe_name_ as feName,t.db_name_ as dbName,t.appointment_date_ as appointmentDate,t.current_latitude_ as currentLatitude,t.current_longitude_ as currentLongitude,t.time_to_location_ as timeToLocation,t.priority_ as priority,t.last_assign_ as lastAssign,t.delay_status_ as delayStatus,t.destination_ as destination  from RL_MD_TRACK_FE_LOCATION t where t.fe_name_ = #{feName} and t.appointment_date_ LIKE #{queryDate} limit #{firstResult1},#{maxResults1};")
	List<TrackFeLocResult> likeOperatorForDate(@Param("feName") String feName, @Param("queryDate") String queryDate,
			@Param("firstResult1") int firstResult1, @Param("maxResults1") int maxResults1);

	@Select("SELECT count(*) as totalRecords from RL_MD_TRACK_FE_LOCATION t where t.fe_name_ = #{feName} and t.appointment_date_ LIKE #{queryDate}")
	ResultCount likeOperatorTotalCount(@Param("feName") String feName, @Param("queryDate") String queryDate);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "findAllFeTodayList")
	List<TrackFeLocResult> getTodayFeList(@Param("feCity1") String feCity1,
			@Param("appointmentDate1") String appointmentDate1, @Param("firstResult1") String firstResult1,
			@Param("maxResults1") String maxResults1);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "countFeTodayList")
	ResultCount getCountOfTodayFeList(@Param("feCity1") String feCity1,
			@Param("appointmentDate1") String appointmentDate1);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "searchFinanceDetails")
	List<FinanceSearchResults> searchFinance(@Param("retailerId") String retailerId,
			@Param("locationId") String locationId, @Param("formatStartDate") String formatStartDate,
			@Param("formatEndDate") String formatEndDate, @Param("firstResult1") String firstResult1,
			@Param("maxResults1") String maxResults1);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "countsearchFinance")
	ResultCount getCountOfsearchFinance(@Param("retailerId") String retailerId, @Param("locationId") String locationId,
			@Param("formatStartDate") String formatStartDate, @Param("formatEndDate") String formatEndDate);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "searchFinanceByRetailer")
	List<FinanceSearchResults> searchFinanceDetailsByRetailer(@Param("retailerId") String retailerId,
			@Param("locationId") String locationId, @Param("formatStartDate") String formatStartDate,
			@Param("formatEndDate") String formatEndDate, @Param("firstResult1") String firstResult1,
			@Param("maxResults1") String maxResults1);

	@SelectProvider(type = ProductCustomQueryProvider.class, method = "countsearchFinanceByRetailer")
	ResultCount getCountOfsearchFinanceDetailsByRetailer(@Param("retailerId") String retailerId,
			@Param("locationId") String locationId, @Param("formatStartDate") String formatStartDate,
			@Param("formatEndDate") String formatEndDate);

	// For Exotel Defult city
	// @Select()

	// Abinash Changes
	// Custom Query for updating Assignee and task Created Time
	@Update("UPDATE ACT_RU_TASK SET ASSIGNEE_ = #{value}, CREATE_TIME_ = #{date} where PROC_INST_ID_ = #{processId}")
	boolean updateTicketAssigneeAndCreatedDate(@Param("value") String value, @Param("date") Date date,
			@Param("processId") String processId);
}
