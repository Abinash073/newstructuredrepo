package com.rlogistics.customqueries;

public class DeviceVersionInfoResult {
    private String appVersion;
    private int count;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
