package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.FeatureRestData;

public class FeatureCustomResult extends FeatureRestData{
	
	private String menuName;
	private String menuLink;
	private String featureOrder;
	
	public String getFeatureOrder() {
		return featureOrder;
	}

	public void setFeatureOrder(String featureOrder) {
		this.featureOrder = featureOrder;
	}

	public String getMenuLink() {
		return menuLink;
	}

	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
}
