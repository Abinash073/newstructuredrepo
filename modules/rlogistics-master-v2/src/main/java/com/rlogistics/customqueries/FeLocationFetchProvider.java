package com.rlogistics.customqueries;

import java.util.Map;

public class FeLocationFetchProvider {
    public String getFeLocation(Map<String,String> params){
        StringBuffer buffer =new StringBuffer();
        buffer.append("SELECT table1.fe_name as feId, table1.recent_date as date, LATITUDE_ as latitude, LONGITUDE_ as longitude,table2.FIRST_ as feName\n" +
                "FROM (\n" +
                        "    SELECT FE_NAME_ AS fe_name,\n" +
                        "    MAX(DATE_) as recent_date\n" +
                        "    FROM RL_MD_FE_LOCATION_TRANS\n" +
                        "    GROUP BY FE_NAME_\n" +
                        "    ) AS table1,\n" +
                        "     RL_MD_" +
                        "FE_LOCATION_TRANS,\n" +
                        "     RL_MD_USER as table2\n" +
                        "\n" +
                        "where table1.fe_name = table2.EMAIL_\n" +
                        "and DATE_ = table1.recent_date\n" +
                        "AND FE_NAME_ =table1.fe_name\n" +
                        "AND CITY_ =#{city}");
        return buffer.toString();
    }
}
