package com.rlogistics.customqueries;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class HistoricQueryFilteredData {
    private String processId;
    private String ticketNumber;
    private String ticketCreationDate;
    private Map<String,String> data;
}
