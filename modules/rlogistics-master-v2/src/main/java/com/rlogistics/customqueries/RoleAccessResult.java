package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RoleAccessRestData;

public class RoleAccessResult extends RoleAccessRestData {

	private String menuId;
	private String menuCode;
	private String menuName;
	private String menuOrder;
	private String menuLink;
	private String featureId;
	private String featureCode;
	private String featureName;
	private String featureOrder;
	private String roleAccessId;
	private String featureLink;
	private String parent;
	private String level;
	private String hasSubMenu;
	
//	Newly Created
	private String retailerId;
	private String serviceId;
	
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getFeatureLink() {
		return featureLink;
	}
	public void setFeatureLink(String featureLink) {
		this.featureLink = featureLink;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuOrder() {
		return menuOrder;
	}
	public void setMenuOrder(String menuOrder) {
		this.menuOrder = menuOrder;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public String getFeatureId() {
		return featureId;
	}
	public void setFeatureId(String featureId) {
		this.featureId = featureId;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	public String getFeatureName() {
		return featureName;
	}
	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}
	public String getFeatureOrder() {
		return featureOrder;
	}
	public void setFeatureOrder(String featureOrder) {
		this.featureOrder = featureOrder;
	}
	public String getRoleAccessId() {
		return roleAccessId;
	}
	public void setRoleAccessId(String roleAccessId) {
		this.roleAccessId = roleAccessId;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getHasSubMenu() {
		return hasSubMenu;
	}
	public void setHasSubMenu(String hasSubMenu) {
		this.hasSubMenu = hasSubMenu;
	}
	
	
}
