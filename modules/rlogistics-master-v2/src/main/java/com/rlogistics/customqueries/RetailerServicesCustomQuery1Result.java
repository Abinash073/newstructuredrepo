package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerServicesRestData;

public class RetailerServicesCustomQuery1Result extends RetailerServicesRestData {

	private String serviceId;
	private String serviceName;
	private String serviceCode;
	
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	private String retailerServiceId;
	
	public String getRetailerServiceId() {
		return retailerServiceId;
	}
	public void setRetailerServiceId(String retailerServiceId) {
		this.retailerServiceId = retailerServiceId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
