
package com.rlogistics.customqueries;
//package com.rlogistics.master.identity.FinanceDetails;

public class FinanceSearchResults {
	
	private String id;
	private String processId;
	private String ticketNo;
	private String retailerId;
	private String locationId;
	private String payTo;
	private String collectFrom;
	private String modeOfPayment;
	private String amount;
	private String processFlow;
	private String activitiType;
	private String createdDate;
	private String customerLocation;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getPayTo() {
		return payTo;
	}
	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}
	public String getCollectFrom() {
		return collectFrom;
	}
	public void setCollectFrom(String collectFrom) {
		this.collectFrom = collectFrom;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getProcessFlow() {
		return processFlow;
	}
	public void setProcessFlow(String processFlow) {
		this.processFlow = processFlow;
	}
	public String getActivitiType() {
		return activitiType;
	}
	public void setActivitiType(String activitiType) {
		this.activitiType = activitiType;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerLocation() {
		return customerLocation;
	}
	public void setCustomerLocation(String customerLocation) {
		this.customerLocation = customerLocation;
	}
	
	
	
}
