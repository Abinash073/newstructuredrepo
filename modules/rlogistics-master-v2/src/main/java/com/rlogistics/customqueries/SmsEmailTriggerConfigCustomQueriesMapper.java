package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface SmsEmailTriggerConfigCustomQueriesMapper {

	@Select("SELECT st.id_ as smsEmailTriggerConfigId, t.code_ as smsEmailTriggerCode, s.id_ as serviceId, st.is_email_required_ as isEmailRequired, st.is_sms_required_ as isSmsRequired, s.name_ as serviceName, s.code_ as serviceCode FROM RL_MD_SERVICE_MASTER s join RL_MD_SMS_EMAIL_TRIGGER t on 1=1 left join RL_MD_SMS_EMAIL_TRIGGER_CONFIG st ON s.id_ = st.service_id_ AND t.code_ = st.sms_email_trigger_code_ ORDER BY s.code_, t.code_")
	List<SmsEmailConfigResult2> getConfiguration();
	
	@Select("SELECT st.id_ as id, st.sms_email_trigger_code_ as smsEmailTriggerCode, st.service_id_ as serviceId, st.is_email_required_ as isEmailRequired, st.is_sms_required_ as isSmsRequired, s.name_ as serviceName, s.code_ as serviceCode, r.id_ as retailerSmsEmailTriggerConfigId, r.retailer_id_ as retailerId, r.is_email_required_ as retailerIsEmailRequired, r.is_sms_required_ as retailerIsSmsRequired, r.service_id_ as retailerServiceId, t.description_ as smsEmailTriggerDescription FROM RL_MD_SMS_EMAIL_TRIGGER_CONFIG st INNER JOIN RL_MD_SERVICE_MASTER s ON (st.service_id_ = s.id_) INNER JOIN RL_MD_SMS_EMAIL_TRIGGER t ON (st.sms_email_trigger_code_ = t.code_) LEFT JOIN RL_MD_RETAILER_SMS_EMAIL_TRIGGER_CONFIG r ON (st.sms_email_trigger_code_ = r.sms_email_trigger_code_ and st.service_id_ = r.service_id_ and r.retailer_id_ = #{retailerId}) ORDER BY s.code_, st.sms_email_trigger_code_ ")
	List<SmsEmailConfigResultByRetailer> getConfigurationByRetailer(@Param("retailerId") String retailerId);
	
	@Select("SELECT st.id_ as id, st.retailer_id_ as retailerId, st.sms_email_trigger_code_ as smsEmailTriggerCode, st.email_template_ as emailTemplate, st.sms_template_ as smsTemplate, st.setup_script_ as setupScript, st.params_ as params, s.description_ as smsEmailTriggerDescription FROM RL_MD_RETAILER_SMS_EMAIL_TEMPLATE st INNER JOIN RL_MD_SMS_EMAIL_TRIGGER s ON (st.sms_email_trigger_code_ = s.code_) limit #{firstResult},#{maxResults}")
	List<SmsEmailTemplateResult> listSmsEmailTemplates(@Param("firstResult") int firstResult, @Param("maxResults") int maxResults);
	
	@Select("SELECT count(*) as totalRecords FROM RL_MD_RETAILER_SMS_EMAIL_TEMPLATE st INNER JOIN RL_MD_SMS_EMAIL_TRIGGER s ON (st.sms_email_trigger_code_ = s.code_)")
	ResultCount listSmsEmailTemplatesCount();
}
