package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerSmsEmailTemplateRestData;

public class SmsEmailTemplateResult extends RetailerSmsEmailTemplateRestData{

	private String smsEmailTriggerDescription;

	public String getSmsEmailTriggerDescription() {
		return smsEmailTriggerDescription;
	}

	public void setSmsEmailTriggerDescription(String smsEmailTriggerDescription) {
		this.smsEmailTriggerDescription = smsEmailTriggerDescription;
	}
}
