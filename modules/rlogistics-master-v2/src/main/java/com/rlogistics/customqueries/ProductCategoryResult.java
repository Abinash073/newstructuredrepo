package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductCategoryRestData;

public class ProductCategoryResult extends ProductCategoryRestData {
	
	private String masterCategoryId;
	private String masterCategoryName;
	
	public String getMasterCategoryId() {
		return masterCategoryId;
	}
	public void setMasterCategoryId(String masterCategoryId) {
		this.masterCategoryId = masterCategoryId;
	}
	public String getMasterCategoryName() {
		return masterCategoryName;
	}
	public void setMasterCategoryName(String masterCategoryName) {
		this.masterCategoryName = masterCategoryName;
	}	
}
