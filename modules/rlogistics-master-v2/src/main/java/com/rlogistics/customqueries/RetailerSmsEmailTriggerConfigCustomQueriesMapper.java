package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface RetailerSmsEmailTriggerConfigCustomQueriesMapper {

	@Select("SELECT st.id_ as id, st.sms_email_trigger_code_ as smsEmailTriggerCode, st.retailer_id_ as retailerId, st.service_id_ as serviceId, st.is_email_required_ as isEmailRequired, st.is_sms_required_ as isSmsRequired, s.name_ as serviceName, s.code_ as serviceCode FROM RL_MD_RETAILER_SMS_EMAIL_TRIGGER_CONFIG st INNER JOIN RL_MD_SERVICE_MASTER s ON (st.service_id_ = s.id_) ORDER BY s.code_, st.sms_email_trigger_code_")
	List<SmsEmailConfigResult> getConfiguration();
}
