package com.rlogistics.customqueries;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class HistoricQueryBaseImpl implements HistoricDataQueryBase {
    private Set<String> variable;
    private StringBuffer queryBuffer;
    private Map<String, String> param;
    private Map<String, String> PathVariables;


    public HistoricQueryBaseImpl() {
        queryBuffer = new StringBuffer();
    }


    public HistoricDataQueryBase initBaseImpl(Set<String> variable, Map<String, String> param, Map<String, String> PathVariables) {
        this.param = param;
        this.variable = variable;
        this.PathVariables = PathVariables;

        return this;
    }

    @Override
    public HistoricDataQueryBase dropTempTableStatement() {
        StringBuffer s = new StringBuffer("drop temporary table if exists ");
        variable.forEach(a -> s
                .append(a)
                .append("tempTable")
                .append(","));
        //todo check if last is actually ','
        s.replace(s.length() - 1, s.length(), ";");
        queryBuffer.append(s);
        return this;
    }

    @Override
    public HistoricDataQueryBase createTempTableStatement() {
        queryBuffer.append(variable.stream().map(this::getTempTableStatement).collect(Collectors.joining()));
        return this;
    }

    @Override
    public HistoricDataQueryBase createIndexToTempTable() {
        queryBuffer.append(variable.stream().map(this::getIndexToTempTable).collect(Collectors.joining()));
        return this;
    }

    @Override
    public HistoricDataQueryBase initSelect() {
        String defaultSelectForProcessId = "rlTicketNotempTable.PROC_INST_ID_ as processId,ticketCloserDateTable.END_TIME_ AS ticketCloserDate,";
        StringBuffer buffer = new StringBuffer("SELECT");

        buffer.append(" ");

        buffer.append(defaultSelectForProcessId);
        variable.forEach(s -> buffer
                .append(String.valueOf(s))
                .append("tempTable")
                .append(".")
                .append(String.valueOf(s))
                .append(" ")
                .append("AS")
                .append(" ")
                .append(String.valueOf(s))
                .append(","));
        buffer.deleteCharAt(buffer.length() - 1);
        queryBuffer.append(buffer);
        return this;

    }

    @Override
    public HistoricDataQueryBase initFrom() {
        String defaultF = "rlTicketNotempTable LEFT JOIN ACT_HI_PROCINST AS ticketCloserDateTable ON rlTicketNotempTable.PROC_INST_ID_ = ticketCloserDateTable.PROC_INST_ID_ AND ticketCloserDateTable.END_TIME_ IS NOT NULL AND ticketCloserDateTable.START_TIME_ BETWEEN #{startTime} AND #{endTime}";
        StringBuffer buffer = new StringBuffer(" FROM");
        buffer.append(" ");
        buffer.append(defaultF);

        variable.stream().filter(s -> !s.equals("rlTicketNo")).forEach(v ->
                buffer
                        .append(" ")
                        .append("LEFT JOIN")
                        .append(" ")
                        .append(v)
                        .append("tempTable")
                        .append(" ")
                        .append("ON")
                        .append(" ")
                        .append("rlTicketNotempTable.PROC_INST_ID_")
                        .append("=")
                        .append(v)
                        .append("tempTable.")
                        .append("PROC_INST_ID_")
        );

        queryBuffer.append(buffer);
        return this;
    }

    @Override
    public HistoricDataQueryBase initWhere() {

        //final String defaultVarValue = "#{param} = #{value}";

        StringBuffer b = new StringBuffer(" ");
        b.append("WHERE");
        b.append(" ");
        b.append("1=1 AND");
        b.append(" ");

        param.entrySet()
                .stream()
                .filter(o -> !isEmpty(o.getValue()))
                .forEach(o -> b
                        .append(String.valueOf(o.getKey()))
                        .append(" ")
                        .append(hasWildCard(o.getValue()) ? "LIKE" : "=")
                        .append(" ")
                        .append("'")
                        .append(changeHashToLikeOperator(o.getValue()))
                        .append("'")
                        .append(" ")
                        .append("AND")
                        .append(" ")
                );
        queryBuffer.append(b.delete(b.length() - 4, b.length() - 1));
        return this;


    }

    @Override
    public StringBuffer getQuery() {
        return queryBuffer;
    }


    private String getTempTableStatement(String variable) {
        final String defaultS = "CREATE TEMPORARY TABLE IF NOT EXISTS #{temp} (#{variable} varchar(600), PROC_INST_ID_ int ) SELECT COALESCE(ACT_HI_VARINST.TEXT_, ACT_HI_VARINST.TEXT2_, ACT_HI_VARINST.LONG_, ACT_HI_VARINST.DOUBLE_, '0') AS #{variable}, ACT_HI_VARINST.PROC_INST_ID_ FROM ACT_HI_VARINST WHERE EXISTS( SELECT 1 FROM ACT_HI_PROCINST WHERE ACT_HI_PROCINST.END_TIME_ IS NOT NULL AND ACT_HI_PROCINST.START_TIME_ BETWEEN #{startTime} AND #{endTime} AND ACT_HI_VARINST.PROC_INST_ID_ = ACT_HI_PROCINST.PROC_INST_ID_ ) AND ACT_HI_VARINST.NAME_ = '#{variable}';";
        return defaultS
                .replaceAll("\\#\\{([^}]*temp?)}", variable + "tempTable")
                .replaceAll("\\#\\{([^}]*variable?)}", variable);
    }

    private String getIndexToTempTable(String variable) {
        final String defaultA = "ALTER TABLE `#{temp}` ADD INDEX `#{temp}_idx_id_rlticketno` (`PROC_INST_ID_`, `#{variable}`);";
        return defaultA
                .replaceAll("\\#\\{([^}]*variable?)}", variable)
                .replaceAll("\\#\\{([^}]*temp?)}", variable + "tempTable");
    }

    private static boolean isEmpty(Object object) {
        String a = String.valueOf(object);
        return a == null || a.equals(" ") || a.equals("") || a.equals("null") || a.equals("Null");
    }

    private static StringBuffer changeHashToLikeOperator(Object o) {
        StringBuffer b = new StringBuffer();
        if (String.valueOf(o).contains("#")) {
            b.append("%");
            b.append(String.valueOf(o).replaceAll("[#]", ""));
            b.append("%");
        } else {
            b.append(String.valueOf(o));
        }
        return b;
    }

    private static boolean hasWildCard(Object object) {
        return String.valueOf(object).contains("#");
    }

    @Override
    public HistoricDataQueryBase initLimit() {
        StringBuffer b = new StringBuffer();
        if (!PathVariables.isEmpty() && PathVariables.containsKey("firstResult")) {
            b.append(" ");
            b.append("LIMIT");
            b.append(" ");
            b.append(PathVariables.get("firstResult"));
            b.append(",");
            b.append(PathVariables.get("maxResults"));
        }
        queryBuffer.append(b.toString().trim());
        return this;
    }

    @Override
    public HistoricDataQueryBase initCount() {
        StringBuffer buffer = new StringBuffer("SELECT");

        buffer.append(" ");

        buffer.append("count(rlTicketNotempTable.PROC_INST_ID_) as count ");
        queryBuffer.append(buffer);
        return this;


    }


}
