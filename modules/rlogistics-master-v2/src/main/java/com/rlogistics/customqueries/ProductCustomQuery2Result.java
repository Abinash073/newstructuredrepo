package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductRestData;

public class ProductCustomQuery2Result extends ProductRestData{

	private String subCategoryName;
	private String categoryId;
	private String categoryName;
	private String retailerAdvReplacementId;
	private String retailerId;
	
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getRetailerAdvReplacementId() {
		return retailerAdvReplacementId;
	}
	public void setRetailerAdvReplacementId(String retailerAdvReplacementId) {
		this.retailerAdvReplacementId = retailerAdvReplacementId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
