package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface ReportingMapper {
    //Count of record on specific location for a specific date,specific retailer & specific location
    @SelectProvider(type = ReportingMapperProvider.class, method = "getReportingQuery")
    List<ReportingResult> getReport(@Param(value = "location") String location, @Param(value = "retailer") String retailer, @Param(value = "startDate") String startDate, @Param(value = "endDate") String endDate, @Param(value = "first") String first, @Param(value = "last") String last);

    @SelectProvider(type = ReportingMapperProvider.class, method = "getReportingCountQuery")
    int getReportCount(@Param(value = "location") String location, @Param(value = "retailer") String retailer, @Param(value = "startDate") String startDate, @Param(value = "endDate") String endDate);

}
