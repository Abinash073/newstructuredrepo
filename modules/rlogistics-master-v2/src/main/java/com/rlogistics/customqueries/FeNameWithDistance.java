package com.rlogistics.customqueries;



public class FeNameWithDistance {
    private String feId;
    private double distance;
    private String firstName;
    private String lastName;

    public String getFeId() {
        return feId;
    }

    public void setFeId(String feId) {
        this.feId = feId;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(String distance) {

        this.distance =Double.parseDouble(distance)/1000;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
