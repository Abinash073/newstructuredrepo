package com.rlogistics.customqueries;

public class ReportingResult {
    private String location;
    private String date;
    private String openedTicket;
    private String closedTicket;
    private String ticketCreated;
    private String retailer;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOpenedTicket() {
        return openedTicket;
    }

    public void setOpenedTicket(String openedTicket) {
        this.openedTicket = openedTicket;
    }

    public String getClosedTicket() {
        return closedTicket;
    }

    public void setClosedTicket(String closedTicket) {
        this.closedTicket = closedTicket;
    }

    public String getTicketCreated() {
        return ticketCreated;
    }

    public void setTicketCreated(String ticketCreated) {
        this.ticketCreated = ticketCreated;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }
}
