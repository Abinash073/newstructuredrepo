package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.LspDetailRestData;

public class LspDetailResult extends LspDetailRestData{
	
	private String cityName;
	private String cityId;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	

}
