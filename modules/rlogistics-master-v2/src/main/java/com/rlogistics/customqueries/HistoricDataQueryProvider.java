package com.rlogistics.customqueries;

import java.util.Map;
import java.util.Set;

public class HistoricDataQueryProvider {

    public String historicDataQuery(Map<Object, Object> param) {
        HistoricDataQueryBase queryBase = new HistoricQueryBaseImpl();
        StringBuffer query = queryBase
                .initBaseImpl((Set<String>) param.get("variable"), (Map<String, String>) param.get("param"), (Map<String, String>) param.get("PathVariables"))
                .dropTempTableStatement()
                .createTempTableStatement()
                .createIndexToTempTable()
                .initSelect()
                .initFrom()
                .initWhere()
                .initLimit()
                .getQuery();
        return changeDate(query, String.valueOf(param.get("startTime")), String.valueOf(param.get("endTime")));


    }

    public String changeDate(StringBuffer queryBuffer, String startTime, String endTime) {
        final String ifStartDateNotPresent = "(SELECT MIN(END_TIME_) from ACT_HI_PROCINST)";
        final String ifEndDateNotPresent = "(SELECT MAX(START_TIME_) from ACT_HI_PROCINST)";


        return String.valueOf(queryBuffer)
                .replaceAll("\\#\\{([^}]*startTime?)}", startTime.equals("") ? ifStartDateNotPresent : "'" + startTime + "'")
                .replaceAll("\\#\\{([^}]*endTime?)}", endTime.equals("") ? ifEndDateNotPresent : "'" + endTime + "'")
                .trim();

    }

    public String historicDataQueryCount(Map<Object, Object> param) {
        HistoricDataQueryBase queryBase = new HistoricQueryBaseImpl();
        StringBuffer query = queryBase
                .initBaseImpl((Set<String>) param.get("variable"), (Map<String, String>) param.get("param"), (Map<String, String>) param.get("PathVariables"))
                .dropTempTableStatement()
                .createTempTableStatement()
                .createIndexToTempTable()
                .initCount()
                .initFrom()
                .initWhere()
                .getQuery();
        return changeDate(query, String.valueOf(param.get("startTime")), String.valueOf(param.get("endTime")));
    }
}
