package com.rlogistics.customqueries;

public class DeviceBrandInfoResult {
    private String brand;
    private  int count;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
