package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface MisReportMapper {

    @SelectProvider(type = MisReportProvider.class,method = "getMisReportQuery")
    List<MisResult> getMisReport(@Param(value = "startDate")String startDate, @Param(value = "endDate")String endDate);
}
