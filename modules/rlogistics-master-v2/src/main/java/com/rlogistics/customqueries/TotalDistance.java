package com.rlogistics.customqueries;

import java.math.BigDecimal;

public class TotalDistance {
    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public String getFeName() {
        return feName;
    }

    public void setFeName(String feName) {
        this.feName = feName;
    }

    public String getFeId() {
        return feId;
    }

    public void setFeId(String feId) {
        this.feId = feId;
    }

    private BigDecimal distance;
    private String feName;
    private  String feId;
}
