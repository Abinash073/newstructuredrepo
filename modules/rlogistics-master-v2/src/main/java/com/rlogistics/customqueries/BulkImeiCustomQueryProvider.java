package com.rlogistics.customqueries;

import java.util.Map;
@SuppressWarnings("unused")
public class BulkImeiCustomQueryProvider {
    public String getBulkImeiQueryByDate(Map<Object, Object> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("select IDENTIFICATION_NO_ as imeiNumber,\n" +
                "PROCESS_ID_ AS processId "+
                "from RL_MD_ADDITIONAL_PRODUCT_DETAILS\n" +
                "where RETAILER_ID_ = '#{retailerId}'\n" +
                "  and CREATED_ON_ between '#{startDate}' and '#{endDate}';");
        return changeDateAndparams(buffer.toString(), params);
    }

    private String changeDateAndparams(String s, Map<Object, Object> params) {
        return s.replaceAll("\\#\\{([^}]*startDate?)}", params.get("startDate").toString())
                .replaceAll("\\#\\{([^}]*endDate?)}", params.get("endDate").toString())
                .replaceAll("\\#\\{([^}]*retailerId?)}", params.get("retailerId").toString());
    }

    ;
}
