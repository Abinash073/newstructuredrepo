package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface LspDetailCustomQueriesMapper {
	
	@Select("SELECT L.ID_ as id,L.NAME_ as name,L.CODE_ as code,L.ADDRESS_ as address,L.CONTACT_NUMBER_ as contactNumber,C.NAME_ as cityName FROM RL_MD_LSP_DETAIL L INNER JOIN RL_MD_CITY C ON (L.CITY_ID_=C.ID_) limit #{firstResult},#{maxResults}")
	List<LspDetailResult> getLspDetailServiceDetails(@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
    
	@Select("select count(*) as totalRecords from RL_MD_LSP_DETAIL L INNER JOIN RL_MD_CITY C ON (L.CITY_ID_=C.ID_)")
	ResultCount getLspCount();
}
