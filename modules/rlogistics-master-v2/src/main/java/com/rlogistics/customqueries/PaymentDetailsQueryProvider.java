package com.rlogistics.customqueries;

import java.util.Map;

public class PaymentDetailsQueryProvider {
    public String getPaymentDetailsByDate(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("select RL_MD_PAYMENT.TICKET_NO_     as ticketNo,\n" +
                "       RL_MD_PAYMENT.AMOUNT_        as amount,\n" +
                "       RL_MD_PAYMENT_DETAILS.PHONE_ as phone,\n" +
                "       RL_MD_PAYMENT.STATUS_        as status,\n" +
                "       RL_MD_PAYMENT.PAYMENT_TYPE_  as paymentType,\n" +
                "       RL_MD_RETAILER.NAME_         as retailerName,\n" +
                "       RL_MD_PAYMENT_DETAILS.NAME_  as customerName,\n" +
                "       RL_MD_PAYMENT.CREATED_ON_    as createdOn,\n" +
                "       RL_MD_PAYMENT.UPDATE_ON_     as updatedOn,\n" +
                "       RL_MD_PAYMENT.LOCATION_      as location\n" +
                "\n" +
                "from RL_MD_PAYMENT_DETAILS,\n" +
                "     RL_MD_PAYMENT\n" +
                "       inner join RL_MD_RETAILER on RL_MD_PAYMENT.RETAILER_ID_ = RL_MD_RETAILER.ID_\n" +
                "where RL_MD_PAYMENT.TICKET_NO_ = RL_MD_PAYMENT_DETAILS.TICKET_NO_\n" +
                "  and RL_MD_PAYMENT.CREATED_ON_ between '#{startDate}' and '#{endDate}'");

        if (params.containsKey("retailerName") && !params.get("retailerName").equals(""))
            buffer.append(" and RL_MD_PAYMENT.RETAILER_ID_ =").append("'").append(params.get("retailerName")).append("'");


        if (params.containsKey("ticketNo") && !params.get("ticketNo").equals(""))
            buffer.append(" and RL_MD_PAYMENT.TICKET_NO_=").append("'").append(params.get("ticketNo")).append("'");


        return replaceDate(buffer, params);


    }

    private String replaceDate(StringBuffer s, Map<String, String> params) {
        return s.toString()
                .replaceAll("\\#\\{([^}]*startDate?)}", params.get("startDate"))
                .replaceAll("\\#\\{([^}]*endDate?)}", params.get("endDate"));
    }
}
