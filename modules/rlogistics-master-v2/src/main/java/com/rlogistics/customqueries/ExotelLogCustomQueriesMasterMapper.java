package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface ExotelLogCustomQueriesMasterMapper {


    @SelectProvider(type = ExotelLogCustomQueryProvider.class, method = "provideExotelLogQuery")
    List<ExotelLogResult> getExotelLog(@Param("processId") String processId, @Param("customerNumber") String customerNumber, @Param("assignee") String assignee, @Param("exotelNumber") String exotelNumber, @Param("date") String date, @Param("ticketNumber")String ticketNumber, @Param("city")String city, @Param("callStatus")String callStatus,@Param("first")String firstResult,@Param("last")String maxResults,@Param("firstDate")String firstDate,@Param("lastDate")String lastDate);

    @SelectProvider(type = ExotelLogCustomQueryProvider.class, method = "provideExotelLogQueryCount")
    ResultCount getExotelLogCount(@Param("processId") String processId, @Param("customerNumber") String customerNumber, @Param("assignee") String assignee, @Param("exotelNumber") String exotelNumber, @Param("date") String date, @Param("ticketNumber") String ticketNumber, @Param("city") String city, @Param("callStatus") String callStatus,@Param("firstDate")String firstDate,@Param("lastDate")String lastDate);


}
