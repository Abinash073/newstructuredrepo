package com.rlogistics.customqueries;

import java.util.Map;

public class ServiceProviderLocationCustomQueryProvider {

	public static String provideFindServiceLocationsQuery(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select s.id_ as id, s.service_provider_id_ as serviceProviderId,s.service_provider_code_ as serviceProviderCode, p.name_ as serviceProviderName, s.address1_ as address1, s.address2_ as address2, s.city_ as city, s.city_id_ as cityId, s.phone1_ as phone1, s.phone2_ as phone2, s.email_ as email, s.contact_person_ as contactPerson, s.area_ as area, sl.is_bizlog_authorized_ as isBizlogAuthorized, sl.is_authorized_service_provider_ as isAuthorizedServiceProvider, sl.kind_of_service_ as kindOfService, sp.id_ as retailerServiceProviderPincodeId, sp.pincode_ as pincode from RL_MD_SERVICE_PROVIDER_LOCATION s INNER JOIN RL_MD_RETAILER_SERVICE_PROVIDER r ON(s.id_ = r.service_provider_location_id_) INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE sl ON(s.id_ = sl.service_provider_location_id_) INNER JOIN RL_MD_SERVICE_PROVIDER p ON(p.id_ = s.service_provider_id_) LEFT JOIN RL_MD_RETAILER_SERVICE_PROVIDER_PINCODE sp ON (r.id_ = sp.retailer_service_provider_id_) where r.retailer_Id_ = #{retailerId}");
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and r.category_id_ = #{categoryId}");
		}
		
		if(params.containsKey("brandId") && !params.get("brandId").equals("")) {
			buffer.append(" ");
			buffer.append("and r.brand_id_ = #{brandId}");
		} else {
			buffer.append(" ");
			buffer.append("and r.brand_id_ IS NULL");
		}
		
		if(params.containsKey("subCategoryId") && !params.get("subCategoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and r.sub_category_id_ = #{subCategoryId}");
		}
		
		if(params.containsKey("pincode") && !params.get("pincode").equals("")) {
			buffer.append(" ");
			buffer.append("and sp.pincode_ = #{pincode}");
		} else {
			buffer.append(" ");
			buffer.append("and sp.pincode_ IS NULL");
		}
		
		if(params.containsKey("cityId") && !params.get("cityId").equals("")) {
			buffer.append(" ");
			if(params.get("cityId").equals("10351")) {
				params.put("cityId", "5061");
			}
			buffer.append("and r.city_id_ = #{cityId}");
		}
		
		if(params.containsKey("isAuthorizedServiceProvider") && !params.get("isAuthorizedServiceProvider").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(sl.is_authorized_service_provider_) = " + "'" +params.get("isAuthorizedServiceProvider").toLowerCase() + "'");
		}
		
		return buffer.toString();
			
	}
	
public static String provideFindServiceLocationsQueryBizlog(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select s.id_ as id, s.service_provider_id_ as serviceProviderId,s.service_provider_code_ as serviceProviderCode,p.name_ as serviceProviderName,s.address1_ as address1, s.address2_ as address2, s.city_ as city, s.city_id_ as cityId, s.phone1_ as phone1, s.phone2_ as phone2, s.email_ as email, s.contact_person_ as contactPerson, s.pincode_ as pincode, s.area_ as area, sl.is_bizlog_authorized_ as isBizlogAuthorized, sl.is_authorized_service_provider_ as isAuthorizedServiceProvider, sl.kind_of_service_ as kindOfService from RL_MD_SERVICE_PROVIDER_LOCATION s , RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE sl, RL_MD_SERVICE_PROVIDER p where s.id_ = sl.service_provider_location_id_ and p.id_ = s.service_provider_id_");
		
		if(params.containsKey("isBizlogAuthorized") && !params.get("isBizlogAuthorized").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(sl.is_bizlog_authorized_) = " + "'" +params.get("isBizlogAuthorized").toLowerCase() + "'");
		}
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and sl.category_id_ = #{categoryId}");
		}
		
		if(params.containsKey("brandId") && !params.get("brandId").equals("")) {
			buffer.append(" ");
			buffer.append("and sl.brand_id_ = #{brandId}");
		} else {
			buffer.append(" ");
			buffer.append("and sl.brand_id_ IS NULL");
		}
		
		if(params.containsKey("subCategoryId") && !params.get("subCategoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and sl.sub_category_id_ = #{subCategoryId}");
		}
		
		if(params.containsKey("cityId") && !params.get("cityId").equals("")) {
			buffer.append(" ");
			buffer.append("and s.city_id_ = #{cityId}");
		}
		
		if(params.containsKey("isAuthorizedServiceProvider") && !params.get("isAuthorizedServiceProvider").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(sl.is_authorized_service_provider_) = " + "'" +params.get("isAuthorizedServiceProvider").toLowerCase() + "'");
		}
		
		return buffer.toString();
			
	}

public static String provideFindServiceProviderQuery(Map<String,String> params) {
	
	StringBuffer buffer=  new StringBuffer();
	
	buffer.append("select sl.id_ as serviceProviderLocationId, sl.city_ as city, sl.city_id_ as cityId, sl.email_ as email, sl.pincode_ as pincode, sl.area_ as area, sl.service_provider_code_ as serviceProviderCode, sl.service_provider_id_ as serviceProviderId, sl.contact_person_ as contactPerson, s.name_ as serviceProviderName, l.category_id_ as categoryId, c.name_ as categoryName from RL_MD_SERVICE_PROVIDER_LOCATION sl INNER JOIN RL_MD_SERVICE_PROVIDER s ON (s.id_ = sl.service_provider_id_) INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE l ON (sl.id_ = l.service_provider_location_id_) INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = l.category_id_)");
	
	if(params.containsKey("serviceProviderId") && !params.get("serviceProviderId").equals("")) {
		buffer.append(" ");
		buffer.append("where sl.service_provider_id_ = #{serviceProviderId}");
	}
	buffer.append(" ");
	buffer.append("order by s.name_, sl.area_, sl.city_, sl.pincode_ limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
	
	
	return buffer.toString();
		
}

public static String provideFindServiceProviderQueryCount(Map<String,String> params) {
	
	StringBuffer buffer=  new StringBuffer();
	
	buffer.append("select count(*) as totalRecords from RL_MD_SERVICE_PROVIDER_LOCATION sl INNER JOIN RL_MD_SERVICE_PROVIDER s ON (s.id_ = sl.service_provider_id_)");
	
	if(params.containsKey("serviceProviderId") && !params.get("serviceProviderId").equals("")) {
		buffer.append(" ");
		buffer.append("where sl.service_provider_id_ = #{serviceProviderId}");
	}
	
	return buffer.toString();
		
}
}
