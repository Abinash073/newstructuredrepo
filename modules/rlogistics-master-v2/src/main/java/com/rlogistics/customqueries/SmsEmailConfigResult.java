package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.SmsEmailTriggerConfigRestData;

public class SmsEmailConfigResult extends SmsEmailTriggerConfigRestData{

	private String serviceName;
	private String serviceCode;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
}
