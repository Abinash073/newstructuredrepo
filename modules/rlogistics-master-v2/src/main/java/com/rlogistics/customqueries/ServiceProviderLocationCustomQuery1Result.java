package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ServiceProviderLocationRestData;

public class ServiceProviderLocationCustomQuery1Result extends ServiceProviderLocationRestData {
	
	private String serviceProviderName;
	private String isBizlogAuthorized;
	private String isAuthorizedServiceProvider;
	private String kindOfService;

	public String getKindOfService() {
		return kindOfService;
	}

	public void setKindOfService(String kindOfService) {
		this.kindOfService = kindOfService;
	}

	public String getIsBizlogAuthorized() {
		return isBizlogAuthorized;
	}

	public void setIsBizlogAuthorized(String isBizlogAuthorized) {
		this.isBizlogAuthorized = isBizlogAuthorized;
	}

	public String getIsAuthorizedServiceProvider() {
		return isAuthorizedServiceProvider;
	}

	public void setIsAuthorizedServiceProvider(String isAuthorizedServiceProvider) {
		this.isAuthorizedServiceProvider = isAuthorizedServiceProvider;
	}

	public String getServiceProviderName() {
		return serviceProviderName;
	}

	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}
}
