package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.SmsEmailTriggerConfigRestData;

public class SmsEmailConfigResultByRetailer extends SmsEmailTriggerConfigRestData {

	private String serviceName;
	private String serviceCode;
	private String retailerSmsEmailTriggerConfigId;
	private String retailerId;
	private String retailerServiceId;
	private int retailerIsEmailRequired;
	private int retailerIsSmsRequired;
	private String smsEmailTriggerDescription;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getRetailerSmsEmailTriggerConfigId() {
		return retailerSmsEmailTriggerConfigId;
	}
	public void setRetailerSmsEmailTriggerConfigId(String retailerSmsEmailTriggerConfigId) {
		this.retailerSmsEmailTriggerConfigId = retailerSmsEmailTriggerConfigId;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getRetailerServiceId() {
		return retailerServiceId;
	}
	public void setRetailerServiceId(String retailerServiceId) {
		this.retailerServiceId = retailerServiceId;
	}
	public int getRetailerIsEmailRequired() {
		return retailerIsEmailRequired;
	}
	public void setRetailerIsEmailRequired(int retailerIsEmailRequired) {
		this.retailerIsEmailRequired = retailerIsEmailRequired;
	}
	public int getRetailerIsSmsRequired() {
		return retailerIsSmsRequired;
	}
	public void setRetailerIsSmsRequired(int retailerIsSmsRequired) {
		this.retailerIsSmsRequired = retailerIsSmsRequired;
	}
	public String getSmsEmailTriggerDescription() {
		return smsEmailTriggerDescription;
	}
	public void setSmsEmailTriggerDescription(String smsEmailTriggerDescription) {
		this.smsEmailTriggerDescription = smsEmailTriggerDescription;
	}	
}
