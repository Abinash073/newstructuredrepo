package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import com.rlogistics.master.identity.FeLocation;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface FeLocationCustomQueryMapper {
    @SelectProvider(type = FeLocationCustomQueryProvider.class,method = "getFeLocation")
    List<FeLocationResult> getFeLocation(@Param("date")String date,@Param("feName") String feName,@Param("ticket")String ticket);
}
