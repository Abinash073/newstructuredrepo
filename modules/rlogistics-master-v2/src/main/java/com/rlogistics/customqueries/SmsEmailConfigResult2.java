package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.SmsEmailTriggerConfigRestData;

public class SmsEmailConfigResult2 extends SmsEmailTriggerConfigRestData {

	private String serviceId;
	private String serviceName;
	private String serviceCode;
	private String smsEmailTriggerConfigId;
	private String smsEmailTriggerCode;
	
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getSmsEmailTriggerConfigId() {
		return smsEmailTriggerConfigId;
	}
	public void setSmsEmailTriggerConfigId(String smsEmailTriggerConfigId) {
		this.smsEmailTriggerConfigId = smsEmailTriggerConfigId;
	}
	public String getSmsEmailTriggerCode() {
		return smsEmailTriggerCode;
	}
	public void setSmsEmailTriggerCode(String smsEmailTriggerCode) {
		this.smsEmailTriggerCode = smsEmailTriggerCode;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
}
