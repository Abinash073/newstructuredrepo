package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerPackingIntraCityRestData;

public class ProductIntraCityResult extends RetailerPackingIntraCityRestData{

	private String productName;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
}
