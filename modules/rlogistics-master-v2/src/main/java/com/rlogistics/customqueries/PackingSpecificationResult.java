package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.PackingSpecificationRestData;

public class PackingSpecificationResult extends PackingSpecificationRestData{
	
	private String categoryName;
	private String serviceName;
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
