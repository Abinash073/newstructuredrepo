package com.rlogistics.customqueries;

import java.util.Map;

public class ProductCustomQueryProvider {
	public static String provideFindProduct2Query(Map params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		if(Character.isUpperCase('C')){
			buffer.append("SELECT p.ID_ AS id,p.NAME_ AS name,p.CODE_ AS code,p.BRAND_ AS brand,b.NAME_ AS brandName");
		}
		
		if(3 < 5){
			buffer.append(" ");
			buffer.append("FROM RL_MD_PRODUCT p, RL_MD_BRAND b");
		}
		
		if(9 > 8){
			buffer.append(" ");
			buffer.append("WHERE p.BRAND_ = b.CODE_ AND p.ID_ = #{id}");
		}
		
		return buffer.toString();
	}
	
	public static String provideFindProductListQuery(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT p.id_ as id,p.name_ as name,p.code_ as code,p.model_ as model,p.sub_category_id_ as subCategoryId,p.brand_id_ as brandId, p.retailer_id_ as retailerId, p.packaging_type_box_id_ as packagingTypeBoxId, p.packaging_type_cover_id_ as packagingTypeCoverId, b.name_ as brandName, c.name_ as categoryName, s.name_ as subCategoryName, s.product_category_id_ as categoryId, pt.name_ as packagingTypeBox, pt2.name_ as packagingTypeCover from RL_MD_PRODUCT p INNER JOIN RL_MD_PRODUCT_SUB_CATEGORY s ON(p.sub_category_id_ = s.id_) INNER JOIN RL_MD_PRODUCT_CATEGORY c ON(s.product_category_id_ = c.id_) INNER JOIN RL_MD_BRAND b ON(p.brand_id_ = b.id_) INNER JOIN RL_MD_PACKAGING_TYPE pt ON(p.packaging_type_box_id_ = pt.id_) LEFT JOIN RL_MD_PACKAGING_TYPE pt2 ON(p.packaging_type_cover_id_ = pt2.id_) where p.retailer_id_ = #{retailerId}");
		
		if(params.containsKey("name") && !params.get("name").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(p.name_) LIKE " + "'" + params.get("name") + "%'");
		}
		if(params.containsKey("code") && !params.get("code").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(p.code_) LIKE " + "'" + params.get("code") + "%'");
		}
		if(params.containsKey("category") && !params.get("category").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{category}");
		}
		if(params.containsKey("subCategory") && !params.get("subCategory").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{subCategory}");
		}
		if(params.containsKey("brand") && !params.get("brand").equals("")) {
			buffer.append(" ");
			buffer.append("and b.id_ = #{brand}");
		}
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and (pt.id_ = #{packagingType} or pt2.id_ = #{packagingType})");
		}
		
		buffer.append(" ");
		buffer.append("limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
		
		return buffer.toString();
	}
	
	public static String provideFindProductListQueryCount(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords from RL_MD_PRODUCT p INNER JOIN RL_MD_PRODUCT_SUB_CATEGORY s ON(p.sub_category_id_ = s.id_) INNER JOIN RL_MD_PRODUCT_CATEGORY c ON(s.product_category_id_ = c.id_) INNER JOIN RL_MD_BRAND b ON(p.brand_id_ = b.id_) INNER JOIN RL_MD_PACKAGING_TYPE pt ON(p.packaging_type_box_id_ = pt.id_) LEFT JOIN RL_MD_PACKAGING_TYPE pt2 ON(p.packaging_type_cover_id_ = pt2.id_) where p.retailer_id_ = #{retailerId}");
		
		if(params.containsKey("name") && !params.get("name").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(p.name_) LIKE " + "'" + params.get("name") + "%'");
		}
		if(params.containsKey("code") && !params.get("code").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(p.code_) LIKE " + "'" + params.get("code") + "%'");
		}
		if(params.containsKey("category") && !params.get("category").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{category}");
		}
		if(params.containsKey("subCategory") && !params.get("subCategory").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{subCategory}");
		}
		if(params.containsKey("brand") && !params.get("brand").equals("")) {
			buffer.append(" ");
			buffer.append("and b.id_ = #{brand}");
		}
		if(params.containsKey("packagingType") && !params.get("packagingType").equals("")) {
			buffer.append(" ");
			buffer.append("and (pt.id_ = #{packagingType} or pt2.id_ = #{packagingType})");
		}
		
		return buffer.toString();
	}
	
	public static String findAllFeTodayList(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT t.id_ as id,t.ticket_no_ as ticketNo,t.fe_name_ as feName,t.db_name_ as dbName,t.appointment_date_ as appointmentDate,t.current_latitude_ as currentLatitude,t.current_longitude_ as currentLongitude,t.time_to_location_ as timeToLocation,t.priority_ as priority,t.last_assign_ as lastAssign,t.delay_status_ as delayStatus,t.destination_ as destination,t.distance_ as distance,t.fe_city_ as feCity from RL_MD_TRACK_FE_LOCATION t where 1=1");

		if(params.containsKey("feCity1") && !params.get("feCity1").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(t.fe_city_) LIKE " + "'" + params.get("feCity1") + "%'");
		}
		if(params.containsKey("appointmentDate1") && !params.get("appointmentDate1").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(t.appointment_date_) LIKE " + "'" + params.get("appointmentDate1") + "%'");
		}
		buffer.append(" ");
		buffer.append("limit " + Integer.valueOf(params.get("firstResult1")) + "," + Integer.valueOf(params.get("maxResults1")));
		
		return buffer.toString();
	}
	
	public static String countFeTodayList(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords from RL_MD_TRACK_FE_LOCATION t where 1=1");

		if(params.containsKey("feCity1") && !params.get("feCity1").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(t.fe_city_) LIKE " + "'" + params.get("feCity1") + "%'");
		}
		if(params.containsKey("appointmentDate1") && !params.get("appointmentDate1").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(t.appointment_date_) LIKE " + "'" + params.get("appointmentDate1") + "%'");
		}
		
		return buffer.toString();
	}
	
	public static String searchFinanceDetails(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
//		buffer.append("SELECT f.id_ as id,f.process_id_ as processId,f.ticket_no_ as ticketNo,f.retailer_id_ as retailerId,f.location_id_ as locationId,f.pay_to_ as payTO,f.collect_from_ as collectFrom,f.mode_of_payment_ as modeOfPayment,f.amount_ as amount,f.process_flow_ as processFlow,f.activiti_type_ as activitiType,f.created_date_ as createdDate,f.customer_location_ as customerLocation from RL_MD_FINANCE_DETAILS f where 1=1");
buffer.append(" Select f.retailer_id_ as retailerId,f.customer_location_ as customerLocation,sum(f.amount_)as collectFrom from RL_MD_FINANCE_DETAILS f where 1=1 and f.pay_to_ like 'Bizlog'");
		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" ");
			
			buffer.append("and f.retailer_id_ LIKE " + "'" + params.get("retailerId") + "%'");
		}
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and f.customer_location_ LIKE " + "'%" + params.get("locationId") + "%'");
		}
		if(params.containsKey("formatStartDate") && !params.get("formatStartDate").equals("") && params.containsKey("formatEndDate") && !params.get("formatEndDate").equals("")) {
			buffer.append(" ");
			buffer.append("and f.created_date_ BETWEEN " + "'" + params.get("formatStartDate") + "'"+" AND '" + params.get("formatEndDate")+"'");
		}
		buffer.append("group by f.customer_location_,f.retailer_id_");
//		buffer.append(" ");
//		buffer.append("limit " + Integer.valueOf(params.get("firstResult1")) + "," + Integer.valueOf(params.get("maxResults1")));
		
		return buffer.toString();
	}
	public static String countsearchFinance(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords from RL_MD_FINANCE_DETAILS f where 1=1");

		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" ");
			buffer.append("and f.retailer_id_ LIKE " + "'%" + params.get("retailerId") + "%'");
		}
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" ");
			buffer.append("and f.customer_location_ LIKE " + "'%" + params.get("locationId") + "%'");
		}
		if(params.containsKey("formatStartDate") && !params.get("formatStartDate").equals("") && params.containsKey("formatEndDate") && !params.get("formatEndDate").equals("")) {
			buffer.append(" ");
			buffer.append("and f.created_date_ BETWEEN " + "'" + params.get("formatStartDate") + "'"+" AND '" + params.get("formatEndDate")+"'");
		}
		
		return buffer.toString();
	}
	
	//THIS IS USED TO FETCH ALL THE INFORMATION ON THE BASIS OF RETAILER AND LOCATION COMBINATION
	public static String searchFinanceByRetailer(Map<String,String> params){

		StringBuffer buffer=  new StringBuffer();

		buffer.append("SELECT f.id_ as id,f.process_id_ as processId,f.ticket_no_ as ticketNo,f.retailer_id_ as retailerId,"+
		"f.location_id_ as locationId,f.pay_to_ as payTO,f.collect_from_ as collectFrom,f.mode_of_payment_ as modeOfPayment,"+
		"f.amount_ as amount,f.process_flow_ as processFlow,f.activiti_type_ as activitiType,"+
		"f.created_date_ as createdDate,f.customer_location_ as customerLocation from RL_MD_FINANCE_DETAILS f "+
		"where f.pay_to_ like 'Bizlog'and f.collect_from_ like 'Retailer'");
		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
		buffer.append(" and f.retailer_id_ LIKE " + "'" + params.get("retailerId") + "%'");
		}
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
		buffer.append(" and f.customer_location_ LIKE " + "'%" + params.get("locationId") + "%'");
		}
		if(params.containsKey("formatStartDate") && !params.get("formatStartDate").equals("") && params.containsKey("formatEndDate") && !params.get("formatEndDate").equals("")) {

		buffer.append(" and f.created_date_ BETWEEN " + "'" + params.get("formatStartDate") + "'"+" AND '" + params.get("formatEndDate")+"'");
		}
//			buffer.append(" ");
//			buffer.append("limit " + Integer.valueOf(params.get("firstResult1")) + "," + Integer.valueOf(params.get("maxResults1")));

		return buffer.toString();
		}
	
	public static String countsearchFinanceByRetailer(Map<String,String> params){
		/* Use params in any way and build a query string. In this example we just ignore the params */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords from RL_MD_FINANCE_DETAILS f where 1=1");

		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" and f.retailer_id_ LIKE " + "'%" + params.get("retailerId") + "%'");
		}
		if(params.containsKey("locationId") && !params.get("locationId").equals("")) {
			buffer.append(" and f.customer_location_ LIKE " + "'%" + params.get("locationId") + "%'");
		}
		if(params.containsKey("formatStartDate") && !params.get("formatStartDate").equals("") && params.containsKey("formatEndDate") && !params.get("formatEndDate").equals("")) {
			buffer.append(" and f.created_date_ BETWEEN " + "'" + params.get("formatStartDate") + "'"+" AND '" + params.get("formatEndDate")+"'");
		}
		
		return buffer.toString();
	}
	
}
