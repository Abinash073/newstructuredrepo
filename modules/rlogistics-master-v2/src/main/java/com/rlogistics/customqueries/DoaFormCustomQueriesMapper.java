package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface DoaFormCustomQueriesMapper {
	
	@Select("select d.id_ as id, d.attachment_id_ as attachmentId, d.category_id_ as categoryId, d.brand_id_ as brandId, c.name_ as categoryName, b.name_ as brandName from RL_MD_DOA_FORM d INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (d.category_id_ = c.id_) INNER JOIN RL_MD_BRAND b ON (d.brand_id_ = b.id_) limit #{firstResult},#{maxResults}")
	List<DOAResult> listDoaForms(@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
	
	@Select("select count(*) as totalRecords from RL_MD_DOA_FORM d INNER JOIN RL_MD_PRODUCT_CATEGORY c ON (d.category_id_ = c.id_) INNER JOIN RL_MD_BRAND b ON (d.brand_id_ = b.id_)")
	ResultCount listDoaFormsCount();
}
