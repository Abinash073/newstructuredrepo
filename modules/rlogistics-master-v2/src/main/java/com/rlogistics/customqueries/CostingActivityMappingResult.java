package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.CostingActivityMappingRestData;

public class CostingActivityMappingResult extends CostingActivityMappingRestData{

	private String costingActivityName;
	private String costingActivityCode;
	private String productCategoryName;
	private String serviceName;
	
	public String getCostingActivityName() {
		return costingActivityName;
	}
	public void setCostingActivityName(String costingActivityName) {
		this.costingActivityName = costingActivityName;
	}
	public String getCostingActivityCode() {
		return costingActivityCode;
	}
	public void setCostingActivityCode(String costingActivityCode) {
		this.costingActivityCode = costingActivityCode;
	}
	public String getProductCategoryName() {
		return productCategoryName;
	}
	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
}
