package com.rlogistics.customqueries;

import java.util.ArrayList;
import java.util.Map;

public class RoleAccessCustomQueryProvider {

	public static String provideGetRoleAccessQuery(Map<String,ArrayList<String>> params) {
		
		ArrayList<String> orderBy = params.get("orderBy");
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT m.id_ as menuId, m.code_ as menuCode, m.name_ as menuName, m.order_ as menuOrder, m.link_ as menuLink, f.id_ as featureId, f.code_ as featureCode, f.name_ as featureName, f.order_ as featureOrder, f.link_ as featureLink, f.level_ as level, f.parent_ as parent, f.has_sub_menu_ as hasSubMenu, r.id_ as roleAccessId, r.role_code_ as roleCode, r.access_ as access FROM RL_MD_MENU m INNER JOIN RL_MD_FEATURE f ON (m.id_ = f.menu_id_) LEFT JOIN RL_MD_ROLE_ACCESS r ON (f.id_ = r.feature_id_ and LOWER(r.role_code_) = #{roleCode}) ORDER BY");
	
		if(orderBy.size()>0) {
			if(orderBy.contains("order")) {
				
				buffer.append(" ");
				buffer.append("m.order_, f.order_");
			}
			
			if(orderBy.contains("name")) {
				if(orderBy.contains("order")) {
					buffer.append(", ");
					buffer.append("m.name_, f.name_");
				} else {
					buffer.append(" ");
					buffer.append("m.name_, f.name_");
				}
				
			}
			
		} else {
		
			buffer.append(" ");
			buffer.append("m.order_, f.order_, m.name_, f.name_");
		}
		return buffer.toString();
			
	}
	
	//Newly Created For Specific Retailer Setting
public static String provideGetRetailerRoleAccessQuery(Map<String,ArrayList<String>> params) {
		
		ArrayList<String> orderBy = params.get("orderBy");
		
		StringBuffer buffer=  new StringBuffer();
				
//		Newly Created
		buffer.append("SELECT m.id_ as menuId, m.code_ as menuCode, m.name_ as menuName, m.order_ as menuOrder, m.link_ as menuLink, f.id_ as featureId, f.code_ as featureCode, f.name_ as featureName, f.order_ as featureOrder, f.link_ as featureLink, f.level_ as level, f.parent_ as parent, f.has_sub_menu_ as hasSubMenu, r.id_ as roleAccessId, r.role_code_ as roleCode, r.access_ as access,rs.retailer_id_ as retailerId,rs.service_id_ as serviceId FROM RL_MD_MENU m INNER JOIN RL_MD_FEATURE f ON (m.id_ = f.menu_id_) LEFT JOIN RL_MD_ROLE_ACCESS r ON (f.id_ = r.feature_id_ and LOWER(r.role_code_) = #{roleId}) LEFT JOIN RL_MD_RETAILER_SERVICES rs ON (rs.retailer_id_ = #{roleCode} and rs.service_id_ = 'ee949081-d4d0-11e7-ac0e-0a3e0a2127a0') ORDER BY");

		if(orderBy.size()>0) {
			if(orderBy.contains("order")) {
				
				buffer.append(" ");
				buffer.append("m.order_, f.order_");
			}
			
			if(orderBy.contains("name")) {
				if(orderBy.contains("order")) {
					buffer.append(", ");
					buffer.append("m.name_, f.name_");
				} else {
					buffer.append(" ");
					buffer.append("m.name_, f.name_");
				}
				
			}
			
		} else {
		
			buffer.append(" ");
			buffer.append("m.order_, f.order_, m.name_, f.name_");
		}
		return buffer.toString();
			
	}
}
