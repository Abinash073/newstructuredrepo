package com.rlogistics.customqueries;

public class TicketMisReportPojo {
	private String consumerName;
	public String consumerComplaintNumber = "";
	public String addressLine1 = "";
	public String addressLine2 = "";
	public String city = "";
	public String pincode = "";
	public String telephoneNumber = "";
	public String emailId = "";
	public String orderNumber = "";
	public String dateOfComplaint = "";
	public String natureOfComplaint = "";
	public String isUnderWarranty = "";
	public String retailer = "";
	public String brand = "";
	public String productCategory = "";
	public String productName = "";
	public String model = "";
	public String identificationNo = "";
	public String dropLocation = "";// rlReturnLocationAddress
	public String dropLocContactPerson = "";
	public String dropLocContactNo = "";
	public String currentStatus = "";
	public String processId = "";
	public String rlTicketNo = "";
	public String rlLocationName = "";
	public String createDate = "";
	public String pickupDate = "";
	public String dropDate = "";
	public String createTime = "";
	public String pickupTime = "";
	public String dropTime = "";
	public String timeStamp = "";
	public String dropCity = "";
	public String closedReason = "";
	public String closedDate = "";
	public String closedTime = "";
	public String remark = "";
	public String docketNo = "";
	public String vehicleNo = "";
	public String weight = "";

//	private String franchiseFeName="";
//	private String agencyName="";

	private String feTypeForPickup = "";
	private String feNameForPickup = "";
	private String agencyNameForPickup = "";
	private String feTypeForDrop = "";
	private String feNameForDrop = "";
	private String agencyNameForDrop = "";


	public String getFeTypeForPickup() {
		return feTypeForPickup;
	}

	public void setFeTypeForPickup(String feTypeForPickup) {
		this.feTypeForPickup = feTypeForPickup;
	}

	public String getFeTypeForDrop() {
		return feTypeForDrop;
	}

	public void setFeTypeForDrop(String feTypeForDrop) {
		this.feTypeForDrop = feTypeForDrop;
	}

	public String getFeNameForPickup() {
		return feNameForPickup;
	}

	public void setFeNameForPickup(String feNameForPickup) {
		this.feNameForPickup = feNameForPickup;
	}

	public String getAgencyNameForPickup() {
		return agencyNameForPickup;
	}

	public void setAgencyNameForPickup(String agencyNameForPickup) {
		this.agencyNameForPickup = agencyNameForPickup;
	}

	public String getFeNameForDrop() {
		return feNameForDrop;
	}

	public void setFeNameForDrop(String feNameForDrop) {
		this.feNameForDrop = feNameForDrop;
	}

	public String getAgencyNameForDrop() {
		return agencyNameForDrop;
	}

	public void setAgencyNameForDrop(String agencyNameForDrop) {
		this.agencyNameForDrop = agencyNameForDrop;
	}



//	public String getFranchiseFeName() {
//		return franchiseFeName;
//	}
//
//	public void setFranchiseFeName(String franchiseFeName) {
//		this.franchiseFeName = franchiseFeName;
//	}
//
//	public String getAgencyName() {
//		return agencyName;
//	}
//
//	public void setAgencyName(String agencyName) {
//		this.agencyName = agencyName;
//	}





	

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getConsumerComplaintNumber() {
		return consumerComplaintNumber;
	}

	public void setConsumerComplaintNumber(String consumerComplaintNumber) {
		this.consumerComplaintNumber = consumerComplaintNumber;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getDateOfComplaint() {
		return dateOfComplaint;
	}

	public void setDateOfComplaint(String dateOfComplaint) {
		this.dateOfComplaint = dateOfComplaint;
	}

	public String getNatureOfComplaint() {
		return natureOfComplaint;
	}

	public void setNatureOfComplaint(String natureOfComplaint) {
		this.natureOfComplaint = natureOfComplaint;
	}

	public String getIsUnderWarranty() {
		return isUnderWarranty;
	}

	public void setIsUnderWarranty(String isUnderWarranty) {
		this.isUnderWarranty = isUnderWarranty;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getIdentificationNo() {
		return identificationNo;
	}

	public void setIdentificationNo(String identificationNo) {
		this.identificationNo = identificationNo;
	}

	public String getDropLocation() {
		return dropLocation;
	}

	public void setDropLocation(String dropLocation) {
		this.dropLocation = dropLocation;
	}

	public String getDropLocContactPerson() {
		return dropLocContactPerson;
	}

	public void setDropLocContactPerson(String dropLocContactPerson) {
		this.dropLocContactPerson = dropLocContactPerson;
	}

	public String getDropLocContactNo() {
		return dropLocContactNo;
	}

	public void setDropLocContactNo(String dropLocContactNo) {
		this.dropLocContactNo = dropLocContactNo;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getRlTicketNo() {
		return rlTicketNo;
	}

	public void setRlTicketNo(String rlTicketNo) {
		this.rlTicketNo = rlTicketNo;
	}

	public String getRlLocationName() {
		return rlLocationName;
	}

	public void setRlLocationName(String rlLocationName) {
		this.rlLocationName = rlLocationName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getDropDate() {
		return dropDate;
	}

	public void setDropDate(String dropDate) {
		this.dropDate = dropDate;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(String pickupTime) {
		this.pickupTime = pickupTime;
	}

	public String getDropTime() {
		return dropTime;
	}

	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getDropCity() {
		return dropCity;
	}

	public void setDropCity(String dropCity) {
		this.dropCity = dropCity;
	}

	public String getClosedReason() {
		return closedReason;
	}

	public void setClosedReason(String closedReason) {
		this.closedReason = closedReason;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getClosedTime() {
		return closedTime;
	}

	public void setClosedTime(String closedTime) {
		this.closedTime = closedTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}


	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	
	

}
