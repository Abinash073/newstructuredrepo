package com.rlogistics.customqueries;

public class ResultCount {

	String totalRecords;

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
}
