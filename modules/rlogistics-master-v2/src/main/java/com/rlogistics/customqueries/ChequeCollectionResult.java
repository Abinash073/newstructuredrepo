package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerChequeCollectionCenterRestData;

public class ChequeCollectionResult extends RetailerChequeCollectionCenterRestData{

	private String city;
	private String cityId;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
}
