package com.rlogistics.customqueries;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface FeLocationTransCustomQueries {

	@Select("SELECT latitude_ AS latitude, longitude_ AS longitude, date_ AS date"
			+ " FROM RL_MD_FE_LOCATION_TRANS"
			+ " WHERE date_ LIKE '%'||#{date}||'%' AND fe_name_ = #{feName}"
			+ " ORDER BY date")
	List<FeLocationTransResult> feLocationTransREsult(@Param("date") String date, @Param("feName") String feName);
	
}
