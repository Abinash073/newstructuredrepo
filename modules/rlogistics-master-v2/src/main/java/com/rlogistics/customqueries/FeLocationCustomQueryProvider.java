package com.rlogistics.customqueries;


import java.util.Map;

public class FeLocationCustomQueryProvider {
    public String getFeLocation(Map<String,String> params){
        StringBuffer buffer  = new StringBuffer();
        buffer.append("SELECT p.LONGITUDE_ AS longitude,p.LATITUDE_ as latitude,p.TICKET_ AS ticket,p.TOTAL_DISTANCE_ AS totalDistance,p.FE_NAME_ AS feName,p.DATE_ as date FROM RL_MD_FE_LOCATION_TRANS p WHERE 1=1");

        if(params.containsKey("date") && !params.get("date").equalsIgnoreCase("")){
            buffer.append(" ");
            buffer.append("AND DATE_ LIKE CONCAT(#{date},'%')");
        }

        if (params.containsKey("feName") && !params.get("feName").equals("")){

            buffer.append(" ");
            buffer.append("AND FE_NAME_= #{feName}");
        }
        if (params.containsKey("ticket") && !params.get("ticket").equals("")){
            buffer.append(" ");
            buffer.append("AND TICKET_= #{ticket}");
        }
        buffer.append(" ORDER BY DATE_ DESC limit 20");

        return buffer.toString();
    }
}
