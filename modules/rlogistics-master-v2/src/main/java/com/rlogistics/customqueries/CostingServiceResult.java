package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.CostingServiceRestData;

public class CostingServiceResult extends CostingServiceRestData{

	private String costingServiceCode;
	private String retailerId;
	private String retailerCostingServiceId;
	private String cost;
	private String status;
	
	public String getCostingServiceCode() {
		return costingServiceCode;
	}
	public void setCostingServiceCode(String costingServiceCode) {
		this.costingServiceCode = costingServiceCode;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getRetailerCostingServiceId() {
		return retailerCostingServiceId;
	}
	public void setRetailerCostingServiceId(String retailerCostingServiceId) {
		this.retailerCostingServiceId = retailerCostingServiceId;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
