package com.rlogistics.customqueries;

import java.util.List;

public class SingleFeTrackResult {
	List<TrackFeLocResult> data;
	int totalRecords;

	
	public List<TrackFeLocResult> getData() {
		return data;
	}
	public void setData(List<TrackFeLocResult> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

}
