package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ServiceProviderLocationRestData;

public class ServiceProviderLocationServiceDetailsResult extends  ServiceProviderLocationRestData {

	private String categoryId;
	private String categoryName;
	private String subCategoryName;
	private String subCategoryId;
	private String brandId;
	private String brandName;
	private String serviceProviderLocationArea;
	private String serviceProviderCode;
	private String serviceProviderLocationServiceId;
	private String isBizlogAuthorized;
	private String isAuthorizedServiceProvider;
	
	public String getIsBizlogAuthorized() {
		return isBizlogAuthorized;
	}
	public void setIsBizlogAuthorized(String isBizlogAuthorized) {
		this.isBizlogAuthorized = isBizlogAuthorized;
	}
	public String getIsAuthorizedServiceProvider() {
		return isAuthorizedServiceProvider;
	}
	public void setIsAuthorizedServiceProvider(String isAuthorizedServiceProvider) {
		this.isAuthorizedServiceProvider = isAuthorizedServiceProvider;
	}
	public String getServiceProviderLocationServiceId() {
		return serviceProviderLocationServiceId;
	}
	public void setServiceProviderLocationServiceId(String serviceProviderLocationServiceId) {
		this.serviceProviderLocationServiceId = serviceProviderLocationServiceId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getServiceProviderLocationArea() {
		return serviceProviderLocationArea;
	}
	public void setServiceProviderLocationArea(String serviceProviderLocationArea) {
		this.serviceProviderLocationArea = serviceProviderLocationArea;
	}
	public String getServiceProviderCode() {
		return serviceProviderCode;
	}
	public void setServiceProviderCode(String serviceProviderCode) {
		this.serviceProviderCode = serviceProviderCode;
	}
}
