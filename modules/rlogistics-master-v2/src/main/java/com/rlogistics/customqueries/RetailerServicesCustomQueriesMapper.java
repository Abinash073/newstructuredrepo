package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface RetailerServicesCustomQueriesMapper {

	@Select("SELECT s.id_ as serviceId,s.name_ as serviceName,s.code_ as serviceCode from RL_MD_SERVICE_MASTER s, RL_MD_RETAILER_SERVICES r where s.id_ = r.service_id_ and r.retailer_id_ = #{retailer}")
	List<RetailerServicesCustomQuery1Result> findServices(@Param("retailer") String retailer);
	
	@Select("SELECT s.id_ as serviceId,s.name_ as serviceName,s.code_ as serviceCode, r.retailer_id_ as retailerId, r.id_ as retailerServiceId from RL_MD_SERVICE_MASTER s LEFT JOIN RL_MD_RETAILER_SERVICES r ON (s.id_ = r.service_id_ and r.retailer_id_ = #{retailer})")
	List<RetailerServicesCustomQuery1Result> findServicesByRetailer(@Param("retailer") String retailer);
	
	@Select("SELECT s.id_ as serviceId,s.name_ as serviceName,s.code_ as serviceCode, r.retailer_id_ as retailerId, r.id_ as retailerServiceId from RL_MD_SERVICE_MASTER s INNER JOIN RL_MD_RETAILER_SERVICES r ON (s.id_ = r.service_id_) where r.retailer_id_ = #{retailer}")
	List<RetailerServicesCustomQuery1Result> findServicesForRetailer(@Param("retailer") String retailer);
	
	@Select("select d.id_ as id, d.retailer_id_ as retailerId, d.brand_id_ as brandId, d.is_installation_available_ as isInstallationAvailable, d.is_repair_available_ as isRepairAvailable, b.name_ as brandName from RL_MD_RETAILER_DOOR_STEP_SERVICE d INNER JOIN RL_MD_BRAND b ON(d.brand_id_ = b.id_) where d.retailer_id_ = #{retailerId} limit #{firstResult},#{maxResults}")
	List<RetailerDoorStepResult> getDoorStepServices(@Param("retailerId") String retailerId, @Param("firstResult") int firstResult, @Param("maxResults") int maxResults);
	
	@Select("select count(*) as totalRecords from RL_MD_RETAILER_DOOR_STEP_SERVICE d INNER JOIN RL_MD_BRAND b ON(d.brand_id_ = b.id_) where d.retailer_id_ = #{retailerId}")
	ResultCount getDoorStepServicesCount(@Param("retailerId") String retailerId);
}
