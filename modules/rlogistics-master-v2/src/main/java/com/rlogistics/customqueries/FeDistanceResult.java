package com.rlogistics.customqueries;

import java.util.List;

public class FeDistanceResult {
	
	private String feId;
	private String distance;
	private String date;
	private String location;
	
	public String getFeId() {
		return feId;
	}
	public void setFeId(String feId) {
		this.feId = feId;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
}
