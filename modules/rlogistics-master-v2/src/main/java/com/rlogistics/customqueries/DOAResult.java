package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.DoaFormRestData;

public class DOAResult extends DoaFormRestData{
	
	private String categoryName;
	private String brandName;
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}	
}
