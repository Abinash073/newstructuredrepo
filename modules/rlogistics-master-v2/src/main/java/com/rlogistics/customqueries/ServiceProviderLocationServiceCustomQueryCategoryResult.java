package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductCategoryRestData;

public class ServiceProviderLocationServiceCustomQueryCategoryResult extends  ProductCategoryRestData {
	
	private String categoryId;
	private String categoryName;
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
}
