package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.LocalPackingRestData;

public class LocalPackingResult extends LocalPackingRestData {

	private String categoryName;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
