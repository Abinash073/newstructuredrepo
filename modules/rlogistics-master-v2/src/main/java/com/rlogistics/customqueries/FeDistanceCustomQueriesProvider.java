package com.rlogistics.customqueries;

import java.util.Map;

public class FeDistanceCustomQueriesProvider {
    public String provideFeDistanceQuery(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("select FE_ID_ as feId,FE_NAME_ as feName,DISTANCE_ as distance from RL_MD_FE_DISTANCE where 1=1");
        if (params.containsKey("feId") && !params.get("feId").equalsIgnoreCase("")) {
            buffer.append(" ");
            buffer.append("and FE_ID_=#{feId}");

        }

        if (params.containsKey("date") && !params.get("date").equalsIgnoreCase("")) {
            buffer.append(" ");
            buffer.append("and DATE(CREATED_ON_)=#{date}");
        }

        if (params.containsKey("startDate") && !params.get("startDate").equalsIgnoreCase("")) {
            buffer.append(" ");
            buffer.append("and DATE(CREATED_ON_) between #{startDate}");
        }
        if (params.containsKey("endDate") && !params.get("endDate").equalsIgnoreCase("")) {
            buffer.append(" ");
            buffer.append("and #{endDate}");
        }
        return buffer.toString();
    }

    public String getLastFeDistanceData(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("select FE_NAME_        as feName,\n" +
                "       TIME_           as time,\n" +
                "       LONGITUDE_      as longitude,\n" +
                "       LATITUDE_       as latitude,\n" +
                "       TOTAL_DISTANCE_ as totalDistance\n" +
                "from RL_MD_LAT_LONG\n" +
                "where FE_NAME_ = '#{feName}'\n" +
                "  and TIME_ like '%#{startDate}%'\n" +
                "order by TIME_ desc\n" +
                "limit 1;");
        return changeNameAndDate(buffer.toString(), params);

    }

    private String changeNameAndDate(String buffer, Map<String, String> params) {
        return buffer.replaceAll("\\#\\{([^}]*feName?)}", params.get("feName"))
                .replaceAll("\\#\\{([^}]*startDate?)}", params.get("date"));
    }

    public String getDistancePerFe(Map<String, String> param) {
        StringBuffer buffer = new StringBuffer("select b.FIRST_          as firstName,\n" +
                "       b.LAST_           as lastName,\n" +
                "       b.EMAIL_          as email,\n" +
                "       a.TOTAL_DISTANCE_ as totalDistance,\n" +
                "       a.TIME_           as time,\n" +
                "       a.PROCESS_ID_     as processId,\n" +
                "       a.TICKET_NO_      as ticketNo\n" +
                "from RL_MD_LAT_LONG as a,\n" +
                "     RL_MD_USER as b\n" +
                "where a.FE_NAME_ = '#{feName}'\n" +
                "  and a.TIME_ between '#{startDate}' and '#{endDate}'\n" +
                "  and b.EMAIL_ = a.FE_NAME_;");
        return changeNameAndDateWithStartAndEnd(param, buffer.toString());

    }

    private String changeNameAndDateWithStartAndEnd(Map<String, String> params, String query) {
        return query.replaceAll("\\#\\{([^}]*feName?)}", params.get("feName"))
                .replaceAll("\\#\\{([^}]*startDate?)}", params.get("startDate"))
                .replaceAll("\\#\\{([^}]*endDate?)}", params.get("endDate"))
                //todo enable if pagination is required
                //.replaceAll("\\#\\{([^}]*start?)}", params.get("start"))
                //.replaceAll("\\#\\{([^}]*end?)}", params.get("end"))
                ;
    }

    public String feDistanceReport(Map<String, String> params) {

        StringBuffer buffer = new StringBuffer(
                "select  a.TOTAL_DISTANCE_ as totalDistance,\n" +
                        "       a.FE_NAME_           as feName,\n" +
                        "       a.CITY_     as city\n" +
                        "from RL_MD_LAT_LONG as a\n" +
                        "where a.TIME_ between '#{startDate}' and '#{endDate}'\n" +
                        "  and a.CITY_ = '#{city}'\n");

        if (params.containsKey("feName") && !params.get("feName").equalsIgnoreCase("")) {
            buffer.append(" ");
            buffer.append("and a.FE_NAME_=#{feName}");
        }
        return changeNameAndDateWithStartAndEndAndCity(params, buffer.toString());
    }

    private String changeNameAndDateWithStartAndEndAndCity(Map<String, String> params, String query) {
        return query.replaceAll("\\#\\{([^}]*feName?)}", params.get("feName"))
                .replaceAll("\\#\\{([^}]*startDate?)}", params.get("startDate"))
                .replaceAll("\\#\\{([^}]*endDate?)}", params.get("endDate"))
                .replaceAll("\\#\\{([^}]*city?)}", params.get("city"))
                //todo enable if pagination is required
                //.replaceAll("\\#\\{([^}]*start?)}", params.get("start"))
                //.replaceAll("\\#\\{([^}]*end?)}", params.get("end"))
                ;

    }
}
