package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface PaymentDetailsCustomQueriesMapper {

    @SelectProvider(type = PaymentDetailsQueryProvider.class, method = "getPaymentDetailsByDate")
    List<PaymentDetailsData> getPaymentDetailsData(@Param("startDate") String startDate,
                                                   @Param("endDate") String endDate,
                                                   @Param("retailerName") String retailerName,
                                                   @Param("ticketNo")String ticketNo
    );
}
