package com.rlogistics.customqueries;
import java.util.List;

import com.rlogistics.customqueries.ImeiInventoryResult;

public class ImeiInventoryResultResponse {
	
	List<ImeiInventoryResult> data;
	int totalRecords;
	
	public List<ImeiInventoryResult> getData() {
		return data;
	}
	public void setData(List<ImeiInventoryResult> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

}
