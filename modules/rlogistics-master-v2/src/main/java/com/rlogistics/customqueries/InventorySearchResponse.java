package com.rlogistics.customqueries;

import java.util.List;

public class InventorySearchResponse {
	
	List<PackagingInventoryResult> data;
	int totalRecords;
	
	public List<PackagingInventoryResult> getData() {
		return data;
	}
	public void setData(List<PackagingInventoryResult> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
}
