//package com.rlogistics.customqueries;
//
//import java.util.List;
//
//import org.apache.ibatis.annotations.Param;
//import org.apache.ibatis.annotations.SelectProvider;
//
//import com.rlogistics.master.CustomQueriesMapper;
//
//@CustomQueriesMapper
//public interface ImeiInventoryCustomQueriesMapper {
//	@SelectProvider(type=ImeiInventoryCustomQueryProvider.class,method="provideImeiInventoryQuery")
//	List<ImeiInventoryResult> findImeiList(@Param("location") String location,@Param("brand") String brand,@Param("imeiNo") String imeiNo,@Param("model") String model,@Param("year") String year,@Param("status") String status,@Param("firstResult") String firstResult,@Param("maxResults") String maxResults);
//    
//	@SelectProvider(type=ImeiInventoryCustomQueryProvider.class,method="provideImeiInventoryQueryCount")
//	ResultCount findImeiInventoryCount(@Param("location") String location,@Param("brand") String brand,@Param("imeiNo") String imeiNo,@Param("model") String model,@Param("year") String year,@Param("status") String status);
//}
