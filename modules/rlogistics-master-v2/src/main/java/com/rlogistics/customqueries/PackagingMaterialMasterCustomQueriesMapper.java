package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface PackagingMaterialMasterCustomQueriesMapper {

	@SelectProvider(type=PackagingMaterialMasterCustomQueryProvider.class,method="provideFindPackagingMaterialsQuery")
	List<PackagingMaterialResult> findPackagingMaterials(@Param("locationId") String locationId,@Param("packagingTypeCode") String packagingTypeCode,@Param("packagingType") String packagingType,@Param("firstResult") String firstResult,@Param("maxResults") String maxResults);
	
	@SelectProvider(type=PackagingMaterialMasterCustomQueryProvider.class,method="provideFindPackagingMaterialsQueryCount")
	ResultCount findPackagingMaterialsCount(@Param("locationId") String locationId,@Param("packagingTypeCode") String packagingTypeCode,@Param("packagingType") String packagingType);
	
	
	@Select("SELECT pi.id_ as id, pi.location_id_ as locationId, pi.packaging_type_code_ as packagingTypeCode, pi.barcode_ as barcode, pi.status_ as status, pi.ticket_number_ as ticketNumber, pi.user_id_ as userId, pi.additional_details_ as additionalDetails, p.name_ as packagingTypeName, p.type_ as packagingType, l.name_ as locationName, l.code_ as locationCode, l.city_ as city, u.first_ as firstName, u.last_ as lastName, u.email_ as email FROM RL_MD_PACKAGING_MATERIAL_INVENTORY pi INNER JOIN RL_MD_PACKAGING_TYPE p ON (pi.packaging_type_code_ = p.code_) INNER JOIN RL_MD_LOCATION l ON (pi.location_id_ = l.id_) LEFT JOIN RL_MD_USER u ON (pi.user_id_ = u.id_)")
	List<PackagingInventoryResult> getPackagingInventory();
	
	@Select("SELECT pi.id_ as id, pi.location_id_ as locationId, pi.packaging_type_code_ as packagingTypeCode, pi.barcode_ as barcode, pi.status_ as status, pi.ticket_number_ as ticketNumber, pi.user_id_ as userId, pi.additional_details_ as additionalDetails, l.name_ as locationName, l.code_ as locationCode, l.city_ as city, u.first_ as firstName, u.last_ as lastName, u.email_ as email FROM RL_MD_PACKAGING_MATERIAL_INVENTORY pi INNER JOIN RL_MD_LOCATION l ON (pi.location_id_ = l.id_) LEFT JOIN RL_MD_USER u ON (pi.user_id_ = u.id_) where pi.barcode_ = #{barcode}")
	PackagingInventoryObject getPackagingInventoryByBarcode(@Param("barcode") String barcode);
	
	@SelectProvider(type=PackagingMaterialMasterCustomQueryProvider.class,method="providePackagingInventorySearch")
	List<PackagingInventoryResult> getPackagingInventorySearch(@Param("barcode") String barcode, @Param("packagingTypeCode") String packagingTypeCode, @Param("locationId") String locationId, @Param("packagingType") String packagingType, @Param("status") String status,@Param("ticketNumber") String ticketNumber,@Param("imeiNo") String imeiNo, @Param("firstResult") String firstResult, @Param("maxResults") String maxResults);
	
	@SelectProvider(type=PackagingMaterialMasterCustomQueryProvider.class,method="providePackagingInventorySearchCount")
	PackagingInventoryResultCount getPackagingInventorySearchCount(@Param("barcode") String barcode, @Param("packagingTypeCode") String packagingTypeCode, @Param("locationId") String locationId, @Param("packagingType") String packagingType, @Param("status") String status, @Param("ticketNumber") String ticketNumber, @Param("imeiNo") String imeiNo);
	
	@Select("select b.id_ as id, b.location_id_ as locationId, l.code_ as locationCode, l.name_ as locationName, b.packaging_type_code_ as packagingTypeCode, p.name_ as packagingTypeName, p.type_ as packagingType, b.starting_barcode_ as startingBarcode, b.ending_barcode_ as endingBarcode, b.item_count_ as itemCount, b.print_status_ as printStatus, b.generated_date_ as generatedDate FROM RL_MD_BARCODE_GENERATION b INNER JOIN RL_MD_LOCATION l ON (l.id_ = b.location_id_) INNER JOIN RL_MD_PACKAGING_TYPE p ON (p.code_ = b.packaging_type_code_) ORDER BY b.generated_date_, b.location_id_, b.packaging_type_code_ limit #{firstResult},#{maxResults}")
	List<BarcodeGenerationResult> getGeneratedBarcodes(@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
	
	@Select("select count(*) as totalRecords FROM RL_MD_BARCODE_GENERATION b INNER JOIN RL_MD_LOCATION l ON (l.id_ = b.location_id_) INNER JOIN RL_MD_PACKAGING_TYPE p ON (p.code_ = b.packaging_type_code_)")
	ResultCount getGeneratedBarcodesCount();
}
