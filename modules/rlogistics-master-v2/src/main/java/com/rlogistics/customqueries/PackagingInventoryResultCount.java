package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.PackagingMaterialInventoryRestData;

public class PackagingInventoryResultCount extends PackagingMaterialInventoryRestData {

	String totalRecords;

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
}
