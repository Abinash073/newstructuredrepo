package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;



@CustomQueriesMapper
public interface ServiceProviderLocationCustomQueriesMapper {
	
	@SelectProvider(type=ServiceProviderLocationCustomQueryProvider.class,method="provideFindServiceLocationsQuery")
	List<ServiceProviderLocationRetailerResult> findServiceLocationsByRetailer(@Param("retailerId") String retailerId,@Param("categoryId") String categoryId, @Param("brandId") String brandId,
			@Param("cityId") String cityId,@Param("subCategoryId") String subCategoryId, @Param("isAuthorizedServiceProvider") String isAuthorizedServiceProvider, @Param("pincode") String pincode);
	
	@SelectProvider(type=ServiceProviderLocationCustomQueryProvider.class,method="provideFindServiceLocationsQueryBizlog")
	List<ServiceProviderLocationCustomQuery1Result> findServiceLocationsByBizlog(@Param("isBizlogAuthorized") String isBizlogAuthorized,@Param("categoryId") String categoryId, @Param("brandId") String brandId,
			@Param("subCategoryId") String subCategoryId,@Param("cityId") String cityId, @Param("isAuthorizedServiceProvider") String isAuthorizedServiceProvider);
	
	@Select("SELECT s.area_ as serviceProviderLocationArea, s.service_provider_code_ as serviceProviderCode, s.city_id_ as cityId, s.city_ city, s.service_provider_id_ serviceProviderId, s.address1_ as address1, s.address2_ as address2, s.pincode_ as pincode, s.email_ as email, s.phone1_ as phone1, s.phone2_ as phone2, b.id_ as brandId, b.name_ as brandName, c.id_ as categoryId, c.name_ as categoryName, sc.id_ as subCategoryId, sc.name_ as subCategoryName, sl.id_ as serviceProviderLocationServiceId, sl.is_bizlog_authorized_ as isBizlogAuthorized, sl.is_authorized_service_provider_ as isAuthorizedServiceProvider from RL_MD_SERVICE_PROVIDER_LOCATION s INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE sl ON (s.id_ = sl.service_provider_location_id_) LEFT JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = sl.category_id_) LEFT JOIN RL_MD_BRAND b ON (b.id_ = sl.brand_id_) LEFT JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (sc.id_ = sl.sub_category_id_) where s.service_provider_id_ = #{serviceProviderId}")
	List<ServiceProviderLocationServiceDetailsResult> findServiceLocationDetails(@Param("serviceProviderId") String serviceProviderId);
	
	@SelectProvider(type=ServiceProviderLocationCustomQueryProvider.class,method="provideFindServiceProviderQuery")
	List<ServiceProviderDetailsResult> findServiceProviderDetails(@Param("serviceProviderId") String serviceProviderId, @Param("firstResult") String firstResult,@Param("maxResults") String maxResults);
	
	@SelectProvider(type=ServiceProviderLocationCustomQueryProvider.class,method="provideFindServiceProviderQueryCount")
	ResultCount findServiceProviderDetailsCount(@Param("serviceProviderId") String serviceProviderId);
	
	@Select("SELECT r.id_ as id, r.retailer_id_ as retailerId, r.service_provider_location_id_ as serviceProviderLocationId, r.category_id_ as categoryId, r.sub_category_id_ as subCategoryId, r.brand_id_ as brandId, r.city_id_ as cityId, r.city_ as city, sl.service_provider_id_ as serviceProviderId, sl.service_provider_code_ as serviceProviderCode, sl.area_ as area, c.name_ as categoryName, sc.name_ as subCategoryName, b.name_ as brandName FROM RL_MD_RETAILER_SERVICE_PROVIDER r INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION sl ON (sl.id_ = r.service_provider_location_id_) LEFT JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = r.category_id_) LEFT JOIN RL_MD_BRAND b ON (b.id_ = r.brand_id_) LEFT JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (sc.id_ = r.sub_category_id_) where r.retailer_id_ = #{retailerId} limit #{firstResult},#{maxResults}")
	List<RetailerServiceProvidersResult> findRetailerServiceProviders(@Param("retailerId") String retailerId,@Param("firstResult") int firstResult,@Param("maxResults") int maxResults);
	
	@Select("SELECT count(*) as totalRecords FROM RL_MD_RETAILER_SERVICE_PROVIDER r INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION sl ON (sl.id_ = r.service_provider_location_id_) LEFT JOIN RL_MD_PRODUCT_CATEGORY c ON (c.id_ = r.category_id_) LEFT JOIN RL_MD_BRAND b ON (b.id_ = r.brand_id_) LEFT JOIN RL_MD_PRODUCT_SUB_CATEGORY sc ON (sc.id_ = r.sub_category_id_) where r.retailer_id_ = #{retailerId}")
	ResultCount findRetailerServiceProvidersCount(@Param("retailerId") String retailerId);
}
