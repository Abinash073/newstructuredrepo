package com.rlogistics.customqueries;

import java.util.Map;

public class CostingServiceCustomQueryProvider {

	public static String provideRetailerCostingActivityMapping(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select cm.id_ as mappingId, cm.costing_activity_id_ as costingActivityId, cm.category_id_ as categoryId, cm.service_id_ as serviceId, c.name_ as costingActivityName, c.code_ as costingActivityCode, p.name_ as productCategoryName, s.name_ as serviceName, rc.retailer_id_ as retailerId, rc.cost_ as cost, rc.status_ as status, rc.id_ as retailerCostingActivityId, r.name_ as retailerName FROM RL_MD_COSTING_ACTIVITY_MAPPING cm INNER JOIN RL_MD_COSTING_ACTIVITY c ON (c.id_ = cm.costing_activity_id_) INNER JOIN RL_MD_PRODUCT_CATEGORY p ON (p.id_ = cm.category_id_) INNER JOIN RL_MD_SERVICE_MASTER s ON (s.id_ = cm.service_id_) LEFT JOIN RL_MD_RETAILER_COSTING_ACTIVITY rc ON (c.code_ = rc.costing_activity_code_ and rc.retailer_id_ = #{retailerId} and cm.service_id_ = rc.service_id_ and cm.category_id_ = rc.category_id_) LEFT JOIN RL_MD_RETAILER r ON (r.id_ = rc.retailer_id_) where 1=1");
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and p.id_ = #{categoryId}");
		}
		
		if(params.containsKey("serviceId") && !params.get("serviceId").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{serviceId}");
		}
		
		if(params.containsKey("activityId") && !params.get("activityId").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{activityId}");
		}
		
		
		buffer.append(" ");
		buffer.append("ORDER BY s.name_, p.name_, c.name_ limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
		
		return buffer.toString();
			
	}
	
	public static String provideRetailerCostingActivityMappingCount(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select count(*) as totalRecords FROM RL_MD_COSTING_ACTIVITY_MAPPING cm INNER JOIN RL_MD_COSTING_ACTIVITY c ON (c.id_ = cm.costing_activity_id_) INNER JOIN RL_MD_PRODUCT_CATEGORY p ON (p.id_ = cm.category_id_) INNER JOIN RL_MD_SERVICE_MASTER s ON (s.id_ = cm.service_id_) LEFT JOIN RL_MD_RETAILER_COSTING_ACTIVITY rc ON (c.code_ = rc.costing_activity_code_ and rc.retailer_id_ = #{retailerId} and cm.service_id_ = rc.service_id_ and cm.category_id_ = rc.category_id_) LEFT JOIN RL_MD_RETAILER r ON (r.id_ = rc.retailer_id_) where 1=1");
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and p.id_ = #{categoryId}");
		}
		
		if(params.containsKey("serviceId") && !params.get("serviceId").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{serviceId}");
		}
		
		if(params.containsKey("activityId") && !params.get("activityId").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{activityId}");
		}
			
		return buffer.toString();
			
	}
	
	public static String provideCostingActivityMapping(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select cm.id_ as id, cm.costing_activity_id_ as costingActivityId, cm.category_id_ as categoryId, cm.service_id_ as serviceId, c.name_ as costingActivityName, c.code_ as costingActivityCode, p.name_ as productCategoryName, s.name_ as serviceName FROM RL_MD_COSTING_ACTIVITY_MAPPING cm INNER JOIN RL_MD_COSTING_ACTIVITY c ON (c.id_ = cm.costing_activity_id_) INNER JOIN RL_MD_PRODUCT_CATEGORY p ON (p.id_ = cm.category_id_) INNER JOIN RL_MD_SERVICE_MASTER s ON (s.id_ = cm.service_id_) where 1=1");
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and p.id_ = #{categoryId}");
		}
		
		if(params.containsKey("serviceId") && !params.get("serviceId").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{serviceId}");
		}
		
		if(params.containsKey("activityId") && !params.get("activityId").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{activityId}");
		}
		
		
		buffer.append(" ");
		buffer.append("limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
		
		return buffer.toString();
			
	}
	
	public static String provideCostingActivityMappingCount(Map<String,String> params) {
		
		StringBuffer buffer=  new StringBuffer();
		
//		System.out.println("params->"+params);
		
		buffer.append("select count(*) as totalRecords FROM RL_MD_COSTING_ACTIVITY_MAPPING cm INNER JOIN RL_MD_COSTING_ACTIVITY c ON (c.id_ = cm.costing_activity_id_) INNER JOIN RL_MD_PRODUCT_CATEGORY p ON (p.id_ = cm.category_id_) INNER JOIN RL_MD_SERVICE_MASTER s ON (s.id_ = cm.service_id_) where 1=1");
		
		if(params.containsKey("categoryId") && !params.get("categoryId").equals("")) {
			buffer.append(" ");
			buffer.append("and p.id_ = #{categoryId}");
		}
		
		if(params.containsKey("serviceId") && !params.get("serviceId").equals("")) {
			buffer.append(" ");
			buffer.append("and s.id_ = #{serviceId}");
		}
		
		if(params.containsKey("activityId") && !params.get("activityId").equals("")) {
			buffer.append(" ");
			buffer.append("and c.id_ = #{activityId}");
		}
			
		return buffer.toString();
			
	}
}
