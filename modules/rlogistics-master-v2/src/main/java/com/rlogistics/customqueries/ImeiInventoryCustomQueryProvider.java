package com.rlogistics.customqueries;

import java.util.Map;

public class ImeiInventoryCustomQueryProvider {
	public static String provideImeiInventoryQuery(Map<String,String> params){
		/* Use params in any way and build a query string. */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT i.location_ as location,i.brand_ as brand,i.imei_No_ as imeiNo,i.model_ as model,i.year_ as year,i.status_ as status,i.barcode_ as barcode,i.retailer_Id_ as retailerId,i.product_Category_ as productCategory,i.product_Cost_ as productCost from RL_MD_IMEI_INVENTORY i where 1=1");
		
		if(params.containsKey("location") && !params.get("location").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.location_) LIKE " + "'" + params.get("location") + "%'");
		}
		if(params.containsKey("brand") && !params.get("brand").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.brand_) LIKE " + "'" + params.get("brand") + "%'");
		}
		if(params.containsKey("productCategory") && !params.get("productCategory").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.product_Category_) LIKE " + "'" + params.get("productCategory") + "%'");
		}
		if(params.containsKey("imeiNo") && !params.get("imeiNo").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.imei_No_) LIKE " + "'" + params.get("imeiNo") + "%'");
		}
		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" ");
			buffer.append("and i.retailer_Id_ = " + "'" + params.get("retailerId")+ "'");
		}
		if(params.containsKey("model") && !params.get("model").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.model_) LIKE " + "'" + params.get("model") + "%'");
		}
		if(params.containsKey("year") && !params.get("year").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.year_) LIKE " + "'" + params.get("year") + "%'");
		}
		if(params.containsKey("barcode") && !params.get("barcode").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.barcode_) LIKE " + "'" + params.get("barcode") + "%'");
		}
		if(params.containsKey("status") && !params.get("status").equals("")) {
			buffer.append(" ");
//			buffer.append("and LOWER(i.status_) LIKE " + "'" +Integer.valueOf(params.get("status")+"'"));
			buffer.append("and LOWER(i.status_) = " + Integer.valueOf(params.get("status")));
		}
		if(params.containsKey("cost1") && !params.get("cost1").equals("") && params.containsKey("cost2") && !params.get("cost2").equals("")) {
			buffer.append(" ");
//			buffer.append("and LOWER(i.status_) LIKE " + "'" +Integer.valueOf(params.get("status")+"'"));
			buffer.append("and LOWER(i.product_Cost_) BETWEEN " + Integer.valueOf(params.get("cost1")) +" AND "+Integer.valueOf(params.get("cost2")));
		}
		
		
		buffer.append(" ");
		buffer.append("limit " + Integer.valueOf(params.get("firstResult")) + "," + Integer.valueOf(params.get("maxResults")));
//		System.out.println("SQL Query  : "+buffer);
		return buffer.toString();
	}
	
	public static String provideImeiInventoryForExcel(Map<String,String> params){
		/* Use params in any way and build a query string. */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT i.location_ as location,i.brand_ as brand,i.imei_No_ as imeiNo,i.model_ as model,i.year_ as year,i.status_ as status,i.barcode_ as barcode,i.retailer_Id_ as retailerId,i.product_Category_ as productCategory,i.product_Cost_ as productCost from RL_MD_IMEI_INVENTORY i where 1=1");
		
		if(params.containsKey("location") && !params.get("location").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.location_) LIKE " + "'" + params.get("location") + "%'");
		}
		if(params.containsKey("brand") && !params.get("brand").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.brand_) LIKE " + "'" + params.get("brand") + "%'");
		}
		if(params.containsKey("productCategory") && !params.get("productCategory").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.product_Category_) LIKE " + "'" + params.get("productCategory") + "%'");
		}
		if(params.containsKey("imeiNo") && !params.get("imeiNo").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.imei_No_) LIKE " + "'" + params.get("imeiNo") + "%'");
		}
		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" ");
			buffer.append("and i.retailer_Id_ = " + "'" + params.get("retailerId")+ "'");
		}
		if(params.containsKey("model") && !params.get("model").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.model_) LIKE " + "'" + params.get("model") + "%'");
		}
		if(params.containsKey("year") && !params.get("year").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.year_) LIKE " + "'" + params.get("year") + "%'");
		}
		if(params.containsKey("barcode") && !params.get("barcode").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.barcode_) LIKE " + "'" + params.get("barcode") + "%'");
		}
		if(params.containsKey("status") && !params.get("status").equals("")) {
			buffer.append(" ");
//			buffer.append("and LOWER(i.status_) LIKE " + "'" +Integer.valueOf(params.get("status")+"'"));
			buffer.append("and LOWER(i.status_) = " + Integer.valueOf(params.get("status")));
		}
		if(params.containsKey("cost1") && !params.get("cost1").equals("") && params.containsKey("cost2") && !params.get("cost2").equals("")) {
			buffer.append(" ");
//			buffer.append("and LOWER(i.status_) LIKE " + "'" +Integer.valueOf(params.get("status")+"'"));
			buffer.append("and LOWER(i.product_Cost_) BETWEEN " + Integer.valueOf(params.get("cost1")) +" AND "+Integer.valueOf(params.get("cost2")));
		}
		return buffer.toString();
	}
	
	public static String provideImeiInventoryQueryCount(Map<String,String> params){
		/* Use params in any way and build a query string.  */
		
		StringBuffer buffer=  new StringBuffer();
		
		buffer.append("SELECT count(*) as totalRecords from RL_MD_IMEI_INVENTORY i where 1=1");
		
		if(params.containsKey("location") && !params.get("location").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.location_) LIKE " + "'" + params.get("location") + "%'");
		}
		if(params.containsKey("brand") && !params.get("brand").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.brand_) LIKE " + "'" + params.get("brand") + "%'");
		}
		if(params.containsKey("imeiNo") && !params.get("imeiNo").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.imei_No_) LIKE " + "'" + params.get("imeiNo") + "%'");
		}
		if(params.containsKey("model") && !params.get("model").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.model_) LIKE " + "'" + params.get("model") + "%'");
		}
		if(params.containsKey("year") && !params.get("year").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.year_) LIKE " + "'" + params.get("year") + "%'");
		}
		if(params.containsKey("status") && !params.get("status").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.status_) = " + Integer.valueOf(params.get("status")));
		}
		if(params.containsKey("barcode") && !params.get("barcode").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.barcode_) = " + params.get("barcode"));
		}
		if(params.containsKey("retailerId") && !params.get("retailerId").equals("")) {
			buffer.append(" ");
			buffer.append("and i.retailer_Id_ = " + "'" + params.get("retailerId")+ "'");
		}
		if(params.containsKey("productCategory") && !params.get("productCategory").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.product_Category_) = " +"'"+ params.get("productCategory")+"'");
		}
		if(params.containsKey("cost1") && !params.get("cost1").equals("") && params.containsKey("cost2") && !params.get("cost2").equals("")) {
			buffer.append(" ");
			buffer.append("and LOWER(i.product_Cost_) BETWEEN " + Integer.valueOf(params.get("cost1")) +" AND "+Integer.valueOf(params.get("cost2")));
		}
		return buffer.toString();
	}

}
