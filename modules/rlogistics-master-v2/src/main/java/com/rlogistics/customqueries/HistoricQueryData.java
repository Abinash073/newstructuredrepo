package com.rlogistics.customqueries;

public class HistoricQueryData extends DataTest {
    private String consumerName = "N/A";
    private String productCategory = "N/A";
    private String product = "N/A";
    private String retailer = "N/A";
    private String telephoneNumber = "N/A";
    private String rlLocationName = "N/A";
    private String natureOfComplaint = "N/A";
    private String rlTicketStatus = "N/A";
    private String rlProcessStatus = "N/A";
    private String dateOfComplaint = "N/A";
    private String rlTicketNo = "N/A";
    private String consumerComplaintNumber = "N/A";
    private String addressLine1 = "N/A";
    private String addressLine2 = "N/A";
    private String city = "N/A";
    private String dropLocCity = "N/A";
    private String dropLocAddress1 = "N/A";
    private String pincode = "N/A";
    private String brand = "N/A";
    private String model = "N/A";
    private String productCode = "N/A";
    private String ticketCreationDate = "N/A";
    private String dateOfPurchase = "N/A";
    private String rlModeOfPayment = "N/A";
    private Object ticketCloserDate = "N/A";
    private String maxValueToBeOffered = "N/A";

    public String getMaxValueToBeOffered() {
        return maxValueToBeOffered;
    }

    public void setMaxValueToBeOffered(String maxValueToBeOffered) {
        this.maxValueToBeOffered = maxValueToBeOffered;
    }

    public Object getTicketCloserDate() {
        return ticketCloserDate;
    }

    public void setTicketCloserDate(Object ticketCloserDate) {
        this.ticketCloserDate = ticketCloserDate;
    }

    public String getRlModeOfPayment() {
        return rlModeOfPayment;
    }

    public void setRlModeOfPayment(String rlModeOfPayment) {
        this.rlModeOfPayment = rlModeOfPayment;
    }

    private String rlReportingCity = "N/A";
    private String rlValueOffered = "N/A";
    private String dropLocContactPerson = "N/A";
    private String orderNumber = "N/A";
    private String rlInvolvedUser = "N/A";
    private String processId = "N/A";
    private String rlReturnLocationAddress = "N/A";

    public String getRlRefundPaymentOption() {
        return rlRefundPaymentOption;
    }

    public void setRlRefundPaymentOption(String rlRefundPaymentOption) {
        this.rlRefundPaymentOption = rlRefundPaymentOption;
    }

    private String identificationNo = "N/A";
    private String rlRefundPaymentOption = "N/A";

    public String getDropLocContactPerson() {
        return dropLocContactPerson;
    }

    public void setDropLocContactPerson(String dropLocContactPerson) {
        this.dropLocContactPerson = dropLocContactPerson;
    }

    public String getRlValueOffered() {
        return rlValueOffered;
    }

    public void setRlValueOffered(String rlValueOffered) {
        this.rlValueOffered = rlValueOffered;
    }


    public String getRlReturnLocationAddress() {
        return rlReturnLocationAddress;
    }

    public void setRlReturnLocationAddress(String rlReturnLocationAddress) {
        this.rlReturnLocationAddress = rlReturnLocationAddress;
    }


    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getRlLocationName() {
        return rlLocationName;
    }

    public String getIdentificationNo() {
        return identificationNo;
    }

    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    public void setRlLocationName(String rlLocationName) {
        this.rlLocationName = rlLocationName;
    }

    public String getNatureOfComplaint() {
        return natureOfComplaint;
    }

    public void setNatureOfComplaint(String natureOfComplaint) {
        this.natureOfComplaint = natureOfComplaint;
    }

    public String getRlTicketStatus() {
        return rlTicketStatus;
    }

    public void setRlTicketStatus(String rlTicketStatus) {
        this.rlTicketStatus = rlTicketStatus;
    }

    public String getRlProcessStatus() {
        return rlProcessStatus;
    }

    public void setRlProcessStatus(String rlProcessStatus) {
        this.rlProcessStatus = rlProcessStatus;
    }

    public String getDateOfComplaint() {
        return dateOfComplaint;
    }

    public void setDateOfComplaint(String dateOfComplaint) {
        this.dateOfComplaint = dateOfComplaint;
    }

    public String getRlTicketNo() {
        return rlTicketNo;
    }

    public void setRlTicketNo(String rlTicketNo) {
        this.rlTicketNo = rlTicketNo;
    }

    public String getConsumerComplaintNumber() {
        return consumerComplaintNumber;
    }

    public void setConsumerComplaintNumber(String consumerComplaintNumber) {
        this.consumerComplaintNumber = consumerComplaintNumber;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDropLocCity() {
        return dropLocCity;
    }

    public void setDropLocCity(String dropLocCity) {
        this.dropLocCity = dropLocCity;
    }

    public String getDropLocAddress1() {
        return dropLocAddress1;
    }

    public void setDropLocAddress1(String dropLocAddress1) {
        this.dropLocAddress1 = dropLocAddress1;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getTicketCreationDate() {
        return ticketCreationDate;
    }

    public void setTicketCreationDate(String ticketCreationDate) {
        this.ticketCreationDate = ticketCreationDate;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getRlReportingCity() {
        return rlReportingCity;
    }

    public void setRlReportingCity(String rlReportingCity) {
        this.rlReportingCity = rlReportingCity;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRlInvolvedUser() {
        return rlInvolvedUser;
    }

    public void setRlInvolvedUser(String rlInvolvedUser) {
        this.rlInvolvedUser = rlInvolvedUser;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }


}
