package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

@CustomQueriesMapper
public interface HistoricDataQueryMapper {
    @SelectProvider(type = HistoricDataQueryProvider.class, method = "historicDataQuery")
    List<HistoricQueryData> getHistoricData(@Param("startTime") String startDate,
                                            @Param("endTime") String endDate,
                                            @Param("param") Map<String, String> param,
                                            @Param("variable") Set<String> variable,
                                            @Param("PathVariables") Map<String, String> PathVariables);

    @SelectProvider(type = HistoricDataQueryProvider.class, method = "historicDataQueryCount")
    Integer getHistoricDataCount(@Param("startTime") String startDate,
                                 @Param("endTime") String endDate,
                                 @Param("param") Map<String, String> param,
                                 @Param("variable") Set<String> variable,
                                 @Param("PathVariables") Map<String, String> PathVariables);

}
