package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ServiceProviderLocationRestData;

public class ServiceProviderDetailsResult extends  ServiceProviderLocationRestData{

	private String serviceProviderName;
	private String serviceProviderLocationId;
	private String categoryId;
	private String categoryName;

	public String getServiceProviderLocationId() {
		return serviceProviderLocationId;
	}

	public void setServiceProviderLocationId(String serviceProviderLocationId) {
		this.serviceProviderLocationId = serviceProviderLocationId;
	}

	public String getServiceProviderName() {
		return serviceProviderName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}
	
}
