package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.MasterCategoryRestData;

public class MasterCategoryResult extends MasterCategoryRestData {

	private String masterCategoryId;
	private String masterCategoryName;
	private String processFlow;
	
	public String getMasterCategoryId() {
		return masterCategoryId;
	}
	public void setMasterCategoryId(String masterCategoryId) {
		this.masterCategoryId = masterCategoryId;
	}
	public String getMasterCategoryName() {
		return masterCategoryName;
	}
	public void setMasterCategoryName(String masterCategoryName) {
		this.masterCategoryName = masterCategoryName;
	}
	public String getProcessFlow() {
		return processFlow;
	}
	public void setProcessFlow(String processFlow) {
		this.processFlow = processFlow;
	}	
}
