package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@CustomQueriesMapper
public interface DeviceDetailsCustomQueriesMapper {
    @Select("select VERSION_ as version,count(VERSION_) as count from RL_MD_DEVICE_DETAILS group by VERSION_")
    List<DeviceInfoResult> getDetailsByVersion();

    @Select("select BRAND_ as brand,count(BRAND_) as count from RL_MD_DEVICE_DETAILS group by BRAND_")
    List<DeviceBrandInfoResult> getDetailsByBrand();

    @Select("select RLAPP_VERSION_ as version,count(RLAPP_VERSION_) as count from RL_MD_DEVICE_DETAILS group by RLAPP_VERSION_")
    List<DeviceVersionInfoResult> getDetailsByAppVersion();

}
