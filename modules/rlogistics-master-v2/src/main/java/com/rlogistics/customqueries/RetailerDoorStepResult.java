package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.RetailerDoorStepServiceRestData;

public class RetailerDoorStepResult extends RetailerDoorStepServiceRestData {

	private String brandName;

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
}
