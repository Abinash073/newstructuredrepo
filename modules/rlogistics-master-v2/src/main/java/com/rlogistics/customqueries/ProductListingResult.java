package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.ProductRestData;

public class ProductListingResult extends ProductRestData {
	
	private String subCategoryName;
	private String categoryId;
	private String categoryName;
	private String retailerAdvReplacementId;
	private String retailerId;
	private String packagingTypeBox;
	private String packagingTypeCover;
	private String brandName;
	
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getRetailerAdvReplacementId() {
		return retailerAdvReplacementId;
	}
	public void setRetailerAdvReplacementId(String retailerAdvReplacementId) {
		this.retailerAdvReplacementId = retailerAdvReplacementId;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getPackagingTypeBox() {
		return packagingTypeBox;
	}
	public void setPackagingTypeBox(String packagingTypeBox) {
		this.packagingTypeBox = packagingTypeBox;
	}
	public String getPackagingTypeCover() {
		return packagingTypeCover;
	}
	public void setPackagingTypeCover(String packagingTypeCover) {
		this.packagingTypeCover = packagingTypeCover;
	}
	
}
