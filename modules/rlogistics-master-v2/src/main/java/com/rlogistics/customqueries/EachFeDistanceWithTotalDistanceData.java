package com.rlogistics.customqueries;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class EachFeDistanceWithTotalDistanceData {
    private List<EachFeDistanceDetails> details = new ArrayList<>();
    private BigDecimal totalDistance;
    private String dateTime = DateTime.now(DateTimeZone.forID("Asia/Kolkata")).toString(DateTimeFormat.forPattern("dd/MM/yyyy"));

    public List<EachFeDistanceDetails> getDetails() {
        return details;
    }

    public void setDetails(List<EachFeDistanceDetails> details) {
        this.details = details;
    }

    public BigDecimal getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(BigDecimal totalDistance) {
        this.totalDistance = totalDistance;
    }


    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}

