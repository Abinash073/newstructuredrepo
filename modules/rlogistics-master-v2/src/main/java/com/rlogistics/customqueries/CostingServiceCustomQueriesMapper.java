package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface CostingServiceCustomQueriesMapper {

	@Select("select c.id_ as id, c.name_ as name, c.code_ as code, r.id_ as retailerCostingServiceId, r.retailer_id_ as retailerId, r.cost_ as cost, r.status_ as status, r.costing_service_code_ as costingServiceCode from RL_MD_COSTING_SERVICE c LEFT JOIN RL_MD_RETAILER_COSTING_SERVICE r ON (c.code_ = r.costing_service_code_ and r.retailer_id_ = #{retailerId})")
	List<CostingServiceResult> getCostingServices(@Param("retailerId") String retailerId);
	
	@SelectProvider(type=CostingServiceCustomQueryProvider.class,method="provideCostingActivityMapping")
	List<CostingActivityMappingResult> getCostingActivityMapping(@Param("categoryId") String categoryId,@Param("serviceId") String serviceId,@Param("activityId") String activityId,@Param("firstResult") String firstResult,@Param("maxResults") String maxResults);
	
	@SelectProvider(type=CostingServiceCustomQueryProvider.class,method="provideCostingActivityMappingCount")
	ResultCount getCostingActivityMappingCount(@Param("categoryId") String categoryId,@Param("serviceId") String serviceId,@Param("activityId") String activityId);
	
	@SelectProvider(type=CostingServiceCustomQueryProvider.class,method="provideRetailerCostingActivityMapping")
	List<RetailerCostingActivityMappingResult> getRetailerCostingActivityMapping(@Param("retailerId") String retailerId,@Param("categoryId") String categoryId,@Param("serviceId") String serviceId,@Param("activityId") String activityId,@Param("firstResult") String firstResult,@Param("maxResults") String maxResults);
	
	@SelectProvider(type=CostingServiceCustomQueryProvider.class,method="provideRetailerCostingActivityMappingCount")
	ResultCount getRetailerCostingActivityMappingCount(@Param("retailerId") String retailerId, @Param("categoryId") String categoryId,@Param("serviceId") String serviceId,@Param("activityId") String activityId);
}
