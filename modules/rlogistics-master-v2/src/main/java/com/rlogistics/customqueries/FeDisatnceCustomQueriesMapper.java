package com.rlogistics.customqueries;

import com.rlogistics.master.CustomQueriesMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@CustomQueriesMapper
public interface FeDisatnceCustomQueriesMapper {
    @SelectProvider(type = FeDistanceCustomQueriesProvider.class, method = "provideFeDistanceQuery")
    List<TotalDistance> getTotalDistance(@Param("feId") String feId, @Param("date") String date, @Param("startDate") String startDate, @Param("endDate") String endDate);

    @SelectProvider(type = FeDistanceCustomQueriesProvider.class, method = "getLastFeDistanceData")
    LastDistanceDetails getLastDistance(@Param("feName") String feName, @Param("date") String date);

    @SelectProvider(type = FeDistanceCustomQueriesProvider.class, method = "getDistancePerFe")
    List<EachFeDistanceDetails> getEachFeDistance(@Param("feName") String feName, @Param("startDate") String startDate, @Param("endDate") String endDate);

    @SelectProvider(type = FeDistanceCustomQueriesProvider.class, method = "feDistanceReport")
    List<FeDistanceReportData> getFeDistance(@Param("feName") String feName, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("city") String city);
}
