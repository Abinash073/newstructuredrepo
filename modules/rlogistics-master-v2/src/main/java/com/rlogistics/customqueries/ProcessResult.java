package com.rlogistics.customqueries;

public class ProcessResult {

	String processId;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}
}
