package com.rlogistics.customqueries;

import com.rlogistics.master.identity.rest.PackagingMaterialInventoryRestData;

public class PackagingInventoryResult extends PackagingMaterialInventoryRestData {

	private String packagingTypeName;
	private int packagingType;
	private String locationName;
	private String locationCode;
	private String city;
	private String firstName;
	private String lastName;
	private String email;
	private String imeiNo;
	
	public String getImeiNo() {
		return imeiNo;
	}
	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}
	public String getPackagingTypeName() {
		return packagingTypeName;
	}
	public void setPackagingTypeName(String packagingTypeName) {
		this.packagingTypeName = packagingTypeName;
	}
	public int getPackagingType() {
		return packagingType;
	}
	public void setPackagingType(int packagingType) {
		this.packagingType = packagingType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}
