package com.rlogistics.customqueries;

import java.util.Map;

public class MisReportProvider {
    //DUNN HATE ME!THY MADE ME DO THIS :(
    private final String dropTempTable = "drop temporary table if exists retailerData, cityData, ticketNoData;";
    private final String retailerTempTable = "create temporary table retailerData (\n" +
            "  TEXT_         varchar(50),\n" +
            "  PROC_INST_ID_ int\n" +
            ") select TEXT_, PROC_INST_ID_\n" +
            "  from ACT_HI_VARINST\n" +
            "  where NAME_ = 'retailer'\n" +
            "    and TEXT_ in (select NAME_ from RL_MD_RETAILER);";
    private final String retailerTempTableIndex = "create index retailerData_idx_index\n" +
            "  on retailerData (`PROC_INST_ID_`, `TEXT_`);";
    private final String ticketNoTempTableWithIndex = "create temporary table ticketNoData (\n" +
            "  TEXT_         varchar(50),\n" +
            "  PROC_INST_ID_ int\n" +
            ")select TEXT_, PROC_INST_ID_\n" +
            " from ACT_HI_VARINST\n" +
            " where NAME_ = 'rlTicketNo'\n" +
            "   and TEXT_ is not null;\n" +
            "\n" +
            "\n" +
            "ALTER table `ticketNoData`\n" +
            "  ADD INDEX `ticketNoData_idx_id`(`PROC_INST_ID_`, `TEXT_`);";
    private final String cityData = "create temporary table cityData (\n" +
            "  TEXT_         varchar(50),\n" +
            "  PROC_INST_ID_ int\n" +
            ") select TEXT_, PROC_INST_ID_\n" +
            "  from ACT_HI_VARINST\n" +
            "  where NAME_ = 'city'\n" +
            "    AND TEXT_ in (select RL_MD_CITY.NAME_ from RL_MD_CITY);\n" +
            "\n" +
            "ALTER TABLE `cityData`\n" +
            "  ADD INDEX `citydata_idx_id_text` (`PROC_INST_ID_`, `TEXT_`);";
    private final String mainQueryWithOutDate = "select a.TEXT_                               as ticketNo,\n" +
            "       b.TEXT_                               as city,\n" +
            "       c.PROC_INST_ID_                       as processId,\n" +
            "       d.TEXT_                               as retailer,\n" +
            "       c.date_format_start_time              as startDate,\n" +
            "       coalesce(date_format_end_time, 'N/A') as endDate\n" +
            "\n" +
            "FROM ticketNoData as a\n" +
            "       LEFT JOIN cityData as b ON a.PROC_INST_ID_ = b.PROC_INST_ID_\n" +
            "       LEFT JOIN ACT_HI_PROCINST as c ON a.PROC_INST_ID_ = c.PROC_INST_ID_\n" +
            "       LEFT JOIN retailerData as d on a.PROC_INST_ID_ = d.PROC_INST_ID_\n" +
            "#where c.date_format_start_time between '2018/06/05' and '2018/06/06'\n" +
            "\n" +
            "    # GROUP BY cityData.TEXT_,\n" +
            "    #          retailerData.TEXT_,\n" +
            "    #          ACT_HI_PROCINST.date_format_time\n" +
            "ORDER BY NULL;";
    private final String mainQueryWithDate = "select a.TEXT_                               as ticketNo,\n" +
            "       b.TEXT_                               as city,\n" +
            "       c.PROC_INST_ID_                       as processId,\n" +
            "       d.TEXT_                               as retailer,\n" +
            "       c.date_format_start_time              as startDate,\n" +
            "       coalesce(date_format_end_time, 'N/A') as endDate\n" +
            "\n" +
            "FROM ticketNoData as a\n" +
            "       LEFT JOIN cityData as b ON a.PROC_INST_ID_ = b.PROC_INST_ID_\n" +
            "       LEFT JOIN ACT_HI_PROCINST as c ON a.PROC_INST_ID_ = c.PROC_INST_ID_\n" +
            "       LEFT JOIN retailerData as d on a.PROC_INST_ID_ = d.PROC_INST_ID_\n" +
            "where c.date_format_start_time between '#{startDate}' and '#{endDate}'\n" +
            "\n" +
            "ORDER BY NULL;";

    public String getMisReportQuery(Map<String, String> param) {

        StringBuffer buffer = new StringBuffer();

        buffer.append(dropTempTable)
                .append(retailerTempTable)
                .append(retailerTempTableIndex)
                .append(ticketNoTempTableWithIndex)
                .append(cityData)
                .append(isEmpty(param) ? mainQueryWithOutDate : mainQueryWithDate);


        return isEmpty(param) ? buffer.toString() : changeDate(buffer, param.get("startTime"), param.get("endTime"));

    }

    private boolean isEmpty(Map<String, String> param) {
        String startDate = param.get("startDate");
        String endDate = param.get("endDate");
        return startDate.isEmpty() || endDate.isEmpty() || startDate.equals("null") || endDate.equals("null") || startDate == null || endDate == null;
    }

    public String changeDate(StringBuffer queryBuffer, String startTime, String endTime) {
        final String ifStartDateNotPresent = "(SELECT MIN(END_TIME_) from ACT_HI_PROCINST)";
        final String ifEndDateNotPresent = "(SELECT MAX(START_TIME_) from ACT_HI_PROCINST)";


        return String.valueOf(queryBuffer)
                .replaceAll("\\#\\{([^}]*startTime?)}", startTime.equals("") ? ifStartDateNotPresent : "'" + startTime + "'")
                .replaceAll("\\#\\{([^}]*endTime?)}", endTime.equals("") ? ifEndDateNotPresent : "'" + endTime + "'")
                .trim();

    }
}
