package com.rlogistics.customqueries;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;

//@Slf4j
public class ExotelLogCustomQueryProvider {
    public String provideExotelLogQuery(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("SELECT p.PROCESS_ID_ as processId,p.CUSTOMER_NUMBER_ as customerNumber,h.FIRST_ as assignee,p.EXOTEL_NUMBER_ as exotelNumber,p.RECORDING_URL_ as recordingUrl, p.STATUS_ as callStatus, p.TICKET_NUMBER_ as ticketNumber, p.CITY_ as city,p.DATE_CREATED_ as date from RL_MD_EXOTEL_LOG p inner join RL_MD_USER h WHERE 1=1 and p.ASSIGNEE_=h.EMAIL_");
        if (params.containsKey("processId") && !params.get("processId").equals("")) {
            buffer.append(" ");
            buffer.append("and PROCESS_ID_=#{processID}");
        }

        if (params.containsKey("customerNumber") && !params.get("customerNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and CUSTOMER_NUMBER_=#{customerNumber}");
        }

        if (params.containsKey("assignee") && !params.get("assignee").equals("")) {
            buffer.append(" ");
            buffer.append("and ASSIGNEE_=#{assignee}");
        }

        if (params.containsKey("exotelNumber") && !params.get("exotelNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and EXOTEL_NUMBER_=#{exotelNumber}");
        }
        if (params.containsKey("callStatus") && !params.get("callStatus").equals("")) {
            buffer.append(" ");
            buffer.append("and p.STATUS_=#{callStatus}");
        }

        if (params.containsKey("firstDate") && !params.get("firstDate").equals("") && params.containsKey("lastDate") && !params.get("lastDate").equals("")) {
            //TODO TIME FILTER
            buffer.append(" ");
            buffer.append("and DATE(p.DATE_CREATED_) between #{firstDate} and #{lastDate}");
        }

        if (params.containsKey("ticketNumber") && !params.get("ticketNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and TICKET_NUMBER_=#{ticketNumber}");

        }

        if (params.containsKey("city") && !params.get("city").equals("")) {
            buffer.append(" ");
            buffer.append("and CITY_=#{city}");

        }
        if (params.containsKey("first") && params.containsKey("last") && !params.get("first").equals("") && !params.get("last").equals("")) {
            buffer.append(" ");
            buffer.append("limit " + Integer.valueOf(params.get("first")) + "," + Integer.valueOf(params.get("last")));
        }
        // log.debug(buffer.toString());
        return buffer.toString();
    }

    public String provideExotelLogQueryCount(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("SELECT count(*) as totalRecords from RL_MD_EXOTEL_LOG p WHERE 1=1");
        if (params.containsKey("processId") && !params.get("processId").equals("")) {
            buffer.append(" ");
            buffer.append("and PROCESS_ID_=#{processID}");
        }

        if (params.containsKey("customerNumber") && !params.get("customerNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and CUSTOMER_NUMBER_=#{customerNumber}");
        }

        if (params.containsKey("assignee") && !params.get("assignee").equals("")) {
            buffer.append(" ");
            buffer.append("and ASSIGNEE_=#{assignee}");
        }

        if (params.containsKey("exotelNumber") && !params.get("exotelNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and EXOTEL_NUMBER_=#{exotelNumber}");
        }

        if (params.containsKey("callStatus") && !params.get("callStatus").equals("")) {
            buffer.append(" ");
            buffer.append("and p.STATUS_=#{callStatus}");
        }

        if (params.containsKey("date") && !params.get("date").equals("")) {
            //TODO TIME FILTER
            buffer.append(" ");
            buffer.append("and DATE_CREATED_ ");
        }

        if (params.containsKey("firstDate") && !params.get("firstDate").equals("") && params.containsKey("lastDate") && !params.get("lastDate").equals("")) {
            buffer.append(" ");
            buffer.append("and DATE(p.DATE_CREATED_) between #{firstDate} and #{lastDate}");
        }

        if (params.containsKey("ticketNumber") && !params.get("ticketNumber").equals("")) {
            buffer.append(" ");
            buffer.append("and TICKET_NUMBER_=#{ticketNumber}");

        }

        if (params.containsKey("city") && !params.get("city").equals("")) {
            buffer.append(" ");
            buffer.append("and CITY_=#{city}");

        }
        // log.debug(buffer.toString());
        return buffer.toString();
    }
}
