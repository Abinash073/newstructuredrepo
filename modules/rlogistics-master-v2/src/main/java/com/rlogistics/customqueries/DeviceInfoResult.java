package com.rlogistics.customqueries;

public class DeviceInfoResult {
    private String version;
    private int count;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
