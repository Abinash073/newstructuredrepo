	package com.rlogistics.customqueries;

	public class FeLocationTransResult {
		private String id;
		private String feName;
		private String latitude;
		private String longitude;
		private String date;

		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public String getFeName() {
			return feName;
		}
		
		public void setFeName(String feName) {
			this.feName = feName;
		}
		
		public String getLatitude() {
			return latitude;
		}
		
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		
		public String getLongitude() {
			return longitude;
		}
		
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		
		public String getDate() {
			return date;
		}
		
		public void setDate(String date) {
			this.date = date;
		}

	}
