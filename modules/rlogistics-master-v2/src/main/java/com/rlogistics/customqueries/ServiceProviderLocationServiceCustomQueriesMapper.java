package com.rlogistics.customqueries;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.rlogistics.master.CustomQueriesMapper;

@CustomQueriesMapper
public interface ServiceProviderLocationServiceCustomQueriesMapper {
	
	@Select("SELECT DISTINCT pc.id_ as categoryId, pc.name_ as categoryName from RL_MD_PRODUCT_CATEGORY pc INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE s ON (pc.id_ = s.category_id_) where s.service_provider_location_id_ = #{serviceProviderLocationId}")
	List<ServiceProviderLocationServiceCustomQueryCategoryResult> findServicesByCategory(@Param("serviceProviderLocationId") String serviceProviderLocationId);

	@Select("SELECT DISTINCT ps.id_ as subCategoryId, ps.name_ as subCategoryName from RL_MD_PRODUCT_SUB_CATEGORY ps INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE s ON (ps.id_ = s.sub_category_id_) where s.service_provider_location_id_ = #{serviceProviderLocationId} and s.category_id_ = #{categoryId}")
	List<ServiceProviderLocationServiceCustomQuerySubCategoryResult> findServicesBySubCategory(@Param("serviceProviderLocationId") String serviceProviderLocationId, @Param("categoryId") String categoryId);
	
	@Select("SELECT DISTINCT b.id_ as brandId, b.name_ as brandName from RL_MD_BRAND b INNER JOIN RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE s ON (b.id_ = s.brand_id_) where s.service_provider_location_id_ = #{serviceProviderLocationId}")
	List<ServiceProviderLocationServiceCustomQueryBrandResult> findServicesByBrand(@Param("serviceProviderLocationId") String serviceProviderLocationId);
}
