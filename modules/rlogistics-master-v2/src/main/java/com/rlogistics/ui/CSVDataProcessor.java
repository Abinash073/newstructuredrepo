package com.rlogistics.ui;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CSVDataProcessor {
    private static Logger log = LoggerFactory.getLogger(CSVDataProcessor.class);

    protected static String[] parseLine(String line, int lineNo) {
        try {
            ArrayList<String> fields = null;

            if ((line == null) || (line.length() == 0))
                return null;

            if (line.charAt(0) == '#')
                return null;

            if (line.charAt(0) == '!') {
                Matcher matcher = Pattern.compile("^!([^\\s]+)\\s*=\\s*(.+)$").matcher(line);

                if (matcher.matches()) {
                    return new String[]{matcher.group(1), matcher.group(2)};
                } else {
                    return null;
                }
            }

            fields = new ArrayList<String>();

            int current = 0;
            int next = 0;
            boolean done = false;
            while (!done) {
                int begin = 0;
                int end = 0;

                if (line.charAt(current) == '"') {
                    next = line.indexOf('"', current + 1);

                    if (next == -1)
                        throw new RuntimeException("Unhandled condition -- no matching quote for quote at line "
                                + lineNo + ", col " + current);

                    begin = current + 1;
                    end = next;

                    if ((next < line.length() - 1) && (line.charAt(next + 1) != ',')) {
                        int nextComma = line.indexOf(',', next);

                        if (nextComma != -1) {
                            next = nextComma;
                        }
                    } else {
                        next++;
                    }
                } else {
                    next = line.indexOf(',', current);
                    begin = current;
                    end = next;
                }

                String field = end == -1 ? line.substring(begin) : line.substring(begin, end);

                if (field.equals("null"))
                    field = null;
                fields.add(field);

                current = next >= 0 ? next + 1 : line.length();

                done = (current >= line.length());
            }

            return fields.toArray(new String[0]);
        } catch (Exception ex) {
            throw new RuntimeException("Exception while parsing line number " + lineNo + " '" + line + "'", ex);
        }
    }

    public static void main(String[] args) {
        String[] lines = {"hello,,world", ",hello,world", ",,hello,world", "hello,world,,"};
        int lineNo = 0;
        for (String line : lines) {
            String[] fields = parseLine(line, lineNo);

            for (String field : fields) {
                System.out.println(lineNo + ":" + field + ":");
            }

            ++lineNo;
        }
    }


}
