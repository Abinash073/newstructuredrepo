package com.rlogistics.activiti;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.spring.SpringProcessEngineConfiguration;

import com.rlogistics.master.identity.MasterdataService;
import com.rlogistics.master.identity.impl.MasterdataServiceImpl;
import com.rlogistics.master.identity.impl.interceptor.RLogisticsCommandContextFactory;

public abstract class RLogisticsSpringProcessEngineConfigurationBase extends SpringProcessEngineConfiguration {

	protected MasterdataService masterdataService = new MasterdataServiceImpl();

	@Override
	protected void initServices() {
		super.initServices();

		initService(masterdataService);
	}

	@Override
	protected void initSessionFactories(){
		super.initSessionFactories();
	}
	
	@Override
	protected void initCommandContextFactory() {

		setCommandContextFactory(new RLogisticsCommandContextFactory());

		super.initCommandContextFactory();
	}

	@Override
	public ProcessEngine buildProcessEngine() {
		/* Enhanced merge of super.buildProcessEngine() implementations*/
	    init();
		ProcessEngine processEngine = new RLogisticsProcessEngineImpl(this);
		ProcessEngines.setInitialized(true);
		autoDeployResources(processEngine);
		return processEngine;
	}

	public MasterdataService getMasterdataService() {
		return masterdataService;
	}

	public void setMasterdataService(MasterdataService masterdataService) {
		this.masterdataService = masterdataService;
	}
}
