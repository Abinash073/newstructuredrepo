package com.rlogistics.activiti;

import org.activiti.engine.impl.ProcessEngineImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.rlogistics.master.identity.MasterdataService;

public class RLogisticsProcessEngineImpl extends ProcessEngineImpl {

	protected MasterdataService masterdataService;

	public RLogisticsProcessEngineImpl(ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
		
		masterdataService = ((RLogisticsSpringProcessEngineConfiguration)processEngineConfiguration).getMasterdataService();
	}
	
	public MasterdataService getMasterdataService() {
		return masterdataService;
	}

	public void setMasterdataService(MasterdataService masterdataService) {
		this.masterdataService = masterdataService;
	}
}
