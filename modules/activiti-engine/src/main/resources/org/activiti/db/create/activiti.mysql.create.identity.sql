create table ACT_ID_GROUP (
    ID_ varchar(64),
    REV_ integer,
    NAME_ varchar(255),
    TYPE_ varchar(255),
    primary key (ID_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

create table ACT_ID_MEMBERSHIP (
    USER_ID_ varchar(64),
    GROUP_ID_ varchar(64),
    primary key (USER_ID_, GROUP_ID_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

create table ACT_ID_USER (
    ID_ varchar(64),
    REV_ integer,
    FIRST_ varchar(255),
    LAST_ varchar(255),
    EMAIL_ varchar(255),
    PWD_ varchar(255),
    PICTURE_ID_ varchar(64),
    primary key (ID_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

create table ACT_ID_INFO (
    ID_ varchar(64),
    REV_ integer,
    USER_ID_ varchar(64),
    TYPE_ varchar(64),
    KEY_ varchar(255),
    VALUE_ varchar(255),
    PASSWORD_ LONGBLOB,
    PARENT_ID_ varchar(255),
    primary key (ID_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table ACT_ID_MEMBERSHIP 
    add constraint ACT_FK_MEMB_GROUP 
    foreign key (GROUP_ID_) 
    references ACT_ID_GROUP (ID_);

alter table ACT_ID_MEMBERSHIP 
    add constraint ACT_FK_MEMB_USER 
    foreign key (USER_ID_) 
    references ACT_ID_USER (ID_);

create table RL_MD_DATASET (
    ID_ varchar(64),
    REV_ integer,
    NAME_ varchar(255),
    primary key (ID_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;
    
-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Table structure for table `RL_MD_ATTACHMENT`
--

DROP TABLE IF EXISTS `RL_MD_ATTACHMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_ATTACHMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(512) COLLATE utf8_bin NOT NULL,
  `KEY1_` varchar(128) COLLATE utf8_bin NOT NULL,
  `KEY2_` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `KEY3_` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_TYPE_` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `KEY1_` (`KEY1_`,`KEY2_`,`KEY3_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_BARCODE_CONFIGURATION`
--

DROP TABLE IF EXISTS `RL_MD_BARCODE_CONFIGURATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_BARCODE_CONFIGURATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `FILE_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_BARCODE_GENERATION`
--

DROP TABLE IF EXISTS `RL_MD_BARCODE_GENERATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_BARCODE_GENERATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STARTING_BARCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ENDING_BARCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STARTING_BARCODE_RANGE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ENDING_BARCODE_RANGE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ITEM_COUNT_` int(11) DEFAULT NULL,
  `PRINT_STATUS_` int(11) DEFAULT NULL,
  `GENERATED_DATE_` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_BARCODE_OFFSET`
--

DROP TABLE IF EXISTS `RL_MD_BARCODE_OFFSET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_BARCODE_OFFSET` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OFFSET_` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_BRAND`
--

DROP TABLE IF EXISTS `RL_MD_BRAND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_BRAND` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_BRAND_CODE` (`CODE_`(20)),
  UNIQUE KEY `UN_BRAND_NAME` (`NAME_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_CITY`
--

DROP TABLE IF EXISTS `RL_MD_CITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_CITY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_COSTING_ACTIVITY`
--

DROP TABLE IF EXISTS `RL_MD_COSTING_ACTIVITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_COSTING_ACTIVITY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_COSTING_ACTIVITY_MAPPING`
--

DROP TABLE IF EXISTS `RL_MD_COSTING_ACTIVITY_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_COSTING_ACTIVITY_MAPPING` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `COSTING_ACTIVITY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_COST_ACTIVTIY` (`COSTING_ACTIVITY_ID_`(20),`CATEGORY_ID_`(20),`SERVICE_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_COSTING_SERVICE`
--

DROP TABLE IF EXISTS `RL_MD_COSTING_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_COSTING_SERVICE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_DATASET`
--

DROP TABLE IF EXISTS `RL_MD_DATASET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_DATASET` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_DATASET_REVISION`
--

DROP TABLE IF EXISTS `RL_MD_DATASET_REVISION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_DATASET_REVISION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `DATASET_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DSVERSION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_DEVICE_INFO`
--

DROP TABLE IF EXISTS `RL_MD_DEVICE_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_DEVICE_INFO` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DEVICE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_DOA_FORM`
--

DROP TABLE IF EXISTS `RL_MD_DOA_FORM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_DOA_FORM` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `ATTACHMENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_DOA` (`CATEGORY_ID_`(20),`BRAND_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_FAQ`
--

DROP TABLE IF EXISTS `RL_MD_FAQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_FAQ` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUB_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FAQ_` text COLLATE utf8_bin,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_FEATURE`
--

DROP TABLE IF EXISTS `RL_MD_FEATURE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_FEATURE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MENU_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_` int(11) DEFAULT NULL,
  `LINK_` varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `LEVEL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HAS_SUB_MENU_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_LOCATION`
--

DROP TABLE IF EXISTS `RL_MD_LOCATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_LOCATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS1_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONTACT_PERSON_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_PINCODE` (`PINCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_LOCATION_PINCODES`
--

DROP TABLE IF EXISTS `RL_MD_LOCATION_PINCODES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_LOCATION_PINCODES` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_LOC_PINCODE` (`LOCATION_ID_`(20),`PINCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_LOCATION_ROLE`
--

DROP TABLE IF EXISTS `RL_MD_LOCATION_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_LOCATION_ROLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ROLE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USERS_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_MASTER_CATEGORY`
--

DROP TABLE IF EXISTS `RL_MD_MASTER_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_MASTER_CATEGORY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_FLOW_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_MENU`
--

DROP TABLE IF EXISTS `RL_MD_MENU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_MENU` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LINK_` varchar(400) COLLATE utf8_bin DEFAULT NULL,
  `ORDER_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PACKAGING_MATERIAL_INVENTORY`
--

DROP TABLE IF EXISTS `RL_MD_PACKAGING_MATERIAL_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PACKAGING_MATERIAL_INVENTORY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `LOCATION_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BARCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BARCODE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_` int(11) NOT NULL DEFAULT '0',
  `TICKET_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ADDITIONAL_DETAILS_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `PRINT_STATUS_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_INVENTORY` (`LOCATION_ID_`(20),`PACKAGING_TYPE_CODE_`(20),`BARCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`wildfly`@`localhost`*/ /*!50003 TRIGGER `PACKAGING_INVENTORY_TRIGGER` BEFORE UPDATE ON `RL_MD_PACKAGING_MATERIAL_INVENTORY`
 FOR EACH ROW BEGIN
INSERT INTO RL_MD_PACKAGING_MATERIAL_INVENTORY_LOG VALUES (UUID(),OLD.LOCATION_ID_,OLD.PACKAGING_TYPE_CODE_,OLD.BARCODE_,OLD.STATUS_,OLD.TICKET_NUMBER_,OLD.USER_ID_,OLD.ADDITIONAL_DETAILS_,NOW());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `RL_MD_PACKAGING_MATERIAL_INVENTORY_LOG`
--

DROP TABLE IF EXISTS `RL_MD_PACKAGING_MATERIAL_INVENTORY_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PACKAGING_MATERIAL_INVENTORY_LOG` (
  `ID_` varchar(64) NOT NULL,
  `LOCATION_ID_` varchar(64) DEFAULT NULL,
  `PACKAGING_TYPE_CODE_` varchar(64) DEFAULT NULL,
  `BARCODE_` varchar(255) DEFAULT NULL,
  `STATUS_` int(11) DEFAULT NULL,
  `TICKET_NUMBER_` varchar(255) DEFAULT NULL,
  `USER_ID_` varchar(64) DEFAULT NULL,
  `ADDITIONAL_DETAILS_` varchar(1023) DEFAULT NULL,
  `DATE_MODIFIED_` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PACKAGING_MATERIAL_MASTER`
--

DROP TABLE IF EXISTS `RL_MD_PACKAGING_MATERIAL_MASTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PACKAGING_MATERIAL_MASTER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `LOCATION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REORDER_LEVEL_` int(11) DEFAULT NULL,
  `MAXIMUM_QUANTITY_ALLOWED_` int(11) DEFAULT NULL,
  `AVAILABLE_QUANTITY_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_PACKAGING` (`LOCATION_ID_`(20),`PACKAGING_TYPE_CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PACKAGING_TYPE`
--

DROP TABLE IF EXISTS `RL_MD_PACKAGING_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PACKAGING_TYPE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `COST_` float DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` int(11) DEFAULT NULL,
  `VOLUMETRIC_SIZE_MIN_` float DEFAULT NULL,
  `VOLUMETRIC_SIZE_MAX_` float DEFAULT NULL,
  `LENGTH_MIN_` float DEFAULT NULL,
  `LENGTH_MAX_` float DEFAULT NULL,
  `HEIGHT_MIN_` float DEFAULT NULL,
  `HEIGHT_MAX_` float DEFAULT NULL,
  `BREADTH_MIN_` float DEFAULT NULL,
  `BREADTH_MAX_` float DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PROCESS_DEPLOYMENT`
--

DROP TABLE IF EXISTS `RL_MD_PROCESS_DEPLOYMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PROCESS_DEPLOYMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `DATASET_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DSVERSION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` int(255) DEFAULT NULL,
  `CREATED_ON_` int(11) DEFAULT NULL,
  `MODIFIED_BY_` int(255) DEFAULT NULL,
  `MODIFIED_ON_` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PRODUCT`
--

DROP TABLE IF EXISTS `RL_MD_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PRODUCT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` text COLLATE utf8_bin,
  `SUB_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODEL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_BOX_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_TYPE_COVER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VOLUMETRIC_WEIGHT_` double DEFAULT NULL,
  `APPROX_VALUE_` double DEFAULT NULL,
  `ACTUAL_WEIGHT_` double DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_NAME` (`RETAILER_ID_`(20),`NAME_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PRODUCT_CATEGORY`
--

DROP TABLE IF EXISTS `RL_MD_PRODUCT_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PRODUCT_CATEGORY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MASTER_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_PROD_CAT` (`NAME_`(64)),
  UNIQUE KEY `UN_CAT_CODE` (`CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_PRODUCT_SUB_CATEGORY`
--

DROP TABLE IF EXISTS `RL_MD_PRODUCT_SUB_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_PRODUCT_SUB_CATEGORY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SUB_CAT_CODE` (`CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RESET_TOKEN`
--

DROP TABLE IF EXISTS `RL_MD_RESET_TOKEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RESET_TOKEN` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TOKEN_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXPIRY_DATE_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS1_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TELEPHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ALT_TELEPHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_ADV_REPLACEMENT_PRODUCTS`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_ADV_REPLACEMENT_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_ADV_REPLACEMENT_PRODUCTS` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_CATEGORIES`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_CATEGORIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_CATEGORIES` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RET_CATEGORY` (`RETAILER_ID_`(20),`CATEGORY_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ALTERNATE_PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONTACT_PERSON_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AREA_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_CHEQUE` (`RETAILER_ID_`(20),`PINCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER_CITY`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER_CITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_CHEQUE_COLLECTION_CENTER_CITY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `CHEQUE_COLLECTION_CENTER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CITY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CITY_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_CITY` (`CITY_ID_`(20),`RETAILER_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_COSTING`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_COSTING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_COSTING` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_MATERIAL_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKAGING_MATERIAL_COST_` float DEFAULT NULL,
  `PACKAGING_COST_` float DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_COSTING_ACTIVITY`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_COSTING_ACTIVITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_COSTING_ACTIVITY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `COSTING_ACTIVITY_CODE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `COST_` float DEFAULT NULL,
  `STATUS_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RET_COST` (`COSTING_ACTIVITY_CODE_`(20),`RETAILER_ID_`(20),`SERVICE_ID_`(20),`CATEGORY_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_COSTING_SERVICE`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_COSTING_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_COSTING_SERVICE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `COSTING_SERVICE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `COST_` float DEFAULT NULL,
  `STATUS_` int(11) DEFAULT '1',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_DOOR_STEP_SERVICE`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_DOOR_STEP_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_DOOR_STEP_SERVICE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `IS_INSTALLATION_AVAILABLE_` int(11) DEFAULT NULL,
  `IS_REPAIR_AVAILABLE_` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_EMAIL_CONFIGURATION`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_EMAIL_CONFIGURATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_EMAIL_CONFIGURATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HOST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USERNAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PORT_` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_EMAIL_TEMPLATE`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_EMAIL_TEMPLATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_EMAIL_TEMPLATE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEMPLATE_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `SETUP_SCRIPT_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_FAQ`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_FAQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_FAQ` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FAQ_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_OTHER`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_OTHER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_OTHER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_ALLOW_ONLY_A_S_P_` int(11) DEFAULT NULL,
  `ONLY_RETAILER_AUTH_FOR_PICK_N_DROP_` int(11) DEFAULT NULL,
  `ADV_PYMT_TO_SERVICE_CENTER_` int(11) DEFAULT NULL,
  `GET_APPROVAL_BEFORE_BUY_BACK_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_PACKING_COST`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_PACKING_COST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_PACKING_COST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PACKING_MATERIAL_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `MATERIAL_COST_` double DEFAULT NULL,
  `PACKING_COST_` double DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_PACK_COST` (`RETAILER_ID_`(20),`PACKING_MATERIAL_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_PACKING_INTRA_CITY`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_PACKING_INTRA_CITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_PACKING_INTRA_CITY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_INTRA_CITY` (`RETAILER_ID_`(20),`PRODUCT_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_PAYS_PACKING_COST`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_PAYS_PACKING_COST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_PAYS_PACKING_COST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_PAYS_PACK` (`RETAILER_ID_`(20),`CATEGORY_ID_`(20),`BRAND_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_PROCESS_FIELD`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_PROCESS_FIELD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_PROCESS_FIELD` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `RETAILER_FIELD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CANONICAL_FIELD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_REPLACEMENT_POLICY`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_REPLACEMENT_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_REPLACEMENT_POLICY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRODUCT_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_RETURN_OR_PICKUP_LOCATIONS`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_RETURN_OR_PICKUP_LOCATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_RETURN_OR_PICKUP_LOCATIONS` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_CODE_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_LOCATION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_ADDRESS_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_ALTERNATE_PHONE_NUMBER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RETURN_POLICY` (`SELLER_CODE_`(20),`SELLER_PINCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SELLER_CATEGORIES`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SELLER_CATEGORIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SELLER_CATEGORIES` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `SELLER_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SELLER` (`RETAILER_ID_`(20),`CATEGORY_ID_`(20),`SELLER_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SERVICEABLE_LOCATIONS`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SERVICEABLE_LOCATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SERVICEABLE_LOCATIONS` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RET_LOC` (`RETAILER_ID_`(20),`PINCODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SERVICES`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SERVICES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SERVICES` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RET_SERVICE` (`RETAILER_ID_`(20),`SERVICE_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SERVICES_COST`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SERVICES_COST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SERVICES_COST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `COST_` double DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SERVICE_PROVIDER`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SERVICE_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SERVICE_PROVIDER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_PROVIDER_LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUB_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RETAILER` (`RETAILER_ID_`(20),`SERVICE_PROVIDER_LOCATION_ID_`(20),`CATEGORY_ID_`(20),`BRAND_ID_`(20),`SUB_CATEGORY_ID_`(20),`CITY_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SERVICE_PROVIDER_PINCODE`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SERVICE_PROVIDER_PINCODE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SERVICE_PROVIDER_PINCODE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `RETAILER_SERVICE_PROVIDER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RET_PINCODE` (`RETAILER_ID_`(20),`PINCODE_`(20),`RETAILER_SERVICE_PROVIDER_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SMS_CONFIGURATION`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SMS_CONFIGURATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SMS_CONFIGURATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USERNAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HASH_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SENDER_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SMS_EMAIL_TEMPLATE`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SMS_EMAIL_TEMPLATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SMS_EMAIL_TEMPLATE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SMS_EMAIL_TRIGGER_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_TEMPLATE_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_TEMPLATE_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `SETUP_SCRIPT_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `PARAMS_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `SUBJECT_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RETAILER_SMS_EMAIL` (`SMS_EMAIL_TRIGGER_CODE_`(20),`RETAILER_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_RETAILER_SMS_EMAIL_TRIGGER_CONFIG`
--

DROP TABLE IF EXISTS `RL_MD_RETAILER_SMS_EMAIL_TRIGGER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_RETAILER_SMS_EMAIL_TRIGGER_CONFIG` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `RETAILER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SMS_EMAIL_TRIGGER_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_EMAIL_REQUIRED_` int(11) DEFAULT NULL,
  `IS_SMS_REQUIRED_` int(11) DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_RETAILER_CONFIG` (`RETAILER_ID_`(20),`SERVICE_ID_`(20),`SMS_EMAIL_TRIGGER_CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_ROLE`
--

DROP TABLE IF EXISTS `RL_MD_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_ROLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `DATASET_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DSVERSION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ROLE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_ROLE_ACCESS`
--

DROP TABLE IF EXISTS `RL_MD_ROLE_ACCESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_ROLE_ACCESS` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `ROLE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FEATURE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACCESS_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_ROLE_MASTER`
--

DROP TABLE IF EXISTS `RL_MD_ROLE_MASTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_ROLE_MASTER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SERVICE_MASTER`
--

DROP TABLE IF EXISTS `RL_MD_SERVICE_MASTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SERVICE_MASTER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CODE_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SERVICE_PROVIDER`
--

DROP TABLE IF EXISTS `RL_MD_SERVICE_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SERVICE_PROVIDER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(1023) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SERVICE_CODE` (`CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SERVICE_PROVIDER_LOCATION`
--

DROP TABLE IF EXISTS `RL_MD_SERVICE_PROVIDER_LOCATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SERVICE_PROVIDER_LOCATION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `SERVICE_PROVIDER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_PROVIDER_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS1_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ADDRESS2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CITY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PHONE1_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PHONE2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONTACT_PERSON_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PINCODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `AREA_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SERVICE` (`SERVICE_PROVIDER_CODE_`(20),`PINCODE_`(20),`CITY_ID_`(20),`AREA_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE`
--

DROP TABLE IF EXISTS `RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SERVICE_PROVIDER_LOCATION_SERVICE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `SERVICE_PROVIDER_LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUB_CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_BIZLOG_AUTHORIZED_` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `IS_AUTHORIZED_SERVICE_PROVIDER_` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `KIND_OF_SERVICE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SERVICE_PROVIDER` (`SERVICE_PROVIDER_LOCATION_ID_`(20),`CATEGORY_ID_`(20),`BRAND_ID_`(20)),
  UNIQUE KEY `UN_LOCATION_SERVICE` (`SERVICE_PROVIDER_LOCATION_ID_`(20),`CATEGORY_ID_`(20),`SUB_CATEGORY_ID_`(20),`BRAND_ID_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SMS_EMAIL_LOG`
--

DROP TABLE IF EXISTS `RL_MD_SMS_EMAIL_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SMS_EMAIL_LOG` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `PROCESS_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `TYPE_` int(11) DEFAULT NULL,
  `RECIPIENT_` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MESSAGE_` text COLLATE utf8_bin,
  `API_MESSAGE_` text COLLATE utf8_bin,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SMS_EMAIL_TEMPLATE`
--

DROP TABLE IF EXISTS `RL_MD_SMS_EMAIL_TEMPLATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SMS_EMAIL_TEMPLATE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `SMS_EMAIL_TRIGGER_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SMS_TEMPLATE_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_TEMPLATE_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `SETUP_SCRIPT_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `PARAMS_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `SUBJECT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_SMS_EMAIL` (`SMS_EMAIL_TRIGGER_CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SMS_EMAIL_TRIGGER`
--

DROP TABLE IF EXISTS `RL_MD_SMS_EMAIL_TRIGGER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SMS_EMAIL_TRIGGER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `CODE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_SMS_EMAIL_TRIGGER_CONFIG`
--

DROP TABLE IF EXISTS `RL_MD_SMS_EMAIL_TRIGGER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_SMS_EMAIL_TRIGGER_CONFIG` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `SMS_EMAIL_TRIGGER_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SERVICE_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_EMAIL_REQUIRED_` int(11) DEFAULT NULL,
  `IS_SMS_REQUIRED_` int(11) DEFAULT NULL,
  `CATEGORY_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `UN_CONFIG` (`SERVICE_ID_`(20),`SMS_EMAIL_TRIGGER_CODE_`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_TICKET_COLUMNS`
--

DROP TABLE IF EXISTS `RL_MD_TICKET_COLUMNS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_TICKET_COLUMNS` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `FIELD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_URL_HANDLER`
--

DROP TABLE IF EXISTS `RL_MD_URL_HANDLER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_URL_HANDLER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `CONTEXT_DEFINER_` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `HANDLER_` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_MD_USER`
--

DROP TABLE IF EXISTS `RL_MD_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_USER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PHONE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ROLE_CODE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LOCATION_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `STATUS_` int(11) NOT NULL DEFAULT '1',
  `RETAILER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `EMAIL_` (`EMAIL_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;


CREATE TABLE `RL_MD_DELETED_TICKET` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `PROCESS_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `RL_TICKET_NO_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONSUMER_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DELETED_DATE_` date DEFAULT NULL,
  `DELETED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `BRAND_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `RETAILER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DELETED_VALUES_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Table structure for table `RL_MD_USER_TASK_REPORT_TABLE`
--

DROP TABLE IF EXISTS `RL_MD_USER_TASK_REPORT_TABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_MD_USER_TASK_REPORT_TABLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `PROCESS_DEPLOYMENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_TASK_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REPORT_TABLE_` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RL_RPT_VARIABLE`
--

DROP TABLE IF EXISTS `RL_RPT_VARIABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RL_RPT_VARIABLE` (
  `ID_` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_ON_` date DEFAULT NULL,
  `MODIFIED_BY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MODIFIED_ON_` date DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-02 11:56:29

