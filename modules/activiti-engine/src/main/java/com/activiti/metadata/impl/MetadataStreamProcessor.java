package com.activiti.metadata.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessRole;

public class MetadataStreamProcessor {
	public static void processUpload(String dataset,String dsVersion,InputStream is) {
		MetadataService metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		
		try {
			BufferedReader bis = new BufferedReader(new InputStreamReader(is));
			
			String line = null;
			int lineNo = 0;
			while((line = bis.readLine()) != null){
				lineNo++;
				
				String[] fields = parseLine(line,lineNo);
				
				if(fields == null || fields.length == 0) continue;
				
				String op = fields[1];
				
				System.out.println("PROCESSING:" + lineNo + ":" + line);
				for(String f:fields){
					System.out.println("------------" + f);
				}
				if(fields[0].equals(DATASET)){
					dataset = fields[1];
				} else if(fields[0].equals(ROLE)){
					String name = fields.length >= 3 ? fields[2] : null;
					String parentRoleId = fields.length >= 4 ? fields[3] : null;
					
					if(op.equals(INSERT) || op.equals(UPDATE)){
						ProcessRole role = metadataService.newProcessRole(dataset,dsVersion);
						
						role.setName(name);
						role.setParentRoleId(parentRoleId);
						
						metadataService.saveProcessRole(role);
					} else if(op.equals(DELETE)){
						metadataService.deleteProcessRole(dataset,dsVersion,name);
					}					
				}
			}
			
		} catch(IOException ex) {
			throw new ActivitiException("Failed to read uploaded content:" + ex.getMessage(), ex);
		}
	}
	
	private static String[] parseLine(String line, int lineNo) {
		try {
		ArrayList<String> fields = null;
		
		if(line.charAt(0) == '#') return null;
		
		if(line.charAt(0) == '!'){
			Matcher matcher = Pattern.compile("^!([^\\s]+)\\s*=\\s*(.+)$").matcher(line);
			
			if(matcher.matches())
			{
				return new String[]{matcher.group(1),matcher.group(2)};
			} else {
				return null;
			}
		}
		
		fields = new ArrayList<String>();
		
		int current = 0;
		int next = 0;
		boolean done = false;
		while(!done){
			int begin = 0;
			int end = 0;
			
			if(line.charAt(current) == '"'){
				next = line.indexOf('"',current+1);
				
				if(next == -1)
					throw new ActivitiException("Unhandled condition -- no matching quote for quote at line " + lineNo + ", col " + current);
				
				begin = current + 1;
				end = next;
				
				if((next < line.length() - 1) && (line.charAt(next+1) != ',')){
					int nextComma = line.indexOf(',',next);
					
					if(nextComma != -1){
						next = nextComma;
					}
				} else {
						next++;
				}
			} else {
				next = line.indexOf(',',current+1);
				begin = current;
				end = next;
			}
			
			String field = end == -1 ? line.substring(begin) : line.substring(begin, end);
			
			if(field.equals("null")) field = null;
			fields.add(field);
			
			current = next > 0 ? next+1 : line.length();
			
			done = (current >= line.length());
		}
		
		return fields.toArray(new String[0]);
		} catch(Exception ex) {
			throw new RuntimeException("Exception while parsing line number " + lineNo + " '" + line + "'",ex);
		}
	}

	public static void main(String[] args) {
		String[] fields = parseLine("USER,INSERT,bangalore1,1,welcome,bangalore1@gmail.com,9898989898", 1);
		for(String f:fields){
			System.out.println("------------" + f);
		}
	}
	
	public static final String DATASET= "DATASET";
	public static final String ROLE = "ROLE";
	public static final String LOCATION = "LOCATION";
	public static final String LOCATION_ROLE = "LOCATION_ROLE";
	public static final String USER = "USER";

	public static final String INSERT = "INSERT";
	public static final String UPDATE = "UPDATE";
	public static final String DELETE = "DELETE";
}
