package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class SaveProcessDatasetRevisionCmd implements Command<Void>, Serializable {

	private static final long serialVersionUID = 1L;
	protected ProcessDatasetRevision role;

	public SaveProcessDatasetRevisionCmd(ProcessDatasetRevision role) {
		this.role = role;
	}

	public Void execute(CommandContext commandContext) {
		if (role == null) {
			throw new ActivitiIllegalArgumentException("DatasetRevision is null");
		}
		if (commandContext.getProcessDatasetRevisionIdentityManager().isNewDatasetRevision(role)) {
			commandContext.getProcessDatasetRevisionIdentityManager().insertDatasetRevision(role);
		} else {
			commandContext.getProcessDatasetRevisionIdentityManager().updateDatasetRevision(role);
		}

		return null;
	}
}
