package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.identity.rlogistics.ProcessDeploymentQuery;
import org.activiti.engine.impl.Page;

public interface ProcessDeploymentIdentityManager {

	ProcessDeploymentQuery createNewProcessDeploymentQuery();

	long findProcessDeploymentCountByQueryCriteria(ProcessDeploymentQueryImpl query);
	List<ProcessDeployment> findProcessDeploymentByQueryCriteria(ProcessDeploymentQueryImpl query, Page page);

	ProcessDeployment findProcessDeploymentById(String id);	  

	void deleteDeployment(String id);

	boolean isNewDeployment(ProcessDeployment dataset);

	void insertDeployment(ProcessDeployment dataset);

	void updateDeployment(ProcessDeployment dataset);

	ProcessDeployment createNewProcessDeployment(String dataset,String dsVersion);
}
