package org.activiti.engine.identity.rlogistics;

import org.activiti.engine.query.Query;

public interface ProcessRoleQuery extends Query<ProcessRoleQuery,ProcessRole>{
	ProcessRoleQuery id(String id);
	ProcessRoleQuery roleId(String roleId);
	ProcessRoleQuery dataset(String dataset);
	ProcessRoleQuery dsVersion(String dsVersion);
	ProcessRoleQuery orderByRoleId();
	ProcessRoleQuery orderByName();
	ProcessRoleQuery orderByParentRoleId();
}
