package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessRoleCmd implements Command<ProcessRole>, Serializable {

	private static final long serialVersionUID = 1L;

	protected String dataset;
	protected String dsVersion;

	public CreateProcessRoleCmd(String dataset,String dsVersion) {
		this.dataset = dataset;
		this.dsVersion = dsVersion;
	}

	public ProcessRole execute(CommandContext commandContext) {
		return commandContext.getProcessRoleIdentityManager().createNewProcessRole(dataset,dsVersion);
	}
}
