package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevisionQuery;
import org.activiti.engine.impl.AbstractQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class ProcessDatasetRevisionQueryImpl extends AbstractQuery<ProcessDatasetRevisionQuery, ProcessDatasetRevision> implements ProcessDatasetRevisionQuery {

	private static final long serialVersionUID = 1L;
	protected String id;
	protected String dataset;
	protected String dsVersion;

	public ProcessDatasetRevisionQueryImpl() {
	}

	public ProcessDatasetRevisionQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public ProcessDatasetRevisionQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	@Override
	public ProcessDatasetRevisionQuery id(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		return this;
	}

	@Override
	public ProcessDatasetRevisionQuery dataset(String dataset) {
		if (dataset == null) {
			throw new ActivitiIllegalArgumentException("Provided dataset is null");
		}
		this.dataset = dataset;
		return this;
	}
	
	@Override
	public ProcessDatasetRevisionQuery dsVersion(String dsVersion) {
		if (dsVersion == null) {
			throw new ActivitiIllegalArgumentException("Provided dsVersion is null");
		}
		this.dsVersion = dsVersion;
		return this;
	}
	
	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getProcessDatasetRevisionIdentityManager().findProcessDatasetRevisionCountByQueryCriteria(this);
	}

	@Override
	public List<ProcessDatasetRevision> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getProcessDatasetRevisionIdentityManager().findProcessDatasetRevisionByQueryCriteria(this, page);
	}

	@Override
	public ProcessDatasetRevisionQuery orderByDsVersion() {
		return orderBy(ProcessDatasetRevisionQueryProperty.DS_VERSION);
	}
	
	@Override
	public ProcessDatasetRevisionQuery orderByDataset() {
		return orderBy(ProcessDatasetRevisionQueryProperty.DATASET);
	}
	
	public String getId() {
		return id;
	}
	
	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getDsVersion() {
		return dsVersion;
	}

	public void setDsVersion(String dsVersion) {
		this.dsVersion = dsVersion;
	}
}
