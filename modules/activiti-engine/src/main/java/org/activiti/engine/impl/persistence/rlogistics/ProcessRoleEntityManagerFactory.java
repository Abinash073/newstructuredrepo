package org.activiti.engine.impl.persistence.rlogistics;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.rlogistics.ProcessRoleEntityManager;
import org.activiti.engine.impl.rlogistics.ProcessRoleIdentityManager;

public class ProcessRoleEntityManagerFactory implements SessionFactory
{
	public Class<?> getSessionType() {
		return ProcessRoleIdentityManager.class;
	}

	public Session openSession() {
		return new ProcessRoleEntityManager();
	}
}