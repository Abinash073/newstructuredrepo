package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDatasetCmd implements Command<ProcessDataset>, Serializable {

	private static final long serialVersionUID = 1L;

	public CreateProcessDatasetCmd() {
	}

	public ProcessDataset execute(CommandContext commandContext) {
		return commandContext.getProcessDatasetIdentityManager().createNewProcessDataset();
	}
}
