package org.activiti.engine.impl.persistence.entity.rlogistics;

import java.util.List;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.identity.rlogistics.ProcessDeploymentQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.rlogistics.ProcessDeploymentIdentityManager;
import org.activiti.engine.impl.rlogistics.ProcessDeploymentQueryImpl;

public class ProcessDeploymentEntityManager extends AbstractManager implements ProcessDeploymentIdentityManager {

	@Override
	public ProcessDeploymentQuery createNewProcessDeploymentQuery() {
		return new ProcessDeploymentQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
	}

	@Override
	public long findProcessDeploymentCountByQueryCriteria(ProcessDeploymentQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectProcessDeploymentCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessDeployment> findProcessDeploymentByQueryCriteria(ProcessDeploymentQueryImpl query, Page page) {
	    return getDbSqlSession().selectList("selectProcessDeploymentByQueryCriteria", query, page);
	}
	
	public void insertDeployment(ProcessDeployment dataset) {
		getDbSqlSession().insert((PersistentObject) dataset);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, dataset));
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, dataset));
		}
	}

	public void updateDeployment(ProcessDeployment updatedDeployment) {
		CommandContext commandContext = Context.getCommandContext();
		DbSqlSession dbSqlSession = commandContext.getDbSqlSession();
		dbSqlSession.update((PersistentObject) updatedDeployment);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_UPDATED, updatedDeployment));
		}
	}

	@Override
	public void deleteDeployment(String id) {
		ProcessDeploymentEntity deployment = (ProcessDeploymentEntity) findProcessDeploymentById(id);
		if (deployment != null) {
			deployment.delete();

			if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
				getProcessEngineConfiguration().getEventDispatcher()
						.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, deployment));
			}
		}
	}

	@Override
	public ProcessDeployment findProcessDeploymentById(String id) {
		ProcessDeploymentQueryImpl query = new ProcessDeploymentQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
		
		query.id(id);
		
	    return (ProcessDeploymentEntity) query.executeSingleResult(Context.getCommandContext());
	}

	@Override
	public boolean isNewDeployment(ProcessDeployment dataset) {
		return ((ProcessDeploymentEntity) dataset).getRevision() == 0;
	}

	@Override
	public ProcessDeployment createNewProcessDeployment(String dataset,String dsVersion) {
	    ProcessDeployment retval = new ProcessDeploymentEntity();
	    
	    retval.setDataset(dataset);
	    retval.setDsVersion(dsVersion);

	    return retval;
	}

}
