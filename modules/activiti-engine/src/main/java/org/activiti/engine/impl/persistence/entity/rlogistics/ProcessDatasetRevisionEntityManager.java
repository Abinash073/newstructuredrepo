package org.activiti.engine.impl.persistence.entity.rlogistics;

import java.util.List;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevisionQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetRevisionIdentityManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetRevisionQueryImpl;

public class ProcessDatasetRevisionEntityManager extends AbstractManager implements ProcessDatasetRevisionIdentityManager {

	@Override
	public ProcessDatasetRevisionQuery createNewProcessDatasetRevisionQuery() {
		return new ProcessDatasetRevisionQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
	}

	@Override
	public long findProcessDatasetRevisionCountByQueryCriteria(ProcessDatasetRevisionQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectProcessDatasetRevisionCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessDatasetRevision> findProcessDatasetRevisionByQueryCriteria(ProcessDatasetRevisionQueryImpl query, Page page) {
	    return getDbSqlSession().selectList("selectProcessDatasetRevisionByQueryCriteria", query, page);
	}
	
	public void insertDatasetRevision(ProcessDatasetRevision role) {
		getDbSqlSession().insert((PersistentObject) role);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, role));
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, role));
		}
	}

	public void updateDatasetRevision(ProcessDatasetRevision updatedDatasetRevision) {
		CommandContext commandContext = Context.getCommandContext();
		DbSqlSession dbSqlSession = commandContext.getDbSqlSession();
		dbSqlSession.update((PersistentObject) updatedDatasetRevision);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_UPDATED, updatedDatasetRevision));
		}
	}

	@Override
	public void deleteDatasetRevision(String dataset,String dsVersion,String roleId) {
		ProcessDatasetRevisionEntity role = (ProcessDatasetRevisionEntity) findProcessDatasetRevisionById(dataset,dsVersion,roleId);
		if (role != null) {
			role.delete();

			if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
				getProcessEngineConfiguration().getEventDispatcher()
						.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, role));
			}
		}
	}

	@Override
	public ProcessDatasetRevision findProcessDatasetRevisionById(String dataset,String dsVersion,String roleId) {
		ProcessDatasetRevisionQueryImpl query = new ProcessDatasetRevisionQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
		
		query.setDataset(dataset);
		query.setDsVersion(dsVersion);
		
	    return (ProcessDatasetRevisionEntity) query.executeSingleResult(Context.getCommandContext());
	}

	@Override
	public boolean isNewDatasetRevision(ProcessDatasetRevision role) {
		return ((ProcessDatasetRevisionEntity) role).getRevision() == 0;
	}

	@Override
	public ProcessDatasetRevision createNewProcessDatasetRevision(String dataset,String dsVersion) {
	    ProcessDatasetRevision retval = new ProcessDatasetRevisionEntity();
	    
	    retval.setDataset(dataset);
	    retval.setDsVersion(dsVersion);
	    
	    return retval;
	}

}
