package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.engine.impl.Page;

public interface ProcessRoleIdentityManager {

	ProcessRoleQuery createNewProcessRoleQuery();

	long findProcessRoleCountByQueryCriteria(ProcessRoleQueryImpl query);
	List<ProcessRole> findProcessRoleByQueryCriteria(ProcessRoleQueryImpl query, Page page);

	ProcessRole findProcessRoleById(String dataset,String dsVersion,String roleId);	  

	void deleteRole(String dataset,String dsVersion,String roleId);

	boolean isNewRole(ProcessRole role);

	void insertRole(ProcessRole role);

	void updateRole(ProcessRole role);

	ProcessRole createNewProcessRole(String dataset,String dsVersion);
}
