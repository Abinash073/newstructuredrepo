package org.activiti.engine.impl.persistence.entity.rlogistics;

import java.util.Base64;
import java.util.Date;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public abstract class BaseProcessEntity {

	private static Random random = new Random(new Date().getTime());

	protected String generateId(){
		try {
			SecretKeySpec sks = new SecretKeySpec(key.getBytes(),algo);
			Mac mac = Mac.getInstance(algo);
			mac.init(sks);
			byte[] data = new byte[16];
			synchronized(BaseProcessEntity.class){
				random.nextBytes(data);
			}
			return Base64.getUrlEncoder().encodeToString(mac.doFinal(data));
		} catch(Exception ex){
			throw new RuntimeException("Exception while trying to generate an id",ex);
		}
	}
	
	private static final String key = "rlogistics-secret-key";
	private static final String algo = "HmacSHA1";
}
