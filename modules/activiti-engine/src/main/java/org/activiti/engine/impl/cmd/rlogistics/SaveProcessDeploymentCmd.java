package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class SaveProcessDeploymentCmd implements Command<Void>, Serializable {

	private static final long serialVersionUID = 1L;
	protected ProcessDeployment deployment;

	public SaveProcessDeploymentCmd(ProcessDeployment deployment) {
		this.deployment = deployment;
	}

	public Void execute(CommandContext commandContext) {
		if (deployment == null) {
			throw new ActivitiIllegalArgumentException("Role is null");
		}
		if (commandContext.getProcessDeploymentIdentityManager().isNewDeployment(deployment)) {
			commandContext.getProcessDeploymentIdentityManager().insertDeployment(deployment);
		} else {
			commandContext.getProcessDeploymentIdentityManager().updateDeployment(deployment);
		}

		return null;
	}
}
