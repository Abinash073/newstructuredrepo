package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDeploymentCmd implements Command<ProcessDeployment>, Serializable {

	private static final long serialVersionUID = 1L;

	protected String dataset;
	protected String dsVersion;

	public CreateProcessDeploymentCmd(String dataset,String dsVersion) {
		this.dataset = dataset;
		this.dsVersion = dsVersion;
	}

	public ProcessDeployment execute(CommandContext commandContext) {
		return commandContext.getProcessDeploymentIdentityManager().createNewProcessDeployment(dataset,dsVersion);
	}
}
