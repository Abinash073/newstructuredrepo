package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class SaveProcessDatasetCmd implements Command<Void>, Serializable {

	private static final long serialVersionUID = 1L;
	protected ProcessDataset dataset;

	public SaveProcessDatasetCmd(ProcessDataset dataset) {
		this.dataset = dataset;
	}

	public Void execute(CommandContext commandContext) {
		if (dataset == null) {
			throw new ActivitiIllegalArgumentException("Dataset is null");
		}
		if (commandContext.getProcessDatasetIdentityManager().isNewDataset(dataset)) {
			commandContext.getProcessDatasetIdentityManager().insertDataset(dataset);
		} else {
			commandContext.getProcessDatasetIdentityManager().updateDataset(dataset);
		}

		return null;
	}
}
