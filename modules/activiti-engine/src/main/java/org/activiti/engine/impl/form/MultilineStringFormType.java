package org.activiti.engine.impl.form;

public class MultilineStringFormType extends StringFormType {

	@Override
	public String getName() {
		return "multiline-string";
	}

}
