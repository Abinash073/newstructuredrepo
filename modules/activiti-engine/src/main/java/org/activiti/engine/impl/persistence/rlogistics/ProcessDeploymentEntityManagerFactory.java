package org.activiti.engine.impl.persistence.rlogistics;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.rlogistics.ProcessDeploymentEntityManager;
import org.activiti.engine.impl.rlogistics.ProcessDeploymentIdentityManager;

public class ProcessDeploymentEntityManagerFactory implements SessionFactory
{
	public Class<?> getSessionType() {
		return ProcessDeploymentIdentityManager.class;
	}

	public Session openSession() {
		return new ProcessDeploymentEntityManager();
	}
}