package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessRoleQueryCmd implements Command<ProcessRoleQuery>, Serializable {
	  
	  private static final long serialVersionUID = 1L;

	  public ProcessRoleQuery execute(CommandContext commandContext) {
	    return commandContext
	      .getProcessRoleIdentityManager()
	      .createNewProcessRoleQuery();
	  }
}
