package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevisionQuery;
import org.activiti.engine.impl.Page;

public interface ProcessDatasetRevisionIdentityManager {

	ProcessDatasetRevisionQuery createNewProcessDatasetRevisionQuery();

	long findProcessDatasetRevisionCountByQueryCriteria(ProcessDatasetRevisionQueryImpl query);
	List<ProcessDatasetRevision> findProcessDatasetRevisionByQueryCriteria(ProcessDatasetRevisionQueryImpl query, Page page);

	ProcessDatasetRevision findProcessDatasetRevisionById(String dataset,String dsVersion,String roleId);	  

	void deleteDatasetRevision(String dataset,String dsVersion,String roleId);

	boolean isNewDatasetRevision(ProcessDatasetRevision role);

	void insertDatasetRevision(ProcessDatasetRevision role);

	void updateDatasetRevision(ProcessDatasetRevision role);

	ProcessDatasetRevision createNewProcessDatasetRevision(String dataset,String dsVersion);
}
