package org.activiti.engine.impl.rlogistics;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.query.QueryProperty;

public class ProcessRoleQueryProperty implements QueryProperty {

	private static final long serialVersionUID = 1L;

	private static final Map<String, ProcessRoleQueryProperty> properties = new HashMap<String, ProcessRoleQueryProperty>();

	public static final ProcessRoleQueryProperty ROLE_ID = new ProcessRoleQueryProperty("RES.ID_");
	public static final ProcessRoleQueryProperty NAME = new ProcessRoleQueryProperty("RES.NAME_");

	private String name;

	public ProcessRoleQueryProperty(String name) {
	    this.name = name;
	    properties.put(name, this);
	  }

	public String getName() {
		return name;
	}

	public static ProcessRoleQueryProperty findByName(String propertyName) {
		return properties.get(propertyName);
	}
}
