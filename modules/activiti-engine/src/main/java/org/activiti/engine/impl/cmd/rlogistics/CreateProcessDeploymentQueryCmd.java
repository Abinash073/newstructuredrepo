package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDeploymentQuery;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDeploymentQueryCmd implements Command<ProcessDeploymentQuery>, Serializable {
	  
	  private static final long serialVersionUID = 1L;

	  public ProcessDeploymentQuery execute(CommandContext commandContext) {
	    return commandContext
	      .getProcessDeploymentIdentityManager()
	      .createNewProcessDeploymentQuery();
	  }
}
