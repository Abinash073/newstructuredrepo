package org.activiti.engine.impl;

import org.activiti.engine.identity.NativeUserQuery;
import org.activiti.engine.identity.MetaUser;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

import java.util.List;
import java.util.Map;

public class NativeUserQueryImpl extends AbstractNativeQuery<NativeUserQuery, MetaUser> implements NativeUserQuery {

  private static final long serialVersionUID = 1L;
  
  public NativeUserQueryImpl(CommandContext commandContext) {
    super(commandContext);
  }

  public NativeUserQueryImpl(CommandExecutor commandExecutor) {
    super(commandExecutor);
  }


 //results ////////////////////////////////////////////////////////////////
  
  public List<MetaUser> executeList(CommandContext commandContext, Map<String, Object> parameterMap, int firstResult, int maxResults) {
    return commandContext
      .getUserIdentityManager()
      .findUsersByNativeQuery(parameterMap, firstResult, maxResults);
  }
  
  public long executeCount(CommandContext commandContext, Map<String, Object> parameterMap) {
    return commandContext
      .getUserIdentityManager()
      .findUserCountByNativeQuery(parameterMap);
  }

}