package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.engine.impl.AbstractQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class ProcessRoleQueryImpl extends AbstractQuery<ProcessRoleQuery, ProcessRole> implements ProcessRoleQuery {

	private static final long serialVersionUID = 1L;
	protected String id;
	protected String dataset;
	protected String dsVersion;
	protected String name;
	protected String nameLike;
	protected String parentRoleId;
	protected String parentRoleIdLike;

	public ProcessRoleQueryImpl() {
	}

	public ProcessRoleQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public ProcessRoleQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	@Override
	public ProcessRoleQuery id(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		return this;
	}

	@Override
	public ProcessRoleQuery dataset(String dataset) {
		if (dataset == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.dataset = dataset;
		return this;
	}
	
	@Override
	public ProcessRoleQuery dsVersion(String dsVersion){
		if (dsVersion == null) {
			throw new ActivitiIllegalArgumentException("Provided dsVersion is null");
		}
		this.dsVersion = dsVersion;
		return this;
	}
	
	@Override
	public ProcessRoleQuery roleId(String name) {
		if (name == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.name = name;
		return this;
	}

	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getProcessRoleIdentityManager().findProcessRoleCountByQueryCriteria(this);
	}

	@Override
	public List<ProcessRole> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getProcessRoleIdentityManager().findProcessRoleByQueryCriteria(this, page);
	}

	@Override
	public ProcessRoleQuery orderByRoleId() {
		return orderBy(ProcessRoleQueryProperty.ROLE_ID);
	}

	@Override
	public ProcessRoleQuery orderByName() {
		return orderBy(ProcessRoleQueryProperty.NAME);
	}

	@Override
	public ProcessRoleQuery orderByParentRoleId() {
		return orderBy(ProcessRoleQueryProperty.ROLE_ID);
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getParentRoleId() {
		return parentRoleId;
	}
	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getDsVersion() {
		return dsVersion;
	}

	public void setDsVersion(String dsVersion) {
		this.dsVersion = dsVersion;
	}
}
