package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class SaveProcessRoleCmd implements Command<Void>, Serializable {

	private static final long serialVersionUID = 1L;
	protected ProcessRole role;

	public SaveProcessRoleCmd(ProcessRole role) {
		this.role = role;
	}

	public Void execute(CommandContext commandContext) {
		if (role == null) {
			throw new ActivitiIllegalArgumentException("Role is null");
		}
		if (commandContext.getProcessRoleIdentityManager().isNewRole(role)) {
			commandContext.getProcessRoleIdentityManager().insertRole(role);
		} else {
			commandContext.getProcessRoleIdentityManager().updateRole(role);
		}

		return null;
	}
}
