package org.activiti.engine.identity.rlogistics;

import org.activiti.engine.query.Query;

public interface ProcessDeploymentQuery extends Query<ProcessDeploymentQuery,ProcessDeployment>{
	ProcessDeploymentQuery id(String id);
	ProcessDeploymentQuery dataset(String dataset);
	ProcessDeploymentQuery dsVersion(String dsVersion);
	ProcessDeploymentQuery name(String name);
	ProcessDeploymentQuery deploymentId(String deploymentId);
	
	ProcessDeploymentQuery orderById();
	ProcessDeploymentQuery orderByDataset();
	ProcessDeploymentQuery orderByDsVersion();
	ProcessDeploymentQuery orderByName();
	ProcessDeploymentQuery orderByDeploymentId();
}
