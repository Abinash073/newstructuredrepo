package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDatasetRevisionCmd implements Command<ProcessDatasetRevision>, Serializable {

	private static final long serialVersionUID = 1L;

	protected String dataset;
	protected String dsVersion;

	public CreateProcessDatasetRevisionCmd(String dataset,String dsVersion) {
		this.dataset = dataset;
		this.dsVersion = dsVersion;
	}

	public ProcessDatasetRevision execute(CommandContext commandContext) {
		return commandContext.getProcessDatasetRevisionIdentityManager().createNewProcessDatasetRevision(dataset,dsVersion);
	}
}
