package org.activiti.engine.impl.persistence.rlogistics;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.rlogistics.ProcessDatasetEntityManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetIdentityManager;

public class ProcessDatasetEntityManagerFactory implements SessionFactory
{
	public Class<?> getSessionType() {
		return ProcessDatasetIdentityManager.class;
	}

	public Session openSession() {
		return new ProcessDatasetEntityManager();
	}
}