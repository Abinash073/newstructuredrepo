package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDatasetQuery;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDatasetQueryCmd implements Command<ProcessDatasetQuery>, Serializable {
	  
	  private static final long serialVersionUID = 1L;

	  public ProcessDatasetQuery execute(CommandContext commandContext) {
	    return commandContext
	      .getProcessDatasetIdentityManager()
	      .createNewProcessDatasetQuery();
	  }
}
