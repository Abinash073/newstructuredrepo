package org.activiti.engine.impl.persistence.rlogistics;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.rlogistics.ProcessDatasetRevisionEntityManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetRevisionIdentityManager;

public class ProcessDatasetRevisionEntityManagerFactory implements SessionFactory
{
	public Class<?> getSessionType() {
		return ProcessDatasetRevisionIdentityManager.class;
	}

	public Session openSession() {
		return new ProcessDatasetRevisionEntityManager();
	}
}