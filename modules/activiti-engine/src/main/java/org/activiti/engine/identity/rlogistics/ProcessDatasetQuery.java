package org.activiti.engine.identity.rlogistics;

import org.activiti.engine.query.Query;

public interface ProcessDatasetQuery extends Query<ProcessDatasetQuery,ProcessDataset>{
	ProcessDatasetQuery id(String id);
	ProcessDatasetQuery name(String name);
	ProcessDatasetQuery datasetId(String datasetId);

	ProcessDatasetQuery orderById();
	ProcessDatasetQuery orderByName();
}
