package org.activiti.engine.impl.persistence.entity.rlogistics;

import java.util.List;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessDatasetQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetIdentityManager;
import org.activiti.engine.impl.rlogistics.ProcessDatasetQueryImpl;

public class ProcessDatasetEntityManager extends AbstractManager implements ProcessDatasetIdentityManager {

	@Override
	public ProcessDatasetQuery createNewProcessDatasetQuery() {
		return new ProcessDatasetQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
	}

	@Override
	public long findProcessDatasetCountByQueryCriteria(ProcessDatasetQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectProcessDatasetCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessDataset> findProcessDatasetByQueryCriteria(ProcessDatasetQueryImpl query, Page page) {
	    return getDbSqlSession().selectList("selectProcessDatasetByQueryCriteria", query, page);
	}
	
	public void insertDataset(ProcessDataset dataset) {
		getDbSqlSession().insert((PersistentObject) dataset);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, dataset));
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, dataset));
		}
	}

	public void updateDataset(ProcessDataset updatedDataset) {
		CommandContext commandContext = Context.getCommandContext();
		DbSqlSession dbSqlSession = commandContext.getDbSqlSession();
		dbSqlSession.update((PersistentObject) updatedDataset);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_UPDATED, updatedDataset));
		}
	}

	@Override
	public void deleteDataset(String datasetId) {
		ProcessDatasetEntity dataset = (ProcessDatasetEntity) findProcessDatasetById(datasetId);
		if (dataset != null) {
			dataset.delete();

			if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
				getProcessEngineConfiguration().getEventDispatcher()
						.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, dataset));
			}
		}
	}

	@Override
	public ProcessDataset findProcessDatasetById(String datasetId) {
		ProcessDatasetQueryImpl query = new ProcessDatasetQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
		
		query.datasetId(datasetId);
		
	    return (ProcessDatasetEntity) query.executeSingleResult(Context.getCommandContext());
	}

	@Override
	public boolean isNewDataset(ProcessDataset dataset) {
		return ((ProcessDatasetEntity) dataset).getRevision() == 0;
	}

	@Override
	public ProcessDataset createNewProcessDataset() {
	    ProcessDataset retval = new ProcessDatasetEntity();
	    
	    return retval;
	}

}
