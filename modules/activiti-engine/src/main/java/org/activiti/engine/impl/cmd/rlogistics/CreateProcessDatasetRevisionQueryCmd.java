package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.identity.rlogistics.ProcessDatasetRevisionQuery;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class CreateProcessDatasetRevisionQueryCmd implements Command<ProcessDatasetRevisionQuery>, Serializable {
	  
	  private static final long serialVersionUID = 1L;

	  public ProcessDatasetRevisionQuery execute(CommandContext commandContext) {
	    return commandContext
	      .getProcessDatasetRevisionIdentityManager()
	      .createNewProcessDatasetRevisionQuery();
	  }
}
