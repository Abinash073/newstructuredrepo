/**
 * @Adarsh Custom Button
 * Initial Stage / May Lead to Bugs
 */


package org.activiti.engine.impl.form;

import org.activiti.engine.form.AbstractFormType;

public class ButtonFormType extends AbstractFormType {
    //private static final long serialVersionUID = 1l;

    public ButtonFormType(){
    }

    public String getName() {
        return "Button";
    }

     //public String getMimeType() {
     //  return "text/plain";
     // }

    public String convertFormValueToModelValue(String propertyValue) {
        String url = String.valueOf(propertyValue);
        return url;
    }

    public String convertModelValueToFormValue(Object modelValue) {
        if (modelValue == null) {
            return null;
        }
        return (String) modelValue;
    }
}
