package org.activiti.engine.impl.rlogistics;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.query.QueryProperty;

public class ProcessDatasetQueryProperty implements QueryProperty {

	private static final long serialVersionUID = 1L;

	private static final Map<String, ProcessDatasetQueryProperty> properties = new HashMap<String, ProcessDatasetQueryProperty>();

	public static final ProcessDatasetQueryProperty DATASET_ID = new ProcessDatasetQueryProperty("RES.ID_");
	public static final ProcessDatasetQueryProperty NAME = new ProcessDatasetQueryProperty("RES.NAME_");

	private String name;

	public ProcessDatasetQueryProperty(String name) {
	    this.name = name;
	    properties.put(name, this);
	  }

	public String getName() {
		return name;
	}

	public static ProcessDatasetQueryProperty findByName(String propertyName) {
		return properties.get(propertyName);
	}
}
