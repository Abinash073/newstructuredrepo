package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class DeleteProcessDeploymentCmd implements Command<Void>, Serializable {

	  private static final long serialVersionUID = 1L;
	  String processDeploymentId;
	  
	  public DeleteProcessDeploymentCmd(String processDeploymentId) {
	    this.processDeploymentId = processDeploymentId;
	  }

	  public Void execute(CommandContext commandContext) {
	    if(processDeploymentId == null) {
	      throw new ActivitiIllegalArgumentException("processDeploymentId is null");
	    }
	    commandContext
	      .getProcessDeploymentIdentityManager()
	      .deleteDeployment(processDeploymentId);
	    
	    return null;
	  }
	}
