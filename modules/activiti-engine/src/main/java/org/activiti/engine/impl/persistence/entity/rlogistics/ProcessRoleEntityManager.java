package org.activiti.engine.impl.persistence.entity.rlogistics;

import java.util.List;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.rlogistics.ProcessRoleIdentityManager;
import org.activiti.engine.impl.rlogistics.ProcessRoleQueryImpl;

public class ProcessRoleEntityManager extends AbstractManager implements ProcessRoleIdentityManager {

	@Override
	public ProcessRoleQuery createNewProcessRoleQuery() {
		return new ProcessRoleQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
	}

	@Override
	public long findProcessRoleCountByQueryCriteria(ProcessRoleQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectProcessRoleCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcessRole> findProcessRoleByQueryCriteria(ProcessRoleQueryImpl query, Page page) {
	    return getDbSqlSession().selectList("selectProcessRoleByQueryCriteria", query, page);
	}
	
	public void insertRole(ProcessRole role) {
		getDbSqlSession().insert((PersistentObject) role);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_CREATED, role));
			getProcessEngineConfiguration().getEventDispatcher()
					.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_INITIALIZED, role));
		}
	}

	public void updateRole(ProcessRole updatedRole) {
		CommandContext commandContext = Context.getCommandContext();
		DbSqlSession dbSqlSession = commandContext.getDbSqlSession();
		dbSqlSession.update((PersistentObject) updatedRole);

		if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
			getProcessEngineConfiguration().getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_UPDATED, updatedRole));
		}
	}

	@Override
	public void deleteRole(String dataset,String dsVersion,String roleId) {
		ProcessRoleEntity role = (ProcessRoleEntity) findProcessRoleById(dataset,dsVersion,roleId);
		if (role != null) {
			role.delete();

			if (getProcessEngineConfiguration().getEventDispatcher().isEnabled()) {
				getProcessEngineConfiguration().getEventDispatcher()
						.dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, role));
			}
		}
	}

	@Override
	public ProcessRole findProcessRoleById(String dataset,String dsVersion,String roleId) {
		ProcessRoleQueryImpl query = new ProcessRoleQueryImpl(Context.getProcessEngineConfiguration().getCommandExecutor());
		
		query.setDataset(dataset);
		query.setDsVersion(dsVersion);
		query.roleId(roleId);
		
	    return (ProcessRoleEntity) query.executeSingleResult(Context.getCommandContext());
	}

	@Override
	public boolean isNewRole(ProcessRole role) {
		return ((ProcessRoleEntity) role).getRevision() == 0;
	}

	@Override
	public ProcessRole createNewProcessRole(String dataset,String dsVersion) {
	    ProcessRole retval = new ProcessRoleEntity();
	    
	    retval.setDataset(dataset);
	    retval.setDsVersion(dsVersion);
	    
	    return retval;
	}

}
