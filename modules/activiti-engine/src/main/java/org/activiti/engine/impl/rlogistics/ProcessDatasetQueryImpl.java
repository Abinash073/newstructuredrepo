package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessDatasetQuery;
import org.activiti.engine.impl.AbstractQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class ProcessDatasetQueryImpl extends AbstractQuery<ProcessDatasetQuery, ProcessDataset> implements ProcessDatasetQuery {

	private static final long serialVersionUID = 1L;
	protected String id;
	protected String name;
	protected String nameLike;

	public ProcessDatasetQueryImpl() {
	}

	public ProcessDatasetQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public ProcessDatasetQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	@Override
	public ProcessDatasetQuery id(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		return this;
	}

	@Override
	public ProcessDatasetQuery name(String name) {
		if (name== null) {
			throw new ActivitiIllegalArgumentException("Provided name is null");
		}
		this.name = name;
		return this;
	}

	@Override
	public ProcessDatasetQuery datasetId(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		return this;
	}

	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getProcessDatasetIdentityManager().findProcessDatasetCountByQueryCriteria(this);
	}

	@Override
	public List<ProcessDataset> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getProcessDatasetIdentityManager().findProcessDatasetByQueryCriteria(this, page);
	}

	@Override
	public ProcessDatasetQuery orderById() {
		return orderBy(ProcessDatasetQueryProperty.DATASET_ID);
	}

	@Override
	public ProcessDatasetQuery orderByName() {
		return orderBy(ProcessDatasetQueryProperty.NAME);
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
