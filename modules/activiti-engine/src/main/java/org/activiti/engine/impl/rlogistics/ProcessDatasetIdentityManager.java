package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessDatasetQuery;
import org.activiti.engine.impl.Page;

public interface ProcessDatasetIdentityManager {

	ProcessDatasetQuery createNewProcessDatasetQuery();

	long findProcessDatasetCountByQueryCriteria(ProcessDatasetQueryImpl query);
	List<ProcessDataset> findProcessDatasetByQueryCriteria(ProcessDatasetQueryImpl query, Page page);

	ProcessDataset findProcessDatasetById(String datasetId);	  

	void deleteDataset(String datasetId);

	boolean isNewDataset(ProcessDataset dataset);

	void insertDataset(ProcessDataset dataset);

	void updateDataset(ProcessDataset dataset);

	ProcessDataset createNewProcessDataset();
}
