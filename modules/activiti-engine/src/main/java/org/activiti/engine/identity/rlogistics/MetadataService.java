package org.activiti.engine.identity.rlogistics;

import java.util.Map;

public interface MetadataService {
	
	public static final String DEFAULT_DATASET = "DEFAULT_DATASET";
	public static final String UNSET_DS_VERSION = "UNSET_DS_VERSION";

	/////////////////////
	ProcessRoleQuery createProcessRoleQuery();

	void deleteProcessRole(String dataset,String dsVersion,String id);

	void saveProcessRole(ProcessRole role);

	ProcessRole newProcessRole(String dataset,String dsVersion);

	/////////////////////
	ProcessDatasetQuery createProcessDatasetQuery();

	void deleteProcessDataset(String id);

	void saveProcessDataset(ProcessDataset dataset);

	ProcessDataset newProcessDataset();

	/////////////////////
	ProcessDeploymentQuery createProcessDeploymentQuery();

	void deleteProcessDeployment(String id);

	void saveProcessDeployment(ProcessDeployment deployment);

	ProcessDeployment newProcessDeployment(String dataset,String dsVersion);

	/////////////////////
	ProcessDatasetRevisionQuery createProcessDatasetRevisionQuery();

	void deleteProcessDatasetRevision(String dataset,String dsVersion,String id);

	void saveProcessDatasetRevision(ProcessDatasetRevision role);

	ProcessDatasetRevision newProcessDatasetRevision(String dataset,String dsVersion);	
	
	String getCurrentDsVersion(String dataset);
	Map<String, String> getCurrentDsVersions();
	void loadCurrentDsVersions();
}
