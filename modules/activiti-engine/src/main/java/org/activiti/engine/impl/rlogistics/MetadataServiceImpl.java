package org.activiti.engine.impl.rlogistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessDatasetQuery;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevision;
import org.activiti.engine.identity.rlogistics.ProcessDatasetRevisionQuery;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.identity.rlogistics.ProcessDeploymentQuery;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDatasetCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDatasetQueryCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDatasetRevisionCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDatasetRevisionQueryCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDeploymentCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessDeploymentQueryCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessRoleCmd;
import org.activiti.engine.impl.cmd.rlogistics.CreateProcessRoleQueryCmd;
import org.activiti.engine.impl.cmd.rlogistics.DeleteProcessDatasetCmd;
import org.activiti.engine.impl.cmd.rlogistics.DeleteProcessDatasetRevisionCmd;
import org.activiti.engine.impl.cmd.rlogistics.DeleteProcessDeploymentCmd;
import org.activiti.engine.impl.cmd.rlogistics.DeleteProcessRoleCmd;
import org.activiti.engine.impl.cmd.rlogistics.SaveProcessDatasetCmd;
import org.activiti.engine.impl.cmd.rlogistics.SaveProcessDatasetRevisionCmd;
import org.activiti.engine.impl.cmd.rlogistics.SaveProcessDeploymentCmd;
import org.activiti.engine.impl.cmd.rlogistics.SaveProcessRoleCmd;

public class MetadataServiceImpl extends ServiceImpl implements MetadataService {

	@Override
	public ProcessRoleQuery createProcessRoleQuery() {
		return commandExecutor.execute(new CreateProcessRoleQueryCmd());
	}

	@Override
	public void deleteProcessRole(String dataset, String dsVersion, String id) {
		commandExecutor.execute(new DeleteProcessRoleCmd(dataset, dsVersion, id));
	}

	@Override
	public void saveProcessRole(ProcessRole role) {
		commandExecutor.execute(new SaveProcessRoleCmd(role));
	}

	@Override
	public ProcessRole newProcessRole(String dataset, String dsVersion) {
		return commandExecutor.execute(new CreateProcessRoleCmd(dataset, dsVersion));
	}

	@Override
	public ProcessDatasetQuery createProcessDatasetQuery() {
		return commandExecutor.execute(new CreateProcessDatasetQueryCmd());
	}

	@Override
	public void deleteProcessDataset(String id) {
		commandExecutor.execute(new DeleteProcessDatasetCmd(id));
	}

	@Override
	public void saveProcessDataset(ProcessDataset dataset) {
		commandExecutor.execute(new SaveProcessDatasetCmd(dataset));
	}

	@Override
	public ProcessDataset newProcessDataset() {
		return commandExecutor.execute(new CreateProcessDatasetCmd());
	}

	@Override
	public ProcessDeploymentQuery createProcessDeploymentQuery() {
		return commandExecutor.execute(new CreateProcessDeploymentQueryCmd());
	}

	@Override
	public void deleteProcessDeployment(String id) {
		commandExecutor.execute(new DeleteProcessDeploymentCmd(id));
	}

	@Override
	public void saveProcessDeployment(ProcessDeployment deployment) {
		commandExecutor.execute(new SaveProcessDeploymentCmd(deployment));
	}

	@Override
	public ProcessDeployment newProcessDeployment(String dataset, String dsVersion) {
		return commandExecutor.execute(new CreateProcessDeploymentCmd(dataset,dsVersion));
	}

	@Override
	public ProcessDatasetRevisionQuery createProcessDatasetRevisionQuery() {
		return commandExecutor.execute(new CreateProcessDatasetRevisionQueryCmd());
	}

	@Override
	public void deleteProcessDatasetRevision(String dataset, String dsVersion, String id) {
		commandExecutor.execute(new DeleteProcessDatasetRevisionCmd(dataset, dsVersion, id));
	}

	@Override
	public void saveProcessDatasetRevision(ProcessDatasetRevision dsRevision) {
		commandExecutor.execute(new SaveProcessDatasetRevisionCmd(dsRevision));
	}
	
	@Override
	public ProcessDatasetRevision newProcessDatasetRevision(String dataset, String dsVersion) {
		return commandExecutor.execute(new CreateProcessDatasetRevisionCmd(dataset, dsVersion));
	}
	
	@Override
	public String getCurrentDsVersion(String dataset) {
		return currentDsVersion.get().get(dataset);
	}

	@Override
	public Map<String, String> getCurrentDsVersions(){
		return currentDsVersion.get();
	}
	
	@Override
	public void loadCurrentDsVersions() {
		currentDsVersion.set(new HashMap<String, String>());
		List<ProcessDatasetRevision> revisions = createProcessDatasetRevisionQuery().list();
		for(ProcessDatasetRevision revision:revisions){
			currentDsVersion.get().put(revision.getDataset(),revision.getDsVersion());
		}
	}

	ThreadLocal<HashMap<String,String>> currentDsVersion = new ThreadLocal<HashMap<String,String>>(); 
}
