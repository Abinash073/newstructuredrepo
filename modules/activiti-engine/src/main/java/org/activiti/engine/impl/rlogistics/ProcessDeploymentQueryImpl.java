package org.activiti.engine.impl.rlogistics;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.rlogistics.ProcessDeployment;
import org.activiti.engine.identity.rlogistics.ProcessDeploymentQuery;
import org.activiti.engine.impl.AbstractQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class ProcessDeploymentQueryImpl extends AbstractQuery<ProcessDeploymentQuery, ProcessDeployment> implements ProcessDeploymentQuery {
	
	private static final long serialVersionUID = 1L;
	protected String id;
	protected String dataset;
	protected String dsVersion;
	protected String name;
	protected String nameLike;
	protected String deploymentId;

	public ProcessDeploymentQueryImpl() {
	}

	public ProcessDeploymentQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public ProcessDeploymentQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	@Override
	public ProcessDeploymentQuery id(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		return this;
	}

	@Override
	public ProcessDeploymentQuery dataset(String dataset) {
		if (dataset == null) {
			throw new ActivitiIllegalArgumentException("Provided dataset is null");
		}
		this.dataset = dataset;
		return this;
	}

	@Override
	public ProcessDeploymentQuery dsVersion(String dsVersion) {
		if (dsVersion == null) {
			throw new ActivitiIllegalArgumentException("Provided dsVersion is null");
		}
		this.dsVersion = dsVersion;
		return this;
	}

	@Override
	public ProcessDeploymentQuery name(String name) {
		if (name == null) {
			throw new ActivitiIllegalArgumentException("Provided name is null");
		}
		this.name = name;
		return this;
	}

	@Override
	public ProcessDeploymentQuery deploymentId(String deploymentId) {
		if (deploymentId == null) {
			throw new ActivitiIllegalArgumentException("Provided deploymentId is null");
		}
		this.deploymentId = deploymentId;
		return this;
	}

	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getProcessDeploymentIdentityManager().findProcessDeploymentCountByQueryCriteria(this);
	}

	@Override
	public List<ProcessDeployment> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getProcessDeploymentIdentityManager().findProcessDeploymentByQueryCriteria(this, page);
	}

	@Override
	public ProcessDeploymentQuery orderById() {
		return orderBy(ProcessDeploymentQueryProperty.ID);
	}

	@Override
	public ProcessDeploymentQuery orderByName() {
		return orderBy(ProcessDeploymentQueryProperty.NAME);
	}
	
	@Override
	public ProcessDeploymentQuery orderByDsVersion() {
		return orderBy(ProcessDeploymentQueryProperty.DS_VERSION);
	}
	
	@Override
	public ProcessDeploymentQuery orderByDataset() {
		return orderBy(ProcessDeploymentQueryProperty.DATASET);
	}
	
	@Override
	public ProcessDeploymentQuery orderByDeploymentId() {
		return orderBy(ProcessDeploymentQueryProperty.DEPLOYMENT_ID);
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getDataset() {
		return dataset;
	}

	public String getDsVersion() {
		return dsVersion;
	}
	
	public String getDeploymentId() {
		return deploymentId;
	}
}
