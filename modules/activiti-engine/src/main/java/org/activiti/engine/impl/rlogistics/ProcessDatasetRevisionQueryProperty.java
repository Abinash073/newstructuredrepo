package org.activiti.engine.impl.rlogistics;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.query.QueryProperty;

public class ProcessDatasetRevisionQueryProperty implements QueryProperty {

	private static final long serialVersionUID = 1L;

	private static final Map<String, ProcessDatasetRevisionQueryProperty> properties = new HashMap<String, ProcessDatasetRevisionQueryProperty>();

	public static final ProcessDatasetRevisionQueryProperty DS_VERSION = new ProcessDatasetRevisionQueryProperty("RES.DS_VERSION_");
	public static final ProcessDatasetRevisionQueryProperty DATASET = new ProcessDatasetRevisionQueryProperty("RES.DATASET_");

	private String name;

	public ProcessDatasetRevisionQueryProperty(String name) {
	    this.name = name;
	    properties.put(name, this);
	  }

	public String getName() {
		return name;
	}

	public static ProcessDatasetRevisionQueryProperty findByName(String propertyName) {
		return properties.get(propertyName);
	}
}
