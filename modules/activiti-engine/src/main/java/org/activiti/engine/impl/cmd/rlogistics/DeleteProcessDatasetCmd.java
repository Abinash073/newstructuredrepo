package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class DeleteProcessDatasetCmd implements Command<Void>, Serializable {

	  private static final long serialVersionUID = 1L;
	  String datasetId;
	  
	  public DeleteProcessDatasetCmd(String datasetId) {
	    this.datasetId = datasetId;
	  }

	  public Void execute(CommandContext commandContext) {
	    if(datasetId == null) {
	      throw new ActivitiIllegalArgumentException("datasetId is null");
	    }
	    commandContext
	      .getProcessDatasetIdentityManager()
	      .deleteDataset(datasetId);
	    
	    return null;
	  }
	}
