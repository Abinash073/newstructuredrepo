package org.activiti.engine.identity.rlogistics;

import org.activiti.engine.query.Query;

public interface ProcessDatasetRevisionQuery extends Query<ProcessDatasetRevisionQuery,ProcessDatasetRevision>{
	ProcessDatasetRevisionQuery id(String id);
	ProcessDatasetRevisionQuery dataset(String dataset);
	ProcessDatasetRevisionQuery dsVersion(String dsVersion);
	
	ProcessDatasetRevisionQuery orderByDsVersion();
	ProcessDatasetRevisionQuery orderByDataset();

}
