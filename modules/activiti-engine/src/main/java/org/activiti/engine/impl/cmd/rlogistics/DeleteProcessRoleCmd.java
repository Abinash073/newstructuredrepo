package org.activiti.engine.impl.cmd.rlogistics;

import java.io.Serializable;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

public class DeleteProcessRoleCmd implements Command<Void>, Serializable {

	  private static final long serialVersionUID = 1L;
	  String roleId;
	  String dataset;
	  String dsVersion;
	  
	  public DeleteProcessRoleCmd(String dataset,String dsVersion, String roleId) {
	    this.roleId = roleId;
	    this.dataset = dataset;
	    this.dsVersion = dsVersion;
	  }

	  public Void execute(CommandContext commandContext) {
	    if(roleId == null) {
	      throw new ActivitiIllegalArgumentException("roleId is null");
	    }
	    commandContext
	      .getProcessRoleIdentityManager()
	      .deleteRole(dataset,dsVersion,roleId);
	    
	    return null;
	  }
	}
