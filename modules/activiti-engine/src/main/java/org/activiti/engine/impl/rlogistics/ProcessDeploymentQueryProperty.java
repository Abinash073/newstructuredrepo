package org.activiti.engine.impl.rlogistics;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.query.QueryProperty;

public class ProcessDeploymentQueryProperty implements QueryProperty {

	private static final long serialVersionUID = 1L;

	private static final Map<String, ProcessDeploymentQueryProperty> properties = new HashMap<String, ProcessDeploymentQueryProperty>();

	public static final ProcessDeploymentQueryProperty ID = new ProcessDeploymentQueryProperty("RES.ID_");
	public static final ProcessDeploymentQueryProperty NAME = new ProcessDeploymentQueryProperty("RES.NAME_");
	public static final ProcessDeploymentQueryProperty DS_VERSION = new ProcessDeploymentQueryProperty("RES.DS_VERSION_");
	public static final ProcessDeploymentQueryProperty DATASET = new ProcessDeploymentQueryProperty("RES.DATASET_");
	public static final ProcessDeploymentQueryProperty DEPLOYMENT_ID = new ProcessDeploymentQueryProperty("RES.DEPLOYMENT_ID_");

	private String name;

	public ProcessDeploymentQueryProperty(String name) {
	    this.name = name;
	    properties.put(name, this);
	  }

	public String getName() {
		return name;
	}

	public static ProcessDeploymentQueryProperty findByName(String propertyName) {
		return properties.get(propertyName);
	}
}
