/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.activiti.engine.impl.persistence.entity;

import org.activiti.engine.history.HistoricFormProperty;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.variable.ByteArrayType;
import org.activiti.engine.impl.variable.VariableType;
import org.activiti.engine.impl.variable.VariableTypes;

/**
 * @author Tom Baeyens
 */
public class HistoricFormPropertyEntity extends HistoricDetailEntity implements HistoricFormProperty {

	private static final long serialVersionUID = 1L;

	protected String name;
	protected VariableType variableType;
	protected String textValue;
	protected String textValue2;
	protected Double doubleValue;
	protected Long longValue;
	protected final ByteArrayRef byteArrayRef = new ByteArrayRef();
	
	String propertyId;
	String propertyValue;

	public HistoricFormPropertyEntity() {
		this.detailType = "FormProperty";
	}

	public HistoricFormPropertyEntity(ExecutionEntity execution, String propertyId, String propertyValue) {
		this(execution, propertyId, propertyValue, null);
	}

	public HistoricFormPropertyEntity(ExecutionEntity execution, String propertyId, String propertyValue,
			String taskId) {
		this.processInstanceId = execution.getProcessInstanceId();
		this.executionId = execution.getId();
		this.taskId = taskId;
		this.time = Context.getProcessEngineConfiguration().getClock().getCurrentTime();
		this.detailType = "FormProperty";

		HistoricActivityInstanceEntity historicActivityInstance = Context.getCommandContext().getHistoryManager()
				.findActivityInstance(execution);
		if (historicActivityInstance != null) {
			this.activityInstanceId = historicActivityInstance.getId();
		}

		VariableTypes variableTypes = Context.getProcessEngineConfiguration().getVariableTypes();

		this.name = propertyId;
		this.variableType = variableTypes.findVariableType(propertyValue);

		if(variableType instanceof ByteArrayType){
			String byteArrayName = "hist.detail.fp-" + name;
			this.byteArrayRef.setValue(byteArrayName, propertyValue.getBytes());
		} else {
			this.textValue = propertyValue;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public VariableType getVariableType() {
		return variableType;
	}

	public void setVariableType(VariableType variableType) {
		this.variableType = variableType;
	}

	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}

	public String getTextValue2() {
		return textValue2;
	}

	public void setTextValue2(String textValue2) {
		this.textValue2 = textValue2;
	}

	public Double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(Double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	public ByteArrayRef getByteArrayRef() {
		return byteArrayRef;
	}

	@Override
	public String getPropertyId() {
		// TODO Auto-generated method stub
		return propertyId;
	}

	@Override
	public String getPropertyValue() {
		// TODO Auto-generated method stub
		return propertyValue;
	}
}
