package com.rlogistics.barcodeprinter;

import java.util.ArrayList;

public class PackagingTypeResponse {
	private ArrayList<PackagingType> data;
	private int totalRecords;
	public ArrayList<PackagingType> getData() {
		return data;
	}
	public void setData(ArrayList<PackagingType> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	
}
