package com.rlogistics.barcodeprinter;

import java.util.ArrayList;

public class ProcessLocationResponse {
	private ArrayList<ProcessLocation> data;
	private int totalRecords;
	public ArrayList<ProcessLocation> getData() {
		return data;
	}
	public void setData(ArrayList<ProcessLocation> data) {
		this.data = data;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	
}
