package com.rlogistics.barcodeprinter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.*;
import javax.swing.border.LineBorder;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.sun.deploy.uitoolkit.ToolkitStore.dispose;

public class BarcodePrinter {
	private static BarcodePrinter instance = new BarcodePrinter();
	private JFrame frame = null;
	private PrintService printService = null;
	private JComboBox<ProcessLocation> locationComboBox = null;
	private JComboBox<PackagingType> packagingTypeComboBox = null;
	private JFormattedTextField numberText = null;
	private JButton printButton = null;

	private ObjectMapper mapper = null;

	private HttpClient httpClient = null;
	private HttpHost httpHost = null;
	private String rltoken = null;

	/* Configuration items */
	private Properties configuration = null;

	private void init() {
//		InputStream is = getClass().getClassLoader().getResourceAsStream("bza-bcp.properties");

		configuration = new Properties();

		/* Load default values */
		configuration.setProperty("server", "rl.bizlog.in");          //For prod:rl.bizlog.in //Demo:54.202.95.252
		configuration.setProperty("port", "8080");                       //8080                     //9080
		configuration.setProperty("id", "bizadmin@rlogistics.com");     //bizadmin@rlogistics.com  //manager.ho@rlogistics.com
		configuration.setProperty("password", "Bizindia111");              //Bangalore@123            //welcome
		configuration.setProperty("fieldOriginX", "15");
		configuration.setProperty("fieldOriginY", "50");
		configuration.setProperty("barcodeHeight", "100");
		configuration.setProperty("SecondLabelOffset", "315");

//		if(is != null){
//			try {
//				configuration.load(is);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
	}
	private void initFrame(){
		frame = new JFrame("BizLog Barcode Printer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout());
		frame.pack();
		frame.setVisible(true);
	}

	private void destroyFrame(){
		frame.dispose();
	}

	private void initHttpInteractions(){
		int port = Integer.valueOf(configuration.getProperty("port"));
		httpHost = new HttpHost(configuration.getProperty("server"),port);
		httpClient = HttpClientBuilder.create().build();

		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	private String interactWithServer(String api,Map<String,String> params){
		HttpPost post = new HttpPost(api);

		List<NameValuePair> nvpParams = new ArrayList<>();

		for(String param:params.keySet()){
			nvpParams.add(new BasicNameValuePair(param,params.get(param)));
		}

		if(rltoken != null){
			nvpParams.add(new BasicNameValuePair("rltoken",rltoken));
		}

		try {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvpParams);
			post.setEntity(entity);
			System.out.println("(" +  configuration.getProperty("server") + api + ")>>>>>>>>> " + EntityUtils.toString(entity));
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(instance.frame,
					"Client Error:" + e.getMessage(),
					"Colud not talk to the server.",
				    JOptionPane.ERROR_MESSAGE);
			return null;
		}

		try {
			HttpResponse response = httpClient.execute(httpHost, post);

			String textResponse = EntityUtils.toString(response.getEntity());

			System.out.println("<<<<<<<<<< " + textResponse);

			return textResponse;
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(instance.frame,
					"Server Error:" + e.getMessage(),
				    "Colud not talk to server.",
				    JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

	private boolean login(){
		Map<String,String> params = new HashMap<>();

		params.put("id", configuration.getProperty("id"));
		params.put("password", configuration.getProperty("password"));

		String response = interactWithServer("/rlogistics-execution/rlservice/auth/login", params);

		try {
			LoginResponse loginResponse = mapper.readValue(response, LoginResponse.class);

			if(!loginResponse.getResponse().isSuccess()){
				JOptionPane.showMessageDialog(instance.frame,
						"Server Error:" + loginResponse.getMessage(),
					    "Colud not login to the server.",
					    JOptionPane.ERROR_MESSAGE);
				return false;
			} else {
				rltoken = loginResponse.getResponse().getToken();
			}
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(instance.frame,
					"Server Error:" + e.getMessage(),
				    "Colud not login to the server.",
				    JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	private ProcessLocation[] loadLoactions() {
		Map<String,String> params = new HashMap<>();

		params.put("firstResult", "0");
		params.put("maxResults", "50");
		params.put("query", "{}");

		String response = interactWithServer("/rlogistics-execution/rlservice/processlocation/query", params);

		try {
			ProcessLocationResponse locations = mapper.readValue(response, ProcessLocationResponse.class);

			return locations.getData().toArray(new ProcessLocation[locations.getData().size()]);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(instance.frame,
					"Server Error:" + e.getMessage(),
				    "Colud not list locations.",
				    JOptionPane.ERROR_MESSAGE);
			return new ProcessLocation[0];
		}
	}

	private PackagingType[] loadPackagingTypes() {
		Map<String,String> params = new HashMap<>();

		params.put("firstResult", "0");
		params.put("maxResults", "50");
		params.put("query", "{}");

		String response = interactWithServer("/rlogistics-execution/rlservice/packagingtype/query", params);

		try {
			PackagingTypeResponse packagingTypes = mapper.readValue(response, PackagingTypeResponse.class);

			return packagingTypes.getData().toArray(new PackagingType[packagingTypes.getData().size()]);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(instance.frame,
					"Server Error:" + e.getMessage(),
				    "Colud not list packaging types.",
				    JOptionPane.ERROR_MESSAGE);
			return new PackagingType[0];
		}
	}

	private void displayControls(){

		initHttpInteractions();
		
		if(!login()){
			destroyFrame();
			return;
		}

		locationComboBox = new JComboBox<>(loadLoactions());
		JLabel locationLabel = new JLabel("Location");
		frame.getContentPane().add(locationLabel);
		frame.getContentPane().add(locationComboBox);

		packagingTypeComboBox = new JComboBox<>(loadPackagingTypes());
		JLabel packagingTypeLabel = new JLabel("Packaging Type");
		frame.getContentPane().add(packagingTypeLabel);
		frame.getContentPane().add(packagingTypeComboBox);

		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);
		numberText = new JFormattedTextField(format);
		numberText.setColumns(10);
		JLabel itemCountLabel = new JLabel("Number of Barcodes");
		frame.getContentPane().add(itemCountLabel);
		frame.getContentPane().add(numberText);

		printButton = new JButton("Print");
		frame.getContentPane().add(printButton);
		printButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				ProcessLocation location = (ProcessLocation) locationComboBox.getSelectedItem();
				PackagingType packagingType = (PackagingType) packagingTypeComboBox.getSelectedItem();

				Map<String,String> params = new HashMap<>();

				params.put("locationId", location.getId());
				params.put("packagingTypeCode", packagingType.getCode());
				params.put("itemCount", numberText.getText());

				String queryString;
				try {
					queryString = mapper.writeValueAsString(params);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(instance.frame,
							"Client Error:" + e.getMessage(),
						    "Colud not build request for server.",
						    JOptionPane.ERROR_MESSAGE);
					return;
				}

				params = new HashMap<>();

				params.put("query",queryString);

				String response = interactWithServer("/rlogistics-execution/rlservice/packaging-material/barcode/generate", params);

				try {
					BarcodeGenerationResponse barcodeGenerationResponse = mapper.readValue(response, BarcodeGenerationResponse.class);

					if(!barcodeGenerationResponse.getResponse().isSuccess()){
						JOptionPane.showMessageDialog(instance.frame,
								"Server Error:" + barcodeGenerationResponse.getMessage(),
							    "Colud not generate barcodes.",
							    JOptionPane.ERROR_MESSAGE);
					} else {
						int itemCount = barcodeGenerationResponse.getResponse().getBarcodeGeneration().getItemCount();
						int startingBarcodeRange = barcodeGenerationResponse.getResponse().getBarcodeGeneration().getStartingBarcodeRange();

						StringBuffer barcodes = new StringBuffer();
						for(int i = 0; i< itemCount;i+=2){
							String barcode = location.getCode().toUpperCase() + packagingType.getCode().toUpperCase() + (startingBarcodeRange + i);

							String label = "^CF0,30,30^FO190,160\n\r" + "^FD" + barcode + "^FS" + "\n\r";

							String barcode2 = null;
							String label2 = null;
							if((i+1)<itemCount) {
								barcode2 = location.getCode().toUpperCase() + packagingType.getCode().toUpperCase() + (startingBarcodeRange + i + 1);

								label2 = "^CF0,30,30^FO550,160\n\r" + "^FD" + barcode2 + "^FS" + "\n\r";
							}

							String commands = "";

							if(barcode2 != null) {
								commands = "^XA\n\r" +
										"^FO" + configuration.getProperty("fieldOriginX") + "," + configuration.getProperty("fieldOriginY") + "^BY3\n\r" +
										"^BUN," + configuration.getProperty("barcodeHeight") + "Y,Y\n\r" +
										"^FD" + barcode + "^FS" +


										"^FO" + configuration.getProperty("SecondLabelOffset") + "," + configuration.getProperty("fieldOriginY") + "^BY3\n\r" +
										"^BUN," + configuration.getProperty("barcodeHeight") + "Y,Y\n\r" +
										"^FD" + barcode2 + "^FS" +
										"^XZ";
							} else {
								commands = "^XA\n\r" +
										"^FO" + configuration.getProperty("fieldOriginX") + "," + configuration.getProperty("fieldOriginY") + "^BY3\n\r" +
										"^BUN," + configuration.getProperty("barcodeHeight") + "Y,Y\n\r" +
										"^FD" + barcode + "^FS" +
										"^XZ";
							}


							System.out.println("C:" + commands);
							Doc doc = new SimpleDoc(commands.getBytes(), DocFlavor.BYTE_ARRAY.AUTOSENSE, null);

							if(printService != null){
								printService.createPrintJob().print(doc, null);
							}

							barcodes.append((i > 0 ? ", " : "") + barcode + ", " + barcode2);
						}

						JTextArea ta = new JTextArea(5,40);
						ta.setEditable(false);
						ta.setLineWrap(true);
						ta.setText(barcodes.toString());
						JOptionPane.showMessageDialog(instance.frame,
								new JScrollPane(ta),
								"Barcodes generated.",
							    JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(instance.frame,
							"Server Error:" + e.getMessage(),
						    "Colud not generate barcodes.",
						    JOptionPane.ERROR_MESSAGE);
				} catch (PrintException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(instance.frame,
							"Print Error:" + e.getMessage(),
						    "Colud not print barcodes.",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		frame.pack();
	}
	//TODO login page

	private void initPrinterService(){
		for(PrintService ps:PrintServiceLookup.lookupPrintServices(null, null)){
			if(ps.getName().toLowerCase().indexOf("zdesigner") >= 0){
				printService = ps;
				break;
			}
		}
	}


	public static void main(String[] args) {

		instance.init();

		instance.initFrame();

		instance.initPrinterService();

		if(instance.printService == null){
			JOptionPane.showMessageDialog(instance.frame,
				    "Colud not find the printer.",
				    "Printer Error",
				    JOptionPane.ERROR_MESSAGE);
			
			/*
			instance.destroyFrame();
			return;
			*/
		}

		instance.displayControls();
	}

}
