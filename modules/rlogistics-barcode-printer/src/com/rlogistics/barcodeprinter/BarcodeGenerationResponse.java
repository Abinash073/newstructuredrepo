package com.rlogistics.barcodeprinter;

public class BarcodeGenerationResponse {

	private Response response;
	private String message;
	
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static class Response{
		private boolean success;
		private BarcodeData barcodeGeneration;
		
		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public BarcodeData getBarcodeGeneration() {
			return barcodeGeneration;
		}

		public void setBarcodeGeneration(BarcodeData barcodeGeneration) {
			this.barcodeGeneration = barcodeGeneration;
		}
		public static class BarcodeData{
			private int itemCount;
			private String startingBarcode;
			private String endingBarcode;
			private int startingBarcodeRange;
			private int endingBarcodeRange;
			
			public int getStartingBarcodeRange() {
				return startingBarcodeRange;
			}
			public void setStartingBarcodeRange(int startingBarcodeRange) {
				this.startingBarcodeRange = startingBarcodeRange;
			}
			public int getEndingBarcodeRange() {
				return endingBarcodeRange;
			}
			public void setEndingBarcodeRange(int endingBarcodeRange) {
				this.endingBarcodeRange = endingBarcodeRange;
			}
			public void setStartingBarcode(String startingBarcode) {
				this.startingBarcode = startingBarcode;
			}
			public void setEndingBarcode(String endingBarcode) {
				this.endingBarcode = endingBarcode;
			}
			public int getItemCount() {
				return itemCount;
			}
			public void setItemCount(int itemCount) {
				this.itemCount = itemCount;
			}
			public String getStartingBarcode() {
				return startingBarcode;
			}
			public String getEndingBarcode() {
				return endingBarcode;
			}
			
		}

	}
}
