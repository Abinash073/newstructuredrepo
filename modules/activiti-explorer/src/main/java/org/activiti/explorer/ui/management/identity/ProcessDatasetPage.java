/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.activiti.explorer.ui.management.identity;

import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.navigation.rlogistics.ProcessDatasetNavigator;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.management.identity.rlogistics.NewProcessDatasetPopupWindow;
import org.activiti.explorer.ui.management.identity.rlogistics.ProcessDatasetListQuery;
import org.activiti.explorer.ui.management.rlogistics.DatasetEntitiesMenuBar;
import org.activiti.explorer.ui.management.rlogistics.DatasetEntityManagementPage;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;

/**
 * Page for managing datasets.
 * 
 * @author Joram Barrez
 */
public class ProcessDatasetPage extends DatasetEntityManagementPage {

	private static final long serialVersionUID = 1L;
	protected String datasetId;
	protected Table datasetTable;
	protected LazyLoadingQuery datasetListQuery;
	protected LazyLoadingContainer datasetListContainer;

	public ProcessDatasetPage() {
		ExplorerApp.get().setCurrentUriFragment(new UriFragment(ProcessDatasetNavigator.DATASET_URI_PART));
	}

	public ProcessDatasetPage(String datasetId) {
		this.datasetId = datasetId;
	}

	@Override
	protected void initUi() {
		super.initUi();
		
		if (datasetId == null) {
			selectElement(0);
		} else {
			selectElement(datasetListContainer.getIndexForObjectId(datasetId));
		}

		initCreateDatasetButton();
	}

	protected Table createList() {
		datasetTable = new Table();

		datasetListQuery = new ProcessDatasetListQuery();
		datasetListContainer = new LazyLoadingContainer(datasetListQuery, 30);
		datasetTable.setContainerDataSource(datasetListContainer);

		// Column headers
		datasetTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		datasetTable.setColumnWidth("icon", 22);
		datasetTable.addContainerProperty("id", String.class, null);
		datasetTable.addContainerProperty("name", String.class, null);
		datasetTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a dataset
		datasetTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = datasetTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String datasetId = (String) item.getItemProperty("id").getValue();
					String datasetName = (String) item.getItemProperty("name").getValue();
					((DatasetEntitiesMenuBar)getToolBar()).setDataset(datasetName);
					setDetailComponent(new ProcessDatasetDetailPanel(ProcessDatasetPage.this, datasetId));

					// Update URL
					ExplorerApp.get().setCurrentUriFragment(
							new UriFragment(ProcessDatasetNavigator.DATASET_URI_PART, datasetId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					ExplorerApp.get()
							.setCurrentUriFragment(new UriFragment(ProcessDatasetNavigator.DATASET_URI_PART));
				}
			}
		});

		return datasetTable;
	}

	/**
	 * Call when some dataset data has been changed
	 */
	public void notifyDatasetChanged(String datasetId) {
		// Clear cache
		datasetTable.removeAllItems();
		datasetListContainer.removeAllItems();

		datasetTable.select(datasetListContainer.getIndexForObjectId(datasetId));
	}

	public void initCreateDatasetButton() {
		Button createDatasetButton = new Button(ExplorerApp.get().getI18nManager().getMessage(Messages.DATASET_CREATE));
		createDatasetButton.setIcon(Images.USER_16);

		createDatasetButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				NewProcessDatasetPopupWindow newProcessDatasetPopupWindow = new NewProcessDatasetPopupWindow();
				ExplorerApp.get().getViewManager().showPopupWindow(newProcessDatasetPopupWindow);
			}
		});

		getToolBar().removeAllButtons();
		getToolBar().addButton(createDatasetButton);
	}

}
