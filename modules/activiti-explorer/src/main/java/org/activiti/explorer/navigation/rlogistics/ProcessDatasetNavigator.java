package org.activiti.explorer.navigation.rlogistics;

import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

public class ProcessDatasetNavigator extends ManagementNavigator {

	public static final String DATASET_URI_PART = "processdataset";

	public String getTrigger() {
		return DATASET_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String datasetId = uriFragment.getUriPart(1);

		if (datasetId != null) {
			ExplorerApp.get().getViewManager().showProcessDatasetPage(datasetId);
		} else {
			ExplorerApp.get().getViewManager().showProcessDatasetPage();
		}
	}
}
