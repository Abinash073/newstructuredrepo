package org.activiti.explorer.navigation.rlogistics;

import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.navigation.ManagementNavigator;
import org.activiti.explorer.navigation.UriFragment;

public class ProcessRoleNavigator extends ManagementNavigator {

	public static final String ROLE_URI_PART = "processrole";

	public String getTrigger() {
		return ROLE_URI_PART;
	}

	public void handleManagementNavigation(UriFragment uriFragment) {
		String dataset = uriFragment.getUriPart(1);
		String roleId = uriFragment.getUriPart(2);


		if (roleId != null) {
			ExplorerApp.get().getViewManager().showProcessRolePage(dataset,roleId);
		} else {
			ExplorerApp.get().getViewManager().showProcessRolePage(dataset);
		}
	}
}
