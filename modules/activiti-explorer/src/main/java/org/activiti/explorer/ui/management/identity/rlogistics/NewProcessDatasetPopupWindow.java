package org.activiti.explorer.ui.management.identity.rlogistics;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.I18nManager;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.PopupWindow;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

public class NewProcessDatasetPopupWindow extends PopupWindow {

	private static final long serialVersionUID = 1L;
	protected transient MetadataService metadataService;
	protected I18nManager i18nManager;
	protected Form form;

	public NewProcessDatasetPopupWindow() {
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		this.i18nManager = ExplorerApp.get().getI18nManager();

		setCaption(i18nManager.getMessage(Messages.DATASET_CREATE));
		setModal(true);
		center();
		setResizable(false);
		setWidth(275, UNITS_PIXELS);
		setHeight(300, UNITS_PIXELS);
		addStyleName(Reindeer.WINDOW_LIGHT);

		initEnterKeyListener();
		initForm();
	}

	protected void initEnterKeyListener() {
		addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				handleFormSubmit();
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { new ShortcutAction("enter", ShortcutAction.KeyCode.ENTER, null) };
			}
		});
	}

	protected void initForm() {
		form = new Form();
		form.setValidationVisibleOnCommit(true);
		form.setImmediate(true);
		addComponent(form);

		initInputFields();
		initCreateButton();
	}

	protected void initInputFields() {
		form.addField("name", new TextField(i18nManager.getMessage(Messages.DATASET_NAME)));
		form.getField("name").focus();
	}

	protected void initCreateButton() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().addComponent(buttonLayout);

		Button createButton = new Button(i18nManager.getMessage(Messages.DATASET_CREATE));
		buttonLayout.addComponent(createButton);
		buttonLayout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

		createButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				handleFormSubmit();
			}
		});
	}

	protected void handleFormSubmit() {
		try {
			// create dataset
			form.commit(); // will throw exception in case validation is false
			ProcessDataset dataset = createDataset();

			// close popup and navigate to fresh dataset
			close();
			ExplorerApp.get().getViewManager().showProcessDatasetPage(dataset.getId());
		} catch (InvalidValueException e) {
			// Do nothing: the Form component will render the errormsgs
			// automatically
			setHeight(340, UNITS_PIXELS);
		}
	}

	protected ProcessDataset createDataset() {
		ProcessDataset dataset = metadataService.newProcessDataset();
		if (form.getField("name").getValue() != null) {
			dataset.setName(form.getField("name").getValue().toString());
		}
		metadataService.saveProcessDataset(dataset);
		return dataset;
	}

}
