package org.activiti.explorer.ui.form;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.MultilineBarcodeFormType;
import org.activiti.explorer.Messages;

import com.vaadin.ui.Field;
import com.vaadin.ui.TextArea;

/**
 * @author Frederik Heremans
 */
public class MultilineBarcodeFormPropertyRenderer extends AbstractFormPropertyRenderer {

	public MultilineBarcodeFormPropertyRenderer() {
	    super(MultilineBarcodeFormType.class);
	  }

	@Override
	public Field getPropertyField(FormProperty formProperty) {
		TextArea textField = new TextArea(getPropertyLabel(formProperty));
	    textField.setRequired(formProperty.isRequired());
	    textField.setEnabled(formProperty.isWritable());
	    textField.setRequiredError(getMessage(Messages.FORM_FIELD_REQUIRED, getPropertyLabel(formProperty)));

	    if (formProperty.getValue() != null) {
	      textField.setValue(formProperty.getValue());
	    }

	    return textField;
	}
}
