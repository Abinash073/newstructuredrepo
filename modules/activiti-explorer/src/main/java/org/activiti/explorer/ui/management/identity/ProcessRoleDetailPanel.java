/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.activiti.explorer.ui.management.identity;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.I18nManager;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.ConfirmationDialogPopupWindow;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ProcessRoleDetailPanel extends DetailPanel implements MemberShipChangeListener {

	private static final long serialVersionUID = 1L;

	protected transient MetadataService metadataService;
	protected I18nManager i18nManager;

	protected ProcessRolePage rolePage;
	protected ProcessRole role;

	protected boolean editingDetails;
	protected HorizontalLayout roleDetailsLayout;
	protected TextField nameField;
	protected TextField parentRoleIdField;

	public ProcessRoleDetailPanel(ProcessRolePage rolePage, String roleId) {
		this.rolePage = rolePage;
		this.i18nManager = ExplorerApp.get().getI18nManager();
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		this.role = metadataService.createProcessRoleQuery().id(roleId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initRoleDetails();

		initActions();
	}

	protected void initActions() {
		rolePage.initCreateRoleButton();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded roleImage = new Embedded(null, Images.USER_50);
		layout.addComponent(roleImage);

		Label roleName = new Label(role.getName() + " " + role.getName());
		roleName.setSizeUndefined();
		roleName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(roleName);
		layout.setComponentAlignment(roleName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(roleName, 1.0f);
	}

	protected void initRoleDetails() {
		Label roleDetailsHeader = new Label(i18nManager.getMessage(Messages.ROLE_HEADER_DETAILS));
		roleDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		roleDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(roleDetailsHeader);

		// Details: picture and basic info
		roleDetailsLayout = new HorizontalLayout();
		roleDetailsLayout.setSpacing(true);
		roleDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(roleDetailsLayout);

		populateRoleDetails();
	}

	protected void populateRoleDetails() {
		loadRoleDetails();
		initDetailsActions();
	}

	protected void loadRoleDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		roleDetailsLayout.addComponent(detailGrid);

		// Details
		addRoleDetail(detailGrid, i18nManager.getMessage(Messages.ROLE_ID), new Label(role.getId())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addRoleDetail(detailGrid, i18nManager.getMessage(Messages.ROLE_NAME),
					new Label(role.getName()));
			addRoleDetail(detailGrid, i18nManager.getMessage(Messages.ROLE_PARENT_ROLE_ID),
					new Label(role.getParentRoleId()));
		} else {
			nameField = new TextField(null, role.getName() != null ? role.getName() : "");
			addRoleDetail(detailGrid, i18nManager.getMessage(Messages.ROLE_NAME), nameField);
			nameField.focus();
			parentRoleIdField = new TextField(null, role.getParentRoleId() != null ? role.getParentRoleId() : "");
			addRoleDetail(detailGrid, i18nManager.getMessage(Messages.ROLE_PARENT_ROLE_ID), parentRoleIdField);
		}
	}

	protected void addRoleDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		roleDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				roleDetailsLayout.removeAllComponents();
				populateRoleDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalName = role.getName();

				// Change data
				role.setName(nameField.getValue().toString());
				String parentRoleIdString = parentRoleIdField.getValue().toString();
				role.setParentRoleId(parentRoleIdString.equals("") ? null : parentRoleIdString);
				metadataService.saveProcessRole(role);

				// Refresh detail panel
				editingDetails = false;
				roleDetailsLayout.removeAllComponents();
				populateRoleDetails();

				// Refresh task list (only if name was changed)
				if (nameChanged(originalName)) {
					rolePage.notifyRoleChanged(role.getId());
				}
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		if (originalName != null) {
			nameChanged = !originalName.equals(role.getName());
		} else {
			nameChanged = role.getName() != null;
		}

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.USER_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				ConfirmationDialogPopupWindow confirmPopup = new ConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.USER_CONFIRM_DELETE, role.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete role from database
						metadataService.deleteProcessRole(MetadataService.DEFAULT_DATASET,MetadataService.UNSET_DS_VERSION,role.getId());

						// Update ui
						rolePage.refreshSelectNext();
					}
				});

				ExplorerApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}

	@Override
	public void notifyMembershipChanged() {
	}

}
