package org.activiti.explorer.ui.management.identity.rlogistics;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.I18nManager;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.custom.PopupWindow;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Reindeer;

public class NewProcessRolePopupWindow extends PopupWindow {

	private static final long serialVersionUID = 1L;
	protected transient MetadataService metadataService;
	protected I18nManager i18nManager;
	protected Form form;
	protected String dataset;

	public NewProcessRolePopupWindow(String dataset) {
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		this.i18nManager = ExplorerApp.get().getI18nManager();
		this.dataset = dataset;
		
		setCaption(i18nManager.getMessage(Messages.ROLE_CREATE));
		setModal(true);
		center();
		setResizable(false);
		setWidth(275, UNITS_PIXELS);
		setHeight(300, UNITS_PIXELS);
		addStyleName(Reindeer.WINDOW_LIGHT);

		initEnterKeyListener();
		initForm();
	}

	protected void initEnterKeyListener() {
		addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				handleFormSubmit();
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { new ShortcutAction("enter", ShortcutAction.KeyCode.ENTER, null) };
			}
		});
	}

	protected void initForm() {
		form = new Form();
		form.setValidationVisibleOnCommit(true);
		form.setImmediate(true);
		addComponent(form);

		initInputFields();
		initCreateButton();
	}

	protected void initInputFields() {
		form.addField("name", new TextField(i18nManager.getMessage(Messages.ROLE_NAME)));
		form.getField("name").focus();

		form.addField("parentRoleId", new TextField(i18nManager.getMessage(Messages.ROLE_PARENT_ROLE_ID)));
	}

	protected void initCreateButton() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().setWidth(100, UNITS_PERCENTAGE);
		form.getFooter().addComponent(buttonLayout);

		Button createButton = new Button(i18nManager.getMessage(Messages.ROLE_CREATE));
		buttonLayout.addComponent(createButton);
		buttonLayout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

		createButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				handleFormSubmit();
			}
		});
	}

	protected void handleFormSubmit() {
		try {
			// create role
			form.commit(); // will throw exception in case validation is false
			ProcessRole role = createRole();

			// close popup and navigate to fresh role
			close();
			ExplorerApp.get().getViewManager().showProcessRolePage(dataset,role.getId());
		} catch (InvalidValueException e) {
			// Do nothing: the Form component will render the errormsgs
			// automatically
			setHeight(340, UNITS_PIXELS);
		}
	}

	protected ProcessRole createRole() {
		ProcessRole role = metadataService.newProcessRole(dataset,MetadataService.UNSET_DS_VERSION);
		if (form.getField("name").getValue() != null) {
			role.setName(form.getField("name").getValue().toString());
		}
		String parentRoleId = form.getField("parentRoleId").getValue().toString();
		role.setParentRoleId(parentRoleId.equals("") ? null : parentRoleId);
		metadataService.saveProcessRole(role);
		return role;
	}

}
