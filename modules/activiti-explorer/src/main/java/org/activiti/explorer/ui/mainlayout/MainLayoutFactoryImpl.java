package org.activiti.explorer.ui.mainlayout;

public class MainLayoutFactoryImpl implements MainLayoutFactory{
	@Override
	public MainLayout createLayout() {
		return new MainLayoutImpl();
	}
}
