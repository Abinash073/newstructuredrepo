package org.activiti.explorer.ui.management.deployment.rlogistics;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.ExtensionAttribute;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowElementsContainer;
import org.activiti.bpmn.model.UserTask;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.repository.Model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.terminal.DownloadStream;

public class ConfigurationExporter {

	protected transient RepositoryService repositoryService;
	protected transient MetadataService metadataService; 
	public ConfigurationExporter(){
		this.repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
	}
	public DownloadStream exportConfiguration() {
		String stamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		File tempDir = null;
		Map<String,File> dirByDataset = new HashMap<String, File>();
		
		try {
			tempDir = Files.createTempDirectory("RLogisticsConfiguration-" + stamp).toFile();
		} catch(Exception ex){
			throw new RuntimeException("Exception while creating a temp dir",ex);
		}
		
		/* Create per dataset directories*/
		try {
			List<ProcessDataset> datasets = metadataService.createProcessDatasetQuery().orderByName().asc().list();
			
			for(ProcessDataset dataset:datasets){
				File datasetDir = new File(tempDir,dataset.getName() + "-" + stamp);
				datasetDir.mkdir();
				dirByDataset.put(dataset.getName(),datasetDir);
			}
		}catch(Exception ex){
			throw new RuntimeException("Exception while creating a dataset dirs",ex);
		}
		
		/* export process definitions */
		try {
		    List<Model> modelList = repositoryService.createModelQuery().list();
			for (Model modelData : modelList) {

				JsonNode editorNode = new ObjectMapper()
						.readTree(repositoryService.getModelEditorSource(modelData.getId()));
				BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
				BpmnModel model = jsonConverter.convertToBpmnModel(editorNode);
				
				if(model.getMainProcess() == null){
					continue;
				}

				String dataset = model.getDefinitionsAttributeValue(BpmnJsonConverter.RL_NAMESPACE,"dataset");
				if((dataset == null) || (dirByDataset.get(dataset) == null)){
					continue;
				}
				
				{
					/* Set dsVersion so that upon import on E1, there is no need to set */
					List<ExtensionAttribute> atts = new ArrayList<ExtensionAttribute>();
					ExtensionAttribute att = new ExtensionAttribute(BpmnJsonConverter.RL_NAMESPACE,"dsVersion");
					att.setNamespacePrefix(BpmnJsonConverter.RL_NAMESPACE_PREFIX);
					att.setValue(stamp);
					atts.add(att);
					model.getDefinitionsAttributes().put("dsVersion",atts);
				}
				
				{
					/* Fix any empty names of tasks */
					fixTaskNames(model);
				}
				
				File exportFile = new File(dirByDataset.get(dataset), model.getMainProcess().getId() + ".bpmn20.xml");
				
				OutputStream out = new FileOutputStream(exportFile);
				out.write(new BpmnXMLConverter().convertToXML(model));
				out.flush();
				out.close();
			}
		} catch(Exception ex){
			throw new RuntimeException("Exception while exporting process definitions",ex);
		}

		/* Export csv data */
		try{
			for(String dataset:dirByDataset.keySet()){
				List<ProcessRole> roles = metadataService.createProcessRoleQuery().dataset(dataset).orderByName().asc().list();
				File exportFile = new File(dirByDataset.get(dataset), "roles.csv");
				PrintStream out = new PrintStream(new FileOutputStream(exportFile));
				for(ProcessRole role:roles){
					out.println("ROLE,INSERT,\"" + role.getName() + "\",\"" + role.getParentRoleId() + "\"");
				}
				out.flush();
				out.close();
			}
		} catch(Exception ex){
			throw new RuntimeException("Exception while exporting roles",ex);
		}
	
		byte[] buffer = new byte[1024];
		DownloadStream ds = null;
		
		try {
			File jarFile = File.createTempFile("RLogisticsConfiguration-" + stamp, ".jar");
			JarOutputStream out = new JarOutputStream(new FileOutputStream(jarFile));						
			
			for(File datasetDir:tempDir.listFiles()){
				for(File componentFile:datasetDir.listFiles()){
					JarEntry entry = new JarEntry(datasetDir.getName() + "/" + componentFile.getName());
					entry.setTime(componentFile.lastModified());
					out.putNextEntry(entry);
					
					BufferedInputStream bis = new BufferedInputStream(new FileInputStream(componentFile));
					int count;
					while((count = bis.read(buffer)) != -1){
						out.write(buffer, 0, count);
					}
					
					out.closeEntry();
					
					bis.close();
				}
			}			
			out.flush();
			out.close();
			
			String formalJarFilename = "RLogisticsConfiguration-" + stamp + ".jar";
			ds = new DownloadStream(new FileInputStream(jarFile), "application/zip", formalJarFilename);
	        ds.setParameter("Content-Disposition", "attachment; filename=" + formalJarFilename);
		} catch(Exception ex) {
			throw new RuntimeException("Exception while building export configuration jar",ex);
		}
		
		return ds;
	}

	private void fixTaskNames(BpmnModel model) {
		for(org.activiti.bpmn.model.Process process:model.getProcesses()){
			int index = 0;
			for(FlowElement flowElement:process.getFlowElements()){
				fixTaskNames(process.getName(),1,index++,flowElement);
			}
		}
	}
	
	private void fixTaskNames(String processName,int level,int index,FlowElement flowElement) {
		if(flowElement instanceof FlowElementsContainer){
			int innerIndex = 0;
			for(FlowElement childFlowElement:((FlowElementsContainer)flowElement).getFlowElements()){
				fixTaskNames(processName,level+1,index++,childFlowElement);
			}
		} else if(flowElement instanceof UserTask){
			UserTask userTask = (UserTask) flowElement;
			
			if((userTask.getName() == null) || (userTask.getName().equals(""))){
				userTask.setName(processName + "-" + level + "-" + index);
			}
		}
	}
}
