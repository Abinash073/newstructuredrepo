package org.activiti.explorer.form;

import com.vaadin.ui.Field;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.ButtonFormType;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.form.AbstractFormPropertyRenderer;

import com.vaadin.ui.Button;
import com.vaadin.ui.themes.Reindeer;

public class ButtonFormPropertyRenderer extends AbstractFormPropertyRenderer {
    public ButtonFormPropertyRenderer(){super(ButtonFormType.class);}

    @Override
    public Field getPropertyField(FormProperty formProperty) {
        Button button=new Button("Call Customer");
        button.setRequired(formProperty.isRequired());
        button.setEnabled(formProperty.isWritable());
        button.addStyleName(Reindeer.BUTTON_DEFAULT);
        //button.click();
        /**
         * Trigger a event
         */
        //button.addClickListener((Button.ClickListener) event -> console.log();


        return button;


    }





}