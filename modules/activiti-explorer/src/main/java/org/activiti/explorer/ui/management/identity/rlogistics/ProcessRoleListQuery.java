package org.activiti.explorer.ui.management.identity.rlogistics;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessRole;
import org.activiti.engine.identity.rlogistics.ProcessRoleQuery;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProcessRoleListQuery extends AbstractLazyLoadingQuery {

	protected String dataset;
	protected transient MetadataService metadataService;

	public ProcessRoleListQuery(String dataset) {
	    this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
	    this.dataset = dataset;
	  }

	public int size() {
		return (int) metadataService.createProcessRoleQuery().dataset(dataset).count();
	}

	public List<Item> loadItems(int start, int count) {
		ProcessRoleQuery query = metadataService.createProcessRoleQuery();		
		query.dataset(dataset);
		List<ProcessRole> roles = query.orderByName().asc().orderByName().asc().listPage(start, count);

		List<Item> roleListItems = new ArrayList<Item>();
		for (ProcessRole role : roles) {
			roleListItems.add(new ProcessRoleListItem(role));
		}
		return roleListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProcessRoleListItem(metadataService.createProcessRoleQuery().dataset(dataset).id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProcessRoleListItem extends PropertysetItem implements Comparable<ProcessRoleListItem> {

		private static final long serialVersionUID = 1L;

		public ProcessRoleListItem(ProcessRole role) {
			addItemProperty("id", new ObjectProperty<String>(role.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(role.getName(), String.class));
		}

		public int compareTo(ProcessRoleListItem other) {
			// Roles are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
