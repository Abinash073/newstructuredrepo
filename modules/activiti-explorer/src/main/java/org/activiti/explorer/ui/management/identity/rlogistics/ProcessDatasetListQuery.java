package org.activiti.explorer.ui.management.identity.rlogistics;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.explorer.data.AbstractLazyLoadingQuery;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

public class ProcessDatasetListQuery extends AbstractLazyLoadingQuery {

	protected transient MetadataService metadataService;

	public ProcessDatasetListQuery() {
	    this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
	  }

	public int size() {
		return (int) metadataService.createProcessDatasetQuery().count();
	}

	public List<Item> loadItems(int start, int count) {
		List<ProcessDataset> datasets = metadataService.createProcessDatasetQuery().orderByName().asc().listPage(start, count);

		List<Item> datasetListItems = new ArrayList<Item>();
		for (ProcessDataset dataset : datasets) {
			datasetListItems.add(new ProcessDatasetListItem(dataset));
		}
		return datasetListItems;
	}

	public Item loadSingleResult(String id) {
		return new ProcessDatasetListItem(metadataService.createProcessDatasetQuery().id(id).singleResult());
	}

	public void setSorting(Object[] propertyIds, boolean[] ascending) {
		throw new UnsupportedOperationException();
	}

	class ProcessDatasetListItem extends PropertysetItem implements Comparable<ProcessDatasetListItem> {

		private static final long serialVersionUID = 1L;

		public ProcessDatasetListItem(ProcessDataset dataset) {
			addItemProperty("id", new ObjectProperty<String>(dataset.getId(), String.class));
			addItemProperty("name", new ObjectProperty<String>(dataset.getName(), String.class));
		}

		public int compareTo(ProcessDatasetListItem other) {
			// Datasets are ordered by default by firstname + lastname, and then on
			// id
			String name = (String) getItemProperty("name").getValue();
			String otherName = (String) other.getItemProperty("name").getValue();

			int comparison = name.compareTo(otherName);
			if (comparison != 0) {
				return comparison;
			} else {
				String id = (String) getItemProperty("id").getValue();
				String otherId = (String) other.getItemProperty("id").getValue();
				return id.compareTo(otherId);
			}
		}

	}

}
