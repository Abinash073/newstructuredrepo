/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.activiti.explorer.ui.management.identity;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.rlogistics.MetadataService;
import org.activiti.engine.identity.rlogistics.ProcessDataset;
import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.I18nManager;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.custom.ConfirmationDialogPopupWindow;
import org.activiti.explorer.ui.custom.DetailPanel;
import org.activiti.explorer.ui.event.ConfirmationEvent;
import org.activiti.explorer.ui.event.ConfirmationEventListener;
import org.activiti.explorer.ui.mainlayout.ExplorerLayout;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Joram Barrez
 */
public class ProcessDatasetDetailPanel extends DetailPanel implements MemberShipChangeListener {

	private static final long serialVersionUID = 1L;

	protected transient MetadataService metadataService;
	protected I18nManager i18nManager;

	protected ProcessDatasetPage datasetPage;
	protected ProcessDataset dataset;

	protected boolean editingDetails;
	protected HorizontalLayout datasetDetailsLayout;
	protected TextField nameField;
	protected TextField parentDatasetIdField;

	public ProcessDatasetDetailPanel(ProcessDatasetPage datasetPage, String datasetId) {
		this.datasetPage = datasetPage;
		this.i18nManager = ExplorerApp.get().getI18nManager();
		this.metadataService = ProcessEngines.getDefaultProcessEngine().getMetadataService();
		this.dataset = metadataService.createProcessDatasetQuery().id(datasetId).singleResult();

		init();
	}

	protected void init() {
		setSizeFull();
		addStyleName(Reindeer.PANEL_LIGHT);

		initPageTitle();
		initDatasetDetails();

		initActions();
	}

	protected void initActions() {
		datasetPage.initCreateDatasetButton();
	}

	protected void initPageTitle() {
		HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, UNITS_PERCENTAGE);
		layout.setSpacing(true);
		layout.setMargin(false, false, true, false);
		layout.addStyleName(ExplorerLayout.STYLE_TITLE_BLOCK);
		addDetailComponent(layout);

		Embedded datasetImage = new Embedded(null, Images.USER_50);
		layout.addComponent(datasetImage);

		Label datasetName = new Label(dataset.getName() + " " + dataset.getName());
		datasetName.setSizeUndefined();
		datasetName.addStyleName(Reindeer.LABEL_H2);
		layout.addComponent(datasetName);
		layout.setComponentAlignment(datasetName, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(datasetName, 1.0f);
	}

	protected void initDatasetDetails() {
		Label datasetDetailsHeader = new Label(i18nManager.getMessage(Messages.DATASET_HEADER_DETAILS));
		datasetDetailsHeader.addStyleName(ExplorerLayout.STYLE_H3);
		datasetDetailsHeader.addStyleName(ExplorerLayout.STYLE_DETAIL_BLOCK);
		addDetailComponent(datasetDetailsHeader);

		// Details: picture and basic info
		datasetDetailsLayout = new HorizontalLayout();
		datasetDetailsLayout.setSpacing(true);
		datasetDetailsLayout.setMargin(false, false, true, false);
		addDetailComponent(datasetDetailsLayout);

		populateDatasetDetails();
	}

	protected void populateDatasetDetails() {
		loadDatasetDetails();
		initDetailsActions();
	}

	protected void loadDatasetDetails() {
		// Grid of details
		GridLayout detailGrid = new GridLayout();
		detailGrid.setColumns(2);
		detailGrid.setSpacing(true);
		detailGrid.setMargin(true, true, false, true);
		datasetDetailsLayout.addComponent(detailGrid);

		// Details
		addDatasetDetail(detailGrid, i18nManager.getMessage(Messages.DATASET_ID), new Label(dataset.getId())); // details
																													// are
																													// non-editable
		if (!editingDetails) {
			addDatasetDetail(detailGrid, i18nManager.getMessage(Messages.DATASET_NAME),
					new Label(dataset.getName()));
		} else {
			nameField = new TextField(null, dataset.getName() != null ? dataset.getName() : "");
			addDatasetDetail(detailGrid, i18nManager.getMessage(Messages.DATASET_NAME), nameField);
			nameField.focus();
		}
	}

	protected void addDatasetDetail(GridLayout detailLayout, String detail, Component value) {
		Label label = new Label(detail + ": ");
		label.addStyleName(ExplorerLayout.STYLE_LABEL_BOLD);
		detailLayout.addComponent(label);
		detailLayout.addComponent(value);
	}

	protected void initDetailsActions() {
		VerticalLayout actionLayout = new VerticalLayout();
		actionLayout.setSpacing(true);
		actionLayout.setMargin(false, false, false, true);
		datasetDetailsLayout.addComponent(actionLayout);

		if (!editingDetails) {
			initEditButton(actionLayout);
			initDeleteButton(actionLayout);
		} else {
			initSaveButton(actionLayout);
		}
	}

	protected void initEditButton(VerticalLayout actionLayout) {
		Button editButton = new Button(i18nManager.getMessage(Messages.USER_EDIT));
		editButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(editButton);
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				editingDetails = true;
				datasetDetailsLayout.removeAllComponents();
				populateDatasetDetails(); // the layout will be populated
											// differently since the
											// 'editingDetails' boolean is set
			}
		});
	}

	protected void initSaveButton(VerticalLayout actionLayout) {
		Button saveButton = new Button(i18nManager.getMessage(Messages.USER_SAVE));
		saveButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(saveButton);
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {

				String originalName = dataset.getName();

				// Change data
				dataset.setName(nameField.getValue().toString());
				metadataService.saveProcessDataset(dataset);

				// Refresh detail panel
				editingDetails = false;
				datasetDetailsLayout.removeAllComponents();
				populateDatasetDetails();

				// Refresh task list (only if name was changed)
				if (nameChanged(originalName)) {
					datasetPage.notifyDatasetChanged(dataset.getId());
				}
			}
		});
	}

	protected boolean nameChanged(String originalName) {
		boolean nameChanged = false;
		if (originalName != null) {
			nameChanged = !originalName.equals(dataset.getName());
		} else {
			nameChanged = dataset.getName() != null;
		}

		return nameChanged;
	}

	protected void initDeleteButton(VerticalLayout actionLayout) {
		Button deleteButton = new Button(i18nManager.getMessage(Messages.USER_DELETE));
		deleteButton.addStyleName(Reindeer.BUTTON_SMALL);
		actionLayout.addComponent(deleteButton);
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				ConfirmationDialogPopupWindow confirmPopup = new ConfirmationDialogPopupWindow(
						i18nManager.getMessage(Messages.USER_CONFIRM_DELETE, dataset.getId()));

				confirmPopup.addListener(new ConfirmationEventListener() {
					protected void rejected(ConfirmationEvent event) {
					}

					protected void confirmed(ConfirmationEvent event) {
						// Delete dataset from database
						metadataService.deleteProcessDataset(dataset.getId());

						// Update ui
						datasetPage.refreshSelectNext();
					}
				});

				ExplorerApp.get().getViewManager().showPopupWindow(confirmPopup);
			}
		});
	}

	@Override
	public void notifyMembershipChanged() {
	}

}
