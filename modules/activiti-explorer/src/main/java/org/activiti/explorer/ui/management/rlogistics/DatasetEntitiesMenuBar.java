/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.activiti.explorer.ui.management.rlogistics;

import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.I18nManager;
import org.activiti.explorer.Messages;
import org.activiti.explorer.ViewManager;
import org.activiti.explorer.ui.custom.ToolBar;
import org.activiti.explorer.ui.custom.ToolbarEntry.ToolbarCommand;

/**
 * @author Joram Barrez
 */
public class DatasetEntitiesMenuBar extends ToolBar {

	private static final long serialVersionUID = 1L;

	public static final String ENTRY_PROCESSUSERS = "processusers";
	public static final String ENTRY_PROCESSLOCATIONS = "processlocations";
	public static final String ENTRY_PROCESSROLES = "processroles";
	public static final String ENTRY_PROCESSLOCATIONROLES = "processlocationroles";

	protected I18nManager i18nManager;
	protected ViewManager viewManager;
	
	protected String dataset;

	public DatasetEntitiesMenuBar() {
		this.i18nManager = ExplorerApp.get().getI18nManager();
		this.viewManager = ExplorerApp.get().getViewManager();
		setWidth("100%");
		
		initToolbarEntries();
	}

	public void setDataset(String dataset){
		this.dataset = dataset;
	}
	
	protected void initToolbarEntries() {
		addProcessRolesToolbarEntry();
	}

	protected void addProcessRolesToolbarEntry() {
		addToolbarEntry(ENTRY_PROCESSROLES, i18nManager.getMessage(Messages.MGMT_MENU_PROCESSROLES),
				new ToolbarCommand() {
					public void toolBarItemSelected() {
						viewManager.showProcessRolePage(dataset);
					}
				});
	}
}
