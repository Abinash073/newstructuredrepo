package org.activiti.explorer.ui.mainlayout;

public interface MainLayoutFactory {
	public MainLayout createLayout();
}
