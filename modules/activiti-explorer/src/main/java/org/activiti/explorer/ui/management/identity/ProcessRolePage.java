/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.activiti.explorer.ui.management.identity;

import org.activiti.explorer.ExplorerApp;
import org.activiti.explorer.Messages;
import org.activiti.explorer.data.LazyLoadingContainer;
import org.activiti.explorer.data.LazyLoadingQuery;
import org.activiti.explorer.navigation.UriFragment;
import org.activiti.explorer.navigation.rlogistics.ProcessRoleNavigator;
import org.activiti.explorer.ui.Images;
import org.activiti.explorer.ui.management.identity.rlogistics.NewProcessRolePopupWindow;
import org.activiti.explorer.ui.management.identity.rlogistics.ProcessRoleListQuery;
import org.activiti.explorer.ui.management.rlogistics.DatasetEntitiesMenuBar;
import org.activiti.explorer.ui.management.rlogistics.DatasetEntityManagementPage;
import org.activiti.explorer.ui.util.ThemeImageColumnGenerator;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;

/**
 * Page for managing roles.
 * 
 * @author Joram Barrez
 */
public class ProcessRolePage extends  DatasetEntityManagementPage{

	private static final long serialVersionUID = 1L;
	protected String dataset;
	protected String roleId;
	protected Table roleTable;
	protected LazyLoadingQuery roleListQuery;
	protected LazyLoadingContainer roleListContainer;

	public ProcessRolePage(String dataset) {
		this.dataset = dataset;
		ExplorerApp.get().setCurrentUriFragment(new UriFragment(ProcessRoleNavigator.ROLE_URI_PART));
	}

	public ProcessRolePage(String dataset,String roleId) {
		this.dataset = dataset;
		this.roleId = roleId;
	}

	@Override
	protected void initUi() {
		super.initUi();

		((DatasetEntitiesMenuBar)getToolBar()).setDataset(dataset);

		if (roleId == null) {
			selectElement(0);
		} else {
			selectElement(roleListContainer.getIndexForObjectId(roleId));
		}

		initCreateRoleButton();
	}

	protected Table createList() {
		roleTable = new Table();

		roleListQuery = new ProcessRoleListQuery(dataset);
		roleListContainer = new LazyLoadingContainer(roleListQuery, 30);
		roleTable.setContainerDataSource(roleListContainer);

		// Column headers
		roleTable.addGeneratedColumn("icon", new ThemeImageColumnGenerator(Images.USER_22));
		roleTable.setColumnWidth("icon", 22);
		roleTable.addContainerProperty("id", String.class, null);
		roleTable.addContainerProperty("name", String.class, null);
		roleTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);

		// Listener to change right panel when clicked on a role
		roleTable.addListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			public void valueChange(ValueChangeEvent event) {
				Item item = roleTable.getItem(event.getProperty().getValue()); // the
																					// value
																					// of
																					// the
																					// property
																					// is
																					// the
																					// itemId
																					// of
																					// the
																					// table
																					// entry
				if (item != null) {
					String roleId = (String) item.getItemProperty("id").getValue();
					setDetailComponent(new ProcessRoleDetailPanel(ProcessRolePage.this, roleId));

					// Update URL
					ExplorerApp.get().setCurrentUriFragment(
							new UriFragment(ProcessRoleNavigator.ROLE_URI_PART, roleId));
				} else {
					// Nothing is selected
					setDetailComponent(null);
					ExplorerApp.get()
							.setCurrentUriFragment(new UriFragment(ProcessRoleNavigator.ROLE_URI_PART));
				}
			}
		});

		return roleTable;
	}

	/**
	 * Call when some role data has been changed
	 */
	public void notifyRoleChanged(String roleId) {
		// Clear cache
		roleTable.removeAllItems();
		roleListContainer.removeAllItems();

		roleTable.select(roleListContainer.getIndexForObjectId(roleId));
	}

	public void initCreateRoleButton() {
		Button createRoleButton = new Button(ExplorerApp.get().getI18nManager().getMessage(Messages.ROLE_CREATE));
		createRoleButton.setIcon(Images.USER_16);

		createRoleButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				NewProcessRolePopupWindow newProcessRolePopupWindow = new NewProcessRolePopupWindow(dataset);
				ExplorerApp.get().getViewManager().showPopupWindow(newProcessRolePopupWindow);
			}
		});

		getToolBar().removeAllButtons();
		getToolBar().addButton(createRoleButton);
	}

}
