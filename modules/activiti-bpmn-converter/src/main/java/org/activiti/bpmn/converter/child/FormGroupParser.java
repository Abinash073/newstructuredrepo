/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.activiti.bpmn.converter.child;

import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.util.BpmnXMLUtil;
import org.activiti.bpmn.model.BaseElement;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FormGroup;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.bpmn.model.UserTask;

/**
 * @author Tijs Rademakers
 */
public class FormGroupParser extends BaseChildElementParser {

  public String getElementName() {
    return ELEMENT_FORMGROUP;
  }


  public boolean accepts(BaseElement element){
    return ((element instanceof UserTask)
        || (element instanceof StartEvent));
  }

  public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {

    if (!accepts(parentElement)) return;

    FormGroup group = new FormGroup();
    BpmnXMLUtil.addXMLLocation(group, xtr);
    group.setName(xtr.getAttributeValue(null, ATTRIBUTE_FORM_GROUP_NAME));
    group.setExpression(xtr.getAttributeValue(null, ATTRIBUTE_FORM_GROUP_EXPRESSION));    
    group.setBeforeGroupScript(xtr.getAttributeValue(null, ATTRIBUTE_FORM_GROUP_BEFORE_GROUP_SCRIPT));    
    group.setAfterGroupScript(xtr.getAttributeValue(null, ATTRIBUTE_FORM_GROUP_AFTER_GROUP_SCRIPT));    
    
    if (parentElement instanceof UserTask) {
      ((UserTask) parentElement).getFormGroups().add(group);
    } else {
      ((StartEvent) parentElement).getFormGroups().add(group);
    }
  }
}
