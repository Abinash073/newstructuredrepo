package com.rlogistics.master.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

import com.rlogistics.master.CustomQueriesMapper;
import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQuery;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({ "com.rlogistics.master.MasterEntity",
		"com.rlogistics.master.MasterEntityLeadsToRestController",
		"com.rlogistics.master.CustomQueriesMapper"})
public class MasterEntityAnnotationProcessor extends AbstractProcessor {

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		
		processMasterEntityAnnotations(roundEnv);
		
		processCustomQueriesMapperAnnotations(roundEnv);
		
		return true;
	}

	public void processCustomQueriesMapperAnnotations(RoundEnvironment roundEnv) {
		List<TypeElement> customQueriesMappers = new ArrayList<>();
		for (Element e : roundEnv.getElementsAnnotatedWith(CustomQueriesMapper.class)) {
			
			TypeElement entityTypeElement = (TypeElement) e;

			customQueriesMappers.add(entityTypeElement);
		}
		CustomQueriesServiceUtil.createCustomQueriesServiceClasses(processingEnv,customQueriesMappers);
	}

	public void processMasterEntityAnnotations(RoundEnvironment roundEnv) {
		List<TypeElement> masterEntities = new ArrayList<>();

		for (Element e : roundEnv.getElementsAnnotatedWith(MasterEntity.class)) {

			TypeElement entityTypeElement = (TypeElement) e;

			masterEntities.add(entityTypeElement);

			TypeElement entityQueryTypeElement = EntityClassUtil.locateQueryTypeElement(entityTypeElement);

			/* Create Entity class */
			EntityClassUtil.createEntityClass(processingEnv, entityTypeElement);

			/* Create Entity Manager Interface */
			IdentityManagerClassUtil.createIdentityManagerInterface(processingEnv, entityTypeElement,
					entityQueryTypeElement);

			if (entityQueryTypeElement != null) {
				QueryClassUtil.createEntityQueryImplClass(processingEnv, entityTypeElement, entityQueryTypeElement);
			}

			/* Create Entity Manager Interface */
			IdentityManagerClassUtil.createIdentityManagerImplementation(processingEnv, entityTypeElement,
					entityQueryTypeElement);

			/* Create session factory -- called manager factory */
			IdentityManagerClassUtil.createEntityManagerFactoryClass(processingEnv, entityTypeElement);

			/* Create command classes */
			CommandClassUtil.createCommandClasses(processingEnv, entityTypeElement, entityQueryTypeElement);

			/* Generate Table XML */
			DBResourcesUtil.createTableXML(processingEnv, entityTypeElement, entityQueryTypeElement);
		}

		MasterdataServiceUtil.createMasterServiceClasses(processingEnv, masterEntities);

		for (Element e : roundEnv.getElementsAnnotatedWith(MasterEntityLeadsToRestController.class)) {
			TypeElement entityTypeElement = (TypeElement) e;

			TypeElement entityQueryTypeElement = null;
			for (Element ee : e.getEnclosedElements()) {
				MasterEntityQuery meq = ee.getAnnotation(MasterEntityQuery.class);
				if (meq != null) {
					entityQueryTypeElement = (TypeElement) ee;
					break;
				}
			}

			RestInterfaceUtil.createRestInterfaceClasses(processingEnv, entityTypeElement, entityQueryTypeElement);
		}
	}
}
