package com.rlogistics.master.processor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

import com.rlogistics.master.MasterEntity;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;

public class IdentityManagerClassUtil {

	public static void createIdentityManagerInterface(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement, TypeElement entityQueryTypeElement){
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		/* Create Entity implemnetation */
		String entityInterfaceName = entityTypeElement.getSimpleName().toString();
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityIdentityManagerInterfaceName = entityInterfaceName + "IdentityManager";
		String entityInterfacePackage = packageElement.getQualifiedName().toString();
		String entityIdentityManagerInterfacePackage = entityInterfacePackage + ".impl";
	
		TypeSpec.Builder typeBuilder = TypeSpec.interfaceBuilder(entityIdentityManagerInterfaceName)
				.addModifiers(Modifier.PUBLIC);
		
		if(entityQueryTypeElement != null){
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("createNew" + entityQueryInterfaceName)
					.returns(ClassName.get(entityQueryTypeElement))
					.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
					.build()
			);
	
			
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "CountByQueryCriteria")
					.returns(ClassName.LONG)
					.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
					.addParameter(ClassName.get("",entityQueryInterfaceName + "Impl"),"query")
					.build()
			);
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "ByQueryCriteria")
					.returns(ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get(entityTypeElement)))
					.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
					.addParameter(ClassName.get("",entityQueryInterfaceName + "Impl"),"query")
					.addParameter(ClassName.get("org.activiti.engine.impl","Page"),"page")
					.build()
			);
			
		}
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("find" + entityInterfaceName + "ById")
				.returns(ClassName.get(entityTypeElement))
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.addParameter(ClassName.get(String.class),"id")
				.build()
		);
		
		if(QueryClassUtil.locateCodeSetterMethod(entityQueryTypeElement) != null){
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "ByCode")
					.returns(ClassName.get(entityTypeElement))
					.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
					.addParameter(ClassName.get(String.class),"code")
					.build()
			);
		}
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("delete" + entityInterfaceName)
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.addParameter(ClassName.get(String.class),"id")
				.build()
		);
		
		String entityVariableName = BeanUtil.initialLowerCase(entityTypeElement.getSimpleName().toString());
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("isNew" + entityInterfaceName)
				.returns(TypeName.BOOLEAN)
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("insert" + entityInterfaceName)
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.build()
		);
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("update" + entityInterfaceName)
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("createNew" + entityInterfaceName)
				.returns(ClassName.get(entityTypeElement))
				.addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC)
				.build()
		);
	
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityIdentityManagerInterfacePackage, typeBuilder);		
	}

	public static  void createIdentityManagerImplementation(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement, TypeElement entityQueryTypeElement) {
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		/* Create Entity implemnetation */
		String entityInterfaceName = entityTypeElement.getSimpleName().toString();
		String entityInterfacePackage = packageElement.getQualifiedName().toString();
	
		String entityImplClassName = entityInterfaceName + "Entity";
		String entityImplClassPackage = entityInterfacePackage + ".impl.persistence.entity";
		
		String entityIdentityManagerInterfaceName = entityInterfaceName + "IdentityManager";
		String entityIdentityManagerInterfacePackage = entityInterfacePackage + ".impl";
	
		String entityIdentityManagerClassName = entityInterfaceName + "EntityManager";
		String entityIdentityManagerClassPackage = entityInterfacePackage + ".impl.persistence.entity";
		
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityQueryImplClassName = entityQueryInterfaceName + "Impl";
		String entityQueryInterfacePackage = packageElement.getQualifiedName().toString();
		String entityQueryImplClassPackage = entityQueryInterfacePackage + ".impl";
		
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityIdentityManagerClassName)
			.addModifiers(Modifier.PUBLIC)
			.superclass(ClassName.get("org.activiti.engine.impl.persistence","AbstractManager"))
			.addSuperinterface(ClassName.get(entityIdentityManagerInterfacePackage,entityIdentityManagerInterfaceName));
	
		if(entityQueryTypeElement != null){
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("createNew" + entityQueryInterfaceName)
					.returns(ClassName.get(entityQueryTypeElement))
					.addModifiers(Modifier.PUBLIC)
					.addStatement(
						"return new $T($T.getProcessEngineConfiguration().getCommandExecutor())",
						ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
						ClassName.get("org.activiti.engine.impl.context", "Context")
					).build()
			);
			
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "CountByQueryCriteria")
					.returns(ClassName.LONG)
					.addModifiers(Modifier.PUBLIC)
					.addParameter(ClassName.get("",entityQueryInterfaceName + "Impl"),"query")
					.addStatement("return (Long) getDbSqlSession().selectOne($S, query)", "select" + entityInterfaceName + "CountByQueryCriteria")
					.build()
			);
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "ByQueryCriteria")
					.returns(ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get(entityTypeElement)))
					.addModifiers(Modifier.PUBLIC)
					.addParameter(ClassName.get("",entityQueryInterfaceName + "Impl"),"query")
					.addParameter(ClassName.get("org.activiti.engine.impl","Page"),"page")
					.addStatement("$T entries = getDbSqlSession().selectList($S, query, page)",ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get(entityInterfacePackage,entityInterfaceName)),"select" + entityInterfaceName + "ByQueryCriteria")
					.addStatement("return entries == null ? new $T() : entries",ParameterizedTypeName.get(ClassName.get(ArrayList.class), ClassName.get(entityInterfacePackage,entityInterfaceName)))
					.build()
			);
			
		}
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("find" + entityInterfaceName + "ById")
				.returns(ClassName.get(entityTypeElement))
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(String.class),"id")
				.addStatement(
					"return ($T)(($T)(new $T($T.getProcessEngineConfiguration().getCommandExecutor()).$L($L))).executeSingleResult($T.getCommandContext())",
					ClassName.get(entityImplClassPackage,entityImplClassName),
					ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
					ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
					ClassName.get("org.activiti.engine.impl.context", "Context"),
					QueryClassUtil.locateIdSetterMethod(entityQueryTypeElement),
					"id",
					ClassName.get("org.activiti.engine.impl.context", "Context")
				).build()
		);
		
		String codeSetter = QueryClassUtil.locateCodeSetterMethod(entityQueryTypeElement);
		
		if(codeSetter != null){
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("find" + entityInterfaceName + "ByCode")
					.returns(ClassName.get(entityTypeElement))
					.addModifiers(Modifier.PUBLIC)
					.addParameter(ClassName.get(String.class),"code")
					.addStatement(
						"return ($T)(($T)(new $T($T.getProcessEngineConfiguration().getCommandExecutor()).$L($L))).executeSingleResult($T.getCommandContext())",
						ClassName.get(entityImplClassPackage,entityImplClassName),
						ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
						ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
						ClassName.get("org.activiti.engine.impl.context", "Context"),
						codeSetter,
						"code",
						ClassName.get("org.activiti.engine.impl.context", "Context")
					).build()
			);
		}
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("delete" + entityInterfaceName)
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(String.class),"id")
				.addStatement(
					"$T entity = ($T)$N(id)",
					ClassName.get(entityImplClassPackage,entityImplClassName),
					ClassName.get(entityImplClassPackage,entityImplClassName),
					"find" + entityInterfaceName + "ById"
				).addStatement(
					(codeSetter != null ? "" : "/*") +
					"if(entity == null){entity = ($T)$N(id);}" +
					(codeSetter != null ? "" : "*/"),
					ClassName.get(entityImplClassPackage,entityImplClassName),
					"find" + entityInterfaceName + "ByCode"
				).addStatement(
					"if(entity != null)"
				).addCode(
					CodeBlock.builder()
					.addStatement("entity.delete()")
					.addStatement("if (getProcessEngineConfiguration().getEventDispatcher().isEnabled())")
					.add(
						CodeBlock.builder()
						.addStatement(
							"getProcessEngineConfiguration().getEventDispatcher().dispatchEvent($T.createEntityEvent($T.ENTITY_DELETED, entity))",
							ClassName.get("org.activiti.engine.delegate.event.impl","ActivitiEventBuilder"),
							ClassName.get("org.activiti.engine.delegate.event","ActivitiEventType")
						).build()
					).build()
				).build()
		);
		
		String entityVariableName = BeanUtil.initialLowerCase(entityTypeElement.getSimpleName().toString());
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("isNew" + entityInterfaceName)
				.returns(TypeName.BOOLEAN)
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.addStatement(
					"return (($T) $N).getRevision() == 0",
					ClassName.get(entityImplClassPackage,entityImplClassName),
					entityVariableName
				).build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("insert" + entityInterfaceName)
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.addStatement(
					"getDbSqlSession().insert(($T) $N)",
					ClassName.get("org.activiti.engine.impl.db","PersistentObject"),
					entityVariableName
				).addCode(
					CodeBlock.builder()
					.addStatement("if (getProcessEngineConfiguration().getEventDispatcher().isEnabled())")
					.add(
						CodeBlock.builder()
							.addStatement(
								"getProcessEngineConfiguration().getEventDispatcher().dispatchEvent($T.createEntityEvent($T.ENTITY_CREATED, $N))",
								ClassName.get("org.activiti.engine.delegate.event.impl","ActivitiEventBuilder"),
								ClassName.get("org.activiti.engine.delegate.event","ActivitiEventType"),
								entityVariableName
							).addStatement(
								"getProcessEngineConfiguration().getEventDispatcher().dispatchEvent($T.createEntityEvent($T.ENTITY_INITIALIZED, $N))",
								ClassName.get("org.activiti.engine.delegate.event.impl","ActivitiEventBuilder"),
								ClassName.get("org.activiti.engine.delegate.event","ActivitiEventType"),
								entityVariableName
							).build()
					).build()
				).build()
		);
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("update" + entityInterfaceName)
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(entityTypeElement),entityVariableName)
				.addStatement(
					"$T.getCommandContext().getDbSqlSession().update(($T) $N)",
					ClassName.get("org.activiti.engine.impl.context", "Context"),
					ClassName.get("org.activiti.engine.impl.db","PersistentObject"),
					entityVariableName
				).addCode(
					CodeBlock.builder()
					.addStatement("if (getProcessEngineConfiguration().getEventDispatcher().isEnabled())")
					.add(
						CodeBlock.builder()
							.addStatement(
								"getProcessEngineConfiguration().getEventDispatcher().dispatchEvent($T.createEntityEvent($T.ENTITY_UPDATED, $N))",
								ClassName.get("org.activiti.engine.delegate.event.impl","ActivitiEventBuilder"),
								ClassName.get("org.activiti.engine.delegate.event","ActivitiEventType"),
								entityVariableName
							).build()
					).build()
				).build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("createNew" + entityInterfaceName)
				.returns(ClassName.get(entityTypeElement))
				.addModifiers(Modifier.PUBLIC)
				.addStatement(
					"return new $T()",
					ClassName.get(entityImplClassPackage,entityImplClassName)
				).build()
		);
		
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityIdentityManagerClassPackage, typeBuilder);
	}

	public static void createEntityManagerFactoryClass(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		/* Create Entity implemnetation */
		String entityInterfaceName = entityTypeElement.getSimpleName().toString();
		String entityInterfacePackage = packageElement.getQualifiedName().toString();
	
		String entityImplClassName = entityInterfaceName + "Entity";
		String entityImplClassPackage = entityInterfacePackage + ".impl.persistence.entity";
	
		String entityIdentityManagerInterfaceName = entityInterfaceName + "IdentityManager";
		String entityIdentityManagerInterfacePackage = entityInterfacePackage + ".impl";
	
		String entityIdentityManagerClassName = entityInterfaceName + "EntityManager";
		String entityIdentityManagerClassPackage = entityInterfacePackage + ".impl.persistence.entity";		
		
		String entityEntityManagerFactoryClassName = entityInterfaceName + "EntityManagerFactory";
		String entityEntityManagerFactoryClassPackage = entityInterfacePackage + ".impl.persistence";
		
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityEntityManagerFactoryClassName)
			.addModifiers(Modifier.PUBLIC)
			.addSuperinterface(ClassName.get("org.activiti.engine.impl.interceptor","SessionFactory"));
	
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("getSessionType")
				.addModifiers(Modifier.PUBLIC)
				.returns(ParameterizedTypeName.get(ClassName.get(Class.class),WildcardTypeName.subtypeOf(Object.class)))
				.addStatement("return $T.class",ClassName.get(entityIdentityManagerInterfacePackage,entityIdentityManagerInterfaceName))
				.build()
		);		

		String configuredTableName = entityTypeElement.getAnnotation(MasterEntity.class).tableName();
		if(configuredTableName != null){
			configuredTableName = "RL_MD_" + BeanUtil.makeDbName(entityInterfaceName);
		}
		
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("openSession")
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get("org.activiti.engine.impl.interceptor","Session"))
				.addStatement(
					"$T.persistentObjectToTableNameMap.put($T.class, $S)",
					ClassName.get("org.activiti.engine.impl.persistence.entity","TableDataManager"),
					ClassName.get(entityImplClassPackage,entityImplClassName),
					configuredTableName
				).addStatement(
					"$T.apiTypeToTableNameMap.put($T.class, $S)",
					ClassName.get("org.activiti.engine.impl.persistence.entity","TableDataManager"),
					ClassName.get(entityInterfacePackage,entityInterfaceName),
					configuredTableName
				).addStatement("return new $T()",ClassName.get(entityIdentityManagerClassPackage,entityIdentityManagerClassName))
				.build()
		);		
	
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityEntityManagerFactoryClassPackage, typeBuilder);
	}

}
