package com.rlogistics.master.processor;

public class BeanUtil {

    public static String initialLowerCase(String name) {
        return Character.toLowerCase(name.charAt(0)) + (name.length() > 1 ? name.substring(1) : "");
    }

    public static String initialUpperCase(String name) {
        System.out.println(name);
        return Character.toUpperCase(name.charAt(0)) + (name.length() > 1 ? name.substring(1) : "");
    }

    public static String makeDbName(String attributeName) {
        String columnName = "";

        for (int i = 0; i < attributeName.length(); ++i) {
            char c = attributeName.charAt(i);

            if ((i > 0) && Character.isUpperCase(c)) {
                columnName += "_";
            }

            columnName += Character.toUpperCase(c);
        }

        return columnName;
    }

}
