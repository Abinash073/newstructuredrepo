package com.rlogistics.master;

public @interface MasterEntityLeadsToRestController {

	public static final String UNASSIGNED_PREFIX = "[unassigned]";

	String prefix() default UNASSIGNED_PREFIX;

}
