package com.rlogistics.master.processor;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

public class CustomQueriesServiceUtil {

	public static void createCustomQueriesServiceClasses(ProcessingEnvironment processingEnvironment,List<TypeElement> customQueriesMappers) {
		String packageName = null;
		TypeSpec.Builder cqImplemnetationTypeBuilder = TypeSpec.classBuilder("CustomQueriesService")
			.addModifiers(Modifier.PUBLIC)
			.superclass(ClassName.get("org.activiti.engine.impl","ServiceImpl"));
		
		for(TypeElement cqMapperTypeElement:customQueriesMappers){
			PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(cqMapperTypeElement);
			packageName = packageElement.getQualifiedName().toString();

			String wrapperClassName = cqMapperTypeElement.getSimpleName().toString();
			TypeSpec.Builder mapperSpecificCqWrapperServiceTypeBuilder = TypeSpec.classBuilder(wrapperClassName)
					.addModifiers(Modifier.PUBLIC,Modifier.STATIC);
			for(Element ee:cqMapperTypeElement.getEnclosedElements()){
				if(!(ee instanceof ExecutableElement)){
					continue;
				}
				
				ExecutableElement method = (ExecutableElement)ee;

				MethodSpec.Builder wrapperMethodBuilder = MethodSpec.methodBuilder(method.getSimpleName().toString())
						.addModifiers(Modifier.PUBLIC,Modifier.STATIC)
						.returns(ClassName.get(method.getReturnType()));
				
				boolean isVoidMethod = processingEnvironment.getTypeUtils().isSameType(method.getReturnType(),processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID));
				
				String argumentList = "";
				for(VariableElement paramElement:method.getParameters()){
					wrapperMethodBuilder.addParameter(ClassName.get(paramElement.asType()), paramElement.getSimpleName().toString());
					if(!argumentList.equals("")){
						argumentList += ",";
					}
					argumentList += paramElement.getSimpleName().toString();
				}
				
				wrapperMethodBuilder.addStatement(
					"$T pec = ($T)$T.getDefaultProcessEngine().getProcessEngineConfiguration()",
					ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
					ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
					ClassName.get("org.activiti.engine","ProcessEngines")
				).addStatement(
					"$T session = pec.getSqlSessionFactory().openSession()",
					ClassName.get("org.apache.ibatis.session","SqlSession")
				).beginControlFlow("try")
				.addStatement(
					"$T mapper = session.getMapper($T.class)",
					ClassName.get(cqMapperTypeElement.asType()),
					ClassName.get(cqMapperTypeElement.asType())
				).addStatement(
					(isVoidMethod ? "" : "return ") + "mapper.$L($L)",
					method.getSimpleName().toString(),
					argumentList
				).nextControlFlow("finally")
				.addStatement("session.close()")
				.endControlFlow();
				
				mapperSpecificCqWrapperServiceTypeBuilder.addMethod(wrapperMethodBuilder.build());
			}
			
			cqImplemnetationTypeBuilder.addType(mapperSpecificCqWrapperServiceTypeBuilder.build());
		}
		
		if(!customQueriesMappers.isEmpty()){			
			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, packageName + ".impl", cqImplemnetationTypeBuilder);
		}
	}
}
