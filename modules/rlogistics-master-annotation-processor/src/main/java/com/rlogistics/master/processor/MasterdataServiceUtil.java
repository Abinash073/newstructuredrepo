package com.rlogistics.master.processor;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

public class MasterdataServiceUtil {

	public static void createMasterServiceClasses(ProcessingEnvironment processingEnvironment,List<TypeElement> masterEntities) {
		String packageName = null;
		TypeSpec.Builder mdInterfaceTypeBuilder = TypeSpec.interfaceBuilder("MasterdataService")
			.addModifiers(Modifier.PUBLIC);
		TypeSpec.Builder mdImplemnetationTypeBuilder = TypeSpec.classBuilder("MasterdataServiceImpl")
			.addModifiers(Modifier.PUBLIC)
			.superclass(ClassName.get("org.activiti.engine.impl","ServiceImpl"));
		MethodSpec.Builder initSessionFactoriesMathodBuilder = MethodSpec.methodBuilder("initSessionFactories")
			.addModifiers(Modifier.PROTECTED)
			.addAnnotation(ClassName.get(Override.class))
			.addStatement("super.initSessionFactories()");
		for(TypeElement masterEntityTypeElement:masterEntities){
			PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(masterEntityTypeElement);
			packageName = packageElement.getQualifiedName().toString();
		
			TypeElement queryTypeElement = EntityClassUtil.locateQueryTypeElement(masterEntityTypeElement);
			{
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("create" + queryTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC,Modifier.ABSTRACT)
							.returns(ClassName.get(queryTypeElement.asType()));
					
					mdInterfaceTypeBuilder.addMethod(methodBuilder.build());
				}
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("create" + queryTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC)
							.returns(ClassName.get(queryTypeElement.asType()));
					String commandClassName = "Create" + queryTypeElement.getSimpleName() + "Cmd";
					String commandPackage = packageName + ".impl.cmd";
					methodBuilder.addStatement("return commandExecutor.execute(new $T())",
						ClassName.get(commandPackage,commandClassName)
					);
					
					mdImplemnetationTypeBuilder.addMethod(methodBuilder.build());
				}
			}
			{
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("delete" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC,Modifier.ABSTRACT)
							.addParameter(ClassName.get(String.class),"id");
					
					mdInterfaceTypeBuilder.addMethod(methodBuilder.build());
				}
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("delete" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC)
							.addParameter(ClassName.get(String.class),"id");
					String commandClassName = "Delete" + masterEntityTypeElement.getSimpleName() + "Cmd";
					String commandPackage = packageName + ".impl.cmd";
					methodBuilder.addStatement("commandExecutor.execute(new $T(id))",
						ClassName.get(commandPackage,commandClassName)
					);
					
					mdImplemnetationTypeBuilder.addMethod(methodBuilder.build());
				}
			}
			{
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("save" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC,Modifier.ABSTRACT)
							.addParameter(ClassName.get(masterEntityTypeElement.asType()),"entity");
					
					mdInterfaceTypeBuilder.addMethod(methodBuilder.build());
				}
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("save" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC)
							.addParameter(ClassName.get(masterEntityTypeElement.asType()),"entity");
					
					String commandClassName = "Save" + masterEntityTypeElement.getSimpleName() + "Cmd";
					String commandPackage = packageName + ".impl.cmd";
					methodBuilder.addStatement("commandExecutor.execute(new $T(entity))",
							ClassName.get(commandPackage,commandClassName)
					);
					mdImplemnetationTypeBuilder.addMethod(methodBuilder.build());
				}
			}
			{
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("new" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC,Modifier.ABSTRACT)
							.returns(ClassName.get(masterEntityTypeElement.asType()));
					
					mdInterfaceTypeBuilder.addMethod(methodBuilder.build());
				}
				{
					MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("new" + masterEntityTypeElement.getSimpleName().toString())
							.addModifiers(Modifier.PUBLIC)
							.returns(ClassName.get(masterEntityTypeElement.asType()));
					String commandClassName = "Create" + masterEntityTypeElement.getSimpleName() + "Cmd";
					String commandPackage = packageName + ".impl.cmd";
					methodBuilder.addStatement("return commandExecutor.execute(new $T())",
						ClassName.get(commandPackage,commandClassName)
					);
					
					mdImplemnetationTypeBuilder.addMethod(methodBuilder.build());
				}
			}
			{
				String entityInterfaceName = masterEntityTypeElement.getSimpleName().toString();
				String entityInterfacePackage = packageElement.getQualifiedName().toString();
			
				String entityEntityManagerFactoryClassName = entityInterfaceName + "EntityManagerFactory";
				String entityEntityManagerFactoryClassPackage = entityInterfacePackage + ".impl.persistence";
				
				initSessionFactoriesMathodBuilder.addStatement(
					"addSessionFactory(new $T())",
					ClassName.get(entityEntityManagerFactoryClassPackage,entityEntityManagerFactoryClassName)
				);
			}
		}
		
		if(!masterEntities.isEmpty()){
			mdImplemnetationTypeBuilder.addSuperinterface(ClassName.get(packageName,"MasterdataService"));
			
			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, packageName, mdInterfaceTypeBuilder);
			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, packageName + ".impl", mdImplemnetationTypeBuilder);

			TypeSpec.Builder peConfigutationTypeBuilder = TypeSpec.classBuilder("RLogisticsSpringProcessEngineConfiguration")
					.addModifiers(Modifier.PUBLIC)
					.superclass(ClassName.get("com.rlogistics.activiti","RLogisticsSpringProcessEngineConfigurationBase"));
			
			peConfigutationTypeBuilder.addMethod(initSessionFactoriesMathodBuilder.build());
			
			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, "com.rlogistics.activiti", peConfigutationTypeBuilder);
		}
	}
}
