package com.rlogistics.master.processor;

import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.rlogistics.master.MasterEntity;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;

@SuppressWarnings("restriction")
public class DBResourcesUtil {

	private ProcessingEnvironment processingEnvironment;
	private TypeElement entityTypeElement;
	private TypeElement entityQueryTypeElement;
	private String entityInterfaceName;
	private String entityImplClassName;
	private String entityInterfacePackage;
	private String entityImplClassPackage;

	private DBResourcesUtil(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement,TypeElement entityQueryTypeElement){
		this.processingEnvironment = processingEnvironment;
		this.entityTypeElement = entityTypeElement;
		this.entityQueryTypeElement = entityQueryTypeElement;
		
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		entityInterfaceName = entityTypeElement.getSimpleName().toString();
		entityImplClassName = entityInterfaceName + "Entity";
		entityInterfacePackage = packageElement.getQualifiedName().toString();
		entityImplClassPackage = entityInterfacePackage + ".impl.persistence.entity";
	}
	
	public static void createTableXML(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement,TypeElement entityQueryTypeElement) {
		new DBResourcesUtil(processingEnvironment, entityTypeElement, entityQueryTypeElement).createTableXMLImpl();
	}
	
	private void createTableXMLImpl() {		
		Table table = new Table(entityInterfaceName,entityImplClassPackage + "." + entityImplClassName);
		
		for(javax.lang.model.element.Element ee:entityTypeElement.getEnclosedElements()){
			MasterEntityAttribute mea = ee.getAnnotation(MasterEntityAttribute.class);
			
			if(mea == null) continue;
			
			String attributeName = EntityClassUtil.parseAttributeName(processingEnvironment, (ExecutableElement)ee);
			
			if(attributeName.equals("id")) continue;
			
			table.columns.add(table.new Column(attributeName, ClassName.get(((ExecutableElement)ee).getReturnType()),mea.columnName().equals(MasterEntityAttribute.UNASSIGNED_COLUMN_NAME) ? null : mea.columnName()));
		}
		
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			
			Element rootElement = document.createElement("mapper");
			document.appendChild(rootElement);
			rootElement.setAttribute("namespace", table.entity);
			
			createSingleEntityInsertCommand(table, rootElement);
			createBulkEntityInsertCommand(table, rootElement);
			createEntityUpdateCommand(table, rootElement);
			createEntityDeleteCommand(table, rootElement);
			createResultMapCommand(table, rootElement);
			createEntitySelectByIdCommand(table, rootElement);
			createEntitySelectByQueryCriteriaCommand(table, rootElement);
			createEntitySelectCountByQueryCriteriaCommand(table, rootElement);
			createEntitySelectByNativeQueryCommand(table, rootElement);
			createEntitySelectCountByNativeQueryCommand(table, rootElement);
			createEntitySelectByQueryCriteriaSqlCommand(table, rootElement);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", new Integer(2));
			Transformer transformer = transformerFactory.newTransformer();
			
			transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			DOMImplementation domImpl = document.getImplementation();
			DocumentType doctype = domImpl.createDocumentType("doctype",
			    "-//mybatis.org//DTD Mapper 3.0//EN",
			    "http://mybatis.org/dtd/mybatis-3-mapper.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			DOMSource source = new DOMSource(document);
			
			FileObject fileObject = processingEnvironment.getFiler().createResource(StandardLocation.CLASS_OUTPUT,"com.rlogistics.db.mapping.entity",entityInterfaceName + ".xml");
			OutputStream outputStream = fileObject.openOutputStream();
			StreamResult result = new StreamResult(outputStream);
			transformer.transform(source, result);
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			throw new RuntimeException("Errror exporting to XML file",ex);
		}
	}

	private void createSingleEntityInsertCommand(Table table, Element rootElement) {
		Element insertCommand = rootElement.getOwnerDocument().createElement("insert");
		rootElement.appendChild(insertCommand);
		insertCommand.setAttribute("id", "insert" + entityInterfaceName);
		insertCommand.setAttribute("parameterType", entityImplClassPackage + "." + entityImplClassName);
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.append("\ninsert into ${prefix}" + table.name + "(ID_,REV_");
		for(Table.Column column:table.columns){
			stringWriter.append("," + column.column);
		}
		stringWriter.append(")\n");
		stringWriter.append("values (#{id ,jdbcType=VARCHAR},1");
		for(Table.Column column:table.columns){
			if(column.jdbcType.equals(Table.Column.BYTEARRAYREF)){
				stringWriter.append(",#{" + column.property + ",typeHandler=ByteArrayRefTypeHandler}");
			} else {
				stringWriter.append(",#{" + column.property + ",jdbcType=" + column.jdbcType + "}");
			}
		}
		stringWriter.append(")");
		
		insertCommand.appendChild(makeTextNode(rootElement, stringWriter));
	}

	public Text makeTextNode(Element rootElement, StringWriter stringWriter) {
		stringWriter.flush();
		Text textNode = rootElement.getOwnerDocument().createTextNode(stringWriter.toString());
		return textNode;
	}

	private void createBulkEntityInsertCommand(Table table, Element rootElement) {
		Element insertCommand = rootElement.getOwnerDocument().createElement("insert");
		rootElement.appendChild(insertCommand);
		insertCommand.setAttribute("id", "bulkInsert" + entityInterfaceName);
		insertCommand.setAttribute("parameterType", "java.util.List");

		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("\ninsert into ${prefix}" + table.name + "(ID_,REV_");
			for(Table.Column column:table.columns){
				stringWriter.append("," + column.column);
			}
			stringWriter.append(")\n");
			stringWriter.append("values");
			insertCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
		
		Element forLoopCommand = rootElement.getOwnerDocument().createElement("foreach");
		insertCommand.appendChild(forLoopCommand);
		forLoopCommand.setAttribute("collection", "list");
		forLoopCommand.setAttribute("item", "xxx");
		forLoopCommand.setAttribute("index", "index");
		forLoopCommand.setAttribute("separator", ",");
		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("(#{xxx.id ,jdbcType=VARCHAR},1");
			for(Table.Column column:table.columns){
				if(column.jdbcType.equals(Table.Column.BYTEARRAYREF)){
					stringWriter.append(",#{xxx." + column.property + ",typeHandler=ByteArrayRefTypeHandler}");
				} else {
					stringWriter.append(",#{xxx." + column.property + ",jdbcType=" + column.jdbcType + "}");
				}
			}
			stringWriter.append(")");
			forLoopCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
	}

	private void createEntityUpdateCommand(Table table, Element rootElement) {
		Element updateCommand = rootElement.getOwnerDocument().createElement("update");
		rootElement.appendChild(updateCommand);
		updateCommand.setAttribute("id", "update" + entityInterfaceName);
		updateCommand.setAttribute("parameterType", entityImplClassPackage + "." + entityImplClassName);
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.append("\nupdate ${prefix}" + table.name + " set REV_ = #{revisionNext ,jdbcType=INTEGER}\n");
		for(Table.Column column:table.columns){
			if(column.jdbcType.equals(Table.Column.BYTEARRAYREF)){
				stringWriter.append(", " + column.column + " = #{" + column.property + " ,typeHandler=ByteArrayRefTypeHandler}");
			} else {
				stringWriter.append(", " + column.column + " = #{" + column.property + " ,jdbcType=" + column.jdbcType + "}");
			}
		}
		stringWriter.append(" where ID_ = #{id} and REV_ = #{revision}");
		updateCommand.appendChild(makeTextNode(rootElement, stringWriter));
	}

	private void createEntityDeleteCommand(Table table, Element rootElement) {
		Element deleteCommand = rootElement.getOwnerDocument().createElement("delete");
		rootElement.appendChild(deleteCommand);
		deleteCommand.setAttribute("id", "delete" + entityInterfaceName);
		deleteCommand.setAttribute("parameterType", entityImplClassPackage + "." + entityImplClassName);
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.append("\ndelete from ${prefix}" + table.name);
		stringWriter.append(" where ID_ = #{id} and REV_ = #{revision}");
		deleteCommand.appendChild(makeTextNode(rootElement, stringWriter));
	}

	private void createResultMapCommand(Table table, Element rootElement) {
		Element resultMapCommand = rootElement.getOwnerDocument().createElement("resultMap");
		rootElement.appendChild(resultMapCommand);
		resultMapCommand.setAttribute("id", BeanUtil.initialLowerCase(entityInterfaceName) + "ResultMap");
		resultMapCommand.setAttribute("type", entityImplClassPackage + "." + entityImplClassName);

		{
			Element idCommand = rootElement.getOwnerDocument().createElement("result");
			resultMapCommand.appendChild(idCommand);
			idCommand.setAttribute("property", "id");
			idCommand.setAttribute("column", "ID_");
			idCommand.setAttribute("jdbcType", "VARCHAR");
		}
		
		{
			Element revisionCommand = rootElement.getOwnerDocument().createElement("result");
			resultMapCommand.appendChild(revisionCommand);
			revisionCommand.setAttribute("property", "revision");
			revisionCommand.setAttribute("column", "REV_");
			revisionCommand.setAttribute("jdbcType", "INTEGER");
		}

		for(Table.Column column:table.columns){
			Element resultCommand = rootElement.getOwnerDocument().createElement("result");
			resultMapCommand.appendChild(resultCommand);
			resultCommand.setAttribute("property", column.property);
			resultCommand.setAttribute("column", column.column);
			if(column.jdbcType.equals(Table.Column.BYTEARRAYREF)){
				resultCommand.setAttribute("typeHandler","ByteArrayRefTypeHandler");
			} else {
				resultCommand.setAttribute("jdbcType", column.jdbcType);
			}
		}
	}

	private void createEntitySelectByIdCommand(Table table, Element rootElement) {
		Element selectCommand = rootElement.getOwnerDocument().createElement("select");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "ById");
		selectCommand.setAttribute("parameterType", "string");
		selectCommand.setAttribute("resultMap", BeanUtil.initialLowerCase(entityInterfaceName) + "ResultMap");
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.append("select * from ${prefix}" + table.name + " where ID_ = #{id,jdbcType=VARCHAR}");
		selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
	}

	private void createEntitySelectByQueryCriteriaCommand(Table table, Element rootElement) {
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityQueryImplClassName = entityQueryInterfaceName + "Impl";
		String entityQueryInterfacePackage = packageElement.getQualifiedName().toString();
		String entityQueryImplClassPackage = entityQueryInterfacePackage + ".impl";
		
		Element selectCommand = rootElement.getOwnerDocument().createElement("select");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "ByQueryCriteria");
		selectCommand.setAttribute("parameterType", entityQueryImplClassPackage + "." + entityQueryImplClassName);
		selectCommand.setAttribute("resultMap", BeanUtil.initialLowerCase(entityInterfaceName) + "ResultMap");

		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${limitBefore} select RES.* ${limitBetween}");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}

		Element includeCommand = rootElement.getOwnerDocument().createElement("include");
		selectCommand.appendChild(includeCommand);
		includeCommand.setAttribute("refid", "select" + entityInterfaceName + "ByQueryCriteriaSql");
		
		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${orderBy} ${limitAfter}");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
	}

	private void createEntitySelectCountByQueryCriteriaCommand(Table table, Element rootElement) {
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityQueryImplClassName = entityQueryInterfaceName + "Impl";
		String entityQueryInterfacePackage = packageElement.getQualifiedName().toString();
		String entityQueryImplClassPackage = entityQueryInterfacePackage + ".impl";
		
		Element selectCommand = rootElement.getOwnerDocument().createElement("select");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "CountByQueryCriteria");
		selectCommand.setAttribute("parameterType", entityQueryImplClassPackage + "." + entityQueryImplClassName);
		selectCommand.setAttribute("resultType", "long");

		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("select count(RES.ID_)");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}

		Element includeCommand = rootElement.getOwnerDocument().createElement("include");
		selectCommand.appendChild(includeCommand);
		includeCommand.setAttribute("refid", "select" + entityInterfaceName + "ByQueryCriteriaSql");
	}

	private void createEntitySelectByNativeQueryCommand(Table table, Element rootElement) {
		Element selectCommand = rootElement.getOwnerDocument().createElement("select");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "ByNativeQuery");
		selectCommand.setAttribute("parameterType", "java.util.Map");
		selectCommand.setAttribute("resultMap", BeanUtil.initialLowerCase(entityInterfaceName) + "ResultMap");

		{
			Element ifCommand = rootElement.getOwnerDocument().createElement("if");
			selectCommand.appendChild(ifCommand);
			ifCommand.setAttribute("test", "resultType == 'LIST_PAGE'");
			
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${limitBefore}");
			ifCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
		
		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${sql}");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}

		{
			Element ifCommand = rootElement.getOwnerDocument().createElement("if");
			selectCommand.appendChild(ifCommand);
			ifCommand.setAttribute("test", "resultType == 'LIST_PAGE'");
			
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${limitAfter}");
			ifCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
	}

	private void createEntitySelectCountByNativeQueryCommand(Table table, Element rootElement) {		
		Element selectCommand = rootElement.getOwnerDocument().createElement("select");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "CountByNativeQuery");
		selectCommand.setAttribute("parameterType", "java.util.Map");
		selectCommand.setAttribute("resultType", "long");

		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("${sql}");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
	}	
	
	private void createEntitySelectByQueryCriteriaSqlCommand(Table table, Element rootElement) {
		Element selectCommand = rootElement.getOwnerDocument().createElement("sql");
		rootElement.appendChild(selectCommand);
		selectCommand.setAttribute("id", "select" + entityInterfaceName + "ByQueryCriteriaSql");

		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("from ${prefix}" + table.name + " RES");
			selectCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}

		Element whereCommand = rootElement.getOwnerDocument().createElement("where");
		selectCommand.appendChild(whereCommand);

		Set<String> attributesInQuery = new HashSet<String>();
		for(javax.lang.model.element.Element ee:entityQueryTypeElement.getEnclosedElements()){
			MasterEntityQueryParameter mqp = ee.getAnnotation(MasterEntityQueryParameter.class);
			
			if(mqp != null){
				attributesInQuery.add(mqp.attribute());
			}
		}
		
		{
			Element ifCommand = rootElement.getOwnerDocument().createElement("if");
			whereCommand.appendChild(ifCommand);
			ifCommand.setAttribute("test", "id != null");
			
			StringWriter stringWriter = new StringWriter();
			stringWriter.append("and RES.ID_ = #{id}");
			ifCommand.appendChild(makeTextNode(rootElement, stringWriter));
		}
		
		for(Table.Column column:table.columns){
			if(!attributesInQuery.contains(column.property)) continue;

			{
				Element ifCommand = rootElement.getOwnerDocument().createElement("if");
				whereCommand.appendChild(ifCommand);
				if(column.jdbcType.equals("INTEGER")||column.jdbcType.equals("FLOAT")||column.jdbcType.equals("SMALLINT")){
					ifCommand.setAttribute("test", column.property + " != -9999");
				} else {
					ifCommand.setAttribute("test", column.property + " != null");
				}
				
				StringWriter stringWriter = new StringWriter();
				stringWriter.append("and RES." + column.column + " = #{" + column.property + "}");
				ifCommand.appendChild(makeTextNode(rootElement, stringWriter));
			}

			if(column.jdbcType.equals("VARCHAR"))
			{
				Element ifCommand = rootElement.getOwnerDocument().createElement("if");
				whereCommand.appendChild(ifCommand);
				ifCommand.setAttribute("test", column.property + "Like != null");
				
				StringWriter stringWriter = new StringWriter();
				stringWriter.append("and RES." + column.column + " LIKE #{" + column.property + "Like}");
				ifCommand.appendChild(makeTextNode(rootElement, stringWriter));
				
			}
			
			if(column.jdbcType.equals("VARCHAR"))
			{
				Element ifCommand = rootElement.getOwnerDocument().createElement("if");
				whereCommand.appendChild(ifCommand);
				ifCommand.setAttribute("test", column.property + "NotEquals != null");
	
				StringWriter stringWriter = new StringWriter();
				stringWriter.append("and RES." + column.column + " != #{" + column.property + "NotEquals}");
				ifCommand.appendChild(makeTextNode(rootElement, stringWriter));

			}
		}
		
	}

	public static String getConfiguiredColumnName(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement,String attributeName){
		String columnName = null;
		
		for(javax.lang.model.element.Element ee:entityTypeElement.getEnclosedElements()){
			MasterEntityAttribute mea = ee.getAnnotation(MasterEntityAttribute.class);
			
			if(mea == null) continue;
			
			String currentAttributeName = EntityClassUtil.parseAttributeName(processingEnvironment, (ExecutableElement)ee);
			
			if(currentAttributeName.equals(attributeName)){
				if(!mea.columnName().equals(MasterEntityAttribute.UNASSIGNED_COLUMN_NAME)){
					columnName = mea.columnName();
				}
				break;
			}
		}
		
		return columnName;
	}

	private class Table{
		public Table(String name,String entity){
			String configurredTableName = entityTypeElement.getAnnotation(MasterEntity.class).tableName();
			this.name = configurredTableName.equals(MasterEntity.UNASSIGNED_TABLE_NAME) ? "RL_MD_" + BeanUtil.makeDbName(name) : configurredTableName;
			this.entity = entity;
		}
		public String name;
		public String entity;
		public ArrayList<Column> columns = new ArrayList<Column>();
		
		public class Column{
			
			private static final String BYTEARRAYREF = "BYTEARRAYREF";
			private static final String SMALLINT = "SMALLINT";
			private static final String FLOAT = "FLOAT";
			private static final String INTEGER = "INTEGER";
			private static final String VARCHAR = "VARCHAR";
			public Column(String name,TypeName type, String columnName){
				property = name;
				column = columnName != null ? columnName : (BeanUtil.makeDbName(name) + "_");
				if(type.equals(TypeName.OBJECT)){
					jdbcType = VARCHAR;
				} else if(type.equals(TypeName.LONG)||type.equals(TypeName.INT)){
					jdbcType = INTEGER;
				} else if(type.equals(TypeName.FLOAT) || type.equals(TypeName.DOUBLE)){
					jdbcType = FLOAT;
				} else if(type.equals(TypeName.BOOLEAN)){
					jdbcType = SMALLINT;
				} else if(type.equals(ArrayTypeName.of(TypeName.BYTE))){
					property += "ByteArrayRef";
					jdbcType = BYTEARRAYREF;
				} else {
					jdbcType = VARCHAR;
				}
			}
			public String property;
			public String column;
			public String jdbcType;
		}
	}
}
