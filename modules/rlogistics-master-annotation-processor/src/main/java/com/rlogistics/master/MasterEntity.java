package com.rlogistics.master;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MasterEntity {
	public static final String UNASSIGNED_TABLE_NAME = "[unassigned]";
	String tableName() default UNASSIGNED_TABLE_NAME;
}
