package com.rlogistics.master.processor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;

import com.rlogistics.master.MasterEntityQueryParameter;
import com.rlogistics.master.MasterEntityQuerySort;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

public class QueryClassUtil {

	public static void addColumnName(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement, TypeSpec.Builder queryPropertyTypeBuilder,String entityQueryInterfaceName,String attributeName){
		
		String columnName = BeanUtil.makeDbName(attributeName) + "_";
		String configuredColumnName = DBResourcesUtil.getConfiguiredColumnName(processingEnvironment, entityTypeElement, attributeName);
		
		queryPropertyTypeBuilder.addField(
				FieldSpec.builder(
					ClassName.get("",entityQueryInterfaceName + "Property"),
					columnName
				).addModifiers(
					Modifier.PUBLIC,Modifier.STATIC,Modifier.FINAL
				).initializer(
					"new $T($S + $S)",
					ClassName.get("",entityQueryInterfaceName + "Property"),
					"RES.",
					configuredColumnName != null ? configuredColumnName : columnName
				).build()
			);
	}

	public static TypeSpec.Builder configureQueryPropertyTypeBuilder(String entityQueryInterfaceName){
		TypeSpec.Builder queryPropertyTypeBuilder = TypeSpec.classBuilder(entityQueryInterfaceName + "Property")
				.addModifiers(Modifier.PUBLIC)
				.addSuperinterface(ClassName.get("org.activiti.engine.query","QueryProperty"));
		
		queryPropertyTypeBuilder.addField(
			FieldSpec.builder(
				ParameterizedTypeName.get(
					ClassName.get(Map.class),
					ClassName.get(String.class),
					ClassName.get("",entityQueryInterfaceName + "Property")
				),
				"properties"
			).addModifiers(
				Modifier.PRIVATE,Modifier.STATIC,Modifier.FINAL
			).initializer(
				"new $T()",
				ParameterizedTypeName.get(
						ClassName.get(HashMap.class),
						ClassName.get(String.class),
						ClassName.get("",entityQueryInterfaceName + "Property")
					)				
			).build()
		);
	
		queryPropertyTypeBuilder.addField(
			FieldSpec.builder(
				ClassName.get(String.class),
				"name"
			).addModifiers(Modifier.PRIVATE).build()
		);
		
		queryPropertyTypeBuilder.addMethod(
			MethodSpec.constructorBuilder()
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get(String.class), "name")
				.addStatement("this.name = name")
				.addStatement("properties.put(name,this)")
				.build()
		);
	
		queryPropertyTypeBuilder.addMethod(
			MethodSpec.methodBuilder("getName")
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(String.class))
				.addStatement("return name")
				.build()
		);
		
		queryPropertyTypeBuilder.addMethod(
			MethodSpec.methodBuilder("findByName")
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get("",entityQueryInterfaceName + "Property"))
				.addParameter(ClassName.get(String.class), "name")
				.addStatement("return properties.get(name)")
				.build()
		);
		
		return queryPropertyTypeBuilder;
	}

	public static void createEntityQueryImplClass(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement, TypeElement entityQueryTypeElement) {
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		/* Create Entity implemnetation */
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityQueryImplClassName = entityQueryInterfaceName + "Impl";
		String entityQueryInterfacePackage = packageElement.getQualifiedName().toString();
		String entityQueryImplClassPackage = entityQueryInterfacePackage + ".impl";
	
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityQueryImplClassName)
			.addModifiers(Modifier.PUBLIC)
			.superclass(ParameterizedTypeName.get(ClassName.get("org.activiti.engine.impl","AbstractQuery"),ClassName.get(entityQueryTypeElement),ClassName.get(entityTypeElement)))
			.addSuperinterface(ClassName.get(entityQueryTypeElement.asType()));
	
		/* Add CTOR s*/
		typeBuilder.addMethod(
			MethodSpec.constructorBuilder()
				.addModifiers(Modifier.PUBLIC)
				.build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.constructorBuilder()
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor","CommandContext"), "commandContext")
				.addStatement("super(commandContext)")
				.build()
		);
	
		typeBuilder.addMethod(
			MethodSpec.constructorBuilder()
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"), "commandExecutor")
				.addStatement("super(commandExecutor)")
				.build()
		);
		
		/* Add exec list and count methods */
		ClassName identityManagerInterfaceClassName = ClassName.get(entityQueryImplClassPackage,entityTypeElement.getSimpleName().toString() + "IdentityManager");
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("executeCount")
				.addAnnotation(ClassName.get(Override.class))
				.returns(TypeName.LONG)
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor","CommandContext"), "commandContext")
				.addStatement("checkQueryOk()")
				.addStatement(
						"return (($T)commandContext).getIdentityManager($T.class).find$LCountByQueryCriteria(this)",
						ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
						identityManagerInterfaceClassName,
						entityTypeElement.getSimpleName().toString()
				).build()
		);
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("executeList")
				.addAnnotation(ClassName.get(Override.class))
				.returns(ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get(entityTypeElement)))
				.addModifiers(Modifier.PUBLIC)
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor","CommandContext"), "commandContext")
				.addParameter(ClassName.get("org.activiti.engine.impl","Page"),"page")
				.addStatement("checkQueryOk()")
				.addStatement(
						"return (($T)commandContext).getIdentityManager($T.class).find$LByQueryCriteria(this,page)",
						ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
						identityManagerInterfaceClassName,
						entityTypeElement.getSimpleName().toString()
				).build()
			);
		
		/* Handle query parameters and sorts
			Also create the QueryProperty class */
		TypeSpec.Builder queryPropertyTypeBuilder = configureQueryPropertyTypeBuilder(entityQueryInterfaceName);
		Set<String> knownParameterAttributes = new HashSet<String>();
		Set<String> knownSortAttributes = new HashSet<String>();
		for(Element ee:entityQueryTypeElement.getEnclosedElements()){
			MasterEntityQueryParameter mqp = ee.getAnnotation(MasterEntityQueryParameter.class);
			
			if(mqp != null){
				boolean knownParameterAttribute = knownParameterAttributes.contains(mqp.attribute());
	
				processQueryParameter(processingEnvironment,(ExecutableElement)ee,mqp,typeBuilder,knownParameterAttribute);
				
				knownParameterAttributes.add(mqp.attribute());				
			}
	
			MasterEntityQuerySort mqs = ee.getAnnotation(MasterEntityQuerySort.class);
			
			if(mqs != null){
				boolean knownSortAttribute = knownSortAttributes.contains(mqs.attribute());
				
				processQuerySort((ExecutableElement)ee,mqs,typeBuilder,knownSortAttribute);
				
				if(!knownSortAttribute){
					addColumnName(processingEnvironment,entityTypeElement, queryPropertyTypeBuilder, entityQueryInterfaceName, mqs.attribute());
				}
				
				knownSortAttributes.add(mqs.attribute());
			}
		}
		
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityQueryImplClassPackage, queryPropertyTypeBuilder);
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityQueryImplClassPackage, typeBuilder);
	}

	public static void processQueryParameter(ProcessingEnvironment processingEnvironment,ExecutableElement ee, MasterEntityQueryParameter mqp, Builder typeBuilder, boolean knownParameterAttribute) {
		String attributeName = mqp.attribute();
		String attributeNameInitCaps = BeanUtil.initialUpperCase(attributeName);
		
		VariableElement firstParameterElement = ee.getParameters().get(0);
		TypeName attributeType = ClassName.get(firstParameterElement.asType());		
		
		if(attributeName == null) return;

		boolean isANumber = processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Integer").asType())
				||processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT))
				||processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Float").asType())
				||processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT))
				||processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Double").asType())
				||processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.DOUBLE));
		boolean isAString = processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.String").asType());
		String attributeLikeName = attributeName + "Like";
		String attributeLikeNameInitCaps = BeanUtil.initialUpperCase(attributeLikeName);
		
		String attributeNotEqualsName = attributeName + "NotEquals";
		String attributeNotEqualsNameInitCaps = BeanUtil.initialUpperCase(attributeNotEqualsName);
		
		if(!knownParameterAttribute){
			/* Create field */
			typeBuilder.addField(
				isANumber
				?FieldSpec.builder(attributeType, attributeName)
					.initializer("($T)-9999",attributeType)
					.build()
				:FieldSpec.builder(attributeType, attributeName)
					.build()
			);
	
			/* Create getter */		
			typeBuilder.addMethod(
				MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps)
					.addModifiers(Modifier.PUBLIC)
					.returns(attributeType)
					.addStatement("return this.$L",attributeName)
					.build()
			);
			
			if(isAString){
				/* Create field */
				typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeLikeName)
					.build()
				);
		
				/* Create getter */		
				typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeLikeNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeLikeName)
						.build()
				);
				
				/* Create field */
				typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeNotEqualsName)
					.build()
				);
		
				/* Create getter */		
				typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNotEqualsNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeNotEqualsName)
						.build()
				);
			}
		}
		
		/* create special setter that is the interface method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder(ee.getSimpleName().toString())
				.addAnnotation(ClassName.get(Override.class))
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(ee.getReturnType()))
				.addParameter(ClassName.get(firstParameterElement.asType()),firstParameterElement.getSimpleName().toString())/* ASSUME exactly one parameter for query criteria method*/
				.addStatement("this.$L = $L", mqp.attribute(), firstParameterElement.getSimpleName().toString())
				.addStatement("return this")
				.build()
		);
		
		if(isAString){
			typeBuilder.addMethod(
					MethodSpec.methodBuilder(ee.getSimpleName().toString() + "Like")
						.addModifiers(Modifier.PUBLIC)
						.returns(ClassName.get(ee.getReturnType()))
						.addParameter(ClassName.get(firstParameterElement.asType()),firstParameterElement.getSimpleName().toString() + "Like")/* ASSUME exactly one parameter for query criteria method*/
						.addStatement("this.$L = $L", mqp.attribute() + "Like", firstParameterElement.getSimpleName().toString() + "Like")
						.addStatement("return this")
						.build()
				);
			
			typeBuilder.addMethod(
					MethodSpec.methodBuilder(ee.getSimpleName().toString() + "NotEquals")
						.addModifiers(Modifier.PUBLIC)
						.returns(ClassName.get(ee.getReturnType()))
						.addParameter(ClassName.get(firstParameterElement.asType()),firstParameterElement.getSimpleName().toString() + "NotEquals")/* ASSUME exactly one parameter for query criteria method*/
						.addStatement("this.$L = $L", mqp.attribute() + "NotEquals", firstParameterElement.getSimpleName().toString() + "NotEquals")
						.addStatement("return this")
						.build()
				);
		}
	}

	public static void processQuerySort(ExecutableElement ee, MasterEntityQuerySort mqs, Builder typeBuilder, boolean knownSortAttribute) {
		/* Create ordering method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder(ee.getSimpleName().toString())
				.addAnnotation(ClassName.get(Override.class))
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(ee.getReturnType()))
				.addStatement("return orderBy($L.$L_)",ClassName.get("",((TypeElement)ee.getEnclosingElement()).getSimpleName() + "Property"),BeanUtil.makeDbName(mqs.attribute()))
				.build()
		);
	}

	public static String locateIdSetterMethod(TypeElement entityQueryTypeElement) {
		for(Element ee:entityQueryTypeElement.getEnclosedElements()){
			MasterEntityQueryParameter mqp = ee.getAnnotation(MasterEntityQueryParameter.class);
			
			if((mqp != null) && mqp.idAttribute()){
				return ee.getSimpleName().toString();
			}
		}

		return "id";
	}

	public static String locateCodeSetterMethod(TypeElement entityQueryTypeElement) {
	
		for(Element ee:entityQueryTypeElement.getEnclosedElements()){
			MasterEntityQueryParameter mqp = ee.getAnnotation(MasterEntityQueryParameter.class);
			
			if((mqp != null) && mqp.codeAttribute()){
				return ee.getSimpleName().toString();
			}
		}

		return null;
	}	
}
