package com.rlogistics.master.processor;

import java.io.IOException;

import javax.annotation.processing.ProcessingEnvironment;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

public class JavaFileUtil {

	public static void writeTypeSpecToFile(ProcessingEnvironment processingEnvironment, String entityIdentityManagerInterfacePackage, TypeSpec.Builder typeBuilder) {
		JavaFile javaFile = JavaFile.builder(entityIdentityManagerInterfacePackage, typeBuilder.build()).build();
		
		try {
			javaFile.writeTo(processingEnvironment.getFiler());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
