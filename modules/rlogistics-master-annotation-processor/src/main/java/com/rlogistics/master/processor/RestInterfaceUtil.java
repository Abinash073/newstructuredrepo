package com.rlogistics.master.processor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityLeadsToRestController;
import com.rlogistics.master.MasterEntityQueryParameter;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

public class RestInterfaceUtil {
	private ProcessingEnvironment processingEnvironment;
	private TypeElement entityTypeElement;
	private TypeElement entityQueryTypeElement;
	private String entityInterfaceName;
	private String entityInterfacePackage;
	private String entityRESTPackage;

	public RestInterfaceUtil(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement, TypeElement entityQueryTypeElement){
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		this.processingEnvironment = processingEnvironment;
		this.entityTypeElement = entityTypeElement;
		this.entityQueryTypeElement = entityQueryTypeElement;

		this.entityInterfaceName = entityTypeElement.getSimpleName().toString();
		this.entityInterfacePackage = packageElement.getQualifiedName().toString();
		this.entityRESTPackage = entityInterfacePackage + ".rest";
	}
	
	public static void createRestInterfaceClasses(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement, TypeElement entityQueryTypeElement){
		RestInterfaceUtil restInterfaceUtil = new RestInterfaceUtil(processingEnvironment,entityTypeElement,entityQueryTypeElement);
		
		restInterfaceUtil.createRestDataClass();
		restInterfaceUtil.createRestQueryDataClass();
		restInterfaceUtil.createRestInterfaceClass();
	}
	
	public void createRestDataClass() {	
		String entityRestDataClassName = entityInterfaceName + "RestData";
		
		String entityVariableName = BeanUtil.initialLowerCase(entityInterfaceName);
		
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityRestDataClassName)
			.addModifiers(Modifier.PUBLIC);

		MethodSpec.Builder copyFromMethodSpec = MethodSpec.methodBuilder("copyFrom" + entityInterfaceName)
			.addParameter(ClassName.get(entityInterfacePackage,entityInterfaceName),entityVariableName);
		MethodSpec.Builder copyToMethodSpec = MethodSpec.methodBuilder("copyTo" + entityInterfaceName)
			.addParameter(ClassName.get(entityInterfacePackage,entityInterfaceName),entityVariableName);				
		
		for(Element ee:entityTypeElement.getEnclosedElements()){
			MasterEntityAttribute mea = ee.getAnnotation(MasterEntityAttribute.class);
			
			if(mea == null) continue;
			
			TypeName attributeType = ClassName.get(((ExecutableElement)ee).getReturnType());
			
			String attributeName = EntityClassUtil.parseAttributeName(processingEnvironment, ((ExecutableElement)ee));
			
			String attributeNameInitCaps = BeanUtil.initialUpperCase(attributeName);
			
			if(attributeName == null) return;
			
			typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeName)
					.build()
				);
		
			typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeName)
						.build()
				);
		
			typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.addParameter(attributeType, attributeName)
						.addStatement("this.$L = $L",attributeName,attributeName)
						.build()
				);
			
			copyFromMethodSpec.addStatement(
				"this.$L = $L.$L()",
				attributeName,
				entityVariableName,
				(attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps
			);
			copyToMethodSpec.addStatement(
				"$L.$L(this.$L)",
				entityVariableName,
				"set" + attributeNameInitCaps,
				attributeName
			);
		}
		
		typeBuilder.addMethod(copyFromMethodSpec.build());
		typeBuilder.addMethod(copyToMethodSpec.build());
		
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityRESTPackage, typeBuilder);		
	}

	public void createRestQueryDataClass() {	
		String entityRestDataClassName = entityInterfaceName + "RestQueryData";
		
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityRestDataClassName)
			.addModifiers(Modifier.PUBLIC);

		for(Element ee:entityTypeElement.getEnclosedElements()){
			MasterEntityAttribute mea = ee.getAnnotation(MasterEntityAttribute.class);
			
			if(mea == null) continue;
			
			TypeName attributeType = ClassName.get(((ExecutableElement)ee).getReturnType());
			
			String attributeName = EntityClassUtil.parseAttributeName(processingEnvironment, ((ExecutableElement)ee));
			
			String attributeNameInitCaps = BeanUtil.initialUpperCase(attributeName);
			
			if(attributeName == null) return;

			boolean isANumber = processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Integer").asType())
					||processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT))
					||processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Float").asType())
					||processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT))
					||processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.Double").asType())
					||processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.DOUBLE));

			typeBuilder.addField(
				isANumber
				?FieldSpec.builder(attributeType, attributeName)
					.initializer("($T)-9999",attributeType)
					.build()
				:FieldSpec.builder(attributeType, attributeName)
					.build()
			);
		
			typeBuilder.addMethod(
				MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps)
					.addModifiers(Modifier.PUBLIC)
					.returns(attributeType)
					.addStatement("return this.$L",attributeName)
					.build()
			);
		
			typeBuilder.addMethod(
				MethodSpec.methodBuilder("set" + attributeNameInitCaps)
					.addModifiers(Modifier.PUBLIC)
					.addParameter(attributeType, attributeName)
					.addStatement("this.$L = $L",attributeName,attributeName)
					.build()
			);
			
			if(processingEnvironment.getTypeUtils().isSameType(((ExecutableElement)ee).getReturnType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.String").asType())){
				String attributeLikeName = attributeName + "Like";
				String attributeLikeNameInitCaps = BeanUtil.initialUpperCase(attributeLikeName);
				
				typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeLikeName)
					.build()
				);
			
				typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeLikeNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeLikeName)
						.build()
				);
			
				typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeLikeNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.addParameter(attributeType, attributeLikeName)
						.addStatement("this.$L = $L",attributeLikeName,attributeLikeName)
						.build()
				);
				
				String attributeNotEqualsName = attributeName + "NotEquals";
				String attributeNotEqualsNameInitCaps = BeanUtil.initialUpperCase(attributeNotEqualsName);
				
				typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeNotEqualsName)
					.build()
				);
			
				typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNotEqualsNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeNotEqualsName)
						.build()
				);
			
				typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeNotEqualsNameInitCaps)
						.addModifiers(Modifier.PUBLIC)
						.addParameter(attributeType, attributeNotEqualsName)
						.addStatement("this.$L = $L",attributeNotEqualsName,attributeNotEqualsName)
						.build()
				);
			}
			
		}
				
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityRESTPackage, typeBuilder);		
	}
	
	public void createRestInterfaceClass() {
		String entityRestDataClassName = entityInterfaceName + "RestData";
		String entityRestQueryDataClassName = entityInterfaceName + "RestQueryData";
		String entityRESTClassName = entityInterfaceName + "RestResource";
		
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityRESTClassName)
			.addModifiers(Modifier.PUBLIC)
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RestController")).build())
			.superclass(ClassName.get("com.rlogistics.http","RLogisticsResource"));
	
		typeBuilder.addField(
			FieldSpec.builder(ClassName.get("org.slf4j","Logger"),"log", Modifier.PRIVATE)
			.initializer("$T.getLogger($T.class)",
				ClassName.get("org.slf4j","LoggerFactory"),
				ClassName.get("",entityRESTClassName)
			).build()
		);
		
		String entityVariableName = BeanUtil.initialLowerCase(entityInterfaceName);
		
		String commandPackage = entityInterfacePackage + ".impl.cmd";
		
		String prefix = entityTypeElement.getAnnotation(MasterEntityLeadsToRestController.class).prefix();
		if(prefix.equals(MasterEntityLeadsToRestController.UNASSIGNED_PREFIX)){
			prefix = entityInterfaceName.toLowerCase();
		}		
		if(!prefix.startsWith("/")){
			prefix = "/" + prefix;
		}
		if(!prefix.endsWith("/")){
			prefix += "/";
		}
		
		/* Create method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("create")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "create")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityVariableName + "Json")
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
						.addMember("value", "$S", entityVariableName)
						.build()
				).build()
			).addStatement("RestResponse restResponse = new RestResponse()")
			.beginControlFlow("try"
			).addStatement("$T $L = new $T().readValue($T.decode($LJson),$T.class)",
				ClassName.get("",entityRestDataClassName),
				entityVariableName,
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
				ClassName.get("java.net", "URLDecoder"),
				entityVariableName,
				ClassName.get("",entityRestDataClassName)
			)
			
			.addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")" 
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("$T $LInstance = commandExecutor.execute(new $T())",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityVariableName,
				ClassName.get(commandPackage,"Create" + entityInterfaceName + "Cmd")
			).addStatement("$L.copyTo$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).addStatement("commandExecutor.execute(new $T($LInstance))",
					ClassName.get(commandPackage,"Save" + entityInterfaceName + "Cmd"),
				entityVariableName
			).addStatement("$L.copyFrom$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			)
			.addStatement("java.util.Map<String, String> result = new java.util.HashMap<String, String>()")
			.addStatement("result.put(\"id\"," + entityVariableName + ".getId())")
			.addStatement("restResponse.setResponse(result)")
			.addStatement("restResponse.setMessage(\"Added Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during create\",ex)")
			.beginControlFlow("if(ex.getMessage().contains(\"Duplicate\"))")
			.addStatement("restResponse.setMessage(\"Record already exists\")")
			.nextControlFlow("else")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);
		
		/* BulkCreate method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("bulkcreate")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "bulkcreate")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityVariableName + "Json")
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
						.addMember("value", "$S", entityVariableName + "List")
						.build()
				).build()
			).addStatement("RestResponse restResponse = new RestResponse()")
			.addStatement("int count = 0")
			.beginControlFlow("try"
			).addStatement("$T $L = new $T()",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
				"objectMapper",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper")
			).addStatement("$T $L = objectMapper.readValue($T.decode($LJson),objectMapper.getTypeFactory().constructCollectionType($T.class, $T.class))",
				ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get("",entityRestDataClassName)),
				entityVariableName + "s",
				ClassName.get("java.net", "URLDecoder"),
				entityVariableName,
				ClassName.get(List.class),
				ClassName.get("",entityRestDataClassName)
			).addStatement("$T $L = new $T()",
					ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get(String.class)),
					"ret" + entityVariableName + "s",
					ParameterizedTypeName.get(ClassName.get(ArrayList.class),ClassName.get(String.class))
				)
			.addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")"  
			).beginControlFlow("for($T $L:$L)",
				ClassName.get("",entityRestDataClassName),
				entityVariableName,
				entityVariableName + "s"
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("$T $LInstance = commandExecutor.execute(new $T())",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityVariableName,
				ClassName.get(commandPackage,"Create" + entityInterfaceName + "Cmd")
			).addStatement("$L.copyTo$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).beginControlFlow("try")
			.addStatement("commandExecutor.execute(new $T($LInstance))",
					ClassName.get(commandPackage,"Save" + entityInterfaceName + "Cmd"),
				entityVariableName
			).addStatement("$L.copyFrom$L($LInstance)",
					entityVariableName,
					entityInterfaceName,
					entityVariableName
				)
			.addStatement("count++")
			.addStatement(
					"$L.add($L.getId())",
					"ret" + entityVariableName + "s",
					entityVariableName
				)
			.nextControlFlow("catch(Exception e)")
			.addStatement("continue")
			.endControlFlow()
			.endControlFlow()
			.addStatement("restResponse.setResponse(ret" + entityVariableName + "s" + ")")
			.addStatement("restResponse.setMessage(count + \" records added Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during bulkCreate\",ex)")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);
		
		
		String entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
		String entityQueryInterfacePackage = processingEnvironment.getElementUtils().getPackageOf(entityQueryTypeElement).getQualifiedName().toString();

		/* Update method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("update")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "update")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityVariableName + "Json")
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
						.addMember("value", "$S", entityVariableName)
						.build()
				).build()				
			).addStatement("RestResponse restResponse = new RestResponse()")
			.beginControlFlow("try"
			).addStatement("$T $L = new $T().readValue($T.decode($LJson),$T.class)",
				ClassName.get("",entityRestDataClassName),
				entityVariableName,
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
				ClassName.get("java.net", "URLDecoder"),
				entityVariableName,
				ClassName.get("",entityRestDataClassName)
			).addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")" 
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("$T.$L $LQuery = commandExecutor.execute(new $T())",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityQueryInterfaceName,
				entityVariableName,
				ClassName.get(commandPackage,"Create" + entityInterfaceName + "QueryCmd")
			).addStatement(
				"$T $LInstance = $LQuery.$L($L.getId()).singleResult();",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityVariableName,
				entityVariableName,
				QueryClassUtil.locateIdSetterMethod(entityQueryTypeElement),
				entityVariableName
			).beginControlFlow("if($LInstance == null)",entityVariableName)
			.addStatement("throw new RuntimeException(\"Could not find $L with id \" + $L.$L)",
				entityInterfaceName,
				entityVariableName,
				"getId()"
			).endControlFlow()
			.addStatement("$L.copyTo$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).addStatement("commandExecutor.execute(new $T($LInstance))",
				ClassName.get(commandPackage,"Save" + entityInterfaceName + "Cmd"),
				entityVariableName
			).addStatement("$L.copyFrom$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).addStatement("java.util.Map<String, String> result = new java.util.HashMap<String, String>()")
			.addStatement("result.put(\"id\"," + entityVariableName + ".getId())")
			.addStatement("restResponse.setResponse(result)")
			.addStatement("restResponse.setMessage(\"Updated Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during update\",ex)")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);

		/* BulkUpdate method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("bulkupdate")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "bulkupdate")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityVariableName + "Json")
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
						.addMember("value", "$S", entityVariableName + "List")
						.build()
				).build()
			).addStatement("RestResponse restResponse = new RestResponse()")
			.addStatement("int count = 0")
			.beginControlFlow("try"
			).addStatement("$T $L = new $T()",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
				"objectMapper",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper")
			).addStatement("$T $L = objectMapper.readValue($T.decode($LJson),objectMapper.getTypeFactory().constructCollectionType($T.class, $T.class))",
				ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get("",entityRestDataClassName)),
				entityVariableName + "s",
				ClassName.get("java.net", "URLDecoder"),
				entityVariableName,
				ClassName.get(List.class),
				ClassName.get("",entityRestDataClassName)
			).addStatement("$T $L = new $T()",
					ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get(String.class)),
					"ret" + entityVariableName + "s",
					ParameterizedTypeName.get(ClassName.get(ArrayList.class),ClassName.get(String.class))
				)
			.addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")" 
			).beginControlFlow("for($T $L:$L)",
				ClassName.get("",entityRestDataClassName),
				entityVariableName,
				entityVariableName + "s"
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("$T.$L $LQuery = commandExecutor.execute(new $T())",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityQueryInterfaceName,
				entityVariableName,
				ClassName.get(commandPackage,"Create" + entityInterfaceName + "QueryCmd")
			).addStatement(
				"$T $LInstance = $LQuery.$L($L.getId()).singleResult();",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityVariableName,
				entityVariableName,
				QueryClassUtil.locateIdSetterMethod(entityQueryTypeElement),
				entityVariableName
			)
			.addStatement("$L.copyTo$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).beginControlFlow("try")
			.addStatement("commandExecutor.execute(new $T($LInstance))",
				ClassName.get(commandPackage,"Save" + entityInterfaceName + "Cmd"),
				entityVariableName
			).addStatement("$L.copyFrom$L($LInstance)",
					entityVariableName,
					entityInterfaceName,
					entityVariableName
				)
			.addStatement("count++")
			.addStatement(
					"$L.add($L.getId())",
					"ret" + entityVariableName + "s",
					entityVariableName
				)
			.nextControlFlow("catch(Exception e)")
			.addStatement("continue")
			.endControlFlow()
			.endControlFlow()
			.addStatement("restResponse.setResponse(ret" + entityVariableName + "s" + ")")
			.addStatement("restResponse.setMessage(count + \" records updated Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during bulkUpdate\",ex)")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);		
		
		String entityIdVariableName = entityVariableName + "Id";
		
		/* Delete method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("delete")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "delete/{id}")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityIdVariableName)
					.addAnnotation(
						AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","PathVariable"))
							.addMember("value", "$S", "id")
							.build()
					).build()
			
			).addStatement("RestResponse restResponse = new RestResponse()")
			.beginControlFlow("try"
			).addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")"
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("commandExecutor.execute(new $T($L))",
				ClassName.get(commandPackage,"Delete" + entityInterfaceName + "Cmd"),
				entityIdVariableName
			).addStatement("restResponse.setResponse(true)")
			.addStatement("restResponse.setMessage(\"Deleted Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during delete\",ex)")
			.addStatement("restResponse.setResponse(false)")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);


		/* BulkDelete method */
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("bulkdelete")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "bulkdelete")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("com.rlogistics.master", "RestResponse"))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityIdVariableName + "ListJson")
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
						.addMember("value", "$S", entityIdVariableName + "List")
						.build()
				).build()
			).addStatement("RestResponse restResponse = new RestResponse()")
			.addStatement("int count = 0")
			.beginControlFlow("try"
			).addStatement("$T $L = new $T()",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
				"objectMapper",
				ClassName.get("com.fasterxml.jackson.databind","ObjectMapper")
			).addStatement("$T $L = objectMapper.readValue($T.decode($LListJson),objectMapper.getTypeFactory().constructCollectionType($T.class, $T.class))",
				ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get(String.class)),
				entityIdVariableName + "s",
				ClassName.get("java.net", "URLDecoder"),
				entityIdVariableName,
				ClassName.get(List.class),
				ClassName.get(String.class)
			).addStatement("$T $L = new $T()",
				ParameterizedTypeName.get(ClassName.get(List.class),ClassName.get(Boolean.class)),
				"ret" + entityIdVariableName + "s",
				ParameterizedTypeName.get(ClassName.get(ArrayList.class),ClassName.get(Boolean.class))
			).addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse))\n\tthrow new Exception (\"rlToken null or invalid\")" 
			).beginControlFlow("for($T $L:$L)",
				ClassName.get(String.class),
				entityIdVariableName,
				entityIdVariableName + "s"
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("commandExecutor.execute(new $T($L))",
				ClassName.get(commandPackage,"Delete" + entityInterfaceName + "Cmd"),
				entityIdVariableName
			).addStatement(
				"$L.add($L)",
				"ret" + entityIdVariableName + "s",
				"true"
			).addStatement("count++")
			.endControlFlow()
			.addStatement("java.util.Map<String, String> result = new java.util.HashMap<String, String>()")
			.addStatement("result.put(\"recordsUpdated\",String.valueOf(count))")
			.addStatement("restResponse.setResponse(result)")
			.addStatement("restResponse.setMessage(\"Deleted Successfully\")")
			.nextControlFlow("catch(Exception ex)")
			.addStatement("httpResponse.setStatus(500)")
			.addStatement("log.error(\"Exception during bulkDelete\",ex)")
			.addStatement("restResponse.setMessage(ex.getMessage())")
			.endControlFlow()
			.addStatement("return $L","restResponse")
			.build()
		);
		
		String entityQueryImplClassName = entityQueryInterfaceName + "Impl";
		String entityQueryImplClassPackage = entityQueryInterfacePackage + ".impl";

		/* Get method*/
		typeBuilder.addMethod(
			MethodSpec.methodBuilder("get")
			.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
				.addMember("value", "$S", prefix + "get/{id}")
				.addMember("produces", "$S", "application/json")
				.build()
			).returns(ClassName.get("", entityRestDataClassName))
			.addParameter(
				ClassName.get("javax.servlet.http","HttpServletRequest"),
				"httpRequest"
			).addParameter(
				ClassName.get("javax.servlet.http","HttpServletResponse"),
				"httpResponse"
			).addParameter(
				ParameterSpec.builder(ClassName.get(String.class),entityIdVariableName)
				.addAnnotation(
					AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","PathVariable"))
						.addMember("value", "$S", "id")
						.build()
				).build()
			).beginControlFlow("try"
			).addStatement(
				"if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}"
			).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
				ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
				ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
				ClassName.get("org.activiti.engine","ProcessEngines")
			).addStatement("$T.$L $LQuery = commandExecutor.execute(new $T())",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityQueryInterfaceName,
				entityVariableName,
				ClassName.get(commandPackage,"Create" + entityInterfaceName + "QueryCmd")
			).addStatement(
				"$T $LInstance = $LQuery.$L($L).singleResult();",
				ClassName.get(entityInterfacePackage,entityInterfaceName),
				entityVariableName,
				entityVariableName,
				QueryClassUtil.locateIdSetterMethod(entityQueryTypeElement),
				entityIdVariableName
			).addStatement(
				"$T $L = new $T()",
				ClassName.get("", entityRestDataClassName),
				entityVariableName,
				ClassName.get("", entityRestDataClassName)
			).addStatement("$L.copyFrom$L($LInstance)",
				entityVariableName,
				entityInterfaceName,
				entityVariableName
			).addStatement("return $L",entityVariableName)
			.nextControlFlow("catch(Exception ex)")
			.addStatement("log.error(\"Exception during get\",ex)")
			.addStatement("return null")
			.endControlFlow()
			.build()
		);
		
		/* Query method */
		MethodSpec.Builder queryMethodBuilder = MethodSpec.methodBuilder("query")
				.addAnnotation(AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestMapping"))
						.addMember("value", "$S", prefix + "query")
						.addMember("produces", "$S", "application/json")
						.build()
					).returns(ClassName.get("com.rlogistics.master", "MasterPaginationResponse"))
					.addParameter(
						ClassName.get("javax.servlet.http","HttpServletRequest"),
						"httpRequest"
					).addParameter(
						ClassName.get("javax.servlet.http","HttpServletResponse"),
						"httpResponse"
					).addParameter(
						ParameterSpec.builder(ClassName.get(String.class),entityVariableName + "Json")
						.addAnnotation(
							AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
								.addMember("value", "$S", "query")
								.build()
						).build()				
					).addParameter(
						ParameterSpec.builder(ClassName.INT,"firstResult")
						.addAnnotation(
							AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
								.addMember("value", "$S", "firstResult")
								.addMember("required", "$L", "false")
								.addMember("defaultValue", "$S", "0")
								.build()
						).build()				
					).addParameter(
						ParameterSpec.builder(ClassName.INT,"maxResults")
						.addAnnotation(
							AnnotationSpec.builder(ClassName.get("org.springframework.web.bind.annotation","RequestParam"))
								.addMember("value", "$S", "maxResults")
								.addMember("required", "$L", "false")
								.addMember("defaultValue", "$S", "25")
								.build()
						).build()				
					).addStatement("MasterPaginationResponse restResponse = new MasterPaginationResponse()")
					.beginControlFlow("try"
					).addStatement("$T $L = new $T().readValue($T.decode($LJson),$T.class)",
						ClassName.get("",entityRestQueryDataClassName),
						entityVariableName,
						ClassName.get("com.fasterxml.jackson.databind","ObjectMapper"),
						ClassName.get("java.net", "URLDecoder"),
						entityVariableName,
						ClassName.get("",entityRestQueryDataClassName)
					).addStatement(
						"if(!beforeMethodInvocation(httpRequest,httpResponse)){return null;}"
					).addStatement("$T commandExecutor = (($T)($T.getDefaultProcessEngine().getProcessEngineConfiguration())).getCommandExecutor()",
						ClassName.get("org.activiti.engine.impl.interceptor","CommandExecutor"),
						ClassName.get("org.activiti.engine.impl.cfg","ProcessEngineConfigurationImpl"),
						ClassName.get("org.activiti.engine","ProcessEngines")
					).addStatement("$T $LQuery = ($T)commandExecutor.execute(new $T())",
						ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
						entityVariableName,
						ClassName.get(entityQueryImplClassPackage,entityQueryImplClassName),
						ClassName.get(commandPackage,"Create" + entityInterfaceName + "QueryCmd"));		
		
		for(Element ee:entityQueryTypeElement.getEnclosedElements()){

			MasterEntityQueryParameter mqp = ee.getAnnotation(MasterEntityQueryParameter.class);
			
			if(mqp == null){
				continue;
			}
			
			String attributeName = mqp.attribute();
			String attributeNameInitCaps = BeanUtil.initialUpperCase(attributeName);
			
			VariableElement firstParameterElement = ((ExecutableElement)ee).getParameters().get(0);
			TypeName attributeType = ClassName.get(firstParameterElement.asType());			

			queryMethodBuilder.addStatement("$LQuery.$L($L.$L())",
				entityVariableName,
				ee.getSimpleName().toString(),
				entityVariableName,
				(attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps
			);
			
			if(processingEnvironment.getTypeUtils().isSameType(firstParameterElement.asType(),processingEnvironment.getElementUtils().getTypeElement("java.lang.String").asType())){
				String attributeLikeName = attributeName + "Like";
				String attributeLikeNameInitCaps = BeanUtil.initialUpperCase(attributeLikeName);
				
				queryMethodBuilder.addStatement("$LQuery.$L($L.$L())",
						entityVariableName,
						ee.getSimpleName().toString() + "Like",
						entityVariableName,
						(attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeLikeNameInitCaps
					);
				
				String attributeNotEqualsName = attributeName + "NotEquals";
				String attributeNotEqualsNameInitCaps = BeanUtil.initialUpperCase(attributeNotEqualsName);
				
				queryMethodBuilder.addStatement("$LQuery.$L($L.$L())",
						entityVariableName,
						ee.getSimpleName().toString() + "NotEquals",
						entityVariableName,
						(attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNotEqualsNameInitCaps
					);
			}
		}
		
		queryMethodBuilder.addStatement("long totalRecords = $LQuery.count()",
				entityVariableName
			);
		
		queryMethodBuilder.addStatement("$T $LList = $LQuery.listPage(firstResult,maxResults)",
			ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get(entityInterfacePackage, entityInterfaceName)),
			entityVariableName,
			entityVariableName
		).addStatement("$T $LReturnList = new $T()",
			ParameterizedTypeName.get(ClassName.get(List.class), ClassName.get("", entityRestDataClassName)),
			entityVariableName,
			ParameterizedTypeName.get(ClassName.get(ArrayList.class), ClassName.get("", entityRestDataClassName))
		)
		.beginControlFlow("for($T $LEntry:$LList)",
			ClassName.get(entityInterfacePackage, entityInterfaceName),
			entityVariableName,
			entityVariableName
		).addStatement("$T $LReturnEntry = new $T()",
			ClassName.get("", entityRestDataClassName),
			entityVariableName,
			ClassName.get("", entityRestDataClassName)
		).addStatement("$LReturnEntry.copyFrom$L($LEntry)",
			entityVariableName,
			entityInterfaceName,
			entityVariableName
		).addStatement("$LReturnList.add($LReturnEntry)",
			entityVariableName,
			entityVariableName
		).endControlFlow().addStatement("restResponse.setData($LReturnList)",entityVariableName).addStatement("restResponse.setTotalRecords(totalRecords)")
		.addStatement("return $L","restResponse")
		.nextControlFlow("catch(Exception ex)")
		.addStatement("log.error(\"Exception during query\",ex)")
		.addStatement("return null")
		.endControlFlow();
		typeBuilder.addMethod(queryMethodBuilder.build());
		
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityRESTPackage, typeBuilder);		
	}
}
