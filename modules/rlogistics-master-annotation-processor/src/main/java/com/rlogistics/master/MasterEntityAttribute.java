package com.rlogistics.master;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MasterEntityAttribute {
	public static String  UNASSIGNED_COLUMN_NAME = "[unassigned]";

	String columnName() default UNASSIGNED_COLUMN_NAME;
}
