package com.rlogistics.master.processor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.NoType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;

import com.rlogistics.master.MasterEntityAttribute;
import com.rlogistics.master.MasterEntityQuery;
import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

public class EntityClassUtil {

	public static void createEntityClass(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement){
		PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);
		
		/* Create Entity implemnetation */
		String entityInterfaceName = entityTypeElement.getSimpleName().toString();
		String entityImplClassName = entityInterfaceName + "Entity";
		String entityInterfacePackage = packageElement.getQualifiedName().toString();
		String entityImplClassPackage = entityInterfacePackage + ".impl.persistence.entity";
	
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityImplClassName)
			.addModifiers(Modifier.PUBLIC)
			.superclass(ClassName.get("org.activiti.engine.impl.persistence.entity.rlogistics","BaseProcessEntity"))
			.addSuperinterface(ClassName.get(entityTypeElement.asType()))
			.addSuperinterface(ClassName.get(Serializable.class))
			.addSuperinterface(ClassName.get("org.activiti.engine.impl.db","PersistentObject"))
			.addSuperinterface(ClassName.get("org.activiti.engine.impl.db","HasRevision"));
	
		typeBuilder.addMethod(
			MethodSpec.constructorBuilder()
				.addModifiers(Modifier.PUBLIC)
				.addStatement("this.id = generateId()")
				.build()
		);
		
		/* Build a persistence method attribute by attribute while building beanness*/
		MethodSpec.Builder persistenceMethodBuilder = MethodSpec.methodBuilder("getPersistentState")
				.addAnnotation(ClassName.get(Override.class))
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(Object.class));
		persistenceMethodBuilder.addStatement("$T<String, Object> persistentState = new $T<String, Object>()",Map.class,HashMap.class);
		for(Element ee:entityTypeElement.getEnclosedElements()){
			MasterEntityAttribute mea = ee.getAnnotation(MasterEntityAttribute.class);
			
			if(mea == null) continue;
			
			EntityClassUtil.processMasterEntityAttribute(processingEnvironment,(ExecutableElement)ee,mea, typeBuilder);
		
			String attributeName = EntityClassUtil.parseAttributeName(processingEnvironment, (ExecutableElement)ee);
			persistenceMethodBuilder.addStatement("persistentState.put($S,$L)", attributeName,attributeName);
		}
		
		persistenceMethodBuilder.addStatement("return persistentState");
		
		typeBuilder.addMethod(persistenceMethodBuilder.build());
		
		/* Add a delete method */
		typeBuilder.addMethod(
				MethodSpec.methodBuilder("delete")
				.addModifiers(Modifier.PUBLIC)
				.addStatement("$T.getCommandContext().getDbSqlSession().delete(this)",ClassName.get("org.activiti.engine.impl.context", "Context"))
				.build()
		);
		
		/* Add revision field and methods*/
		EntityClassUtil.addBeanAttribute(typeBuilder, TypeName.INT, "revision");
		typeBuilder.addMethod(
				MethodSpec.methodBuilder("getRevisionNext")
				.addAnnotation(ClassName.get(Override.class))
				.addModifiers(Modifier.PUBLIC)
				.returns(TypeName.INT)
				.addStatement("return revision + 1")
				.build()
		);
		
		JavaFileUtil.writeTypeSpecToFile(processingEnvironment, entityImplClassPackage, typeBuilder);		
	}

	public static void addBeanAttribute(TypeSpec.Builder typeBuilder,TypeName attributeType,String attributeName){
		String attributeNameInitCaps = BeanUtil.initialUpperCase(attributeName);
		
		if(attributeName == null) return;
		
		if(attributeType.equals(ArrayTypeName.of(TypeName.BYTE))){
			/* use ByteArrayRef*/
			TypeName byteArrayRefTypeName = ClassName.get("org.activiti.engine.impl.persistence.entity", "ByteArrayRef");
			typeBuilder.addField(
					FieldSpec.builder(byteArrayRefTypeName, attributeName)
					.initializer("new $T()", ClassName.get("org.activiti.engine.impl.persistence.entity", "ByteArrayRef"))
					.build()
				);
			typeBuilder.addMethod(
					MethodSpec.methodBuilder("get" + attributeNameInitCaps + "ByteArrayRef")
						.addModifiers(Modifier.PUBLIC)
						.returns(byteArrayRefTypeName)
						.addStatement("return this.$L",attributeName)
						.build()
				);
		
			typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeNameInitCaps + "ByteArrayRef")
						.addModifiers(Modifier.PUBLIC)
						.addParameter(byteArrayRefTypeName, attributeName)
						.addStatement("this.$L = $L",attributeName,attributeName)
						.build()
				);

			typeBuilder.addMethod(
					MethodSpec.methodBuilder("get" + attributeNameInitCaps)
						.addAnnotation(ClassName.get(Override.class))
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L.getBytes()",attributeName)
						.build()
				);
		
			typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeNameInitCaps)
						.addAnnotation(ClassName.get(Override.class))
						.addModifiers(Modifier.PUBLIC)
						.addParameter(attributeType, attributeName)
						.addStatement("this.$L.setValue($S,$L)",attributeName,"value-" + attributeName,attributeName)
						.build()
				);
		} else {
			typeBuilder.addField(
					FieldSpec.builder(attributeType, attributeName)
					.build()
				);
			typeBuilder.addMethod(
					MethodSpec.methodBuilder((attributeType.equals(TypeName.BOOLEAN) ? "is" : "get") + attributeNameInitCaps)
						.addAnnotation(ClassName.get(Override.class))
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeType)
						.addStatement("return this.$L",attributeName)
						.build()
				);
		
			typeBuilder.addMethod(
					MethodSpec.methodBuilder("set" + attributeNameInitCaps)
						.addAnnotation(ClassName.get(Override.class))
						.addModifiers(Modifier.PUBLIC)
						.addParameter(attributeType, attributeName)
						.addStatement("this.$L = $L",attributeName,attributeName)
						.build()
				);

		}
	}

	public static String parseAttributeName(ProcessingEnvironment processingEnvironment, ExecutableElement e) {
		String methodName = e.getSimpleName().toString();
		
		if(e.getParameters() != null && (e.getParameters().size() > 0)) {
			processingEnvironment.getMessager().printMessage(Kind.ERROR, "Unexpected method signature " + e.toString() + ". Expected void method.");
			return null;
		}
		
		if(e.getReturnType() instanceof NoType){
			processingEnvironment.getMessager().printMessage(Kind.ERROR, "Unexpected method signature " + e.toString() + ". Expected a non-void return type.");
			return null;
		}
		
		if(methodName.startsWith("is")){
			methodName = methodName.substring(2);
		} else if(methodName.startsWith("get")){
			methodName = methodName.substring(3);
		} else {
			processingEnvironment.getMessager().printMessage(Kind.ERROR, "Unexpected formatting of method name" + e.toString() + ". Expected a get or is method.");
			return null;
		}
		
		return BeanUtil.initialLowerCase(methodName);
	}

	public static void processMasterEntityAttribute(ProcessingEnvironment processingEnvironment, ExecutableElement e, MasterEntityAttribute mea, TypeSpec.Builder typeBuilder) {
		TypeMirror attributeType = e.getReturnType();
	
		String attributeName = parseAttributeName(processingEnvironment, e);
		
		addBeanAttribute(typeBuilder,ClassName.get(attributeType),attributeName);
	}

	public static TypeElement locateQueryTypeElement(TypeElement entityTypeElement){
		TypeElement entityQueryTypeElement = null;
		for (Element ee : entityTypeElement.getEnclosedElements()) {
			MasterEntityQuery meq = ee.getAnnotation(MasterEntityQuery.class);
			if (meq != null) {
				entityQueryTypeElement = (TypeElement) ee;
				break;
			}
		}
		
		return entityQueryTypeElement;
	}
}
