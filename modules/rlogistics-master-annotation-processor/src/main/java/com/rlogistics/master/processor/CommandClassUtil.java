package com.rlogistics.master.processor;

import java.io.Serializable;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;

public class CommandClassUtil {

	public static void createCommandClasses(ProcessingEnvironment processingEnv, TypeElement entityTypeElement,TypeElement entityQueryTypeElement) {
		createCreateCommandClass(processingEnv, entityTypeElement);
		if(entityQueryTypeElement != null){
			createCreateQueryCommandClass(processingEnv, entityTypeElement,entityQueryTypeElement);
		}
		createDeleteCommandClass(processingEnv, entityTypeElement);
		createSaveCommandClass(processingEnv, entityTypeElement);
	}

	
	private static void createCreateCommandClass(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
		new CreateCommandClassCreator(processingEnvironment, entityTypeElement).create();
	}

	private static void createCreateQueryCommandClass(ProcessingEnvironment processingEnvironment,TypeElement entityTypeElement,TypeElement entityQueryTypeElement) {
		new CreateQueryCommandClassCreator(processingEnvironment, entityTypeElement,entityQueryTypeElement).create();
	}

	private static void createDeleteCommandClass(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
		new DeleteCommandClassCreator(processingEnvironment, entityTypeElement).create();
	}

	private static void createSaveCommandClass(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
		new SaveCommandClassCreator(processingEnvironment, entityTypeElement).create();
	}

	private static abstract class CommandClassCreator {
		protected ProcessingEnvironment processingEnvironment;
		protected TypeElement entityTypeElement;
		protected String entityInterfaceName;
		protected String entityInterfacePackage;
		protected String entityIdentityManagerInterfaceName;
		protected String entityIdentityManagerInterfacePackage;

		protected String commandPackage;
		public CommandClassCreator(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement){
			this.processingEnvironment = processingEnvironment;
			this.entityTypeElement = entityTypeElement;
			
			PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityTypeElement);

			entityInterfaceName = entityTypeElement.getSimpleName().toString();
			entityInterfacePackage = packageElement.getQualifiedName().toString();

			entityIdentityManagerInterfaceName = entityInterfaceName + "IdentityManager";
			entityIdentityManagerInterfacePackage = entityInterfacePackage + ".impl";

			commandPackage = entityInterfacePackage + ".impl.cmd";
		}
		
		public void create() {
			/* Create Entity implemnetation */
			TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(getCommandClassName()).addModifiers(Modifier.PUBLIC)
					.addSuperinterface(
							ParameterizedTypeName.get(ClassName.get("org.activiti.engine.impl.interceptor", "Command"),
									ClassName.get(entityInterfacePackage, entityInterfaceName)))
					.addSuperinterface(Serializable.class);

			addConstructor(typeBuilder);
			addExecuteMethod(typeBuilder);

			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, commandPackage, typeBuilder);
		}
		
		protected void addConstructor(TypeSpec.Builder typeBuilder) {
			
		}

		protected void addExecuteMethod(TypeSpec.Builder typeBuilder) {
			typeBuilder.addMethod(getExecuteMethodSpec().build());
		}

		protected abstract String getCommandClassName();
		protected abstract MethodSpec.Builder getExecuteMethodSpec();
	}
	
	private static class CreateCommandClassCreator extends CommandClassCreator{

		protected String commandClassName;

		
		public CreateCommandClassCreator(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
			super(processingEnvironment, entityTypeElement);
			commandClassName = "Create" + entityInterfaceName + "Cmd";
		}

		@Override
		protected MethodSpec.Builder getExecuteMethodSpec() {
			return MethodSpec.methodBuilder("execute").addModifiers(Modifier.PUBLIC)
			.returns(ClassName.get(entityInterfacePackage, entityInterfaceName))
			.addParameter(ClassName.get("org.activiti.engine.impl.interceptor", "CommandContext"),
					"commandContext")
			.addStatement("return (($T)commandContext).getIdentityManager($T.class).createNew$L()",
					ClassName.get("com.rlogistics.master.identity.impl.interceptor",
							"RLogisticsCommandContext"),
					ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
					entityTypeElement.getSimpleName().toString());
		}

		@Override
		protected String getCommandClassName() {
			return commandClassName;
		}		
	}

	private static class CreateQueryCommandClassCreator extends CommandClassCreator{

		protected String commandClassName;
		protected String entityQueryInterfaceName;
		protected String entityQueryInterfacePackage;
		protected TypeElement entityQueryTypeElement;
		
		public CreateQueryCommandClassCreator(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement,TypeElement entityQueryTypeElement) {
			super(processingEnvironment, entityTypeElement);
			commandClassName = "Create" + entityInterfaceName + "QueryCmd";

			this.entityQueryTypeElement = entityQueryTypeElement;
			
			PackageElement packageElement = processingEnvironment.getElementUtils().getPackageOf(entityQueryTypeElement);

			entityQueryInterfaceName = entityQueryTypeElement.getSimpleName().toString();
			entityQueryInterfacePackage = packageElement.getQualifiedName().toString();
		}

		public void create() {
			/* Create Entity implemnetation */
			TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(getCommandClassName()).addModifiers(Modifier.PUBLIC)
					.addSuperinterface(
							ParameterizedTypeName.get(ClassName.get("org.activiti.engine.impl.interceptor", "Command"),
									ClassName.get(entityQueryTypeElement.asType())))
					.addSuperinterface(Serializable.class);

			addConstructor(typeBuilder);
			addExecuteMethod(typeBuilder);

			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, commandPackage, typeBuilder);
		}
		
		@Override
		protected MethodSpec.Builder getExecuteMethodSpec() {
			return MethodSpec.methodBuilder("execute")
				.addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(entityQueryTypeElement.asType()))
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor", "CommandContext"),"commandContext")
				.addStatement(
					"return (($T)commandContext).getIdentityManager($T.class).createNew$LQuery()",
					ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
					ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
					entityTypeElement.getSimpleName().toString()
			);
		}

		@Override
		protected String getCommandClassName() {
			return commandClassName;
		}		
	}

	private static class DeleteCommandClassCreator extends CommandClassCreator{

		protected String commandClassName;
		
		public DeleteCommandClassCreator(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
			super(processingEnvironment, entityTypeElement);
			commandClassName = "Delete" + entityInterfaceName + "Cmd";
		}

		public void create() {
			/* Create Entity implemnetation */
			TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(getCommandClassName()).addModifiers(Modifier.PUBLIC)
					.addSuperinterface(
							ParameterizedTypeName.get(ClassName.get("org.activiti.engine.impl.interceptor", "Command"),
									ClassName.get(Void.class)))
					.addSuperinterface(Serializable.class);

			addConstructor(typeBuilder);
			addExecuteMethod(typeBuilder);

			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, commandPackage, typeBuilder);
		}

		@Override
		protected MethodSpec.Builder getExecuteMethodSpec() {
			return MethodSpec.methodBuilder("execute").addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(Void.class))
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor", "CommandContext"),"commandContext")
				.addStatement(
					"(($T)commandContext).getIdentityManager($T.class).delete$L(id)",
					ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
					ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
					entityTypeElement.getSimpleName().toString()
				).addStatement("return null");
		}

		protected void addConstructor(com.squareup.javapoet.TypeSpec.Builder typeBuilder) {
			
			typeBuilder.addField(ClassName.get(String.class),"id",Modifier.PRIVATE);

			typeBuilder.addMethod(
				MethodSpec.constructorBuilder()
					.addModifiers(Modifier.PUBLIC)
					.addParameter(ClassName.get(String.class), "id")
					.addStatement("this.id = id")
					.build()
			);
		}

		@Override
		protected String getCommandClassName() {
			return commandClassName;
		}		
	}

	private static class SaveCommandClassCreator extends CommandClassCreator{

		protected String commandClassName;
		
		public SaveCommandClassCreator(ProcessingEnvironment processingEnvironment, TypeElement entityTypeElement) {
			super(processingEnvironment, entityTypeElement);
			commandClassName = "Save" + entityInterfaceName + "Cmd";
		}
		
		public void create() {
			/* Create Entity implemnetation */
			TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(getCommandClassName()).addModifiers(Modifier.PUBLIC)
					.addSuperinterface(
							ParameterizedTypeName.get(ClassName.get("org.activiti.engine.impl.interceptor", "Command"),
									ClassName.get(Void.class)))
					.addSuperinterface(Serializable.class);

			addConstructor(typeBuilder);
			addExecuteMethod(typeBuilder);

			JavaFileUtil.writeTypeSpecToFile(processingEnvironment, commandPackage, typeBuilder);
		}

		@Override
		protected MethodSpec.Builder getExecuteMethodSpec() {
			return MethodSpec.methodBuilder("execute").addModifiers(Modifier.PUBLIC)
				.returns(ClassName.get(Void.class))
				.addParameter(ClassName.get("org.activiti.engine.impl.interceptor", "CommandContext"),"commandContext")
				.beginControlFlow(
					"if((($T)commandContext).getIdentityManager($T.class).isNew$L(entity))",
					ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
					ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
					entityTypeElement.getSimpleName().toString()
				)
				.addStatement(
					"(($T)commandContext).getIdentityManager($T.class).insert$L(entity)",
					ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
					ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
					entityTypeElement.getSimpleName().toString()
				).nextControlFlow("else")
				.addStatement(
						"(($T)commandContext).getIdentityManager($T.class).update$L(entity)",
						ClassName.get("com.rlogistics.master.identity.impl.interceptor","RLogisticsCommandContext"),
						ClassName.get(entityIdentityManagerInterfacePackage, entityIdentityManagerInterfaceName),
						entityTypeElement.getSimpleName().toString()
					)
				.endControlFlow()
				.addStatement("return null");
		}

		protected void addConstructor(TypeSpec.Builder typeBuilder) {
			
			typeBuilder.addField(ClassName.get(entityInterfacePackage,entityInterfaceName),"entity",Modifier.PRIVATE);

			typeBuilder.addMethod(
				MethodSpec.constructorBuilder()
					.addModifiers(Modifier.PUBLIC)
					.addParameter(ClassName.get(entityInterfacePackage,entityInterfaceName), "entity")
					.addStatement("this.entity = entity")
					.build()
			);
		}

		@Override
		protected String getCommandClassName() {
			return commandClassName;
		}		
	}
}
